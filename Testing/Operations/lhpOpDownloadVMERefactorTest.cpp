/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: lhpOpDownloadVMERefactorTest.cpp,v $
Language:  C++
Date:      $Date: 2009-09-19 17:52:26 $
Version:   $Revision: 1.1.2.5 $
Authors:   Stefano Perticoni
==========================================================================
Copyright (c) 2002/2004 
CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/

#include "mafDefines.h" 
#include "lhpDefines.h"
#include <wx/file.h>

//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include <cppunit/config/SourcePrefix.h>
#include "lhpOpDownloadVMERefactorTest.h"
#include "lhpUser.h"
#include "medVMEFactory.h"
#include "mafVMEStorage.h"
#include "mafVMERoot.h"
#include "mafVMESurface.h"

#include "mafOpValidateTree.h"

void lhpOpDownloadVMERefactorTest::setUp()
{
  m_PythonExe = "c:\\Python25\\python.exe ";
  assert(wxFileExists(m_PythonExe.GetCStr()));

  m_PythonwExe = "c:\\Python25\\pythonw.exe ";
  assert(wxFileExists(m_PythonwExe.GetCStr()));

}

void lhpOpDownloadVMERefactorTest::tearDown()
{
  
}

void lhpOpDownloadVMERefactorTest::TestConstructorDestructor()
{
  lhpOpDownloadVMERefactor *op = new lhpOpDownloadVMERefactor("lhpOpDownloadVMERefactor");
  cppDEL(op);
}

void lhpOpDownloadVMERefactorTest::TestDownload_test_volume_AND_test_mesh_tetra10_dataresources()
{
  mafString lhpDataRoot = LHP_DATA_ROOT;
  mafString targetMSFForDownload = lhpDataRoot;
  targetMSFForDownload.Append("/lhpOpDownloadVMERefactorTest/targetMSFForDownload/targetMSFForDownload.msf");
  
  mafString basketListFileName = LHP_DATA_ROOT;
  basketListFileName.Append("/lhpOpDownloadVMERefactorTest/msfWith1SmallMeshAnd1SmallVolume/test_volume_AND_test_mesh_tetra10_dataresources.txt");

  mafString outputMSF = lhpDataRoot;
  outputMSF.Append("/lhpOpDownloadVMERefactorTest/targetMSFForDownload/downloadedMSF_test_volume_AND_test_mesh_tetra10_dataresources.msf");

  Download(basketListFileName, targetMSFForDownload, outputMSF);
  
  mafVMEStorage reloadStorage;
  reloadStorage.SetURL(outputMSF);
  reloadStorage.Restore();
  mafVMERoot *reloadedVMERoot = reloadStorage.GetRoot();

  std::map<int , std::string> vmeID_vmeType_Map;
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(35, "mafVMEGroup"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(5, "mafVMESurface"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(37, "mafVMEMesh"));

  CheckVMEsExistenceAndTypeInTree(reloadedVMERoot,vmeID_vmeType_Map);

  bool checkMSFSuccessful = CheckMSFConsistency(reloadedVMERoot);
  CPPUNIT_ASSERT(checkMSFSuccessful == true);
}

void lhpOpDownloadVMERefactorTest::TestDownload_msfWith1SmallMeshAnd1SmallVolume_dataresource()
{
  mafString lhpDataRoot = LHP_DATA_ROOT;

  mafString targetMSFForDownload = lhpDataRoot;
  targetMSFForDownload.Append("/lhpOpDownloadVMERefactorTest/targetMSFForDownload/targetMSFForDownload.msf");

  mafString basketListFileName = LHP_DATA_ROOT;
  basketListFileName.Append("/lhpOpDownloadVMERefactorTest/msfWith1SmallMeshAnd1SmallVolume/msfWith1SmallMeshAnd1SmallVolume_dataresource.txt");

  mafString outputMSF = lhpDataRoot;
  outputMSF.Append("/lhpOpDownloadVMERefactorTest/targetMSFForDownload/downloadedMSF_msfWith1SmallMeshAnd1SmallVolume_dataresource.msf");
  
  Download(basketListFileName, targetMSFForDownload, outputMSF);

  mafVMEStorage reloadStorage;
  reloadStorage.SetURL(outputMSF);
  reloadStorage.Restore();
  mafVMERoot *reloadedVMERoot = reloadStorage.GetRoot();
  

  std::map<int , std::string> vmeID_vmeType_Map;
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(35, "mafVMEGroup"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(5, "mafVMESurface"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(37, "mafVMEMesh"));

  CheckVMEsExistenceAndTypeInTree(reloadedVMERoot,vmeID_vmeType_Map);

  bool checkMSFSuccessful = CheckMSFConsistency(reloadedVMERoot);
  CPPUNIT_ASSERT(checkMSFSuccessful == true);

}

void lhpOpDownloadVMERefactorTest::TestDownload_msf_test_import_export_vme_Dataresource()
{
  mafString lhpDataRoot = LHP_DATA_ROOT;  
  
  mafString basketListFileName = LHP_DATA_ROOT;
  basketListFileName.Append("/lhpOpDownloadVMERefactorTest/msf_test_import_export_vme/msf_test_import_export_vme_Dataresource.txt");

  mafString targetMSFForDownload = lhpDataRoot;
  targetMSFForDownload.Append("/lhpOpDownloadVMERefactorTest/targetMSFForDownload/targetMSFForDownload.msf");

  mafString outputMSF = lhpDataRoot;
  outputMSF.Append("/lhpOpDownloadVMERefactorTest/targetMSFForDownload/downloadedMSF.msf");

  Download(basketListFileName, targetMSFForDownload, outputMSF);

  mafVMEStorage reloadStorage;
  reloadStorage.SetURL(outputMSF);
  reloadStorage.Restore();
  mafVMERoot *reloadedVMERoot = reloadStorage.GetRoot();

  std::map<int , std::string> vmeID_vmeType_Map;
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(35, "mafVMEGroup"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(5, "mafVMESurface"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(36, "mafVMEVolumeGray"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(37, "mafVMEMesh"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(39, "mafVMEVector"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(40, "mafVMELandmarkCloud"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(41, "medVMEAnalog"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(42, "mafVMEExternalData"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(43, "mafVMEImage"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(44, "mafVMEPolyline"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(45, "mafVMERawMotionData"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(46, "mafVMELandmarkCloud"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(47, "mafVMELandmarkCloud"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(48, "mafVMELandmarkCloud"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(49, "mafVMELandmarkCloud"));
  vmeID_vmeType_Map.insert(std::pair<int, std::string>(50, "mafVMELandmarkCloud"));

  CheckVMEsExistenceAndTypeInTree(reloadedVMERoot,vmeID_vmeType_Map);

  bool checkMSFSuccessful = CheckMSFConsistency(reloadedVMERoot);

  CPPUNIT_ASSERT(checkMSFSuccessful == true);
}

void lhpOpDownloadVMERefactorTest::Download( mafString inBasketListFileName, mafString inTargetMSFForDownload , mafString outMSFContainingDownloadedVMEs )
{
  assert(wxFileExists(inTargetMSFForDownload.GetCStr()));
  
  int result = medVMEFactory::Initialize();
  CPPUNIT_ASSERT(result == MAF_OK);

  mafVMEStorage loadStorage;
  loadStorage.SetURL(inTargetMSFForDownload);

  mafVMERoot *vmeRoot = loadStorage.GetRoot();
  loadStorage.Restore();

  wxInitialize();

  lhpOpDownloadVMERefactor *opDownload = new lhpOpDownloadVMERefactor("lhpOpDownloadVMERefactor");

  assert(opDownload->m_TestMode == false);
  opDownload->TestModeOn();

  opDownload->SetInput(vmeRoot);

  mafString targetMSFDirForDownload = inTargetMSFForDownload;
  targetMSFDirForDownload.Append("/../");
  assert(wxDirExists(targetMSFDirForDownload.GetCStr()));

  opDownload->SetMSFDirectoryABSPath(targetMSFDirForDownload);

  lhpUser *user = new lhpUser(NULL);
  int proxyFlag = 0;
  mafString proxyHost = "";
  mafString proxyPort = "";
  int rememberMe = 0;
  user->SetCredentials(mafString("testuser"),mafString("6w8DHF"),proxyFlag,proxyHost,proxyPort,rememberMe);
  opDownload->m_User = user;

  assert(wxFileExists(inBasketListFileName.GetCStr()));

  opDownload->SetResourcesToDownloadLocalFileName(inBasketListFileName);

  opDownload->m_PythonInterpreterFullPath = this->m_PythonExe;
  opDownload->m_PythonwInterpreterFullPath = this->m_PythonwExe;

  opDownload->Download();

  cout << "Storing MSF to disk for analysys...";
  
  mafString savedMSFFileName = outMSFContainingDownloadedVMEs;
  loadStorage.SetURL(savedMSFFileName);

  loadStorage.Store();
  cout << "Success storing to: " << loadStorage.GetURL();
  
  opDownload->KillThreadedUploaderDownloaderProcess();

  cppDEL(opDownload);
  cppDEL(user);

}

bool lhpOpDownloadVMERefactorTest::CheckMSFConsistency( mafVMERoot *treeRoot )
{
  mafVMERoot *reloadedVMERoot = treeRoot;
  CPPUNIT_ASSERT(reloadedVMERoot);

  cout << "MSF validation in progress...";

  mafOpValidateTree *opValidateTree = new mafOpValidateTree();
  opValidateTree->SetInput(reloadedVMERoot);
  int returnValue = opValidateTree->ValidateTree();

  cout << "Success!";

  CPPUNIT_ASSERT(returnValue == mafOpValidateTree::VALIDATE_SUCCESS);

  cppDEL(opValidateTree);

  return true;
}

void lhpOpDownloadVMERefactorTest::CheckVMEsExistenceAndTypeInTree( mafVMERoot* treeRoot, std::map<int, std::string> &vmeID_vmeType_Map )
{

  CPPUNIT_ASSERT(treeRoot != NULL);

  std::map<int, std::string>::iterator mapIterator;

  for (mapIterator = vmeID_vmeType_Map.begin(); mapIterator != vmeID_vmeType_Map.end(); mapIterator++)
  {
    int vmeID = mapIterator->first;
    std::string vmeType = mapIterator->second;
    
    mafNode *node = treeRoot->FindInTreeById(vmeID);
    CPPUNIT_ASSERT(node != NULL);
    CPPUNIT_ASSERT(node->IsA(vmeType.c_str()));
  }
}