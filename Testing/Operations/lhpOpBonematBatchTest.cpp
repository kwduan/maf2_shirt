/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: lhpOpBonematBatchTest.cpp,v $
Language:  C++
Date:      $Date: 2010-11-30 16:30:35 $
Version:   $Revision: 1.1.2.1 $
Authors:   Stefano Perticoni
==========================================================================
Copyright (c) 2002/2004 
CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/

#include "mafDefines.h" 
#include "lhpDefines.h"
#include <wx/file.h>

//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include <cppunit/config/SourcePrefix.h>
#include "lhpOpBonematBatchTest.h"

#include "medOpImporterVTK.h"
#include "lhpOpImporterAnsysCDBFile.h"
#include "lhpOpExporterAnsysInputFile.h"

#include "wx/wfstream.h"
#include "wx/tokenzr.h"
#include "wx/txtstrm.h"
#include "wx/init.h"


void lhpOpBonematBatchTest::setUp()
{
	
}

void lhpOpBonematBatchTest::tearDown()
{
	
}

void lhpOpBonematBatchTest::TestFixture()
{

}

void lhpOpBonematBatchTest::TestConstructorDestructor()
{
  lhpOpBonematBatch *op = new lhpOpBonematBatch("op bonemat batch");
  cppDEL(op);
}


void lhpOpBonematBatchTest::TestBonematBatchExecution()
{

	lhpOpBonematBatch *op = new lhpOpBonematBatch("op bonemat batch");


	::wxInitialize();

	wxString pythonExeFullPath = "c:/Python25/python.exe";
	CPPUNIT_ASSERT(wxFileExists(pythonExeFullPath.c_str()));

	op->SetPythonExeFullPath(pythonExeFullPath.c_str());

	wxString configurationFileName = LHP_DATA_ROOT;
	configurationFileName << "/lhpOpBonematBatchTest/configurationFileBonematBatch2Runs.txt";
	CPPUNIT_ASSERT(wxFileExists(configurationFileName));

	op->SetConfigurationFileName(configurationFileName.c_str());

	op->Execute();

	::wxUninitialize();

	cppDEL(op);

	wxString validatedOutput = LHP_DATA_ROOT;
	validatedOutput << "/lhpOpBonematBatchTest/ansysOutputToCheckAgainst.inp";
	CPPUNIT_ASSERT(wxFileExists(validatedOutput));

	wxString ansysOutputBatch1 = LHP_DATA_ROOT;
	ansysOutputBatch1 << "/lhpOpBonematBatchTest/ansysOutputBatch1.inp";
	CPPUNIT_ASSERT(wxFileExists(ansysOutputBatch1));

	wxString ansysOutputBatch2 = LHP_DATA_ROOT;
	ansysOutputBatch2 << "/lhpOpBonematBatchTest/ansysOutputBatch2.inp";
	CPPUNIT_ASSERT(wxFileExists(ansysOutputBatch2));

	TextFilesEquals(ansysOutputBatch1, validatedOutput);
	TextFilesEquals(ansysOutputBatch2, validatedOutput);
 }



void lhpOpBonematBatchTest::TestTextFilesEquals()
{

	wxString file1=LHP_DATA_ROOT;
	file1<<"/lhpOpBonematBatchTest/configurationFileBonematBatch5Runs.txt";

	wxString file2=LHP_DATA_ROOT;
	file2<<"/lhpOpBonematBatchTest/configurationFileBonematBatch5Runs.txt";

	assert(TextFilesEquals(file1 , file2));

	file1=LHP_DATA_ROOT;
	file1<<"/lhpOpBonematBatchTest/configurationFileBonematBatch5Runs.txt";

	file2=LHP_DATA_ROOT;
	file2<<"/lhpOpBonematBatchTest/configurationFileBonematBatch2Runs.txt";

	assert(TextFilesEquals(file1, file2) == false);
};

bool lhpOpBonematBatchTest::TextFilesEquals(wxString file1, wxString file2)
{
	wxFileInputStream f1( file1 );
	wxFileInputStream f2( file2 );

	wxTextInputStream text1( f1 );
	wxTextInputStream text2( f2 );
	wxString line1;
	wxString line2;

	while (!f1.Eof())
	{
	
		line1 = text1.ReadLine();
		line2 = text2.ReadLine();
		wxStringTokenizer tkz1(line1,wxT(' '),wxTOKEN_DEFAULT);
		wxStringTokenizer tkz2(line2,wxT(' '),wxTOKEN_DEFAULT);

		while (tkz1.HasMoreTokens())
		{
			mafString scalar1 = tkz1.GetNextToken();
			mafString scalar2 = tkz2.GetNextToken();

			if ( scalar1 == scalar2)
			{
				continue;
			}
			else
			{
				return false;
			}
		}
	}

	return true;
}
