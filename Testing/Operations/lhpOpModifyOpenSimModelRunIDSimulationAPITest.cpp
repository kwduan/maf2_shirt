/*=========================================================================

 Program: MAF2Medical
 Module: lhpOpModifyOpenSimModelRunIDSimulationAPITest
 Authors: Stefano Perticoni

 Copyright (c) B3C
 All rights reserved. See Copyright.txt or
 http://www.scsitaly.com/Copyright.htm for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "medDefines.h"
//----------------------------------------------------------------------------
// NOTE: Every CPP generatedFile in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "lhpOpModifyOpenSimModelRunIDSimulationAPITest.h"
#include <vnl/vnl_vector.h>

#include "mafString.h"
#include "medVMEAnalog.h"
#include "mafVMEOutputScalarMatrix.h"

#include <string>

#include <iostream>
#include "lhpOpImporterOpenSimIKSimulationResults.h"
#include "mafVMEFactory.h"
#include "mafVMEStorage.h"
#include "mafVMERoot.h"
#include "mafVMESurface.h"
#include "vtkDataSet.h"
#include "vtkFieldData.h"
#include "lhpDefines.h"
#include "wx/wfstream.h"
#include "wx/txtstrm.h"
#include "lhpOpModifyOpenSimModelRunIDSimulationAPI.h"

//-----------------------------------------------------------
void lhpOpModifyOpenSimModelRunIDSimulationAPITest::TestDynamicAllocation() 
//-----------------------------------------------------------
{
  lhpOpModifyOpenSimModelRunIDSimulationAPI *simulation = new lhpOpModifyOpenSimModelRunIDSimulationAPI("importer");
  cppDEL(simulation);
}
//-----------------------------------------------------------
void lhpOpModifyOpenSimModelRunIDSimulationAPITest::TestStaticAllocation() 
//-----------------------------------------------------------
{
  lhpOpModifyOpenSimModelRunIDSimulationAPI simulation; 
}

//-----------------------------------------------------------
void lhpOpModifyOpenSimModelRunIDSimulationAPITest::TestRunInverseDynamicsSimulation() 
//-----------------------------------------------------------
{
	mafString dirPrefix = LHP_DATA_ROOT;
	mafString testDataDir = dirPrefix;
	
	testDataDir.Append("/lhpOpModifyOpenSimModelRunIDSimulationAPITest/");

	CPPUNIT_ASSERT(wxDirExists(testDataDir));

	wxSetWorkingDirectory(testDataDir.GetCStr());

	wxString outpuSimulationIDFileName = testDataDir;
	outpuSimulationIDFileName.Append("Analysis1.sto");

	wxRemoveFile(outpuSimulationIDFileName);
	CPPUNIT_ASSERT(wxFileExists(outpuSimulationIDFileName) == false);

	mafString simulationSetupFileName = testDataDir;
	simulationSetupFileName.Append("setupInverseDynamics.xml");

	CPPUNIT_ASSERT(wxFileExists(simulationSetupFileName.GetCStr()) == true);

	wxString idLogFileName = "out.log";
	wxString idLogABSFileName = testDataDir; 
	idLogABSFileName.Append(idLogFileName);

	wxRemoveFile(idLogABSFileName);
	assert(wxFileExists(idLogABSFileName) == false);

	wxString idErrFileName = "err.log";
	wxString idErrABSFileName = testDataDir;
    idErrABSFileName.Append(idErrFileName);

	wxRemoveFile(idErrABSFileName);
	assert(wxFileExists(idErrABSFileName) == false);

	lhpOpModifyOpenSimModelRunIDSimulationAPI *simulation = new lhpOpModifyOpenSimModelRunIDSimulationAPI();
   	simulation->RunSimulation(simulationSetupFileName.GetCStr(), idLogABSFileName , idErrABSFileName , outpuSimulationIDFileName );	
	cppDEL(simulation);
	
	CPPUNIT_ASSERT(wxFileExists(outpuSimulationIDFileName) == true);

	wxString checkIKOutputFile = testDataDir;
	checkIKOutputFile.Append("Analysis1_TO_CHECK_AGAINST.sto");

	CPPUNIT_ASSERT(wxFileExists(checkIKOutputFile));

	std::ifstream   referenceFile(checkIKOutputFile.c_str());
	referenceFile.seekg(0, std::ios_base::end);
	size_t referenceFileSize = referenceFile.tellg();
	referenceFile.close();

	std::ifstream   generatedFile(outpuSimulationIDFileName.c_str());
	generatedFile.seekg(0, std::ios_base::end);
	size_t generatedFileSize = generatedFile.tellg();
	generatedFile.close();

	CPPUNIT_ASSERT(generatedFileSize == referenceFileSize);
}