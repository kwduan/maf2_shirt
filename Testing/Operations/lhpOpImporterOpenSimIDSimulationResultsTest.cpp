/*=========================================================================

 Program: MAF2Medical
 Module: lhpOpImporterOpenSimIDSimulationResultsTest
 Authors: Stefano Perticoni

 Copyright (c) B3C
 All rights reserved. See Copyright.txt or
 http://www.scsitaly.com/Copyright.htm for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "medDefines.h"
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "lhpOpImporterOpenSimIDSimulationResultsTest.h"
#include <vnl/vnl_vector.h>

#include "mafString.h"
#include "medVMEAnalog.h"
#include "mafVMEOutputScalarMatrix.h"

#include <string>

#include <iostream>
#include "lhpOpImporterOpenSimIDSimulationResults.h"
#include "mafVMEFactory.h"
#include "mafVMEStorage.h"
#include "mafVMERoot.h"
#include "mafVMESurface.h"
#include "vtkDataSet.h"
#include "vtkFieldData.h"
#include "lhpDefines.h"
#include "lhpOpImporterOpenSimIKSimulationResults.h"
#include "vtkMAFSmartPointer.h"
#include "vtkDataSetWriter.h"

//-----------------------------------------------------------
void lhpOpImporterOpenSimIDSimulationResultsTest::TestDynamicAllocation() 
//-----------------------------------------------------------
{
  lhpOpImporterOpenSimIDSimulationResults *importer = new lhpOpImporterOpenSimIDSimulationResults("importer");
  cppDEL(importer);
}
//-----------------------------------------------------------
void lhpOpImporterOpenSimIDSimulationResultsTest::TestStaticAllocation() 
//-----------------------------------------------------------
{
  lhpOpImporterOpenSimIDSimulationResults importer; 
}

//-----------------------------------------------------------
void lhpOpImporterOpenSimIDSimulationResultsTest::TestImportIDResults() 
//-----------------------------------------------------------
{
	//-----------------------
	// load the msf
	//-----------------------

	mafString dirPrefix = LHP_DATA_ROOT;
	mafString testModelFileName = dirPrefix;
	
	testModelFileName.Append("/lhpOpImporterOpenSimIDSimulationResultsTest/testModel/testModel.msf");

	CPPUNIT_ASSERT(wxFileExists(testModelFileName.GetCStr()));

	// in order to create VME from storage we need the factory to initialize 
	mafVMEFactory::Initialize();

	//create a new storage: this also creates a root
	mafVMEStorage storage2;
	storage2.SetURL(testModelFileName.GetCStr());

	mafVMERoot* inRoot;
	inRoot = NULL;

	inRoot = storage2.GetRoot();
	CPPUNIT_ASSERT(inRoot);

	// root register the storage so its reference count must be one 
	CPPUNIT_ASSERT(inRoot->GetReferenceCount() == 1);
	
	storage2.Restore();

	wxString vme1Name = "R_KNEE_in_child";
	wxString vme2Name = "R_HIP_in_child";

	mafNode *vme1 = inRoot->FindInTreeByName(vme1Name);
	mafVMESurface *vmeSurface1 = mafVMESurface::SafeDownCast(vme1);
	vmeSurface1->GetOutput()->GetVTKData()->Update();
	vtkDataSet *ds = vmeSurface1->GetOutput()->GetVTKData();
	vtkFieldData *fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 0);

	mafNode *vme2 = inRoot->FindInTreeByName(vme2Name);
	mafVMESurface *vmeSurface2 = mafVMESurface::SafeDownCast(vme2);
	vmeSurface2->GetOutput()->GetVTKData()->Update();
	ds = vmeSurface2->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 0);
	
	lhpOpImporterOpenSimIDSimulationResults *importer=new lhpOpImporterOpenSimIDSimulationResults("importer");
	importer->SetInput(inRoot);
	importer->TestModeOn();
	mafString outputIDFileName= dirPrefix;
	outputIDFileName.Append("/lhpOpImporterOpenSimIDSimulationResultsTest/InputID/InverseDynamics_Analysis1.sto");
	CPPUNIT_ASSERT(wxFileExists(outputIDFileName.GetCStr()));

	importer->SetFileName(outputIDFileName.GetCStr());
	importer->Import();
	mafDEL(importer);

	vmeSurface1->GetOutput()->GetVTKData()->Update();
	ds = vmeSurface1->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 2);

	double range[2];
	fd->GetArray(0)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 0.4 && range[1] == 1.0);

	fd->GetArray(1)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -21.175342040000000 && range[1] == 39.657880089999999);

	vmeSurface2->GetOutput()->GetVTKData()->Update();
	ds = vmeSurface2->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 7);

	fd->GetArray(0)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 0.4  && range[1] == 1.0);

	fd->GetArray(1)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -9.9720076399999993 && range[1] == 63.007997650000000);

	fd->GetArray(2)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -20.180416470000001 && range[1] == 91.023423330000000);

	fd->GetArray(6)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -8.5947841100000009 && range[1] == 55.708672210000003);
}

//-----------------------------------------------------------
void lhpOpImporterOpenSimIDSimulationResultsTest::TestImportIDResultsAndIKResults() 
//-----------------------------------------------------------
{
	//-----------------------
	// load the msf
	//-----------------------

	mafString dirPrefix = LHP_DATA_ROOT;
	mafString testModelFileName = dirPrefix;

	testModelFileName.Append("/lhpOpImporterOpenSimIDSimulationResultsTest/testModel/testModel.msf");

	CPPUNIT_ASSERT(wxFileExists(testModelFileName.GetCStr()));

	// in order to create VME from storage we need the factory to initialize 
	mafVMEFactory::Initialize();

	//create a new storage: this also creates a root
	mafVMEStorage storage2;
	storage2.SetURL(testModelFileName.GetCStr());

	mafVMERoot* inRoot;
	inRoot = NULL;

	inRoot = storage2.GetRoot();
	CPPUNIT_ASSERT(inRoot);

	// root register the storage so its reference count must be one 
	CPPUNIT_ASSERT(inRoot->GetReferenceCount() == 1);

	storage2.Restore();

	wxString vme1Name = "R_KNEE_in_child";
	wxString vme2Name = "R_HIP_in_child";

	mafNode *vme1 = inRoot->FindInTreeByName(vme1Name);
	mafVMESurface *vmeSurface1 = mafVMESurface::SafeDownCast(vme1);
	vmeSurface1->GetOutput()->GetVTKData()->Update();
	vtkDataSet *ds = vmeSurface1->GetOutput()->GetVTKData();
	vtkFieldData *fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 0);

	mafNode *vme2 = inRoot->FindInTreeByName(vme2Name);
	mafVMESurface *vmeSurface2 = mafVMESurface::SafeDownCast(vme2);
	vmeSurface2->GetOutput()->GetVTKData()->Update();
	ds = vmeSurface2->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 0);

	lhpOpImporterOpenSimIDSimulationResults *importer=new lhpOpImporterOpenSimIDSimulationResults("importer");
	importer->SetInput(inRoot);
	importer->TestModeOn();
	mafString outputIDFileName= dirPrefix;
	outputIDFileName.Append("/lhpOpImporterOpenSimIDSimulationResultsTest/InputID/InverseDynamics_Analysis1.sto");
	CPPUNIT_ASSERT(wxFileExists(outputIDFileName.GetCStr()));

	importer->SetFileName(outputIDFileName.GetCStr());
	importer->Import();
	mafDEL(importer);

	vmeSurface1->GetOutput()->GetVTKData()->Update();
	ds = vmeSurface1->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 2);

	double range[2];
	fd->GetArray(0)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 0.4 && range[1] == 1.0);

	fd->GetArray(1)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -21.175342040000000 && range[1] == 39.657880089999999);

	vmeSurface2->GetOutput()->GetVTKData()->Update();
	ds = vmeSurface2->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 7);

	fd->GetArray(0)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 0.4  && range[1] == 1.0);

	fd->GetArray(1)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -9.9720076399999993 && range[1] == 63.007997650000000);

	fd->GetArray(2)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -20.180416470000001 && range[1] == 91.023423330000000);

	fd->GetArray(6)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -8.5947841100000009 && range[1] == 55.708672210000003);

	///////////////////////////////////////////////////////////////////////

	testModelFileName = dirPrefix;

	testModelFileName.Append("/lhpOpImporterOpenSimIKSimulationResultsTest/testModel/testModel.msf");

	CPPUNIT_ASSERT(wxFileExists(testModelFileName.GetCStr()));

	lhpOpImporterOpenSimIKSimulationResults *ikImporter=new lhpOpImporterOpenSimIKSimulationResults("importer");
	ikImporter->SetInput(inRoot);
	ikImporter->TestModeOn();
	mafString outputIKFileName= dirPrefix;
	outputIKFileName.Append("/lhpOpImporterOpenSimIKSimulationResultsTest/OutputIK/output_ik.mot");
	CPPUNIT_ASSERT(wxFileExists(outputIKFileName.GetCStr()));

	ikImporter->SetFileName(outputIKFileName.GetCStr());
	ikImporter->Import();
	mafDEL(ikImporter);

	vmeSurface1->GetOutput()->GetVTKData()->Update();
	ds = vmeSurface1->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();

	mafString outputDataFileName= dirPrefix;
	outputDataFileName.Append("/lhpOpImporterOpenSimIKSimulationResultsTest/OutputIK/outputData.vtk");


	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 4);

	fd->GetArray(2)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 0.4 && range[1] == 1.165);

	fd->GetArray(3)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -63.854639530000000 && range[1] == -4.1234770599999999);

	vmeSurface2->GetOutput()->GetVTKData()->Update();
	ds = vmeSurface2->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 14);

	fd->GetArray(7)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 0.4 && range[1] == 1.165);

	fd->GetArray(8)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -93.038042439999998 && range[1] == -84.903906079999999);

	fd->GetArray(9)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -23.229515830000000 && range[1] == 20.568822489999999);

	fd->GetArray(10)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 89.760364019999997 && range[1] == 105.49038135000001);

	vtkMAFSmartPointer<vtkDataSetWriter> dsw;
	dsw->SetInput(ds);
	dsw->SetFileName(outputDataFileName.GetCStr());
	dsw->Write();
}

//-----------------------------------------------------------
void lhpOpImporterOpenSimIDSimulationResultsTest::TestImportMultipleIDResults() 
//-----------------------------------------------------------
{
	//-----------------------
	// load the msf
	//-----------------------

	mafString dirPrefix = LHP_DATA_ROOT;
	mafString testModelFileName = dirPrefix;

	testModelFileName.Append("/lhpOpImporterOpenSimIDSimulationResultsTest/testModel/testModel.msf");

	CPPUNIT_ASSERT(wxFileExists(testModelFileName.GetCStr()));

	// in order to create VME from storage we need the factory to initialize 
	mafVMEFactory::Initialize();

	//create a new storage: this also creates a root
	mafVMEStorage storage2;
	storage2.SetURL(testModelFileName.GetCStr());

	mafVMERoot* inRoot;
	inRoot = NULL;

	inRoot = storage2.GetRoot();
	CPPUNIT_ASSERT(inRoot);

	// root register the storage so its reference count must be one 
	CPPUNIT_ASSERT(inRoot->GetReferenceCount() == 1);

	storage2.Restore();

	wxString vme1Name = "R_KNEE_in_child";
	wxString vme2Name = "R_HIP_in_child";

	mafNode *vme1 = inRoot->FindInTreeByName(vme1Name);
	mafVMESurface *vmeSurface1 = mafVMESurface::SafeDownCast(vme1);
	vmeSurface1->GetOutput()->GetVTKData()->Update();
	vtkDataSet *ds = vmeSurface1->GetOutput()->GetVTKData();
	vtkFieldData *fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 0);

	mafNode *vme2 = inRoot->FindInTreeByName(vme2Name);
	mafVMESurface *vmeSurface2 = mafVMESurface::SafeDownCast(vme2);
	vmeSurface2->GetOutput()->GetVTKData()->Update();
	ds = vmeSurface2->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 0);

	lhpOpImporterOpenSimIDSimulationResults *importer=new lhpOpImporterOpenSimIDSimulationResults("importer");
	importer->SetInput(inRoot);
	importer->TestModeOn();
	mafString outputIDFileName= dirPrefix;
	outputIDFileName.Append("/lhpOpImporterOpenSimIDSimulationResultsTest/InputID/InverseDynamics_Analysis_0.sto");
	CPPUNIT_ASSERT(wxFileExists(outputIDFileName.GetCStr()));

	importer->SetFileName(outputIDFileName.GetCStr());
	importer->Import();
	mafDEL(importer);

	vmeSurface1->GetOutput()->GetVTKData()->Update();
	ds = vmeSurface1->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 4);

	double range[2];
	fd->GetArray(0)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 0.4 && range[1] == 1.0);

	fd->GetArray(1)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -21.175342040000000 && range[1] == 39.657880089999999);

	vmeSurface2->GetOutput()->GetVTKData()->Update();
	ds = vmeSurface2->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 19);

	fd->GetArray(0)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 0.4  && range[1] == 1.0);

	fd->GetArray(1)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -9.9720076399999993 && range[1] == 63.007997650000000);

	fd->GetArray(2)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -20.180416470000001 && range[1] == 91.023423330000000);

	fd->GetArray(6)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == -8.5947841100000009 && range[1] == 55.708672210000003);

	vtkMAFSmartPointer<vtkDataSetWriter> dsw;
	dsw->SetInput(vmeSurface2->GetOutput()->GetVTKData());
	mafString outputVTKFileName = dirPrefix;
	outputVTKFileName.Append("/lhpOpImporterOpenSimIDSimulationResultsTest/InputID/InverseDynamicsMultiple.vtk");
	dsw->SetFileName(outputVTKFileName.GetCStr());
	dsw->Write();
}