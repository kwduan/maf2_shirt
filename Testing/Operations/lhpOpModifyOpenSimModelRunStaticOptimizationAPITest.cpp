/*=========================================================================

 Program: MAF2Medical
 Module: lhpOpModifyOpenSimModelRunStaticOptimizationAPITest
 Authors: Stefano Perticoni

 Copyright (c) B3C
 All rights reserved. See Copyright.txt or
 http://www.scsitaly.com/Copyright.htm for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "medDefines.h"
//----------------------------------------------------------------------------
// NOTE: Every CPP generatedFile in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "lhpOpModifyOpenSimModelRunStaticOptimizationAPITest.h"
#include "lhpOpModifyOpenSimModelRunStaticOptimizationAPI.h"
#include <vnl/vnl_vector.h>

#include "mafString.h"
#include "medVMEAnalog.h"
#include "mafVMEOutputScalarMatrix.h"

#include <string>

#include <iostream>
#include "mafVMEFactory.h"
#include "mafVMEStorage.h"
#include "mafVMERoot.h"
#include "mafVMESurface.h"
#include "vtkDataSet.h"
#include "vtkFieldData.h"
#include "lhpDefines.h"
#include "wx/wfstream.h"
#include "wx/txtstrm.h"

//-----------------------------------------------------------
void lhpOpModifyOpenSimModelRunStaticOptimizationAPITest::TestDynamicAllocation() 
//-----------------------------------------------------------
{
  lhpOpModifyOpenSimModelRunStaticOptimizationAPI *simulation = new lhpOpModifyOpenSimModelRunStaticOptimizationAPI("importer");
  cppDEL(simulation);
}
//-----------------------------------------------------------
void lhpOpModifyOpenSimModelRunStaticOptimizationAPITest::TestStaticAllocation() 
//-----------------------------------------------------------
{
  lhpOpModifyOpenSimModelRunStaticOptimizationAPI simulation; 
}

//-----------------------------------------------------------
void lhpOpModifyOpenSimModelRunStaticOptimizationAPITest::TestRunStaticOptimization() 
//-----------------------------------------------------------
{
	wxString dirPrefix = LHP_DATA_ROOT;
	wxString testDataDir = dirPrefix;
	
	testDataDir.Append("/lhpOpModifyOpenSimModelRunStaticOptimizationAPITest/");
	CPPUNIT_ASSERT(wxDirExists(testDataDir));

	wxString logFileName = "out.log";
	wxString logABSFileName = testDataDir + logFileName;
	wxRemoveFile(logABSFileName);
	assert(wxFileExists(logABSFileName) == false);

	wxString errFileName = "err.log";
	wxString errABSFileName = testDataDir + errFileName;
	wxRemoveFile(errABSFileName);
	assert(wxFileExists(errABSFileName) == false);

	wxString outputStaticOptimizationControlsAbsFileName = testDataDir + "Output_StaticOptimization_controls.xml";
	wxRemoveFile(outputStaticOptimizationControlsAbsFileName);
	assert(wxFileExists(outputStaticOptimizationControlsAbsFileName) == false);

	wxString outputStaticOptimizationForceAbsFileName = testDataDir + "Output_StaticOptimization_force.sto";
	wxRemoveFile(outputStaticOptimizationForceAbsFileName);
	assert(wxFileExists(outputStaticOptimizationForceAbsFileName) == false);

	wxString outputStaticOptimizationActivationAbsFileName = testDataDir + "Output_StaticOptimization_activation.sto";
	wxRemoveFile(outputStaticOptimizationActivationAbsFileName);
	assert(wxFileExists(outputStaticOptimizationActivationAbsFileName) == false);

	wxString simulationSetupFileABSFileName = testDataDir;
	simulationSetupFileABSFileName.Append("setupStaticOptimization.xml");
	assert(wxFileExists(simulationSetupFileABSFileName) == true);

	lhpOpModifyOpenSimModelRunStaticOptimizationAPI *simulation = new lhpOpModifyOpenSimModelRunStaticOptimizationAPI();
   	simulation->RunStaticOptimization(simulationSetupFileABSFileName, 
		logABSFileName , 
		errABSFileName , 
		outputStaticOptimizationControlsAbsFileName , 
		outputStaticOptimizationForceAbsFileName,
		outputStaticOptimizationActivationAbsFileName );	

	cppDEL(simulation);
	
	CPPUNIT_ASSERT(wxFileExists(outputStaticOptimizationControlsAbsFileName) == true);
	CPPUNIT_ASSERT(wxFileExists(outputStaticOptimizationForceAbsFileName) == true);
	CPPUNIT_ASSERT(wxFileExists(outputStaticOptimizationActivationAbsFileName) == true);

}