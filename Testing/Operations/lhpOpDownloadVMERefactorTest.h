/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: lhpOpDownloadVMERefactorTest.h,v $
Language:  C++
Date:      $Date: 2009-11-10 15:25:31 $
Version:   $Revision: 1.1.2.5 $
Authors:   Stefano Perticoni
==========================================================================
Copyright (c) 2002/2004 
CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/

#ifndef __CPP_UNIT_lhpOpBonematTest_H__
#define __CPP_UNIT_lhpOpBonematTest_H__

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

class mafVMERoot;

#include "lhpOpDownloadVMERefactor.h"

/** 
DEBUG/DEVELOPMENT:
to debug this component along with the ThreadedUploaderDownloader server (ie Upload/Download manager)
run ThreadedUploaderDownloader.py in Eclipse in Debug mode on port 50000

REFACTOR THIS: we need to put lhpOpDownloadVMERefactorTest in automatic regression (Parabuild) 
on both development and production biomedtown as soon as possible. Prerequisites for this:

1: configuration file for webservices
extract all webservices uri/stuff in order to confine
devel/production ws different URIs to a single file. In this way both devel and production 
tests can be run by loading only the different configuration file

2: production server speed enhancement
At the present time this test cannot be run in production since it overloads the server
*/
class lhpOpDownloadVMERefactorTest : public CPPUNIT_NS::TestFixture
{
  public: 
    // CPPUNIT fixture: executed before each test
    void setUp();

    // CPPUNIT fixture: executed after each test
    void tearDown();

    CPPUNIT_TEST_SUITE(lhpOpDownloadVMERefactorTest);	
    CPPUNIT_TEST(TestConstructorDestructor);
    CPPUNIT_TEST(TestDownload_msfWith1SmallMeshAnd1SmallVolume_dataresource);
    CPPUNIT_TEST(TestDownload_test_volume_AND_test_mesh_tetra10_dataresources);
    CPPUNIT_TEST(TestDownload_msf_test_import_export_vme_Dataresource);
    CPPUNIT_TEST_SUITE_END();

  protected:  
  
    void TestConstructorDestructor();   
    void TestDownload_test_volume_AND_test_mesh_tetra10_dataresources();
    void TestDownload_msfWith1SmallMeshAnd1SmallVolume_dataresource();
    void TestDownload_msf_test_import_export_vme_Dataresource();

    void Download( mafString inBasketListFileName, mafString inTargetMSFForDownload , mafString outMSFContainingDownloadedVMEs);

    bool CheckMSFConsistency( mafVMERoot *treeRoot);

    void CheckVMEsExistenceAndTypeInTree( mafVMERoot* treeRoot, std::map<int, std::string> &vmeID_vmeType_Map );
   
    mafString m_PythonExe;
    mafString m_PythonwExe;
};
      
int
main( int argc, char* argv[] )
{
  // Create the event manager and test controller
  CPPUNIT_NS::TestResult controller;

  // Add a listener that colllects test result
  CPPUNIT_NS::TestResultCollector result;
  controller.addListener( &result );        

  // Add a listener that print dots as test run.
  CPPUNIT_NS::BriefTestProgressListener progress;
  controller.addListener( &progress );      

  // Add the top suite to the test runner
  CPPUNIT_NS::TestRunner runner;
  runner.addTest( lhpOpDownloadVMERefactorTest::suite());
  runner.run( controller );

  // Print test in a compiler compatible format.
  CPPUNIT_NS::CompilerOutputter outputter( &result, CPPUNIT_NS::stdCOut() );
  outputter.write();  


  return result.wasSuccessful() ? 0 : 1;
}
#endif
