/*=========================================================================

 Program: MAF2Medical
 Module: lhpOpImporterOpenSimSOSimulationResultsTest
 Authors: Stefano Perticoni

 Copyright (c) B3C
 All rights reserved. See Copyright.txt or
 http://www.scsitaly.com/Copyright.htm for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "medDefines.h"
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "lhpOpImporterOpenSimSOSimulationResultsTest.h"
#include <vnl/vnl_vector.h>

#include "mafString.h"
#include "medVMEAnalog.h"
#include "mafVMEOutputScalarMatrix.h"

#include <string>

#include <iostream>
#include "lhpOpImporterOpenSimSOSimulationResults.h"
#include "mafVMEFactory.h"
#include "mafVMEStorage.h"
#include "mafVMERoot.h"
#include "mafVMESurface.h"
#include "vtkDataSet.h"
#include "vtkFieldData.h"
#include "lhpDefines.h"
#include "lhpOpImporterOpenSimIKSimulationResults.h"
#include "vtkMAFSmartPointer.h"
#include "vtkDataSetWriter.h"
#include "medVMEComputeWrapping.H"
#include "mafVMEPolyline.h"

//-----------------------------------------------------------
void lhpOpImporterOpenSimSOSimulationResultsTest::TestDynamicAllocation() 
//-----------------------------------------------------------
{
  lhpOpImporterOpenSimSOSimulationResults *importer = new lhpOpImporterOpenSimSOSimulationResults("importer");
  cppDEL(importer);
}
//-----------------------------------------------------------
void lhpOpImporterOpenSimSOSimulationResultsTest::TestStaticAllocation() 
//-----------------------------------------------------------
{
  lhpOpImporterOpenSimSOSimulationResults importer; 
}

//-----------------------------------------------------------
void lhpOpImporterOpenSimSOSimulationResultsTest::TestImportSOResults() 
//-----------------------------------------------------------
{
	//-----------------------
	// load the msf
	//-----------------------

	mafPlugNode<medVMEComputeWrapping>("Generalized another VME Meter with wrapping geometry");

	mafString dirPrefix = LHP_DATA_ROOT;
	mafString testModelFileName = dirPrefix;
	
	testModelFileName.Append("/lhpOpImporterOpenSimSOSimulationResultsTest/testModel/Demo4.msf");

	CPPUNIT_ASSERT(wxFileExists(testModelFileName.GetCStr()));

	// in order to create VME from storage we need the factory to initialize 
	mafVMEFactory::Initialize();

	//create a new storage: this also creates a root
	mafVMEStorage storage2;
	storage2.SetURL(testModelFileName.GetCStr());

	mafVMERoot* inRoot;
	inRoot = NULL;

	inRoot = storage2.GetRoot();
	CPPUNIT_ASSERT(inRoot);

	// root register the storage so its reference count must be one 
	CPPUNIT_ASSERT(inRoot->GetReferenceCount() == 1);
	
	storage2.Restore();

	wxString vme1Name = "glut_max1_r";
	wxString vme2Name = "glut_max3_r";

	mafNode *vme1 = inRoot->FindInTreeByName(vme1Name);
	mafVMEPolyline *vmeMuscle1 = mafVMEPolyline::SafeDownCast(vme1);
	vmeMuscle1->GetOutput()->GetVTKData()->Update();
	vtkDataSet *ds = vmeMuscle1->GetOutput()->GetVTKData();
	vtkFieldData *fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 0);

	mafNode *vme2 = inRoot->FindInTreeByName(vme2Name);
	mafVMEPolyline*vmeMuscle2 = mafVMEPolyline::SafeDownCast(vme2);
	vmeMuscle2->GetOutput()->GetVTKData()->Update();
	ds = vmeMuscle2->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 0);
	
	lhpOpImporterOpenSimSOSimulationResults *importer=new lhpOpImporterOpenSimSOSimulationResults("importer");
	importer->SetInput(inRoot);
	importer->TestModeOn();
	mafString outputIDFileName= dirPrefix;
	outputIDFileName.Append("/lhpOpImporterOpenSimSOSimulationResultsTest/InputSO/StaticOptimization_force.sto");
	CPPUNIT_ASSERT(wxFileExists(outputIDFileName.GetCStr()));

	importer->SetFileName(outputIDFileName.GetCStr());
	importer->Import();
	mafDEL(importer);

	mafString outputDataFileName= dirPrefix;
	outputDataFileName.Append("/lhpOpImporterOpenSimSOSimulationResultsTest/OutputSO/outputData.vtk");

	vtkMAFSmartPointer<vtkDataSetWriter> dsw;
	dsw->SetInput(ds);
	dsw->SetFileName(outputDataFileName.GetCStr());
	dsw->Write();

	vmeMuscle1->GetOutput()->GetVTKData()->Update();
	ds = vmeMuscle1->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 2);

	double range[2];
	fd->GetArray(0)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 0.0 && range[1] == 1.1650000000000000);

	fd->GetArray(1)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 10.215787760000000 && range[1] == 150.61384810999999);

	vmeMuscle2->GetOutput()->GetVTKData()->Update();
	ds = vmeMuscle2->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 2);

	fd->GetArray(0)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 0.0  && range[1] == 1.1650000000000000);

	fd->GetArray(1)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 11.068961699999999 && range[1] == 49.997995809999999);

}

//-----------------------------------------------------------
void lhpOpImporterOpenSimSOSimulationResultsTest::TestImportMultipleSOResults() 
//-----------------------------------------------------------
{
	//-----------------------
	// load the msf
	//-----------------------

	mafPlugNode<medVMEComputeWrapping>("Generalized another VME Meter with wrapping geometry");

	mafString dirPrefix = LHP_DATA_ROOT;
	mafString testModelFileName = dirPrefix;

	testModelFileName.Append("/lhpOpImporterOpenSimSOSimulationResultsTest/testModel/Demo4.msf");

	CPPUNIT_ASSERT(wxFileExists(testModelFileName.GetCStr()));

	// in order to create VME from storage we need the factory to initialize 
	mafVMEFactory::Initialize();

	//create a new storage: this also creates a root
	mafVMEStorage storage2;
	storage2.SetURL(testModelFileName.GetCStr());

	mafVMERoot* inRoot;
	inRoot = NULL;

	inRoot = storage2.GetRoot();
	CPPUNIT_ASSERT(inRoot);

	// root register the storage so its reference count must be one 
	CPPUNIT_ASSERT(inRoot->GetReferenceCount() == 1);

	storage2.Restore();

	wxString vme1Name = "glut_max1_r";
	wxString vme2Name = "glut_max3_r";

	mafNode *vme1 = inRoot->FindInTreeByName(vme1Name);
	mafVMEPolyline *vmeMuscle1 = mafVMEPolyline::SafeDownCast(vme1);
	vmeMuscle1->GetOutput()->GetVTKData()->Update();
	vtkDataSet *ds = vmeMuscle1->GetOutput()->GetVTKData();
	vtkFieldData *fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 0);

	mafNode *vme2 = inRoot->FindInTreeByName(vme2Name);
	mafVMEPolyline*vmeMuscle2 = mafVMEPolyline::SafeDownCast(vme2);
	vmeMuscle2->GetOutput()->GetVTKData()->Update();
	ds = vmeMuscle2->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 0);

	lhpOpImporterOpenSimSOSimulationResults *importer=new lhpOpImporterOpenSimSOSimulationResults("importer");
	importer->SetInput(inRoot);
	importer->TestModeOn();
	mafString outputIDFileName= dirPrefix;
	outputIDFileName.Append("/lhpOpImporterOpenSimSOSimulationResultsTest/InputSO/StaticOptimization_force_0.sto");
	CPPUNIT_ASSERT(wxFileExists(outputIDFileName.GetCStr()));

	importer->SetFileName(outputIDFileName.GetCStr());
	importer->Import();
	mafDEL(importer);

	mafString outputDataFileName= dirPrefix;
	outputDataFileName.Append("/lhpOpImporterOpenSimSOSimulationResultsTest/OutputSO/outputDataMultiple1.vtk");

	vtkMAFSmartPointer<vtkDataSetWriter> dsw;
	dsw->SetInput(ds);
	dsw->SetFileName(outputDataFileName.GetCStr());
	dsw->Write();

	vmeMuscle1->GetOutput()->GetVTKData()->Update();
	ds = vmeMuscle1->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 4);

	double range[2];
	fd->GetArray(0)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 0.0 && range[1] == 1.1650000000000000);

	fd->GetArray(1)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 10.215787760000000 && range[1] == 150.61384810999999);

	vmeMuscle2->GetOutput()->GetVTKData()->Update();
	ds = vmeMuscle2->GetOutput()->GetVTKData();
	fd = ds->GetFieldData();
	CPPUNIT_ASSERT(fd->GetNumberOfArrays() == 4);

	fd->GetArray(0)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 0.0  && range[1] == 1.1650000000000000);

	fd->GetArray(1)->GetRange(range);
	CPPUNIT_ASSERT(range[0] == 11.068961699999999 && range[1] == 49.997995809999999);

	mafString outputDataFileName2= dirPrefix;
	outputDataFileName2.Append("/lhpOpImporterOpenSimSOSimulationResultsTest/OutputSO/outputDataMultiple2.vtk");

	dsw->SetInput(ds);
	dsw->SetFileName(outputDataFileName2.GetCStr());
	dsw->Write();

}



