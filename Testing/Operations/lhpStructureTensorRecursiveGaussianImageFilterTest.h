/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: lhpStructureTensorRecursiveGaussianImageFilterTest.h,v $
Language:  C++
Date:      $Date: 2011-10-20 14:50:13 $
Version:   $Revision: 1.1.2.2 $
Authors:   Stefano Perticoni
==========================================================================
Copyright (c) 2002/2004 
CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/

#ifndef __CPP_UNIT_lhpOpBonematTest_H__
#define __CPP_UNIT_lhpOpBonematTest_H__

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>

#include "lhpOpBonemat.h"

class lhpOpImporterC3D;

class itkStructureTensorRecursiveGaussianImageFilterTest : public CPPUNIT_NS::TestFixture
{
  public:
    // CPPUNIT fixture: executed before each test
    void setUp();

    // CPPUNIT fixture: executed after each test
    void tearDown();

    CPPUNIT_TEST_SUITE(itkStructureTensorRecursiveGaussianImageFilterTest);	    

	CPPUNIT_TEST(TestCylinderSynthetic);

	CPPUNIT_TEST(TestPrimitiveObjectsITKInput);

	CPPUNIT_TEST(TestVTKToITKToVTK);
    
	CPPUNIT_TEST(TestTrabeculiGrassiITKInput);
	CPPUNIT_TEST(TestTrabeculiGrassiVTKInput);

    CPPUNIT_TEST_SUITE_END();

  protected:
	
	void itkStructureTensorRecursiveGaussianImageFilterTestNew();
    void itkStructureTensorRecursiveGaussianImageFilterTestOld();
    
	void TestCylinderSynthetic();
    void TestPrimitiveObjectsITKInput();
    void TestVTKToITKToVTK();
	void TestTrabeculiGrassiITKInput();
	void TestTrabeculiGrassiVTKInput();

	void itkStructureTensorRecursiveGaussianImageFilterTestITKInput( std::string &inputImageAbsPath, std::string &primaryEigenVectorOutputImageAbsPath, std::string &primaryEigenValueOutputImageAbsPath );	
	void itkStructureTensorRecursiveGaussianImageFilterTestVTKInput( std::string &inputImageAbsPath, std::string &primaryEigenVectorOutputImageAbsPath, std::string &primaryEigenValueOutputImageAbsPath );

private:
};


int
main( int argc, char* argv[] )
{
  // Create the event manager and test controller
  CPPUNIT_NS::TestResult controller;

  // Add a listener that colllects test result
  CPPUNIT_NS::TestResultCollector result;
  controller.addListener( &result );        

  // Add a listener that print dots as test run.
  CPPUNIT_NS::BriefTestProgressListener progress;
  controller.addListener( &progress );      

  // Add the top suite to the test runner
  CPPUNIT_NS::TestRunner runner;
  runner.addTest( itkStructureTensorRecursiveGaussianImageFilterTest::suite());
  runner.run( controller );

  // Print test in a compiler compatible format.
  CPPUNIT_NS::CompilerOutputter outputter( &result, CPPUNIT_NS::stdCOut() );
  outputter.write(); 

  return result.wasSuccessful() ? 0 : 1;
}
#endif
