/*=========================================================================

 Program: MAF2Medical
 Module: lhpOpModifyOpenSimModelRunIKSimulationAPITest
 Authors: Stefano Perticoni

 Copyright (c) B3C
 All rights reserved. See Copyright.txt or
 http://www.scsitaly.com/Copyright.htm for details.

 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "medDefines.h"
//----------------------------------------------------------------------------
// NOTE: Every CPP generatedFile in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "lhpOpModifyOpenSimModelRunIKSimulationAPITest.h"
#include <vnl/vnl_vector.h>

#include "mafString.h"
#include "medVMEAnalog.h"
#include "mafVMEOutputScalarMatrix.h"

#include <string>

#include <iostream>
#include "lhpOpImporterOpenSimIKSimulationResults.h"
#include "mafVMEFactory.h"
#include "mafVMEStorage.h"
#include "mafVMERoot.h"
#include "mafVMESurface.h"
#include "vtkDataSet.h"
#include "vtkFieldData.h"
#include "lhpDefines.h"
#include "lhpOpModifyOpenSimModelRunIKSimulationAPI.h"
#include "wx/wfstream.h"
#include "wx/txtstrm.h"

//-----------------------------------------------------------
void lhpOpModifyOpenSimModelRunIKSimulationAPITest::TestDynamicAllocation() 
//-----------------------------------------------------------
{
  lhpOpModifyOpenSimModelRunIKSimulationAPI *simulation = new lhpOpModifyOpenSimModelRunIKSimulationAPI("importer");
  cppDEL(simulation);
}
//-----------------------------------------------------------
void lhpOpModifyOpenSimModelRunIKSimulationAPITest::TestStaticAllocation() 
//-----------------------------------------------------------
{
  lhpOpModifyOpenSimModelRunIKSimulationAPI simulation; 
}

//-----------------------------------------------------------
void lhpOpModifyOpenSimModelRunIKSimulationAPITest::TestRunIkSimulation() 
//-----------------------------------------------------------
{
	mafString dirPrefix = LHP_DATA_ROOT;
	mafString testDataDir = dirPrefix;
	
	testDataDir.Append("/lhpOpModifyOpenSimModelRunIKSimulationAPITest/");

	CPPUNIT_ASSERT(wxDirExists(testDataDir));

	wxString outpuSimulationIKFileName = testDataDir;
	outpuSimulationIKFileName.Append("subject01_walk1_ik.mot");

	wxRemoveFile(outpuSimulationIKFileName);
	CPPUNIT_ASSERT(wxFileExists(outpuSimulationIKFileName) == false);

	mafString simulationSetupFileName = testDataDir;
	simulationSetupFileName.Append("subject01_Setup_IK.xml");

	lhpOpModifyOpenSimModelRunIKSimulationAPI *simulation = new lhpOpModifyOpenSimModelRunIKSimulationAPI();
   	simulation->RunSimulation(simulationSetupFileName.GetCStr() , testDataDir.GetCStr() , "out.log" , "err.log" , outpuSimulationIKFileName );	
	cppDEL(simulation);
	
	CPPUNIT_ASSERT(wxFileExists(outpuSimulationIKFileName) == true);

	wxString checkIKOutputFile = testDataDir;
	checkIKOutputFile.Append("subject01_walk1_ik _TO_CHECK_AGAINST.mot");

	CPPUNIT_ASSERT(wxFileExists(checkIKOutputFile));

	std::ifstream   referenceFile(checkIKOutputFile.c_str());
	referenceFile.seekg(0, std::ios_base::end);
	size_t referenceFileSize = referenceFile.tellg();
	referenceFile.close();

	std::ifstream   generatedFile(outpuSimulationIKFileName.c_str());
	generatedFile.seekg(0, std::ios_base::end);
	size_t generatedFileSize = generatedFile.tellg();
	generatedFile.close();

	CPPUNIT_ASSERT(generatedFileSize == referenceFileSize);
}