# Create a file to prove that lxml python library exists in current machine:

supportedVersion = "2.2.4"

try:
    import lxml

    # Version control is not provided by this library :(
    #version = lxml.version()
    line1 = "lxml " + str(supportedVersion) + " found :-)" 
    
    print line1
    
    file = open("lxmlVersionCorrect.txt","w")
    file.write(line1)
    file.close() 
    pass
    
except:
    line1 =  "Warning : lxml " + str(supportedVersion) + " not found!"
    line2 =  "Several features will not  work without lxml"

    print line1
    print line2

    file = open("lxmlNotFound.txt","w")
    file.write(line1)
    file.write(line2)    
    file.close() 