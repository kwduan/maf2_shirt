# Create a file to prove that python exists in current machine:
# file name is used by the NSIS installer
supportedVersion  = "2.8.10.1 (msw-unicode)"

try:
  import wx
      # check wxPython version
  version = wx.version()
  
  if(version != supportedVersion):
    line1 =  "Warning : wxPython Version must be " + supportedVersion     
    line2 =  "Other versions may not work. Found Version: "  + str(version)
    
    print line1
    print line2
    
    file = open("wxPythonVersionWrong.txt","w")
    file.write(line1)
    file.write(line2)    
    file.close() 
      
  else:
    line1 = "wxPython " + str(supportedVersion) + " found :-)" 
    
    print line1
    
    file = open("wxPythonVersionCorrect.txt","w")
    file.write(line1)
    file.close() 
    pass

except:  

  line1 =  "Warning : wxPython " + str(supportedVersion) + " not found!"
  line2 =  "Several features will not  work without wxPython"
  
  print line1
  print line2
    
  file = open("wxPythonNotFound.txt","w")
  file.write(line1)
  file.write(line2)    
  file.close() 

  