import base64
from datetime import datetime
from xmlrpclib import ServerProxy


### PARAMETERS FROM COMMAND LINE
date_of_birth = '1950-1-1'


### WORKFLOW MANAGER CONNECTOR
wfmng = ServerProxy('http://wfmng.vphop.eu/')

### LOGIN TO RETRIEVE SESSION TICKET
username = 'mbalasso'
password = base64.decodestring('YmFuemFp')

ret = wfmng.login(username, password)
ticket = ret['sessionTicket'].replace('sessionTicket=', '').replace("\"",'')

### GET SUBJECT NEW IDENTIFIER
ret = wfmng.getOpenClinicaMaxSubjectId(ticket)
new_subject_id = ret[0]
print new_subject_id

### CREATE NEW SUBJECT

data = {
    'subjectId': new_subject_id,
    'personId': '',
    'secondaryId': 'MXL_%s' % new_subject_id,
    'sex': 'm',
    'dateEnroll': str(datetime.now().date()),
    'dateBirth': date_of_birth
}

ret = wfmng.insertOpenClinicaSubject(ticket, data)

print ret

### GET OPENCLINICA EVENTS
events = wfmng.getOpenClinicaStudyEvents(new_subject_id, ticket)
event = [e for e in events if e['name'].count('MXL')].pop()

### SCHEDULE EVENT
data = {
    'subjectId': new_subject_id,
    'crfId': event['crf_id'],
    'location': 'Bologna',
    'startDate': str(datetime.now().date()),
    'endDate': '',
}

wfmng.setOpenClinicaScheduleEvent(ticket, data)
