# Create a file to report python version
# file name is used by the NSIS installer
import sys
installedVersion = sys.version[:5]
  
requiredVersion  = "2.5.4"  

if(installedVersion != requiredVersion):
    line1 =  "Warning : Python Version must be " + requiredVersion     
    line2 =  "Other versions may not work. Found Version: "  + str(installedVersion)

    print line1
    print line2

    file = open("PythonVersionWrong.txt","w")
    file.write(line1)
    file.write(line2)    
    file.close() 
      
else:
    line1 = "Python " + str(requiredVersion) + " found :-)" 

    print line1

    file = open("PythonVersionCorrect.txt","w")
    file.write(line1)
    file.close() 
    pass

  