REM -----------------------------------------------------------------------------
REM CMake scripts 
REM -----------------------------------------------------------------------------

IF "%1" == "APP_VS2010_RELEASE_SLIM" GOTO APP_VS2010_RELEASE_SLIM
IF "%1" == "APP_VS2010_RELEASE_SLIM_OPENSIM3" GOTO APP_VS2010_RELEASE_SLIM_OPENSIM3
IF "%1" == "APP_VS2010_RELEASE_FAT_OPENSIM3" GOTO APP_VS2010_RELEASE_FAT_OPENSIM3
IF "%1" == "APP_VS2010_RELEASE_FAT" GOTO APP_VS2010_RELEASE_FAT
IF "%1" == "APP_DLL_VS2010_RELEASE_SLIM" GOTO APP_DLL_VS2010_RELEASE_SLIM
IF "%1" == "DOXYGEN_DOCUMENTATION" GOTO DOXYGEN_DOCUMENTATION

REM -----------------------------------------------------------------------------
:APP_VS2010_RELEASE_SLIM
REM -----------------------------------------------------------------------------

mkdir Build
cd Build

cmake.exe  ../Source ^
  -G"Visual Studio 10" ^
  -DBUILD_TESTING:BOOL=ON ^
  -DBUILD_DOCUMENTATION:BOOL=OFF ^
  -DBUILD_FAT_INSTALLER:BOOL=OFF ^
  -DCPPUNIT_INCLUDE_DIR:PATH="C:/cppunit-1.12.0_VS2010_BUILD/cppunit-1.12.0/include" ^
  -DCPPUNIT_LIBRARY:FILEPATH="C:/cppunit-1.12.0_VS2010_BUILD/cppunit-1.12.0/lib/cppunit.lib" ^
  -DMAF_BINARY_PATH:PATH=d:/MAF2Libs/VS2010R/Build/ ^
  -DMED_BINARY_PATH:PATH=d:/MAF2MedicalLibs/VS2010R/Build/ ^
  -DLHPBUILDER_EXECUTABLE_OUTPUT_PATH:PATH="%WORKSPACE%/Build/bin/Release" ^
  -DLHPBUILDER_INSTALLER_EXECUTABLE_OUTPUT_DIR:PATH="%WORKSPACE%/Build/bin/Deploy" ^
  -DIPOSE_EXECUTABLE_OUTPUT_PATH:PATH="%WORKSPACE%/Build/bin/Release" ^
  -DVISUAL_STUDIO_RUNTIME="%VS_REDISTRIBUTABLE_FOLDER%\vcredist_x86.exe" ^
  -DIPOSE_INSTALLER_EXECUTABLE_OUTPUT_DIR:PATH="%WORKSPACE%/Build/bin/Deploy"

cd ..   

GOTO END

REM -----------------------------------------------------------------------------
:APP_VS2010_RELEASE_SLIM_OPENSIM3
REM -----------------------------------------------------------------------------

mkdir Build
cd Build

cmake.exe  ../Source ^
  -G"Visual Studio 10" ^
  -DUSE_OPENSIM_API:BOOL=ON ^
  -DUSE_MATIO_API:BOOL=ON ^
  -DMATIO_HEADERS_DIR:PATH="C:/MATIO_VS_2010/src/" ^
  -DMATIO_LIB:PATH="C:/MATIO_VS_2010/Release/matio.lib" ^
  -DBUILD_NMSBUILDER:BOOL=ON ^
  -DBUILD_LHPBUILDER:BOOL=OFF ^
  -DBUILD_PSLOADER:BOOL=OFF ^
  -DBUILD_PSLOADERPLUS:BOOL=OFF ^
  -DBUILD_BUILDER:BOOL=OFF ^
  -DBUILD_BONEMAT:BOOL=OFF ^
  -DBUILD_IPOSE:BOOL=OFF ^
  -DBUILD_TESTING:BOOL=ON ^
  -DBUILD_DOCUMENTATION:BOOL=OFF ^
  -DBUILD_FAT_INSTALLER:BOOL=OFF ^
  -DCPPUNIT_INCLUDE_DIR:PATH="C:/cppunit-1.12.0_VS2010_BUILD/cppunit-1.12.0/include" ^
  -DCPPUNIT_LIBRARY:FILEPATH="C:/cppunit-1.12.0_VS2010_BUILD/cppunit-1.12.0/lib/cppunit.lib" ^
  -DMAF_BINARY_PATH:PATH=d:/MAF2Libs/VS2010R/Build/ ^
  -DMED_BINARY_PATH:PATH=d:/MAF2MedicalLibs/VS2010R/Build/ ^
  -DLHPBUILDER_EXECUTABLE_OUTPUT_PATH:PATH="%WORKSPACE%/Build/bin/Release" ^
  -DLHPBUILDER_INSTALLER_EXECUTABLE_OUTPUT_DIR:PATH="%WORKSPACE%/Build/bin/Deploy" ^
  -DVISUAL_STUDIO_RUNTIME="%VS_REDISTRIBUTABLE_FOLDER%\vcredist_x86.exe"
  
cd ..   

GOTO END

REM -----------------------------------------------------------------------------
:APP_VS2010_RELEASE_FAT_OPENSIM3
REM -----------------------------------------------------------------------------

mkdir Build
cd Build

cmake.exe  ../Source ^
  -G"Visual Studio 10" ^
  -DUSE_OPENSIM_API:BOOL=ON ^
  -DUSE_MATIO_API:BOOL=ON ^
  -DMATIO_HEADERS_DIR:PATH="C:/MATIO_VS_2010/src/" ^
  -DMATIO_LIB:PATH="C:/MATIO_VS_2010/Release/matio.lib" ^
  -DBUILD_NMSBUILDER:BOOL=ON ^
  -DBUILD_LHPBUILDER:BOOL=OFF ^
  -DBUILD_PSLOADER:BOOL=OFF ^
  -DBUILD_PSLOADERPLUS:BOOL=OFF ^
  -DBUILD_BUILDER:BOOL=OFF ^
  -DBUILD_BONEMAT:BOOL=OFF ^
  -DBUILD_IPOSE:BOOL=OFF ^
  -DBUILD_TESTING:BOOL=ON ^
  -DBUILD_DOCUMENTATION:BOOL=OFF ^
  -DBUILD_FAT_INSTALLER:BOOL=ON ^
  -DCPPUNIT_INCLUDE_DIR:PATH="C:/cppunit-1.12.0_VS2010_BUILD/cppunit-1.12.0/include" ^
  -DCPPUNIT_LIBRARY:FILEPATH="C:/cppunit-1.12.0_VS2010_BUILD/cppunit-1.12.0/lib/cppunit.lib" ^
  -DMAF_BINARY_PATH:PATH=d:/MAF2Libs/VS2010R/Build/ ^
  -DMED_BINARY_PATH:PATH=d:/MAF2MedicalLibs/VS2010R/Build/ ^
  -DLHPBUILDER_EXECUTABLE_OUTPUT_PATH:PATH="%WORKSPACE%/Build/bin/Release" ^
  -DLHPBUILDER_INSTALLER_EXECUTABLE_OUTPUT_DIR:PATH="%WORKSPACE%/Build/bin/Deploy" ^
  -DVISUAL_STUDIO_RUNTIME="%VS_REDISTRIBUTABLE_FOLDER%\vcredist_x86.exe"
  
cd ..   

GOTO END


REM -----------------------------------------------------------------------------
:APP_VS2010_RELEASE_FAT
REM -----------------------------------------------------------------------------

mkdir Build
cd Build

cmake.exe  ../Source ^
  -G"Visual Studio 10" ^
  -DBUILD_TESTING:BOOL=OFF ^
  -DBUILD_DOCUMENTATION:BOOL=OFF ^
  -DBUILD_FAT_INSTALLER:BOOL=ON ^
  -DMAF_BINARY_PATH:PATH=d:/MAF2Libs/VS2010R/Build/ ^
  -DMED_BINARY_PATH:PATH=d:/MAF2MedicalLibs/VS2010R/Build/ ^
  -DLHPBUILDER_EXECUTABLE_OUTPUT_PATH:PATH="%WORKSPACE%/Build/bin/Release" ^
  -DLHPBUILDER_INSTALLER_EXECUTABLE_OUTPUT_DIR:PATH="%WORKSPACE%/Build/bin/Deploy" ^
  -DIPOSE_EXECUTABLE_OUTPUT_PATH:PATH="%WORKSPACE%/Build/bin/Release" ^
  -DVISUAL_STUDIO_RUNTIME="%VS_REDISTRIBUTABLE_FOLDER%\vcredist_x86.exe" ^
  -DIPOSE_INSTALLER_EXECUTABLE_OUTPUT_DIR:PATH="%WORKSPACE%/Build/bin/Deploy"

cd ..   

GOTO END

REM -----------------------------------------------------------------------------
:APP_DLL_VS2010_RELEASE_SLIM
REM -----------------------------------------------------------------------------

mkdir Build
cd Build

cmake.exe  ../Source ^
  -G"Visual Studio 10" ^
  -DLHP_BUILD_DLL:BOOL=ON ^
  -DBUILD_TESTING:BOOL=ON ^
  -DBUILD_DOCUMENTATION:BOOL=OFF ^
  -DBUILD_FAT_INSTALLER:BOOL=OFF ^
  -DCPPUNIT_INCLUDE_DIR:PATH="C:/cppunit-1.12.0_VS2010_BUILD/cppunit-1.12.0/include" ^
  -DCPPUNIT_LIBRARY:FILEPATH="C:/cppunit-1.12.0_VS2010_BUILD/cppunit-1.12.0/lib/cppunit.lib" ^
  -DMAF_BINARY_PATH:PATH=d:/MAF2Libs/VS2010R_DLL/Build/ ^
  -DMED_BINARY_PATH:PATH=d:/MAF2MedicalLibs/VS2010R_DLL/Build/ ^
  -DLHPBUILDER_EXECUTABLE_OUTPUT_PATH:PATH="%WORKSPACE%/Build/bin/Release" ^
  -DLHPBUILDER_INSTALLER_EXECUTABLE_OUTPUT_DIR:PATH="%WORKSPACE%/Build/bin/Deploy" ^
  -DIPOSE_EXECUTABLE_OUTPUT_PATH:PATH="%WORKSPACE%/Build/bin/Release" ^
  -DVISUAL_STUDIO_RUNTIME="%VS_REDISTRIBUTABLE_FOLDER%\vcredist_x86.exe" ^
  -DIPOSE_INSTALLER_EXECUTABLE_OUTPUT_DIR:PATH="%WORKSPACE%/Build/bin/Deploy"
  
cd ..

GOTO END

REM -----------------------------------------------------------------------------
:DOXYGEN_DOCUMENTATION
REM -----------------------------------------------------------------------------

mkdir Build
cd Build

cmake.exe  ../Source  -G"Visual Studio 10" ^
  -DMAF_BINARY_PATH:PATH=d:\MAF2Libs\Doxygen\Build\ ^
  -DMED_BINARY_PATH:PATH=d:\MAF2MedicalLibs\Doxygen\Build\ ^
  -DBUILD_DOCUMENTATION:BOOL=ON ^
  -DBUILD_TESTING:BOOL=OFF ^
  -DBUILD_EXAMPLES:BOOL=OFF ^
  -DDOXYGEN_DOT_EXECUTABLE="D:/Graphviz 2.28/bin/dot.exe" ^
  -DDOXYGEN_DOT_PATH="D:/Graphviz 2.28/bin" ^
  -DDOXYGEN_EXECUTABLE="D:/doxygen/bin/doxygen.exe"

cd ..
  
GOTO END

REM -----------------------------------------------------------------------------
:END
REM -----------------------------------------------------------------------------