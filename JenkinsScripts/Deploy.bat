REM -----------------------------------------------------------------------------
REM	NSIS Installer deploy scripts
REM -----------------------------------------------------------------------------

IF "%1" == "ALL_LHP_APPS" GOTO ALL_LHP_APPS
IF "%1" == "NMS_BUILDER" GOTO NMS_BUILDER
IF "%1" == "BUILDERM2O" GOTO BUILDERM2O

REM -----------------------------------------------------------------------------
:ALL_LHP_APPS
REM -----------------------------------------------------------------------------

	REM remove the deploy directory and create it again
	rmdir /s /q  %WORKSPACE%\Build\bin\Deploy
	mkdir %WORKSPACE%\Build\bin\Deploy

	cd Build
	cd bin
	cd Release

	"%ProgramFiles%\NSIS\makensis.exe" /V4 LHPBuilderInstallerScript.nsi

	"%ProgramFiles%\NSIS\makensis.exe" /V4 psLoaderInstallerScript.nsi

	"%ProgramFiles%\NSIS\makensis.exe" /V4 psLoaderPlusInstallerScript.nsi

	"%ProgramFiles%\NSIS\makensis.exe" /V4 BuilderInstallerScript.nsi

	"%ProgramFiles%\NSIS\makensis.exe" /V4 BonematInstallerScript.nsi

	"%ProgramFiles%\NSIS\makensis.exe" /V4 NMSBuilderInstallerScript.nsi

	"%ProgramFiles%\NSIS\makensis.exe" /V4 iposeInstallerScript.nsi

        "%ProgramFiles%\NSIS\makensis.exe" /V4 BuilderM2OInstallerScript.nsi

	cd ..
	cd ..
	cd ..

GOTO END

REM -----------------------------------------------------------------------------
:NMS_BUILDER
REM -----------------------------------------------------------------------------

	REM remove the deploy directory and create it again
	rmdir /s /q  %WORKSPACE%\Build\bin\Deploy
	mkdir %WORKSPACE%\Build\bin\Deploy

	cd Build
	cd bin
	cd Release

	"%ProgramFiles%\NSIS\makensis.exe" /V4 NMSBuilderInstallerScript.nsi

	cd ..
	cd ..
	cd ..

GOTO END

REM ------------------------------Added by Kewei Duan 13/05/2014-----------------
:BUILDERM2O
REM -----------------------------------------------------------------------------

	REM remove the deploy directory and create it again
	rmdir /s /q   %WORKSPACE%\BuilderM2O-Build\bin\Deploy
	mkdir %WORKSPACE%\BuilderM2O-Build\bin\Deploy

	cd %WORKSPACE%\BuilderM2O-Build\bin\Deploy

	"%ProgramFiles%\NSIS\makensis.exe" /V4 %WORKSPACE%\BuilderM2O-Build\bin\Release\BuilderM2OInstallerScript.nsi

	cd ..
	cd ..

GOTO END

REM -----------------------------------------------------------------------------
:END
REM -----------------------------------------------------------------------------
