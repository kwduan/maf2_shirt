REM -----------------------------------------------------------------------------
REM Build scripts
REM -----------------------------------------------------------------------------

IF "%1" == "APP_VS2010_RELEASE" GOTO APP_VS2010_RELEASE
IF "%1" == "BUILDERM2O" GOTO BUILDERM2O
IF "%1" == "BUILDERM2O_SCS04" GOTO BUILDERM2O_SCS04


:APP_VS2010_RELEASE


cd Build
CALL "%PROGRAMFILES%/Microsoft Visual Studio 10.0/Common7/Tools/vsvars32.bat"
devenv LHP.sln /project ALL_BUILD.vcxproj /build release /out build_log.txt

cd ..

GOTO END

REM -----------Added by Kewei Duan 20/05/2014------------------------------------
:BUILDERM2O
REM -----------------------------------------------------------------------------
cd %WORKSPACE%\BuilderM2O-Build
set PATH=C:\Program Files (x86)\CMake 2.8\bin;%PATH%
CALL "%PROGRAMFILES(x86)%/Microsoft Visual Studio 10.0/VC/vcvarsall.bat"
cmake %WORKSPACE%\BuilderM2O-Source -G "Visual Studio 10" -DCMAKE_BUILD_TYPE="Release" -DMAF_BINARY_PATH:PATH=C:/Develop/Projects/Builder-workspace/MAF2-build -DMED_BINARY_PATH:PATH=C:/Develop/Projects/Builder-workspace/MAF2-MED-build -DBUILD_TESTING=OFF -DBUILD_PSLOADER=OFF -DBUILD_LHPBUILDER=OFF -DBUILD_PSLOADERPLUS=OFF -DBUILD_BUILDER=OFF -DBUILD_BONEMAT=OFF -DBUILD_NMSBUILDER=OFF -DBUILD_IPOSE=OFF -DBUILD_BUILDERM2O=ON -DUSE_OPENSIM_API=OFF -DUSE_MATIO_API=OFF -DVISUAL_STUDIO_RUNTIME="%WORKSPACE%\BuilderM2O-Source\Installer\vcredist_x86.exe" -DBUILD_FAT_INSTALLER="ON"
MSBuild ALL_BUILD.vcxproj /property:Configuration=Release


GOTO END

REM -----------Added by Kewei Duan 20/05/2014------------------------------------
:BUILDERM2O_SCS04
REM -----------------------------------------------------------------------------

mkdir %WORKSPACE%\BuilderM2O-Build
cd BuilderM2O-Build

CALL "%PROGRAMFILES%/Microsoft Visual Studio 10.0/Common7/Tools/vsvars32.bat"
cmake %WORKSPACE%\BuilderM2O-Source -G "Visual Studio 10" -DCMAKE_BUILD_TYPE="Release" -DMAF_BINARY_PATH:PATH=d:/MAF2Libs/VS2010R/Build/ -DMED_BINARY_PATH:PATH=d:/MAF2MedicalLibs/VS2010R/Build/ -DBUILD_TESTING=OFF -DBUILD_PSLOADER=OFF -DBUILD_LHPBUILDER=OFF -DBUILD_PSLOADERPLUS=OFF -DBUILD_BUILDER=OFF -DBUILD_BONEMAT=OFF -DBUILD_NMSBUILDER=OFF -DBUILD_IPOSE=OFF -DBUILD_BUILDERM2O=ON -DUSE_OPENSIM_API=OFF -DUSE_MATIO_API=OFF -DVISUAL_STUDIO_RUNTIME="%WORKSPACE%\BuilderM2O-Source\Installer\vcredist_x86.exe" -DBUILD_FAT_INSTALLER="ON"
devenv LHP.sln /project ALL_BUILD.vcxproj /build release /out build_log.txt


GOTO END

REM -----------------------------------------------------------------------------
:END
REM -----------------------------------------------------------------------------


