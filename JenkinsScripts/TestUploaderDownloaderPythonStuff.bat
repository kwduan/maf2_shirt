REM -----------------------------------------------------------------------------
REM VMEUploaderDownloader tests driver script
REM -----------------------------------------------------------------------------

copy "%WORKSPACE%\Build\VMEUploaderDownloaderRefactor\CTestTestfile.cmake" ^
     "%WORKSPACE%\Source\VMEUploaderDownloaderRefactor\CTestTestfile.cmake"
			
copy "%WORKSPACE%\Build\VMEUploaderDownloaderRefactor\webServicesClient\CTestTestfile.cmake" ^
     "%WORKSPACE%\Source\VMEUploaderDownloaderRefactor\webServicesClient\CTestTestfile.cmake"
	 
cd .\Source\VMEUploaderDownloaderRefactor
ctest.exe
cd ..\..

REM -----------------------------------------------------------------------------
