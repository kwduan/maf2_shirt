import wx
import wx.aui
import CustomGaugeWx
import Queue , time
import os, sys
import wx.lib.scrolledpanel as scrolled
import images
from Debug import Debug

# GAUGE
NOT_PULSE = 0
PULSE = 1

USE_CUSTOMTREECTRL = False
ALLOW_AUI_FLOATING = False
DEFAULT_PERSPECTIVE = "Default Perspective"

class MyTaskBarIcon(wx.TaskBarIcon):
    TBMENU_RESTORE = wx.NewId()
    TBMENU_CLOSE   = wx.NewId()
    TBMENU_CHANGE  = wx.NewId()
    TBMENU_REMOVE  = wx.NewId()
    
    def __init__(self, frame):
        wx.TaskBarIcon.__init__(self)
        self.frame = frame
        # Set the image
        icon = wx.Icon('VerticalApplicationIcon.ico', wx.BITMAP_TYPE_ICO, 16, 16)
        self.SetIcon(icon, "VMEUploaderDownloader")
        self.imgidx = 1
        
        # bind some events
        self.Bind(wx.EVT_TASKBAR_LEFT_DCLICK, self.OnTaskBarActivate)
        self.Bind(wx.EVT_MENU, self.OnTaskBarActivate, id=self.TBMENU_RESTORE)
        self.Bind(wx.EVT_MENU, self.OnTaskBarClose, id=self.TBMENU_CLOSE)
        self.Bind(wx.EVT_MENU, self.OnTaskBarChange, id=self.TBMENU_CHANGE)
        self.Bind(wx.EVT_MENU, self.OnTaskBarRemove, id=self.TBMENU_REMOVE)

    def CreatePopupMenu(self):
        menu = wx.Menu()
        menu.Append(self.TBMENU_RESTORE, "Restore VMEUploaderDownloader")
        menu.Append(self.TBMENU_CLOSE,   "Close VMEUploaderDownloader")
        return menu


    def MakeIcon(self, img):
        if "wxMSW" in wx.PlatformInfo:
            img = img.Scale(16, 16)
        elif "wxGTK" in wx.PlatformInfo:
            img = img.Scale(22, 22)
        icon = wx.IconFromBitmap(img.ConvertToBitmap() )
        return icon
    

    def OnTaskBarActivate(self, evt):
        if self.frame.IsIconized():
            self.frame.Iconize(False)
        if not self.frame.IsShown():
            self.frame.Show(True)
        self.frame.Raise()

    def OnTaskBarClose(self, evt):
        wx.CallAfter(self.frame.Close)

    def OnTaskBarChange(self, evt):            
        pass
    
    def OnTaskBarRemove(self, evt):
        self.RemoveIcon()


class MyLog(wx.PyLog):
    def __init__(self, textCtrl, logTime=0):
        wx.PyLog.__init__(self)
        self.tc = textCtrl
        self.logTime = logTime

    def DoLogString(self, message, timeStamp):
        if self.tc:
            self.tc.AppendText(message + '\n')

"""Uploader Downloader GUI built using WX"""
class FileTransferFrame(wx.Frame):
    def __init__(self, master, queue, endCommand):        
        wx.Frame.__init__(self, None, -1, "My Title", size = (400, 335), \
                          style=wx.DEFAULT_FRAME_STYLE)
        
        self.master = master
        self.queue = queue 
        self.quitApplication = endCommand
        
        pnl = wx.Panel(self)
        self.pnl = pnl
        self.mgr = wx.aui.AuiManager()
        self.mgr.SetManagedWindow(pnl)
        
        self.BuildMenuBar()
        self.finddata = wx.FindReplaceData()
        self.finddata.SetFlags(wx.FR_DOWN)

        icon = wx.Icon('VerticalApplicationIcon.ico', wx.BITMAP_TYPE_ICO, 16, 16)
        self.SetIcon(icon)
            
        self.SetTitle("Upload Download")
        
        if "gtk2" in wx.PlatformInfo:
            self.ovr.SetStandardFonts()

        # Set up a log window
        self.log = wx.TextCtrl(pnl, -1,
                              style = wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        
        wx.Log_SetActiveTarget(MyLog(self.log))

        if wx.Platform == "__WXMAC__":
            self.log.MacCheckSpelling(False)

   #     self.Bind(wx.EVT_ACTIVATE, self.OnActivate)
        # wx.GetApp().Bind(wx.EVT_ACTIVATE_APP, self.OnAppActivate)

        # add the windows to the splitter and split it.
        leftBox = wx.BoxSizer(wx.VERTICAL)

        self.scrolledPanel = scrolled.ScrolledPanel(pnl, -1, size=(140, 300),
                                 style = wx.TAB_TRAVERSAL|wx.SUNKEN_BORDER, name="panel1" )
        
        # Use the aui manager to set up everything
        self.mgr.AddPane(self.scrolledPanel, wx.aui.AuiPaneInfo().CenterPane().Name("Notebook"))
        self.mgr.AddPane(self.log,
                         wx.aui.AuiPaneInfo().
                         Bottom().BestSize((-1, 200)).
                         MinSize((-1, 100)).
                         Floatable(ALLOW_AUI_FLOATING).FloatingSize((500, 200)).
                         Caption("Log Messages").
                         CloseButton(True).
                         Name("LogWindow"))

        self.mgr.Update()
        self.mgr.SetFlags(self.mgr.GetFlags() ^ wx.aui.AUI_MGR_TRANSPARENT_DRAG)
                    
        self.verticalBoxSizer = wx.BoxSizer(wx.VERTICAL)
        
        self.bars = []
        self.endingBars = []
        self.staticLabel = wx.StaticText(self.scrolledPanel, -1,  '', (150,15), (200,25))
        self.staticLine = wx.StaticLine(self.scrolledPanel, -1, (25,35), (360,2))
        
        self.verticalBoxSizer.Add(self.staticLabel, 0,  wx.ALIGN_CENTER_HORIZONTAL)
        self.verticalBoxSizer.Add(self.staticLine, 0 , wx.ALIGN_CENTER_HORIZONTAL)
    
        self.scrolledPanel.SetSizer( self.verticalBoxSizer )
    
        self.CreateStatusBar()
        self.uploadNumber = 0
        self.downloadNumber = 0
        
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        
        #self.EVT_RESULT(self.OnCreateBar) # Old event disconnected
        self.EVT_RESULT(self.OnCreateSingleBar)
        self.Show()
        
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.processIncoming)
        self.timeToCall = 100
        self.timer.Start(milliseconds=self.timeToCall, oneShot=True)
        self.barCreated = False
        self.ModalityGauge = "UPLOAD"
        self.ListProcessedId = list()
        
        try:
            self.tbicon = MyTaskBarIcon(self)
            
        except:
            self.tbicon = None

        self.numOfTransferredVME = 0
        self.numOfVMEToBeTransferred = 0
        wx.LogMessage("Log area ready...")
           
    def EVT_RESULT(self,func):
        #self.Connect(-1,-1,10,func)
        self.Connect(-1,-1,10,func)

    def BuildMenuBar(self):

        # Make a File menu
        self.mainmenu = wx.MenuBar()
        menu = wx.Menu()
        
        exitItem = wx.MenuItem(menu, -1, 'E&xit\t', 'Close the application')
        exitItem.SetBitmap(images.catalog['exit'].getBitmap())
        menu.AppendItem(exitItem)
        self.Bind(wx.EVT_MENU, self.OnFileExit, exitItem)
        wx.App.SetMacExitMenuItemId(exitItem.GetId())
        self.mainmenu.Append(menu, '&File')
        self.SetMenuBar(self.mainmenu)

    def OnFileExit(self, *event):
        self.Close()

    def OnCreateBar(self, event):
        self.__createBar(event.VmeID)
    
    def OnCreateSingleBar(self, event):
        #print ">> Event trapped by OnCreateSingleBar"
        self.__createSingleBar()
        
    def OnClose(self, event):
        answer = wx.MessageBox("Are you sure you want to quit the Upload/Download Manager?", "Confirm", wx.STAY_ON_TOP | wx.OK | wx.CANCEL )
        if (answer == wx.OK):
            self.quitApplication()
            
            self.dying = True
            self.mainmenu = None
            if self.tbicon is not None:
                self.tbicon.Destroy()

            self.Destroy()
            sys.exit()
            
    def __createSingleBar(self):
        self.bars.append(CustomGaugeWx.CustomGaugeWx(self.scrolledPanel,0,100,(160, 50),(self.staticLine.GetSize()[0]/2.0, 25),gaugeModality = self.ModalityGauge ,gaugePulse = NOT_PULSE))
        self.verticalBoxSizer.Add(self.bars[0],flag=wx.CENTER)
        self.scrolledPanel.SetupScrolling(scrollToTop = False)
        self.Refresh()
        self.barCreated = True
        #print ">>Bar created"
        if(self.timer.IsRunning() == False): self.timer.Start(milliseconds=self.timeToCall, oneShot=True)
        pass
            
    def __createBar(self,vmeID):
            # avoid duplicates (bug #1869 fix)
        if(vmeID in self.ListProcessedId):
            self.barCreated = True
            pass
        else:
            self.bars.append(CustomGaugeWx.CustomGaugeWx(self.scrolledPanel, len(self.bars),\
            100, (160, (len(self.bars)+1) * 50),\
            (self.staticLine.GetSize()[0]/2.0, 25),\
            gaugeModality = self.ModalityGauge ,gaugePulse = NOT_PULSE))
            
            self.verticalBoxSizer.Add(self.bars[len(self.bars)-1],flag=wx.CENTER)
            
            # maintain vertical scrollbar position after refresh
            self.scrolledPanel.SetupScrolling(scrollToTop = False)
            self.Refresh()
            
            if(vmeID!= -2):
                self.ListProcessedId.append(vmeID) #(bug #1869 fix)
            self.barCreated = True
            if(self.timer.IsRunning() == False): self.timer.Start(milliseconds=self.timeToCall, oneShot=True)
            pass
            
    def createSingleBar(self, modality):
        self.ModalityGauge = modality
        #event = wx.PyEvent(10)
        #event.SetEventType(10)
        self.barCreated = False
        #wx.PostEvent(self, event)
        self.__createSingleBar()
        #print ">> Event sent... waiting for bar creation"
        #while(self.barCreated == False):
        #    pass # Wait for bar creation
        #print ">> Loop end..."
        
    def createFileTransferBar(self, modality,id = -2):
        self.ModalityGauge = modality
        event = wx.PyEvent(10)
        event.SetEventType(10)
        event.VmeID = id
        self.barCreated = False
        wx.PostEvent(self, event)
        count = 0

        while(self.barCreated == False):
            count = count + 1
            # Sometimes wx.Python don't trap the event and the process wait untill another thread try to create a prgress bar, so:
            if(id != -2):
                if (count >= 5000): # avoid deadlocks (bug #1869 fix) (To do: find a better way to check this)
                    return 0
        return 1 # (bug #1869 fix)
    
    def setSingleBarTitle(self, title):
        self.bars[0].SetTitle(title)
        pass
    
    def setTransferredVMEName(self , title):  
        self.bars[len(self.bars)-1].SetTitle(title)
        pass
        
    def __del__(self):
        self.quitApplication()
        wx.Frame.__del__(self)

    def clearVMETransferStatus(self):
        self.numOfVMEToBeTransferred = 0
        self.numOfTransferredVME = 0
        
    def processIncoming(self, args = None):
        """
        Handle all the messages currently in the queue (if any).
        """

        # ToDo: 
        # - Improve download progress!
        # - Log download process!
        
        # Changing progress definition:
        # only one bar for all VMEs
        # progress is numOfTransferredVME/numOfVMEToBeTransferred * 100 (+ increment)
        # No elapsed time but numOfTransferredVME on numOfVMEToBeTransferred
        
        progress = 0
                
        #if(self.numOfVMEToBeTransferred == 0):
        if os.path.exists("lhpCommunication.txt") == True:
            while 1:
                try:
                    commFile = open("lhpCommunication.txt","r")
                    for line in commFile:
                        self.numOfVMEToBeTransferred = int(line)
                    commFile.close()
                    #os.remove("lhpCommunication.txt")
                    break
                except:
                    pass
        else:
            pass
            #self.numOfVMEToBeTransferred = 0 // THIS MUST BE RESTORED
        
        #try:
        #    os.remove("_lhpGuiEnd.lhp")
        #except:
        #    pass
        if(self.numOfTransferredVME == self.numOfVMEToBeTransferred):
            self.numOfTransferredVME = 0
            self.numOfVMEToBeTransferred = 0
            #ctrlFile = open("_lhpGuiEnd.lhp","w")
            #ctrlFile.close()
        
        self.timer.Stop()
        increment = 0
        while self.queue.qsize():
           
            try:
                gauge_FileTransferStatus_RemainingTimeInSeconds_Progress = self.queue.get(0)
                
                #gauge_FileTransferStatus_RemainingTimeInSeconds_Progress is composed by observer (the gauge) and a value
                #if value is -1 or gauge as option gaugepulse, is pulsing
                #else set the value of the progress
                                
                fileTransferBox = gauge_FileTransferStatus_RemainingTimeInSeconds_Progress[0]
                fileTransferStatus = gauge_FileTransferStatus_RemainingTimeInSeconds_Progress[1]
                
                ##################
                #### UPLOAD
                ##################
                
                ##### SRB
                
                if (fileTransferStatus == 2480):
                    fileTransferBox.SetEndingLabel('Getting resource... (WSCreateResource)')
                    self.queue.task_done(0)
                
                elif (fileTransferStatus == 2496):
                    fileTransferBox.SetEndingLabel('Successful get resource. (WSCreateResource)')

                    self.queue.task_done(0)
                
                elif (fileTransferStatus == 2468):
                    fileTransferBox.SetEndingLabel('Failed get resource! (WSCreateResource)')
                    wx.LogMessage('Failed get free SRB resource!')
                    self.queue.task_done(0)
                
                
                ##### MTOM
                
                elif (fileTransferStatus == 107):
                    fileTransferBox.SetEndingLabel('Upload (WSUpload)...')
                    self.queue.task_done(0)
                
                elif (fileTransferStatus == 3210):
                    
                    increment = round(float(gauge_FileTransferStatus_RemainingTimeInSeconds_Progress[3]) / float(numOfVMEToBeTransferred))
                    remainingTime = \
                    round(float(gauge_FileTransferStatus_RemainingTimeInSeconds_Progress[2]))
                    
                    hms = self.SecondsToHMS(remainingTime)
                    hmsString = str(int(hms[0])) + "h:" + str(int(hms[1])) + "m:" +\
                    str(int(hms[2])) + "s"
                    fileTransferBox.SetEndingLabel('Upload in progress... Please wait.')
                    self.queue.task_done(0)
               
                elif (fileTransferStatus == 108):
                    fileTransferBox.SetEndingLabel('Failed Upload! (WSUpload - Error while sending binary file)')
                    wx.LogMessage('Failed MTOM Upload! (WSUpload - Error while sending binary file)')
                    self.numOfTransferredVME = self.numOfTransferredVME + 1 # put here a roll back mechanism
                    self.queue.task_done(0)

                elif (fileTransferStatus == 109):
                    fileTransferBox.SetEndingLabel('Successful Upload. (WSUpload - Binary sent)')
                    
                    increment = round(float(50) / float(self.numOfVMEToBeTransferred))
                    self.queue.task_done(0)
                                
                ##### MD5
                                
                elif (fileTransferStatus == 105):
                    fileTransferBox.SetEndingLabel('MD5 checksum control...')
                    self.queue.task_done(0)                

                elif (fileTransferStatus == 2000):
                    fileTransferBox.SetEndingLabel('Successful MD5 checksum control.')
                    self.queue.task_done(0)
                    increment = round(float(70) / float(self.numOfVMEToBeTransferred))
                    
                elif (fileTransferStatus == 1111):
                    fileTransferBox.SetEndingLabel('Failed MD5 checksum check!')
                    wx.LogMessage('Failed MD5 checksum check!')
                    self.numOfTransferredVME = self.numOfTransferredVME + 1 # put here a roll back mechanism
                    self.queue.task_done(0)
                    
                ##### XML
                    
                elif (fileTransferStatus == 10000):
                    
                    increment = round(float(70) / float(self.numOfVMEToBeTransferred))
                    fileTransferBox.SetEndingLabel('Upload resource (xml)... (WSUpload)')
                    self.queue.task_done(0)
                
                elif(fileTransferStatus == 110):
                  
                    increment = 0
                    self.numOfTransferredVME = self.numOfTransferredVME + 1 #increments the number of uploaded VME
                    fileTransferBox.SetEndingLabel('Completed.')
                    self.queue.task_done(0)
                    
                    
                    
                
                elif (fileTransferStatus == 106):
                    fileTransferBox.SetEndingLabel('Failed upload resource (xml)! (WSUpload)')
                    wx.LogMessage('Failed upload resource (xml)! (WSUpload)')
                    self.queue.task_done(0)
                    self.numOfTransferredVME = self.numOfTransferredVME + 1 # put here a roll back mechanism

                ##################
                #### DOWNLOAD
                ##################
                
                ##### MTOM Download
                
                # TODO CHECK DOWNLOAD!!!
                
                elif (fileTransferStatus == 91988):
                    
                    #increment = round(float(gauge_FileTransferStatus_RemainingTimeInSeconds_Progress[3])/ float(self.numOfVMEToBeTransferred))
                    remainingTime = \
                    round(float(gauge_FileTransferStatus_RemainingTimeInSeconds_Progress[2]))
                    hms = self.SecondsToHMS(remainingTime)
                    hmsString = str(int(hms[0])) + "h:" + str(int(hms[1])) + "m:" +\
                    str(int(hms[2])) + "s"
                    fileTransferBox.SetEndingLabel('Download in progress... Please wait.')
                    
                    self.queue.task_done(0)
               
                elif (fileTransferStatus == 99986):
                    #increment = 0
                    fileTransferBox.SetEndingLabel('Successful Download (completed!)')
                    self.numOfTransferredVME = self.numOfTransferredVME + 1
                    self.queue.task_done(0)
                                
                elif (fileTransferStatus == 49984):
                    fileTransferBox.SetEndingLabel('Successful XML Download , no binary data associated (completed!)')
                    wx.LogMessage('Successful XML Download , no binary data associated (completed!)')
                    #progress = 0
                    self.numOfTransferredVME = self.numOfTransferredVME + 1
                    self.queue.task_done(0)
                
                elif (fileTransferStatus == 39876):
                    
                    fileTransferBox.SetEndingLabel('Download Checksum Check Failed!')
                    wx.LogMessage('Download Checksum Check Failed!')
                    self.numOfTransferredVME = self.numOfTransferredVME + 1
                    self.queue.task_done(0)
                
                elif (fileTransferStatus == 120):
                    
                    fileTransferBox.SetEndingLabel('Generic Error.')
                    fileTransferBox.SetEndingLabel('Generic Error.')
                    self.numOfTransferredVME = self.numOfTransferredVME + 1
                    self.queue.task_done(0)
                
                else:
                    if Debug:
                        print "#########################################"
                        print "# cannot handle fileTransferStatus: ", str(fileTransferStatus) 
                        print "#########################################"
                        fileTransferBox.SetEndingLabel('cannot handle fileTransferStatus.')
                        

            except:
                pass
        else:
            try:
                self.uploadNumber = self.downloadNumber = 0
                finished = True
                for i in self.bars:
                    if(i.gauge.GetValue() != 110 or i.gauge.GetValue() != 120):
                        finished = False
                        self.uploadNumber += 1
                
                if (finished == False): 
                    self.timer.Start(milliseconds=self.timeToCall, oneShot=True)
            except:
                pass
        
        if (self.numOfVMEToBeTransferred != 0):
            progress = round(float(float(self.numOfTransferredVME) / float(self.numOfVMEToBeTransferred)) * 100) + increment;
            fileTransferBox.gauge.SetValue(progress)
            fileTransferBox.SetRemainingTimeLabel("%d" % self.numOfTransferredVME + "/" + "%d" % self.numOfVMEToBeTransferred + " | %d%%" % progress)
        else: # If the number of vme that will be transferred is unknown
            fileTransferBox.gauge.Pulse()
            fileTransferBox.SetRemainingTimeLabel("%d/?" % self.numOfTransferredVME)

        self.Refresh()
    
    def SecondsToHMS(self,t):
        """Convert time t in seconds to hours minutes seconds."""
        hours, t = divmod(t, 60*60)
        minutes, seconds = divmod(t, 60)
        return (hours, minutes, seconds)

def test():
    def endFunction(): print "ending"
    import Queue
    app = wx.PySimpleApp()
    gui = FileTransferFrame(None , Queue.Queue(), endFunction)
    #gui.createSingleBar("UPLOAD")
    app.MainLoop()
    
if __name__ == '__main__':
  test()
  




