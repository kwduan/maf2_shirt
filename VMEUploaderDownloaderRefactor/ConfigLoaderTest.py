import unittest
import ConfigLoader
import os

class ConfigLoaderTest(unittest.TestCase):
      
    def testServiceURL(self):
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"
        
        curDir = os.getcwd()        
        
        print " current directory is: " + curDir
        
        cl = ConfigLoader.ConfigLoader("","testuser")
        self.assertEqual(cl.ServiceURL,"https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2")
        
    def testWebServicesURL(self):
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"
        
        curDir = os.getcwd()        
        
        print " current directory is: " + curDir
        
        cl = ConfigLoader.ConfigLoader("","testuser")
        self.assertEqual(cl.WebServicesURL,"https://ws.biomedtown.org")        
        
if __name__ == '__main__':
    unittest.main()
                