#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Stefano Perticoni <s.perticoni@scsolutions.it>
#-----------------------------------------------------------------------------

import sys
sys.path.append('./webServicesClient')

import httplib, urlparse, string
from base64 import encodestring, decodestring
import lhpCSVDictionaryDownloader

from Debug import Debug
import unittest
import msfParser

from xml.dom import minidom
from xml.dom import Node
from datetime import *
from time import *

import StringIO
import os


import urllib, urllib2, base64, re, os, cookielib, sys

class lhpCSVDictionaryDownloaderTest(unittest.TestCase):
      
    def setUp(self):
        
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"
        
        curDir = os.getcwd()        
        
        print " current directory is: " + curDir
          
    
    def testDownloadMasterDictionaryHTTPS(self):
        
        host = "www.biomedtown.org"
        selector = "/biomed_town/LHDL/users/swclient/dictionaries/LHDL_dictionary"
        
        dictionaryName = "lhpXMLDictionary"
        
        outputXMLDictionaryFilename = lhpCSVDictionaryDownloader.run(host, selector, dictionaryName)
        
        outputXMLDictionaryABSFileName = os.getcwd() + "\\" + outputXMLDictionaryFilename
        dictionaryExist = os.path.exists(outputXMLDictionaryABSFileName)
        self.assertTrue(dictionaryExist)
        os.remove(outputXMLDictionaryABSFileName)
        
    def testDownloadDicomSubdictionaryHTTPS(self):
                
        host = "www.biomedtown.org"
        selector = "/biomed_town/LHDL/users/swclient/dictionaries/DicomSource"
        
        dictionaryName = "lhpXMLDicomSubdictionary"
        
        outputXMLDictionaryFilename = lhpCSVDictionaryDownloader.run(host, selector, dictionaryName)
    
        outputXMLDictionaryABSFileName = os.getcwd() + "\\" + outputXMLDictionaryFilename
        dictionaryExist = os.path.exists(outputXMLDictionaryABSFileName)
        self.assertTrue(dictionaryExist)
        os.remove(outputXMLDictionaryABSFileName)
        
    def testDownloadMASourceSubdictionaryHTTPS(self):
        
        host = "www.biomedtown.org"
        selector = "/biomed_town/LHDL/users/swclient/dictionaries/MASource"
        
        dictionaryName = "lhpXMLMAsourceSubdictionary"
        
        outputXMLDictionaryFilename = lhpCSVDictionaryDownloader.run(host, selector, dictionaryName)
        
        outputXMLDictionaryABSFileName = os.getcwd() + "\\" + outputXMLDictionaryFilename
        dictionaryExist = os.path.exists(outputXMLDictionaryABSFileName)
        self.assertTrue(dictionaryExist)
        os.remove(outputXMLDictionaryABSFileName)
        
    def testDownloadFAOntologySubdictionaryHTTPS(self):
        
        host = "www.biomedtown.org"
        selector = "/biomed_town/LHDL/users/swclient/dictionaries/FA_onto"
        
        dictionaryName = "lhpXMLFAOntologySourceSubdictionary"
        
        outputXMLDictionaryFilename = lhpCSVDictionaryDownloader.run(host, selector, dictionaryName)
    
        outputXMLDictionaryABSFileName = os.getcwd() + "\\" + outputXMLDictionaryFilename
        dictionaryExist = os.path.exists(outputXMLDictionaryABSFileName)
        self.assertTrue(dictionaryExist)
        os.remove(outputXMLDictionaryABSFileName)
        
    def testDownloadMicroCTSubdictionaryHTTPS(self):
                
        host = "www.biomedtown.org"
        selector = "/biomed_town/LHDL/users/swclient/dictionaries/MicroCTSource"
        
        dictionaryName = "lhpXMLMicroCTSubdictionary"
        
        outputXMLDictionaryFilename = lhpCSVDictionaryDownloader.run(host, selector, dictionaryName)
        
        outputXMLDictionaryABSFileName = os.getcwd() + "\\" + outputXMLDictionaryFilename
        dictionaryExist = os.path.exists(outputXMLDictionaryABSFileName)
        self.assertTrue(dictionaryExist)
        os.remove(outputXMLDictionaryABSFileName)
        
    def testRun(self):
        
        host = "www.biomedtown.org"
        selector = "/biomed_town/LHDL/users/swclient/dictionaries/LHDL_dictionary"
        
        dictionaryName = "lhpXMLDictionary"
        
        lhpCSVDictionaryDownloader.run(host, selector, dictionaryName)
    
        
        
if __name__ == '__main__':
     unittest.main()
    
