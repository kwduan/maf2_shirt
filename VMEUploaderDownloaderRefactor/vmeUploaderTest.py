#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Stefano Perticoni
#-----------------------------------------------------------------------------

import vmeUploader
import os
import msfParser
import sys, string
import unittest
import shutil

class vmeUploaderTest(unittest.TestCase):
    
    def testCreateOutgoingDirectoryAndUpload(self):

        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"
        
        curDir = os.getcwd()        
        
        print " current directory is: " + curDir
        
        upl = vmeUploader.vmeUploader()
        testDir = curDir + r'\InputMSFForUploadTestData'
        upl.InputMSFDirectory = testDir
        upl.UnhandledPlusManualTagsListFileName = testDir + r'\unhandledPlusManualTagsList.csv'
        upl.HandledAutoTagsListFileName = testDir + r'\handledAutoTagsList.csv'
        upl.OutputFolderName = curDir + r'\Outgoing'
        upl.VmeToExtractID = 1    
        upl.Upload()
        
        outputFileFullPath = upl.OutputFolderName + "\\" + upl.OutputVMEXMLName
        
        existOutput = os.path.exists(outputFileFullPath)
        self.assertTrue(existOutput)
        
        os.remove(outputFileFullPath)
        existOutput = os.path.exists(outputFileFullPath)
        self.assertFalse(existOutput)
        
        
if __name__ == '__main__':
    unittest.main()
    
