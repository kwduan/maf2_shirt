import os, sys, shutil, time
from webServicesClient import xmlrpcDemoWS
from base64 import decodestring
import xml.dom.minidom as xd
from lhpDefines import *
from Debug import Debug
import ConfigLoader

from Crypto.PublicKey import RSA 
import base64

class downloadSingleXML():
    def __init__(self):
        
        self.fileToDownload = 'UNDEFINED'
        self.datasetSRBURI = 'UNDEFINED'
        self.datasetFileSize = -1
        self.proxyHost = ""
        self.proxyPort = 0
        self.serviceURL = ""
        self.decPassphrase = ""
        pass
    
    def download(self):
        self.proxyHost, self.proxyPort = retriveProxyParameters()
        # Get the service url from the configuration file
        cl = ConfigLoader.ConfigLoader("",self.user)
        ws = xmlrpcDemoWS.xmlrpc_demoWS()
        ws.setCredentials(self.user, self.password) 
        if self.serviceURL == "":
            ws.setServer(cl.ServiceURL)
        else:
             ws.setServer(self.serviceURL)
        ws.ProxyURL = self.proxyHost
        ws.ProxyPort = self.proxyPort
        
        #####ENCRYPTION######
        key=RSA.generate(1024,os.urandom)

        pubkey=key.publickey().exportKey()
        
        self.Result = ws.run('xmldownload', self.fileToDownload,pubkey)
        
        passPhrase = ''
        decPassphrase = ''
        try:
            if len(self.Result) == 2:
                passPhrase = self.Result[1]
                decPassphrase=key.decrypt(base64.b64decode(passPhrase))        
        except Exception,e:
            pass
        print decPassphrase
        
        print self.Result[0]
        
    def retrieveTagValue(self, tag):
        """Retrieve tag value from downloaded local XML file

Example:
d.retrieveTagValue('L0000_resource_data_Dataset_DatasetURI')
"""
        
        returnValue = ''
        try:
           file = open(self.fileToDownload, 'r')
        except :
            return ""
        dom = xd.parse(file)
        if dom.getElementsByTagName("fault"):
            if Debug:
                print "------Error in xmldownload service--------"
            return       
        for el in dom.getElementsByTagName('TItem'):
           if(el.attributes != None and el.attributes.get('Name')):
              attrNode = el.attributes.get('Name')
              attrValue = attrNode.nodeValue
              if(attrValue == tag):
                  childToFind = el.getElementsByTagName('TC')[0] # first element of childList
                  returnValue = childToFind.firstChild.nodeValue
                  if Debug:
                      print returnValue
        file.close()
        
        return returnValue
    
    def moveFileInIncomingCacheDirectory(self):
        shutil.move(self.fileToDownload, self.incomingCacheDir + self.fileToDownload)
        pass

def main():
       
    if(len(sys.argv) == 6):
        sys.argv = sys.argv[1:]

        dsXML = downloadSingleXML()
        dsXML.user = sys.argv[0]
        dsXML.password = sys.argv[1]
        dsXML.serviceURL = sys.argv[2]
        dsXML.fileToDownload = sys.argv[3]
        dsXML.incomingCacheDir = str(sys.argv[4]).replace("?", " ")
        dsXML.download()
        dsXML.datasetSRBURI = dsXML.retrieveTagValue('L0000_resource_data_Dataset_DatasetURI')
        dsXML.datasetFileSize = dsXML.retrieveTagValue('L0000_resource_data_Size_FileSize')        
        dsXML.moveFileInIncomingCacheDirectory()
        
        
        # datatset SRB Uri: BEWARE !!! This print value is used by the Client application!
        # DO NOT CHANGE! 
        print dsXML.datasetSRBURI
        
        # dataset file size: BEWARE !!! This print value is used by the Client application!
        # DO NOT CHANGE!  
        print dsXML.datasetFileSize
        
    else:
        print """
        Usage: 
        downloadSingleXML.py
        testuser # substitute user name
        6w8DHF # substitute user password
        dataresource-8660 # substitute xml test data present in the repository
        os.getcwd()+"\\testDownload\\" # substitute incoming cache dir
       
        Output:
        # datatset SRB Uri: BEWARE !!! This print value is used by the Client application!
        datasetSRBURI
        
        # dataset file size: BEWARE !!! This print value is used by the Client application!
        datasetFileSize
       
       """
if __name__ == '__main__':
    main()
