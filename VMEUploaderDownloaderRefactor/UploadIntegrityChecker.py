"""
This class implements a control for upload integrity.
It check that all VMEs are uploaded correctly.
"""

import os
import listBasket
#import wx

class ClientUploadStruct:
    def __init__(self):
        self.serverHost = ""
        self.serverPort = ""
        self.transferModality = ""
        self.cacheVMEIdForUploadOrSizeForDownload = ""
        self.currentCacheChildABSFolderWorkaround = ""
        self.user = ""
        #self.password = ""
        self.repositoryURL = ""
        self.originalVMEIdForUploadOrXMLURIForDownload = ""
        self.vmeHasLinkForUploadOrSRBDataURIForDownload = ""
        self.uploadWithChildren = ""
        self.XMLUploadedResourcesRollBackLocalFileName = ""
        self.remoteXMLResourceURI = ""
        self.isLastResourceToUpload = ""
        self.name = ""
        return
        

class UploadIntegrityChecker:
    def __init__(self,user,password, repository, filename="UploadCallStack.txt"):
        self.filename = filename
        self.user = user
        self.password = password
        self.clientCallStack = list()
        self.failureCallStack = list()
        self.resourcesID = list()
        self.serviceURL = repository
        return
        
    def __LoadClientCallStack(self):
    # Loads the client call-stack from control file
        ctrlFile = open(self.filename,"r")
        counter = 1
        for line in ctrlFile:
            #if counter == 1:
            #    item = ClientUploadStruct()
            #    item.serverHost = line
            #elif counter == 2:
            #    item.serverPort = line
            #elif counter == 3:
            #    item.transferModality = line
            if counter == 1:
                item = ClientUploadStruct()
                item.cacheVMEIdForUploadOrSizeForDownload = line[0:len(line) - 1]
            elif counter == 2:
                item.currentCacheChildABSFolderWorkaround = line[0:len(line) - 1]
            elif counter == 3:
                item.user = line[0:len(line) - 1]
            #elif counter == 4:
            #    item.password = line[0:len(line) - 1]
            elif counter == 4:
                item.repositoryURL = line[0:len(line) - 1]
            elif counter == 5:
                item.originalVMEIdForUploadOrXMLURIForDownload = line[0:len(line) - 1]
            elif counter == 6:
                item.vmeHasLinkForUploadOrSRBDataURIForDownload = line[0:len(line) - 1]
            elif counter == 7:
                item.uploadWithChildren = line[0:len(line) - 1]
            elif counter == 8:
                item.XMLUploadedResourcesRollBackLocalFileName = line[0:len(line) - 1]
            elif counter == 9:
                item.remoteXMLResourceURI = line[0:len(line) - 1]
            elif counter == 10:
                item.isLastResourceToUpload = line[0:len(line) - 1]
            elif counter == 11:
                item.name = line[0:len(line) - 1]
                self.clientCallStack.append(item)
                counter = 0
            counter = counter + 1
        return
        
    def __GetSandBoxResourceListing(self):
    # Get resources Ids from SandBox
        sandBox = listBasket.listBasket(4) # From SandBox
        sandBox.SetCredentials(self.user, self.password)
        sandBox.SetServiceURL(str(self.serviceURL))
        sandBox.getListFromBasket()
        for r in range(0,len(sandBox.IdList)/2):
            self.resourcesID.append(sandBox.IdList[1 + (r * 2)]) # get dataresources
        return

        
    def __CheckVMEIntegrity(self,dataresource):
        for id in self.resourcesID:
            #wx.LogMessage("comparing %s with" % id + " %s" % dataresource)
            if dataresource == id:
                return True
        return False # Not correctly uploaded vme are not listed

        
    def CheckUploadIntegrity(self, failureList):
        try:
            self.__LoadClientCallStack()
        except:
            return
        self.__GetSandBoxResourceListing()
        for item in self.clientCallStack:
            if(self.__CheckVMEIntegrity(item.remoteXMLResourceURI)==False):
                if(item.user == self.user):
                    failureList.append(item) # get the item that was not uploaded correctly
        
        #if(len(failureList) == 0):
        os.remove(self.filename)
        #else:
        if(len(failureList) > 0):
            self.failureCallStack = failureList
            self.__SaveFailureCallStack()
        return
            
    def __SaveFailureCallStack(self):
        ClearPendingUploads()
        ctrlFile = open(self.filename,"a")
        # Write all parameters
        for item in self.failureCallStack:
            #ctrlFile.write(item.serverHost + "\n")
            #ctrlFile.write(item.serverPort + "\n")
            #ctrlFile.write(item.transferModality + "\n")
            ctrlFile.write(item.cacheVMEIdForUploadOrSizeForDownload + "\n")
            ctrlFile.write(item.currentCacheChildABSFolderWorkaround + "\n")
            ctrlFile.write(item.user + "\n")
            #ctrlFile.write(item.password + "\n")
            ctrlFile.write(item.repositoryURL + "\n")
            ctrlFile.write(item.originalVMEIdForUploadOrXMLURIForDownload + "\n")
            ctrlFile.write(item.vmeHasLinkForUploadOrSRBDataURIForDownload + "\n")
            ctrlFile.write(item.uploadWithChildren + "\n")
            ctrlFile.write(item.XMLUploadedResourcesRollBackLocalFileName + "\n")
            ctrlFile.write(item.remoteXMLResourceURI + "\n")
            ctrlFile.write(item.isLastResourceToUpload + "\n")
            ctrlFile.write(item.name + "\n")
        ctrlFile.close()
        return

# Static-like methods
def ClearPendingUploads():
    try:
        os.remove(self.filename)
    except:
        pass

def ArePendingDownloads():
    return (os.path.exist(self.filename) and os.path.isfile(self.filename))

def test():
    uploadChecker = UploadIntegrityChecker("testuser","6w8DHF", "https://test.physiomespace.com/Members/testuser/")
    l = list()
    uploadChecker.CheckUploadIntegrity(l)
    

if __name__ == '__main__':
        test()
            
