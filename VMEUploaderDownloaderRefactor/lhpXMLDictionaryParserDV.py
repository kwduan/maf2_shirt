#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Josef Kohout
#-----------------------------------------------------------------------------
from test.test_codecs import Str2StrTest
from pickle import NONE

import sys, string
from Debug import Debug
from xml.dom import minidom 
from xml.dom import Node
import Enum
import os
from string import split
import lhpXMLDictionaryParser
            
class lhpXMLDictionaryParserDV(lhpXMLDictionaryParser.lhpXMLDictionaryParser):
    """This class ehances lhpXMLDictionaryParser and adds an option to extract default values for each tag"""
        
    def GetVMETagArrayTagDefaultsList(self):
        """Return list of every dictionary tags default values """
        
        self.TagArrayTagDefaultsList = []
        self.__GetTagArrayTagDefaultsInternal(self.DictionaryDOMDocumentRoot, 0)
        return self.TagArrayTagDefaultsList
    
    def __GetTagArrayTagDefaultsInternal(self, parent, level):  
        dictionary = self.GetAttributesDictionary(parent)
        if "DefaultValue" in dictionary:
            self.TagArrayTagDefaultsList.append(dictionary["DefaultValue"])            
        else:        
            self.TagArrayTagDefaultsList.append("")                    
        
        if parent.childNodes:
            for node in parent.childNodes:
                self.__GetTagArrayTagDefaultsInternal(node, level)
    
def run(xmlDictionaryFilename, outputTagsFileName):
    """"""
    # load XML dictionary
    lhpXMLDictionaryParserInstance = lhpXMLDictionaryParserDV()
    lhpXMLDictionaryParserInstance.LoadXMLDictionary(xmlDictionaryFilename)
    
    tagsNames = lhpXMLDictionaryParserInstance.GetVMETagArrayTagNamesList()
    tagsDefault = lhpXMLDictionaryParserInstance.GetVMETagArrayTagDefaultsList()
    
    # Save
    tagsFile = open(outputTagsFileName, 'w')
    for index in range(len(tagsNames)):
        print "storing: " + str(tagsNames[index]) + "," + str(tagsDefault[index])
        print >> tagsFile, str(tagsNames[index]) + "," + str(tagsDefault[index])
    tagsFile.close()

    return 

if __name__ == '__main__':
    import sys
    usage_msg = '''Usage: %s <xmlDictionaryFilename> <outputCsvFile>''' % sys.argv[0]

    if len(sys.argv) != 3:
        print 'Error :\n' + usage_msg
        sys.exit(1)

    xmlDictionaryFilename = sys.argv[1]    
    outputTagsFileName = sys.argv[2]
    
    run(xmlDictionaryFilename, outputTagsFileName)
