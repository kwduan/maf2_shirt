# --------------------------------------------------------------------------------- #
# FLEXAUTOCHECKTREECTRL wxPython IMPLEMENTATION
# Derived by wx.lib.agw.customtreectrl
#
# This class activate autochecking of a node subtree according to 
# data associated with this node.
#
# Eleonora Mambrini @ 17 August 2010
#
# --------------------------------------------------------------------------------- #
import wx

import customtreectrl as customTree
from customtreectrl import TreeEvent
from customtreectrl import (TR_AUTO_CHECK_CHILD, TR_AUTO_CHECK_PARENT, TR_AUTO_TOGGLE_CHILD, wxEVT_TREE_ITEM_CHECKING, wxEVT_TREE_ITEM_CHECKED)
                
#---------------------------------------------------------------------------
# MyCustomTreeCtrl Implementation
#---------------------------------------------------------------------------
class FlexAutoCheckTreeCtrl(customTree.CustomTreeCtrl):

    def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=0, agwStyle=wx.TR_DEFAULT_STYLE, validator=wx.DefaultValidator,
                 name="MyCustomTreeCtrl"):

        customTree.CustomTreeCtrl.__init__(self, parent, id, pos, size, style, agwStyle, validator)
            
    def CheckItem(self, item, checked=True, forceAutoCheck=False):
        """
        Actually checks/uncheks an item, sending (eventually) the two
        events ``EVT_TREE_ITEM_CHECKING`` and ``EVT_TREE_ITEM_CHECKED``.

        :param `item`: an instance of L{GenericTreeItem};
        :param `checked`: ``True`` to check an item, ``False`` to uncheck it.
        """

        # Should we raise an error here?!?        
        if item.GetType() == 0:
            return
        
        itemData = item.GetData()
        
        autocheck = itemData[1] or forceAutoCheck

        if item.GetType() == 2:    # it's a radio button
            if not checked and item.IsChecked():  # Try To Unckeck?
                return
            else:
                if not self.UnCheckRadioParent(item, checked):
                    return

                self.CheckSameLevel(item, False)
                return
            
        # Radiobuttons are done, let's handle checkbuttons...
        e = TreeEvent(wxEVT_TREE_ITEM_CHECKING, self.GetId())
        e.SetItem(item)
        e.SetEventObject(self)
        
        if self.GetEventHandler().ProcessEvent(e):
            # Blocked by user
            return 
        
        item.Check(checked)
        dc = wx.ClientDC(self)
        self.RefreshLine(item)

        if self.HasAGWFlag(TR_AUTO_CHECK_CHILD) and autocheck == True:
            ischeck = self.IsItemChecked(item)
            self.AutoCheckChild(item, ischeck)
        if self.HasAGWFlag(TR_AUTO_CHECK_PARENT):
            ischeck = self.IsItemChecked(item)
            self.AutoCheckParent(item, ischeck)
        elif self.HasAGWFlag(TR_AUTO_TOGGLE_CHILD):
            self.AutoToggleChild(item)

        e = TreeEvent(wxEVT_TREE_ITEM_CHECKED, self.GetId())
        e.SetItem(item)
        e.SetEventObject(self)
        self.GetEventHandler().ProcessEvent(e)

        
