#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Roberto Mucci <r.mucci@cineca.it>
#-----------------------------------------------------------------------------

from webServicesClient import xmlrpcDemoWS
import xml.dom.minidom as xd
import os, time
from lhpDefines import *
import StringIO
from Debug import Debug

import urllib, urllib2, base64, re, os, cookielib, sys
from HttpsProxy import *

from Crypto.PublicKey import RSA
import base64

class SenderPassPhrase:
    """Creates an empty resource on repository"""
  
    def __init__(self):
      
        self.userName = "UNDEFINED"
        self.password = "UNDEFINED"
        self.URL = "UNDEFINED"
        
        self.passphrase = ""
        self.publickey = ""
        self.passphraseEncrypted = ""
        self.xmlresource = ""
                
        # proxy
        self.proxyHost = ''
        self.ProxyPort = ''
    def EncryptPassPhrase(self):
    
        key = RSA.importKey(self.publickey)
        self.passphraseEncrypted = key.encrypt(self.passphrase,"")[0]
        
    def Send(self):
        """ 
           Encrypte passphrase using public key and up
        """
        
        self.EncryptPassPhrase()
        
        maxTry = 2
        self.proxyHost, self.proxyPort = retriveProxyParameters()
        if Debug:
            print "->"+ self.proxyHost + "<-"
            print "->"+ str(self.proxyPort) + "<-"
        
        ws = xmlrpcDemoWS.xmlrpc_demoWS()
        ws.setCredentials(self.userName, self.password)
        ws.setServer(self.URL)
        ws.ProxyURL = self.proxyHost
        ws.ProxyPort = self.proxyPort
        XMLURI = -1
        
        # timeout in seconds
        self.Timeout = 240
        socket.setdefaulttimeout(self.Timeout)
        
        for c in range(0,maxTry):
            try:
                enc= base64.b64encode(self.passphraseEncrypted)                
                out = ws.run('setDataResourcePassphrase',self.xmlresource,enc)[1]
               
            except:
                if Debug:
                    print "-----------Error calling SetDataResourcePassPhrase service------------"
                    
                print "Unexpected error:", sys.exc_info()[0]
                
                print 'NO'
                time.sleep(3)
                continue
                
            dom = xd.parseString(out)
            
            if dom.getElementsByTagName("fault"):
                if Debug:
                    print "-----------Error in createresource service------------"
                    return "ERROR"
                # for el in dom.getElementsByTagName("string"):
                    # for node in el.childNodes:  
                        # error = node.data
                    # if (error.find('Over Quota') != -1):
                        # overQuota = "OverQuota"
                        # print overQuota
                        # return overQuota
                    
                # if Debug:
                    # print error
                  

            
            for el in dom.getElementsByTagName("string"):
                for node in el.childNodes:  
                    XMLURI = node.data
            if XMLURI == -1:
                continue
            else:
                break
    
        print XMLURI
        if  XMLURI == -1:
            sys.exit(1)
        return XMLURI
  
def main():
    
    usage_msg = '''Usage: %s user, password, URL ''' % sys.argv[0]
    if(len(sys.argv) != 7): #for test
        print len(sys.argv)
        for i in range(0,len(sys.argv)):
            print sys.argv[i]
        print 'Error :\n' + usage_msg
        sys.exit(1)
        
       
    if(len(sys.argv) == 7):
        sys.argv = sys.argv[1:]
        getURI = SenderPassPhrase()
        getURI.userName = sys.argv[0]
        if Debug: print 'userName : '+getURI.userName
        getURI.password = sys.argv[1]
        if Debug: print 'password : '+getURI.password
        getURI.URL = sys.argv[2]
        if Debug: print 'URL : '+getURI.URL
        
        getURI.passphrase = sys.argv[3]
        if Debug: print 'passphrase : '+getURI.passphrase
        getURI.publickey = str(sys.argv[4])
        getURI.publickey = getURI.publickey.replace('###','\n')
        
        getURI.xmlresource = sys.argv[5]
        if Debug: print 'xmlresource : '+getURI.xmlresource
        
        getURI.XMLURI = getURI.Send()
        
        print getURI.XMLURI

if __name__ == '__main__':
    main()
        