## Matteo Balasso m.balasso@scsolutions.it
## under the supervision of Stefano Perticoni s.perticoni@scsolutions.it

"""Usage:

leaking.py -x TESTMODULE.py 
    Execute the given test module and report about leaking objects (if any)
    coverage data.

leaking.py -b DIR
    Execute all Test modules found into DIR and its subfolders and report
    with coverage statment percentage and percentage of tested module over
    all modules found.
    Memory Leaks information are collected as well. 


Works with Python 2.5.X
"""

import os
import re
import string
import sys
import types
import gc


def howmany(cls):
    return len([x for x in gc.get_objects() if isinstance(x,cls)])


the_leaking = None

class leaking:
    error = "leaking error"

    # Cache of leaking object analysis result
    leaking_objects = []

    def __init__(self):
        global the_leaking
        if the_leaking:
            raise self.error, "Only one coverage object allowed."
    
    def help(self, error=None):
        if error:
            print error
            print
        print __doc__
        sys.exit(1)

    def command_line(self):
        import getopt
        settings = {}
        optmap = {
            '-h': 'help',
            '-x': 'execute',
            '-b': 'batch',
            '-c': 'only-coverage'
            }
        
        short_opts = string.join(map(lambda o: o[1:], optmap.keys()), '')
        long_opts = optmap.values()
        options, args = getopt.getopt(sys.argv[1:], short_opts,
                                      long_opts)
        for o, a in options:
            if optmap.has_key(o):
                settings[optmap[o]] = 1
            elif optmap.has_key(o + ':'):
                settings[optmap[o + ':']] = a
            elif o[2:] in long_opts:
                settings[o[2:]] = 1
            elif o[2:] + '=' in long_opts:
                settings[o[2:]] = a
            else:
                self.help("Unknown option: '%s'." % o)

        if settings.get('help'):
            self.help()

        args_needed = (settings.get('execute') or settings.get('batch'))

        action = settings.get('erase') or args_needed
        
        if not action:
            self.help("You must specify at least one of -b, -x")
            
        if not args_needed and args:
            self.help("Unexpected arguments %s." % args)

            

        if settings.get('execute'):
            if not args:
                self.help("Nothing to do.")
                return 0

            if not str(args[0]).lower().count('test.py'):
                self.help("Please pass me a test module.")
                return 0

            print "\n--->>> EXECUTING TEST MODULE\n"




            my_test_modules = [ args[0] ]
            
            my_target_modules = []

               
        if settings.get('batch'):
            #here we have to do our marvellous batch
            print "\n--->>> BATCH EXECUTING TEST MODULES\n"

            
            
            if not args:
                self.help("Please insert the directory for scanning load data.")
            
            try:
               
                from scan_directory import scan_directory

                my_test_modules = scan_directory(args[0], scanSub=True, ext=".py", key="Test", haveKey=True, withPath=True, withExt=True)
                my_target_modules = scan_directory(args[0], scanSub=True, ext=".py", key="Test", haveKey=False, withPath=True, withExt=True)

                black_list = ['coverageleaking.py','Enum.py','scan_directory.py','images.py', 'vmeUploaderOnly.py','DSV.py','CSVOMATIC.py','leaking.py','__init__.py', 'Debug.py', 'downloadSelectorFrame.py']
                to_remove = []
                
                for targ in my_target_modules:
                    a,tail = os.path.split(targ)
                    if black_list.count(tail):
                        to_remove.append(targ)

                for t in to_remove:
                    my_target_modules.remove(t)


            except Exception, e:
                print "Exception: ", str(e)


        if settings.get('batch') or settings.get('execute'):

            sysPathBeforeBatch = sys.path
            defaultTestExecutionDirectory = os.getcwd()

            try:
                
                import __main__

                original_stdout = sys.stdout
                original_stderr = sys.stderr
                fout = open("leaking_test_mods_output.log", "w")

                for mod in my_test_modules:
                    
                    gc.collect()
                    sys.path = sysPathBeforeBatch
                    os.chdir(defaultTestExecutionDirectory)
                    sys.argv = []
                    path,name = os.path.split(mod)
                    
                    sys.argv.insert(0,name)
                    sys.path.append(path)
                    sys.path[0] = os.path.dirname(path)
                    
                    #search for the corresponding target module
                    target_name =  name.count("Test.py") and name.replace("Test.py",".py") or name.count("test.py") and name.replace("test.py",".py")

                    
                    objs = []

                    try:

                        #Importing target module                 
                        m = __import__(os.path.splitext(target_name)[0])           

                        try:
                            #look for class types into target module
                            for sym in dir(m):
                                o = getattr (m, sym)
                                if type(o) is types.ClassType:                            
                                    objs.append((o,howmany(o)))
                                    #DEBUG print "\t\t\t %s, %d" % (o,howmany(o))

                        except BaseException,e:
                            print "\t\t", str(e)

                    except BaseException,e:
                       
                        print "\tTrouble while importing ", target_name, str(e)

                    if not settings.get('only-coverage'):

                        try:
                            sys.stdout = original_stdout
                            sys.stderr = original_stderr

                            
                            print "\t>", mod
                            print "\t> Executing Test Module (CTRL+C To Interrupt)", mod
                       
                            print "\n\n\n----------->>>", mod

                            

                            execfile(mod , __main__.__dict__)

                            sys.stdout = original_stdout
                            sys.stderr = original_stderr
                            
                            print "\t\t\t>DONE"


                        except SystemExit :
                            pass
                        except Exception,e:
                            sys.stdout = original_stdout
                            sys.stderr = original_stderr
                            print "\t\t1Exception: ", str(e)
                        except KeyboardInterrupt:
                            sys.stdout = original_stdout
                            sys.stderr = original_stderr
                            print "\t\tKeayboardInterrupt"

                        #check for leaks
                        for o,hm in objs:                
                            if howmany(o) > hm:
                                #DEBUG sys.stdout = original_stdout
                                #DEBUG print "LEAK: \t",o,howmany(o) - hm
                                self.leaking_objects.append((o, howmany(o)-hm, target_name))

                #close output file 
                fout.close()

                #redirect stout and stderr
                sys.stdout = original_stdout
                sys.stderr = original_stderr

                #print a nice_report


                if settings.get('batch'):

                    print "\n---->BATCH ENDED" 

                    print "\n\n--->>> COVERAGE REPORT\n"

                    print "%d modules over a total of %d have a test module\n" % (len(my_test_modules), len(my_target_modules))
                    print "Coverage = " + str(int( float(len(my_test_modules) * 100) / float(len(my_target_modules)) )) + "%"
                    
                    print "\n\n----- Mods without a test \n"                    
                    
                    for target in my_target_modules:
                         if not my_test_modules.count( target.replace('.py','Test.py') ):
                             print "\t %s " % target

                if not settings.get('only-coverage'):

                    try:
                        print "\n--->>> LEAKING OBJECTS REPORT:\n"

                        #format string operations
                        ob_names = []
                        for ob,ls,mod in self.leaking_objects:
                            ob_names.append(str(ob))
                            
                        
                        header = "TypeClass" + " Leaks   Module"                   
                        
                        print header
                        print "-"*len(header)

                        for ob,ls,mod in self.leaking_objects:
                            args = (ob,ls,mod)
                            print ob,"\t",ls,"\t",mod

                        print "\n"
                    except BaseException, e:
                        print str(e)
                
            except Exception, e:
                print "2Exception: ", str(e)

            sys.path = sysPathBeforeBatch


            
        if not args:
            args = self.cexecuted.keys()
            
        ignore_errors = settings.get('ignore-errors')
        show_missing = settings.get('show-missing')
        directory = settings.get('directory=')
        if settings.get('report'):
            self.report(args, show_missing, ignore_errors)
        if settings.get('annotate'):
            self.annotate(args, directory, ignore_errors)

    
# Singleton object.
the_leaking = leaking()

# Command-line interface.
if __name__ == '__main__':     
    the_leaking.command_line()


