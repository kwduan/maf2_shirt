#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Roberto Mucci <r.mucci@cineca.it>
#-----------------------------------------------------------------------------

import binaryImporter
import os, time, shutil
from lhpDefines import *
import StringIO
import unittest
import sys

class binaryImporterTest(unittest.TestCase):
      
    def testCopyBinary(self):

        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"
        
        curDir = os.getcwd()        
        
        print " current directory is: " + curDir
        
        sourceFileName = curDir + r'\msf_test_import_export_VME\msf_test_import_export_VME.56.vtk'
        targetFileName = curDir + r'\msf_test_import_export_VME\newVME.1.vtk'
        
        sys.argv = []        
        
        sys.argv.append("false")  # isAnimated = false        
        sys.argv.append(sourceFileName)
        sys.argv.append(targetFileName)
        
        binImporter = binaryImporter.binaryImporter(TestMode = True)
        binImporter.completeFileSizeFromRepository = 252
        binImporter.CopyBinary()
        
        line = 0
        for arg in sys.argv:
            print "sys.argv[",line,"]: ", arg
            line = line + 1
        
        targetFileExists = os.path.exists(targetFileName)
        self.assertTrue(targetFileExists)
        
        targetFileSize = os.stat(targetFileName)[6]
        self.assertEqual(targetFileSize, binImporter.completeFileSizeFromRepository)
        
if __name__ == '__main__':
    unittest.main()
                