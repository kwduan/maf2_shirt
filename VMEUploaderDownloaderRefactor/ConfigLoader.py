import sys

class ConfigLoader:
    def __init__(self, abspath, user):
        self.AbsPath = abspath
        self.UserName = user
        self.ConfigFileName = "VMEUploadDownloader_config.ini"
        self.ServiceURL = self.GetServiceURL()
        self.WebServicesURL = self.GetWebServicesURL()
        
    #m_ServiceURL parameter of 
    def GetServiceURL(self):
        serviceURL = self._ReadParameter("ServiceURL")
        if(serviceURL == ""): # if empty put the standard one
            serviceURL = "https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2"
        
        if(serviceURL.find("{username}") != -1):
            serviceURL = serviceURL.replace("{username}",self.UserName)
        return serviceURL
        
    def GetWebServicesURL(self):
        webServicesURL = self._ReadParameter("WebServicesURL")
        if(webServicesURL == ""): # if empty put the standard one
            webServicesURL = "https://ws.biomedtown.org"
        return webServicesURL
    
    def _ReadParameter(self,parameter):
        #assert(self.AbsPath)
        assert(self.ConfigFileName)
        parameterValue = ""
        parameterName = parameter + "="
        
        if(self.AbsPath != ""):
            absConfigFilePath = self.AbsPath + "//" + self.ConfigFileName
        else:
            absConfigFilePath = self.ConfigFileName
        try:
            configFile = open(absConfigFilePath, "r")
            
            for lineRaw in configFile:
            
                line= ""
                # remove spaces from the line
                for word in lineRaw:
                    line = line + word.strip()
                
                # remove the portion of string after the first '#' (comments)
                commentPlace = line.find('#')
                if(commentPlace != -1):
                    line = line[0:commentPlace]
                
                # put in lower case
                #line = line.lower()
                
                # Get the service url
                servicePlace = line.find(parameterName)
                if(servicePlace != -1): #the last win
                    line = line[servicePlace + len(parameterName) : len(line)]
                    parameterValue = line
            configFile.close()
        except:
            parameterValue = ""
              
        return parameterValue
 
def test():
    cl = ConfigLoader("")
    print cl.ServiceURL
    print cl.WebServicesURL

if __name__ == '__main__':
    test()