#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Roberto Mucci <r.mucci@cineca.it>
#-----------------------------------------------------------------------------

import lhpEditRemoteTag
import os, time, shutil
from lhpDefines import *
import StringIO
import unittest
import sys
import downloadSingleXML
import xml.dom.minidom as xd


class lhpEditRemoteTagTest(unittest.TestCase):
      
    def testEditTag(self):
        
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"
        
        curDir = os.getcwd()        
        
        print " current directory is: " + curDir
       
        user = "testuser"
        password = "6w8DHF"
       
        xmlResourceToDownload = 'dataresource.2010-06-16.1276692717524'
        tagName = "L0000_resource_Documentation"
        d = downloadSingleXML.downloadSingleXML()
        
        d.user = user
        d.password = password
        d.serviceURL = 'https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2'
        
        d.fileToDownload = xmlResourceToDownload
        d.incomingCacheDir = str(os.getcwd()+"\\testDownload\\")
      
        print "downloading xml resource: " , str(xmlResourceToDownload) , " in " , d.incomingCacheDir , "..."
        d.download()
        print "done!"
       
        oldTagValue = d.retrieveTagValue(tagName) 
        print "old tag value from repository is: " + oldTagValue
        
        newTagValueToSet = "UNDEFINED"
         
        # from https://www.biomedtown.org/pluto to TOPOLINO and back...
        if oldTagValue == "https://www.biomedtown.org/pluto":
            newTagValueToSet = "https://www.biomedtown.org/topolino"
        else:
            newTagValueToSet = "https://www.biomedtown.org/pluto"
            
        print "newTagValueToSet: " , newTagValueToSet
        
        serviceURL = 'https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2/' + xmlResourceToDownload
        
        parameters =  xmlResourceToDownload + ',' + tagName + ',' + newTagValueToSet
        print "parameters: " , str(parameters)
        
        sys.argv = []
        sys.argv.append(user) 
        sys.argv.append(password)
        sys.argv.append(serviceURL)
        sys.argv.append(parameters)
        
        print ""
        print "Arguments for main:"
        print user + " " + password + " " + serviceURL + " " + parameters
        edit = lhpEditRemoteTag.lhpEditRemoteTag()
        print ""
        
        print "Editing remote tag: " , tagName , " on remote resource: " , xmlResourceToDownload
        out = edit.EditTag()
        
        print out
        dom = xd.parseString(out) 
        editSuccessful = not dom.getElementsByTagName("fault")
        self.assertTrue(editSuccessful)

if __name__ == '__main__':
    unittest.main()
        
