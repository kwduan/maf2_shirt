import DownloadIntegrityChecker
import unittest
import shutil
import difflib
import pickle
import os

class DownloadIntegrityCheckerTest(unittest.TestCase):
 
        
    def setUp(self):
        
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"
        curDir = os.getcwd()        
        print " current directory is: " + curDir
        
        
    def testFixture(self):
        pass
        
        
    def test(self):
        dic = DownloadIntegrityChecker.DownloadIntegrityChecker()
        
        item = DownloadIntegrityChecker.ClientDownloadStruct()
        
        item.cacheVMEIdForUploadOrSizeForDownload = "cacheVMEIdForUploadOrSizeForDownload"
        item.currentCacheChildABSFolderWorkaround = "currentCacheChildABSFolderWorkaround"
        item.user = "user"
        item.repositoryURL = "repositoryURL"
        item.originalVMEIdForUploadOrXMLURIForDownload = "originalVMEIdForUploadOrXMLURIForDownload"
        item.vmeHasLinkForUploadOrSRBDataURIForDownload = "vmeHasLinkForUploadOrSRBDataURIForDownload"
        item.isLastResourceToDownload = "isLastResourceToDownload"
        item.binaryCall = "binaryCall"
        
        dic.SaveFailedDownload(item)
        assert(dic.GetNumberOfFailedDownload()==1)
        
        dic.SetFailureAsCallStack()
        
        pop_item = DownloadIntegrityChecker.ClientDownloadStruct()
        
        dic.RemoveDownloadCall("currentCacheChildABSFolderWorkaround",pop_item)
                
        assert(item.cacheVMEIdForUploadOrSizeForDownload == pop_item.cacheVMEIdForUploadOrSizeForDownload)
        assert(item.currentCacheChildABSFolderWorkaround == pop_item.currentCacheChildABSFolderWorkaround)
        assert(item.user == pop_item.user)
        assert(item.repositoryURL == pop_item.repositoryURL)
        assert(item.originalVMEIdForUploadOrXMLURIForDownload == pop_item.originalVMEIdForUploadOrXMLURIForDownload)
        assert(item.vmeHasLinkForUploadOrSRBDataURIForDownload == pop_item.vmeHasLinkForUploadOrSRBDataURIForDownload)
        assert(item.isLastResourceToDownload == pop_item.isLastResourceToDownload)
        assert(item.binaryCall == pop_item.binaryCall)
        
        assert(dic.GetNumberOfFailedDownload()==0)
        
        
if __name__ == '__main__':
    
    unittest.main()        
"""

class ClientDownloadStruct:
    def __init__(self):
        self.cacheVMEIdForUploadOrSizeForDownload = ""
        self.currentCacheChildABSFolderWorkaround = ""
        self.user = ""
        self.repositoryURL = ""
        self.originalVMEIdForUploadOrXMLURIForDownload = ""
        self.vmeHasLinkForUploadOrSRBDataURIForDownload = ""
        self.isLastResourceToDownload = ""
        self.binaryCall = ""
        return

class BinariesDataresourceStruct:
    def __init__(self):
        self.binaries = []
        self.dataresource = ""
        
class DownloadIntegrityChecker:
    def __init__(self,user="",password="",out_msf_dir="",tmp_msf_filename="",filename="DownloadCallStack.txt"):
        self.filename = filename
        self.out_msf_dir = out_msf_dir
        self.tmp_msf_filename = tmp_msf_filename #outputMAF.msf
        self.user = user
        self.password = password
        self.clientCallStack = list()
        self.failureCallStack = list()
        self.failure_filename = "DownloadFailureCallStack.txt"
        self.binaryDataList = list()
        #self.app = wx.PySimpleApp()
        #self.app.MainLoop()
        return
        
    def __LoadClientCallStack(self):
        # Loads the client call-stack from control file
        done = False
        count = 0
        while(done == False):
            try:
                ctrlFile = open(self.filename,"r")
                counter = 1
                for line in ctrlFile:
                    if counter == 1:
                        item = ClientDownloadStruct()
                        item.cacheVMEIdForUploadOrSizeForDownload = line[0:len(line) - 1]
                    elif counter == 2:
                        item.currentCacheChildABSFolderWorkaround = line[0:len(line) - 1]
                    elif counter == 3:
                        item.user = line[0:len(line) - 1]
                    #elif counter == 4:
                    #    item.password = line[0:len(line) - 1]
                    elif counter == 4:
                        item.repositoryURL = line[0:len(line) - 1]
                    elif counter == 5:
                        item.originalVMEIdForUploadOrXMLURIForDownload = line[0:len(line) - 1]
                    elif counter == 6:
                        item.vmeHasLinkForUploadOrSRBDataURIForDownload = line[0:len(line) - 1]
                    elif counter == 7:
                        item.isLastResourceToDownload = line[0:len(line) - 1]
                    elif counter == 8:
                        item.binaryCall = line[0:len(line) - 1]
                        self.clientCallStack.append(item)
                        counter = 0
                    counter = counter + 1
                ctrlFile.close()
                done = True
            except:
                #print "file not found"
                done = False
                count = count + 1
                if (count==10):
                    return
                time.sleep(1)
                
                
 
    def __GetMSFBinaryDataList(self):
        if(self.tmp_msf_filename == ""):
            return
        assert(os.path.exists(self.tmp_msf_filename))
        try:
            file = open(self.tmp_msf_filename,"r")
        except:
            return
        
        msfElementTree = ElementTree.parse(file)
        msfRootNode = msfElementTree.getroot()
        
        self.__GetMSFBinaryDataListLoop(msfRootNode)
                        
                        
    def __GetMSFBinaryDataListLoop(self,rootNode):
        xmlParser = msfLXMLParser.msfLXMLParser()
        for node in rootNode:
            #  search for a Node with name "Node" ie a vme
            if node.tag == "Node":
                print node.get("Name")
                binaries = []
                binariesTrim = []
                binaries = xmlParser.GetVMEDataURLList(node) # get the URLs of the binaries associated to the VME
                for binary in binaries:
                    if(binary!=""):
                        binariesTrim.append(binary)
                if(len(binariesTrim)!=0):
                    print binariesTrim
                    #print "detected binaries:", binaries
                    #item = BinariesDataresourceStruct()
                    #item.binaries = binaries
                    # Workaround to get the vme resource id (dataresource...) from the msf
                    tagArrayNode = xmlParser.GetVmeTagArrayNode(node)
                    print tagArrayNode
                    if(len(tagArrayNode)>0):
                        tagNode = xmlParser.GetVmeTagItemNodeByName(tagArrayNode,'L0000_resource_data_Quality_QualityScore1_URL')
                        print tagNode
                        tagValue = xmlParser.GetTagNodeText(tagNode)
                        print tagValue
                        if(tagValue!=""):
                            pos = tagValue.find("dataresource.")
                            dataresource = tagValue[pos:pos+37]
                            item = BinariesDataresourceStruct()
                            item.binaries = binariesTrim
                            item.dataresource = dataresource
                            self.binaryDataList.append(item)
            self.__GetMSFBinaryDataListLoop(node)
    
    def GetMSFBinaryDataListTest(self):
        testFile = open("GetMSFBinaryDataListTest.txt","w+")
        self.__GetMSFBinaryDataList()
        for item in self.binaryDataList:
            for binary in item.binaries:
                testFile.write(binary + ", ")
            testFile.write(item.dataresource + "\n")
        testFile.close()
    
    def __CheckVMEIntegrity(self,dataresource):
        for item in self.binaryDataList:
            if(item.dataresource == dataresource):
                retval = True
                for binary in item.binaries:
                    retval = os.path.exists(self.out_msf_dir + "//" + binary) and retval
                    print "for " + dataresource + " retval is " + str(retval)
                return retval
        return True
    
    def CheckDownloadIntegrity(self, failureList):
        while(self.__IsLocked()):
            time.sleep(1) # wait one second
            pass
        self.__Lock()
        if(self.user == ""):
            return
            self._Unlock()
        try:
            self.__LoadClientCallStack()
        except:
            return
            self.__Unlock()
        self.__GetMSFBinaryDataList()
        for item in self.clientCallStack:
            if(self.__CheckVMEIntegrity(item.originalVMEIdForUploadOrXMLURIForDownload)==False):
                if(item.user == self.user):
                    failureList.append(item) # get the item that was not uploaded correctly
        self.__Unlock()
        #if(len(failureList) == 0):
        os.remove(self.filename)
        #else:
        if(len(failureList) > 0):
            self.failureCallStack = failureList
            self.__SaveFailureCallStack()
        return
    
    #Not used
    def __SaveFailureCallStack(self):
        self.ClearPendingDownloads()
        ctrlFile = open(self.filename,"")
        # Write all parameters
        for item in self.failureCallStack:
            #ctrlFile.write(item.serverHost + "\n")
            #ctrlFile.write(item.serverPort + "\n")
            #ctrlFile.write(item.transferModality + "\n")
            ctrlFile.write(item.cacheVMEIdForUploadOrSizeForDownload + "\n")
            ctrlFile.write(item.currentCacheChildABSFolderWorkaround + "\n")
            ctrlFile.write(item.user + "\n")
            #ctrlFile.write(item.password + "\n")
            ctrlFile.write(item.repositoryURL + "\n")
            ctrlFile.write(item.originalVMEIdForUploadOrXMLURIForDownload + "\n")
            ctrlFile.write(item.vmeHasLinkForUploadOrSRBDataURIForDownload + "\n")
            ctrlFile.write(item.isLastResourceToDownload + "\n")
            ctrlFile.write(item.binaryCall + "\n")
        ctrlFile.close()
        return
    
    def SaveFailedDownload(self,failed_item):
        self.ClearPendingDownloads()
        self.__Lock()
        ctrlFile = open(self.failure_filename,"a")
        # Write all parameters

        #ctrlFile.write(failed_item.serverHost + "\n")
        #ctrlFile.write(failed_item.serverPort + "\n")
        #ctrlFile.write(failed_item.transferModality + "\n")
        ctrlFile.write(failed_item.cacheVMEIdForUploadOrSizeForDownload + "\n")
        ctrlFile.write(failed_item.currentCacheChildABSFolderWorkaround + "\n")
        ctrlFile.write(failed_item.user + "\n")
        #ctrlFile.write(failed_item.password + "\n")
        ctrlFile.write(failed_item.repositoryURL + "\n")
        ctrlFile.write(failed_item.originalVMEIdForUploadOrXMLURIForDownload + "\n")
        ctrlFile.write(failed_item.vmeHasLinkForUploadOrSRBDataURIForDownload + "\n")
        ctrlFile.write(failed_item.isLastResourceToDownload + "\n")
        ctrlFile.write(failed_item.binaryCall + "\n")
        ctrlFile.flush()
        ctrlFile.close()
        self.__Unlock()
        return
    
    def __SaveClientCallStack(self):
        self.ClearPendingDownloads()
        ctrlFile = open(self.filename,"w")
        # Write all parameters
        for item in self.clientCallStack:
            #ctrlFile.write(item.serverHost + "\n")
            #ctrlFile.write(item.serverPort + "\n")
            #ctrlFile.write(item.transferModality + "\n")
            ctrlFile.write(item.cacheVMEIdForUploadOrSizeForDownload + "\n")
            ctrlFile.write(item.currentCacheChildABSFolderWorkaround + "\n")
            ctrlFile.write(item.user + "\n")
            #ctrlFile.write(item.password + "\n")
            ctrlFile.write(item.repositoryURL + "\n")
            ctrlFile.write(item.originalVMEIdForUploadOrXMLURIForDownload + "\n")
            ctrlFile.write(item.vmeHasLinkForUploadOrSRBDataURIForDownload + "\n")
            ctrlFile.write(item.isLastResourceToDownload + "\n")
            ctrlFile.write(item.binaryCall + "\n")
        ctrlFile.close()
    
    def RemoveDownloadCall(self,cacheDir,pop_item,removeVmeWithNoBinary=True):
        # Lock the file
        while(self.__IsLocked()):
            time.sleep(1) # wait one second
            pass
        
        self.__Lock()

        self.__LoadClientCallStack()

        nClientCallStack = list()
        #wx.MessageBox("Starting download check","Information",wx.ICON_INFORMATION)
        for item in self.clientCallStack:
            #wx.MessageBox("confronto\n%s" % cacheDir + "\n%s" % item.currentCacheChildABSFolderWorkaround +"\nrisultato %d" % item.currentCacheChildABSFolderWorkaround.find(cacheDir),"Information",wx.ICON_INFORMATION)
            if(cacheDir.find(item.currentCacheChildABSFolderWorkaround) == -1 and (removeVmeWithNoBinary==False or item.vmeHasLinkForUploadOrSRBDataURIForDownload != ".")):
                nClientCallStack.append(item)
                #wx.MessageBox("%s" % item.vmeHasLinkForUploadOrSRBDataURIForDownload,"Information",wx.ICON_INFORMATION)
            #elif((removeVmeWithNoBinary==False or item.vmeHasLinkForUploadOrSRBDataURIForDownload != ".")):
            elif((item.vmeHasLinkForUploadOrSRBDataURIForDownload != ".")):
                pop_item.cacheVMEIdForUploadOrSizeForDownload = item.cacheVMEIdForUploadOrSizeForDownload
                pop_item.currentCacheChildABSFolderWorkaround = item.currentCacheChildABSFolderWorkaround
                pop_item.user = item.user
                pop_item.repositoryURL = item.repositoryURL
                pop_item.originalVMEIdForUploadOrXMLURIForDownload = item.originalVMEIdForUploadOrXMLURIForDownload
                pop_item.vmeHasLinkForUploadOrSRBDataURIForDownload = item.vmeHasLinkForUploadOrSRBDataURIForDownload
                pop_item.isLastResourceToDownload = item.isLastResourceToDownload
                pop_item.binaryCall = item.binaryCall
        # Unlock the file
        self.__Unlock()
        del self.clientCallStack
        self.clientCallStack = nClientCallStack
        self.__SaveClientCallStack()
        #self.app.Exit() 
        #return pop_item

    
    def __Lock(self):
        try:
            f = open("_lock.lhp","w")
            f.close()
        except:
            return
    
    def __IsLocked(self):
        return (os.path.exists("_lock.lhp"))
    
    def __Unlock(self):
        #if(self.__IsLocked()==True):
        try:
            os.remove("_lock.lhp")
        except:
            pass
    
    def GetNumberOfPendingDownloads(self):
        while(self.__IsLocked()):
            time.sleep(1) # wait one second
            pass
        self.__Lock()
        try:
            ctrlFile = open(self.filename,"r")
            count = 0
            for line in ctrlFile:
                count = count + 1
            ctrlFile.close()
            self.__Unlock()
            return count/8
        except:
            self.__Unlock()
            return 0
        
                 
    def GetNumberOfFailedDownload(self):
        while(self.__IsLocked()):
            time.sleep(1) # wait one second
            pass
        self.__Lock()
        try:
            ctrlFile = open(self.failure_filename,"r")
            count = 0
            for line in ctrlFile:
                count = count + 1
            ctrlFile.close()
            self.__Unlock()
            return count/8
        except:
            self.__Unlock()
            return 0
    
    def SetFailureAsCallStack(self):
        while(self.__IsLocked()):
            time.sleep(1) # wait one second
            pass
        if(self.GetNumberOfFailedDownload() > 0):
            self.ClearPendingDownloads()
            self.__Lock()
            os.rename(self.failure_filename,"DownloadCallStack2.txt")
            self.__Unlock()
        else:
            self.__Unlock()
            return
        
    def ClearPendingDownloads(self):
        while(self.__IsLocked()):
            time.sleep(1) # wait one second
            pass
        self.__Lock()
        if(os.path.exists(self.filename)):
            while (1):
                try:
                    os.remove(self.filename)
                    break
                except:
                    pass
        self.__Unlock()
        
def main():
    import sys
    downloadChecker = DownloadIntegrityChecker()
    #print downloadChecker.GetNumberOfFailedDownload()
    pop_item = ClientDownloadStruct()
    downloadChecker.RemoveDownloadCall(sys.argv[1],pop_item)
    downloadChecker.GetNumberOfPendingDownloads()
    #downloadChecker.SetFailureAsCallStack()

if __name__ == '__main__':
        main()
        
"""