import wx
import sys

from webServicesClient import xmlrpcDemoWS
import xml.dom.minidom as xd
import os, time
from lhpDefines import *
from Debug import Debug
import ConfigLoader
 
########################################################################
class Group:
    """"""
 
    #----------------------------------------------------------------------
    def __init__(self, id, title):
        """Constructor"""
        self.id = id
        self.title = title
        
########################################################################
class ListGroups(wx.Frame):
 
    #----------------------------------------------------------------------
    def __init__(self):
        self.IdList = [] #create original list of basket vmes
        self.IdListSelected = [] #this list will be copied from handle object
        self.fileName = "groupId.txt"
        self.currentPassword = ''
        self.proxyHost = ""
        self.proxyPort = 0
        pass
    
    def SetCredentials(self, user , password):
        self.currentUser = user
        self.currentPassword = password
         
    def getGroupsList(self):
        self.removeSelectedGroupFile()
        
        self.proxyHost, self.proxyPort = retriveProxyParameters()
        
        cl = ConfigLoader.ConfigLoader("",self.currentUser)
        ws = xmlrpcDemoWS.xmlrpc_demoWS()
        
        ws.setServer(cl.ServiceURL)
        ws.setCredentials(self.currentUser, self.currentPassword)
        ws.ProxyURL = self.proxyHost
        ws.ProxyPort = self.proxyPort


        if Debug:
            print "->"+ ws.ProxyURL + "<-"
            print "->"+ str(ws.ProxyPort) + "<-"
        
        try:
            self.Result = ws.run('listgroups')[1]
                
        except Exception, e:
            print "ERROR %s" % str(e)
        self.__createListFromReultingXML()
        print self.IdList
        pass
    
    def __createListFromReultingXML(self):
        #add element to self.IdList
        dom = xd.parseString(self.Result)
        if dom.getElementsByTagName("fault"):
            print "-----Error in listgroups service---------"
            return
        for el in dom.getElementsByTagName("string"):
            for node in el.childNodes:  
                self.IdList.append(node.data)
        pass
    
    def removeSelectedGroupFile(self):
        if(os.path.exists(self.fileName)):
            os.remove(self.fileName)
        pass
 
# Run the program
def test():
    lb = ListGroups()
    lb.SetCredentials("testuser", "6w8DHF")
    lb.getGroupsList()
    print "Id List: " + str(lb.IdList)

if __name__ == '__main__':
  test()

