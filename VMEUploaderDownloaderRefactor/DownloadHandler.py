import vmeDownloader
from webServicesClient import WSDownload
import os, sys, string, time, re ,shutil
import hashlib
import msvcrt
import xml.dom.minidom as xd
from stat import ST_SIZE 
import threading, thread, CustomThread
from lhpDefines import *
from Debug import Debug
import wx
import time
import ConfigLoader

class DownloadHandler:
    queue = None
    def __init__(self, queue, observer, dirCache, srbData, usr , pwd, urlServer, isLast, fileSize):
        DownloadHandler.queue = queue
        self.observer = observer
        self.dirCache = dirCache
        self.srbData = srbData
        self.currentUser = usr
        self.currentPassword = pwd
        self.urlServer = urlServer
        self.fileSize = fileSize
        self.isLast = isLast
        self.block = threading.Lock()
        self.localChksum = ""
        self.remoteChksum = "UNDEFINED"
        self.proxyHost = ""
        self.proxyPort = 0
        self.downloadFinished = False
        pass
    
    def __download(self):
      
        self.proxyHost, self.proxyPort = retriveProxyParameters()
        
        cl = ConfigLoader.ConfigLoader("","")
        serviceUrl = cl.WebServicesURL + '/WSDownload.cgi'
       
        oldDir = os.getcwd()
        if(self.dirCache != None):
           os.chdir(self.dirCache)
        if Debug:
            print os.getcwd()


        # serviceUrl = 'http://wsdevel.biomedtown.org/WSDownload.cgi'
        mtomD = WSDownload.WSDownload()

        if Debug:
            print "->"+ self.proxyHost + "<-"
            print "->"+ str(self.proxyPort) + "<-"
         
        try:
            if Debug:
                print "Calling mtom download webservice WSDownload.cgi inside thread"

            mtomD.Download(self.srbData,serviceUrl,self.proxyHost,self.proxyPort)
            if Debug:
                print "Inside Download Thread"
        
        except:
            if Debug:
                print "-------Error calling WSDownload.cgi----------" 
        
            self.block.acquire()
            if(DownloadHandler.queue):
                percentage = 120 #120 for 'Error!'           
                lista = [self.observer,percentage]
                DownloadHandler.queue.put(lista)
            self.block.release()
            return
        
        if Debug:
                print "Successful call to mtom download webservice mafSRBDownload.cgi inside thread"

        self.downloadFinished = True
        os.chdir(oldDir)    
        
        pass
    
    
    def GetCurrentLocalFileDimensionDuringDownload(self):
        localFileName = self.dirCache+self.srbData
        if(os.path.exists(localFileName)):
            return os.stat(localFileName).st_size
        else:
            if Debug:
                print localFileName , " not found"
            return 0
        
    
    def moveFileInMSFDirectory(self):
            if Debug:
                print "Move file in current MSF dir"
            #rename file, reading information by conf file
            try:
              file = open(self.dirCache+"configuration.conf","r")
            except:
              if Debug:
                  print "------Unable to Open ConfigurationFile-----------"
              return
            
            fullPathInMSF = file.read()
            file.close()
            if Debug:
                print "move " + self.dirCache+self.srbData + " in " + fullPathInMSF
            
            if(os.path.exists(fullPathInMSF)):
                os.remove(fullPathInMSF)
                
            pos = fullPathInMSF.rfind('/')
            binaryname = fullPathInMSF[pos:]
            os.rename(self.dirCache+self.srbData, self.dirCache+binaryname)
            pass
        
    def download(self):
        if Debug:
            print "DownloadHandler inside download:"
        
        if (self.srbData == "."):
             fileTransferStatus = 49984
                      
             self.block.acquire()
         
             lista = [self.observer,fileTransferStatus]
             DownloadHandler.queue.put(lista)
             self.block.release()
             return
         
        if(self.srbData != "."):
            thread.start_new_thread(self.__download,()) #here start download thread
            progress = 0
            previousTime = time.time()
            previousFileDimension = 0
            totalDataDownloaded = 0
            totalTime = 0
            lastSpeed = 0

            while(1):
              try:
                currentFileDimension = self.GetCurrentLocalFileDimensionDuringDownload()
              except:
                currentFileDimension = 0
              if(self.fileSize!="0"):
                progress = 95 * float(currentFileDimension)/float(self.fileSize)
              currentTime = time.time()
              elapsedTime = currentTime - previousTime
              transferredDimension = currentFileDimension-previousFileDimension
              totalDataDownloaded = currentFileDimension                 
              totalTime = totalTime + elapsedTime
              remainingTime = "unknown"
              remainingData = float(self.fileSize) - float(totalDataDownloaded)
              
              if elapsedTime != 0:
                 lastSpeed = transferredDimension / elapsedTime 
           
              previousTime = currentTime
              previousFileDimension = currentFileDimension
                
              if lastSpeed != 0:
                 remainingTime = remainingData / lastSpeed                     
              
              if Debug:
                  print  ""
                  print  "########################################"
                  print "in time: "  + str(elapsedTime) 
                  
                  print "transferred: " + str(transferredDimension)
                  print ""
                  
                  print "percent transferred:" + str(progress)  + "%"
                  
                  print "data transferred: " + str(totalDataDownloaded) 
                  
                  print "in total time: " + str(totalTime)
                                    
                  print str(remainingData) + " remaining"
                  
                  print ""
                  
                  if elapsedTime != 0:
                    print "last speed: " + str(lastSpeed)
                  else:
                    print "last speed: unknown"  
                  
                
                  if lastSpeed != 0:
                     print "remaining time: " + str(remainingTime)                                          
                  else:
                     print "remaining time: unknown"
                     
                  print  "########################################"
                              
              if(DownloadHandler.queue):
                  # 91988: downloading...
                  fileTransferStatus = 91988
                  gauge_FileTransferStatus_RemainingTimeInSeconds_Progress = \
                  [self.observer, fileTransferStatus, remainingTime, progress]             
                  self.block.acquire()
                  DownloadHandler.queue.put(gauge_FileTransferStatus_RemainingTimeInSeconds_Progress)
                  self.block.release()
      
                  
              if(self.downloadFinished == True):
                  break
              
              time.sleep(0.5)
            
            if Debug:
                print str(self.GetCurrentLocalFileDimensionDuringDownload())

            #-----MD5 check-point-----#
            self.localChksum = self.md5(self.dirCache+self.srbData)
            if Debug:
                print "Local checksum=  " + self.localChksum
            
            try:
                self.remoteChksum = self.retrieveTagValue('L0000_resource_data_Dataset_LocalFileCheckSum')
            
            except:
                if Debug:
                    print "------------Remote Checksum not found------------"
                self.block.acquire()
                if(DownloadHandler.queue):
                    fileTransferStatus = 120 #110 for 'Error!'
                    lista = [self.observer,fileTransferStatus]
                    DownloadHandler.queue.put(lista)
                self.block.release()
                #self.remoteChksum = "extdata"
            
            print self.remoteChksum
            self.remoteChksum = self.remoteChksum.lower()
            
            if Debug:
                print "Remote checksum= " + self.remoteChksum
            
            if (self.localChksum == self.remoteChksum):
                if Debug:
                    print " "
                    print "MD5 checksum control successful!"
                    print " "
                
                if(DownloadHandler.queue):
                    self.block.acquire()
                    # 99986: MD5 successful
                    fileTransferStatus = 99986
                    lista = [self.observer,fileTransferStatus]
                    DownloadHandler.queue.put(lista)
                    self.block.release()
                    self.moveFileInMSFDirectory()
   
            else:
                if Debug:
                    print " "
                    print "Error: MD5 checksum control unsuccessful!"
                    print "self.localChksum: " , self.localChksum
                    print "self.remoteChksum: " , self.remoteChksum
                    print " "
                    
                if(DownloadHandler.queue):
                    self.block.acquire()
                    fileTransferStatus = 39876 #Download Checksum Failed
                    lista = [self.observer,fileTransferStatus]
                    DownloadHandler.queue.put(lista)
                    self.block.release()                      
                return 
        else: 
            if(DownloadHandler.queue):
                self.block.acquire()
                # 91345: srbdata = .???
                fileTransferStatus = 91345
                lista = [self.observer,fileTransferStatus]
                DownloadHandler.queue.put(lista)
                self.block.release() 
                
        if Debug:
            print "IsLast: " + str(self.isLast)
        if (self.isLast == "false"):
            #replace status for resource correctly downloaded
            if(os.path.exists(sys.path[0] + '\\status.txt')):
                  while 1:
                        size = os.path.getsize(sys.path[0] + '\\status.txt')
                        statusFile = open(sys.path[0] + '\\status.txt', 'a')
                        try:
                            msvcrt.locking(statusFile.fileno(), msvcrt.LK_RLCK, size)
                            statusFile.write('ended\n')
                            statusFile.close()  
                            break                                      
                        except:
                            counter = counter+1 #to avoid deadlock
                            statusFile.close()
                            if(counter == 10):
                                if Debug:
                                  print "----------Can not read in status.lhp-----------"
                                pass
                        
        else:
          if(os.path.exists(sys.path[0] + '\\status.txt')):
              while 1:
                    size = os.path.getsize(sys.path[0] + '\\status.txt')
                    statusFile = open(sys.path[0] + '\\status.txt', 'a')
                    try:
                        msvcrt.locking(statusFile.fileno(), msvcrt.LK_RLCK, size)
                        statusFile.write('lastEnded\n')
                        statusFile.close()  
                        break                                      
                    except:
                        counter = counter+1 #to avoid deadlock
                        statusFile.close()
                        if(counter == 10):
                            if Debug:
                              print "----------Can not read in status.lhp-----------"
                            pass
        
        processStarted = 0
        processEnded = 0
        startedCount = 0
        endedCount = 0
        lastStartedCount = 0
        lastEndedCount = 0
        lastStarted = False
        size = os.path.getsize(sys.path[0] + '\\status.txt')
        statusFile = open(sys.path[0] + '\\status.txt', 'r')
        try:
            msvcrt.locking(statusFile.fileno(), msvcrt.LK_RLCK, size)
            for line in statusFile:
                if line == "started\n":
                    startedCount += 1
                    if processStarted == 0:
                        processStarted += 1
                    if lastStarted == True:
                        processStarted += 1
                        #lastEnded = False
                        lastStarted = False
                elif line == "ended\n":
                    endedCount +=1   
                elif line == "lastStarted\n":
                    lastStarted = True
                    lastStartedCount += 1
                    if processStarted == 0:
                        processStarted += 1
                elif line == "lastEnded\n":
                    #lastEnded = True
                    lastEndedCount +=1
                    processEnded += 1
                  
                  
            statusFile.close()  
            
            if Debug:     
                print "processStarted :" + str(processStarted)     
                print "processEnded :" + str(processEnded)     
                print "startedCount :" + str(startedCount)     
                print "endedCount :" + str(endedCount)     
                print "lastStartedCount :" + str(lastStartedCount)     
                print "lastEndedCount :" + str(lastEndedCount)       
            allEnded = False
            if processStarted == processEnded and startedCount == endedCount and lastStartedCount == lastEndedCount:
                allEnded = True

        except:
            counter = counter+1 #to avoid deadlock
            statusFile.close()
            if(counter == 10):
                if Debug:
                      print "----------Can not read in status.txt-----------"
                pass                    
        if allEnded == True:
            self.block.acquire()  
            fileTransferStatus = 130 #130 for 'ALL COMPLETE!'
            lista = [self.observer,fileTransferStatus]
            DownloadHandler.queue.put(lista)
            self.block.release()
            if(os.path.exists(sys.path[0] + '\\status.txt')):
                os.remove(sys.path[0] + '\\status.txt')
                if Debug:
                    print "status.txt removed"
    
        
    def md5(self,fileName):
        #Compute md5 hash of the specified file
        m = hashlib.md5()
        try:
            fd = open(fileName,"rb")
            fd.close()
        except IOError:
            if Debug:
                print str("Unable to open the file" , fileName , " in readmode:")
            return
        for eachLine in open(fileName,"rb"):
            m.update(eachLine)
        return m.hexdigest()
    
    def retrieveTagValue(self, tag):
        returnValue = ''
        fileName = self.dirCache + 'outputMAF.msf'
        try:
           file = open(fileName, 'r')
        except :
            if Debug:
                
                print str("Unable to open the file in readmode:", fileName)
            
            return str("Unable to open the file " , fileName , "in readmode")
            
        dom = xd.parse(file)
        if dom.getElementsByTagName("fault"):
                return       
        for el in dom.getElementsByTagName('TItem'):
           if(el.attributes != None and el.attributes.get('Name')):
              attrNode = el.attributes.get('Name')
              attrValue = attrNode.nodeValue
              if(attrValue == tag):
                  childToFind = el.getElementsByTagName('TC')[0] # first element of childList
                  returnValue = childToFind.firstChild.nodeValue
        file.close()
        
        return returnValue


                      
def createDownloadHandler(queue, observer, dirCache, srbData, usr , pwd, urlServer, isLast, fileSize):
    downloadHandler = DownloadHandler(queue, observer, dirCache, srbData, usr, pwd, urlServer, isLast, fileSize)
    downloadHandler.download()
    
if __name__ == '__main__':
  createDownloadHandler(None, None, None, None, None, None, None, None)
