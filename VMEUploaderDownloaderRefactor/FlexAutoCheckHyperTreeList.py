# --------------------------------------------------------------------------------- #
# FLEXAUTOCHECKHYPERTREELIST wxPython IMPLEMENTATION
# Derived by wx.lib.agw.HyperTreeList
#
# This class uses FlexAutoCheckTreeCtrl instead of CustomTreeCtrl to enable flexible subtree autochecking
#
# Eleonora Mambrini, @ 17 August 2010
#
# --------------------------------------------------------------------------------- #

import wx

import HyperTreeList as HTL

from HyperTreeList import TreeListHeaderWindow
from HyperTreeList import TreeListItem
from HyperTreeList import EditTextCtrl
from HyperTreeList import (TREE_HITTEST_ONITEMCHECKICON)

from customtreectrl import TreeRenameTimer as TreeListRenameTimer
from FlexAutoCheckTreeCtrl import FlexAutoCheckTreeCtrl

from HyperTreeList import TR_VIRTUAL
from customtreectrl import DragImage, TreeEvent
#from customtreectrl import (TR_AUTO_CHECK_CHILD, TR_AUTO_CHECK_PARENT, TR_AUTO_TOGGLE_CHILD, wxEVT_TREE_ITEM_CHECKING, wxEVT_TREE_ITEM_CHECKED)


# --------------------------------------------------------------------------
# Constants
# --------------------------------------------------------------------------

_NO_IMAGE = -1

_DEFAULT_COL_WIDTH = 100
_LINEHEIGHT = 10
_LINEATROOT = 5
_MARGIN = 2
_MININDENT = 16
_BTNWIDTH = 9
_BTNHEIGHT = 9
_EXTRA_WIDTH = 4
_EXTRA_HEIGHT = 4

_MAX_WIDTH = 30000  # pixels; used by OnPaint to redraw only exposed items

_DRAG_TIMER_TICKS = 250   # minimum drag wait time in ms
_FIND_TIMER_TICKS = 500   # minimum find wait time in ms
_RENAME_TIMER_TICKS = 250 # minimum rename wait time in ms

# ---------------------------------------------------------------------------
# FlexAutoCheckTreeListMainWindow implementation
# ---------------------------------------------------------------------------
class FlexAutoCheckTreeListMainWindow(FlexAutoCheckTreeCtrl):
    """
    This class represents the main window (and thus the main column) in L{HyperTreeList}.

    :note: This is a subclass of L{CustomTreeCtrl}.
    """

    def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=0, agwStyle=wx.TR_DEFAULT_STYLE, validator=wx.DefaultValidator,
                 name="wxtreelistmainwindow"):
        """
        Default class constructor.
        
        :param `parent`: parent window. Must not be ``None``;
        :param `id`: window identifier. A value of -1 indicates a default value;
        :param `pos`: the control position. A value of (-1, -1) indicates a default position,
         chosen by either the windowing system or wxPython, depending on platform;
        :param `size`: the control size. A value of (-1, -1) indicates a default size,
         chosen by either the windowing system or wxPython, depending on platform;
        :param `style`: the underlying `wx.PyScrolledWindow` style;
        :param `agwStyle`: the AGW-specific L{TreeListMainWindow} window style. This can be a
         combination of the following bits:
        
         ============================== =========== ==================================================
         Window Styles                  Hex Value   Description
         ============================== =========== ==================================================
         ``TR_NO_BUTTONS``                      0x0 For convenience to document that no buttons are to be drawn.
         ``TR_SINGLE``                          0x0 For convenience to document that only one item may be selected at a time. Selecting another item causes the current selection, if any, to be deselected. This is the default.
         ``TR_HAS_BUTTONS``                     0x1 Use this style to show + and - buttons to the left of parent items.
         ``TR_NO_LINES``                        0x4 Use this style to hide vertical level connectors.
         ``TR_LINES_AT_ROOT``                   0x8 Use this style to show lines between root nodes. Only applicable if ``TR_HIDE_ROOT`` is set and ``TR_NO_LINES`` is not set.
         ``TR_DEFAULT_STYLE``                   0x9 The set of flags that are closest to the defaults for the native control for a particular toolkit.
         ``TR_TWIST_BUTTONS``                  0x10 Use old Mac-twist style buttons.
         ``TR_MULTIPLE``                       0x20 Use this style to allow a range of items to be selected. If a second range is selected, the current range, if any, is deselected.
         ``TR_EXTENDED``                       0x40 Use this style to allow disjoint items to be selected. (Only partially implemented; may not work in all cases).
         ``TR_HAS_VARIABLE_ROW_HEIGHT``        0x80 Use this style to cause row heights to be just big enough to fit the content. If not set, all rows use the largest row height. The default is that this flag is unset.
         ``TR_EDIT_LABELS``                   0x200 Use this style if you wish the user to be able to edit labels in the tree control.
         ``TR_ROW_LINES``                     0x400 Use this style to draw a contrasting border between displayed rows.
         ``TR_HIDE_ROOT``                     0x800 Use this style to suppress the display of the root node, effectively causing the first-level nodes to appear as a series of root nodes.
         ``TR_FULL_ROW_HIGHLIGHT``           0x2000 Use this style to have the background colour and the selection highlight extend  over the entire horizontal row of the tree control window.
         ``TR_AUTO_CHECK_CHILD``             0x4000 Only meaningful foe checkbox-type items: when a parent item is checked/unchecked its children are checked/unchecked as well.
         ``TR_AUTO_TOGGLE_CHILD``            0x8000 Only meaningful foe checkbox-type items: when a parent item is checked/unchecked its children are toggled accordingly.
         ``TR_AUTO_CHECK_PARENT``           0x10000 Only meaningful foe checkbox-type items: when a child item is checked/unchecked its parent item is checked/unchecked as well.
         ``TR_ALIGN_WINDOWS``               0x20000 Flag used to align windows (in items with windows) at the same horizontal position.
         ============================== =========== ==================================================

        :param `validator`: window validator;
        :param `name`: window name.
        """

        FlexAutoCheckTreeCtrl.__init__(self, parent, id, pos, size, style, agwStyle, validator, name)
        
        self._shiftItem = None
        self._editItem = None
        self._selectItem = None

        self._curColumn = -1 # no current column
        self._owner = parent
        self._main_column = 0
        self._dragItem = None

        self._imgWidth = self._imgWidth2 = 0
        self._imgHeight = self._imgHeight2 = 0
        self._btnWidth = self._btnWidth2 = 0
        self._btnHeight = self._btnHeight2 = 0
        self._checkWidth = self._checkWidth2 = 0
        self._checkHeight = self._checkHeight2 = 0
        self._agwStyle = agwStyle
        self._current = None

        # TextCtrl initial settings for editable items
        self._renameTimer = TreeListRenameTimer(self)
        self._left_down_selection = False

        self._dragTimer = wx.Timer(self)
        self._findTimer = wx.Timer(self)
        
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_MOUSE_EVENTS, self.OnMouse)

        # Listen for EVT_SCROLLWIN in a separate event handler so that the
        # default handler can be called without entering an infinite loop.
        # See OnScroll for why calling the default handler manually is needed.
        # Store the default handler in _default_evt_handler.
        scroll_evt_handler = wx.EvtHandler()
        self.PushEventHandler(scroll_evt_handler)
        scroll_evt_handler.Bind(wx.EVT_SCROLLWIN, self.OnScroll)
        self._default_evt_handler = scroll_evt_handler.GetNextHandler()

        # Sets the focus to ourselves: this is useful if you have items
        # with associated widgets.
        self.SetFocus()
        self.SetBackgroundStyle(wx.BG_STYLE_CUSTOM)


    def SetBuffered(self, buffered):
        """
        Sets/unsets the double buffering for the main window.

        :param `buffered`: ``True`` to use double-buffering, ``False`` otherwise.

        :note: Currently we are using double-buffering only on Windows XP.
        """

        self._buffered = buffered
        if buffered:
            self.SetBackgroundStyle(wx.BG_STYLE_CUSTOM)
        else:
            self.SetBackgroundStyle(wx.BG_STYLE_SYSTEM)


    def IsVirtual(self):
        """ Returns ``True`` if L{TreeListMainWindow} has the ``TR_VIRTUAL`` flag set. """
        
        return self.HasAGWFlag(TR_VIRTUAL)


#-----------------------------------------------------------------------------
# functions to work with tree items
#-----------------------------------------------------------------------------

    def GetItemImage(self, item, column=None, which=wx.TreeItemIcon_Normal):
        """
        Returns the item image.

        :param `item`: an instance of L{TreeListItem};
        :param `column`: if not ``None``, an integer specifying the column index.
         If it is ``None``, the main column index is used;
        :param `which`: can be one of the following bits:

         ================================= ========================
         Item State                        Description
         ================================= ========================
         ``TreeItemIcon_Normal``           To get the normal item image
         ``TreeItemIcon_Selected``         To get the selected item image (i.e. the image which is shown when the item is currently selected)
         ``TreeItemIcon_Expanded``         To get the expanded image (this only makes sense for items which have children - then this image is shown when the item is expanded and the normal image is shown when it is collapsed)
         ``TreeItemIcon_SelectedExpanded`` To get the selected expanded image (which is shown when an expanded item is currently selected) 
         ================================= ========================
        """
        
        column = (column is not None and [column] or [self._main_column])[0]

        if column < 0:
            return _NO_IMAGE

        return item.GetImage(which, column)


    def SetItemImage(self, item, image, column=None, which=wx.TreeItemIcon_Normal):
        """
        Sets the item image for a particular item state.

        :param `item`: an instance of L{TreeListItem};
        :param `image`: an index within the normal image list specifying the image to use;
        :param `column`: if not ``None``, an integer specifying the column index.
         If it is ``None``, the main column index is used;
        :param `which`: the item state.

        :see: L{GetItemImage} for a list of valid item states.
        """
        
        column = (column is not None and [column] or [self._main_column])[0]

        if column < 0:
            return
        
        item.SetImage(column, image, which)
        dc = wx.ClientDC(self)
        self.CalculateSize(item, dc)
        self.RefreshLine(item)


    def GetItemWindowEnabled(self, item, column=None):
        """
        Returns whether the window associated with an item is enabled or not.

        :param `item`: an instance of L{TreeListItem};
        :param `column`: if not ``None``, an integer specifying the column index.
         If it is ``None``, the main column index is used.
        """

        return item.GetWindowEnabled(column)


    def GetItemWindow(self, item, column=None):
        """
        Returns the window associated with an item.

        :param `item`: an instance of L{TreeListItem};
        :param `column`: if not ``None``, an integer specifying the column index.
         If it is ``None``, the main column index is used.
        """
        
        return item.GetWindow(column)


    def SetItemWindow(self, item, window, column=None):
        """
        Sets the window associated to an item.

        :param `item`: an instance of L{TreeListItem};
        :param `wnd`: a non-toplevel window to be displayed next to the item;
        :param `column`: if not ``None``, an integer specifying the column index.
         If it is ``None``, the main column index is used.

        :note: The window parent should not be the L{HyperTreeList} itself, but actually
         an instance of L{TreeListMainWindow}. The current solution here is to reparent
         the window to this class.
        """

        # Reparent the window to ourselves
        if window.GetParent() != self:
            window.Reparent(self)
        
        item.SetWindow(window, column)
        if window:
            self._hasWindows = True
        

    def SetItemWindowEnabled(self, item, enable=True, column=None):
        """
        Sets whether the window associated with an item is enabled or not.

        :param `item`: an instance of L{TreeListItem};
        :param `enable`: ``True`` to enable the associated window, ``False`` to disable it;
        :param `column`: if not ``None``, an integer specifying the column index.
         If it is ``None``, the main column index is used.
        """

        item.SetWindowEnabled(enable, column)


# ----------------------------------------------------------------------------
# navigation
# ----------------------------------------------------------------------------

    def IsItemVisible(self, item):
        """
        Returns whether the item is visible or not.

        :param `item`: an instance of L{TreeListItem};
        """

        # An item is only visible if it's not a descendant of a collapsed item
        parent = item.GetParent()

        while parent:
        
            if not parent.IsExpanded():
                return False
            
            parent = parent.GetParent()
        
        startX, startY = self.GetViewStart()
        clientSize = self.GetClientSize()

        rect = self.GetBoundingRect(item)
        
        if not rect:
            return False
        if rect.GetWidth() == 0 or rect.GetHeight() == 0:
            return False
        if rect.GetBottom() < 0 or rect.GetTop() > clientSize.y:
            return False
        if rect.GetRight() < 0 or rect.GetLeft() > clientSize.x:
            return False

        return True


    def GetPrevChild(self, item, cookie):
        """
        Returns the previous child of an item.

        :param `item`: an instance of L{TreeListItem};
        :param `cookie`: a parameter which is opaque for the application but is necessary
         for the library to make these functions reentrant (i.e. allow more than one
         enumeration on one and the same object simultaneously).

        :note: This method returns ``None`` if there are no further siblings.
        """

        children = item.GetChildren()

        if cookie >= 0:            
            return children[cookie], cookie-1
        else:        
            # there are no more of them
            return None, cookie


    def GetFirstExpandedItem(self):
        """ Returns the first item which is in the expanded state. """

        return self.GetNextExpanded(self.GetRootItem())


    def GetNextExpanded(self, item):
        """
        Returns the next expanded item after the input one.

        :param `item`: an instance of L{TreeListItem}.
        """                

        return self.GetNext(item, False)


    def GetPrevExpanded(self, item):
        """
        Returns the previous expanded item before the input one.

        :param `item`: an instance of L{TreeListItem}.
        """                

        return self.GetPrev(item, False)


    def GetFirstVisibleItem(self):
        """ Returns the first visible item. """

        return self.GetNextVisible(self.GetRootItem())


    def GetPrevVisible(self, item):
        """
        Returns the previous visible item before the input one.

        :param `item`: an instance of L{TreeListItem}.
        """                

        i = self.GetNext(item, False)
        while i:
            if self.IsItemVisible(i):
                return i
            i = self.GetPrev(i, False)
        
        return None


# ----------------------------------------------------------------------------
# operations
# ----------------------------------------------------------------------------

    def DoInsertItem(self, parent, previous, text, ct_type=0, wnd=None, image=-1, selImage=-1, data=None):
        """
        Actually inserts an item in the tree.

        :param `parentId`: an instance of L{TreeListItem} representing the
         item's parent;
        :param `previous`: the index at which we should insert the item;
        :param `text`: the item text label;
        :param `ct_type`: the item type (see L{CustomTreeCtrl.SetItemType} for a list of valid
         item types);
        :param `wnd`: if not ``None``, a non-toplevel window to show next to the item;
        :param `image`: an index within the normal image list specifying the image to
         use for the item in unselected state;
        :param `selImage`: an index within the normal image list specifying the image to
         use for the item in selected state; if `image` > -1 and `selImage` is -1, the
         same image is used for both selected and unselected items;
        :param `data`: associate the given Python object `data` with the item.
        """
        
        self._dirty = True # do this first so stuff below doesn't cause flicker
        arr = [""]*self.GetColumnCount()
        arr[self._main_column] = text
        
        if not parent:
            # should we give a warning here?
            return self.AddRoot(text, ct_type, wnd, image, selImage, data)
        
        self._dirty = True     # do this first so stuff below doesn't cause flicker

        item = TreeListItem(self, parent, arr, ct_type, wnd, image, selImage, data)
        
        if wnd is not None:
            self._hasWindows = True
            self._itemWithWindow.append(item)
        
        parent.Insert(item, previous)

        return item


    def AddRoot(self, text, ct_type=0, wnd=None, image=-1, selImage=-1, data=None):
        """
        Adds a root item to the L{TreeListMainWindow}.

        :param `text`: the item text label;
        :param `ct_type`: the item type (see L{CustomTreeCtrl.SetItemType} for a list of valid
         item types);
        :param `wnd`: if not ``None``, a non-toplevel window to show next to the item;
        :param `image`: an index within the normal image list specifying the image to
         use for the item in unselected state;
        :param `selImage`: an index within the normal image list specifying the image to
         use for the item in selected state; if `image` > -1 and `selImage` is -1, the
         same image is used for both selected and unselected items;
        :param `data`: associate the given Python object `data` with the item.

        :warning: only one root is allowed to exist in any given instance of L{TreeListMainWindow}.
        """        

        if self._anchor:
            raise Exception("\nERROR: Tree Can Have Only One Root")

        if wnd is not None and not (self._agwStyle & wx.TR_HAS_VARIABLE_ROW_HEIGHT):
            raise Exception("\nERROR: In Order To Append/Insert Controls You Have To Use The Style TR_HAS_VARIABLE_ROW_HEIGHT")

        if text.find("\n") >= 0 and not (self._agwStyle & wx.TR_HAS_VARIABLE_ROW_HEIGHT):
            raise Exception("\nERROR: In Order To Append/Insert A MultiLine Text You Have To Use The Style TR_HAS_VARIABLE_ROW_HEIGHT")

        if ct_type < 0 or ct_type > 2:
            raise Exception("\nERROR: Item Type Should Be 0 (Normal), 1 (CheckBox) or 2 (RadioButton). ")

        self._dirty = True     # do this first so stuff below doesn't cause flicker
        arr = [""]*self.GetColumnCount()
        arr[self._main_column] = text
        self._anchor = TreeListItem(self, None, arr, ct_type, wnd, image, selImage, data)
        
        if wnd is not None:
            self._hasWindows = True
            self._itemWithWindow.append(self._anchor)            
        
        if self.HasAGWFlag(wx.TR_HIDE_ROOT):
            # if root is hidden, make sure we can navigate
            # into children
            self._anchor.SetHasPlus()
            self._anchor.Expand()
            self.CalculatePositions()
        
        if not self.HasAGWFlag(wx.TR_MULTIPLE):
            self._current = self._key_current = self._selectItem = self._anchor
            self._current.SetHilight(True)
        
        return self._anchor


    def Delete(self, item):
        """
        Deletes an item.

        :param `item`: an instance of L{TreeListItem}.
        """

        if not item:
            raise Exception("\nERROR: Invalid Tree Item. ")
        
        self._dirty = True     # do this first so stuff below doesn't cause flicker

        if self._textCtrl != None and self.IsDescendantOf(item, self._textCtrl.item()):
            # can't delete the item being edited, cancel editing it first
            self._textCtrl.StopEditing()

        # don't stay with invalid self._shiftItem or we will crash in the next call to OnChar()
        changeKeyCurrent = False
        itemKey = self._shiftItem
        
        while itemKey:
            if itemKey == item:  # self._shiftItem is a descendant of the item being deleted
                changeKeyCurrent = True
                break
            
            itemKey = itemKey.GetParent()
        
        parent = item.GetParent()
        if parent:
            parent.GetChildren().remove(item)  # remove by value
        
        if changeKeyCurrent:
            self._shiftItem = parent

        self.SendDeleteEvent(item)
        if self._selectItem == item:
            self._selectItem = None

        # Remove the item with window
        if item in self._itemWithWindow:
            for wnd in item._wnd:
                if wnd:
                    wnd.Hide()
                    wnd.Destroy()
                
            item._wnd = []
            self._itemWithWindow.remove(item)
            
        item.DeleteChildren(self)
        del item


    # Don't leave edit or selection on a child which is about to disappear
    def ChildrenClosing(self, item):
        """
        We are about to destroy the item's children.

        :param `item`: an instance of L{TreeListItem}.
        """

        if self._textCtrl != None and item != self._textCtrl.item() and self.IsDescendantOf(item, self._textCtrl.item()):
            self._textCtrl.StopEditing()

        if self.IsDescendantOf(item, self._selectItem):
            self._selectItem = item
            
        if item != self._current and self.IsDescendantOf(item, self._current):
            self._current.SetHilight(False)
            self._current = None

            
    def DeleteRoot(self):
        """
        Removes the tree root item (and subsequently all the items in
        L{TreeListMainWindow}.
        """

        if self._anchor:

            self._dirty = True
            self.SendDeleteEvent(self._anchor)
            self._current = None
            self._selectItem = None
            self._anchor.DeleteChildren(self)
            del self._anchor
            self._anchor = None


    def DeleteAllItems(self):
        """ Delete all items in the L{TreeListMainWindow}. """

        self.DeleteRoot()
        

    def HideWindows(self):
        """ Hides the windows associated to the items. Used internally. """
        
        for child in self._itemWithWindow:
            if not self.IsItemVisible(child):
                for column in xrange(self.GetColumnCount()):
                    wnd = child.GetWindow(column)
                    if wnd and wnd.IsShown():
                        wnd.Hide()
                

    def EnableItem(self, item, enable=True, torefresh=True):
        """
        Enables/disables an item.

        :param `item`: an instance of L{TreeListItem};
        :param `enable`: ``True`` to enable the item, ``False`` otherwise;
        :param `torefresh`: whether to redraw the item or not.
        """
        
        if item.IsEnabled() == enable:
            return

        if not enable and item.IsSelected():
            self.DoSelectItem(item, not self.HasAGWFlag(wx.TR_MULTIPLE))

        item.Enable(enable)

        for column in xrange(self.GetColumnCount()):
            wnd = item.GetWindow(column)

            # Handles the eventual window associated to the item        
            if wnd:
                wnd.Enable(enable)
        
        if torefresh:
            # We have to refresh the item line
            dc = wx.ClientDC(self)
            self.CalculateSize(item, dc)
            self.RefreshLine(item)


    def IsItemEnabled(self, item):
        """
        Returns whether an item is enabled or disabled.

        :param `item`: an instance of L{TreeListItem}.
        """

        return item.IsEnabled()
    

    def GetCurrentItem(self):
        """ Returns the current item. """

        return self._current

    
    def GetColumnCount(self):
        """ Returns the total number of columns. """

        return self._owner.GetHeaderWindow().GetColumnCount()


    def SetMainColumn(self, column):
        """
        Sets the L{HyperTreeList} main column (i.e. the position of the underlying
        L{CustomTreeCtrl}.

        :param `column`: if not ``None``, an integer specifying the column index.
         If it is ``None``, the main column index is used.
        """
        
        if column >= 0 and column < self.GetColumnCount():
            self._main_column = column


    def GetMainColumn(self):
        """
        Returns the L{HyperTreeList} main column (i.e. the position of the underlying
        L{CustomTreeCtrl}.
        """
        
        return self._main_column
    

    def ScrollTo(self, item):
        """
        Scrolls the specified item into view.

        :param `item`: an instance of L{TreeListItem}.
        """

        # ensure that the position of the item it calculated in any case
        if self._dirty:
            self.CalculatePositions()

        # now scroll to the item
        xUnit, yUnit = self.GetScrollPixelsPerUnit()
        start_x, start_y = self.GetViewStart()
        start_y *= yUnit
        client_w, client_h = self.GetClientSize ()

        x, y = self._anchor.GetSize (0, 0, self)
        x = self._owner.GetHeaderWindow().GetWidth()
        y += yUnit + 2 # one more scrollbar unit + 2 pixels
        x_pos = self.GetScrollPos(wx.HORIZONTAL)

        if item._y < start_y+3:
            # going down, item should appear at top
            self.SetScrollbars(xUnit, yUnit, (xUnit and [x/xUnit] or [0])[0], (yUnit and [y/yUnit] or [0])[0],
                               x_pos, (yUnit and [item._y/yUnit] or [0])[0])
            
        elif item._y+self.GetLineHeight(item) > start_y+client_h:
            # going up, item should appear at bottom
            item._y += yUnit + 2
            self.SetScrollbars(xUnit, yUnit, (xUnit and [x/xUnit] or [0])[0], (yUnit and [y/yUnit] or [0])[0],
                               x_pos, (yUnit and [(item._y+self.GetLineHeight(item)-client_h)/yUnit] or [0])[0])
        

    def SetDragItem(self, item):
        """
        Sets the specified item as member of a current drag and drop operation.

        :param `item`: an instance of L{TreeListItem}.
        """

        prevItem = self._dragItem
        self._dragItem = item
        if prevItem:
            self.RefreshLine(prevItem)
        if self._dragItem:
            self.RefreshLine(self._dragItem)


# ----------------------------------------------------------------------------
# helpers
# ----------------------------------------------------------------------------

    def AdjustMyScrollbars(self):
        """ Internal method used to adjust the `wx.PyScrolledWindow` scrollbars. """

        if self._anchor:
            xUnit, yUnit = self.GetScrollPixelsPerUnit()
            if xUnit == 0:
                xUnit = self.GetCharWidth()
            if yUnit == 0:
                yUnit = self._lineHeight

            x, y = self._anchor.GetSize(0, 0, self)
            y += yUnit + 2 # one more scrollbar unit + 2 pixels
            x_pos = self.GetScrollPos(wx.HORIZONTAL)
            y_pos = self.GetScrollPos(wx.VERTICAL)
            x = self._owner.GetHeaderWindow().GetWidth() + 2
            if x < self.GetClientSize().GetWidth():
                x_pos = 0

            self.SetScrollbars(xUnit, yUnit, x/xUnit, y/yUnit, x_pos, y_pos)
        else:
            self.SetScrollbars(0, 0, 0, 0)
    

    def PaintItem(self, item, dc):
        """
        Actually draws an item.

        :param `item`: an instance of L{TreeListItem};
        :param `dc`: an instance of `wx.DC`.
        """

        def _paintText(text, textrect, alignment):
            """
            Sub-function to draw multi-lines text label aligned correctly.

            :param `text`: the item text label (possibly multiline);
            :param `textrect`: the label client rectangle;
            :param `alignment`: the alignment for the text label, one of ``wx.ALIGN_LEFT``,
             ``wx.ALIGN_RIGHT``, ``wx.ALIGN_CENTER``.
            """
            
            txt = text.splitlines()
            if alignment != wx.ALIGN_LEFT and len(txt):
                yorigin = textrect.Y
                for t in txt:
                    w, h = dc.GetTextExtent(t)
                    plus = textrect.Width - w
                    if alignment == wx.ALIGN_CENTER:
                        plus /= 2
                    dc.DrawLabel(t, wx.Rect(textrect.X + plus, yorigin, w, yorigin+h))
                    yorigin += h
                return
            dc.DrawLabel(text, textrect)
        
        attr = item.GetAttributes()
        
        if attr and attr.HasFont():
            dc.SetFont(attr.GetFont())
        elif item.IsBold():
            dc.SetFont(self._boldFont)
        if item.IsHyperText():
            dc.SetFont(self.GetHyperTextFont())
            if item.GetVisited():
                dc.SetTextForeground(self.GetHyperTextVisitedColour())
            else:
                dc.SetTextForeground(self.GetHyperTextNewColour())

        colText = wx.Colour(*dc.GetTextForeground())
        
        if item.IsSelected():
            if (wx.Platform == "__WXMAC__" and self._hasFocus):
                colTextHilight = wx.SystemSettings_GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT)
            else:
                colTextHilight = wx.SystemSettings_GetColour(wx.SYS_COLOUR_HIGHLIGHTTEXT)

        else:
            attr = item.GetAttributes()
            if attr and attr.HasTextColour():
                colText = attr.GetTextColour()
            
        if self._vistaselection:
            colText = colTextHilight = wx.BLACK
                
        total_w = self._owner.GetHeaderWindow().GetWidth()
        total_h = self.GetLineHeight(item)
        off_h = (self.HasAGWFlag(wx.TR_ROW_LINES) and [1] or [0])[0]
        off_w = (self.HasAGWFlag(wx.TR_COLUMN_LINES) and [1] or [0])[0]
##        clipper = wx.DCClipper(dc, 0, item.GetY(), total_w, total_h) # only within line

        text_w, text_h, dummy = dc.GetMultiLineTextExtent(item.GetText(self.GetMainColumn()))

        drawItemBackground = False
        # determine background and show it
        if attr and attr.HasBackgroundColour():
            colBg = attr.GetBackgroundColour()
            drawItemBackground = True
        else:
            colBg = self._backgroundColour
        
        dc.SetBrush(wx.Brush(colBg, wx.SOLID))
        dc.SetPen(wx.TRANSPARENT_PEN)

        if self.HasAGWFlag(wx.TR_FULL_ROW_HIGHLIGHT):

            itemrect = wx.Rect(0, item.GetY() + off_h, total_w-1, total_h - off_h)
            
            if item == self._dragItem:
                dc.SetBrush(self._hilightBrush)
                if wx.Platform == "__WXMAC__":
                    dc.SetPen((item == self._dragItem) and [wx.BLACK_PEN] or [wx.TRANSPARENT_PEN])[0]

                dc.SetTextForeground(colTextHilight)

            elif item.IsSelected():

                wnd = item.GetWindow(self._main_column)
                wndx = 0
                if wnd:
                    wndx, wndy = item.GetWindowSize(self._main_column)

                itemrect = wx.Rect(0, item.GetY() + off_h, total_w-1, total_h - off_h)
                
                if self._usegradients:
                    if self._gradientstyle == 0:   # Horizontal
                        self.DrawHorizontalGradient(dc, itemrect, self._hasFocus)
                    else:                          # Vertical
                        self.DrawVerticalGradient(dc, itemrect, self._hasFocus)
                elif self._vistaselection:
                    self.DrawVistaRectangle(dc, itemrect, self._hasFocus)
                else:
                    if wx.Platform in ["__WXGTK2__", "__WXMAC__"]:
                        flags = wx.CONTROL_SELECTED
                        if self._hasFocus: flags = flags | wx.CONTROL_FOCUSED
                        wx.RendererNative.Get().DrawItemSelectionRect(self._owner, dc, itemrect, flags) 
                    else:
                        dc.SetBrush((self._hasFocus and [self._hilightBrush] or [self._hilightUnfocusedBrush])[0])
                        dc.SetPen((self._hasFocus and [self._borderPen] or [wx.TRANSPARENT_PEN])[0])
                        dc.DrawRectangleRect(itemrect)
                
                dc.SetTextForeground(colTextHilight)

            # On GTK+ 2, drawing a 'normal' background is wrong for themes that
            # don't allow backgrounds to be customized. Not drawing the background,
            # except for custom item backgrounds, works for both kinds of theme.
            elif drawItemBackground:

                itemrect = wx.Rect(0, item.GetY() + off_h, total_w-1, total_h - off_h)
                dc.SetBrush(wx.Brush(colBg, wx.SOLID))
                dc.DrawRectangleRect(itemrect)
                dc.SetTextForeground(colText)
                                                
            else:
                dc.SetTextForeground(colText)

        else:
            
            dc.SetTextForeground(colText)

        text_extraH = (total_h > text_h and [(total_h - text_h)/2] or [0])[0]
        img_extraH = (total_h > self._imgHeight and [(total_h-self._imgHeight)/2] or [0])[0]
        x_colstart = 0
        
        for i in xrange(self.GetColumnCount()):
            if not self._owner.GetHeaderWindow().IsColumnShown(i):
                continue

            col_w = self._owner.GetHeaderWindow().GetColumnWidth(i)
            dc.SetClippingRegion(x_colstart, item.GetY(), col_w, total_h) # only within column

            image = _NO_IMAGE
            x = image_w = wcheck = hcheck = 0

            if i == self.GetMainColumn():
                x = item.GetX() + _MARGIN
                if self.HasButtons():
                    x += (self._btnWidth-self._btnWidth2) + _LINEATROOT
                else:
                    x -= self._indent/2
                
                if self._imageListNormal:
                    image = item.GetCurrentImage(i)
                    
                if item.GetType() != 0 and self._imageListCheck:
                    checkimage = item.GetCurrentCheckedImage()
                    wcheck, hcheck = self._imageListCheck.GetSize(item.GetType())
                else:
                    wcheck, hcheck = 0, 0
            
            else:
                x = x_colstart + _MARGIN
                image = item.GetImage(column=i)
                
            if image != _NO_IMAGE:
                image_w = self._imgWidth + _MARGIN

            # honor text alignment
            text = item.GetText(i)
            alignment = self._owner.GetHeaderWindow().GetColumn(i).GetAlignment()

            text_w, dummy, dummy = dc.GetMultiLineTextExtent(text)

            if alignment == wx.ALIGN_RIGHT:
                w = col_w - (image_w + wcheck + text_w + off_w + _MARGIN + 1)
                x += (w > 0 and [w] or [0])[0]

            elif alignment == wx.ALIGN_CENTER:
                w = (col_w - (image_w + wcheck + text_w + off_w + _MARGIN))/2
                x += (w > 0 and [w] or [0])[0]
            
            text_x = x + image_w + wcheck + 1
            
            if i == self.GetMainColumn():
                item.SetTextX(text_x)

            if not self.HasAGWFlag(wx.TR_FULL_ROW_HIGHLIGHT):
                dc.SetBrush((self._hasFocus and [self._hilightBrush] or [self._hilightUnfocusedBrush])[0])
                dc.SetPen((self._hasFocus and [self._borderPen] or [wx.TRANSPARENT_PEN])[0])
                if i == self.GetMainColumn():
                    if item == self._dragItem:
                        if wx.Platform == "__WXMAC__":  # don't draw rect outline if we already have the background colour
                            dc.SetPen((item == self._dragItem and [wx.BLACK_PEN] or [wx.TRANSPARENT_PEN])[0])

                        dc.SetTextForeground(colTextHilight)
                        
                    elif item.IsSelected():

                        itemrect = wx.Rect(text_x-2, item.GetY() + off_h, text_w+2*_MARGIN, total_h - off_h)

                        if self._usegradients:
                            if self._gradientstyle == 0:   # Horizontal
                                self.DrawHorizontalGradient(dc, itemrect, self._hasFocus)
                            else:                          # Vertical
                                self.DrawVerticalGradient(dc, itemrect, self._hasFocus)
                        elif self._vistaselection:
                            self.DrawVistaRectangle(dc, itemrect, self._hasFocus)
                        else:
                            if wx.Platform in ["__WXGTK2__", "__WXMAC__"]:
                                flags = wx.CONTROL_SELECTED
                                if self._hasFocus: flags = flags | wx.CONTROL_FOCUSED
                                wx.RendererNative.Get().DrawItemSelectionRect(self._owner, dc, itemrect, flags) 
                            else:
                                dc.DrawRectangleRect(itemrect)

                        dc.SetTextForeground(colTextHilight)

                    elif item == self._current:
                        dc.SetPen((self._hasFocus and [wx.BLACK_PEN] or [wx.TRANSPARENT_PEN])[0])
                    
                    # On GTK+ 2, drawing a 'normal' background is wrong for themes that
                    # don't allow backgrounds to be customized. Not drawing the background,
                    # except for custom item backgrounds, works for both kinds of theme.
                    elif drawItemBackground:

                        itemrect = wx.Rect(text_x-2, item.GetY() + off_h, text_w+2*_MARGIN, total_h - off_h)
                        dc.SetBrush(wx.Brush(colBg, wx.SOLID))
                        dc.DrawRectangleRect(itemrect)

                    else:
                        dc.SetTextForeground(colText)
    
                else:
                    dc.SetTextForeground(colText)
                
            if self.HasAGWFlag(wx.TR_COLUMN_LINES):  # vertical lines between columns
                pen = wx.Pen(wx.SystemSettings_GetColour(wx.SYS_COLOUR_3DLIGHT), 1, wx.SOLID)
                dc.SetPen((self.GetBackgroundColour() == wx.WHITE and [pen] or [wx.WHITE_PEN])[0])
                dc.DrawLine(x_colstart+col_w-1, item.GetY(), x_colstart+col_w-1, item.GetY()+total_h)
            
            dc.SetBackgroundMode(wx.TRANSPARENT)

            if image != _NO_IMAGE:
                y = item.GetY() + img_extraH
                if wcheck:
                    x += wcheck

                if item.IsEnabled():
                    imglist = self._imageListNormal
                else:
                    imglist = self._grayedImageList
                
                imglist.Draw(image, dc, x, y, wx.IMAGELIST_DRAW_TRANSPARENT)

            if wcheck:
                if item.IsEnabled():
                    imglist = self._imageListCheck
                else:
                    imglist = self._grayedCheckList

                if self.HasButtons():  # should the item show a button?
                    btnWidth = self._btnWidth
                else:
                    btnWidth = -self._btnWidth
                
                imglist.Draw(checkimage, dc,
                             item.GetX() + btnWidth + _MARGIN,
                             item.GetY() + ((total_h > hcheck) and [(total_h-hcheck)/2] or [0])[0]+1,
                             wx.IMAGELIST_DRAW_TRANSPARENT)

            text_w, text_h, dummy = dc.GetMultiLineTextExtent(text)
            text_extraH = (total_h > text_h and [(total_h - text_h)/2] or [0])[0]            
            text_y = item.GetY() + text_extraH
            textrect = wx.Rect(text_x, text_y, text_w, text_h)
        
            if not item.IsEnabled():
                foreground = dc.GetTextForeground()
                dc.SetTextForeground(self._disabledColour)
                _paintText(text, textrect, alignment)
                dc.SetTextForeground(foreground)
            else:
                if wx.Platform == "__WXMAC__" and item.IsSelected() and self._hasFocus:
                    dc.SetTextForeground(wx.WHITE)
                _paintText(text, textrect, alignment)

            wnd = item.GetWindow(i)            
            if wnd:
                if text_w == 0:
                    wndx = text_x
                else:
                    wndx = text_x + text_w + 2*_MARGIN
                xa, ya = self.CalcScrolledPosition((0, item.GetY()))
                wndx += xa
                if item.GetHeight() > item.GetWindowSize(i)[1]:
                    ya += (item.GetHeight() - item.GetWindowSize(i)[1])/2
                    
                if not wnd.IsShown():
                    wnd.Show()
                if wnd.GetPosition() != (wndx, ya):
                    wnd.SetPosition((wndx, ya))                
            
            x_colstart += col_w
            dc.DestroyClippingRegion()
        
        # restore normal font
        dc.SetFont(self._normalFont)


    # Now y stands for the top of the item, whereas it used to stand for middle !
    def PaintLevel(self, item, dc, level, y, x_maincol):
        """
        Paint a level in the hierarchy of L{TreeListMainWindow}.

        :param `item`: an instance of L{TreeListItem};
        :param `dc`: an instance of `wx.DC`;
        :param `level`: the item level in the tree hierarchy;
        :param `y`: the current vertical position in the `wx.PyScrolledWindow`;
        :param `x_maincol`: the horizontal position of the main column.
        """

        if item.IsHidden():
            return y, x_maincol
        
        # Handle hide root (only level 0)
        if self.HasAGWFlag(wx.TR_HIDE_ROOT) and level == 0:
            for child in item.GetChildren():
                y, x_maincol = self.PaintLevel(child, dc, 1, y, x_maincol)
            
            # end after expanding root
            return y, x_maincol
        
        # calculate position of vertical lines
        x = x_maincol + _MARGIN # start of column

        if self.HasAGWFlag(wx.TR_LINES_AT_ROOT):
            x += _LINEATROOT # space for lines at root
            
        if self.HasButtons():
            x += (self._btnWidth-self._btnWidth2) # half button space
        else:
            x += (self._indent-self._indent/2)
        
        if self.HasAGWFlag(wx.TR_HIDE_ROOT):
            x += self._indent*(level-1) # indent but not level 1
        else:
            x += self._indent*level # indent according to level
        
        # set position of vertical line
        item.SetX(x)
        item.SetY(y)

        h = self.GetLineHeight(item)
        y_top = y
        y_mid = y_top + (h/2)
        y += h

        exposed_x = dc.LogicalToDeviceX(0)
        exposed_y = dc.LogicalToDeviceY(y_top)

        # horizontal lines between rows?
        draw_row_lines = self.HasAGWFlag(wx.TR_ROW_LINES)

        if self.IsExposed(exposed_x, exposed_y, _MAX_WIDTH, h + draw_row_lines):
            if draw_row_lines:
                total_width = self._owner.GetHeaderWindow().GetWidth()
                # if the background colour is white, choose a
                # contrasting colour for the lines
                pen = wx.Pen(wx.SystemSettings_GetColour(wx.SYS_COLOUR_3DLIGHT), 1, wx.SOLID)
                dc.SetPen((self.GetBackgroundColour() == wx.WHITE and [pen] or [wx.WHITE_PEN])[0])
                dc.DrawLine(0, y_top, total_width, y_top)
                dc.DrawLine(0, y_top+h, total_width, y_top+h)
            
            # draw item
            self.PaintItem(item, dc)

            # restore DC objects
            dc.SetBrush(wx.WHITE_BRUSH)
            dc.SetPen(self._dottedPen)

            # clip to the column width
            clip_width = self._owner.GetHeaderWindow().GetColumn(self._main_column).GetWidth()
##            clipper = wx.DCClipper(dc, x_maincol, y_top, clip_width, 10000)

            if not self.HasAGWFlag(wx.TR_NO_LINES):  # connection lines

                # draw the horizontal line here
                dc.SetPen(self._dottedPen)
                x2 = x - self._indent
                if x2 < (x_maincol + _MARGIN):
                    x2 = x_maincol + _MARGIN
                x3 = x + (self._btnWidth-self._btnWidth2)
                if self.HasButtons():
                    if item.HasPlus():
                        dc.DrawLine(x2, y_mid, x - self._btnWidth2, y_mid)
                        dc.DrawLine(x3, y_mid, x3 + _LINEATROOT, y_mid)
                    else:
                        dc.DrawLine(x2, y_mid, x3 + _LINEATROOT, y_mid)
                else:
                    dc.DrawLine(x2, y_mid, x - self._indent/2, y_mid)
                
            if item.HasPlus() and self.HasButtons():  # should the item show a button?
                
                if self._imageListButtons:

                    # draw the image button here
                    image = wx.TreeItemIcon_Normal
                    if item.IsExpanded():
                        image = wx.TreeItemIcon_Expanded
                    if item.IsSelected():
                        image += wx.TreeItemIcon_Selected - wx.TreeItemIcon_Normal
                    xx = x - self._btnWidth2 + _MARGIN
                    yy = y_mid - self._btnHeight2
                    dc.SetClippingRegion(xx, yy, self._btnWidth, self._btnHeight)
                    self._imageListButtons.Draw(image, dc, xx, yy, wx.IMAGELIST_DRAW_TRANSPARENT)
                    dc.DestroyClippingRegion()

                elif self.HasAGWFlag(wx.TR_TWIST_BUTTONS):

                    # draw the twisty button here
                    dc.SetPen(wx.BLACK_PEN)
                    dc.SetBrush(self._hilightBrush)
                    button = [wx.Point() for j in xrange(3)]
                    if item.IsExpanded():
                        button[0].x = x - (self._btnWidth2+1)
                        button[0].y = y_mid - (self._btnHeight/3)
                        button[1].x = x + (self._btnWidth2+1)
                        button[1].y = button[0].y
                        button[2].x = x
                        button[2].y = button[0].y + (self._btnHeight2+1)
                    else:
                        button[0].x = x - (self._btnWidth/3)
                        button[0].y = y_mid - (self._btnHeight2+1)
                        button[1].x = button[0].x
                        button[1].y = y_mid + (self._btnHeight2+1)
                        button[2].x = button[0].x + (self._btnWidth2+1)
                        button[2].y = y_mid
                    
                    dc.DrawPolygon(button)

                else: # if (HasAGWFlag(wxTR_HAS_BUTTONS))

                    rect = wx.Rect(x-self._btnWidth2, y_mid-self._btnHeight2, self._btnWidth, self._btnHeight)
                    flag = (item.IsExpanded() and [wx.CONTROL_EXPANDED] or [0])[0]
                    wx.RendererNative.GetDefault().DrawTreeItemButton(self, dc, rect, flag)        

        # restore DC objects
        dc.SetBrush(wx.WHITE_BRUSH)
        dc.SetPen(self._dottedPen)
        dc.SetTextForeground(wx.BLACK)

        if item.IsExpanded():

            # process lower levels
            if self._imgWidth > 0:
                oldY = y_mid + self._imgHeight2
            else:
                oldY = y_mid + h/2
            
            for child in item.GetChildren():

                y, x_maincol = self.PaintLevel(child, dc, level+1, y, x_maincol)

                # draw vertical line
                if not self.HasAGWFlag(wx.TR_NO_LINES):
                    Y1 = child.GetY() + child.GetHeight()/2
                    dc.DrawLine(x, oldY, x, Y1)

        return y, x_maincol        


# ----------------------------------------------------------------------------
# wxWindows callbacks
# ----------------------------------------------------------------------------

    def OnEraseBackground(self, event):
        """
        Handles the ``wx.EVT_ERASE_BACKGROUND`` event for L{TreeListMainWindow}.

        :param `event`: a `wx.EraseEvent` event to be processed.
        """

        # do not paint the background separately in buffered mode.
        if not self._buffered:
            FlexAutoCheckTreeCtrl.OnEraseBackground(self, event)


    def OnPaint(self, event):
        """
        Handles the ``wx.EVT_PAINT`` event for L{TreeListMainWindow}.

        :param `event`: a `wx.PaintEvent` event to be processed.
        """

        if self._buffered:

            # paint the background
            dc = wx.BufferedPaintDC(self)
            rect = self.GetUpdateRegion().GetBox()
            dc.SetClippingRect(rect)
            dc.SetBackground(wx.Brush(self.GetBackgroundColour()))
            if self._backgroundImage:
                self.TileBackground(dc)
            else:
                dc.Clear()

        else:
            dc = wx.PaintDC(self)

        self.PrepareDC(dc)

        if not self._anchor or self.GetColumnCount() <= 0:
            return

        # calculate button size
        if self._imageListButtons:
            self._btnWidth, self._btnHeight = self._imageListButtons.GetSize(0)
        elif self.HasButtons():
            self._btnWidth = _BTNWIDTH
            self._btnHeight = _BTNHEIGHT
        
        self._btnWidth2 = self._btnWidth/2
        self._btnHeight2 = self._btnHeight/2

        # calculate image size
        if self._imageListNormal:
            self._imgWidth, self._imgHeight = self._imageListNormal.GetSize(0)
        
        self._imgWidth2 = self._imgWidth/2
        self._imgHeight2 = self._imgHeight/2

        if self._imageListCheck:
            self._checkWidth, self._checkHeight = self._imageListCheck.GetSize(0)

        self._checkWidth2 = self._checkWidth/2
        self._checkHeight2 = self._checkHeight/2
            
        # calculate indent size
        if self._imageListButtons:
            self._indent = max(_MININDENT, self._btnWidth + _MARGIN)
        elif self.HasButtons():
            self._indent = max(_MININDENT, self._btnWidth + _LINEATROOT)
        
        # set default values
        dc.SetFont(self._normalFont)
        dc.SetPen(self._dottedPen)

        # calculate column start and paint
        x_maincol = 0
        for i in xrange(self.GetMainColumn()):
            if not self._owner.GetHeaderWindow().IsColumnShown(i):
                continue
            x_maincol += self._owner.GetHeaderWindow().GetColumnWidth(i)
        
        y, x_maincol = self.PaintLevel(self._anchor, dc, 0, 0, x_maincol)


    def HitTest(self, point, flags=0):
        """
        Calculates which (if any) item is under the given point, returning the tree item
        at this point plus extra information flags plus the item's column.

        :param `point`: an instance of `wx.Point`, a point to test for hits;
        :param `flags`: a bitlist of the following values:

         ================================== =============== =================================
         HitTest Flags                      Hex Value       Description
         ================================== =============== =================================
         ``TREE_HITTEST_ABOVE``                         0x1 Above the client area
         ``TREE_HITTEST_BELOW``                         0x2 Below the client area
         ``TREE_HITTEST_NOWHERE``                       0x4 No item has been hit
         ``TREE_HITTEST_ONITEMBUTTON``                  0x8 On the button associated to an item
         ``TREE_HITTEST_ONITEMICON``                   0x10 On the icon associated to an item
         ``TREE_HITTEST_ONITEMINDENT``                 0x20 On the indent associated to an item
         ``TREE_HITTEST_ONITEMLABEL``                  0x40 On the label (string) associated to an item
         ``TREE_HITTEST_ONITEM``                       0x50 Anywhere on the item
         ``TREE_HITTEST_ONITEMRIGHT``                  0x80 On the right of the label associated to an item
         ``TREE_HITTEST_TOLEFT``                      0x200 On the left of the client area
         ``TREE_HITTEST_TORIGHT``                     0x400 On the right of the client area
         ``TREE_HITTEST_ONITEMUPPERPART``             0x800 On the upper part (first half) of the item
         ``TREE_HITTEST_ONITEMLOWERPART``            0x1000 On the lower part (second half) of the item
         ``TREE_HITTEST_ONITEMCHECKICON``            0x2000 On the check/radio icon, if present
         ================================== =============== =================================

        :note: the item (if any, ``None`` otherwise), the `flags` and the column are always
         returned as a tuple.
        """

        w, h = self.GetSize()
        column = -1

        if not isinstance(point, wx.Point):
            point = wx.Point(*point)

        if point.x < 0:
            flags |= wx.TREE_HITTEST_TOLEFT
        if point.x > w:
            flags |= wx.TREE_HITTEST_TORIGHT
        if point.y < 0:
            flags |= wx.TREE_HITTEST_ABOVE
        if point.y > h:
            flags |= wx.TREE_HITTEST_BELOW
        if flags:
            return None, flags, column

        if not self._anchor:
            flags = wx.TREE_HITTEST_NOWHERE
            column = -1
            return None, flags, column
        
        hit, flags, column = self._anchor.HitTest(self.CalcUnscrolledPosition(point), self, flags, column, 0)
        if not hit:
            flags = wx.TREE_HITTEST_NOWHERE
            column = -1
            return None, flags, column
        
        return hit, flags, column


    def EditLabel(self, item, column=None):
        """
        Starts editing an item label.

        :param `item`: an instance of L{TreeListItem};
        :param `column`: if not ``None``, an integer specifying the column index.
         If it is ``None``, the main column index is used.        
        """

        if not item:
            return

        column = (column is not None and [column] or [self._main_column])[0]

        if column < 0 or column >= self.GetColumnCount():
            return

        self._editItem = item

        te = TreeEvent(wx.wxEVT_COMMAND_TREE_BEGIN_LABEL_EDIT, self._owner.GetId())
        te.SetItem(self._editItem)
        te.SetInt(column)
        te.SetEventObject(self._owner)
        self._owner.GetEventHandler().ProcessEvent(te)

        if not te.IsAllowed():
            return

        # ensure that the position of the item it calculated in any case
        if self._dirty:
            self.CalculatePositions()

        header_win = self._owner.GetHeaderWindow()
        alignment = header_win.GetColumnAlignment(column)
        if alignment == wx.ALIGN_LEFT:
            style = wx.TE_LEFT
        elif alignment == wx.ALIGN_RIGHT:
            style = wx.TE_RIGHT
        elif alignment == wx.ALIGN_CENTER:
            style = wx.TE_CENTER
            
        if self._textCtrl != None and (item != self._textCtrl.item() or column != self._textCtrl.column()):
            self._textCtrl.StopEditing()
            
        self._textCtrl = EditTextCtrl(self, -1, self._editItem, column,
                                      self, self._editItem.GetText(column),
                                      style=style|wx.TE_PROCESS_ENTER)
        self._textCtrl.SetFocus()


    def OnRenameTimer(self):
        """ The timer for renaming has expired. Start editing. """

        self.EditLabel(self._current, self._curColumn)


    def OnRenameAccept(self, value):
        """
        Called by L{EditTextCtrl}, to accept the changes and to send the
        ``EVT_TREE_END_LABEL_EDIT`` event.

        :param `value`: the new value of the item label.        
        """

        # TODO if the validator fails this causes a crash
        le = TreeEvent(wx.wxEVT_COMMAND_TREE_END_LABEL_EDIT, self._owner.GetId())
        le.SetItem(self._editItem)
        le.SetEventObject(self._owner)
        le.SetLabel(value)
        le._editCancelled = False
        self._owner.GetEventHandler().ProcessEvent(le)

        if not le.IsAllowed():
            return

        if self._curColumn == -1:
            self._curColumn = 0
           
        self.SetItemText(self._editItem, value, self._curColumn)


    def OnRenameCancelled(self):
        """
        Called by L{EditTextCtrl}, to cancel the changes and to send the
        ``EVT_TREE_END_LABEL_EDIT`` event.
        """

        # let owner know that the edit was cancelled
        le = TreeEvent(wx.wxEVT_COMMAND_TREE_END_LABEL_EDIT, self._owner.GetId())
        le.SetItem(self._editItem)
        le.SetEventObject(self._owner)
        le.SetLabel("")
        le._editCancelled = True

        self._owner.GetEventHandler().ProcessEvent(le)

    
    def OnMouse(self, event):
        """
        Handles the ``wx.EVT_MOUSE_EVENTS`` event for L{TreeListMainWindow}.

        :param `event`: a `wx.MouseEvent` event to be processed.
        """

        if not self._anchor:
            return

        # we process left mouse up event (enables in-place edit), right down
        # (pass to the user code), left dbl click (activate item) and
        # dragging/moving events for items drag-and-drop
        if not (event.LeftDown() or event.LeftUp() or event.RightDown() or \
                event.RightUp() or event.LeftDClick() or event.Dragging() or \
                event.GetWheelRotation() != 0 or event.Moving()):
            self._owner.GetEventHandler().ProcessEvent(event)
            return
        

        # set focus if window clicked
        if event.LeftDown() or event.RightDown():
            self._hasFocus = True
            self.SetFocusIgnoringChildren()

        # determine event
        p = wx.Point(event.GetX(), event.GetY())
        flags = 0
        item, flags, column = self._anchor.HitTest(self.CalcUnscrolledPosition(p), self, flags, self._curColumn, 0)

        underMouse = item
        underMouseChanged = underMouse != self._underMouse

        if underMouse and (flags & wx.TREE_HITTEST_ONITEM) and not event.LeftIsDown() and \
           not self._isDragging and (not self._renameTimer or not self._renameTimer.IsRunning()):
            underMouse = underMouse
        else:
            underMouse = None

        if underMouse != self._underMouse:
            if self._underMouse:
                # unhighlight old item
                self._underMouse = None
             
            self._underMouse = underMouse

        # Determines what item we are hovering over and need a tooltip for
        hoverItem = item

        if (event.LeftDown() or event.LeftUp() or event.RightDown() or \
            event.RightUp() or event.LeftDClick() or event.Dragging()):
            if self._textCtrl != None and item != self._textCtrl.item():
                self._textCtrl.StopEditing()

        # We do not want a tooltip if we are dragging, or if the rename timer is running
        if underMouseChanged and not self._isDragging and (not self._renameTimer or not self._renameTimer.IsRunning()):
            
            if hoverItem is not None:
                # Ask the tree control what tooltip (if any) should be shown
                hevent = TreeEvent(wx.wxEVT_COMMAND_TREE_ITEM_GETTOOLTIP, self.GetId())
                hevent.SetItem(hoverItem)
                hevent.SetEventObject(self)

                if self.GetEventHandler().ProcessEvent(hevent) and hevent.IsAllowed():
                    self.SetToolTip(hevent._label)

                if hoverItem.IsHyperText() and (flags & wx.TREE_HITTEST_ONITEMLABEL) and hoverItem.IsEnabled():
                    self.SetCursor(wx.StockCursor(wx.CURSOR_HAND))
                    self._isonhyperlink = True
                else:
                    if self._isonhyperlink:
                        self.SetCursor(wx.StockCursor(wx.CURSOR_ARROW))
                        self._isonhyperlink = False
                        
        # we only process dragging here
        if event.Dragging():
            
            if self._isDragging:
                if not self._dragImage:
                    # Create the custom draw image from the icons and the text of the item                    
                    self._dragImage = DragImage(self, self._current or item)
                    self._dragImage.BeginDrag(wx.Point(0,0), self)
                    self._dragImage.Show()

                self._dragImage.Move(p)

                if self._countDrag == 0 and item:
                    self._oldItem = self._current
                    self._oldSelection = self._current

                if item != self._dropTarget:
                        
                    # unhighlight the previous drop target
                    if self._dropTarget:
                        self._dropTarget.SetHilight(False)
                        self.RefreshLine(self._dropTarget)
                    if item:
                        item.SetHilight(True)
                        self.RefreshLine(item)
                        self._countDrag = self._countDrag + 1
                    self._dropTarget = item

                    self.Update()

                if self._countDrag >= 3 and self._oldItem is not None:
                    # Here I am trying to avoid ugly repainting problems... hope it works
                    self.RefreshLine(self._oldItem)
                    self._countDrag = 0
                    
                return # nothing to do, already done

            if item == None:
                return # we need an item to dragging

            # determine drag start
            if self._dragCount == 0:
                self._dragTimer.Start(_DRAG_TIMER_TICKS, wx.TIMER_ONE_SHOT)
            
            self._dragCount += 1
            if self._dragCount < 3:
                return # minimum drag 3 pixel
            if self._dragTimer.IsRunning():
                return

            # we're going to drag
            self._dragCount = 0

            # send drag start event
            command = (event.LeftIsDown() and [wx.wxEVT_COMMAND_TREE_BEGIN_DRAG] or [wx.wxEVT_COMMAND_TREE_BEGIN_RDRAG])[0]
            nevent = TreeEvent(command, self._owner.GetId())
            nevent.SetEventObject(self._owner)
            nevent.SetItem(self._current) # the dragged item
            nevent.SetPoint(p)
            nevent.Veto()         # dragging must be explicit allowed!
            
            if self.GetEventHandler().ProcessEvent(nevent) and nevent.IsAllowed():
                
                # we're going to drag this item
                self._isDragging = True
                self.CaptureMouse()
                self.RefreshSelected()

                # in a single selection control, hide the selection temporarily
                if not (self._agwStyle & wx.TR_MULTIPLE):
                    if self._oldSelection:
                    
                        self._oldSelection.SetHilight(False)
                        self.RefreshLine(self._oldSelection)
                else:
                    selections = self.GetSelections()
                    if len(selections) == 1:
                        self._oldSelection = selections[0]
                        self._oldSelection.SetHilight(False)
                        self.RefreshLine(self._oldSelection)

        elif self._isDragging:  # any other event but not event.Dragging()

            # end dragging
            self._dragCount = 0
            self._isDragging = False
            if self.HasCapture():
                self.ReleaseMouse()
            self.RefreshSelected()

            # send drag end event event
            nevent = TreeEvent(wx.wxEVT_COMMAND_TREE_END_DRAG, self._owner.GetId())
            nevent.SetEventObject(self._owner)
            nevent.SetItem(item) # the item the drag is started
            nevent.SetPoint(p)
            self._owner.GetEventHandler().ProcessEvent(nevent)
            
            if self._dragImage:
                self._dragImage.EndDrag()

            if self._dropTarget:
                self._dropTarget.SetHilight(False)
                self.RefreshLine(self._dropTarget)
                
            if self._oldSelection:
                self._oldSelection.SetHilight(True)
                self.RefreshLine(self._oldSelection)
                self._oldSelection = None

            self._isDragging = False
            self._dropTarget = None
            if self._dragImage:
                self._dragImage = None
            
            self.Refresh()

        elif self._dragCount > 0:  # just in case dragging is initiated

            # end dragging
            self._dragCount = 0

        # we process only the messages which happen on tree items
        if item == None or not self.IsItemEnabled(item):
            self._owner.GetEventHandler().ProcessEvent(event)
            return
        
        # remember item at shift down
        if event.ShiftDown():
            if not self._shiftItem:
                self._shiftItem = self._current
        else:
            self._shiftItem = None
        
        if event.RightUp():

            self.SetFocus()
            nevent = TreeEvent(wx.wxEVT_COMMAND_TREE_ITEM_RIGHT_CLICK, self._owner.GetId())
            nevent.SetEventObject(self._owner)
            nevent.SetItem(item) # the item clicked
            nevent.SetInt(self._curColumn) # the column clicked
            nevent.SetPoint(p)
            self._owner.GetEventHandler().ProcessEvent(nevent)

        elif event.LeftUp():

            if self._lastOnSame:
                if item == self._current and self._curColumn != -1 and \
                   self._owner.GetHeaderWindow().IsColumnEditable(self._curColumn) and \
                   flags & (wx.TREE_HITTEST_ONITEMLABEL | TREE_HITTEST_ONITEMCOLUMN):
                    self._renameTimer.Start(_RENAME_TIMER_TICKS, wx.TIMER_ONE_SHOT)
                
                self._lastOnSame = False
            
            if (((flags & wx.TREE_HITTEST_ONITEMBUTTON) or (flags & wx.TREE_HITTEST_ONITEMICON)) and \
                self.HasButtons() and item.HasPlus()):

                # only toggle the item for a single click, double click on
                # the button doesn't do anything (it toggles the item twice)
                if event.LeftDown():
                    self.Toggle(item)

                # don't select the item if the button was clicked
                return         
            
            # determine the selection if not done by left down
            if not self._left_down_selection:
                unselect_others = not ((event.ShiftDown() or event.ControlDown()) and self.HasAGWFlag(wx.TR_MULTIPLE))
                self.DoSelectItem(item, unselect_others, event.ShiftDown())
                self.EnsureVisible (item)
                self._current = self._key_current = item # make the new item the current item
            else:
                self._left_down_selection = False
            
        elif event.LeftDown() or event.RightDown() or event.LeftDClick():

            if column >= 0:
                self._curColumn = column
            
            if event.LeftDown() or event.RightDown():
                self.SetFocus()
                self._lastOnSame = item == self._current
            
            if (((flags & wx.TREE_HITTEST_ONITEMBUTTON) or (flags & wx.TREE_HITTEST_ONITEMICON)) and \
                self.HasButtons() and item.HasPlus()):

                # only toggle the item for a single click, double click on
                # the button doesn't do anything (it toggles the item twice)
                if event.LeftDown():
                    self.Toggle(item)

                # don't select the item if the button was clicked
                return

            if flags & TREE_HITTEST_ONITEMCHECKICON and event.LeftDown():
                if item.GetType() > 0:
                    self.CheckItem(item, not self.IsItemChecked(item))                        
                    return
                
            # determine the selection if the current item is not selected
            if not item.IsSelected():
                unselect_others = not ((event.ShiftDown() or event.ControlDown()) and self.HasAGWFlag(wx.TR_MULTIPLE))
                self.DoSelectItem(item, unselect_others, event.ShiftDown())
                self.EnsureVisible(item)
                self._current = self._key_current = item # make the new item the current item
                self._left_down_selection = True
            
            # For some reason, Windows isn't recognizing a left double-click,
            # so we need to simulate it here.  Allow 200 milliseconds for now.
            if event.LeftDClick():

                # double clicking should not start editing the item label
                self._renameTimer.Stop()
                self._lastOnSame = False

                # send activate event first
                nevent = TreeEvent(wx.wxEVT_COMMAND_TREE_ITEM_ACTIVATED, self._owner.GetId())
                nevent.SetEventObject(self._owner)
                nevent.SetItem(item) # the item clicked
                nevent.SetInt(self._curColumn) # the column clicked
                nevent.SetPoint(p)
                if not self._owner.GetEventHandler().ProcessEvent(nevent):

                    # if the user code didn't process the activate event,
                    # handle it ourselves by toggling the item when it is
                    # double clicked
                    if item.HasPlus():
                        self.Toggle(item)
                
        else: # any other event skip just in case

            event.Skip()

        
    def OnScroll(self, event):
        """
        Handles the ``wx.EVT_SCROLLWIN`` event for L{TreeListMainWindow}.

        :param `event`: a `wx.ScrollEvent` event to be processed.
        """

        # Let wx.PyScrolledWindow compute the new scroll position so that
        # TreeListHeaderWindow is repainted with the same scroll position as
        # TreeListMainWindow.
        #
        # event.Skip() would not work, Update() would call
        # TreeListHeaderWindow.OnPaint() synchronously, before
        # wx.PyScrolledWindow.OnScroll() is called by the event handler. OnPaint()
        # would not use the latest scroll position so the header and the tree
        # scrolling positions would be unsynchronized.
        self._default_evt_handler.ProcessEvent(event)
        
        if event.GetOrientation() == wx.HORIZONTAL:
            self._owner.GetHeaderWindow().Refresh()
            self._owner.GetHeaderWindow().Update()
        

    def CalculateSize(self, item, dc):
        """
        Calculates overall position and size of an item.

        :param `item`: an instance of L{TreeListItem};
        :param `dc`: an instance of `wx.DC`.
        """

        attr = item.GetAttributes()

        if attr and attr.HasFont():
            dc.SetFont(attr.GetFont())
        elif item.IsBold():
            dc.SetFont(self._boldFont)
        else:
            dc.SetFont(self._normalFont)

        text_w = text_h = wnd_w = wnd_h = 0
        for column in xrange(self.GetColumnCount()):
            w, h, dummy = dc.GetMultiLineTextExtent(item.GetText(column))
            text_w, text_h = max(w, text_w), max(h, text_h)
            
            wnd = item.GetWindow(column)
            if wnd:
                wnd_h = max(wnd_h, item.GetWindowSize(column)[1])
                if column == self._main_column:
                    wnd_w = item.GetWindowSize(column)[0]

        text_w, dummy, dummy = dc.GetMultiLineTextExtent(item.GetText(self._main_column))
        text_h+=2

        # restore normal font
        dc.SetFont(self._normalFont)

        image_w, image_h = 0, 0
        image = item.GetCurrentImage()

        if image != _NO_IMAGE:
        
            if self._imageListNormal:
            
                image_w, image_h = self._imageListNormal.GetSize(image)
                image_w += 2*_MARGIN

        total_h = ((image_h > text_h) and [image_h] or [text_h])[0]

        checkimage = item.GetCurrentCheckedImage()
        if checkimage is not None:
            wcheck, hcheck = self._imageListCheck.GetSize(checkimage)
            wcheck += 2*_MARGIN
        else:
            wcheck = 0

        if total_h < 30:
            total_h += 2            # at least 2 pixels
        else:
            total_h += total_h/10   # otherwise 10% extra spacing

        if total_h > self._lineHeight:
            self._lineHeight = max(total_h, wnd_h+2)

        item.SetWidth(image_w+text_w+wcheck+2+wnd_w)
        item.SetHeight(max(total_h, wnd_h+2))

        
    def CalculateLevel(self, item, dc, level, y, x_colstart):
        """
        Calculates the level of an item inside the tree hierarchy.

        :param `item`: an instance of L{TreeListItem};
        :param `dc`: an instance of `wx.DC`;
        :param `level`: the item level in the tree hierarchy;
        :param `y`: the current vertical position inside the `wx.PyScrolledWindow`;
        :param `x_colstart`: the x coordinate at which the item's column starts.
        """

        # calculate position of vertical lines
        x = x_colstart + _MARGIN # start of column
        if self.HasAGWFlag(wx.TR_LINES_AT_ROOT):
            x += _LINEATROOT # space for lines at root
        if self.HasButtons():
            x += (self._btnWidth-self._btnWidth2) # half button space
        else:
            x += (self._indent-self._indent/2)
        
        if self.HasAGWFlag(wx.TR_HIDE_ROOT):
            x += self._indent * (level-1) # indent but not level 1
        else:
            x += self._indent * level # indent according to level
        
        # a hidden root is not evaluated, but its children are always
        if self.HasAGWFlag(wx.TR_HIDE_ROOT) and (level == 0):
            # a hidden root is not evaluated, but its
            # children are always calculated
            children = item.GetChildren()
            count = len(children)
            level = level + 1
            for n in xrange(count):
                y = self.CalculateLevel(children[n], dc, level, y, x_colstart)  # recurse
                
            return y

        self.CalculateSize(item, dc)

        # set its position
        item.SetX(x)
        item.SetY(y)
        y += self.GetLineHeight(item)

        if not item.IsExpanded():
            # we don't need to calculate collapsed branches
            return y

        children = item.GetChildren()
        count = len(children)
        level = level + 1
        for n in xrange(count):
            y = self.CalculateLevel(children[n], dc, level, y, x_colstart)  # recurse
        
        return y
    

    def CalculatePositions(self):
        """ Recalculates all the items positions. """
        
        if not self._anchor:
            return

        dc = wx.ClientDC(self)
        self.PrepareDC(dc)

        dc.SetFont(self._normalFont)
        dc.SetPen(self._dottedPen)

        y, x_colstart = 2, 0
        for i in xrange(self.GetMainColumn()):
            if not self._owner.GetHeaderWindow().IsColumnShown(i):
                continue
            x_colstart += self._owner.GetHeaderWindow().GetColumnWidth(i)
        
        self.CalculateLevel(self._anchor, dc, 0, y, x_colstart) # start recursion


    def SetItemText(self, item, text, column=None):
        """
        Sets the item text label.

        :param `item`: an instance of L{TreeListItem};
        :param `text`: a string specifying the new item label;
        :param `column`: if not ``None``, an integer specifying the column index.
         If it is ``None``, the main column index is used.
        """

        dc = wx.ClientDC(self)
        item.SetText(column, text)
        self.CalculateSize(item, dc)
        self.RefreshLine(item)


    def GetItemText(self, item, column=None):
        """
        Returns the item text label.

        :param `item`: an instance of L{TreeListItem};
        :param `column`: if not ``None``, an integer specifying the column index.
         If it is ``None``, the main column index is used.
        """

        if self.IsVirtual():
            return self._owner.OnGetItemText(item, column)
        else:
            return item.GetText(column)
   

    def GetItemWidth(self, column, item):
        """
        Returns the item width.

        :param `column`: an integer specifying the column index;
        :param `item`: an instance of L{TreeListItem}.
        """
        
        if not item:
            return 0

        # determine item width
        font = self.GetItemFont(item)
        if not font.IsOk():
            if item.IsBold():
                font = self._boldFont
            elif item.IsItalic():
                font = self._italicFont
            elif item.IsHyperText():
                font = self.GetHyperTextFont()
            else:
                font = self._normalFont
            
        dc = wx.ClientDC(self)
        dc.SetFont(font)
        w, h, dummy = dc.GetMultiLineTextExtent(item.GetText(column))
        w += 2*_MARGIN

        # calculate width
        width = w + 2*_MARGIN
        if column == self.GetMainColumn():
            width += _MARGIN
            if self.HasAGWFlag(wx.TR_LINES_AT_ROOT):
                width += _LINEATROOT
            if self.HasButtons():
                width += self._btnWidth + _LINEATROOT
            if item.GetCurrentImage() != _NO_IMAGE:
                width += self._imgWidth

            # count indent level
            level = 0
            parent = item.GetParent()
            root = self.GetRootItem()
            while (parent and (not self.HasAGWFlag(wx.TR_HIDE_ROOT) or (parent != root))):
                level += 1
                parent = parent.GetParent()
            
            if level:
                width += level*self.GetIndent()

        wnd = item.GetWindow(column)
        if wnd:
            width += wnd.GetSize()[0] + 2*_MARGIN
            
        return width


    def GetBestColumnWidth(self, column, parent=None):
        """
        Returns the best column's width based on the items width in this column.

        :param `column`: an integer specifying the column index;
        :param `parent`: an instance of L{TreeListItem}.
        """

        maxWidth, h = self.GetClientSize()
        width = 0

        # get root if on item
        if not parent:
            parent = self.GetRootItem()

        # add root width
        if not self.HasAGWFlag(wx.TR_HIDE_ROOT):
            w = self.GetItemWidth(column, parent)
            if width < w:
                width = w
            if width > maxWidth:
                return maxWidth

        item, cookie = self.GetFirstChild(parent)
        while item:
            w = self.GetItemWidth(column, item)
            if width < w:
                width = w
            if width > maxWidth:
                return maxWidth

            # check the children of this item
            if item.IsExpanded():
                w = self.GetBestColumnWidth(column, item)
                if width < w:
                    width = w
                if width > maxWidth:
                    return maxWidth

            # next sibling
            item, cookie = self.GetNextChild(parent, cookie)
        
        return width


    def HideItem(self, item, hide=True):
        """
        Hides/shows an item.

        :param `item`: an instance of L{TreeListItem};
        :param `hide`: ``True`` to hide the item, ``False`` to show it.
        """

        item.Hide(hide)
        self.Refresh()
       
                
#---------------------------------------------------------------------------
# MyHyperTreeList Implementation
#---------------------------------------------------------------------------
class FlexAutoCheckHyperTreeList(HTL.HyperTreeList):

    def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=0, agwStyle=wx.TR_DEFAULT_STYLE, validator=wx.DefaultValidator,
                 name="HyperTreeList"):

        wx.PyControl.__init__(self, parent, id, pos, size, style, validator, name)

        self._header_win = None
        self._main_win = None
        self._headerHeight = 0
        self._attr_set = False
        
        main_style = style & ~(wx.SIMPLE_BORDER|wx.SUNKEN_BORDER|wx.DOUBLE_BORDER|
                               wx.RAISED_BORDER|wx.STATIC_BORDER)

        self._agwStyle = agwStyle
        
        self._main_win = FlexAutoCheckTreeListMainWindow(self, -1, wx.Point(0, 0), size, main_style, agwStyle, validator)
        self._main_win._buffered = False

        self._header_win = TreeListHeaderWindow(self, -1, self._main_win, wx.Point(0, 0),
                                                wx.DefaultSize, wx.TAB_TRAVERSAL)
        self._header_win._buffered = False
        
        self.CalculateAndSetHeaderHeight()
        self.Bind(wx.EVT_SIZE, self.OnSize)

        self.SetBuffered(HTL.IsBufferingSupported())
#        self._main_win.SetAGWWindowStyleFlag(agwStyle)

            
    
        
