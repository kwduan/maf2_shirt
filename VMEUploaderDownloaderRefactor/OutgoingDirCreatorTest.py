import msfParser
from sets import Set
from xml.dom import minidom
import OutgoingDirCreator
import unittest
import os
from shutil import copytree
from fileUtilities import rmdir_recursive

class OutgoingDirCreatorTest(unittest.TestCase):
              
    def setUp(self):  
        print "Beware:  In order to work this test must be launched from VMEUploaderDownloader dir!"  
        self.curDir = os.getcwd()        
        print " current directory is: " + self.curDir
       
    def testCreateOutgoingAndRemoveDicomBlackListedTags(self):
        
        inputDir = self.curDir + r'\InputMSFDicomTagsRemovalTestData'
        msfCacheDirABSPath = self.curDir + r'\cacheInputMSFDicomTagsRemovalTestData'
        if os.path.exists(msfCacheDirABSPath): rmdir_recursive(msfCacheDirABSPath)
        
        copytree(inputDir, msfCacheDirABSPath)
        
        inputXMLFile = msfCacheDirABSPath + "\\test_tagDICOM.msf"
        
        self.assertTrue(os.path.exists(inputXMLFile))
        
        msfParserInstance = msfParser.msfParser()
        
        doc = minidom.parse(inputXMLFile)
        rootNode = doc.documentElement
        
        vmeNode = msfParserInstance.GetVmeNodeById(rootNode,3)
        tagArray = msfParserInstance.GetVmeTagArrayNode(vmeNode)        
        tagNamesListBeforeRemoval = msfParserInstance.GetTagNames(tagArray)
        
        outgoingDirABSPath = self.curDir + r'\Outgoing'         
        if(os.path.exists(outgoingDirABSPath) == False ): os.mkdir(outgoingDirABSPath)
    
        odc = OutgoingDirCreator.OutgoingDirCreator()
        
        odc.InputMSFDirectory = msfCacheDirABSPath
        odc.OutputFolderName = outgoingDirABSPath
        
        odc.VmeToExtractID = 3
        odc.originalId = 3
        odc.hasLink = False
        odc.withChild = False
        odc.vmeName = "1.2.840.113619.2.30.1.1762288646.1924.1114238749.475"
        odc.hasBinary = True
        odc.binaryName = "test_tagDICOM.3.vtk"
        odc.user = "testuser"
        odc.UploadFullMSF = False
        odc.OutputVMEXMLName = "test_tagDICOM.xml"
        
        odc.tagsToBeRemovedLocalFileName = "InputMSFDicomTagsRemovalTestData\\tagsToBeRemovedFromUploadedVMETagArray.txt"
        
        localChksum = odc.CreateOutgoinDirAndReturnLocalMD5Checksum()
        
        self.assertEqual(localChksum, "608d5245e633a587e70ceae64d1827d3")
            
        outputXMLFile = outgoingDirABSPath + "\\" + odc.OutputVMEXMLName
        self.assertTrue(os.path.exists(outputXMLFile))
        
        doc = minidom.parse(outputXMLFile)
        rootNode = doc.documentElement
        
        tagArray = msfParserInstance.GetVmeTagArrayNode(rootNode)
        
        tagNamesListAfterRemoval = msfParserInstance.GetTagNames(tagArray)
            
        initialTagsSet = Set(tagNamesListBeforeRemoval)
        tagsSetAfterRemoval = Set(tagNamesListAfterRemoval)
        
        keptTags =  tagsSetAfterRemoval.intersection(initialTagsSet)
        removedTags = initialTagsSet - tagsSetAfterRemoval
        
        shouldBeRemoved = Set([u'AccessionNumber', u'AdditionalPatientHistory', u'PatientsName', u'PatientsSex', u'SeriesDescription', u'StudyDescription', u'InstitutionName', u'ReferringPhysiciansName', u'StudyID', u'PatientsBirthDate', \
        u'SOPInstanceUID', u'StationName', u'StudyInstanceUID', u'SeriesInstanceUID', u'PatientsWeight'])

        self.assertEquals(removedTags, shouldBeRemoved)
        
if __name__ == '__main__':
    unittest.main()
    