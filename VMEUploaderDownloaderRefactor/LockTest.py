import os
from Lock import Lock
import unittest

class LockTest(unittest.TestCase):
    
    def setUp(self):  
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"  
        
       
    def testCreateLockFile(self):
    
        filename = 'LockTestFileName.txt'
        lock = Lock( filename )
        
        assert( open(filename,'r').read().count( str( os.getpid() ) ) )
    
    def testDeleteLockFile(self):
        
        filename = 'LockTestFileName.txt'
        lock = Lock( filename )
        
        lock.deleteLockFile()
        
        assert( not os.path.exists( filename) ) 
    
if __name__ == '__main__':
    unittest.main()
