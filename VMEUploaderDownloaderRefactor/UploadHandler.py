import OutgoingDirCreator
from webServicesClient import WSUpload, WSCreateResource, xmlrpcDemoWS
import msfParser
from xml.dom import minidom
from xml.dom import Node
import xml.dom.minidom as xd
import os, sys, string, time, re , shutil
import threading, thread, CustomThread
import fileUtilities
import urllib
import msvcrt
import wx
import fileinput
from lhpDefines import *
from Debug import Debug
import shutil
import time
import ConfigLoader


class UploadHandler:
    """Upload VME XML data on remote repository along with its binary data (if present) on SRB
    
    Use UploadHandlerTest to test and debug this component
    """
    queue = None
    def __init__(self, queue, observer , dirCache, id, usr , pwd, urlServer, originalId, hasLink, \
                 withChild, msfListFile, XMLURI, isLast, vmeName):
        UploadHandler.queue = queue
        self.observer = observer
        self.dirCache = dirCache
        self.dirOutgoing = ""
        self.id = id
        self.currentUser = usr
        self.currentPassword = pwd
        self.urlServer = urlServer
        self.originalId = originalId
        self.msg = 0
        self.binaryFileSize = 0
        self.isBinaryDataPresent = False
        self.binaryName = ''
        self.BinaryURI = "NOT PRESENT"
        self.isLast = isLast
        self.block = threading.Lock()
        self.threads = []
        self.hasLink = hasLink
        self.withChild = withChild
        self.msfListFile = msfListFile
        self.XMLURI = XMLURI
        self.vmeName = vmeName
        self.existThread = 0
        self.remoteChksum = ""
        self.localChksum = ""
        self.uri = ""
        self.proxyHost = ""
        self.proxyPort = 0
        self.maxTry = 5
        self.RemoveCacheDirectoriesAfterUpload = True
        self.cl = ConfigLoader.ConfigLoader("","") #Should be invoke before changing working directory
        
            
    def upload(self):
        self.proxyHost, self.proxyPort = retriveProxyParameters()
        
        if Debug:
            print "->" + self.proxyHost + "<-"
            print "->" + str(self.proxyPort) + "<-"
       
        self.createOutgoingDir()
        
        self.binarySendResult = None
        self.isBinaryDataPresent = self.isBinaryPresent()      
        
        #Check if VME has a binary data        
        if(self.isBinaryDataPresent == True):
            
            #get free SRB resource (return URI string)
            self.getFreeResource() 
            if Debug:
                print "Get the free SRB resource for binary Upload: " + self.BinaryURI
            
            self.createXMLAndBinary()
                    
            binaryFileName = self.getBinaryFile()
            if Debug:
                print "binary name: " + str(binaryFileName)
            if (binaryFileName != ""):
               
                if Debug:
                    print self.BinaryURI
                    
                #binarySendResult = False
                oldPercentage = - 1
                fileTransferStatus = 0
                timeStep = 0.5
                
                uploadSpeed = self.GetHeuristicUploadSpeedEstimateInKBPerSecond("..\\..")
                
                tStart = time.time() 
                
                uploadTimeEstimate = self.binaryFileSize / uploadSpeed
                
                tFinish = tStart + uploadTimeEstimate        
                
                if Debug:
                    print "binaryFileSize: " + str(self.binaryFileSize)
                    print "tStart: " + str(tStart)
                    print "uploadTimeEstimate: " + str(uploadTimeEstimate)
                    print "tFinish: " + str(tFinish)
                    
                uploadedOverTotal = 0 
                timeElapsed = 0
                
                thread.start_new_thread(self.sendBinaryFileThreadFunction, ())      
                
                if Debug:
                    print "Wainting for sending binary..."
                    print "Total Size of Binary: " + str(self.binaryFileSize)
                
                while 1:
                  
                    time.sleep(timeStep)
                     
                    uploadedOverTotal = (timeElapsed / uploadTimeEstimate)
                    
                    if uploadedOverTotal >= 1:
                        uploadedOverTotal = 1
                    progress = 80 * float(uploadedOverTotal)
                    
                    timeElapsed = time.time() - tStart
                    
                    remainingTime = uploadTimeEstimate - timeElapsed 
                    
                    if remainingTime < 0:
                        remainingTime = 0
                        
                    if Debug:
                        print "timeElapsed: " + str(timeElapsed)
                        print "uploadedOverTotal: " + str(uploadedOverTotal)
                        print "progress: " + str(progress) + "%"
                        print "uploaded data size: " + str(uploadedOverTotal*self.binaryFileSize)
                        print "remainingTime: " + str(remainingTime)
                        print ""
                        print "binarySendResult: " + str(self.binarySendResult) 
                   
                    # 3210 : sending binary data progress bar update ...
                    fileTransferStatus = 3210
                    gauge_FileTransferStatus_RemainingTimeInSeconds_Progress = \
                    [self.observer, fileTransferStatus, remainingTime, progress]             
                    self.block.acquire()
                    UploadHandler.queue.put(gauge_FileTransferStatus_RemainingTimeInSeconds_Progress)
                    self.block.release()
                    
                    if(self.binarySendResult != None):
                        
                        # 6162 : binary send result ! none 
                        fileTransferStatus = 6162
                        self.block.acquire()
                        gauge_FileTransferStatus = \
                        [self.observer, fileTransferStatus]
                        UploadHandler.queue.put(gauge_FileTransferStatus)
                        self.block.release()
                        break
        else: 
        
            self.createXMLAndBinary()
            self.binarySendResult = True
                       
        #---MD5 check-point-----------------------------------#
        # 105 : before MD5 checksum check
        fileTransferStatus = 105 
        gauge_FileTransferStatus_RemainingTimeInSeconds = [self.observer, fileTransferStatus]
        self.block.acquire()  
        UploadHandler.queue.put(gauge_FileTransferStatus_RemainingTimeInSeconds)
        self.block.release()
        
        if(self.isBinaryDataPresent == True and self.binarySendResult == True):
        
            if Debug:
                print "Local checksum=  " + self.localChksum
                print "Remote checksum= " + self.remoteChksum
            
            if (self.localChksum == self.remoteChksum):
                
                # 2000 :  successful MD5 checksum check
                fileTransferStatus = 2000
                gauge_FileTransferStatus_RemainingTimeInSeconds = [self.observer, fileTransferStatus]
                self.block.acquire()  
                UploadHandler.queue.put(gauge_FileTransferStatus_RemainingTimeInSeconds)
                self.block.release()
                
                if Debug:
                    print " "
                    print "-------MD5 checksum control successful!-----------"
                    print " "
            else:
            
                if Debug:
                    print " "
                    print "-------Error: MD5 checksum control unsuccessful!--------"
                    print " "
                
                # 120 : error in MD5 checksum check
                fileTransferStatus = 120
                gauge_FileTransferStatus_RemainingTimeInSeconds = [self.observer, fileTransferStatus]
                self.block.acquire()  
                UploadHandler.queue.put(gauge_FileTransferStatus_RemainingTimeInSeconds)
                self.block.release()
                self.binarySendResult = False
                errorFile = open(sys.path[0] + '\\ErrorFound.lhp', 'a')
                errorFile.write('Checksum Error uploading binary data of VME: ' + self.vmeName + '.')
                errorFile.close() 
                if(self.msfListFile != "noMsf"):
                    self.removeUploadedXml()
                
        if(self.binarySendResult == True):
          
          self.sendXMLFile()
          
          # 110 : successful sending XML file
          fileTransferStatus = 110 
          gauge_FileTransferStatus_RemainingTimeInSeconds = [self.observer, fileTransferStatus]
          self.block.acquire()  
          UploadHandler.queue.put(gauge_FileTransferStatus_RemainingTimeInSeconds)
          self.block.release()
          allEnded = False
 
          if (self.isLast == "false"):
              #replace status for resource correctly uploadedOverTotal
              if(os.path.exists(sys.path[0] + '\\status.txt')):
                  
                  counter = 0
                  while 1:
                        size = os.path.getsize(sys.path[0] + '\\status.txt')
                        statusFile = open(sys.path[0] + '\\status.txt', 'a')
                        try:
                            msvcrt.locking(statusFile.fileno(), msvcrt.LK_RLCK, size)
                            statusFile.write('ended\n')
                            statusFile.close()  
                            break                                      
                        except:
                            counter = counter + 1 #to avoid deadlock
                            statusFile.close()
                            if(counter == 10):
                                if Debug:
                                  print "----------Can not read in status.lhp-----------"
                                pass
                        
    
          else:
              if(os.path.exists(sys.path[0] + '\\status.txt')):
                  while 1:
                        size = os.path.getsize(sys.path[0] + '\\status.txt')
                        statusFile = open(sys.path[0] + '\\status.txt', 'a')
                        try:
                            msvcrt.locking(statusFile.fileno(), msvcrt.LK_RLCK, size)
                            statusFile.write('lastEnded\n')
                            statusFile.close()  
                            break                                      
                        except:
                            counter = counter + 1 #to avoid deadlock
                            statusFile.close()
                            if(counter == 10):
                                if Debug:
                                  print "----------Can not read in status.lhp-----------"
                                pass
          processStarted = 0
          processEnded = 0
          startedCount = 0
          endedCount = 0
          lastStartedCount = 0
          lastEndedCount = 0
          lastStarted = False
          
          try:
              size = os.path.getsize(os.getcwd() + '\\status.txt')
              statusFile = open(sys.path[0] + '\\status.txt', 'r')
          
              msvcrt.locking(statusFile.fileno(), msvcrt.LK_RLCK, size)
              for line in statusFile:
                  if line == "started\n":
                      startedCount += 1
                      if processStarted == 0:
                          processStarted += 1
                      if lastStarted == True:
                          processStarted += 1
                          lastStarted = False
                  elif line == "ended\n":
                      endedCount += 1   
                  elif line == "lastStarted\n":
                      lastStarted = True
                      lastStartedCount += 1
                      if processStarted == 0:
                          processStarted += 1
                  elif line == "lastEnded\n":
                      lastEndedCount += 1
                      processEnded += 1
                      
                      
              statusFile.close()  
              
              if Debug:
                  print "processStarted :" + str(processStarted)     
                  print "processEnded :" + str(processEnded)     
                  print "startedCount :" + str(startedCount)     
                  print "endedCount :" + str(endedCount)     
                  print "lastStartedCount :" + str(lastStartedCount)     
                  print "lastEndedCount :" + str(lastEndedCount)                   
              if processStarted == processEnded and startedCount == endedCount and lastStartedCount == lastEndedCount:
                  allEnded = True

          except:
              #counter = counter + 1 #to avoid deadlock
              #statusFile.close()
              #if(counter == 10):
              if Debug:
                    print "----------Can not read in status.txt-----------"
              pass                  
          
          if allEnded == True:
              self.block.acquire()  
              fileTransferStatus = 130 #130 for 'ALL COMPLETE!'
              gauge_FileTransferStatus_RemainingTimeInSeconds = [self.observer, fileTransferStatus]
              UploadHandler.queue.put(gauge_FileTransferStatus_RemainingTimeInSeconds)
              self.block.release()
              if(os.path.exists(sys.path[0] + '\\status.txt')):
                  os.remove(sys.path[0] + '\\status.txt')
                  if Debug:
                      print "status.txt removed" 
                  
        
          if Debug:
              print "End Upload"
              print "Uploaded Binary in SRB: " + self.BinaryURI
              print "Uploaded XML on Biomedtown: " + self.XMLName
              print "Uploaded by: " + self.currentUser
              print "In server url: " + self.urlServer
              print "--------Upload successful :D :D :D --------"
          
          if Debug:
              print "--------cleaning up cache directories--------"
              print "cache dir is: " + str(self.dirCache) 
              print "outgoing dir is " + str(self.dirOutgoing)
              print "current dir is: " + str(os.getcwd())
          
          os.chdir(self.dirOutgoing + r"\..\..")
          
          if (self.RemoveCacheDirectoriesAfterUpload == True):
          
              if Debug:
                  print "changing to: " + str(os.getcwd())
                  print "removing " + str(self.dirCache) 
              
              fileUtilities.rmdir_recursive(self.dirCache)
              
              if (os.path.isdir(self.dirCache) == False):
                if Debug:
                    print "done!"
              else:
                if Debug:
                    print "cannot remove " + str(self.dirCache)
            
    
              if Debug:
                  print "removing " + str(self.dirOutgoing)
              fileUtilities.rmdir_recursive(self.dirOutgoing)
              
              if (os.path.isdir(self.dirOutgoing) == False):
                if Debug:
                    print "done!"
              else:
                if Debug:
                    print "cannot remove " + str(self.dirOutgoing)
              if Debug:
                  print "--------clean up cache directories successful--------"
              
          
        else:  # if(self.binarySendResult == True):
          # 1234: error uploading binary data MTOM thread
          fileTransferStatus = 1234 #120 for 'error!'
          
          gauge_FileTransferStatus_RemainingTimeInSeconds = [self.observer, fileTransferStatus]
          
          self.block.acquire()  
          UploadHandler.queue.put(gauge_FileTransferStatus_RemainingTimeInSeconds)
          self.block.release()
          
          if Debug:
              print "--------Error uploading binary data----------------"
              

                  
    def createOutgoingDir(self):
        curDir = os.getcwd()
        count = 0
        self.dirOutgoing = curDir + r'\Outgoing'
        directory = self.dirOutgoing + '\\' + str(count)
       
        while(os.path.exists(directory)):
           count = count + 1
           directory = self.dirOutgoing + '\\' + str(count)
        self.dirOutgoing = directory
        if Debug:
            print "creating Outgoing directory: " + str(directory)
        os.mkdir(directory)
        
        		
    def createXMLAndBinary(self):
        curDir = os.getcwd()
        odc = OutgoingDirCreator.OutgoingDirCreator()
        odc.InputMSFDirectory = self.dirCache

        odc.OutputFolderName = self.dirOutgoing
        odc.VmeToExtractID = int(self.id)
        odc.originalId = self.originalId
        odc.DatasetURI = self.BinaryURI
        odc.hasLink = self.hasLink
        odc.withChild = self.withChild
        odc.vmeName = self.vmeName
        odc.hasBinary = self.isBinaryDataPresent
        odc.binaryName = self.binaryName
        odc.user = self.currentUser
        if(self.msfListFile != 'noMsf'):
            odc.UploadFullMSF = True
            
        self.localChksum = odc.CreateOutgoinDirAndReturnLocalMD5Checksum()
        if(self.localChksum == '' and self.isBinaryDataPresent == True and self.msfListFile != 'noMsf'):
            percentage = 120 #120 for 'error!'
            lista = [self.observer, percentage]
            self.block.acquire()  
            UploadHandler.queue.put(lista)
            self.block.release()
            self.removeUploadedXml()
            sys.exit(1)
            
        
        #get size of binary locally
        if (self.isBinaryDataPresent == True):
            self.binaryFileSize = self.getBinaryFileSize()

    def launchXMLEditor(self, dir):
        oldDir = os.getcwd()
        os.chdir("\"C:\\Program Files\\Peter's XML Editor\\")
        #print os.getcwd()
        command = "\"" + dir + "\\" + self.getXMLFile() + "\""
        command = "pxe.exe " + command
        #print command
        os.system(command)
        os.chdir(oldDir)
        
    def getFreeResource(self):
        #here call module to get URI of first free resource
        instance = WSCreateResource.WSCreateResource()
        serviceUrl = self.cl.WebServicesURL + '/WSCreateResource.cgi'
                
        # 2480: before getmting SRB resource
        fileTransferStatus = 2480
        lista = [self.observer, fileTransferStatus]
        self.block.acquire()  
        UploadHandler.queue.put(lista)
        self.block.release()
        
        try:
            binarySRBURI = instance.CreateResource(serviceUrl, self.proxyHost, self.proxyPort)
        except:
            if Debug:
                print "------Error calling WSCreateResource.cgi----------"  
            return ""
        if binarySRBURI == "":
            # 2468: error getting SRB resource
            fileTransferStatus = 2468
            lista = [self.observer, fileTransferStatus]
            self.block.acquire()  
            UploadHandler.queue.put(lista)
            self.block.release()
            errorFile = open(sys.path[0] + '\\ErrorFound.lhp', 'a')
            errorFile.write('Error calling WSCreateResource.cgi.')
            errorFile.close() 
            if(self.msfListFile != "noMsf"):
                self.removeUploadedXml()
                sys.exit(1)
                             
        self.BinaryURI = binarySRBURI
        # 2496: successful getting SRB resource
        fileTransferStatus = 2496
        lista = [self.observer, fileTransferStatus]
        self.block.acquire()  
        UploadHandler.queue.put(lista)
        self.block.release()
        
        

    def sendBinaryFileThreadFunction(self):
        os.rename(self.dirOutgoing + "\\" + self.getBinaryFile(), self.dirOutgoing + "\\" + self.BinaryURI)
        self.sendBinaryFileThreadFunctionInternal(self.BinaryURI)
        if Debug:
            print "Sending Thread Finished"
		
    def sendXMLFile(self):
        overQuota = 0
        self.XMLName = -1
        
        
        os.rename(self.dirOutgoing + "\\" + self.getXMLFile(), self.dirOutgoing + "\\" + self.XMLURI)
        oldDir = os.getcwd()
        os.chdir(self.dirOutgoing)
        
        ws = xmlrpcDemoWS.xmlrpc_demoWS()
        ws.setCredentials(self.currentUser, self.currentPassword)
        ws.setServer("%s/%s" % (self.urlServer,self.XMLURI ))
        ws.ProxyURL = self.proxyHost
        ws.ProxyPort = self.proxyPort
        
        # 10000 : before xmluploadDataresource
        fileTransferStatus = 10000
        lista = [self.observer, fileTransferStatus]
        self.block.acquire()  
        UploadHandler.queue.put(lista)
        self.block.release()
        
        for c in range(0,self.maxTry):
            
            try:
                out = ws.run('xmluploadDataresource', self.XMLURI, self.vmeName)[1]
        
            except BaseException, e:
                if Debug:
                    print "-----------Error in xmlupload service------------" + str(e)
                time.sleep(1)     
                continue 

            
            dom = xd.parseString(out)
    
            if dom.getElementsByTagName("fault"):
                
                if Debug:
                    print "-----------Error in xmlupload service------------"
                
                for el in dom.getElementsByTagName("string"):
                    for node in el.childNodes:  
                        error = node.data
                
                if (error.find('Over Quota') != -1):
                    wx.MessageBox("Over quota! Data uploaded will be removed.", wx.MessageBoxCaptionStr, wx.STAY_ON_TOP | wx.OK)
                    overQuota = 1
                    break
                time.sleep(1)
                continue

            for el in dom.getElementsByTagName("string"):
                for node in el.childNodes:  
                    self.XMLName = node.data
            
            if self.XMLName == -1:
                time.sleep(1)
                continue            
            else:
                break
            
        if self.XMLName == -1 or overQuota == 1:
            
            # 106 : error uploading XML
            lista = [self.observer, 106]
            
            self.block.acquire()  
            UploadHandler.queue.put(lista)
            self.block.release()
            
            if Debug:
               print 'Error in xmlUpload service uploading VME: ' + self.vmeName + '.'
        
            #write a file used by builder to catch error and stop MSF upload
            errorFile = open(sys.path[0] + '\\ErrorFound.lhp', 'a')
            errorFile.write('Error in xmlupload service uploading VME: ' + self.vmeName + '.')
            errorFile.write(error)
            errorFile.close() 
            if(os.path.exists(sys.path[0] + '\\' + self.msfListFile)):
                
                counter = 0
                
                while 1:
                    size = os.path.getsize(sys.path[0] + '\\' + self.msfListFile)
                    msfList = open(sys.path[0] + '\\' + self.msfListFile, 'a')
                    try:
                        msvcrt.locking(msfList.fileno(), msvcrt.LK_RLCK, size)
                        msfList.write(self.XMLURI + '\n')
                        if Debug:
                            print 'data: ' + self.XMLURI  
                        msfList.close()
                        break
                    except:
                        counter = counter + 1 #to avoid deadlock
                        msfList.close()
                        pass
                    if(counter == 3):

                        if Debug:
                            print "----------Can not write in " + self.msfList + "-----------"
                        msfList.close()
                        os.remove(sys.path[0] + '\\' + self.msfListFile)
                        sys.exit(1)
            if(self.msfListFile != "noMsf"):
                self.removeUploadedXml() 
            sys.exit(1)
            
    
        counter = 0
        
        if(self.msfListFile != 'noMsf' and str(self.id) != '-1'):
            #better to use a locked file
            if(os.path.exists(sys.path[0] + '\\' + self.msfListFile)):
                while 1:
                    size = os.path.getsize(sys.path[0] + '\\' + self.msfListFile)
                    msfList = open(sys.path[0] + '\\' + self.msfListFile, 'a')
                    
                    try:
                        msvcrt.locking(msfList.fileno(), msvcrt.LK_RLCK, size)
                        msfList.write(self.XMLName + '\n')
                        msfList.close()
                        break
                    except:
                        counter = counter + 1 #to avoid deadlock
                        msfList.close()
                        pass
                    if(counter == 3):

                        if Debug:
                            print "----------Can not write in " + self.msfList + "-----------"
                        msfList.close()
                        os.remove(sys.path[0] + '\\' + self.msfListFile)
                        sys.exit(1)            
                
        if(str(self.id) == '-1'):
            if(os.path.exists(sys.path[0] + '\\' + self.msfListFile)):
                os.remove(sys.path[0] + '\\' + self.msfListFile)
                    
                          
        if Debug:
            print self.XMLName
        
    def isBinaryPresent(self):
        result = False
        
        #if node is root VME, no binary is present
        if (str(self.id) == "-1"):
            if Debug:
                print "\nNo binary data"
            return result
        
        oldDir = os.getcwd()
        os.chdir(self.dirCache)
        files = os.listdir(self.dirCache)
    
        msfFileNameList = []
        for file in files:
           if re.search('\\.msf$', file):
              msfFileNameList.append(file)
        assert(len(msfFileNameList) == 1)
        
        msfFileName = msfFileNameList[0]
        domDocument = minidom.parse(msfFileName)
        msfRootNode = domDocument.documentElement
        
        rootNode = msfRootNode
        
        if Debug:
            print "\ninput MSF Directory: " + self.dirCache
            print "\ninput MSF filename: " + msfFileName
            print "\nExtracting vme with ID: " + str(self.id) + '\n' 
        
        msfDOMParserInstance = msfParser.msfParser()
        
        # get the vme node
        outVmeNode = msfDOMParserInstance.GetVmeNodeById(rootNode, self.id)
        fileNameList = msfDOMParserInstance.GetVMEDataURLList(outVmeNode)
        if (len(fileNameList) == 1 and len(fileNameList[0]) != 0):
            self.binaryName = fileNameList[0]
            result = True
            if Debug:
                print "Binary data present"
               
        os.chdir(oldDir)  
        return result
        
    
            
    def sendBinaryFileThreadFunctionInternal(self, filename):
        
        oldDir = os.getcwd()
        
        os.chdir(self.dirOutgoing)

        
        # 107 : before MTOM upload thread
        lista = [self.observer, 107]
        self.block.acquire()  
        UploadHandler.queue.put(lista)
        self.block.release()
        
        if Debug:
            print "->" + self.proxyHost + "<-"
            print "->" + str(self.proxyPort) + "<-"
        
        instance = WSUpload.WSUpload()
        
        fileSent = 0
        
        for c in range(0,self.maxTry):
            try:
                serviceUrl = self.cl.WebServicesURL + '/WSUpload.cgi'
                result = instance.Upload(filename, serviceUrl, self.proxyHost, self.proxyPort)
                if Debug:
                    print "Binary upload  successful!"
                fileSent = 1
                break
            except:
                if Debug:
                    print "--------Error calling WSUpload.cgi-----------"
                time.sleep(1)
                continue
        
        if fileSent == 1: 
            # 109 : successful MTOM upload thread
            fileTransferStatus = 109 
            lista = [self.observer, fileTransferStatus]            
            self.block.acquire()  
            UploadHandler.queue.put(lista)
            self.block.release()     
            
            self.remoteChksum = result.chksum
            self.remoteChksum = self.remoteChksum.lower()
            
            if Debug:
                print "Remote checksum: " + str(self.remoteChksum)
            
            self.uri = result.uriFile
        
            if Debug:
                print "URI File: " + str(result.uriFile)
            
            self.binarySendResult = True
            os.chdir(oldDir)
        
        else:
            
            self.binarySendResult = False
            
            # 108 : failed MTOM upload thread 
            lista = [self.observer, 108]
            
            self.block.acquire()  
            UploadHandler.queue.put(lista)
            self.block.release()
        
            
  #  def __removeSRBData(self,filename):
  #      
  #      oldDir = os.getcwd()
  #      os.chdir(self.dirOutgoing)
  #      
  #      print "->"+ self.proxyHost + "<-"
  #      print "->"+ str(self.proxyPort) + "<-"
  #      
  #      instance = WSDelete.WSDelete()
  #      result = instance.Delete(filename,'http://wsdevel.biomedtown.org/WSDelete.cgi',self.proxyHost,self.proxyPort)
   

#        time.sleep(1)
#        os.chdir(oldDir)
        
    
       
    def removeUploadedXml(self):
        self.proxyHost, self.proxyPort = retriveProxyParameters()
        ws = xmlrpcDemoWS.xmlrpc_demoWS()
        ws.setCredentials(self.currentUser, self.currentPassword)
        ws.ProxyURL = self.proxyHost
        ws.ProxyPort = self.proxyPort
        
        if(self.msfListFile != 'noMsf'):
            #better to use a locked file
            if(os.path.exists(sys.path[0] + '\\' + self.msfListFile)):
                msfList = open(sys.path[0] + '\\' + self.msfListFile, 'r')
                for line in msfList.readlines():
                    if Debug:
                        print "removing file: " + line
                    if(line != ""):
                        line = line.replace ("\n", "")
                        ws.setServer(self.urlServer)
                        
                        #check if resource exists on repository
                        page = urllib.urlopen(self.urlServer + line)
                        pagedata = page.read()
                        result = pagedata.find('Resource not found')
                        if(result != - 1):
                            continue
                        try:
                            out = ws.run('xmldelete', line)[1]
                        except:
                            if Debug:
                                print "-----Error in xmldelete service---------" 
                            continue
                            
            
                        dom = xd.parseString(out)
                        if dom.getElementsByTagName("fault"):
                            if Debug:
                                print "-----------Error in xmldelete service------------"
                            for el in dom.getElementsByTagName("string"):
                                for node in el.childNodes:  
                                    error = node.data
                            if Debug:
                                print error
                            continue
        
            os.remove(sys.path[0] + '\\' + self.msfListFile)           


    def getXMLFile(self):
        files = os.listdir(self.dirOutgoing)
        #print files
        xmlFile = ""
        for file in files:
            if (re.search('\\.up$', file)):
               xmlFile = file
        return xmlFile

    def getBinaryFile(self):
        files = os.listdir(self.dirOutgoing)
        #print files
        binaryFile = ""
        for file in files:
            if (re.search('\\.up$', file) == None):
               binaryFile = file
        return binaryFile
    
    def getBinaryFileSize(self):
        return os.stat(self.dirOutgoing + "\\" + self.getBinaryFile()).st_size
    
        
    def GetHeuristicUploadSpeedEstimateInKBPerSecond(self, dataDirPath="."):
        
        try:
            testFile = str(dataDirPath) + "\\vmeUploaderTestData\\uploadSpeedProbeData\\uploadSpeedProbeData.vtk"
            testFileSize = os.stat(testFile).st_size
            instanceURI = WSCreateResource.WSCreateResource()
            
            serviceUrl = self.cl.WebServicesURL + '/WSCreateResource.cgi'            
            
            binaryURI = "NOT PRESENT"
            freename = instanceURI.CreateResource(serviceUrl, self.proxyHost, self.proxyPort)
            
            if Debug:
                print "speed probe testFile: " + testFile
                print "speed probe SRB resource for upload: " + freename
                print "speed probe file size: " + str(testFileSize)
                
            shutil.copyfile(testFile, freename)
            instanceUP = WSUpload.WSUpload()
            
            startT = time.time()
                        
            serviceUrl = self.cl.WebServicesURL + '/WSUpload.cgi'
            result = instanceUP.Upload(freename, serviceUrl, self.proxyHost, self.proxyPort)
            endT = time.time()
            
            tElapsed = endT - startT
            
            speed = testFileSize / tElapsed
            
            shutil.move(freename, testFile)

        
            heuristicSpeedInBytes = speed
        except:
            heuristicSpeedInBytes = 1000
            
        
        if Debug:
            print "Heuristic upload speed estimate using probe data: " + str(heuristicSpeedInBytes)
        
        return heuristicSpeedInBytes

		
def startUpload(queue, observer, dirCache, id , usr , pwd, urlServer, originalId, hasLink, \
    withChild, msfListFile, XMLURI, isLast, vmeName):\
    
    uploadHandler = UploadHandler(queue, observer, dirCache, id, usr , pwd, urlServer, originalId, \
    hasLink, withChild, msfListFile, XMLURI, isLast, vmeName)
    
    uploadHandler.upload()
    
    
if __name__ == '__main__':
  startUpload()
