# -*- coding: iso-8859-1 -*-
import wx
import sys

provider = wx.SimpleHelpProvider()
wx.HelpProvider_Set(provider)

import listGroups


class groupsSelectorFrame(wx.Frame):
    def __init__(self,parent,id = -1,title='',pos = wx.Point(1,1),size = wx.Size(260,120),style = wx.DEFAULT_FRAME_STYLE,name = 'frame', app = None):
        self.App = app  
        
        self.listGroups = listGroups.ListGroups()
        self.listGroups.SetCredentials(self.App.user, self.App.password)
        pre=wx.PreFrame()
        self.OnPreCreate()
        pre.Create(parent,id,title,pos,size,style,name)
        self.PostCreate(pre)
        self.initBefore()
        self.VwXinit()
        self.initAfter()
        self.Selections = []

    def __del__(self):
        self.Ddel()
        return

    def VwXinit(self):
        self.Show(False)
        self.panel = wx.Panel(self,-1, style=wx.SUNKEN_BORDER)

        # OK button
        self.okButton = wx.Button(self.panel,-1,"",wx.Point(141,151),wx.Size(100,30))
        self.okButton.SetLabel("Ok")
        self.Bind(wx.EVT_BUTTON,self.okButton_VwXEvOnButtonClick,self.okButton)


        self.groups =[]
        self.PopulateDict()
        self.list = []
        #self.listGroups.getGroupsList()
    


        self.comboBox = wx.ComboBox(self.panel, size=wx.DefaultSize, choices=[])
        self.widgetMaker(self.comboBox, self.groups)
        
        self.description = ""
        if len(self.groups)>0:
            self.comboBox.SetSelection(0)
            self.description = self.groups[0].id
            
        
        self.textWidget1 = wx.StaticText(self.panel, -1, "Choose from groups list: ", (45, 25), style=wx.ALIGN_CENTRE)
        
        self.horSizer1 = wx.BoxSizer(wx.HORIZONTAL)
        self.horSizer1.Add(self.textWidget1, 1, wx.ALIGN_CENTER, wx.ALL, 10)
        self.horSizer1.Add(self.comboBox, 1, wx.ALIGN_CENTER, wx.ALL, 10)
        
        #self.textWidget2 = wx.StaticText(self.panel, -1, self.description, (45, 25), style=wx.ALIGN_CENTRE)
        
        #self.horSizer2 = wx.BoxSizer(wx.HORIZONTAL)
        #self.horSizer2.Add(self.textWidget2, 1, wx.ALIGN_CENTER, wx.ALL, 10)
       
        sizer = wx.BoxSizer(wx.VERTICAL)
        
        sizer.Add(self.horSizer1, 1, wx.ALIGN_CENTER, wx.ALL, 4)
        #sizer.Add(self.horSizer2, 1, wx.ALIGN_CENTER, wx.ALL, 4)
        sizer.Add(self.okButton, 0, wx.ALIGN_CENTER, wx.ALL, 10)
        
        self.panel.SetAutoLayout(1)
        self.panel.SetSizer(sizer)
        self.panel.Layout()
        self.panel.Fit()
        self.Layout()
        return

    def PopulateDict(self):
        self.listGroups.getGroupsList()
        for i in range(0, len(self.listGroups.IdList)/2):
            id = self.listGroups.IdList[i*2]
            title = self.listGroups.IdList[i*2 +1]
            self.groups.append(listGroups.Group(id, title))
        return
    
    #----------------------------------------------------------------------
    def widgetMaker(self, widget, objects):
        """"""
        for obj in objects:
            widget.Append(obj.title)
            print obj.title
        widget.Bind(wx.EVT_COMBOBOX, self.onSelect)
 
    #----------------------------------------------------------------------
    def onSelect(self, event):
        """"""
        print "You selected: " + self.comboBox.GetStringSelection()
        sel = self.comboBox.GetSelection()
        
        self.description = self.groups[sel].id
        #self.textWidget2.SetLabel(self.text)
        #self.panel.SetAutoLayout(1)
        #self.panel.Layout()
        #self.Refresh()
        

    def okButton_VwXEvOnButtonClick(self,event):
        sel = self.comboBox.GetSelection()
        string = str(self.groups[sel].id)
        #string = self.comboBox.GetStringSelection()
        self.writeOnFile(string)
        self.App.ExitMainLoop()
        return #end function
    
    def writeOnFile(self, string):
        #create file with selected vme's from basket
        file = open(self.listGroups.fileName,"w")
        file.write(string)
        file.write("\n")
        
        file.close()
        return

    def OnPreCreate(self):
        #add your code here

        return

    def initBefore(self):
        #add your code here

        return

    def initAfter(self):
        #add your code here
        self.Centre() 
        self.Show()
        return

    def Ddel(self): #init function
        #[ f9]Code VwX...Don't modify[ f9]#
        #add your code here

        return #end function

#[win]end your code



class App(wx.App):
    def OnInit(self):
        self.user = sys.argv[0]
        self.password = sys.argv[1]
        #self.user = "testuser"
        #self.password = "6w8DHF"
        
        self.main = groupsSelectorFrame(None,-1,title=self.user ,app = self)
            
        self.main.Show()
        self.SetTopWindow(self.main)
        return 1

def main():
    usage_msg = '''Usage: %s user, password''' % sys.argv[0]
    if(len(sys.argv) != 3):
       print 'Error :\n' + usage_msg
       sys.exit(1)
       
    if(len(sys.argv) == 3):
        sys.argv = sys.argv[1:]
        #print sys.argv
        application = App(0)
        application.MainLoop()
        application.ExitMainLoop()

if __name__ == '__main__':
    main()
