#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Roberto Mucci
#-----------------------------------------------------------------------------

import lhpEditVMETag
import Debug
import os
import msfParser
import sys, string
import unittest
import shutil

class lhpEditVMETagTest(unittest.TestCase):
    
    def testSaveXMLWithTagModified(self):

        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"
        
        curDir = os.getcwd()        
        
        print " current directory is: " + curDir

        
        tag = lhpEditVMETag.lhpEditVMETag()
        testDataDir = curDir + r'\msf_test_import_export_VME'
        tag.InputCacheDirectory = testDataDir
        tag.InputMSFDirectory = testDataDir
        tag.OutputFolderName = testDataDir
        tag.UnhandledPlusManualTagsListFileName = r'\unhandledPlusManualTagsList.csv'
        tag.HandledAutoTagsListFileName = testDataDir + r'\handledAutoTagsList.csv'
        tag.FakeRootMSFFileName = curDir + r'\applicationData\fakeRoot.xml'
        tag.FakeMSFFileName = curDir + r'\applicationData\fakeMSF.xml'
        tag.VmeToExtractID = '1'
        tag.BuildOutputMSF()
        
        outputMSFABSName = tag.OutputFolderName + "\\" + tag.OutputMSFFileName
        outputFileExists = os.path.exists(outputMSFABSName)
        self.assertTrue(outputFileExists)
        
        os.remove(outputMSFABSName)
        outputFileExists = os.path.exists(outputMSFABSName)        
        self.assertFalse(outputFileExists)

if __name__ == '__main__':
    unittest.main()
    
