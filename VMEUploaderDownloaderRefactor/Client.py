import socket
import sys
from Debug import Debug
import os
import random
import time 
import threading
import decimal
import WinDLLLoader

# This class is a workaround for a problem that happens in the ior's network:
# Some processes remain suspended an never end.
# this class wait some seconds and then kill the parent process

class suicide(threading.Thread):
    def __init__ (self, pid):
        self.timeout = 40 # wait 20 seconds
        self.pid = pid
        self.terminate = False
        threading.Thread.__init__(self)
    
    def run(self):
        if(Debug):
            print "starting suicide"
        start_time = time.time()
        cur_time = time.time()
        while((cur_time - start_time) <  self.timeout):
            if (self.terminate == True):
                sys.exit(0)
            time.sleep(2)
            if(Debug):
                print "waiting 2 sec"
                print "elapsed time ", str(cur_time - start_time)
            cur_time = time.time()
        
        if(Debug):
            print "I'm the process " + str(self.pid) + " and I'm dying... Adios!"
        result = False
        
        while(result == False):# and self.terminate == False):
            result = self.kill(self.pid)

        # may add a control on download here!
        sys.exit(1)
        
    def kill(self,pid):
        strNumberOfThreads = "0"
        # Here must decrement the download thread
        if(os.path.exists("lhpDownloadThreads.txt")):
            commFile = open("lhpDownloadThreads.txt","r") # Refactor this to use a single file (lhpCommunication.txt)
            # read the number of download thread
            for line in commFile:
                strNumberOfThreads = line
            self.numberOfThreads = decimal.Decimal(strNumberOfThreads,0)
            if(self.numberOfThreads > 0):
                self.numberOfThreads = self.numberOfThreads - 1
            commFile.close()
                           
            commFile = open("lhpDownloadThreads.txt","w+") # Refactor this to use a single file (lhpCommunication.txt)
            # write the number of download thread
            commFile.write("%d" % self.numberOfThreads)
            commFile.close()
        
        # first try load the DLLs from the right location
        dllLoader = WinDLLLoader.WinDLLLoader()
        pywintypes25_log = dllLoader.Load("pywintypes25")
        pythoncom25_log = 0
        if(pywintypes25_log == 1): #dll dependecy
            pythoncom25_log = dllLoader.Load("pythoncom25")
        if(Debug):
            print "Loading pywintypes25 returns", pywintypes25_log
            print "Loading pythoncom25 returns", pythoncom25_log
        print 
        try:
            """kill function for Win32"""
            import win32api
            handle = win32api.OpenProcess(1, 0, pid)
            return (0 != win32api.TerminateProcess(handle, 0))
        except: # pywin not installed
            return True

#start the controller

controller = suicide(os.getpid())
controller.start()

#used by client
serverHost = sys.argv[1] # server address

serverPort = int(sys.argv[2]) # server serverPort

transferModality = sys.argv[3] #transferModality "UPLOAD" or "DOWNLOAD"

cacheVMEIdForUploadOrSizeForDownload = sys.argv[4] # vme cacheVMEIdForUploadOrSizeForDownload if UPLOAD , size if DOWNLOAD

currentCacheChildABSFolderWorkaround = sys.argv[5] # currentCacheChildABSFolderWorkaround dir

user = sys.argv[6] #user
password = sys.argv[7] #pwd

repositoryURL = sys.argv[8] #server url where upload files

originalVMEIdForUploadOrXMLURIForDownload = sys.argv[9] #original Id in UPLOAD, XML URI in DOWNLOAD
vmeHasLinkForUploadOrSRBDataURIForDownload = sys.argv[10] #vme has link in UPLOAD, SRB data URI in DOWNALOAD

if (transferModality == "UPLOAD"):
    uploadWithChildren = sys.argv[11] #vme with child (UPLOAD)
    XMLUploadedResourcesRollBackLocalFileName = sys.argv[12] #file for upload rollback (UPLOAD)
    remoteXMLResourceURI = sys.argv[13] #XML resource URI (UPLOAD)
    isLastResourceToUpload = sys.argv[14] #is last VME?(UPLOAD)
            
    name = ""
    count = 0
    for i in sys.argv:
      if(count > 14):
        if(name == ""):
          name = i
        else:
          name = name + ' ' + i #vme name if UPLOAD
      count = count + 1
      
else:
    # DOWNLOAD
    isLastResourceToDownload = sys.argv[11] #is last VME?(DOWNLOAD)
      
# Save a call-stack file for an integrity control
if (transferModality == "UPLOAD"):
    ctrlFile = open("UploadCallStack.txt","a")
    # Write all parameters
    #ctrlFile.write(serverHost + "\n")
    #ctrlFile.write(serverPort + "\n")
    #ctrlFile.write(transferModality + "\n")
    ctrlFile.write(cacheVMEIdForUploadOrSizeForDownload + "\n")
    ctrlFile.write(currentCacheChildABSFolderWorkaround + "\n")
    ctrlFile.write(user + "\n")
    #ctrlFile.write(password + "\n")
    ctrlFile.write(repositoryURL + "\n")
    ctrlFile.write(originalVMEIdForUploadOrXMLURIForDownload + "\n")
    ctrlFile.write(vmeHasLinkForUploadOrSRBDataURIForDownload + "\n")
    ctrlFile.write(uploadWithChildren + "\n")
    ctrlFile.write(XMLUploadedResourcesRollBackLocalFileName + "\n")
    ctrlFile.write(remoteXMLResourceURI + "\n")
    ctrlFile.write(isLastResourceToUpload + "\n")
    ctrlFile.write(name + "\n")
    ctrlFile.flush()
    ctrlFile.close()
elif (transferModality == "DOWNLOAD"):
    ctrlFile = open("DownloadCallStack.txt","a")
    ctrlFile.write(cacheVMEIdForUploadOrSizeForDownload + "\n")
    ctrlFile.write(currentCacheChildABSFolderWorkaround + "\n")
    ctrlFile.write(user + "\n")
    ctrlFile.write(repositoryURL + "\n")
    ctrlFile.write(originalVMEIdForUploadOrXMLURIForDownload + "\n")
    ctrlFile.write(vmeHasLinkForUploadOrSRBDataURIForDownload + "\n")
    ctrlFile.write(isLastResourceToDownload + "\n")
    ctrlFile.write("undefined" + "\n")
    ctrlFile.flush()
    ctrlFile.close()

# Simulate download corruption
#deleteBin = random.randint(0,5)
#deleteBin = 1
#if(deleteBin == 1):
#    while(1):
#        time.sleep(10)
    
# Used by a client to create upload download threads in ThreadedUploaderDownloader
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
# connect to server
mySocket.settimeout(20)
mySocket.connect((serverHost, serverPort))

# compose message
if (transferModality == "UPLOAD"):
    message = transferModality + ' ' + cacheVMEIdForUploadOrSizeForDownload + ' ' +\
    currentCacheChildABSFolderWorkaround + ' ' + user + ' ' + password + ' ' + repositoryURL + ' '  +\
    originalVMEIdForUploadOrXMLURIForDownload + ' ' + vmeHasLinkForUploadOrSRBDataURIForDownload + ' ' +\
    uploadWithChildren + ' ' + XMLUploadedResourcesRollBackLocalFileName + ' ' + remoteXMLResourceURI +' ' +\
    isLastResourceToUpload + ' ' + name
else:
    # DOWNLOAD
    message = transferModality + ' ' + cacheVMEIdForUploadOrSizeForDownload + ' ' +\
    currentCacheChildABSFolderWorkaround + ' ' + user + ' ' + password + ' ' + repositoryURL + ' '  +\
    originalVMEIdForUploadOrXMLURIForDownload + ' ' + vmeHasLinkForUploadOrSRBDataURIForDownload + ' ' +\
    isLastResourceToDownload
    
mySocket.sendall(message) # send message to server

# if stop signal, then leave loop
dataReceived = mySocket.recv(1024) # receive dataReceived from server (up to 1024 bytes)
if Debug:
    print dataReceived

mySocket.close() # close socket

controller.terminate = True