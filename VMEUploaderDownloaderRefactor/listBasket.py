from webServicesClient import xmlrpcDemoWS
import xml.dom.minidom as xd
import os, time
from lhpDefines import *
from Debug import Debug
import ConfigLoader

class listBasket:
    def __init__(self, fromSandbox):
        self.IdList = [] #create original list of basket vmes
        self.IdListSelected = [] #this list will be copied from handle object
        self.IdListParent = []
        self.IdListType = []   
        self.IdListHasChild = []
        self.fileName = "ToDownload.txt"
        self.fromSandbox = fromSandbox
        self.currentUser = ''
        self.currentPassword = ''
        self.ServiceURL = ''
        self.Result = None
        self.proxyHost = ""
        self.proxyPort = 0
        pass
    
    def SetCredentials(self, user , password):
        self.currentUser = user
        self.currentPassword = password
        
    def SetServiceURL(self, serviceURL):
        self.ServiceURL = serviceURL
    
    def getListFromBasket(self):
        self.removeIdListSelectedFile()
        
        self.proxyHost, self.proxyPort = retriveProxyParameters()
        
        ws = xmlrpcDemoWS.xmlrpc_demoWS()
        
        if self.ServiceURL == '':
            cl = ConfigLoader.ConfigLoader("",self.currentUser)
            ws.setServer(cl.ServiceURL)
        else:
            ws.setServer(self.ServiceURL)
        ws.setCredentials(self.currentUser, self.currentPassword)
        ws.ProxyURL = self.proxyHost
        ws.ProxyPort = self.proxyPort


        if Debug:
            print "->"+ ws.ProxyURL + "<-"
            print "->"+ str(ws.ProxyPort) + "<-"
        
        try:
            # standard info list
            if  self.fromSandbox == 0:
                self.Result = ws.run('listbasket')[1]
            elif self.fromSandbox == 1:
                self.Result = ws.run('listsandbox')[1] 
            # hierarchical info list
            elif self.fromSandbox  == 2:
                self.Result = ws.run('listbaskettree')[1] 
            elif self.fromSandbox == 3:
                self.Result = ws.run('listsandboxtree')[1]
            else :
                self.Result = ws.run('listsandboxsimple')[1] 
                
                
        except Exception, e:
            print "ERROR %s" % str(e)
        #print self.Result
        self.__createListFromReultingXML()
        #print self.IdList
        pass
    
    def __createListFromReultingXML(self):
        #add element to self.IdList
        #self.IdList.append("Id1.xml")
        #self.IdList.append("Id2.xml")
        dom = xd.parseString(self.Result)
        if dom.getElementsByTagName("fault"):
            print "-----Error in listbasket service---------"
            return
        count = 0
        for el in dom.getElementsByTagName("string"):
            if len(el.childNodes) == 0:
                    self.IdList.append(str('?'))
            for node in el.childNodes:
                self.IdList.append(node.data)
        pass
    
    def removeIdListSelectedFile(self):
        if(os.path.exists(self.fileName)):
            os.remove(self.fileName)
        pass

    def writeIdListSelectedOnFile(self):
        #create file with selected vme's from basket
        file = open(self.fileName,"w")
        count = 0
        for id in self.IdListSelected:
            file.write(str(id))
            if len(self.IdListParent)>0:
                file.write(" ")
                file.write(str(self.IdListParent[count]))
            if len(self.IdListType)>0:
                file.write(" ")
                file.write(str(self.IdListType[count]))
            if len(self.IdListHasChild)>0:
                file.write(" ")
                file.write(str(self.IdListHasChild[count]))
            count = count +1 
            if(id != self.IdListSelected[len(self.IdListSelected)-1]):
                file.write("\n")
        
        file.close()
        pass

def test():
    lb = listBasket(0)
    lb.getListFromBasket()
    print "Id List: " + str(lb.IdList)

if __name__ == '__main__':
  test()
