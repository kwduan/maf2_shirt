from webServicesClient import xmlrpcDemoWS
import xml.dom.minidom as xd
from lhpDefines import *
from Debug import Debug
import sys
import decimal
import ConfigLoader

class AvailableSpace:
    def __init__(self):
        self.currentUser = ''
        self.currentPassword = ''
        self.currentRepository = ''
        self.Result = None
        self.proxyHost = ""
        self.proxyPort = 0
        pass
        
    def SetCredentials(self, user , password):
        self.currentUser = user
        self.currentPassword = password
    def SetRepository(self, repository):
        if repository == '':
            self.currentRepository = self.user
        else:
            self.currentRepository = repository
        
    def getUserAvailableSpace(self):
        self.proxyHost, self.proxyPort = retriveProxyParameters()
        # Get the service url from the configuration file
        cl = ConfigLoader.ConfigLoader("",self.currentRepository)
        ws = xmlrpcDemoWS.xmlrpc_demoWS()
        ws.setServer(cl.ServiceURL)
        ws.setCredentials(self.currentUser, self.currentPassword)
        ws.ProxyURL = self.proxyHost
        ws.ProxyPort = self.proxyPort


        if Debug:
            print "->"+ ws.ProxyURL + "<-"
            print "->"+ str(ws.ProxyPort) + "<-"
        
        try:
            self.Result = ws.run('getuseravailablespace')
                
        except Exception, e:
            print "ERROR %s" % str(e)
        
        #print self.Result
        if(self.Result[0] == True):
            print self.__GetResult()
            return self.__GetResult()
        else:
            return None
        
    def __GetResult(self):
        #add element to self.IdList
        #self.IdList.append("Id1.xml")
        #self.IdList.append("Id2.xml")
        dom = xd.parseString(self.Result[1])
        if dom.getElementsByTagName("fault"):
            print "-----Error in getuseravailablespace service---------"
            return
        for el in dom.getElementsByTagName("string"):
            for node in el.childNodes:  
                return node.data

def main():
    user = sys.argv[1] 
    password = sys.argv[2] 
    repository = sys.argv[3]
    #needed = decimal.Decimal(sys.argv[3])
    iAS = AvailableSpace()
    iAS.SetCredentials(user,password)
    iAS.SetRepository(repository)
    #result = decimal.Decimal(iAS.getUserAvailableSpace())
    result = iAS.getUserAvailableSpace()
    if Debug:
        print "Available free space is: " + str(result) + "byte"
    resFile = open("lhpAvailableFreeSpaceOnServer.txt","w")
    resFile.write(result)
    resFile.close()

if __name__ == '__main__':
  main()
        