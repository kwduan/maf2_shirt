#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Stefano Perticoni, Roberto Mucci
#-----------------------------------------------------------------------------

import fileUtilities
import sys, string
import msfParser
import UploadHandler
import shutil
import sets
import os
import re
import csv
import datetime, time
import hashlib
from Debug import Debug
from xml.dom import minidom
from xml.dom import Node
 
class OutgoingDirCreator:
    """Create outgoing cache dir for the VME to be uploded containing:  
    1) a vme xml file (built from an MSF given the vme ID) 
    2) vme binary data (if present)
    
    Default name for written vme XML file is exportedVME.up
    """
    
    def __init__(self):                       
        
        self.InputMSFDirectory = "No input msf cache directory specified"
        self.VmeToExtractID = 1    
        self.originalId = 1
        self.OutputVMEXMLName = "exportedVME.up" 
        self.OutputFolderName = "No outgoing folder directory specified"
        self.DatasetURI = "No dataset URI specified"
        self.vmeName = ""
        self.hasLink = ""
        self.withChild = ""
        self.hasBinary = False
        self.UploadFullMSF = False
        self.binaryName = ''
        self.localChksum = ""
        self.user = ""
        self.tagsToBeRemovedLocalFileName = "tagsToBeRemovedFromUploadedVMETagArray.txt"
        
    def CreateOutgoinDirAndReturnLocalMD5Checksum(self):
        self.__Parse()
        return self.localChksum
        
    def __Parse(self):
    
        msfDOMParserInstance = msfParser.msfParser()
       
        # Search the MSF file inside given the given directory
        vmeUploaderDownloaderDir = os.getcwd()
        os.chdir(self.InputMSFDirectory)
        files = os.listdir(self.InputMSFDirectory)
        
        if Debug:
            print "Directory: " + self.InputMSFDirectory + " contains: "
            print files
        msfFileNameList = []
        for file in files:
            if re.search('\\.msf$',file):
               msfFileNameList.append(file)
        assert(len(msfFileNameList)  == 1)
        
        msfFileName = msfFileNameList[0]
        domDocument = minidom.parse(msfFileName)
        msfRootNode = domDocument.documentElement
        
        if Debug:
            print "\ninput MSF Directory: " + self.InputMSFDirectory
            print "\ninput MSF filename: " + msfFileName
            print "\nExtracting vme with ID: " + str(self.VmeToExtractID) + '\n' 
            """parse the msf extracting tags from the UnhandledPlusManualTagsList file """
        
        rootNode = msfRootNode
        vmeId = self.VmeToExtractID
        
        if (str(vmeId) == "-1"):
            # get the root node
            outVmeNode = msfDOMParserInstance.GetRootNode(rootNode)
        else:
            # get the vme node
            outVmeNode = msfDOMParserInstance.GetVmeNodeById(rootNode, vmeId)
        
        
        # get the tagArray node
        outVmeTagArrayNode = msfDOMParserInstance.GetVmeTagArrayNode(outVmeNode)
        
        #AUTO TAGS SET  WITH  WEBSERVICE
        nodeURI = msfDOMParserInstance.GetTagNodeByTagName(outVmeTagArrayNode, "L0000_resource_data_Dataset_DatasetURI")
        msfDOMParserInstance.SetTagNodeText(nodeURI, self.DatasetURI)       
        text = msfDOMParserInstance.GetTagNodeText(nodeURI)
        if Debug:
            print text
        
        today = datetime.date.today()
        nodeURI = msfDOMParserInstance.GetTagNodeByTagName(outVmeTagArrayNode, "L0000_resource_data_Dataset_UploadDate")
        msfDOMParserInstance.SetTagNodeText(nodeURI, today)       
        day = msfDOMParserInstance.GetTagNodeText(nodeURI)
        if Debug:
            print day
            
        nodeURI = msfDOMParserInstance.GetTagNodeByTagName(outVmeTagArrayNode, "L0000_resource_data_Ownership_OwnerID")
        msfDOMParserInstance.SetTagNodeText(nodeURI, self.user)    
        user = msfDOMParserInstance.GetTagNodeText(nodeURI)
        if Debug:
            print user
        
        if (self.withChild == "true"):   
            newDir = os.getcwd()
            os.chdir(sys.path[0])
            #cicle on file with list of child URI
            list = ''
            fileName = str(self.vmeName)
            fileName = fileName.replace('/', '_')
            fileName = fileName.replace('\\', '_')
            fileName = fileName.replace(':', '_')
            fileName = fileName.replace('*', '_')
            fileName = fileName.replace('?', '_')
            fileName = fileName.replace('\"', '_')
            fileName = fileName.replace('<', '_')
            fileName = fileName.replace('>', '_')
            fileName = fileName.replace('!', '_')
            fileName = fileName.replace('|', '_')

            fileName = fileName + str(self.originalId) + ".childURI"
                
            if Debug:
                print "file name: " + fileName 
            if(os.path.exists(fileName)):
                file = open(fileName ,"r")
                if Debug:
                    print "file opened: " + fileName
                for line in file.readlines():
                    line = line.replace("\n", ' ')
                    list += line                
                    
                nodeURI = msfDOMParserInstance.GetTagNodeByTagName(outVmeTagArrayNode, "L0000_resource_MAF_TreeInfo_VmeChildURI1")
                msfDOMParserInstance.SetTagNodeText(nodeURI, list)       
                childURI = msfDOMParserInstance.GetTagNodeText(nodeURI)
                    
                if Debug:
                    print childURI 
                file.close()
                os.remove(fileName)
                os.chdir(newDir)
            else:
                print fileName + ' not Found'
                errorFile = open(sys.path[0] + '\\ErrorFound.lhp', 'a')
                errorFile.write(fileName + ' not Found\n')
                errorFile.close() 
                return 
                
                
        if (self.hasLink == "true" and self.UploadFullMSF == False):   
            newDir = os.getcwd()
            os.chdir(sys.path[0])
            #cicle on file with list of link URI
            list = ''
            fileName = self.vmeName 
            fileName = str(self.vmeName)
            fileName = fileName.replace('/', '_')
            fileName = fileName.replace('\\', '_')
            fileName = fileName.replace(':', '_')
            fileName = fileName.replace('*', '_')
            fileName = fileName.replace('?', '_')
            fileName = fileName.replace('\"', '_')
            fileName = fileName.replace('<', '_')
            fileName = fileName.replace('>', '_')
            fileName = fileName.replace('!', '_')
            fileName = fileName.replace('|', '_')
            fileName = fileName + str(self.originalId) + ".linkURI"
            
            if(os.path.exists(fileName)):
                file = open(fileName,"r")

                for line in file.readlines():
                    line = line.replace("\n", ' ')
                    list += line                
                    
                nodeURI = msfDOMParserInstance.GetTagNodeByTagName(outVmeTagArrayNode, "L0000_resource_MAF_Procedural_VMElinkURI1")
                msfDOMParserInstance.SetTagNodeText(nodeURI, list)       
                linkURI = msfDOMParserInstance.GetTagNodeText(nodeURI)
                if Debug:
                    print linkURI 
                file.close()
                os.remove(fileName)
                os.chdir(newDir)
            else:
                print fileName + ' not Found'
                errorFile = open(sys.path[0] + '\\ErrorFound.lhp', 'a')
                errorFile.write(fileName + ' not Found\n')
                errorFile.close() 
                return 
            
        #read the Tags black list file: these tags will be removed from uploaded XML
        blackListFileName = vmeUploaderDownloaderDir + "\\" + self.tagsToBeRemovedLocalFileName
        
        try:
            
            blackList = []
        
            f = open(blackListFileName,'r') 
            for line in f: 
                blackList.append(str(line).strip())
            f.close()
            
            msfDOMParserInstance.RemoveTagsByList(outVmeTagArrayNode, blackList)
        
        except:
            
            print "Problems removing tags, check for " + str(blackListFileName) + " file existence"
            pass
                
        # create output directory
        fileUtilities._mkdir(self.OutputFolderName)
        
        # save created XML to this directory
        os.chdir(self.OutputFolderName)    
        oldDir = os.getcwd()
        os.chdir(self.InputMSFDirectory)
        
        if(self.hasBinary == True):
            if Debug:
                print self.InputMSFDirectory
            os.chdir(self.InputMSFDirectory)
            shutil.copyfile(self.binaryName,self.OutputFolderName + "\\" + self.binaryName)
            
            #-----MD5 checksum calculation-----#
            os.chdir(self.OutputFolderName)
            if Debug:
                print self.OutputFolderName
            self.localChksum = self.md5(self.binaryName)
            if Debug:
                print "Local checksumII= " + self.localChksum
            
            #AUTO TAGS SET  WITH  WEBSERVICE      
            nodeURI = msfDOMParserInstance.GetTagNodeByTagName(outVmeTagArrayNode, "L0000_resource_data_Dataset_LocalFileCheckSum")
            msfDOMParserInstance.SetTagNodeText(nodeURI, self.localChksum)       
            chksum = msfDOMParserInstance.GetTagNodeText(nodeURI)
            if Debug:
                print chksum

        
        os.chdir(oldDir)
        newDoc = minidom.Document()
        newDoc.appendChild(outVmeNode)
        outFileXML = open(self.OutputVMEXMLName, 'w')
        newDoc.writexml(outFileXML)

        outFileXML.close()
        

        if Debug:
            print "\nWritten output XML file " + self.OutputVMEXMLName + " in directory " + self.OutputFolderName

    
    def md5(self,fileName):
        #Compute md5 hash of the specified file
        m = hashlib.md5()
        try:
            fd = open(fileName,"rb")
            fd.close()
        except IOError:
            if Debug:
                print "Unable to open the file in read mode: " + fileName
            return
        for eachLine in open(fileName,"rb"):
            m.update(eachLine)
        return m.hexdigest()

        
def run(inputMSFDirectory, vmeToExtractId, outputFolderName, outputVMEXMLName):                                            
    upl = OutgoingDirCreator()
    upl.InputMSFDirectory = inputMSFDirectory
    upl.VmeToExtractID = int(vmeToExtractId)
    upl.OutputFolderName = outputFolderName
    upl.OutputVMEXMLName = outputVMEXMLName
    upl.Parse()    

def main():
    args = sys.argv[1:]
    if len(args) != 4:
        print """
        usage: python.exe vmeUploader.py 
        inputMSFDirectory vmeToExtractId 
        autoTagsFile.csv manualTagsFile.csv
        outputFolderName outputVMEXMLName.xml
        """
        sys.exit(-1)
    run(args[0],args[1],args[2],args[3])


if __name__ == '__main__':
    main()
