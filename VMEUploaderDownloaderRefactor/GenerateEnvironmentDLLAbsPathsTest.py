import os
import sys
import unittest
import commands
import time

class GenerateEnvironmentDLLAbsPathsTest(unittest.TestCase):
    
    def setUp(self):  
        pass 
    
    def test(self):
    
        CurrentDirectoryTest = sys.path[0] or os.getcwd()
        
        if os.path.exists(CurrentDirectoryTest + "\\EnvironmentDLLAbsPaths.ini"):
            os.remove(CurrentDirectoryTest + "\\EnvironmentDLLAbsPaths.ini")
        
        if os.path.exists(CurrentDirectoryTest + "\\EnvironmentDLLRelPaths.ini"):
            
            import __main__            
            execfile("GenerateEnvironmentDLLAbsPaths.py" , __main__.__dict__)           
            
            assert os.path.exists(CurrentDirectoryTest + "\\EnvironmentDLLAbsPaths.ini")            
            os.remove(CurrentDirectoryTest + "\\EnvironmentDLLAbsPaths.ini")

        
if __name__ == '__main__':
    unittest.main()
