import unittest , os, time
import MtomSRBSize

class MtomSRBSizeTest(unittest.TestCase):
    """"""
    def testSRBSize(self):
          
          proxyHost = ""
          proxyPort = 0
          remoteSRBDataName = 'data_12094'
          serviceURI = 'https://ws-lhdl.cineca.it/mafSRBSize.cgi'
          
          remoteFileSize = 847
                
          instance = MtomSRBSize.MtomSize()
          fileSizeFromService = instance.ListSrbDir(remoteSRBDataName, serviceURI, proxyHost, proxyPort)
          
          self.assertAlmostEqual(fileSizeFromService, remoteFileSize)
          
if __name__ == '__main__':
    unittest.main()