import unittest, shutil
import WSUpload,WSCreateResource
import time
from hashlib import md5

def ComputeMD5Checksum(fileName):
    """Compute md5 hash of the specified file"""
    
    m = md5()
    try:
        fd = open(fileName,"rb")
    except IOError:
            print "Unable to open the file in readmode:", fileName
            return
    content = fd.readlines()
    fd.close()
    for eachLine in content:
        m.update(eachLine)
    return m.hexdigest()
    
class WSUploadTest(unittest.TestCase):
    """"""
    def testUploadAndEstimateBandwidth(self):
        testFile = "..\\vmeUploaderTestData\\uploadSpeedProbeData\\uploadSpeedProbeData.vtk"
        testFileSize = 69.462
        
        srbGetFreeResourceServiceURI = 'https://ws.biomedtown.org/WSCreateResource.cgi'
        srbUploadResourceServiceURI = 'https://ws.biomedtown.org/WSUpload.cgi'
        
        binaryURI = "NOT PRESENT"
        
        # your proxy host here:
        proxyHost = ""
        
        # your proxy port here:
        proxyPort = 0
        
        if (proxyHost == "" and proxyPort == 0):
            print ""
            print "Not using PROXY"
        
        else:
            
            print "using PROXY:"
            print "-> proxyHost: " + str(proxyHost) + "<-"
            print "-> proxyPort: " + str(proxyPort) + "<-"
            print ""
        
        srbClient = WSCreateResource.WSCreateResource()
        srbFreeResource = srbClient.CreateResource(srbGetFreeResourceServiceURI, proxyHost, proxyPort)
        
        print ""
        print "Test file to be uploaded is: " + testFile
        print "Created binary resource: " + srbFreeResource
        print ""
        

        
        shutil.copyfile(testFile,srbFreeResource)
        srbUploader = WSUpload.WSUpload()
        
        startT =  time.time()
       
        srbUploadResult = srbUploader.Upload(srbFreeResource,\
        srbUploadResourceServiceURI,proxyHost, proxyPort)
        
        endT = time.time()
        
        tElapsed = endT-startT
        
        speed = testFileSize / tElapsed
        
        print "estimated upload speed: " + str(speed) + " KB/s"
        
        print "binary upload result: " + str(srbUploadResult)
        
        shutil.move(srbFreeResource, testFile)
        
        remoteChecksum = srbUploadResult.chksum
        remoteFileName = srbUploadResult.uriFile
                
        localChecksum = ComputeMD5Checksum(testFile).upper()
        
        print ""
        print "local cheksum: " + str(localChecksum) 
        print "remote cheksum: " + str(remoteChecksum)
        print "remoteFileName: " + str(remoteFileName)
        print ""
        
        success = False
        if localChecksum == remoteChecksum:
           print "Binary Upload  successful :-)"
           success = True
        else:
           print "Binary Upload  not completed successfully :-(... something is wrong!"
           success = False
           
        print ""
        self.assertTrue(success)

if __name__ == '__main__':
    unittest.main()
