import unittest, shutil
import WSUpload,WSCreateResource, WSDownload, WSDelete
import time
from hashlib import md5

from WSUploadTest import ComputeMD5Checksum 

import os, commands, md5

class WSDeleteTest(unittest.TestCase):
    """"""
    
    def testDelete(self):
    
        testFile = "..\\vmeUploaderTestData\\uploadSpeedProbeData\\uploadSpeedProbeData.vtk"
        testFileSize = 69.462
        
        serverURL = 'http://ws.biomedtown.org/'
    
        serviceGetFreeResourceServiceURI = serverURL+'WSCreateResource.cgi'
        serviceUploadResourceServiceURI = serverURL+'WSUpload.cgi'
        serviceWSDownloadURI = serverURL+'WSDownload.cgi'
        serviceWSDeleteURI = serverURL+'WSDelete.cgi'
        
        
        
        # your proxy host here:
        proxyHost = ""
        
        # your proxy port here:
        proxyPort = 0
        
        if (proxyHost == "" and proxyPort == 0):
            print ""
            print "Not using PROXY"
        
        else:
            
            print "using PROXY:"
            print "-> proxyHost: " + str(proxyHost) + "<-"
            print "-> proxyPort: " + str(proxyPort) + "<-"
            print ""
        
        #1 Create Resource
        
        srbClient = WSCreateResource.WSCreateResource()
        srbFreeResource = srbClient.CreateResource(serviceGetFreeResourceServiceURI, proxyHost, proxyPort)

        print "Created binary resource: " + srbFreeResource
        
        
        #2 Upload Binary
        
        shutil.copyfile(testFile,srbFreeResource)
        srbUploader = WSUpload.WSUpload()
        
        startT =  time.time()
       
        srbUploadResult = srbUploader.Upload(srbFreeResource,\
        serviceUploadResourceServiceURI,proxyHost, proxyPort)
        
        endT = time.time()
        
        tElapsed = endT-startT
        
        speed = testFileSize / tElapsed
        
        shutil.move(srbFreeResource, testFile)
        
        remoteChecksum = srbUploadResult.chksum
        remoteFileName = srbUploadResult.uriFile
                
        localChecksum = ComputeMD5Checksum(testFile).upper()
        
        success = False
        if localChecksum == remoteChecksum:
           print "UPLOAD OK"
           success = True
        else:
           print "UPLOAD KO"
           success = False
           

        
        self.assertEqual(success,True)
        
        
        #4 Test Download OK
        
        transferredFileRemoteSRBName = srbFreeResource        
        transferredFileSize = testFileSize   
        
        self.assertEqual(WSDownload.WSDownload().Download(transferredFileRemoteSRBName,serviceWSDownloadURI,proxyHost, proxyPort), True)
        
        f1 = open(testFile, 'rb')
        f2 = open(transferredFileRemoteSRBName, 'rb')

        if md5.new(f1.read()).digest() ==  md5.new(f2.read()).digest() :
            print "DOWNLOAD OK" 
        f1.close()
        f2.close()

        #3 Delete

        self.assertEqual(WSDelete.WSDelete().Delete(srbFreeResource,serviceWSDeleteURI,proxyHost, proxyPort), True)
        

        
        
        #5 Test Download FAILED

        transferredFileRemoteSRBName = srbFreeResource        
        transferredFileSize = testFileSize   
        
        self.assertEqual(WSDownload.WSDownload().Download(transferredFileRemoteSRBName,serviceWSDownloadURI,proxyHost, proxyPort), False)

        print "DELETE OK"


if __name__ == '__main__':
    unittest.main()
