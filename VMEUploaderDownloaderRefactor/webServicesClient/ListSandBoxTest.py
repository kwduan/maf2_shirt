import unittest
import xmlrpcDemoWS
import os, commands, md5
import xml.dom.minidom as xd

class ListSandboxTest(unittest.TestCase):
    """"""
    def test(self):

        ws = xmlrpcDemoWS.xmlrpc_demoWS()
        ws.setServer("https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2")
        ws.setCredentials("testuser","6w8DHF")

        #res = "<?xml version='1.0'?>\n<methodResponse>\n<params>\n<param>\n<value><array><data>\n<value><string>Data_426.xml</string></value>\n<value><string>prova_benincasa</string></value>\n</data></array></value>\n</param>\n</params>\n</methodResponse>\n"

        print "list sandbox service"
        out = ws.run('listsandbox')
        self.assertEqual(out[0], True)
        #self.assertEqual(out[1], res)
        
        resourceList = []
        dom = xd.parseString(out[1])
        if dom.getElementsByTagName("fault"):
            print "-----Error in listbasket service---------"
            self.assertTrue(False)
            return
        for el in dom.getElementsByTagName("string"):
            for node in el.childNodes:  
                resourceList.append(node.data)

        print resourceList
        
        if (len(resourceList) == 0):
            print "-----Resource list is empty (something is wrong...)---------"
            self.assertTrue(False)
        print "done!"


if __name__ == '__main__':
    unittest.main()
