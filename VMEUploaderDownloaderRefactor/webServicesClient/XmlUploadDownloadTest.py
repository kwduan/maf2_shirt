import unittest
import xmlrpcDemoWS
import os, commands, md5

class XmlUploadDownloadTest(unittest.TestCase):
    """"""
    def testUploadDownload(self):

        ws = xmlrpcDemoWS.xmlrpc_demoWS()
        ws.ServerURL = 'https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2'
        
        username = 'testuser'
        password = '6w8DHF'
        
        ws.setCredentials(username, password)
        ws.setServer('https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2')        
        ws.ProxyURL = ""
        ws.ProxyPort = 0
        
        
        testFileName = "testXml.msf"
        
        file = open(testFileName, 'w')
        file.write("useless file to test upload")
        file.close()
        
#        #delete
#        print ""
#        print "delete remote " + testFileName + "..."
#        try:
#            ws.run('xmldelete', testFileName)
#        except:
#            
#            print "problems removing remote " + testFileName + "..."
#            self.assertTrue(False)
#            return
#        
#        print ""
#        print "done!"

        #upload
        print "upload " + testFileName + "..."
        out = ws.run('xmlupload', testFileName)
        self.assertEqual(out[0], True)
        print "done!"
        
        #copy
        testFileOLD = testFileName + "OLD"
        try:
            print "try to remove local " + testFileOLD + "..."
            os.remove(testFileOLD)
        except:
            pass
        print "rename local " + testFileName + " as " + testFileOLD
        os.renames(testFileName, testFileOLD)
        print "done!"
        
        #download
        print "download remote " + testFileName + "..."
        out = ws.run('xmldownload', testFileName)
        self.assertEqual(out, True)
        print "done!"
        
        #md5
        print "md5 checking..."
        f1 = open(testFileName, 'rb')
        f2 = open(testFileOLD, 'rb')
        self.assertEqual(md5.new(f1.read()).digest(), md5.new(f2.read()).digest())
        f1.close()
        f2.close()
        print "done!"
            
if __name__ == '__main__':
    unittest.main()
