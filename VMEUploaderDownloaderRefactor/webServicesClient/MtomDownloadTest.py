import unittest
from MtomDownload import MtomDownload
import os, commands, md5

class MtomDownloadTest(unittest.TestCase):
    """"""
    
    def testDownload(self):
        
        # transfer source 
        transferredFileLocalName = "..\\vmeUploaderTestData\\uploadSpeedProbeData\\uploadSpeedProbeData.vtk"
        
        # transfer target
        transferredFileRemoteSRBName = "data_11513"        
        transferredFileSize = 69.462
        
        serviceUrl = 'https://ws-lhdl.cineca.it/mafSRBDownload.cgi'
        
        # your proxy host here:
        proxyHost = ""
        
        # your proxy port here:
        proxyPort = 0
        
        if (proxyHost == "" and proxyPort == 0):
            print ""
            print "Not using PROXY"
        
        else:
            
            print "using PROXY:"
            print "-> proxyHost: " + str(proxyHost) + "<-"
            print "-> proxyPort: " + str(proxyPort) + "<-"
            print ""
        
        
        self.assertEqual(MtomDownload().Download(transferredFileRemoteSRBName,serviceUrl,proxyHost,\
        proxyPort), True)
        
        f1 = open(transferredFileLocalName, 'rb')
        f2 = open(transferredFileRemoteSRBName, 'rb')
        self.assertEqual(md5.new(f1.read()).digest(), md5.new(f2.read()).digest())
        f1.close()
        f2.close()

if __name__ == '__main__':
    unittest.main()
