#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Roberto Mucci   <r.mucci@cineca.it>
#-----------------------------------------------------------------------------

import fileUtilities
import sys, string
import msfParser
import msfLXMLParser
import shutil
import sets
import os
import re
import csv
import datetime
import time
from Debug import Debug
from xml.dom import minidom
from xml.dom import Node
import lxml.etree as ElementTree

import urllib, urllib2, base64, re, os, cookielib, sys
from HttpsProxy import *

class lhpEditVMETag:
    """Edit VME tags and save them into original msf"""    
    
    def __init__(self):
        curDir = sys.path[0]
        self.InputCacheDirectory = "No input cache dir specified"
        self.InputMSFDirectory = "No input msf dir specified"
        self.msfFileName = "No MSF Name specified"
        self.UnhandledPlusManualTagsListFileName = "No UnhandledPlusManualTagsListFile! specified"
        self.HandledAutoTagsListFileName = "No HandledAutoTagsListFile! specified" 
        self.VmeToExtractID = 1
        self.OutputFolderName = "No output msf dir specified"
        self.OutputVMEXMLName = "OutputXML.xml"  #xml with tag edited
        self.OutputMSFFileName = "OutputMSF.lhp"
        self.FakeRootMSFFileName = "FakeRoot"
        self.FakeMSFFileName = "FakeMSF"
        
        self.DatasetURI = ""

        
    def ParseInput(self,id,multiIds):
        #Create an xml from msf to edit
        
        print "multiIds = "+str(multiIds)
        
        #msfDOMParserInstance = msfParser.msfParser()
        msfLXMLParserInstance = msfLXMLParser.msfLXMLParser()

        #Search the MSF file given the InputMSFDirectory directory
        # os.chdir(self.InputMSFDirectory)
        # files = os.listdir(self.InputMSFDirectory)
        
        # if Debug:
            # print "Directory: " + self.InputMSFDirectory + " contains: "
            # print files
        # msfFileNameList = []
        # for file in files:
            # if re.search('\\.msf$',file):
               # msfFileNameList.append(file)
        
        # self.msfFileName = msfFileNameList[0]
        # if Debug:
            # print self.msfFileName
        
        # msfFileDOMDocument = minidom.parse(self.msfFileName) // Changed to use lxml library
        #msfElementTree = ElementTree.parse(self.msfFileName)
        #msfRootNode = msfFileDOMDocument.documentElement
        msfRootNode = self.msfElementTree.getroot()
 
        if Debug:
            print "\ninput MSF Directory: " + self.InputMSFDirectory
            print "\ninput MSF filename: " + msfFileNameList[0]
            print "\nlhdl dictionary: " + self.UnhandledPlusManualTagsListFileName + '\n'
            print "\nExtracting vme with ID: " + str(id) + '\n' 
            """parse the msf extracting tags from the UnhandledPlusManualTagsList file """
        
        rootNode = msfRootNode
        vmeId = id
        
        if (vmeId == str(-1)):
            # get the root node
            outVmeNode = msfLXMLParserInstance.GetRootNode(rootNode)
        else:
            # get the vme node
            outVmeNode = msfLXMLParserInstance.GetVmeNodeById(rootNode, int(vmeId))

        # get the tagArray node
        print "<<<<<<<VME ID : " + str(vmeId)
        print msfRootNode
        outVmeTagArrayNode = msfLXMLParserInstance.GetVmeTagArrayNode(outVmeNode)
        
        #print ElementTree.tostring(outVmeTagArrayNode)
        #try:
        #    for node in outVmeTagArrayNode:
        #        pass
        #except:
        #    outVmeTagArrayNode = ElementTree.Element("Item") #Support empty tags vme
        #    outVmeTagArrayNode.set("Type","mafTagArray")
        #    outVmeTagArrayNode.set("NumberOfTags","0")
        #    outVmeNode.append(outVmeTagArrayNode)
        # get the tags list from vme tag array node

        vmeTagList = msfLXMLParserInstance.GetTagNames(outVmeTagArrayNode)

        if Debug:            
            print vmeTagList

        
        # AUTO TAGS
           
        if Debug:
            print self.HandledAutoTagsListFileName
        
        HandledAutoTagsListFileNameHelp = ""
        if multiIds:
          HandledAutoTagsListFileNameHelp = self.InputCacheDirectory + "\\" + str(vmeId) + "\\" + self.HandledAutoTagsListFileName
        else:
          HandledAutoTagsListFileNameHelp = self.HandledAutoTagsListFileName
        
        csv.field_size_limit(sys.maxint)
        autoTagsReader = csv.reader(open(HandledAutoTagsListFileNameHelp, "r"))
    
        autoTagsDictionary = {}
         
        try:
            for row in autoTagsReader:
                if Debug:                   
                    print "row: " +  str(row)
                autoTagsDictionary[row[0]] = str(row[1])
                
        except csv.Error, e:
            sys.exit('file %s, line %d: %s' % (HandledAutoTagsListFileNameHelp, autoTagsReader.line_num, e))
       
        if Debug:          
            print autoTagsDictionary

        
        autoTagsList = sorted(autoTagsDictionary.keys())
        if Debug:
            print autoTagsList


        # remove old auto tags from tagArray
        msfLXMLParserInstance.RemoveTagsByList(outVmeTagArrayNode, autoTagsList)
    
        # add new and updated autotags
        # "TAGNAME" , "TAGTEXT"
        #msfDOMParserInstance.AddTagsFromDictionary(msfFileDOMDocument,outVmeTagArrayNode, autoTagsDictionary)
        msfLXMLParserInstance.AddTagsFromDictionary(outVmeTagArrayNode, autoTagsDictionary)
    
        # UNHANDLED AUTO + MANUAL TAGS
        
        # import unhandled auto + manual csv
        
        # create a list of keys
        
        if multiIds:
          UnhandledPlusManualTagsListFileNameHelp = self.InputCacheDirectory + "\\" + str(vmeId) + "\\" + self.UnhandledPlusManualTagsListFileName
        else:
          UnhandledPlusManualTagsListFileNameHelp = self.InputCacheDirectory + "\\" + self.UnhandledPlusManualTagsListFileName
        
        unhandledPlusManualTagsReader = csv.reader(open(UnhandledPlusManualTagsListFileNameHelp, "r"))     
    
        unhandledPlusManualTagsDictionary  = {}

        try:
            for row in unhandledPlusManualTagsReader:
                if Debug:                   
                    print row
                unhandledPlusManualTagsDictionary[row[0]] = str(row[1])
                
        except csv.Error, e:
            sys.exit('file %s, line %d: %s' % (UnhandledPlusManualTagsListFileNameHelp, unhandledPlusManualTagsReader.line_num, e))

        if Debug:          
            print unhandledPlusManualTagsDictionary  

        unhandledPlusManualTagsList = unhandledPlusManualTagsDictionary.keys()
        unhandledPlusManualTagsList.sort()

        #unhandledPlusManualTagsList = sorted(unhandledPlusManualTagsDictionary.keys())
        #print unhandledPlusManualTagsList 
    
        # find matching manual and unhandled manual tags between dictionary and vme tagArray
        vmeTagListSet = set(vmeTagList)

        if Debug:
            print vmeTagListSet
        
        unhandledPlusManualTagsListSet = set(unhandledPlusManualTagsList) 
        # print unhandledPlusManualTagsListSet
        
        tagsToBeExported = vmeTagListSet.intersection(unhandledPlusManualTagsListSet)
        # print "\nThese tags will be exported: \n" + str(tagsToBeExported)
        
        tagsToBeAnnotatedManuallySet = unhandledPlusManualTagsListSet.difference(vmeTagList)
       
        # Add tags to be annotated manually from list
        isinstance(tagsToBeAnnotatedManuallySet,set)

        #tagsToBeAnnotatedManuallySorted = sorted(tagsToBeAnnotatedManuallySet)
        tagsToBeAnnotatedManuallySorted = list(tagsToBeAnnotatedManuallySet)
        tagsToBeAnnotatedManuallySorted.sort()
        
        tagsToBeAnnotatedManuallyMap = {}
       
        # for each item in set
        for key in tagsToBeAnnotatedManuallySorted:
            
            # create a map item
            tagsToBeAnnotatedManuallyMap[key] = unhandledPlusManualTagsDictionary[key] 
        
        if Debug:
            print "tagsToBeAnnotatedManuallyMap: "
            print tagsToBeAnnotatedManuallyMap
        

        #msfDOMParserInstance.AddTagsFromDictionary(msfFileDOMDocument,outVmeTagArrayNode, tagsToBeAnnotatedManuallyMap)
        msfLXMLParserInstance.AddTagsFromDictionary(outVmeTagArrayNode, tagsToBeAnnotatedManuallyMap)
                

        tagsToBeRemoved  = vmeTagListSet.difference(tagsToBeExported)
        # print "\nThese tags will be removed from output vme XML: \n" + str(tagsToBeRemoved)
    
        a = list(tagsToBeRemoved)
        if Debug:
            print a
        
        # create output directory
        fileUtilities._mkdir(self.OutputFolderName) 
        
        # save created XML to this directory
        os.chdir(self.OutputFolderName)
        
        newDoc = ElementTree.ElementTree()
        newDoc._setroot(outVmeNode)
        #print newDoc
        outFileXML = open(self.OutputVMEXMLName, 'w')
        outFileXML.write(ElementTree.tostring(newDoc,pretty_print=True))
        #newDoc.write(outFileXML,pretty_print=True)
        outFileXML.close()
        
        if Debug:
            print "\nWritten XML file " + self.OutputVMEXMLName + " with tag edited in directory " + self.OutputFolderName
        #sys.exit(0)
        
        
    def BuildOutputMSF(self):
        
        # Search the MSF file given the InputMSFDirectory directory
        os.chdir(self.InputMSFDirectory)
        files = os.listdir(self.InputMSFDirectory)
        
        if Debug:
            print "Directory: " + self.InputMSFDirectory + " contains: "
            print files
        msfFileNameList = []
        for file in files:
            if re.search('\\.msf$',file):
               msfFileNameList.append(file)
        
        self.msfFileName = msfFileNameList[0]
        if Debug:
            print self.msfFileName

        
        print "MSF:" + self.msfFileName
        self.msfElementTree = ElementTree.parse(self.msfFileName)
        
        for id in self.VmeToExtractID:
          print "ID = " + str(id)
          self.ParseInput(id,len(self.VmeToExtractID)!=1)
        
          self.InputVMEXMLFileName = self.OutputVMEXMLName
          
          domP = msfLXMLParser.msfLXMLParser()
      
          #  Import the fake root tree
          importedMSFDocument = ElementTree.parse(self.FakeRootMSFFileName)
          fakeRootNode = importedMSFDocument.getroot()
          
          #  Import the fake MSF tree
          importedMSFDocument = ElementTree.parse(self.FakeMSFFileName)
          fakeMSFNode = importedMSFDocument.getroot()
         
          # Open the imported VME XML
          importedVMEDocument = ElementTree.parse(self.InputVMEXMLFileName)
          importedVmeNode = importedVMEDocument.getroot()

          msfOutputDoc = ElementTree.ElementTree()
          #newDoc._setroot(outVmeNode)
          if (id != str(-1)):
              msfOutputDoc._setroot(fakeRootNode)
              outputDoc = msfOutputDoc.getroot()
              childrenNode = domP.GetNodeByNodeName(outputDoc,"Children") 
              # append the imported XML to the Children node
              childrenNode.append(importedVmeNode)
          else:
              msfOutputDoc._setroot(fakeMSFNode)
              outputDoc = msfOutputDoc.getroot()
              # create the msfParser
              childrenNode = domP.GetMSFNode(outputDoc) 
              # append the imported XML to the Children node
              childrenNode.append(importedVmeNode)

              
          # create output directory
          fileUtilities._mkdir(self.OutputFolderName)
          
          # save created XML to this directory
          os.chdir(self.OutputFolderName)
          
          
          # write to XML
          # outMSFFile = ""
          # if len(self.VmeToExtractID)!=1:
            # outMSFFile = open(self.InputCacheDirectory+"\\" + str(id) + "\\"+self.OutputMSFFileName, 'w')
            # print "created files " + self.InputCacheDirectory+"\\" + str(id) + "\\"+self.OutputMSFFileName
          # else:
            # outMSFFile = open(self.OutputMSFFileName, 'w')
            # print "created files " + self.OutputFolderName+"\\"+self.OutputMSFFileName
            
          outMSFFile = ""
          if len(self.VmeToExtractID)!=1:
            outMSFFile = open(self.InputCacheDirectory+"\\" + str(id) + "\\"+self.OutputMSFFileName, 'w')
            print "created files " + self.InputCacheDirectory+"\\" + str(id) + "\\"+self.OutputMSFFileName
          else:
            outMSFFile = open(self.InputMSFDirectory + "\\" + self.OutputMSFFileName, 'w')
            print "created files " + self.InputMSFDirectory + "\\" + self.OutputMSFFileName
          msfOutputDoc.write(outMSFFile)
              
          #remove XML file
          xmlFilePath = self.OutputFolderName + "\\" + self.InputVMEXMLFileName
          os.remove(xmlFilePath)
          
          if Debug:
            print "\nWritten output MSF file " + self.OutputMSFFileName + " in directory " + self.InputCacheDirectory

            
def run(inputCacheDirectory, inputMSFDirectory, vmeToExtractIds, unhandledPlusManualTagsListFileName,handledAutoTagsListFileName):                                        
     curDir = sys.path[0]
     lhpEditVMETagInstance = lhpEditVMETag()
     lhpEditVMETagInstance.InputCacheDirectory = inputCacheDirectory
     lhpEditVMETagInstance.InputMSFDirectory = inputMSFDirectory

     lhpEditVMETagInstance.UnhandledPlusManualTagsListFileName = unhandledPlusManualTagsListFileName
     lhpEditVMETagInstance.VmeToExtractID = vmeToExtractIds.split()
     
     if len(lhpEditVMETagInstance.VmeToExtractID) != 1:
        lhpEditVMETagInstance.HandledAutoTagsListFileName = handledAutoTagsListFileName
     else:
        lhpEditVMETagInstance.HandledAutoTagsListFileName = curDir + '\\' + handledAutoTagsListFileName
     
     lhpEditVMETagInstance.OutputVMEXMLName = "OutputXML.xml"
     lhpEditVMETagInstance.OutputFolderName = inputMSFDirectory #put out xml in the msf directoty
     lhpEditVMETagInstance.FakeRootMSFFileName = curDir + r'\applicationData\fakeRoot.xml'
     lhpEditVMETagInstance.FakeMSFFileName = curDir + r'\applicationData\fakeMSF.xml'

     lhpEditVMETagInstance.BuildOutputMSF() 
    
if __name__ == '__main__':    
    import sys
    usage_msg = '''Usage: %s
    InputCacheDirectoryABSFolder 
    InputMSFDirectoryABSFolder 
    InputVMEId
    InputUnhandledPlusManualTagsListFileName 
    InputHandledAutoTagsListFileName
    ''' % sys.argv[0]
    
    if len(sys.argv) != 6:
        print 'Error :\n' + usage_msg
        sys.exit(1)
        
    inputCacheDirectory = sys.argv[1].replace("??", " ")
    inputMSFDirectory = sys.argv[2].replace("??", " ")
    vmeToExtractIds = sys.argv[3]
    unhandledPlusManualTagsListFileName = sys.argv[4]
    handledAutoTagsListFileName = sys.argv[5]
    
    print "\ninput cache Directory: " + inputMSFDirectory
    print "\ninput MSF Directory: " + inputMSFDirectory
    print "\ninput vme Id: " + vmeToExtractIds
    print "\nunhandledPlusManualTagsListFileName: " + unhandledPlusManualTagsListFileName
    print "\nhandledAutoTagsListFileName: " + handledAutoTagsListFileName
    
    run(inputCacheDirectory, inputMSFDirectory, vmeToExtractIds, unhandledPlusManualTagsListFileName,
        handledAutoTagsListFileName)

 
