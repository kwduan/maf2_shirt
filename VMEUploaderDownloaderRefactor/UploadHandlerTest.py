import UploadHandler
import unittest
import os , Queue
import XMLResourceCreator
from shutil import copytree
from fileUtilities import rmdir_recursive
from Debug import Debug

class UploadHandlerTest(unittest.TestCase):
              
    def setUp(self):  
        print "Beware:  In order to work this test must be launched from VMEUploaderDownloader dir!"  
        self.curDir = os.getcwd()        
        print " current directory is: " + self.curDir
       
        self.user = "testuser"
        self.password  = "6w8DHF"
        self.repositoryURL = "https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2"
        self.proxyHost = ''
        self.proxyPort = 0
    
    def testCreateUploadHandler(self):
        
        if (self.proxyHost == "" and self.proxyPort == 0):
            print ""
            print "Not using PROXY"
        
        else:
            
            print "using PROXY:"
            print "-> proxyHost: " + str(self.proxyHost) + "<-"
            print "-> proxyPort: " + str(self.proxyPort) + "<-"
            print ""
        

        queue = Queue.Queue()
        observer = None
        
        inputDir = self.curDir + r'\InputMSFForUploadTestData'
        msfCacheDirectory = self.curDir + r'\cacheInputMSFForUploadTestData'
        if os.path.exists(msfCacheDirectory): rmdir_recursive(msfCacheDirectory)
        
        copytree(inputDir, msfCacheDirectory)
        
        dirOutgoing = self.curDir + r'\Outgoing' #dir  where write xml and binary
        
        vmeID = 1
        if(os.path.exists(msfCacheDirectory) == False): os.mkdir(msfCacheDirectory)
        if(os.path.exists(dirOutgoing) == False ): os.mkdir(dirOutgoing)
    
        hasLink = False
        withChild = False
        msfListFile = "noMSF"
        print "creating XML resource on repository ..."
        xmlResource = self.CreateXMLResource()
        print "done!"
        print ""
        isLast = "true"
        vmeName = "test_volume"
        
        print "uploading ", str(vmeName) , " XML and binary data on repository ..."
        uploadHandler = UploadHandler.UploadHandler(\
        queue,observer, msfCacheDirectory, vmeID , self.user, self.password,self.repositoryURL, \
        vmeID , hasLink, withChild, msfListFile, xmlResource , isLast, vmeName)
        uploadHandler.proxyHost = self.proxyHost
        uploadHandler.proxyPort = self.proxyPort
        uploadHandler.upload()
        
        print "local binary checksum: " , uploadHandler.localChksum
        print "remote binary checksum: " ,uploadHandler.remoteChksum
        
        self.assertEqual(uploadHandler.localChksum, uploadHandler.remoteChksum)
        
        print 
        print "done!"
        print ""
        print "upload test successful :-)"
        print ""
    
    def CreateXMLResource(self):
        
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"
        
        curDir = os.getcwd()        
        
        print " current directory is: " + curDir
        resourceCreator = XMLResourceCreator.XMLResourceCreator()
        resourceCreator.userName = self.user
        resourceCreator.password = self.password
        resourceCreator.URL = self.repositoryURL 
        resourceCreator.proxyHost = self.proxyHost
        resourceCreator.proxyPort = self.proxyPort
        resourceCreated = resourceCreator.CreateEmptyXMLResource()
        print "created XML resource: " + resourceCreated[0]
        return resourceCreated
    
if __name__ == '__main__':
    unittest.main()
    