'''
Created on 28/lug/2010

@author: Eleonora Mambrini
'''

# -*- coding: iso-8859-1 -*-
import wx

from customtreectrl import (TR_AUTO_CHECK_CHILD)

import listBasket
import FlexAutoCheckHyperTreeList as HTL
import os
import urllib2
import decimal
import ConfigLoader

# New object that implements a multicolumn tree list with check boxes and sort feature

class CheckListCtrl(HTL.FlexAutoCheckHyperTreeList):
    def __init__(self, parent, itemDataMap, user, serviceURL):
#        MSFTreeList.MSFHyperTreeList.__init__(self, parent, -1, style=wx.TR_HIDE_ROOT|wx.TR_HAS_VARIABLE_ROW_HEIGHT, agwStyle=(wx.TR_HIDE_ROOT | wx.TR_ROW_LINES | wx.TR_HAS_BUTTONS | wx.TR_DEFAULT_STYLE | wx.TR_HAS_VARIABLE_ROW_HEIGHT | TR_AUTO_CHECK_CHILD))
        HTL.FlexAutoCheckHyperTreeList.__init__(self, parent, -1, style=wx.TR_HIDE_ROOT, agwStyle=(wx.TR_HIDE_ROOT | wx.TR_ROW_LINES | wx.TR_HAS_BUTTONS | wx.TR_DEFAULT_STYLE | wx.TR_HAS_VARIABLE_ROW_HEIGHT | TR_AUTO_CHECK_CHILD))
        
        self.User = user
        self.ServiceURL = serviceURL
        
        # Get the service url from the configuration file
        cl = ConfigLoader.ConfigLoader("",self.User)
        #Create columns
        self.AddColumn("Name",250,wx.LEFT)
        self.AddColumn("Type",100,wx.LEFT)
        self.AddColumn("Size",100,wx.LEFT)
        self.AddColumn("Date",100,wx.LEFT)
        self.AddColumn("Quality",100,wx.LEFT)
        
        
        self.SetMainColumn(0) # the one with the tree in it...

        self.root = self.AddRoot("The Repository Root", 0, 0)
        
        items = itemDataMap.items()
        imageList = wx.ImageList(15,15)
        wx.InitAllImageHandlers()
        images = list()
        iindex = -1 # image index
        cindex = 0 # temporary image index
        index = 0 # row index
        #Create rows
        
        child = None
        itemParent = None
        
        for key, data in items:
            
            corrupted = False
            
            for i in range(0,8):
                if data[i] == '?':  #corrupted data
                    corrupted = True
            if corrupted == True:
                continue
            
#          managing images
            iindex = -1 # image index in images list
            cindex = 0  # current index
            for name in images: # search if is the image is already present              
                if name == data[1]:
                    iindex = cindex
                    break
                cindex = cindex + 1
                
            if iindex == -1: # if not present
                images.append("%s" % data[1])
                try:
                    response = urllib2.urlopen(self.ServiceURL + "/%s" % data[1]) # get it from the web
                except:
                    response = urllib2.urlopen(self.ServiceURL + "/mafVMEGeneric.png") # get it from the web
                tempImage = open("%s" % data[1],"wb") # temporary create a copy on disk
                tempImage.write(response.read())
                tempImage.close()
                iindex = imageList.Add(wx.Bitmap("%s" % data[1])) # load it in the image list
                print data[1]
                os.remove("%s" % data[1]) # delete it from disk
#            else:
#                iindex = iindex + 2

            a = decimal.Decimal(data[4],2) # conversion for the size field
            if a > 1000000000: # Gigabytes
                a = a / 1000000000
                strSize = "%.2f GB" % a
            elif a > 1000000: # Megabytes
                a = a / 1000000
                strSize = "%.2f MB" % a
            elif a > 1000: # Kilobytes
                a = a / 1000
                strSize = "%.2f KB" % a
            else: # Bytes
                strSize = "%d Bytes" % a

#          data[0] -> ""
#          data[1] -> img
#          data[2] -> name
#          data[3] -> type
#          data[4] -> size
#          data[5] -> date
#          data[6] -> quality
#          data[7] -> resource ID
#          data[8] -> parent resource ID   

                
#            searching for parent item
            itemParent = self.GetItem(data[8])
            if itemParent is None:
                itemParent = self.root                 

            child = self.AppendItem(itemParent, data[2],1)
            
            self.SetPyData(child, None)
            self.SetItemText(child, data[3], 1)
            self.SetItemText(child, strSize, 2)
            self.SetItemText(child, data[5], 3)
            self.SetItemText(child, data[6], 4)
            self.SetItemImage(child, iindex, which=wx.TreeItemIcon_Normal)
            self.SetItemImage(child, iindex, which=wx.TreeItemIcon_Expanded)
#            self.SetItemBackgroundColour(child,wx.Colour(220,220,220))
          
            itemData = list()
            itemData.append(key)
            isARoot = 0
            if str(data[3]).find('Root') >= 0:
                isARoot = 1
            itemData.append(isARoot)
            itemData.append(data[7])
            child.SetData(itemData)
            
#            self.parent = child
            

            index = index + 1
        self.AssignImageList(imageList)
        #Create sorter
        self.itemDataMap = itemDataMap
        #ColumnSorterMixin.__init__(self, 7)

    def GetListCtrl(self):
        return self
    
    def GetItemCount(self):
        numberOfItems = self.GetMainWindow().GetCount()
        return numberOfItems
    
    def GetCheckedItems(self, itemParent=None, checkedItems=[]): 
        if itemParent is None: 
            itemParent = self.GetRootItem() 
        child, cookie = self.GetFirstChild(itemParent) 
        while child: 
            if self.GetMainWindow().IsItemChecked(child): 
                checkedItems.append(child) 
            checkedItems = self.GetCheckedItems(child, checkedItems) 
            child, cookie = self.GetNextChild(itemParent, cookie) 
        return checkedItems
    
    def GetItem(self, resourceID, itemParent=None, item=None): 
        if itemParent is None: 
            itemParent = self.GetRootItem() 
        child, cookie = self.GetFirstChild(itemParent) 
        while child: 
            itemResourceID = child.GetData()[2]
            if itemResourceID == resourceID:
                item = child 
                break
            item = self.GetItem(resourceID, child, item) 
            child, cookie = self.GetNextChild(itemParent, cookie) 
        return item
        

class downloadSelectorFrame(wx.Frame):
    def __init__(self,parent,id = -1,title='',pos = wx.Point(1,1),size = wx.Size(552,260),style = wx.DEFAULT_FRAME_STYLE,name = 'frame', app = None):
        self.App = app
        if title.find('Sandbox') != -1:
            self.fromSandbox = 3
        else:
            self.fromSandbox = 2          
        self.lBasket = listBasket.listBasket(self.fromSandbox)
        self.lBasket.SetCredentials(self.App.user, self.App.password)
        self.lBasket.SetServiceURL(self.App.repository)
        pre=wx.PreFrame()
        self.OnPreCreate()
        pre.Create(parent,id,title,pos,size,style,name)
        self.PostCreate(pre)
        self.initBefore()
        self.VwXinit()
        self.initAfter()
        self.Selections = []

    def __del__(self):
        self.Ddel()
        return

    def VwXinit(self):
        self.Show(False)
        self.panel = wx.Panel(self,-1,wx.Point(-5,-5),wx.Size(552,260))
        self.selectAllButton = wx.Button(self.panel,-1,"",wx.Point(141,151),wx.Size(130,30))
        self.selectAllButton.SetLabel("Select all")
        self.downloadButton = wx.Button(self.panel,-1,"",wx.Point(141,151),wx.Size(130,30))
        self.downloadButton.SetLabel("Download")
        self.Bind(wx.EVT_BUTTON,self.selectAllButton_VwXEvOnButtonClick,self.selectAllButton)
        self.Bind(wx.EVT_BUTTON,self.downloadButton_VwXEvOnButtonClick,self.downloadButton)
        self.PopulateDict()
        self.checkListCtrl = CheckListCtrl(self.panel,self.itemDataMap,self.App.user,self.App.repository)
        self.verticalSizer = wx.BoxSizer(wx.VERTICAL)
        self.horizontalListCtrl = wx.BoxSizer(wx.HORIZONTAL)
        self.horizontalButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.horizontalListCtrl.Add(self.checkListCtrl,1,wx.CENTER|wx.EXPAND|wx.FIXED_MINSIZE,3)
        self.horizontalButtonSizer.Add(self.selectAllButton,1,wx.CENTER|wx.FIXED_MINSIZE,3)
        self.horizontalButtonSizer.Add(self.downloadButton,1,wx.CENTER|wx.FIXED_MINSIZE,3)
        self.verticalSizer.Add(self.horizontalListCtrl,3,wx.CENTER|wx.EXPAND|wx.FIXED_MINSIZE,3)
        self.verticalSizer.Add(self.horizontalButtonSizer,1,wx.CENTER|wx.FIXED_MINSIZE,3)
        self.panel.SetSizer(self.verticalSizer);self.panel.SetAutoLayout(1);self.panel.Layout();
        self.Refresh()
        return

    def PopulateDict(self):
        self.lBasket.getListFromBasket()
        ld = list()
        ln = list()
        for r in range(0,len(self.lBasket.IdList)/8):
            l = list()
            l.append("") # empty for the first column
            for c in range(0, 8):
                l.append(self.lBasket.IdList[r * 8 + c]) # skip dataresource-...
            ld.append(tuple(l))
            ln.append(r+1)
        lc = zip(ln,ld)
        self.itemDataMap = dict(lc)
        return

    def VwXDelComp(self):
        return
    
    def selectAllButton_VwXEvOnButtonClick(self,event): #init function      
#        for count in range(0,self.checkListCtrl.GetItemCount()):
#                self.checkListCtrl.CheckItem(count)
#                #self.checkList.Check(count)
        itemParent = self.checkListCtrl.GetRootItem() 
        child, cookie = self.checkListCtrl.GetFirstChild(itemParent) 
        while child: 
            self.checkListCtrl.CheckItem(child, True, True)
            child, cookie = self.checkListCtrl.GetNextChild(itemParent, cookie) 
        

#[win]add your code here
    def downloadButton_VwXEvOnButtonClick(self,event): #init function
        #[271]Code event VwX...Don't modify[271]#
        #add your code here
        totalNumberOfSelected = 0
        self.Selections = []
        self.Checked = []
        self.SelectionsParentId = []
        self.SelectionsType = []
        self.SelectionsHasChild = []
        
        downloadGroup = 0
        
        self.checkListCtrl.GetCheckedItems(None, self.Checked)
        if(len(self.Checked) == 0):
            wx.MessageBox("Must be selected some vme")
            return
        
        for count in range(0, len(self.Checked)):
            id = self.Checked[count].GetData()[0]
            sel = self.Checked[count].GetData()[2] # datasource corrisponding to VME name checked
            
            parent = ""
            type = ""
            hasChild = "false"
            items = self.itemDataMap.items()
            for key, data in items:
                if data[7] == sel:
                    parent = data[8]
                    type = data[3]
                if data[8] == sel:
                    hasChild = "true"
            
            if self.Checked[count].GetData()[1] == 1:
                # if downloading a root, won't append child VME
                downloadGroup = 1
                self.Selections.append(sel)
                self.SelectionsParentId.append(parent)
                self.SelectionsType.append(type)
                self.SelectionsHasChild.append(hasChild)
            else:     
                if downloadGroup == 0:
                    self.Selections.append(sel) #get datasource-... corrisponding to VME name checked
                    self.SelectionsParentId.append(parent)
                    self.SelectionsType.append(type)
                    self.SelectionsHasChild.append(hasChild)
            
        
        #if(len(self.Selections) != 1): 
        #    wx.MessageBox("You can Select for Now 1 only vme (temporarly)")
        #    return
        self.lBasket.IdListSelected = self.Selections #copy selection list for listBasket selected list
        self.lBasket.IdListParent = self.SelectionsParentId #copy selection list for listBasket selected list
        self.lBasket.IdListType = self.SelectionsType
        self.lBasket.IdListHasChild = self.SelectionsHasChild
        self.lBasket.writeIdListSelectedOnFile()
        self.App.ExitMainLoop()
        return #end function

    def OnPreCreate(self):
        #add your code here

        return

    def initBefore(self):
        #add your code here

        return

    def initAfter(self):
        #add your code here
        self.Centre() 
        self.Show()
        return

    def Ddel(self): #init function
        #[ f9]Code VwX...Don't modify[ f9]#
        #add your code here

        return #end function

#[win]end your code
