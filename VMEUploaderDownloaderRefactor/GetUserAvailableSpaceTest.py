import os
from GetUserAvailableSpace import AvailableSpace 
import unittest

class AvailableSpaceTest(unittest.TestCase):
    
    def setUp(self):  
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"  
        
        self.curDir = os.getcwd()
        self.user = 'testuser' #substitute
        self.pwd  = '6w8DHF' #substitute
        
        print " current directory is: " + self.curDir
       
    def testGetUserAvailableSpace(self):
        userSpace = AvailableSpace()
        userSpace.SetCredentials(self.user, self.pwd)
        space = int( userSpace.getUserAvailableSpace() )
        
        print space
        
        # ==========================================================
        # BEWARE!!! testuser quota has to be set exactyl to 1000 Mb
        # otherwise the test will FAIL
        #         
        assert ( space <= 1044293807 )
        #
        # ========================================================== :D
    
    
if __name__ == '__main__':
    unittest.main()
