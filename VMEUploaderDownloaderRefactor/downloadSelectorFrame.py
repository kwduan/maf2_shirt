# -*- coding: iso-8859-1 -*-
# Don't modify comment 

import wx
from wx.lib.mixins.listctrl import CheckListCtrlMixin,ColumnSorterMixin
#[inc]add your include files here
import listBasket
import os
import urllib2
import decimal
import ConfigLoader
#[inc]end your include

# New object that implements a multicolumn list with check boxes and sort feature
class CheckListCtrl(wx.ListCtrl, CheckListCtrlMixin, ColumnSorterMixin):
    def __init__(self, parent, itemDataMap):
        wx.ListCtrl.__init__(self, parent, -1, style=(wx.LC_REPORT))
        CheckListCtrlMixin.__init__(self)
        # Get the service url from the configuration file
        cl = ConfigLoader.ConfigLoader("")
        #Create columns
        self.InsertColumn(0,"",wx.CENTER,25) # column for check box
        self.InsertColumn(1,"",wx.CENTER,15) # column for image
        self.InsertColumn(2,"Name",wx.LEFT,100)
        self.InsertColumn(3,"Type",wx.LEFT,100)
        self.InsertColumn(4,"Size",wx.LEFT,100)
        self.InsertColumn(5,"Date",wx.LEFT,100)
        self.InsertColumn(6,"Quality",wx.LEFT,100)
        items = itemDataMap.items()
        imageList = wx.ImageList(15,15)
        wx.InitAllImageHandlers()
        imageList.Add(wx.Bitmap("uncheck.bmp")) # for checkbox
        imageList.Add(wx.Bitmap("check.bmp")) # for checkbox
        images = list()
        iindex = -1 # image index
        cindex = 0 # temporary image index
        index = 0 # row index
        #Create rows
        for key, data in items:
          # managing images
          iindex = -1
          cindex = 0
          for name in images: # search if is the image is already present
            if name == data[1]:
              iindex = cindex
              break
            cindex = cindex + 1
          if iindex == -1: # if not present
            images.append("%s" % data[1])
            try:
                response = urllib2.urlopen(cl.ServiceURL + "/lhprepository2/%s" % data[1]) # get it from the web
            except:
                response = urllib2.urlopen(cl.ServiceURL + "/lhprepository2/mafVMEGeneric.png") # get it from the web
            tempImage = open("%s" % data[1],"wb") # temporary create a copy on disk
            tempImage.write(response.read())
            tempImage.close()
            iindex = imageList.Add(wx.Bitmap("%s" % data[1])) # load it in the image list
            os.remove("%s" % data[1]) # delete it from disk
          else:
            iindex = iindex + 2
          self.InsertStringItem(index,data[0])
          self.SetStringItem(index,1,"",iindex)
          self.SetStringItem(index,2,data[2],-1)
          self.SetStringItem(index,3,data[3],-1)
          a = decimal.Decimal(data[4],2) # conversion for the size field
          if a > 1000000000: # Gigabytes
            a = a / 1000000000
            strSize = "%.2f GB" % a
          elif a > 1000000: # Megabytes
            a = a / 1000000
            strSize = "%.2f MB" % a
          elif a > 1000: # Kilobytes
            a = a / 1000
            strSize = "%.2f KB" % a
          else: # Bytes
            strSize = "%d Bytes" % a
          self.SetStringItem(index,4,strSize,-1)
          self.SetStringItem(index,5,data[5],-1)
          self.SetStringItem(index,6,data[6],-1)
          self.SetItemData(index, key)
          index = index + 1
        self.AssignImageList(imageList,wx.IMAGE_LIST_SMALL)
        #Create sorter
        self.itemDataMap = itemDataMap
        ColumnSorterMixin.__init__(self, 7)

    def GetListCtrl(self):
        return self

class downloadSelectorFrame(wx.Frame):
    def __init__(self,parent,id = -1,title='',pos = wx.Point(1,1),size = wx.Size(552,260),style = wx.DEFAULT_FRAME_STYLE,name = 'frame', app = None):
        self.App = app
        if title.find('Sandbox') != -1:
            self.fromSandbox = 1
        else:
            self.fromSandbox = 0          
        
        self.lBasket = listBasket.listBasket(self.fromSandbox)
        self.lBasket.SetCredentials(self.App.user, self.App.password)
        pre=wx.PreFrame()
        self.OnPreCreate()
        pre.Create(parent,id,title,pos,size,style,name)
        self.PostCreate(pre)
        self.initBefore()
        self.VwXinit()
        self.initAfter()
        self.Selections = []

    def __del__(self):
        self.Ddel()
        return

    def VwXinit(self):
        self.Show(False)
        self.panel = wx.Panel(self,-1,wx.Point(-5,-5),wx.Size(552,260))
        self.selectAllButton = wx.Button(self.panel,-1,"",wx.Point(141,151),wx.Size(130,30))
        self.selectAllButton.SetLabel("Select all")
        self.downloadButton = wx.Button(self.panel,-1,"",wx.Point(141,151),wx.Size(130,30))
        self.downloadButton.SetLabel("Download")
        self.Bind(wx.EVT_BUTTON,self.selectAllButton_VwXEvOnButtonClick,self.selectAllButton)
        self.Bind(wx.EVT_BUTTON,self.downloadButton_VwXEvOnButtonClick,self.downloadButton)
        self.PopulateDict()
        self.listCtrl = CheckListCtrl(self.panel,self.itemDataMap)
        self.verticalSizer = wx.BoxSizer(wx.VERTICAL)
        self.horizontalListCtrl = wx.BoxSizer(wx.HORIZONTAL)
        self.horizontalButtonSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.horizontalListCtrl.Add(self.listCtrl,1,wx.CENTER|wx.EXPAND|wx.FIXED_MINSIZE,3)
        self.horizontalButtonSizer.Add(self.selectAllButton,1,wx.CENTER|wx.FIXED_MINSIZE,3)
        self.horizontalButtonSizer.Add(self.downloadButton,1,wx.CENTER|wx.FIXED_MINSIZE,3)
        self.verticalSizer.Add(self.horizontalListCtrl,3,wx.CENTER|wx.EXPAND|wx.FIXED_MINSIZE,3)
        self.verticalSizer.Add(self.horizontalButtonSizer,1,wx.CENTER|wx.FIXED_MINSIZE,3)
        self.panel.SetSizer(self.verticalSizer);self.panel.SetAutoLayout(1);self.panel.Layout();
        self.Refresh()
        return

    def PopulateDict(self):
        self.lBasket.getListFromBasket()
        ld = list()
        ln = list()
        for r in range(0,len(self.lBasket.IdList)/7):
          l = list()
          l.append("") # empty for the first column
          for c in range(0, 6):
            l.append(self.lBasket.IdList[r * 7 + c]) # skip dateresurce-...
          ld.append(tuple(l))
          ln.append(r+1)
        lc = zip(ln,ld)
        self.itemDataMap = dict(lc)
        return

    def VwXDelComp(self):
        return
    
    def selectAllButton_VwXEvOnButtonClick(self,event): #init function
        for count in range(0,self.listCtrl.GetItemCount()):
                self.listCtrl.CheckItem(count)
                #self.checkList.Check(count)
        

#[win]add your code here
    def downloadButton_VwXEvOnButtonClick(self,event): #init function
        #[271]Code event VwX...Don't modify[271]#
        #add your code here
        totalNumberOfSelected = 0
        self.Selections = []
        for count in range(0,self.listCtrl.GetItemCount()):            
            if(self.listCtrl.IsChecked(count)):
                self.Selections.append(self.lBasket.IdList[(((self.listCtrl.GetItemData(count)-1)*7)+6)]) #get datasource-... corrisponding to VME name checked
                totalNumberOfSelected = totalNumberOfSelected + 1
        if(len(self.Selections) == 0):
            wx.MessageBox("Must be selected some vme")
            return
        
        #if(len(self.Selections) != 1): 
        #    wx.MessageBox("You can Select for Now 1 only vme (temporarly)")
        #    return
        self.lBasket.IdListSelected = self.Selections #copy selection list for listBasket selected list
        self.lBasket.writeIdListSelectedOnFile()
        self.App.ExitMainLoop()
        return #end function

    def OnPreCreate(self):
        #add your code here

        return

    def initBefore(self):
        #add your code here

        return

    def initAfter(self):
        #add your code here
        self.Centre() 
        self.Show()
        return

    def Ddel(self): #init function
        #[ f9]Code VwX...Don't modify[ f9]#
        #add your code here

        return #end function

#[win]end your code
