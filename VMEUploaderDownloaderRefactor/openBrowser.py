import sys
import webbrowser
import ConfigLoader
def main():
    
    usage_msg = '''Usage: %s user, password''' % sys.argv[0]
    if(len(sys.argv) != 2):
        print 'Error :\n' + usage_msg
        sys.exit(1)
       
    if(len(sys.argv) == 2):
        user = sys.argv[1]
        
    # Get the service url from the configuration file
    cl = ConfigLoader.ConfigLoader("", user)
    webbrowser.open(cl.ServiceURL)

if __name__ == '__main__':
    main()
