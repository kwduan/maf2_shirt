#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Stefano Perticoni <s.perticoni@scsolutions.it>
#-----------------------------------------------------------------------------


import HttpsFileDownload
import unittest
import os

class HttpsFileDownloadTest(unittest.TestCase):
      
    def setUp(self):
        
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"
        
        curDir = os.getcwd()        
        
        print " current directory is: " + curDir
          
    
    def testDownloadFile(self):
        
        # URL
        host = "www.biomedtown.org"
        fileSelector = "/biomed_town/LHDL/users/swclient/tagsToBeRemovedFromUploadedVMETagArray/tagsToBeRemovedFromUploadedVMETagArray"
        
        # output file name
        outputFileName = "downloadedFile.txt"
        
        HttpsFileDownload.run(host, fileSelector, outputFileName)
        
        downloadedFileABSFileName = os.getcwd() + "\\" + outputFileName
        downloadedFileExists = os.path.exists(downloadedFileABSFileName)
        self.assertTrue(downloadedFileExists)
        os.remove(downloadedFileABSFileName)
            
        
if __name__ == '__main__':
     unittest.main()
    
