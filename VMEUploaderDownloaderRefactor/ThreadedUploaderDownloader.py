"""
This recipe describes how to handle asynchronous I/O in an environment where
you are running Tkinter as the graphical user interface. Tkinter is safe
to use as long as all the graphics commands are handled in a single thread.
Since it is more efficient to make I/O channels to block and wait for something
to happen rather than poll at regular intervals, we want I/O to be handled
in separate threads. These can communicate in a threasafe way with the main,
GUI-oriented process through one or several queues. In this solution the GUI
still has to make a poll at a reasonable interval, to check if there is
something in the queue that needs processing. Other solutions are possible,
but they add a lot of complexity to the application.

Created by Jacob Hall?n, AB Strakt, Sweden. 2001-10-17
"""

import wx
import GuiFactory
import time
import CustomThread
import random
import Queue
import UploadHandler, DownloadHandler
from webServicesClient import xmlrpcDemoWS
from Debug import Debug
import xml.dom.minidom as xd
from lhpDefines import *
import thread
import msvcrt
import Server
import Lock
import sys, os
from time import sleep
import ThreadPool
import UploadIntegrityChecker
import DownloadIntegrityChecker
import decimal

class ThreadedUploaderDownloader:
    """
    Launch the main part of the GUI and the worker thread. 
    periodicCall and endApplication could reside in the GUI part, but putting them here
    means that you have all the thread controls in a single place.
    """
    def __init__(self, master, stringAppType, periodicProcedure):
        """
        Start the GUI and the asynchronous threads. We are in the main
        (original) thread of the application, which will later be used by
        the GUI. We spawn a new thread for the worker.
        """
        
        self.threadPool = ThreadPool.ThreadPool(1)
        
        self.master = master
        self.serverListeningPort = int(sys.argv[1])
        self.threads = []
        self.periodicProcedure = periodicProcedure
        # create server thread waiting for client (Client.py class) socket connections
        thread.start_new_thread(Server.createServer,(self,self.serverListeningPort,1))
        if Debug:
            print "create server"
            
        #lock file: activeLock.lhp holds the process id of the ThreadedUploaderDownloader
        #and it's used by the C++ client code 
        self.lock = Lock.Lock(os.getcwd() + "\\activeLock.lhp")
        if Debug:
            print "create lock file activeLock.lhp"
        
        # Create the queue
        self.queue = Queue.Queue()
        
        # create the GUI
        guiFactory = GuiFactory.GuiFactory()
        self.FileTransferFrame = (guiFactory.createGui(stringAppType))(master, self.queue, self.endApplication)
        if Debug:
            print "create FileTransferFrame"
        
        # status.txt holds the list of started file transfer threads
        # it's used to know if all vme transfer are finished 
        # <status.txt start>
        #started
        #ended
        #started
        #ended
        #lastStarted
        #lastEnded
        # <status.txt end>
        if(os.path.exists(sys.path[0] + '\\status.txt')):
           os.remove(sys.path[0] + '\\status.txt')
           if Debug:
               print "status.txt removed" 

        # Start the periodic call in the GUI to check if the queue contains
        # anything
        self.periodicCall()
        #self.isWaiting = False # (bug #1869 fix)
        
        # create only one progress bar
        self.FileTransferFrame.createSingleBar("UPLOAD")
        self.FileTransferFrame.setSingleBarTitle("Transfer information...")
        self.numberOfThreads = 0 # add this member to avoid memory saturation while downloading a large amount of vme (not used in upload)
        self.workingDir = os.getcwd();
        
    def periodicCall(self):
        """
        Check every 1000 ms if there is something new in the queue.
        """
        
        if(self.periodicProcedure):        
            self.FileTransferFrame.processIncoming()
            self.periodicProcedure(1000, self.periodicCall)
        
    def createFileTransferThread(self, tuplaFromServer):
        #tuplaFromServer is
        #0 is modality (UPLOAD or DOWNLOAD)
        #1 is id for UPLOAD, SRB file size for DOWNLOAD 
        #2 is directory
        #3 is usr
        #4 is pwd
        #5 is serverUrl
        #6 is originalId in UPLOAD, SRB data URI in DOWNLOAD
        #7 is hasLink (UPLOAD)
        #8 is withChild (UPLOAD)
        #9 file for upload rollback (UPLOAD)
        #10 is XML resource URI (UPLOAD)
        #11 is last VME?(UPLOAD)
        #12 is vme name (UPLOAD)
        fileTransferModality = tuplaFromServer[0]
        
        if(fileTransferModality == "UPLOAD"):
            #pass
            self.createThreadForUpload(tuplaFromServer)
              
        elif (fileTransferModality == "DOWNLOAD"):
           self.createThreadForDownload(tuplaFromServer)
        
    def createThreadForUpload(self, tuplaFromServer):
        #tuplaFromServer is
        #0 is modality UPLOAD
        #1 is id, 
        #2 is directory
        #3 is usr
        #4 is pwd
        #5 is serverUrl
        #6 is originalId
        #7 is hasLink
        #8 is withChild
        #9 file for upload rollback (UPLOAD)        
        #10 is XML resource URI
        #11 is last VME?(UPLOAD)
        #12 is vme name
        transferModality = tuplaFromServer[0]
        vmeName = tuplaFromServer[12]
        vmeID = tuplaFromServer[6]

        wx.LogMessage("Created upload thread for " + str(vmeName))
        
        
        #self.FileTransferFrame.setSingleBarTitle("Upload: Current VME is %s" % str(vmeName))
        self.FileTransferFrame.setSingleBarTitle("Uploading...")
        progressBarObserver = self.FileTransferFrame.bars[0]
        #self.isWaiting = True
        
        args = (progressBarObserver,\
        tuplaFromServer[2],\
        tuplaFromServer[1],tuplaFromServer[3],tuplaFromServer[4], tuplaFromServer[5], tuplaFromServer[6],\
        tuplaFromServer[7], tuplaFromServer[8], tuplaFromServer[9], tuplaFromServer[10],\
        tuplaFromServer[11], vmeName)
        
        self.threadPool.queueTask(self.workerThreadUpload, args = (progressBarObserver,\
        tuplaFromServer[2],\
        tuplaFromServer[1],tuplaFromServer[3],tuplaFromServer[4], tuplaFromServer[5], tuplaFromServer[6],\
        tuplaFromServer[7], tuplaFromServer[8], tuplaFromServer[9],\
        tuplaFromServer[10], tuplaFromServer[11], vmeName))
        
            
    def createThreadForDownload(self, tuplaFromServer):
        #tuplaFromServer is
        #0 is modality DOWNLOAD)
        #1 is SRB file size
        #2 is directory
        #3 is usr
        #4 is pwd
        #5 is serverUrl
        #6 is XML data URI 
        #7 is SRB data URI 
        #8 is last VME? 
        
        
        # Here write on a file the number of threads
        # avoid memory saturation while downloading a large amount of vme (lhpBuilder control)
        
        # workaround to get the absolute path:
        threadFileAbsPath = self.workingDir + "\\" + "lhpDownloadThreads.txt"
        
        strNumberOfThreads = "0"
        # Here must increment the download thread
        try:
            commFile = open(threadFileAbsPath,"r") # Refactor this to use a single file (lhpCommunication.txt)
            # read the number of download thread
            for line in commFile:
                strNumberOfThreads = line
            self.numberOfThreads = decimal.Decimal(strNumberOfThreads,0)
            self.numberOfThreads = self.numberOfThreads + 1
            commFile.close()
        except:
            self.numberOfThreads = 1
            
        if self.numberOfThreads < 1:
            self.numberOfThreads = 1

        commFile = open(threadFileAbsPath,"w+") # Refactor this to use a single file (lhpCommunication.txt)
        # write the number of download thread
        commFile.write("%d" % self.numberOfThreads)
        commFile.close()
        
        self.userName = tuplaFromServer[3]
        self.password = tuplaFromServer[4]
        
        vmeName = self.GetVmeName(tuplaFromServer[6], tuplaFromServer[5])
        SRBDataURI = tuplaFromServer[7]
        
        #self.FileTransferFrame.setSingleBarTitle("Upload: Current VME is %s" % vmeName)
        self.FileTransferFrame.setSingleBarTitle("Downloading...")
        
        #self.FileTransferFrame.setTransferredVMEName(vmeName)  
        
        wx.LogMessage("Created download thread for " + str(vmeName) + " (%d running threads)" % self.numberOfThreads)
        #wx.LogMessage("SRBDataURI:  " + str(SRBDataURI))
        
        progressBarObserver = self.FileTransferFrame.bars[0]

        self.threadPool.queueTask(\
        self.workerThreadDownload, \
        args = (progressBarObserver, \
        tuplaFromServer[2],SRBDataURI, tuplaFromServer[3],tuplaFromServer[4],tuplaFromServer[5],\
        tuplaFromServer[8],tuplaFromServer[1]))
        
    def workerThreadUpload(self, (observer, dirCache , id, usr, pwd, urlServer,\
        originalId, hasLink, withChild, msfListFile, XMLURI, isLast, vmeName)):
        """
        This is where we handle the asynchronous I/O. For example, it may be
        a 'select()'.
        One important thing to remember is that the thread has to yield
        control.
        """
        try:
            statusFile = open(sys.path[0] + '\\status.txt', 'a')
            statusFile.close()
            
            counter = 0
            
            while 1:
                    size = os.path.getsize(sys.path[0] + '\\status.txt')
                    statusFile = open(sys.path[0] + '\\status.txt', 'a')
                    try:
                        msvcrt.locking(statusFile.fileno(), msvcrt.LK_RLCK, size)
                        if (isLast == "true"):
                            statusFile.write('lastStarted\n')
                        else:
                            statusFile.write('started\n')

                        statusFile.close()
                        break
                    except:
                        counter = counter+1 #to avoid deadlock
                        statusFile.close()
                        if(counter == 10):
                            if Debug:
                                print "----------Can not write in status.txt-----------"
                            pass

            UploadHandler.startUpload(self.queue, observer, dirCache, \
            id, usr, pwd, urlServer, originalId, hasLink, withChild, msfListFile, XMLURI, isLast, vmeName)
            if (isLast == "true"):
                
            
                # Check for upload integrity
                self.FileTransferFrame.setSingleBarTitle("Checking upload integrity...")
                uploadChecker = UploadIntegrityChecker.UploadIntegrityChecker(usr,pwd,urlServer)
                failureList = list()
                uploadChecker.CheckUploadIntegrity(failureList)
                
                if(len(failureList) > 0):
                    # Detected failed uploads
                    self.FileTransferFrame.setSingleBarTitle("Detected %d corrupted uploads!" % len(failureList))
                    wx.LogMessage("Detected %d corrupted uploads!" % len(failureList))
                    self.FileTransferFrame.numOfTransferredVME = self.FileTransferFrame.numOfTransferredVME - len(failureList)
                    #item.user == user This is supposed to be true :-)
                    for item in failureList: # restart failed uploads :)
                        if item == failureList[len(failureList)-1]:
                            item.isLastResourceToUpload = "true"
                        else:
                            item.isLastResourceToUpload = "false"
                        tupla = ["UPLOAD",item.cacheVMEIdForUploadOrSizeForDownload,item.currentCacheChildABSFolderWorkaround,
                                usr,pwd,item.repositoryURL,item.originalVMEIdForUploadOrXMLURIForDownload,
                                item.vmeHasLinkForUploadOrSRBDataURIForDownload,item.uploadWithChildren,item.XMLUploadedResourcesRollBackLocalFileName,
                                item.remoteXMLResourceURI,item.isLastResourceToUpload,item.name]
                        # Restart failed upload
                        self.createFileTransferThread(tupla)                    
                else:
                    wx.MessageBox("Upload completed!","Information",wx.ICON_INFORMATION)
                    self.FileTransferFrame.setSingleBarTitle("Upload completed.")
                    wx.LogMessage("Upload completed!")
                    self.FileTransferFrame.clearVMETransferStatus()

        except:
            pass
        
    def workerThreadDownload(self, (observer, dirCache , srbData, usr, pwd, urlServer, isLast, filesize)):
        """
        This is where we handle the asynchronous I/O. For example, it may be
        a 'select()'.
        One important thing to remember is that the thread has to yield
        control.
        """
        try:
            statusFile = open(sys.path[0] + '\\status.txt', 'a')
            statusFile.close()
            counter = 0
            
            while 1:
                    size = os.path.getsize(sys.path[0] + '\\status.txt')
                    statusFile = open(sys.path[0] + '\\status.txt', 'a')
                    try:
                        msvcrt.locking(statusFile.fileno(), msvcrt.LK_RLCK, size)
                        if (isLast == "true"):
                            statusFile.write('lastStarted\n')
                        else:
                            statusFile.write('started\n')

                        statusFile.close()
                        break
                    except:
                        counter = counter+1 #to avoid deadlock
                        statusFile.close()
                        if(counter == 10):
                            if Debug:
                                print "----------Can not write in status.txt-----------"
                            pass
            DownloadHandler.createDownloadHandler(self.queue ,observer, dirCache , srbData, usr, pwd, urlServer, isLast, filesize)
            self.FileTransferFrame.setSingleBarTitle("Download completed!")
            if (isLast == "true"):
                #wx.MessageBox("Download completed!","Information",wx.ICON_INFORMATION)
                #self.FileTransferFrame.setSingleBarTitle("Download completed.")
                #wx.LogMessage("Download completed! Please wait for binary copy...")
                pass
        except:
            wx.LogMessage("Unable to open status file")
            #This is required when a root is download (entire msf)
            #wx.MessageBox("Download completed!","Information",wx.ICON_INFORMATION)
            self.FileTransferFrame.setSingleBarTitle("Download completed!")
            #self.FileTransferFrame.setSingleBarTitle("Download completed.")
            #wx.LogMessage("Download completed! Please wait for binary copy...")
            pass
        """   
        # This is done after any download is completed
        # check for download integrity
        self.FileTransferFrame.setSingleBarTitle("Checking download integrity...")
        wx.LogMessage("Checking download integrity in dir " + dirCache)
        downloadChecker = DownloadIntegrityChecker.DownloadIntegrityChecker(usr,pwd,dirCache,dirCache + "\\outputMAF.msf") # check in the cache dir
        failureList = list()
        downloadChecker.CheckDownloadIntegrity(failureList)
        if(len(failureList) > 0):
            # Detected failed downloads
            self.FileTransferFrame.setSingleBarTitle("Detected %d corrupted downloads!" % len(failureList))
            wx.LogMessage("Detected %d corrupted downloads!" % len(failureList))
            self.FileTransferFrame.numOfTransferredVME = self.FileTransferFrame.numOfTransferredVME - len(failureList)
            #item.user == user This is supposed to be true :-)
            for item in failureList: # restart failed uploads :)
                if item == failureList[len(failureList)-1]:
                    item.isLastResourceToUpload = "true"
                else:
                    item.isLastResourceToUpload = "false"
                
                tupla = ["DOWNLOAD",item.cacheVMEIdForUploadOrSizeForDownload,item.currentCacheChildABSFolderWorkaround,
                        usr,pwd,item.repositoryURL,item.originalVMEIdForUploadOrXMLURIForDownload,
                        item.vmeHasLinkForUploadOrSRBDataURIForDownload,item.isLastResourceToUpload,item.name]
                # Restart failed upload
                self.createFileTransferThread(tupla)
        """
        self.numberOfThreads = self.numberOfThreads - 1
        commFile = open("lhpDownloadThreads.txt","w+") # Refactor this to use a single file (lhpCommunication.txt)
        commFile.write("%d" % self.numberOfThreads)
        commFile.close()
        
    def GetVmeName(self, XMLUri, serviceURL):
        self.proxyHost, self.proxyPort = retriveProxyParameters()
        if Debug:
            print "->"+ self.proxyHost + "<-"
            print "->"+ str(self.proxyPort) + "<-"
            
        ws = xmlrpcDemoWS.xmlrpc_demoWS()
        #ws.setServer('https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2/' + XMLUri)
        ws.setServer(serviceURL + XMLUri)
        ws.setCredentials(self.userName, self.password)
        ws.ProxyURL = self.proxyHost
        ws.ProxyPort = self.proxyPort
    
        if Debug:
            print "XML URI = " + XMLUri 
        out = ws.run('gettitle', XMLUri)[1]
        
        self.vmeName = []
        dom = xd.parseString(out)
        if dom.getElementsByTagName("fault"):
            if Debug:
                if Debug:
                    print "------Error in gettitle service-------"
            for el in dom.getElementsByTagName("string"):
                for node in el.childNodes:  
                    error = node.data
            if Debug:
                print error
            
            #write a file used by builder to catch error 
            errorFile = open(os.getcwd() + '\\ErrorFound.lhp', 'w')
            errorFile.write('Error in gettitle service.\n')   
            errorFile.write(error)   
            errorFile.close()
            return
        for el in dom.getElementsByTagName("string"):
            for node in el.childNodes:  
                self.vmeName.append(node.data)            
        pass
        
        if Debug:
            print "VME Name: " + self.vmeName[0]
        return self.vmeName[0]
        

    def endApplication(self):
        #delete file
        self.lock.deleteLockFile()

    def __del__(self):
        self.endApplication()
        
def test(stringAppType):
  try:
      if(stringAppType == "wx"):
          import wx
          app = wx.PySimpleApp()
          client = ThreadedUploaderDownloader(app , stringAppType, None)
          app.MainLoop()
      elif(stringAppType == "tk"):
          import Tkinter
          root = Tkinter.Tk()
          client = ThreadedUploaderDownloader(root ,stringAppType, root.after)
          root.mainloop()
  except:
      pass
      
if __name__ == '__main__':
        test("wx")