#-----------------------------------------------------------------------------
# author:  Gianluigi Crimi <crimi@tecno.ior.it>
# PreemptiveBasicAuthHandler is needed by biomedtown
#-----------------------------------------------------------------------------

import base64
import urllib2

class PreemptiveBasicAuthHandler(urllib2.BaseHandler):

    def __init__(self, password_manager):
        self.password_manager = password_manager

    def http_request(self, request):
        url = request.get_full_url()
        username, password = self.password_manager.find_user_password(None,url)
        if password is None:
            return request

        raw = "%s:%s" % (username, password)
        auth = 'Basic %s' % base64.b64encode(raw).strip()

        request.add_unredirected_header('Authorization', auth)
        return request
    https_request = http_request