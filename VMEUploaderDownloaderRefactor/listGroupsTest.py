import os
import listGroups
import unittest

class listGroupsTest(unittest.TestCase):
    
    def setUp(self):  
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"  
        
        self.curDir = os.getcwd()
        self.user = 'testuser' #substitute
        self.pwd  = '6w8DHF' #substitute
        
        print " current directory is: " + self.curDir
       
    def testListing(self):
        lGroups = listGroups.ListGroups()
        lGroups.SetCredentials(self.user, self.pwd)
        lGroups.getGroupsList()
        print lGroups.Result
        for test in lGroups.IdList:
            print test
        pass
    
if __name__ == '__main__':
    unittest.main()
