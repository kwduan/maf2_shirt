import DownloadHandler
import downloadSingleXML
import unittest
import os , Queue

class DownloadHandlerTest(unittest.TestCase):
      
    def setUp(self):  
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"  
        self.curDir = os.getcwd()        
        print " current directory is: " + self.curDir
       
    
    def testCreateDownloadHandlerAndDownloadFromSRB(self):
        
        d = downloadSingleXML.downloadSingleXML()
        
        d.user = "testuser"
        d.password = "6w8DHF"
        d.fileToDownload = 'dataresource.2010-06-16.1276692717524'
        d.incomingCacheDir = str(os.getcwd()+"\\testDownload\\")
        d.serviceURL = 'https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2' 
      
        d.download()
       
        d.datasetSRBURI = d.retrieveTagValue('L0000_resource_data_Dataset_DatasetURI') 
        print d.datasetSRBURI
        assert(d.datasetSRBURI == "data_55721")

        d.datasetFileSize = d.retrieveTagValue('L0000_resource_data_Size_FileSize')
        fileSizeOnRepository = d.datasetFileSize
        
        assert(fileSizeOnRepository == "1217")
        d.moveFileInIncomingCacheDirectory()

        queue = None
        observer = None
        dirCache = d.incomingCacheDir 
        if(os.path.exists(dirCache) == False): os.mkdir(dirCache)
        urlServer = 'https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2'
        usr = 'testuser'  
        pwd = '6w8DHF' 
        srbData = 'data_55721'
        fileSize = 1217
        
        downloadHandler = DownloadHandler.DownloadHandler(queue, observer, dirCache, srbData\
                                                           , usr , pwd, urlServer, True , fileSize)            
        downloadHandler.download()
        print "testCreateDownloadHandlerAndDownloadFromSRB successful!"
        
        downloadedFileSize = downloadHandler.GetCurrentLocalFileDimensionDuringDownload()
        
        self.assertEqual(int(fileSizeOnRepository), int(downloadedFileSize))
       
        downloadedFileABSName = d.incomingCacheDir + d.datasetSRBURI
        
        self.assertTrue(os.path.exists(downloadedFileABSName))
        os.remove(str(downloadedFileABSName))
        
if __name__ == '__main__':
    unittest.main()