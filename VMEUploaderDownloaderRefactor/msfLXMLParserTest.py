#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Stefano Perticoni
#-----------------------------------------------------------------------------

# This is a vme in XML
# 
# <Node Crypting="0" Id="1" Name="test_volume" Type="mafVMEVolumeGray">
# 
# Element: Node
#                 Attribute -- Name: Type  Value: mafVMEVolumeGray
#                 Attribute -- Name: Crypting  Value: 0
#                 Attribute -- Name: Id  Value: 1
#                 Attribute -- Name: Name  Value: test_volume

import Debug
import msfLXMLParser
import sys, string
from xml.dom import minidom
from xml.dom import Node
from lxml import etree as etree 

import unittest
import shutil
import difflib
import pickle
import os
import gc

class msfLXMLParserTest(unittest.TestCase):
 
        
    def setUp(self):
        
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"
        curDir = os.getcwd()        
        
        print " current directory is: " + curDir
        
        self.inFileName = curDir + r'\msf_test_import_export_VME\msf_test_import_export_VME.msf'
        self.copyFileName = curDir + r'\msf_test_import_export_VME\copyied_msf_test_import_export_VME.msf'
        shutil.copy(self.inFileName, self.copyFileName)
        
    def testFixture(self):
        pass
        
        
    def testExtractTestVolumeVME(self):
        #NodeName: Node
                #Attribute -- Name: T5ype  Value: mafVMEVolumeGray
                #Attribute -- Name: Crypting  Value: 0
                #Attribute -- Name: Id  Value: 1
                #Attribute -- Name: Name  Value: test_volume
        #Content: "
        print " Extracted vme info:"
        p = msfLXMLParser.msfLXMLParser()
        #doc = minidom.parse(self.copyFileName)
        #rootNode = doc.documentElement
        doc = etree.parse( self.copyFileName )
        rootNode = doc.getroot()
        
        vme = p.GetVmeNodeById(rootNode, 1)
        #outFile = open('vmeTestVolumeDOMNode.txt', 'w')
        #p.PrintDOMTree(vme,outFile)
    
        #doc.unlink()
        
    def testGetTagNames(self):
        
        doc = etree.parse( self.copyFileName )
        rootNode = doc.getroot()
        
        p = msfLXMLParser.msfLXMLParser()
        vme = p.GetVmeNodeById(rootNode, 1)
        tagArray = p.GetVmeTagArrayNode(vme)
        tagList = p.GetTagNames(tagArray)
        print tagList
        #doc.unlink()
        
    def testGetVmeNodeByID(self):
        
        doc = etree.parse( self.copyFileName )
        rootNode = doc.getroot()
        
        p = msfLXMLParser.msfLXMLParser()
        
        vme = p.GetVmeNodeById(rootNode, 1)
        self.assertNotEqual(vme, None)
    
        #doc.unlink()
        
    def testGetVmeTagNodeByName(self):
        
        doc = etree.parse( self.copyFileName )
        rootNode = doc.getroot()
        
        p = msfLXMLParser.msfLXMLParser()
        
        vme = p.GetVmeNodeById(rootNode, 1)
        tagArray = p.GetVmeTagArrayNode(vme)
        tagItem = p.GetVmeTagItemNodeByName(tagArray, r'Dicom_CT_peakvoltage(kV)')
        self.assertNotEqual(tagItem,None)
    
        #doc.unlink()
        
    def testGetVMEDataURLList(self):
        
        doc = etree.parse( self.copyFileName )
        rootNode = doc.getroot()
        
        p = msfLXMLParser.msfLXMLParser()
        
        vme = p.GetVmeNodeById(rootNode, 1)
        
        data = p.GetVMEDataURLList(vme)
        #print data
        self.assertEqual(data[0],"TESTmsf.1.vtk")
        self.assertEqual(len(data),1)
        
        #doc.unlink()
        
        
    def testPickle(self):
        # testing VMEUploaderDownloader serialization for list objects 
        stupidList = [1,2,3]
        outFile = open('pickleStupidList.txt', 'w')
        pickle.dump(stupidList,outFile)
        outFile.close
        outFile = open('pickleStupidList.txt', 'r')
        newList = pickle.load(outFile)
        self.assertEqual(stupidList,newList)
        
    
    def testAddTagsFromList(self):
        
        doc = etree.parse( self.copyFileName )
        rootNode = doc.getroot()
        
        p = msfLXMLParser.msfLXMLParser()
        
        vme = p.GetVmeNodeById(rootNode, 1)
        
        tagArray = p.GetVmeTagArrayNode(vme)
        self.assertNotEqual(tagArray, None)
        #p.PrintNodeToScreen(tagArray)
        #p.AddTagsFromList(doc,tagArray, [r'Dicom_CT_peakvoltage(kV)',r'pippo',r'pluto'])
        
        #outFile = open('afterTagsAdding.txt', 'w')
        #p.PrintDOMTree(vme, outFile)
        
        #doc.unlink()
        
        
if __name__ == '__main__':
    
    unittest.main()
    

