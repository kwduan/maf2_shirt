#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Roberto Mucci <r.mucci@cineca.it>
#-----------------------------------------------------------------------------

from webServicesClient import xmlrpcDemoWS
import xml.dom.minidom as xd
import os, time, shutil
from lhpDefines import *
import StringIO
import urllib
import zipfile
from Debug import Debug
import DownloadIntegrityChecker

import random

import urllib, urllib2, base64, re, os, cookielib, sys
from HttpsProxy import *

try:
    import wx
except:
    pass

class binaryImporter:
      
    def __init__(self,TestMode = False):
        self.TestMode = TestMode
        if(self.TestMode == False):
            self.app = wx.PySimpleApp()
            self.app.MainLoop()
        self.isAnimated = sys.argv[0]
        self.absOldItemURL = sys.argv[1].replace("???", " ") #URL with spaces arrives with "???" instead of them
        self.absNewItemURL = sys.argv[2].replace("???", " ") #URL with spaces arrives with "???" instead of them
        self.completeFileSizeFromRepository = -1
        self.binaryCall = sys.argv[0] + " " + sys.argv[1]+ " " + sys.argv[2]


    def GetTagValue(self, msfABSFileName, tagName):
        assert(os.path.exists(msfABSFileName))
        returnValue = ''
        try:
           file = open(msfABSFileName, 'r')
        except :
            return "Unable to open the file " , str(msfABSFileName) , " in readmode"
        # ToDo: Change this with lxml (but not significatively affect speed)
        dom = xd.parse(file)
        if dom.getElementsByTagName("fault"):
                return       
        for el in dom.getElementsByTagName('TItem'):
           if(el.attributes != None and el.attributes.get('Name')):
              attrNode = el.attributes.get('Name')
              attrValue = attrNode.nodeValue
              if(attrValue == tagName):
                  childToFind = el.getElementsByTagName('TC')[0] # first element of childList
                  returnValue = childToFind.firstChild.nodeValue
        file.close()
        
        return returnValue
    
    def CopyBinary(self):
        """ 
           Copy binary data into MSF folder
        """
                
        msfFileName = self.absOldItemURL + "\\..\\outputMAF.msf"
        resourceDataFileSize = "L0000_resource_data_Size_FileSize"
        
        if self.TestMode == False:
            self.completeFileSizeFromRepository = (long)(self.GetTagValue(msfFileName, resourceDataFileSize))
        else:
            # Set filesize in the Test class
            pass
        
        startTime = time.time()
        timeElapsed = 0
        timeOut = 60
        currentFileSize = 0
        stopCounter = 0
        lastFileSizeComputedDuringDownload = 0
        
        if (self.TestMode == False):
            downloadChecker = DownloadIntegrityChecker.DownloadIntegrityChecker()
            cur_item = DownloadIntegrityChecker.ClientDownloadStruct()
            downloadChecker.RemoveDownloadCall(self.absOldItemURL,cur_item)
        
        callFileName = "_"
        i = 0
        while(1):
            # choose a valid new filename
            i = i + 1
            callFileName = "_" + str(i)
            if(os.path.exists("DownloadCallStack\\" + callFileName + ".call") == False and os.path.exists(callFileName + ".tmp") == False):
                # create the tmp file to avoid duplicates
                tmp = open(callFileName + ".tmp","w+")
                tmp.close()
                break
        
        while 1:

            #print "timeElapsed is " + str(timeElapsed)
            # Simulate download corruption
            #deleteBin = random.randint(0,10)
            #if(deleteBin == 1):
            #    os.remove(self.absOldItemURL)
            #    #wx.MessageBox("This member is randomly chosen to be deleted to simulate download error\nBinary: %s" % self.absOldItemURL,"Debug (binaryImporter.py)", wx.ICON_INFORMATION)
            #    continue
            
            if os.path.exists(self.absOldItemURL):
                currentFileSize = (long)(os.path.getsize(self.absOldItemURL))

                if currentFileSize >= self.completeFileSizeFromRepository: # changed == to >= for external files
                    break
            else:
                currentFileSize = 0
            
            currentTime = time.time()
            timeElapsed = currentTime - startTime

            
            if (lastFileSizeComputedDuringDownload == currentFileSize):
                stopCounter = stopCounter + 1 # stopCounter * 3 sec is the time elapsed since de binary file is not modified (to avoid unnecessary waiting)
            else:
                stopCounter = 0 # reset counter
            
            if ((lastFileSizeComputedDuringDownload == currentFileSize and timeElapsed > timeOut) or stopCounter >= 10): # 30 sec since de binary file is not modified
                if Debug:
                    print "TimeOut ", timeOut, "s passed and file dimension on disk unchanged"
                    #print("Using what is on disk anyway ...")
                    print "currentFileSize is: " , currentFileSize
                    print("completeFileSizeFromRepository should be: "), self.completeFileSizeFromRepository
                    print("local vs repository file dimension mismatch...")
                #print "failed"
                if(os.path.exists(self.absOldItemURL) == False):
                    if (self.TestMode == False):
                        if(cur_item.originalVMEIdForUploadOrXMLURIForDownload == ""): # avoid to wait forever if an error occurs
                            callFile = open("DownloadCallStack\\" + callFileName + ".call","w")
                            callFile.write("1\n") # (write succes because it is unable to restart the download)
                            callFile.flush()
                            callFile.close()
                        else:                        
                            #wx.MessageBox("Unable to copy the binary file to the msf folder.\nProbably the binary file was not downloaded correctly.\nBinary: %s" % self.absOldItemURL,"Error in binaryImporter!",wx.ICON_ERROR)
                            # Integrity mechanism!!!
                            # here save a .call file
                            #print "write the corrupted file"
                            callFile = open("DownloadCallStack\\" + callFileName + ".call","w")
                            callFile.write("0\n") # failure
                            callFile.write(cur_item.cacheVMEIdForUploadOrSizeForDownload + "\n")
                            callFile.write(cur_item.currentCacheChildABSFolderWorkaround + "\n")
                            callFile.write(cur_item.user + "\n")
                            callFile.write(cur_item.repositoryURL + "\n")
                            callFile.write(cur_item.originalVMEIdForUploadOrXMLURIForDownload + "\n")
                            callFile.write(cur_item.vmeHasLinkForUploadOrSRBDataURIForDownload + "\n")
                            callFile.write(cur_item.isLastResourceToDownload + "\n")
                            callFile.write(self.binaryCall + "\n")
                            callFile.flush()
                            callFile.close()
                    os.remove(callFileName + ".tmp") # remove temporary file
                    return # Error binary file not downloaded
                else:
                    break # Use the incomplete downloaded files
            
            lastFileSizeComputedDuringDownload = currentFileSize
            
                
            # check every 3 seconds
            time.sleep(3)
        
        if (self.isAnimated == "false"):
            shutil.copy(self.absOldItemURL, self.absNewItemURL)
             #tell to the user that binary data is ready?
        
        else:
            baseName = os.path.split(self.absNewItemURL)
            id = baseName[1].split('.')[1]          
            baseName = baseName[1].split('.')[0]
            fileIn = zipfile.ZipFile(self.absOldItemURL, "r")
            fileOut = zipfile.ZipFile(self.absNewItemURL, "w")
            now = time.localtime(time.time())[:6]
            
            for name in fileIn.namelist():
                data = fileIn.read(name)
                ext = name.split('.')[2]    
                id = int(id) + 1
                newName = baseName + '.' + str(id) + '.' + ext
                info = zipfile.ZipInfo(newName)
                info.date_time = now
                info.compress_type = zipfile.ZIP_DEFLATED
                fileOut.writestr(info, data)
         
            fileIn.close()
            fileOut.close()
            
        if (self.TestMode == False):

            # Here use client call stack to detect if all vme are downloaded correctly
                        
            #downloadIntegrityChecker = DownloadIntegrityChecker.DownloadIntegrityChecker()
            #downloadIntegrityChecker.RemoveDownloadCall(self.absOldItemURL)
            callFile = open("DownloadCallStack\\" + callFileName + ".call","w")
            callFile.write("1\n") # succes (parameters are not useful)
            callFile.flush()
            callFile.close()
            os.remove(callFileName + ".tmp") # remove temporary file
            #wx.MessageBox("Number of pending downloads %d" % downloadIntegrityChecker.GetNumberOfPendingDownloads(),"Information! (binaryImporter)",wx.ICON_INFORMATION)
            #if downloadIntegrityChecker.GetNumberOfPendingDownloads() == 0:
            #    if (self.TestMode == False):
            #        wx.MessageBox("Download Completed!","Information! (binaryImporter)",wx.ICON_INFORMATION)
            
            #wx.MessageBox("Binary copy!\nfrom - %s" % self.absOldItemURL + "\nTo: - %s" % self.absNewItemURL,"Information",wx.ICON_INFORMATION)
            self.app.Exit()
  
def main():
    
    usage_msg = '''Usage: %s isAnimated, absOldItemURL, absNewItemURL ''' % sys.argv[0]
    if(len(sys.argv) <3): 
         print 'Error :\n' + usage_msg
         sys.exit(1)

       
    if(len(sys.argv) >= 3):
        sys.argv = sys.argv[1:]
        #print sys.argv
        importer = binaryImporter()
        importer.CopyBinary()

if __name__ == '__main__':
    main()
        