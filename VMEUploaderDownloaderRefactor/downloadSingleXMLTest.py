#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Stefano Perticoni <s.perticoni@scsolutions.it>
#-----------------------------------------------------------------------------


import os
import downloadSingleXML
import unittest
import sys

class downloadSingleXMLTest(unittest.TestCase):
    
    def setUp(self):  
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"        
        self.curDir = os.getcwd()
        
        print " current directory is: " + self.curDir
       
    def testDownloadSingleXML(self):
        d = downloadSingleXML.downloadSingleXML()
        
        d.user = "testuser"
        d.password = "6w8DHF"
        d.serviceURL = 'https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2' 
        d.fileToDownload = 'dataresource.2010-06-16.1276692717524'
        d.incomingCacheDir = str(os.getcwd()+"\\testDownload\\")
      
        d.download()
        
        d.datasetSRBURI = d.retrieveTagValue('L0000_resource_data_Dataset_DatasetURI')
        assert(d.datasetSRBURI == "data_55721")

        d.datasetFileSize = d.retrieveTagValue('L0000_resource_data_Size_FileSize')
        assert(d.datasetFileSize == "1217")

        d.moveFileInIncomingCacheDirectory()
        
    def testMain(self):
       
       #main handle arguments
       #0 program
       #1 user
       #2 pwd
       #3 xml resource file name to download
       #4 abs dir path where to download it

        sys.argv = []
        sys.argv.append("downloadSingleXML.py")
        sys.argv.append("testuser")
        sys.argv.append("6w8DHF")
        sys.argv.append("https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2")
        sys.argv.append('dataresource.2010-06-16.1276692717524')
        sys.argv.append(os.getcwd() + "\\testDownload\\")
        result = downloadSingleXML.main()
       
if __name__ == '__main__':
    unittest.main()
