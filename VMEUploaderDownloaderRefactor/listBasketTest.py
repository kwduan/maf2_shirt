import os
import listBasket
import unittest

class listBasketTest(unittest.TestCase):
    
    def setUp(self):  
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"  
        
        self.curDir = os.getcwd()
        self.user = 'testuser' #substitute
        self.pwd  = '6w8DHF' #substitute
        
        
        print " current directory is: " + self.curDir
       
    def testListing(self):
        lBasket = listBasket.listBasket(3)
        lBasket.SetCredentials(self.user, self.pwd)
        lBasket.getListFromBasket()
        print lBasket.Result
        for test in lBasket.IdList:
            print test
        pass
    
    def testWritingOnFile(self):
        lBasket = listBasket.listBasket(0)
        lBasket.SetCredentials(self.user, self.pwd)
        lBasket.getListFromBasket()
        lBasket.IdListSelected = lBasket.IdList
        lBasket.writeIdListSelectedOnFile()
         
        file = open(lBasket.fileName, 'r')
        
        stream = file.read()
        
        print "***FILE***"
        print stream     
        file.close()
        print "***END FILE***"
    
if __name__ == '__main__':
    unittest.main()
