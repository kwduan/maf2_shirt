#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Roberto Mucci <r.mucci@cineca.it>
#-----------------------------------------------------------------------------

import XMLResourceCreator
import os, time, shutil
from lhpDefines import *
import StringIO
import unittest
import sys

class lhpGetXMLURITest(unittest.TestCase):
      
    def testGetURI(self):
        
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"
        
        curDir = os.getcwd()        
        
        print " current directory is: " + curDir
        
        resourceCreator = XMLResourceCreator.XMLResourceCreator()
        resourceCreator.userName = "testuser"       
        resourceCreator.password = "6w8DHF"
        resourceCreator.URL = "https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2" #substitute
        
        returnedResourceName =  resourceCreator.CreateEmptyXMLResource()
        
        findStringPosition = returnedResourceName.find("dataresource")
        
        self.assertEqual(findStringPosition, 0)
        
if __name__ == '__main__':
    unittest.main()
        