#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# author: Roberto Mucci <r.mucci@cineca.it>
#-----------------------------------------------------------------------------

import lhpReadRemoteTag
import os, time, shutil
from lhpDefines import *
import StringIO
import unittest
import sys

class lhpReadRemoteTagTest(unittest.TestCase):
      
    def testReadTag(self):
        
        resource_TagName = 'dataresource.2010-06-16.1276692717524,L0000_resource_MAF_TreeInfo_VmeChildURI1'
        self.readRemoteTagHelper(resource_TagName)
        
    def readRemoteTagHelper(self, resource_TagName):    
        print "Beware:  In order to work run this test must be launched from VMEUploaderDownloader dir!"
        
        curDir = os.getcwd()        
        
        print " current directory is: " + curDir
        
        serviceURL = "https://www.biomedtown.org/biomed_town/LHDL/users/repository/lhprepository2/";
  
        sys.argv = []
        sys.argv.append("testuser") 
        sys.argv.append("6w8DHF")
        sys.argv.append(serviceURL)
        sys.argv.append(resource_TagName)     
        read = lhpReadRemoteTag.lhpReadRemoteTag()
        returnTagValue = read.ReadTag()
        
        self.assertEqual(returnTagValue, "This tag will be filled during the upload process")
        
if __name__ == '__main__':
    unittest.main()
        