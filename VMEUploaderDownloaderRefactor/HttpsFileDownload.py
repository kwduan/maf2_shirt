#-----------------------------------------------------------------------------
# BEWARE!!! This is mostly a prototype!!!
# code is changing very fast so don't rely on it :P
# authors: Stefano Perticoni <s.perticoni@scsolutions.it>, Gianluigi Crimi <crimi@tecno.ior.it>
#
# ATTENTION: if your system is behind a proxy server you must set the environment variable 'https_proxy' 
# to 'http://<user>:<password>@<server>:<port>
#-----------------------------------------------------------------------------

import urllib2, string, PreemptiveBasicAuthHandler
from Debug import Debug
import sys

class HttpsFileDownload:
    """Download selected file through HTTPS protocol.\
    See main() comments for further details"""

    def __init__(self):
        
        # URL
        self.Host = "https://www.biomedtown.org"
        self.FileSelector = "/biomed_town/LHDL/users/swclient/tagsToBeRemovedFromUploadedVMETagArray/tagsToBeRemovedFromUploadedVMETagArray"
        
        # authentication
        self.Username = 'lhpparabuild'
        self.Password = '2bf5ZM'
        
        # output file name
        self.OutputFileName = "downloadedFile.txt"
        
    def Download(self):
    
        #if Debug:
        print "Connecting to self.Host: " + self.Host            
        print "Retrieving: " + self.FileSelector

		#Creating password manager for biomedtown login
        passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
        passman.add_password(None, self.Host, self.Username, self.Password)

        #Creating and install URL opener with preemptive authentication
        authhandler = PreemptiveBasicAuthHandler.PreemptiveBasicAuthHandler(passman)
        opener = urllib2.build_opener(authhandler)
        urllib2.install_opener(opener)

        #Get file from https and save to the output file
        f = open(self.OutputFileName, 'w')
        f.write(urllib2.urlopen(self.Host+self.FileSelector).read())
        f.close()
        
        if Debug:    
            f2 = open(self.OutputFileName, 'r')
            for line in f2:
                print line
                
                
        return 0;
    
    
def run(host, fileSelector, outputFileName):                                            
    
    dictDownloader = HttpsFileDownload()    
    dictDownloader.Host = host   
    dictDownloader.FileSelector = fileSelector
    dictDownloader.OutputFileName = outputFileName
    dictDownloader.Download()
    
    return dictDownloader.OutputFileName

def main():
    args = sys.argv[1:]
    if len(args) != 3:
        print """
        usage: python.exe HttpsFileDownload.py
        Host FileSelector OutputFileName
        
        For example:
        Host = "https://www.biomedtown.org"
        FileSelector = "/biomed_town/LHDL/users/swclient/tagsToBeRemovedFromUploadedVMETagArray/tagsToBeRemovedFromUploadedVMETagArray"
        OutputFileName = "pippo.txt"
        """
        sys.exit(-1)
    print args
    run(args[0],args[1],args[2])

    

if __name__ == '__main__':
    main()