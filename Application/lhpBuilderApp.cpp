/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: lhpBuilderApp.cpp,v $
Language:  C++
Date:      $Date: 2012-04-26 12:54:59 $
Version:   $Revision: 1.1.1.1.2.69 $
Authors:   Paolo Quadrani , Stefano Perticoni
==========================================================================
Copyright (c) 2001/2005
CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/

#include "medDefines.h"
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------
#include "lhpBuilderDecl.h"
#include "lhpBuilderApp.h"

#include <wx/datetime.h>
#include <wx/config.h>

#include "mafDecl.h"
#include "mafVMEFactory.h"
#include "mafPics.h"
#include "mafGUIMDIFrame.h"
#include "mafInteractionFactory.h"

#include "mafNodeFactory.h"
#include "mafNodeGeneric.h"
#include "mafNodeRoot.h"
#include "mafVMERoot.h"
#include "mafVMESurface.h"
#include "mafVMELandmark.h"
#include "mafPipeFactoryVME.h"
#include "mafPipeVolumeSlice.h"
#include "medPipeFactoryVME.h"
#include "medVMEFactory.h"
#include "medPipeVolumeDRR.h"
#include "medPipeTrajectories.h"
#include "mafVMEAFRefSys.h"
#include "lhpVMEPoseGroup.h"
#include "mafVMEHelAxis.h"
#include "mafOpDecomposeTimeVarVME.h"
#include "mafOpImporterMSF.h"
#include "mafOpImporterExternalFile.h"
#include "mafOpLabelExtractor.h"
#include "mafOpOpenExternalFile.h"
#include "mafOpExporterBmp.h"
#include "mafOpCreateGroup.h"
#include "mafOpCreateMeter.h"
#include "medOpCreateWrappedMeter.h"
#include "mafOpCreateSlicer.h"
#include "mafOpCreateRefSys.h"
#include "mafOpFilterSurface.h"
#include "mafOpEditMetadata.h"
#include "mafOp2DMeasure.h"
#include "mafOpReparentTo.h"
// #include "mmoDICOMImporter.h"
#include "medOpImporterDicomOffis.h"
#include "mafOpReparentTo.h"
#include "mafOpImporterImage.h"
#include "mafOpImporterSTL.h"
#include "mafOpExporterSTL.h"
#include "mafOpExporterVTK.h"
#include "mafOpImporterVTK.h"
#include "mafOpImporterMSF1x.h"
#include "mafOpImporterVRML.h"
#include "mafOpCreateVolume.h"
#include "mafOpVOIDensityEditor.h"
#include "mafOpImporterBBF.h"
//BES: 23.6.2008 - Large Volume - to be merged
#include "mafOpImporterRAWVolume_BES.h"
#include "mafOpImporterRAWVolume.h"
#include "mafOpExporterRaw.h"
#include "medOpImporterRAWImages.h"
#include "mafOpExtractIsosurface.h"
#include "mafOpCrop.h"
#include "mafOpVOIDensity.h"
#include "medOpVolumeResample.h"
#include "mafOpAddLandmark.h"
#include "medOpRegisterClusters.h"
#include "mafOpCreateSurfaceParametric.h"
#include "mmoBuildHierarchy.h"
#include "mmoTimeReduce.h"
#include "mmoINPExporter.h"
#include "mmoMTRExporter.h"
#include "mmoINPImporter.h"
#include "mmoMTRImporter.h"
#include "mmoLnSurf.h"
#include "mmoAFSys.h"
#include "mmoAverageLM.h"
#include "mmoHelAxis.h"
#include "mmoStickPalpation.h"
#include "medOpScaleDataset.h"
#include "medOpMove.h"
//#include "mafOpMAFTransform.h"
#include "medOpImporterGRFWS.h"
#include "medOpExporterGRFWS.h"
#include "medPipeGraph.h"
#include "medPipeVectorFieldMapWithArrows.h"
#include "mafVMERawMotionData.h"
//#include "lhpOpImporterC3D.h"
#ifdef MED_USE_BTK
#include "lhpOpImporterC3DBTK.h"
#endif
#include "medOpImporterMotionData.h"
#include "medOpExporterLandmark.h"
#include "medOpExporterLandmarkWS.h"
#include "medOpExporterMeters.h"
#include "medOpClassicICPRegistration.h"
#include "mafOpImporterMesh.h"
#include "mafOpExporterMesh.h"
#include "mafOpImporterVMEDataSetAttributes.h"
#include "medOpImporterLandmark.h"
#include "medOpImporterLandmarkWS.h"
#include "lhpOpBonemat.h"
#include "lhpOpBonematCaller.h"
#include "lhpOpImporterAnsysInputFile.h"
#include "lhpOpExporterAnsysInputFile.h"
#include "lhpOpImporterAnsysCDBFile.h"
#include "medOpFreezeVME.h"
#include "medOpExporterWrappedMeter.h"
#include "medOpIterativeRegistration.h"
#include "medOpCreateLabeledVolume.h"
#include "lhpOpKeyczarIntegrationTest.h"
#include "medOpSurfaceMirror.h"
#include "medOpImporterAnalogWS.h"
#include "medOpExporterAnalogWS.h"
#include "medOpMML.h"
#include "medOpMML3.h"
#include "lhpOpOpenRepository.h"
#include "mafViewVTK.h"
#include "mafViewCompound.h"
#include "mafViewRXCT.h"
#include "mafViewRX.h"
#include "mafViewOrthoSlice.h"
#include "mafViewArbitrarySlice.h"
#include "medViewArbitraryOrthoSlice.h"
#include "mafViewGlobalSliceCompound.h"
#include "mafViewSlice.h"
#include "mafViewImageCompound.h"
#include "mafViewIntGraph.h"
#include "medViewSlicer.h"
#include "medViewVTKCompound.h"
#include "lhpViewAnalogGraphXY.h"

#include "lhpOpMultiscaleExplore.h"
#include "lhpOpTextureOrientation.h"
#include "lhpOpComputeTensor.h"
#include "medOpCropDeformableROI.h"
#include "mafOpValidateTree.h"
#include "mafOpGarbageCollectMSFDir.h"
#include "mafOpApplyTrajectory.h"

#include "lhpOp3DVolumeBrush.h"

// #include "medOpImporterDicomXA.h"

#include "medOpComputeWrapping.h"
#include "medVMEComputeWrapping.h"
#include "medPipeComputeWrapping.h"

#include "medGUIDicomSettings.h"

#include "medOpMakeVMETimevarying.h"
#include "medOpEqualizeHistogram.h"
#include "medOpSegmentationRegionGrowingLocalAndGlobalThreshold.h"
#include "medOpSegmentation.h"
#include "lhpOpExtractGeometry.h"

#include "lhpOpImporterAnalogASCII.h"
#include "lhpOpImporterMeshFeBio.h"

#include "lhpOpComputeHausdorffDistance.h"

//temporary for testing
#include "mafViewSingleSliceCompound.h"

// VMEUploaderDownloader Refactor Target
#include "lhpOpUploadVMERefactor.h"
#include "lhpGUISettingsOpUploadMultiVMERefactor.h"
#include "lhpOpUploadMultiVMERefactor.h"
#include "lhpOpEditTagRefactor.h"
#include "lhpOpDownloadVMERefactor.h"
#include "mafOpExporterMSF.h"

#ifdef MAF_USE_ITK
#include "lhpOpCreateSurfaceScalar.h"
#include "lhpVMESurfaceScalarVarying.h"
#endif
#include "lhpVisualPipeSurfaceScalar.h"

#include "lhpOpASFMotionRetargeting.h"
#include "lhpOpMotionRetargeting.h"
#include "lhpOpConvertMotionData.h"

//BES: 14.11.2008 - added muscle wrapping
#include "medOpCreateMuscleWrapper.h"
#include "medVMEMuscleWrapper.h"
#include "medOpMeshDeformation.h"
#include "medVMEMusculoSkeletalModel.h"
#include "mafVMEShortcut.h"
#include "mafOpCreateVMEShortcut.h"

#include "lhpOpMeshGeneratorExecutable.h"
#include "lhpVMEScalarMatrix.h"

#include "lhpPipeVolumeBrushSlice.h"

#include "medPipeCompoundVolumeFixedScalars.h"

#include <vtkTimerLog.h>
#include "lhpOpBonematBatch.h"

#if defined(VPHOP_WP10)	//for testing purposes only
#include "lhpOpRemoveTracability.h"
#include "lhpOpMultiReparentTo.h"
#include "lhpOpMergeSurfaces.h"
#include "lhpOpMoveVMEUp.h"
#include "lhpOpMoveVMEDown.h"
#include "lhpOpSmoothMuscle.h"
#include "medOpMeshQuality.h"
#include "medOpCleanSurface.h"
#include "mafOpDecimateSurface.h"
#include "mafOpConnectivitySurface.h"
#include "lhpOpSetLowerRes.h"
#include "lhpOpFuseMotionData.h"
#include "lhpOpComputeProgressiveHull.h"
#include "medOpCreateJoint.h"
#include "medOpCreateParticle.h"
#include "medVMEJoint.h"
#include "medPipeJoint.h"
#include "lhpOpFilterPolyDataUsingExtApp.h"
#include "lhpOpScaleMMA.h"
#include "lhpOpExtractLandmarkEOS.h"
#include "lhpOpMSModelCheckTags.h"
#include "lhpOpMuscleDecompose.h"
#endif

#include "mafOpRemoveCells.h"
#include "medOpSmoothSurfaceCells.h"
#include "medOpInteractiveClipSurface.h"
#include "medOpExtrusionHoles.h"
#include "medOpSplitSurface.h"
#include "medOpFillHoles.h"
#include "lhpOpShowHistogram.h"
#include "lhpOpComputeGradientStructureTensor.h"

#include "medViewSliceBlendRX.h"

#include "medViewSliceNotInterpolatedCompound.h"

// TODO: REFACTOR THIS
// this component is used only to override the Accept,
// and it`s the minimal amount of code in order to override the method
// it could go in a separate file with other redefined Accept`s

class lhpOpMove : public medOpMove
{
public:

	/** Return true for the acceptable vme type. */
	bool Accept(mafNode* vme)
	{
		bool accepted = false;

		accepted =   !vme->IsA("lhpVMESurfaceScalarVarying") \
			&& !vme->IsA("mafVMEMeter") \
			&& !vme->IsA("medVMELabeledVolume") \
			&& !vme->IsA("mafVMEHelicalAxis");

		if (accepted == false)
		{
			return false;
		}
		else
		{
			return medOpMove::Accept(vme);
		}
	}
};

class lhpOpScaleDataset : public medOpScaleDataset
{
public:

	/** Return true for the acceptable vme type. */
	bool Accept(mafNode* vme)
	{
		bool accepted = false;

		accepted =   !vme->IsA("lhpVMESurfaceScalarVarying") \
			&& !vme->IsA("mafVMEMeter") \
			&& !vme->IsA("medVMELabeledVolume") \
			&& !vme->IsA("mafVMEHelicalAxis");

		if (accepted == false)
		{
			return false;
		}
		else
		{
			return medOpScaleDataset::Accept(vme);
		}
	}
};

// END TODO: REFACTOR THIS

//BES: 23.6.2008 - TO BE UNCOMMENTED WHEN VTK IS PATCHED AS I SUGGESTED
////BES: 14.5.2008 - special memory manager for supporting of large data
//#include <vtkDataArrayMemMng.h>

//--------------------------------------------------------------------------------
// Create the Application
//--------------------------------------------------------------------------------

IMPLEMENT_APP(lhpBuilderApp)

	////BES: 14.5.2008 - OnIdle to unlock blocks
	//BEGIN_EVENT_TABLE(lhpBuilderApp, wxApp)
	//  EVT_IDLE(lhpBuilderApp::OnIdle)
	//END_EVENT_TABLE()

	//UNCOMMENT TO ALLOW MEMORY LEAKS TRACER (SLOW, ESPECIALLY FROM OpenFile DIALOGS)
	//#include <vld.h>

	//--------------------------------------------------------------------------------
	bool lhpBuilderApp::OnInit()
	//--------------------------------------------------------------------------------
{
	////BES: 14.5.2008 - initialize the manager (with default settings)
	////It provides a sophisticated memory-disk swap algorithm that
	////allows handling of many large size memory blocks that would
	////take more memory than available in total.
	////The mechanism is NOT GUARANTEED TO BE SAFE
	////Swapping (especially, if HandleNewFailure option is enabled)
	////may lead to artifacts in data or even
	//vtkDataArrayMemMng::InitializeManagerUnSafeMode();

	mafPictureFactory::GetPictureFactory()->Initialize();

#include "pic/lhpBuilder/FRAME_ICON16x16.xpm"
	mafADDPIC(FRAME_ICON16x16);

#include "pic/lhpBuilder/FRAME_ICON32x32.xpm"
	mafADDPIC(FRAME_ICON32x32);

#include "pic/lhpBuilder/MDICHILD_ICON.xpm"
	mafADDPIC(MDICHILD_ICON);

	int result;

	result = medVMEFactory::Initialize();
	assert(result == MAF_OK);

	result = medPipeFactoryVME::Initialize();
	assert(result==MAF_OK);

	result = mafInteractionFactory::Initialize();
	assert(result==MAF_OK);

	mafPlugNode<mafVMERawMotionData>("VME representing raw motion data");
	mafPlugNode<mafVMEAFRefSys>("VME representing anatomical frame");
	mafPlugNode<lhpVMEPoseGroup>("Pose group for time-varying vme that not inherit for mafVMEGEneric");
	mafPlugNode<mafVMEHelAxis>("VME representing helical axis");
	mafPlugNode<mafVMEVolumeLarge>("VME storing large volume datasets with one scalar component");
	mafPlugNode<lhpVMEScalarMatrix>("VME representing analog timevariyng data");
	mafPlugNode<medVMEComputeWrapping>("Generalized another VME Meter with wrapping geometry");
	mafPlugPipe<medPipeComputeWrapping>("Pipe to Visualize Compute Wrapping Meter");
	mafPlugPipe<lhpPipeAnalogGraphXY>("vph2PipeAnalogGraph");

#ifdef MAF_USE_ITK
	mafPlugNode<lhpVMESurfaceScalarVarying>("VME representing surface with attached time varying mafVMEScalar");
#endif

	//BES: 14.11.2008 - some stupid VME to demonstrate muscle wrapping
	mafPlugNode<medVMEMusculoSkeletalModel>("Hierarchical VME representing a musculoskeletal model");
	mafPlugNode<medVMEMuscleWrapper>("Procedural VME representing muscle deformed according to its action lines");

#ifdef VPHOP_WP10
	mafPlugNode<medVMEJoint>("Procedural-like VME that represents a joint in the musculoskeletal model");
	mafPlugPipe<medPipeJoint>("Visual pipe to render a joint (of musculoskeletal model)");
#endif

	mafPlugPipe<lhpVisualPipeSurfaceScalar>("Visual pipe to render a surface with its scalar values");

	//Youbing:13.04.2010 - plug the lhpVolumeBrushSlice pipe
	mafPlugPipe<lhpPipeVolumeBrushSlice>("Visual pipe to render paintings on a volume slice");

	//BES: 25.2.2011
	mafPlugNode<mafVMEShortcut>("VME that provides a virtual link (shortcut) to another VME");

	m_Logic = new lhpBuilderLogic();
	m_Logic->GetTopWin()->SetTitle("LHPBuilder");
	m_Logic->Configure();
	SetTopWindow(mafGetFrame());

	wxRegKey RegKey(wxString("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\lhpBuilder"));
	if(RegKey.Exists())
	{
		RegKey.Create();
		wxString revision;
		RegKey.QueryValue(wxString("DisplayVersion"), revision);
		m_Logic->SetRevision(revision);
	}
	else
	{
		wxString revision="0.1";
		m_Logic->SetRevision(revision);
	}

	//------------------------- Importers -------------------------
	medGUIDicomSettings *dicomSettings=new medGUIDicomSettings(NULL,"DICOM");
	m_Logic->Plug(new medOpImporterDicomOffis("DICOM"),"Images",true,dicomSettings);

	// m_Logic->Plug(new mmoDICOMImporter("DICOM"),"Images");
	// m_Logic->Plug(new medOpImporterDicomXA("DICOM XA"),"Images");
	m_Logic->Plug(new mafOpImporterSTL("STL"),"Geometries");
	m_Logic->Plug(new mafOpImporterVTK("VTK"),"Other");
	m_Logic->Plug(new mafOpImporterMSF("MSF"),"Other");
	m_Logic->Plug(new mafOpImporterMSF1x("MAF 1.x"),"Other");
	m_Logic->Plug(new mafOpImporterBBF("BFF (VolumeLarge)"),"Other");
	m_Logic->Plug(new mafOpImporterRAWVolume_BES("Raw Volume"),"Images");
	m_Logic->Plug(new mafOpImporterRAWVolume("Raw Volume Legacy"),"Images");
	m_Logic->Plug(new medOpImporterRAWImages("Raw Images"),"Images");
	//m_Logic->Plug(new medOpImporterRAWImages("Raw Images Legacy"),"Images");
	m_Logic->Plug(new mafOpImporterImage("Images"),"Images");
	//m_Logic->Plug(new lhpOpImporterC3D("C3D"),"Motion Analysis");
#ifdef MED_USE_BTK
	m_Logic->Plug(new lhpOpImporterC3DBTK("C3D-BTK"),"Motion Analysis");
#endif
	m_Logic->Plug(new medOpImporterLandmark("Landmark"),"Motion Analysis");
	m_Logic->Plug(new medOpImporterMotionData<mafVMERawMotionData>("Raw Motion Data", "RAW Motion Data (*.MAN)|*.MAN", "Dictionary (*.txt)|*.txt"), "Motion Analysis");
	// m_Logic->Plug(new mmoLandmarkImporter("Landmark")); //Old Importer
	m_Logic->Plug(new medOpImporterAnalogWS("ASCII Analog (VWs)"), "Motion Analysis");
	m_Logic->Plug(new medOpImporterLandmarkWS("ASCII Trajectories (VWs)"),"Motion Analysis");
	m_Logic->Plug(new medOpImporterGRFWS("ASCII Force Plates (VWs)"), "Motion Analysis");
	m_Logic->Plug(new mafOpImporterMesh("Generic Mesh"), "Finite Element");
	m_Logic->Plug(new lhpOpImporterAnsysInputFile("Ansys Input File"), "Finite Element");
	m_Logic->Plug(new lhpOpImporterAnsysCDBFile("Ansys CDB File"), "Finite Element");
	m_Logic->Plug(new mafOpImporterVRML("VRML"), "Geometries");
	m_Logic->Plug(new mmoINPImporter("INP/INP_AF"), "Geometries");
	m_Logic->Plug(new mmoMTRImporter("MTR"), "Geometries");
	m_Logic->Plug(new mafOpImporterExternalFile("External data"), "Other");
	m_Logic->Plug(new lhpOpImporterAnalogASCII("ASCII Analog (X,Y)"),"Other");
	m_Logic->Plug(new lhpOpImporterMeshFeBio("FEBio"), "Finite Element");

	//-------------------------------------------------------------

	//------------------------- Exporters -------------------------
	m_Logic->Plug(new mafOpExporterMSF("MSF (for AE Testing)"), "Other");
	m_Logic->Plug(new mafOpExporterSTL("STL"),"Geometries");
	m_Logic->Plug(new mmoINPExporter("INP"),"Geometries");
	m_Logic->Plug(new mmoMTRExporter("MTR"), "Geometries");
	m_Logic->Plug(new mafOpExporterVTK("VTK"), "Other");
	m_Logic->Plug(new mafOpExporterRAW("Raw"), "Images");
	m_Logic->Plug(new mafOpExporterBmp("Bmp"), "Images");
	m_Logic->Plug(new medOpExporterLandmark("Landmark"), "Motion Analysis");
	m_Logic->Plug(new medOpExporterWrappedMeter("Action Line"), "Other");
	m_Logic->Plug(new medOpExporterAnalogWS("ASCII Analog (VWs)"),"Motion Analysis");
	m_Logic->Plug(new medOpExporterLandmarkWS("ASCII Trajectories (VWs)"),"Motion Analysis");
	m_Logic->Plug(new medOpExporterGRFWS("ASCII Force Plates (VWs)"),"Motion Analysis");
	m_Logic->Plug(new medOpExporterMeters("Meters"), "Other");
	m_Logic->Plug(new mafOpExporterMesh("Generic Mesh"), "Finite Element");
	m_Logic->Plug(new lhpOpExporterAnsysInputFile("Ansys Input File"),"Finite Element");
	//-------------------------------------------------------------

	//------------------------- Operations -------------------------

	//	m_Logic->Plug(new lhpOpDeleteCorruptedVMEs("BES: Delete Corrupted VMEs"), "MSF Tools");

	m_Logic->Plug(new mafOpValidateTree("Validate MSF Tree"), "MSF Tools");
	m_Logic->Plug(new mafOpGarbageCollectMSFDir("Garbage Collect MSF Directory"), "MSF Tools");

	m_Logic->Plug(new mafOpCreateGroup("Group"),"Create/New");
	m_Logic->Plug(new mafOpCreateVolume("Constant Volume"),"Create/New");
	m_Logic->Plug(new mafOpCreateVMEShortcut("Shortcut"),"Create/New");

#ifdef MAF_USE_ITK
	m_Logic->Plug(new lhpOpCreateSurfaceScalar("Surface Scalar"),"Create/Derive");
#endif
	m_Logic->Plug(new medOpMakeVMETimevarying("Time Varying VME"),"Create/Derive");
	m_Logic->Plug(new mmoLnSurf("Lineset and surface"),"Create/Derive");
	m_Logic->Plug(new mafOpCreateRefSys("Refsys"),"Create/New");
	m_Logic->Plug(new mafOpCreateSlicer("Slicer"),"Create/Derive");
	m_Logic->Plug(new mafOpCreateSurfaceParametric("Parametric Surface"),"Create/New");
	m_Logic->Plug(new mafOpAddLandmark("Add Landmark \tCtrl+A"),"Create/New");
	m_Logic->Plug(new medOpFreezeVME("Freeze VME"),"Create/Derive");
	m_Logic->Plug(new medOpRegisterClusters("Register Landmark Cloud"),"Modify/Fuse");
	m_Logic->Plug(new mafOpCreateMeter("Distance Meter"),"Create/Derive");
	//m_Logic->Plug(new medOpCreateWrappedMeter("Wrapped Meter"),"Create/Derive");
	m_Logic->Plug(new medOpComputeWrapping("Wrapped Action Line"),"Create/Derive");//15-1-2009

	m_Logic->Plug(new medOpCreateMuscleWrapper("Create Muscle Wrapper"),"Create/Derive"); //BES: 14.11.2008

	// m_Logic->Plug(new mmoEditMetadata("Metadata Editor"),"Modify");
	m_Logic->Plug(new medOpEqualizeHistogram(),"Modify");
	m_Logic->Plug(new mafOpVOIDensityEditor("Volume Density"),"Modify");
	m_Logic->Plug(new medOpMeshDeformation("Deform Surface"), "Modify");
	m_Logic->Plug(new mafOpApplyTrajectory("Apply Trajectory"), "Modify");
	m_Logic->Plug(new mafOpExtractIsosurface("Extract Isosurface"),"Create/Derive");
	m_Logic->Plug(new medOpSegmentationRegionGrowingLocalAndGlobalThreshold(),"Create/Derive");
	m_Logic->Plug(new medOpSurfaceMirror("Surface Mirror"),"Modify");
	m_Logic->Plug(new mafOpCrop("Crop Volume"),"Modify");
	m_Logic->Plug(new medOpVolumeResample("Volume Resample"),"Modify");
	m_Logic->Plug(new mafOp2DMeasure("2D Measure"),"Measure");
	m_Logic->Plug(new mafOpVOIDensity("VOI Density"),"Measure");
	m_Logic->Plug(new mafOpReparentTo("Reparent to...  \tCtrl+R"),"Modify/Fuse");
	m_Logic->Plug(new lhpOpScaleDataset(),"Modify");
	m_Logic->Plug(new lhpOpMove(),"Modify");
	m_Logic->Plug(new medOpCropDeformableROI(_("Masking")),_("Modify"));
	m_Logic->Plug(new mafOpImporterVMEDataSetAttributes("VME DataSet Attributes Adder"),"Modify");
	m_Logic->Plug(new medOpClassicICPRegistration("Register Surface"),"Modify/Fuse");
	m_Logic->Plug(new mmoAFSys("AFRefsys"),"Create/Derive");
	m_Logic->Plug(new mmoAverageLM("Average landmark"),"Create/Derive");
	m_Logic->Plug(new mmoStickPalpation("Wand palpated landmark"),"Create/Derive");
	m_Logic->Plug(new medOpSegmentation("Segmentate volume"),"Create/Derive");
	m_Logic->Plug(new lhpOpExtractGeometry("Surface from binary volume"),"Create/Derive");
	m_Logic->Plug(new lhpOpComputeHausdorffDistance("Compute Hausdorff Distance"), "Create/Derive");

	m_Logic->Plug(new mmoHelAxis("Helical axis"),"Create/Derive");
	m_Logic->Plug(new mmoTimeReduce("Time reduce"),"Modify");
	m_Logic->Plug(new mmoBuildHierarchy("Make hierarchical"),"Modify/Fuse");
	m_Logic->Plug(new lhpOpBonemat("Bonemat"),"Modify");
  m_Logic->Plug(new lhpOpBonematCaller("Bonemat Caller"),"Modify");
	m_Logic->Plug(new lhpOpBonematBatch("Bonemat Batch"),"Modify");
	m_Logic->Plug(new lhpOpMeshGeneratorExecutable("Generate Volumetric Mesh"),"Create/New");
	m_Logic->Plug(new medOpIterativeRegistration("Iterative Registration"),"Modify/Fuse");
	m_Logic->Plug(new mafOpOpenExternalFile("Open with external program"),"Manage");
	m_Logic->Plug(new medOpCreateLabeledVolume("Labeled Volume"),"Create/Derive");

	m_Logic->Plug(new mafOpDecomposeTimeVarVME("Decompose Time"),"Create/Derive");
	m_Logic->Plug(new mafOpLabelExtractor("Extract Label"),"Create/Derive");
	m_Logic->Plug(new lhpOpMultiscaleExplore("Multiscale Viewer"),"Manage");
	m_Logic->Plug(new medOpMML("MML Register from template"),"Modify");
	m_Logic->Plug(new medOpMML3("MML3 Register from template"),"Modify");
	// m_Logic->Plug(new lhpOpKeyczarIntegrationTest("Security Libraries Integration"),"Test");
	m_Logic->Plug(new lhpOpComputeTensor("Compute Tensors"), "Modify");
	m_Logic->Plug(new lhpOpComputeGradientStructureTensor("Compute Gradient Structure Tensor"), "Modify");

	m_Logic->Plug(new lhpOpTextureOrientation("Texture Orientation"),"Create/Derive");

	// New Upload Download VME (Refactor to be tested...)
	// lhpGUISettingsOpUploadMultiVMERefactor *updownSettings=new lhpGUISettingsOpUploadMultiVMERefactor(NULL,"Upload Multi VME");
	// m_Logic->Plug(new lhpOpUploadMultiVMERefactor("Upload Multi VME"),"Manage",true, updownSettings);
	m_Logic->Plug(new lhpOpUploadMultiVMERefactor("Upload Multi VME"),"Manage");
	m_Logic->Plug(new lhpOpEditTagRefactor("Edit Tag VME"),"Manage");
	m_Logic->Plug(new lhpOpDownloadVMERefactor("Download VME From Basket", lhpOpDownloadVMERefactor::FROM_BASKET),"Manage");
	m_Logic->Plug(new lhpOpDownloadVMERefactor("Download VME From Sandbox", lhpOpDownloadVMERefactor::FROM_SANDBOX),"Manage");

	m_Logic->Plug(new lhpOpOpenRepository("Open repository..."),"Manage");
	m_Logic->Plug(new lhpOp3DVolumeBrush("Volume Brush"),"Create/Derive");

	m_Logic->Plug(new mafOpFilterSurface(_("Filter Surface")),_("Modify/Surface Editing"));
	m_Logic->Plug(new mafOpRemoveCells(_("Remove Cells")),_("Modify/Surface Editing"));
	m_Logic->Plug(new medOpSmoothSurfaceCells(_("Smooth Cells")),_("Modify/Surface Editing"));
	m_Logic->Plug(new medOpFillHoles(_("Fill Holes")),_("Modify/Surface Editing"));
	m_Logic->Plug(new medOpInteractiveClipSurface(_("Clip")),_("Modify/Surface Editing"));
	m_Logic->Plug(new medOpSplitSurface(_("Split")),_("Modify/Surface Editing"));
	m_Logic->Plug(new medOpExtrusionHoles(_("Extrude Edges")),_("Modify/Surface Editing"));
	m_Logic->Plug(new lhpOpShowHistogram(_("Show Histogram")));

	//m_Logic->Plug(new lhpOp3DVolumeBrush("Volume Brush"),"Modify");
	// m_Logic->Plug(new lhpOpUploadVME("Upload VME"),"Manage");

	//// Old Upload Download VME (Production)
	//m_Logic->Plug(new lhpOpUploadMultiVME("Upload VME"),"Manage");
	//m_Logic->Plug(new lhpOpEditTag("Edit Tag VME"),"Manage");
	//m_Logic->Plug(new lhpOpDownloadVME("Download VME"),"Manage");
	//// m_Logic->Plug(new lhpOpUploadVME("Upload VME"),"Manage");

#if defined(VPHOP_WP10)
	//these are operations added by VPHOP WP10
	m_Logic->Plug(new lhpOpRemoveTracability("Remove Operation History"),"VPHOP WP10 - TESTING/MSF Tools [WP10]");
	m_Logic->Plug(new lhpOpMultiReparentTo("Reparent to (Multi)...  \tCtrl+Shift+R"),"VPHOP WP10 - TESTING/MSF Tools [WP10]");
	m_Logic->Plug(new lhpOpMoveVMEUp("Move VME Up\tCtrl+Shift+U"),"VPHOP WP10 - TESTING/MSF Tools [WP10]");
	m_Logic->Plug(new lhpOpMoveVMEDown("Move VME Down\tCtrl+Shift+D"),"VPHOP WP10 - TESTING/MSF Tools [WP10]");
	m_Logic->Plug(new lhpOpEditTagRefactor("Edit Tag VME ...\tCtrl+E"),"VPHOP WP10 - TESTING/MSF Tools [WP10]");
	m_Logic->Plug(new lhpOpSetLowerRes("Set Lower Resolution\tCtrl+Alt+L"), "VPHOP WP10 - TESTING/MSF Tools [WP10]");
	m_Logic->Plug(new lhpOpMSModelCheckTags(), "VPHOP WP10 - TESTING/MSF Tools [WP10]");

	m_Logic->Plug(new medOpCreateMuscleWrapper("Create Muscle Wrapper\tCtrl+W"),"VPHOP WP10 - TESTING/Create [WP10]");
	m_Logic->Plug(new medOpCreateJoint("Create Joint\tCtrl+J"),"VPHOP WP10 - TESTING/Create [WP10]");
	m_Logic->Plug(new medOpCreateParticle("Create Particle\tCtrl+P"), "VPHOP WP10 - TESTING/Create [WP10]");
	m_Logic->Plug(new lhpOpMergeSurfaces("Merge Surface VMEs\tCtrl+Shift+M"),"VPHOP WP10 - TESTING/Create [WP10]");
	m_Logic->Plug(new lhpOpComputeProgressiveHull("Compute Hull\tCtrl+Shift+H"), "VPHOP WP10 - TESTING/Create [WP10]");
	m_Logic->Plug(new lhpOpMuscleDecompose("Decompose Muscle\tCtrl+Shift+Alt+D"), "VPHOP WP10 - TESTING/Create [WP10]");
	m_Logic->Plug(new lhpOpExtractLandmarkEOS("EOS Lanmark Extraction\tCtrl+Shift+E"),"VPHOP WP10 - TESTING/Create [WP10]"); //Youbing
	m_Logic->Plug(new lhpOpConvertMotionData("Convert Motion Data\tCtrl+Shift+C"),_("VPHOP WP10 - TESTING/Create [WP10]"));

	m_Logic->Plug(new lhpOpSmoothMuscle("Smooth Muscle\tCtrl+Shift+L"),"VPHOP WP10 - TESTING/Filtering [WP10]");
	m_Logic->Plug(new mafOpFilterSurface(_("Filter Surface\tCtrl+Alt+F")),_("VPHOP WP10 - TESTING/Filtering [WP10]"));
	m_Logic->Plug(new mafOpRemoveCells(_("Remove Cells")),_("VPHOP WP10 - TESTING/Filtering [WP10]"));
	m_Logic->Plug(new medOpSmoothSurfaceCells(_("Smooth Cells")),_("VPHOP WP10 - TESTING/Filtering [WP10]"));
	m_Logic->Plug(new medOpFillHoles(_("Fill Holes")),_("VPHOP WP10 - TESTING/Filtering [WP10]"));
	m_Logic->Plug(new mafOpDecimateSurface(_("Decimate")),_("VPHOP WP10 - TESTING/Filtering [WP10]"));
	m_Logic->Plug(new medOpCleanSurface(_("Clean")),_("VPHOP WP10 - TESTING/Filtering [WP10]"));
	m_Logic->Plug(new mafOpConnectivitySurface(_("Connectivity")),_("VPHOP WP10 - TESTING/Filtering [WP10]"));
	m_Logic->Plug(new lhpOpFilterPolyDataUsingExtApp(),_("VPHOP WP10 - TESTING/Filtering [WP10]"));
	m_Logic->Plug(new medOpMeshQuality("Check Surface"),_("VPHOP WP10 - TESTING/Filtering [WP10]"));

	m_Logic->Plug(new lhpOpScaleMMA("Atlas Scaling\tCtrl+Shift+D"),"VPHOP WP10 - TESTING"); //Youbing
	m_Logic->Plug(new lhpOpFuseMotionData("Motion Fusion\tCtrl+Shift+F"),"VPHOP WP10 - TESTING");
	m_Logic->Plug(new lhpOpASFMotionRetargeting("ASF Motion Retargeting"), _("VPHOP WP10 - TESTING"));
	m_Logic->Plug(new lhpOpMotionRetargeting("Motion Retargeting\tCtrl+Alt+M"),_("VPHOP WP10 - TESTING"));
#endif

	//-------------------------------------------------------------

	//------------------------- Views -------------------------
	//View Arbitrary Slice
	mafViewArbitrarySlice *ArbitraryView = new mafViewArbitrarySlice("Arbitrary");
	ArbitraryView->PackageView();
	m_Logic->Plug(ArbitraryView);

	//View ArbitraryOrthoSlice
	medViewArbitraryOrthoSlice *ArbitraryOSView = new medViewArbitraryOrthoSlice("EARLY PROTOTYPE Arbitrary Orthoslice");
	ArbitraryOSView->PackageView();
	m_Logic->Plug(ArbitraryOSView);

	// View DRR
	medViewVTKCompound *vdrr = new medViewVTKCompound("DRR");
	mafViewVTK *drrVTK = new mafViewVTK("DRR");
	drrVTK->PlugVisualPipe("mafVMEVolumeGray","medPipeVolumeDRR",MUTEX);
	drrVTK->PlugVisualPipe("mafVMEVolumeLarge","medPipeVolumeDRR",MUTEX);
	vdrr->SetExternalView(drrVTK);
	vdrr->PackageView();
	m_Logic->Plug(vdrr);

	// View Analog graph
	mafViewVTK *vtkgraph = new mafViewVTK("Analog Graph", CAMERA_PERSPECTIVE, false);
	vtkgraph->PlugVisualPipe("medVMEAnalog", "medPipeGraph",MUTEX);
	medViewVTKCompound *graph = new medViewVTKCompound("Analog Graph");
	graph->SetExternalView(vtkgraph);
	graph->PackageView();
	m_Logic->Plug(graph);

	//View Global Slice
	mafViewGlobalSliceCompound *GlobalSlice = new mafViewGlobalSliceCompound("Global Slice");
	GlobalSlice->PackageView();
	m_Logic->Plug(GlobalSlice);

	//View Isosurface
	medViewVTKCompound *viso = new medViewVTKCompound("Isosurface");
	mafViewVTK *isoVTK = new mafViewVTK("Isosurface");
	isoVTK->PlugVisualPipe("mafVMEVolumeGray", "mafPipeIsosurface",MUTEX);
	isoVTK->PlugVisualPipe("medVMELabeledVolume", "mafPipeIsosurface",MUTEX);
	isoVTK->PlugVisualPipe("mafVMEVolumeLarge","mafPipeIsosurface",MUTEX);
	viso->SetExternalView(isoVTK);
	viso->PackageView();
	m_Logic->Plug(viso);

	//View Isosurface (GPU)
	medViewVTKCompound *visoGPU = new medViewVTKCompound("Isosurface (GPU)");
	mafViewVTK *vtkGPU = new mafViewVTK("Isosurface (GPU)");
	vtkGPU->PlugVisualPipe("mafVMEVolumeGray", "mafPipeIsosurfaceGPU",MUTEX);   //BES: 13.11.2008 - GPU support, mafPipeIsosurfaceGPU to be merged with mafPipeIsosurface in future
	vtkGPU->PlugVisualPipe("medVMELabeledVolume", "mafPipeIsosurfaceGPU",MUTEX);
	vtkGPU->PlugVisualPipe("mafVMEVolumeLarge", "mafPipeIsosurfaceGPU",MUTEX);
	visoGPU->SetExternalView(vtkGPU);
	visoGPU->PackageView();
	m_Logic->Plug(visoGPU);

	//View Orthoslice
	mafViewOrthoSlice *viewOrthoSlice = new mafViewOrthoSlice("OrthoSlice");
	viewOrthoSlice->PackageView();
	m_Logic->Plug(viewOrthoSlice);

	//View RXCT
	mafViewRXCT *vrxctl = new mafViewRXCT("RXCT");
	vrxctl->PackageView();
	m_Logic->Plug(vrxctl);

	//View Surface
	medViewVTKCompound *vsurface = new medViewVTKCompound("Surface");
	mafViewVTK *surfaceVTK = new mafViewVTK("Surface");
	surfaceVTK->PlugVisualPipe("mafVMESurface","mafPipeSurface");
	surfaceVTK->PlugVisualPipe("mafVMELandmark", "medPipeTrajectories");
	vsurface->SetExternalView(surfaceVTK);
	vsurface->PackageView();
	m_Logic->Plug(vsurface);

	//View Biomechanical graph
	mafViewIntGraph *vgraph = new mafViewIntGraph("Biomechanical graph");
	m_Logic->Plug(vgraph);

	//View Slicer
	medViewSlicer *slicerView = new medViewSlicer("Slicer");
	slicerView->PackageView();
	m_Logic->Plug(slicerView);

	medViewSliceNotInterpolatedCompound *sliceNIView = new medViewSliceNotInterpolatedCompound("Slice not interpolated (test)");
	sliceNIView->PackageView();
	m_Logic->Plug(sliceNIView);

	//View Compound Volume
	medViewVTKCompound *vvectors = new medViewVTKCompound("Compound Pipe Visualization");
	mafViewVTK *vectorsVTK = new mafViewVTK("Compound Volume");
	vectorsVTK->PlugVisualPipe("mafVMEVolumeGray", "medPipeCompoundVolume", VISIBLE);
	vvectors->SetExternalView(vectorsVTK);
	vvectors->PackageView();
	m_Logic->Plug(vvectors);

	//View Analog graph X,Y
	lhpViewAnalogGraphXY *graphView = new lhpViewAnalogGraphXY("Analog Graph (X,Y)", CAMERA_PERSPECTIVE, false);
	graphView->PlugVisualPipe("medVMEAnalog", "medPipeGraph",NON_VISIBLE);
	graphView->PlugVisualPipe("lhpVMEScalarMatrix", "lhpPipeAnalogGraphXY",VISIBLE);
	m_Logic->Plug(graphView);

	// Map vector/scalar fields
	medViewVTKCompound *vfmap = new medViewVTKCompound("Vector/Scalar Fields Arrows Map");
	mafViewVTK *vfmapVTK = new mafViewVTK("Vector/scalar fields mapping");
	vfmapVTK->PlugVisualPipe("mafVMESurface","medPipeVectorFieldMapWithArrows");
	vfmapVTK->PlugVisualPipe("mafVMEMesh", "medPipeVectorFieldMapWithArrows");
	vfmap->SetExternalView(vfmapVTK);
	vfmap->PackageView();
	m_Logic->Plug(vfmap);

	//temporary for testing
	//mafViewSingleSliceCompound *sliceView = new mafViewSingleSliceCompound("Test Slice");
	//sliceView->PackageView();
	//m_Logic->Plug(sliceView);

	medViewSliceBlendRX *vblendrx = new medViewSliceBlendRX();
	vblendrx->PackageView();
	m_Logic->Plug(vblendrx);

	//-------------------------------------------------------------

	wxBitmap splashBitmap;
	splashBitmap.LoadFile("../Splash/SPLASH_SCREEN.bmp", wxBITMAP_TYPE_BMP);
	m_Logic->ShowSplashScreen(splashBitmap);

	// show the application
	m_Logic->ShowSplashScreen(splashBitmap);
	m_Logic->Show();
	m_Logic->GetCredentials();

#if defined(_MSC_VER)
	m_Logic->Init(__argc, __argv); // calls FileNew - which create the root
#else
	m_Logic->Init(__argc, __argv); // calls FileNew - which create the root
#endif

	return TRUE;
}
//--------------------------------------------------------------------------------
int lhpBuilderApp::OnExit()
	//--------------------------------------------------------------------------------
{
	cppDEL(m_Logic);

	//this hack is fixing VTK internal memory leak
	vtkTimerLog::CleanupLog();
	return 0;
}

//void lhpBuilderApp::OnIdle( wxIdleEvent &event )
//{
//  vtkDataArrayMemMng::GetDataArrayMemMng()->UnlockAllMemory(0);
//  event.Skip();
//}