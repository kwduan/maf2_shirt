/*=========================================================================
  Program:   Multimod Application Framework
  Module:    $RCSfile: BuilderM2O.h,v $
  Language:  C++
  Date:      $Date: 2014-04-16 11:07 $
  Version:   $ $
  Authors:   Kewei Duan
==========================================================================*/

#ifndef __BuilderM2OApp_H__
#define __BuilderM2OApp_H__
//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "BuilderM2OLogic.h"
#include "mafEvent.h"
#include "mafObserver.h"

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class lhpUser;

class BuilderM2OApp : public wxApp
{
public:
  bool OnInit();
  int  OnExit();


  ////Called when the application is in the idle state
  //virtual void OnIdle(wxIdleEvent& event);  
  //DECLARE_EVENT_TABLE()

protected:
  BuilderM2OLogic *m_Logic;

};
DECLARE_APP(BuilderM2OApp)

//BES: 28.5.2009 - cannot link under VS 2008 because of some missing link libs
#if _MSC_VER >= 1500
#pragma comment( lib, "ofstd.lib" ) 
#pragma comment( lib, "netapi32.lib" ) 
#endif
#endif 