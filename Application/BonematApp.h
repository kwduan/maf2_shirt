/*=========================================================================
  Program:   Multimod Application Framework
  Module:    $RCSfile: BonematApp.h,v $
  Language:  C++
  Date:      $Date: 2010-07-06 07:51:19 $
  Version:   $Revision: 1.1.1.1.2.1 $
  Authors:   Roberto Mucci, Simone Brazzale
==========================================================================
  Copyright (c) 2001/2005 
  CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/
#ifndef __BonematApp_H__
#define __BonematApp_H__
//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "lhpBuilderLogic.h" 
#include "mafEvent.h"
#include "mafObserver.h"

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class lhpUser;

class BonematApp : public wxApp
{
public:
  bool OnInit();
  int  OnExit();


  ////Called when the application is in the idle state
  //virtual void OnIdle(wxIdleEvent& event);  
  //DECLARE_EVENT_TABLE()

protected:
  lhpBuilderLogic *m_Logic;

};
DECLARE_APP(BonematApp)

//BES: 28.5.2009 - cannot link under VS 2008 because of some missing link libs
#if _MSC_VER >= 1500
#pragma comment( lib, "ofstd.lib" ) 
#pragma comment( lib, "netapi32.lib" ) 
#endif
#endif 
