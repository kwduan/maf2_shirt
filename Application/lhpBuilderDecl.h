/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: lhpBuilderDecl.h,v $
Language:  C++
Date:      $Date: 2011-09-05 14:12:39 $
Version:   $Revision: 1.1.1.1.2.2 $
Authors:   Daniele Giunchi , Stefano Perticoni
==========================================================================
Copyright (c) 2001/2005 
CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/

#ifndef __lhpBuilderDecl_H__
#define __lhpBuilderDecl_H__

#include "lhpDefines.h"
#include "medDecl.h"

enum LHP_MAIN_EVENT_ID
{
	ID_MSF_DATA_CACHE = MED_EVT_USER_START,
  ID_REQUEST_USER,
  ID_REQUEST_APPLICATION_NAME,
  ID_REQUEST_PYTHON_EXE_INTERPRETER,
  ID_REQUEST_PYTHONW_EXE_INTERPRETER,
  LHP_EVT_USER_START,
  ID_REQUEST_OPENSIM_DIR,
  ID_REQUEST_OPENSIM_VERSION,
};

#endif