/*=========================================================================
  Program:   Multimod Application Framework
  Module:    $RCSfile: nmsBuilderApp.h,v $
  Language:  C++
  Date:      $Date: 2011-02-07 15:12:13 $
  Version:   $Revision: 1.1.2.2 $
  Authors:   Simone Brazzale
==========================================================================
  Copyright (c) 2001/2005 
  CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/
#ifndef __nmsBuilderApp_H__
#define __nmsBuilderApp_H__
//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "nmsBuilderLogic.h" 
#include "mafEvent.h"
#include "mafObserver.h"

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class lhpUser;

class nmsBuilderApp : public wxApp
{
public:
  bool OnInit();
  int  OnExit();


  //Called when the application is in the idle state
  //virtual void OnIdle(wxIdleEvent& event);  
  //DECLARE_EVENT_TABLE()

protected:
  nmsBuilderLogic *m_Logic;

};
DECLARE_APP(nmsBuilderApp)

//BES: 28.5.2009 - cannot link under VS 2008 because of some missing link libs
#if _MSC_VER >= 1500
#pragma comment( lib, "ofstd.lib" ) 
#pragma comment( lib, "netapi32.lib" ) 
#endif
#endif 
