/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: nmsBuilderLogic.h,v $
Language:  C++
Date:      $Date: 2011-02-07 15:12:14 $
Version:   $Revision: 1.1.2.1 $
Authors:   Simone Brazzale
==========================================================================
Copyright (c) 2002/2004
CINECA - Interuniversity Consortium (www.cineca.it) 
=========================================================================*/
#ifndef __nmsBuilderLogic_H__
#define __nmsBuilderLogic_H__

//----------------------------------------------------------------------------
// includes :
//----------------------------------------------------------------------------
#include "medLogicWithManagers.h"

//----------------------------------------------------------------------------
// forward reference
//----------------------------------------------------------------------------
class lhpGUIPythonSettings;
class lhpGUIOpenSimSettings;

//----------------------------------------------------------------------------
// nmsBuilderLogic :
//----------------------------------------------------------------------------
/**
*/
class nmsBuilderLogic: public medLogicWithManagers
{
public:
	nmsBuilderLogic();
	virtual     ~nmsBuilderLogic(); 

	virtual void OnEvent(mafEventBase *maf_event);

	/** Validate the current MSF tree. Return the following enums from mafOpValidateTree

	mafOpValidateTree::VALIDATE_ERROR = 0, (also alert the user with an Error msgbox)
	mafOpValidateTree::VALIDATE_SUCCESS,
	mafOpValidateTree::VALIDATE_WARNING, (also alert the user with a Warning msgbox)

	*/
	int ValidateMSFTree();

	/** Configure the application.
	At this point are plugged all the managers, the side-bar docking panel. 
	Are plugged also all the setting to the dialogs interface. */
	virtual void Configure();

protected:
	/** Respond to a VME_ADDED evt. propagate evt. to SideBar,ViewManager,ecc.. */
	virtual void VmeAdded(mafNode *vme);


  lhpGUIPythonSettings *m_PythonSettings;
  lhpGUIOpenSimSettings *m_OpenSimSettings;
};
#endif
