/*=========================================================================
  Program:   Multimod Application Framework
  Module:    $RCSfile: BonematApp.cpp,v $
  Language:  C++
  Date:      $Date: 2011-05-27 07:52:02 $
  Version:   $Revision: 1.1.1.1.2.5 $
  Authors:   Roberto Mucci, Simone Brazzale
==========================================================================
  Copyright (c) 2001/2005 
  CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/

#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------
#include "BonematApp.h"

#include <wx/datetime.h>
#include <wx/config.h>

#include "mafDecl.h"
#include "mafVMEFactory.h"
#include "mafPics.h"
#include "mafGUIMDIFrame.h"
#include "mafInteractionFactory.h"

#include "mafNodeFactory.h" 
#include "mafNodeGeneric.h"
#include "mafNodeRoot.h"
#include "mafVMERoot.h"
#include "mafVMESurface.h"
#include "mafVMELandmark.h" 
#include "mafPipeFactoryVME.h"
#include "medPipeFactoryVME.h"
#include "medVMEFactory.h"
#include "mafVMEAFRefSys.h" 
#include "lhpVMEPoseGroup.h"
#include "mafVMEHelAxis.h" 
#include "medOpImporterDicomOffis.h"
//BES: 23.6.2008 - Large Volume - to be merged 
#include "mafOpImporterRAWVolume_BES.h"
#include "medOpImporterRAWImages.h"
#include "mafVMERawMotionData.h" 
//#include "lhpOpImporterC3D.h"
#include "mafOpImporterMesh.h"
#include "mafOpExporterMesh.h"
#include "lhpOpBonemat.h"
#include "lhpOpImporterAnsysInputFile.h"
#include "lhpOpExporterAnsysInputFile.h"
#include "lhpOpImporterAnsysCDBFile.h"
#include "lhpOpExporterMeshFeBio.h"
#include "mafViewRXCT.h"
#include "mafViewOrthoSlice.h"
#include "mafViewArbitrarySlice.h"
#include "medViewVTKCompound.h"
#include "lhpViewAnalogGraphXY.h"
#include "medVMEComputeWrapping.h"
#include "medPipeComputeWrapping.h"
#include "medGUIDicomSettings.h"
#ifdef MAF_USE_ITK
#include "lhpVMESurfaceScalarVarying.h"
#endif
#include "lhpVisualPipeSurfaceScalar.h"
#include "lhpVMEScalarMatrix.h"
#include "lhpOpBonematBatch.h"
#include <vtkTimerLog.h>



//--------------------------------------------------------------------------------
// Create the Application
//--------------------------------------------------------------------------------
IMPLEMENT_APP(BonematApp)


//--------------------------------------------------------------------------------
bool BonematApp::OnInit()
//--------------------------------------------------------------------------------
{
  ////BES: 14.5.2008 - initialize the manager (with default settings)
  ////It provides a sophisticated memory-disk swap algorithm that
  ////allows handling of many large size memory blocks that would
  ////take more memory than available in total.
  ////The mechanism is NOT GUARANTEED TO BE SAFE
  ////Swapping (especially, if HandleNewFailure option is enabled)
  ////may lead to artifacts in data or even 
  //vtkDataArrayMemMng::InitializeManagerUnSafeMode();  

  mafPictureFactory::GetPictureFactory()->Initialize();
 
  #include "pic/Bonemat/FRAME_ICON16x16.xpm"
  mafADDPIC(FRAME_ICON16x16);

  #include "pic/Bonemat/FRAME_ICON32x32.xpm"
  mafADDPIC(FRAME_ICON32x32);

  #include "pic/Bonemat/MDICHILD_ICON.xpm"
  mafADDPIC(MDICHILD_ICON);

  int result;
 
  result = medVMEFactory::Initialize();
  assert(result == MAF_OK);

  result = medPipeFactoryVME::Initialize();
  assert(result==MAF_OK);

  result = mafInteractionFactory::Initialize();
  assert(result==MAF_OK);

  
  mafPlugNode<mafVMERawMotionData>("VME representing raw motion data");
  mafPlugNode<mafVMEAFRefSys>("VME representing anatomical frame");
  mafPlugNode<lhpVMEPoseGroup>("Pose group for time-varying vme that not inherit for mafVMEGEneric");
  mafPlugNode<mafVMEHelAxis>("VME representing helical axis");
  mafPlugNode<mafVMEVolumeLarge>("VME storing large volume datasets with one scalar component");
  mafPlugNode<lhpVMEScalarMatrix>("VME representing analog timevariyng data");
  mafPlugNode<medVMEComputeWrapping>("Generalized another VME Meter with wrapping geometry");
  mafPlugPipe<medPipeComputeWrapping>("Pipe to Visualize Compute Wrapping Meter");
  mafPlugPipe<lhpPipeAnalogGraphXY>("vph2PipeAnalogGraph");

#ifdef MAF_USE_ITK
  mafPlugNode<lhpVMESurfaceScalarVarying>("VME representing surface with attached time varying mafVMEScalar");
#endif

  
  mafPlugPipe<lhpVisualPipeSurfaceScalar>("Visual pipe to render a surface with its scalar values");


  m_Logic = new lhpBuilderLogic();
  m_Logic->GetTopWin()->SetTitle("Bonemat");
  m_Logic->Configure();
  SetTopWindow(mafGetFrame());  

	wxRegKey RegKey(wxString("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Bonemat"));
	if(RegKey.Exists())
	{
		RegKey.Create();
		wxString revision;
		RegKey.QueryValue(wxString("DisplayVersion"), revision);
		m_Logic->SetRevision(revision);
	}
	else
	{
		wxString revision="0.1";
		m_Logic->SetRevision(revision);
	}

  //------------------------- Importers -------------------------
  medGUIDicomSettings *dicomSettings=new medGUIDicomSettings(NULL,"DICOM");
  m_Logic->Plug(new medOpImporterDicomOffis("DICOM"),"Images",true,dicomSettings);

  m_Logic->Plug(new mafOpImporterRAWVolume_BES("Raw Volume"),"Images");
  m_Logic->Plug(new medOpImporterRAWImages("Raw Images"),"Images");
  m_Logic->Plug(new mafOpImporterMesh("Generic Mesh"), "Finite Element");
  m_Logic->Plug(new lhpOpImporterAnsysInputFile("Ansys Input File"), "Finite Element");	
  m_Logic->Plug(new lhpOpImporterAnsysCDBFile("Ansys CDB File"), "Finite Element");
  
  //-------------------------------------------------------------

  //------------------------- Exporters -------------------------
  m_Logic->Plug(new mafOpExporterMesh("Generic Mesh"), "Finite Element");
  m_Logic->Plug(new lhpOpExporterAnsysInputFile("Ansys Input File"),"Finite Element");
  //-------------------------------------------------------------

  //------------------------- Operations -------------------------
  m_Logic->Plug(new lhpOpBonemat("Bonemat"));
	m_Logic->Plug(new lhpOpBonematBatch("Bonemat Batch"),"Modify");
  //-------------------------------------------------------------

  //------------------------- Views -------------------------
  //View Arbitrary Slice
  mafViewArbitrarySlice *ArbitraryView = new mafViewArbitrarySlice("Arbitrary");
  ArbitraryView->PackageView();
  m_Logic->Plug(ArbitraryView);

  //View Orthoslice
  mafViewOrthoSlice *viewOrthoSlice = new mafViewOrthoSlice("OrthoSlice");
  viewOrthoSlice->PackageView();
  m_Logic->Plug(viewOrthoSlice);

  //View RXCT
  mafViewRXCT *vrxctl = new mafViewRXCT("RXCT");
  vrxctl->PackageView();
  m_Logic->Plug(vrxctl);
  
  //View Surface
  medViewVTKCompound *vsurface = new medViewVTKCompound("Surface");
  mafViewVTK *surfaceVTK = new mafViewVTK("Surface");
  surfaceVTK->PlugVisualPipe("mafVMESurface","mafPipeSurface");
  surfaceVTK->PlugVisualPipe("mafVMELandmark", "medPipeTrajectories");
  vsurface->SetExternalView(surfaceVTK);
  vsurface->PackageView();
	m_Logic->Plug(vsurface);
  //-------------------------------------------------------------

  wxBitmap splashBitmap;
  splashBitmap.LoadFile("../Splash/SPLASH_SCREEN.bmp", wxBITMAP_TYPE_BMP);
  //m_Logic->ShowSplashScreen(splashBitmap); 

  // show the application
	m_Logic->ShowSplashScreen(splashBitmap);
  m_Logic->Show();

  m_Logic->Init(argc,argv); // calls FileNew - which create the root

  return TRUE;
}
//--------------------------------------------------------------------------------
int BonematApp::OnExit()
//--------------------------------------------------------------------------------
{
  cppDEL(m_Logic);

  //this hack is fixing VTK internal memory leak
  vtkTimerLog::CleanupLog();
  return 0;
}

//void BonematApp::OnIdle( wxIdleEvent &event )
//{ 
//  vtkDataArrayMemMng::GetDataArrayMemMng()->UnlockAllMemory(0);
//  event.Skip();
//}
