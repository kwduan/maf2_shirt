/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: nmsBuilderLogic.cpp,v $
Language:  C++
Date:      $Date: 2011-02-07 15:12:14 $
Version:   $Revision: 1.1.2.1 $
Authors:   Simone Brazzale
==========================================================================
Copyright (c) 2002/2004
CINECA - Interuniversity Consortium (www.cineca.it) 
=========================================================================*/


#include "mafDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "nmsBuilderLogic.h"
#include "lhpBuilderDecl.h"
#include "mafDecl.h"
#include "mafTagArray.h"
#include "mafOp.h"
#include "mafOpManager.h"
#include "mafOpValidateTree.h"
#include "mafGUISettingsDialog.h"
#include "lhpGUIPythonSettings.h"
#include "lhpUtils.h"

#include "psLoaderGUIContextualMenu.h"
#include "mafViewManager.h"
#include "mafGUIMDIChild.h"
#include "mafGUIMDIFrame.h"
#include "wx\busyinfo.h"
#include "lhpGUIOpenSimSettings.h"

//----------------------------------------------------------------------------
nmsBuilderLogic::nmsBuilderLogic()
//----------------------------------------------------------------------------
{
	m_PythonSettings = new lhpGUIPythonSettings(this);
	m_OpenSimSettings = new lhpGUIOpenSimSettings(this);

}
//----------------------------------------------------------------------------
nmsBuilderLogic::~nmsBuilderLogic()
//----------------------------------------------------------------------------
{
	cppDEL(m_PythonSettings);
	cppDEL(m_OpenSimSettings);
}
//----------------------------------------------------------------------------
void nmsBuilderLogic::OnEvent(mafEventBase *maf_event)
//----------------------------------------------------------------------------
{
	if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
	{
		switch(e->GetId())
		{
		case ABOUT_APPLICATION:
			{
				wxString message = m_AppTitle.GetCStr();
				message += _(" Application ");
				message += m_Revision;
				wxMessageBox(message, "About Application");
				mafLogMessage(wxString::Format("%s",m_Revision.GetCStr()));
			}
			break;
		case ID_REQUEST_APPLICATION_NAME:
			{
				e->SetString(&m_AppTitle);
			}
			break;

		case ID_MSF_DATA_CACHE:
			{
				//comunicate to operation msf directory
				e->SetString(&m_VMEManager->GetFileName());
			}
			break;
    
		case ID_REQUEST_PYTHON_EXE_INTERPRETER:
			{
				if(m_PythonSettings->GetPythonExe())
				{
					e->SetString(&m_PythonSettings->GetPythonExe());
				}
			}
			break;

		case ID_REQUEST_PYTHONW_EXE_INTERPRETER:
			{
				if(m_PythonSettings->GetPythonwExe())
				{
					//(Remember that this logic is the same for all applications)
					// Fix bug #2258
					// Try to avoid load wrong dll
					// Here call a scripts that generate absolute paths for external (python) dll libraries starting from interpreter path
					// so script that requires external libraries can load the right dll version
					// (for safety if the specified path do not exists the script will try to load the system dll without rising blocking errors)
					mafString pythonw_interpreter = m_PythonSettings->GetPythonwExe();
					mafString script_dir = wxString(lhpUtils::lhpGetApplicationDirectory() + "\\VMEUploaderDownloaderRefactor\\").c_str();
					wxString old_dir = wxGetCwd();
					wxSetWorkingDirectory(script_dir.GetCStr());

					mafLogMessage( _T("Now current working directory is: '%s' "), wxGetCwd().c_str() );
					wxString command2execute;
					command2execute.Clear();
					command2execute = pythonw_interpreter.GetCStr();

					command2execute.Append(" GenerateEnvironmentDLLAbsPaths.py ");
					command2execute.Append(pythonw_interpreter.GetCStr());

					mafLogMessage("");
					mafLogMessage( _T("Executing command: '%s'"), command2execute.c_str() );
					mafLogMessage("");

					long pid = -1;
					if (pid = wxExecute(command2execute, wxEXEC_SYNC) != 0)
					{
						mafLogMessage(_T("SYNC Command process '%s' terminated with exit code %d."),command2execute.c_str(), pid);
					}
					wxSetWorkingDirectory(old_dir);
					mafLogMessage( _T("Current working directory is: '%s' "), wxGetCwd().c_str());
					//

					e->SetString(&m_PythonSettings->GetPythonwExe());
				}
			}
			break;

		case ID_REQUEST_OPENSIM_DIR:
			{
				if(m_OpenSimSettings->GetOpenSimDir())
				{
					e->SetString(&m_OpenSimSettings->GetOpenSimDir());
				}
			}
			break;

		case ID_REQUEST_OPENSIM_VERSION:
			{
				e->SetString(&m_OpenSimSettings->GetOpenSimVersionString());	
			}
			break;

		case MENU_FILE_SAVE:
			{
				OnFileSave(); 

				ValidateMSFTree();
			}
		break;

		case MENU_FILE_SAVEAS:
			{
				OnFileSaveAs(); 

				ValidateMSFTree();
			}
			break;


		default:
			mafLogicWithManagers::OnEvent(maf_event);
			break; 
		} // end switch case
	} // end if SafeDowncast
}
//----------------------------------------------------------------------------
void nmsBuilderLogic::VmeAdded(mafNode *vme)
//----------------------------------------------------------------------------
{
	mafLogicWithManagers::VmeAdded(vme);
	// check for the presence of the LHDL attribute

	mafTagArray *lhdlArray = mafTagArray::SafeDownCast(vme->GetAttribute("LHDL"));
	if (lhdlArray == NULL)
	{
		lhdlArray = mafTagArray::New();
		lhdlArray->SetName("LHDL");
		vme->SetAttribute("LHDL",lhdlArray);
	}
}
//----------------------------------------------------------------------------
void nmsBuilderLogic::Configure()
//----------------------------------------------------------------------------
{
	mafLogicWithManagers::Configure(); // create the GUI - and calls CreateMenu
	if (m_PythonSettings)
	{
		m_SettingsDialog->AddPage(m_PythonSettings->GetGui(), m_PythonSettings->GetLabel());
	}

	if (m_OpenSimSettings)
	{
		m_SettingsDialog->AddPage(m_OpenSimSettings->GetGui(), m_OpenSimSettings->GetLabel());
	}
}

int nmsBuilderLogic::ValidateMSFTree()
{
	mafOpValidateTree *validateTree = new mafOpValidateTree();
	validateTree->TestModeOn();
	validateTree->SetInput(m_VMEManager->GetRoot());
	int result = validateTree->ValidateTree();

	wxBusyInfo info("Validating MSF Tree... Please wait");

	std::ostringstream stringStream;
	stringStream << "Running " << validateTree->GetTypeName() << " ..." << std::endl;          

	if (mafOpValidateTree::VALIDATE_ERROR == result)
	{
		stringStream << validateTree->GetTypeName() << " returned VALIDATE_ERROR" << std::endl;          
		wxMessageBox( stringStream.str().c_str(), _("Error"), wxOK | wxICON_ERROR  ); 
	} 
	else if (mafOpValidateTree::VALIDATE_SUCCESS == result)
	{
		stringStream << validateTree->GetTypeName() << " returned VALIDATE_SUCCESS" << std::endl;         
	}
	else if (mafOpValidateTree::VALIDATE_WARNING == result)
	{
		stringStream << validateTree->GetTypeName() << " returned VALIDATE_WARNING" << std::endl;          
		wxMessageBox( stringStream.str().c_str(), _("Warning"), wxOK | wxICON_WARNING  ); 
	}

	mafLogMessage(stringStream.str().c_str());

	mafDEL(validateTree);

	return result;
}
