﻿/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: BonematCallee.cpp,v $
Language:  C++
Date:      $Date: 2009-05-19 14:29:56 $
Version:   $Revision: 1.1 $
Authors:   Gianluigi Crimi
==========================================================================
Copyright (c) 2002/2004 
CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/

#include "mafDefines.h" 
#include "lhpDefines.h"


//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "BonematCallee.h"

#include "lhpOpBonemat.h"
#include "medOpImporterVTK.h"
#include "mafOpExporterVTK.h"
#include "mafVMEMesh.h"
#include "mafVMEVolumeGray.h"

int main( int argc, char* argv[] )
{
  printf("--------------------------------------------------\n");
  printf("                       BONEMAT                     \n");
  printf("--------------------------------------------------\n");

  if(argc != 6)
  {
    printf("usage: \n");
    printf("BonematCallee [Volume].vtk [Mesh].vtk [Configuration].xml [Frequency].txt [Output].vtk\n");

    return MAF_ERROR;
  }

  // SET PARAMETERS
  wxString m_inVTKVolumeFileName = argv[1];
  wxString m_inVMEMeshFileName= argv[2];
  wxString m_inConfigurationFileName= argv[3];
  wxString m_inFrequencyFileName= argv[4];
  wxString m_outVTKMeshFileName= argv[5];

  // IMPORT VOLUME
  medOpImporterVTK *importerVTK=new medOpImporterVTK();
  importerVTK->TestModeOn();

  mafLogMessage(m_inVTKVolumeFileName);

  importerVTK->SetFileName(m_inVTKVolumeFileName);
  importerVTK->ImportVTK();

  mafVMEVolumeGray *inputCTVolume=mafVMEVolumeGray::SafeDownCast(importerVTK->GetOutput());

  if(inputCTVolume == NULL)
  {  
    mafDEL(importerVTK);
    return MAF_ERROR;
  }

  inputCTVolume->Modified();
  inputCTVolume->Update();

  // IMPORT MESH
  importerVTK->TestModeOn();
  importerVTK->SetFileName(m_inVMEMeshFileName);
  importerVTK->ImportVTK();

  mafVMEMesh *inputVMEMesh=mafVMEMesh::SafeDownCast(importerVTK->GetOutput());

  if(inputVMEMesh == NULL)
  {  
    mafDEL(importerVTK);
    return MAF_ERROR;
  }

  inputVMEMesh->Modified();
  inputVMEMesh->Update();

  // EXECUTE
  // Create the Bonemat operation
  lhpOpBonemat *opBonemat = new lhpOpBonemat("op bonemat");
  opBonemat->TestModeOn();
  opBonemat->SetInput(inputVMEMesh);
  opBonemat->SetSourceVolume(inputCTVolume);
  opBonemat->LoadConfigurationFile(m_inConfigurationFileName); 
  opBonemat->SetFrequencyFileName(m_inFrequencyFileName);

  int result = opBonemat->Execute();

  if(result == MAF_OK)
  {
    // EXPORT BONEMAT MESH
    mafOpExporterVTK *exporterVTK=new mafOpExporterVTK();
    exporterVTK->TestModeOn();
    exporterVTK->SetInput(opBonemat->GetInput());
    exporterVTK->SetFileName(m_outVTKMeshFileName);
    exporterVTK->ExportVTK();

    mafDEL(exporterVTK);
  }

  mafDEL(opBonemat);
  mafDEL(importerVTK);

	return result;
}


