/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: BuilderM2OLogic.h,v $
Language:  C++
Date:      $Date: 2014-04-22 11:50 $
Authors:   Kewei Duan
==========================================================================*/

#ifndef __BuilderM2OLogic_H__
#define __BuilderM2OLogic_H__

//----------------------------------------------------------------------------
// includes :
//----------------------------------------------------------------------------
#include "medLogicWithManagers.h"

//----------------------------------------------------------------------------
// forward reference
//----------------------------------------------------------------------------
class lhpGUINetworkConnectionSettings;
class lhpGUIPythonSettings;
class lhpUser;

//----------------------------------------------------------------------------
// BuilderM2OLogic :
//----------------------------------------------------------------------------
/**
*/
class BuilderM2OLogic: public medLogicWithManagers
{
public:
	BuilderM2OLogic();
	virtual     ~BuilderM2OLogic(); 

	virtual void OnEvent(mafEventBase *maf_event);

	/** Validate the current MSF tree. Return the following enums from mafOpValidateTree

	mafOpValidateTree::VALIDATE_ERROR = 0, (also alert the user with an Error msgbox)
	mafOpValidateTree::VALIDATE_SUCCESS,
	mafOpValidateTree::VALIDATE_WARNING, (also alert the user with a Warning msgbox)

	*/
	int ValidateMSFTree();

	/** Configure the application.
	At this point are plugged all the managers, the side-bar docking panel. 
	Are plugged also all the setting to the dialogs interface. */
	virtual void Configure();

	/** Get user credentials */
	void GetCredentials();

	/** Superclass override with subapps specific contextual menu */
	void ViewContextualMenu(bool vme_menu);

protected:
	/** Respond to a VME_ADDED evt. propagate evt. to SideBar,ViewManager,ecc.. */
	virtual void VmeAdded(mafNode *vme);

	lhpGUINetworkConnectionSettings *m_NetworkConnectionSettings;
	lhpGUIPythonSettings *m_PythonSettings;
	lhpUser  *m_User;
};
#endif
