/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: BuilderM2ODecl.h,v $
Language:  C++
Date:      $Date: 2014-04-24 $
Authors:   Kewei Duan
==========================================================================*/


#ifndef __BuilderM2ODecl_H__
#define __BuilderM2ODecl_H__

#include "lhpDefines.h"
#include "medDecl.h"

enum LHP_MAIN_EVENT_ID
{
	ID_MSF_DATA_CACHE = MED_EVT_USER_START,
    ID_REQUEST_USER,
    ID_REQUEST_APPLICATION_NAME,
    ID_REQUEST_PYTHON_EXE_INTERPRETER,
    ID_REQUEST_PYTHONW_EXE_INTERPRETER,
    LHP_EVT_USER_START,
    ID_REQUEST_OPENSIM_DIR,
    ID_REQUEST_OPENSIM_VERSION,
};

#endif