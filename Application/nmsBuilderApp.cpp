/*=========================================================================
  Program:   Multimod Application Framework
  Module:    $RCSfile: nmsBuilderApp.cpp,v $
  Language:  C++
  Date:      $Date: 2012-04-19 13:26:46 $
  Version:   $Revision: 1.1.2.21 $
  Authors:   Simone Brazzale
==========================================================================
  Copyright (c) 2001/2005 
  CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/


#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------
#include "nmsBuilderApp.h"

#include <wx/datetime.h>
#include <wx/config.h>
#include <vtkTimerLog.h>

#ifdef MAF_USE_ITK
#include "lhpVMESurfaceScalarVarying.h"
#endif

#ifdef MED_USE_BTK
#include "lhpOpImporterC3DBTK.h" 
#endif

/* -----
  MAF
----- */
#include "mafDecl.h"
#include "mafVMEFactory.h"
#include "mafPics.h"
#include "mafGUIMDIFrame.h"
#include "mafInteractionFactory.h"
#include "mafNodeFactory.h" 
#include "mafNodeGeneric.h"
#include "mafNodeRoot.h"
#include "mafVMERoot.h"
#include "mafVMESurface.h"
#include "mafVMELandmark.h" 
#include "mafPipeFactoryVME.h"
#include "mafPipeVolumeSlice.h"
#include "mafVMEAFRefSys.h" 
#include "mafVMEHelAxis.h" 
// importers
#include "mafOpImporterMSF.h"
#include "mafOpImporterExternalFile.h"
#include "mafOpImporterImage.h"
#include "mafOpImporterSTL.h"
#include "mafOpImporterVTK.h"
#include "mafOpImporterRAWVolume_BES.h"
#include "mafOpImporterRAWVolume.h"
#include "mafOpImporterMesh.h"
#include "mafOpImporterVMEDataSetAttributes.h"
// exporters
#include "mafOpExporterBmp.h"
#include "mafOpExporterSTL.h"
#include "mafOpExporterVTK.h"
#include "mafOpExporterRaw.h"
#include "mafOpExporterMesh.h"
// operations
#include "mafOpLabelExtractor.h"
#include "mafOpOpenExternalFile.h"
#include "mafOpApplyTrajectory.h"
#include "mafOpCreateGroup.h"
#include "mafOpCreateMeter.h"
#include "mafOpCreateSlicer.h"
#include "mafOpCreateRefSys.h"
#include "mafOpFilterSurface.h"
#include "mafOpEditMetadata.h"
#include "mafOp2DMeasure.h"
#include "mafOpReparentTo.h"
#include "mafOpReparentTo.h"
#include "mafOpVOIDensityEditor.h"
#include "mafOpDecomposeTimeVarVME.h"
#include "mafOpExtractIsosurface.h"
#include "mafOpCrop.h"
#include "mafOpVOIDensity.h"
#include "mafOpAddLandmark.h"
#include "mafOpCreateSurfaceParametric.h"
#include "mafVMERawMotionData.h" 
// views
#include "mafViewVTK.h"
#include "mafViewCompound.h"
#include "mafViewRXCT.h"
#include "mafViewRX.h"
#include "mafViewOrthoSlice.h"
#include "mafViewArbitrarySlice.h"
#include "mafViewGlobalSliceCompound.h"
#include "mafViewSlice.h"
#include "mafViewImageCompound.h"
#include "mafViewIntGraph.h"
// temporary for testing
#include "mafViewSingleSliceCompound.h"

#include "lhpViewSurfacePlot.h"
#include "lhpPipeSurfacePlot.h"

/* -----
  MED
----- */
#include "medPipeFactoryVME.h"
#include "medVMEFactory.h"
#include "medPipeVolumeDRR.h"
#include "medPipeTrajectories.h" 
#include "medPipeGraph.h"
#include "medVMEComputeWrapping.h"
#include "medPipeComputeWrapping.h"
#include "medGUIDicomSettings.h"
#include "medVMEMuscleWrapper.h"
#include "medPipeCompoundVolumeFixedScalars.h"
// operations
#include "medOpImporterDicomOffis.h"
#include "medOpVolumeResample.h"
#include "medOpRegisterClusters.h"
#include "medOpScaleDataset.h"
#include "medOpMove.h"
#include "medOpClassicICPRegistration.h"
#include "medOpFreezeVME.h"
#include "medOpSurfaceMirror.h"
#include "medOpComputeWrapping.h"
#include "medOpMakeVMETimevarying.h"
#include "medOpMeshDeformation.h"
#include "medOpCreateMuscleWrapper.h"
#include "medOpComputeInertialTensor.h"
#include "mafOpMAFTransform.h"
// importers
#include "medOpImporterGRFWS.h"
#include "medOpImporterMotionData.h"
#include "medOpImporterLandmark.h"
#include "medOpImporterLandmarkWS.h"
#include "medOpImporterAnalogWS.h"
#include "medOpImporterRAWimages.h"
#include "medOpImporterVTKXML.h"
// exporters
#include "medOpExporterLandmark.h"
#include "medOpExporterMeters.h"
#include "medOpExporterWrappedMeter.h"
#include "medOpExporterVTKXML.h"
// views
#include "medViewArbitraryOrthoSlice.h"
#include "medViewSlicer.h"
#include "medViewVTKCompound.h"

/* -----
  LHP
----- */
#include "lhpVMEPoseGroup.h"
#include "lhpOpBonemat.h"
#include "lhpVMEScalarMatrix.h"
#include "lhpPipeVolumeBrushSlice.h"
#include "lhpOpBonematBatch.h"
#include "lhpOpImporterAnsysInputFile.h"
#include "lhpOpExporterAnsysInputFile.h"
#include "lhpOpImporterAnsysCDBFile.h"
#include "lhpOpImporterAnalogASCII.h"
#include "lhpOpImporterMeshFeBio.h"
#include "lhpOpExporterMeshFeBio.h"
#include "lhpVisualPipeSurfaceScalar.h"
#include "lhpOpExporterMatrixOctave.h"
#include "lhpOpImporterMatrixOctave.h"

// mmo
#include "mmoTimeReduce.h"
#include "mmoINPExporter.h"
#include "mmoMTRExporter.h"
#include "mmoINPImporter.h"
#include "mmoMTRImporter.h"
#include "mmoAFSys.h"
#include "mmoAverageLM.h"
#include "lhpOpCreateOpenSimModel.h"
#include "lhpOpModifyOpenSimModel.h"
#include "lhpOpModifyOpenSimModelCreateBodyFromSurface.h"
#include "lhpGUIOpenSimSettings.h"
#include "lhpOpModifyOpenSimModelCreateGeometry.h"
#include "lhpOpModifyOpenSimModelCreateCustomJoint.h"
#include "lhpOpModifyOpenSimModelCreateFreeJoint.h"
#include "lhpOpModifyOpenSimModelCreatePinJoint.h"
#include "lhpOpModifyOpenSimModelCreateBallJoint.h"
#include "lhpOpModifyOpenSimModelCreateMarkerSet.h"
#include "lhpOpModifyOpenSimModelCreateBodyFromSurfacesGroup.h"
#include "lhpOpModifyOpenSimModelRunIKSimulation.h"
#include "lhpOpModifyOpenSimModelRunIDSimulation.h"
#include "lhpOpCreateOpenSimModelWorkflow.h"
#include "lhpOpModifyOpenSimModelCreateMuscle.h"
#include "lhpOpModifyOpenSimModelCreateMuscleMultiPoint.h"

#ifdef USE_MATIO_API
	#include "lhpOpExporterMatrixMatlab.h"
	#include "lhpOpImporterMatrixMatlab.h"
#endif

#ifdef USE_OPENSIM_API 
	#include "lhpOpModifyOpenSimModelRunIKSimulationAPI.h"
	#include "lhpOpModifyOpenSimModelRunIDSimulationAPI.h"
	#include "lhpOpModifyOpenSimModelRunStaticOptimizationAPI.h"
	#include "lhpOpModifyOpenSimModelCreateCustomJointAPI.h"
#endif

#include "lhpOpImporterOpenSimIKSimulationResults.h"
#include "lhpOpImporterOpenSimIDSimulationResults.h"
#include "lhpOpModifyOpenSimModelCreateMuscleFromMeter.h"
#include "lhpOpImporterOpenSimSOSimulationResults.h"

// TODO: REFACTOR THIS 
// this component is used only to override the Accept,  
// and it`s the minimal amount of code in order to override the method
// it could go in a separate file with other redefined Accept`s


class lhpOpMove : public medOpMove
{
public:

  /** Return true for the acceptable vme type. */
  bool Accept(mafNode* vme)
  {
    bool accepted = false;
    
    accepted =   !vme->IsA("lhpVMESurfaceScalarVarying") \
      && !vme->IsA("mafVMEMeter") \
      && !vme->IsA("medVMELabeledVolume") \
      && !vme->IsA("mafVMEHelicalAxis");  

    if (accepted == false)
    {
      return false;
    }
    else
    {
      return medOpMove::Accept(vme);
    }
  }
};

class lhpOpScaleDataset : public medOpScaleDataset
{
public:

  /** Return true for the acceptable vme type. */
  bool Accept(mafNode* vme)
  {
    bool accepted = false;

    accepted =   !vme->IsA("lhpVMESurfaceScalarVarying") \
      && !vme->IsA("mafVMEMeter") \
      && !vme->IsA("medVMELabeledVolume") \
      && !vme->IsA("mafVMEHelicalAxis");  

    if (accepted == false)
    {
      return false;
    }
    else
    {
      return medOpScaleDataset::Accept(vme);
    }
  }
};

// END TODO: REFACTOR THIS 


//BES: 23.6.2008 - TO BE UNCOMMENTED WHEN VTK IS PATCHED AS I SUGGESTED
////BES: 14.5.2008 - special memory manager for supporting of large data
//#include <vtkDataArrayMemMng.h>


//--------------------------------------------------------------------------------
// Create the Application
//--------------------------------------------------------------------------------

IMPLEMENT_APP(nmsBuilderApp)

////BES: 14.5.2008 - OnIdle to unlock blocks
//BEGIN_EVENT_TABLE(nmsBuilderApp, wxApp)
//  EVT_IDLE(nmsBuilderApp::OnIdle)
//END_EVENT_TABLE()


//--------------------------------------------------------------------------------
bool nmsBuilderApp::OnInit()
//--------------------------------------------------------------------------------
{
  ////BES: 14.5.2008 - initialize the manager (with default settings)
  ////It provides a sophisticated memory-disk swap algorithm that
  ////allows handling of many large size memory blocks that would
  ////take more memory than available in total.
  ////The mechanism is NOT GUARANTEED TO BE SAFE
  ////Swapping (especially, if HandleNewFailure option is enabled)
  ////may lead to artifacts in data or even 
  //vtkDataArrayMemMng::InitializeManagerUnSafeMode();  


  mafPictureFactory::GetPictureFactory()->Initialize();	
 
  #include "pic/nmsBuilder/FRAME_ICON16x16.xpm"
  mafADDPIC(FRAME_ICON16x16);

  #include "pic/nmsBuilder/FRAME_ICON32x32.xpm"
  mafADDPIC(FRAME_ICON32x32);

  #include "pic/nmsBuilder/MDICHILD_ICON.xpm"
  mafADDPIC(MDICHILD_ICON);

  int result;
 
  result = medVMEFactory::Initialize();
  assert(result == MAF_OK);

  result = medPipeFactoryVME::Initialize();
  assert(result==MAF_OK);

  result = mafInteractionFactory::Initialize();
  assert(result==MAF_OK);

  
  mafPlugNode<mafVMERawMotionData>("VME representing raw motion data");
  mafPlugNode<mafVMEAFRefSys>("VME representing anatomical frame");
  mafPlugNode<lhpVMEPoseGroup>("Pose group for time-varying vme that not inherit for mafVMEGEneric");
  mafPlugNode<mafVMEHelAxis>("VME representing helical axis");
  mafPlugNode<mafVMEVolumeLarge>("VME storing large volume datasets with one scalar component");
  mafPlugNode<lhpVMEScalarMatrix>("VME representing analog timevariyng data");
  mafPlugNode<medVMEComputeWrapping>("Generalized another VME Meter with wrapping geometry");
  mafPlugPipe<medPipeComputeWrapping>("Pipe to Visualize Compute Wrapping Meter");
  mafPlugPipe<lhpVisualPipeSurfaceScalar>("Visual pipe to render a surface with its scalar values");
  mafPlugPipe<lhpPipeSurfacePlot>("vph2PipeAnalogGraph");

#ifdef MAF_USE_ITK
  mafPlugNode<lhpVMESurfaceScalarVarying>("VME representing surface with attached time varying mafVMEScalar");
#endif

  //BES: 14.11.2008 - some stupid VME to demonstrate muscle wrapping
  mafPlugNode<medVMEMuscleWrapper>("Procedural VME representing muscle deformed according to its action lines");
  //Youbing:13.04.2010 - plug the lhpVolumeBrushSlice pipe
  mafPlugPipe<lhpPipeVolumeBrushSlice>("Visual pipe to render paintings on a volume slice");

  m_Logic = new nmsBuilderLogic();
  m_Logic->GetTopWin()->SetTitle("nmsBuilder");
  m_Logic->Configure();
  SetTopWindow(mafGetFrame());  

	wxRegKey RegKey(wxString("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\nmsBuilder"));
	if(RegKey.Exists())
	{
		RegKey.Create();
		wxString revision;
		RegKey.QueryValue(wxString("DisplayVersion"), revision);
		m_Logic->SetRevision(revision);
	}
	else
	{
		wxString revision="0.1";
		m_Logic->SetRevision(revision);
	}

  //------------------------- Importers -------------------------
  medGUIDicomSettings *dicomSettings=new medGUIDicomSettings(NULL,"DICOM");
  m_Logic->Plug(new medOpImporterDicomOffis("DICOM"),"Images",true,dicomSettings);
  m_Logic->Plug(new mafOpImporterRAWVolume_BES("Raw Volume"),"Images");
  m_Logic->Plug(new medOpImporterRAWImages("Raw Images"),"Images");
  m_Logic->Plug(new mafOpImporterImage("Images"),"Images");
  m_Logic->Plug(new mmoINPImporter("INP/INP_AF"), "Geometries");
  m_Logic->Plug(new mafOpImporterSTL("STL"),"Geometries");
  m_Logic->Plug(new mmoMTRImporter("MTR"), "Geometries");
  m_Logic->Plug(new medOpImporterVTKXML("VTP"),"Geometries");
  m_Logic->Plug(new mafOpImporterVTK("VTK"),"Other");
  m_Logic->Plug(new mafOpImporterMSF("MSF"),"Other");
  m_Logic->Plug(new mafOpImporterExternalFile("External data"), "Other");
#ifdef MED_USE_BTK
  m_Logic->Plug(new lhpOpImporterC3DBTK("C3D-BTK"),"Motion Analysis");  
#endif
  m_Logic->Plug(new medOpImporterLandmark("Landmark"),"Motion Analysis"); 
  m_Logic->Plug(new medOpImporterMotionData<mafVMERawMotionData>("Raw Motion Data", "RAW Motion Data (*.MAN)|*.MAN", "Dictionary (*.txt)|*.txt"), "Motion Analysis");
  m_Logic->Plug(new lhpOpImporterAnalogASCII("ASCII Analog (X,Y)"),"Motion Analysis");
  m_Logic->Plug(new medOpImporterAnalogWS("ASCII Analog (VWs)"), "Motion Analysis");
  m_Logic->Plug(new medOpImporterLandmarkWS("ASCII Trajectories (VWs)"),"Motion Analysis");
  m_Logic->Plug(new medOpImporterGRFWS("ASCII Force Plates (VWs)"), "Motion Analysis");
  m_Logic->Plug(new mafOpImporterMesh("Generic Mesh"), "Finite Element");

  // Commented for nmsBuilder software first release (27/09/2012)
  // m_Logic->Plug(new lhpOpImporterMeshFeBio("Mesh FEBio"), "Finite Element");

  m_Logic->Plug(new lhpOpImporterAnsysCDBFile("Ansys CDB File"), "Finite Element");	
  m_Logic->Plug(new lhpOpImporterAnsysInputFile("Ansys Input File"), "Finite Element");	
  
  // Commented for nmsBuilder software first release (27/09/2012)
  // m_Logic->Plug(new lhpOpImporterMatrixOctave("Octave"),"Other");
  
  #ifdef USE_MATIO_API
	  m_Logic->Plug(new lhpOpImporterMatrixMatlab("Matlab"),"Other");
	  m_Logic->Plug(new lhpOpExporterMatrixMatlab("Matlab"),"Other");
  #endif
  
  m_Logic->Plug(new lhpOpImporterOpenSimIKSimulationResults("Inverse Kinematics Simulation Results"),"OpenSim");
  m_Logic->Plug(new lhpOpImporterOpenSimIDSimulationResults("Inverse Dynamics Simulation Results"),"OpenSim");
  m_Logic->Plug(new lhpOpImporterOpenSimSOSimulationResults("Static Optimization Simulation Results"),"OpenSim");

  //-------------------------------------------------------------

  //------------------------- Exporters -------------------------
  m_Logic->Plug(new medOpExporterWrappedMeter("Action Line"), "Other");
  m_Logic->Plug(new medOpExporterMeters("Meters"), "Other");
  m_Logic->Plug(new mafOpExporterVTK("VTK"), "Other");
  m_Logic->Plug(new mmoINPExporter("INP"),"Geometries");
  m_Logic->Plug(new mmoMTRExporter("MTR"), "Geometries");
  m_Logic->Plug(new mafOpExporterSTL("STL"),"Geometries");
  m_Logic->Plug(new medOpExporterVTKXML("VTP"),"Geometries");
	m_Logic->Plug(new mafOpExporterRAW("Raw"), "Images");
  m_Logic->Plug(new mafOpExporterBmp("Bmp"), "Images");
  m_Logic->Plug(new medOpExporterLandmark("Landmark"), "Motion Analysis");
  m_Logic->Plug(new mafOpExporterMesh("Generic Mesh"), "Finite Element");
  
  // Commented for nmsBuilder software first release (27/09/2012)
  // m_Logic->Plug(new lhpOpExporterMeshFeBio("Mesh FEBio"),"Finite Element");

  m_Logic->Plug(new lhpOpExporterAnsysInputFile("Ansys Input File"),"Finite Element");
  
  // Commented for nmsBuilder software first release (27/09/2012)
  // m_Logic->Plug(new lhpOpExporterMatrixOctave("Octave"),"Other");

  //-------------------------------------------------------------

  //------------------------- Operations -------------------------
  
	m_Logic->Plug(new mafOpCreateGroup("Group"),"Create/New"); //
    m_Logic->Plug(new mafOpCreateRefSys("Refsys"),"Create/New"); //
  	m_Logic->Plug(new mafOpCreateSurfaceParametric("Parametric Surface"),"Create/New"); //
  	m_Logic->Plug(new mafOpAddLandmark("Add Landmark \tCtrl+A"),"Create/New"); //
    m_Logic->Plug(new mafOpCreateSlicer("Slicer"),"Create/Derive"); //
    m_Logic->Plug(new medOpFreezeVME("Freeze VME"),"Create/Derive"); //
    m_Logic->Plug(new mafOpCreateMeter("Distance Meter"),"Create/Derive"); //
    m_Logic->Plug(new medOpComputeWrapping("Wrapped Action Line"),"Create/Derive");//
    m_Logic->Plug(new medOpCreateMuscleWrapper("Muscle Wrapper"),"Create/Derive"); //
  	m_Logic->Plug(new mafOpExtractIsosurface("Extract Isosurface"),"Create/Derive"); //
    m_Logic->Plug(new mafOpDecomposeTimeVarVME("Decompose Time"),"Create/Derive"); //
    m_Logic->Plug(new mafOpLabelExtractor("Extract Label"),"Create/Derive"); //
    m_Logic->Plug(new medOpRegisterClusters("Register Landmark Cloud"),"Modify/Fuse"); //
    m_Logic->Plug(new mafOpReparentTo("Reparent to...  \tCtrl+R"),"Modify/Fuse"); //
    m_Logic->Plug(new medOpClassicICPRegistration("Register Surface"),"Modify/Fuse"); //
  	m_Logic->Plug(new mafOpFilterSurface("Filter Surface"),"Modify"); //
    m_Logic->Plug(new mafOpVOIDensityEditor("Volume Density"),"Modify"); //
    m_Logic->Plug(new medOpMeshDeformation("Deform Surface"), "Modify"); //
    m_Logic->Plug(new mafOpApplyTrajectory("Apply Trajectory"), "Modify");
    m_Logic->Plug(new medOpSurfaceMirror("Surface Mirror"),"Modify"); //
  	m_Logic->Plug(new mafOpCrop("Crop Volume"),"Modify"); //
  	m_Logic->Plug(new medOpVolumeResample("Volume Resample"),"Modify"); //
    m_Logic->Plug(new lhpOpScaleDataset(),"Modify"); //
    m_Logic->Plug(new lhpOpMove(),"Modify"); //
	
    m_Logic->Plug(new mafOpImporterVMEDataSetAttributes("VME DataSet Attributes Adder"),"Modify"); //
    m_Logic->Plug(new mmoTimeReduce("Time reduce"),"Modify");//
  	m_Logic->Plug(new mafOp2DMeasure("2D Measure"),"Measure"); //
  	m_Logic->Plug(new mafOpVOIDensity("VOI Density"),"Measure"); //
  	m_Logic->Plug(new medOpComputeInertialTensor("Compute Inertial Tensor"),"Measure"); //
    m_Logic->Plug(new mafOpOpenExternalFile("Open with external program"),"Manage"); //
    m_Logic->Plug(new mmoAFSys("AFRefsys"),"Create/Derive"); //
    m_Logic->Plug(new mmoAverageLM("Average landmark"),"Create/Derive");  //
	m_Logic->Plug(new mafOpEditMetadata("VME Metadata"),"Modify");  //
	m_Logic->Plug(new lhpOpCreateOpenSimModel("OpenSim Model"),"Create/New"); //
	m_Logic->Plug(new lhpOpCreateOpenSimModelWorkflow("OpenSim Model from Wizard"),"Create/New", true); //

	m_Logic->Plug(new lhpOpModifyOpenSimModel("Editing Tools"),"Modify/Opensim Model", true); //
	m_Logic->Plug(new lhpOpModifyOpenSimModelCreateBodyFromSurface("Create Body from Surface"),"Modify/Opensim Model", true);  //
	m_Logic->Plug(new lhpOpModifyOpenSimModelCreateBodyFromSurfacesGroup("Create Body from Surfaces Group"),"Modify/Opensim Model", true); //
	m_Logic->Plug(new lhpOpModifyOpenSimModelCreateGeometry("Create Geometry"),"Modify/Opensim Model", true); //
	m_Logic->Plug(new lhpOpModifyOpenSimModelCreateFreeJoint("Create Free Joint with Ground"),"Modify/Opensim Model", true); //
	
	// Commented for nmsBuilder software first release (27/09/2012)
	m_Logic->Plug(new lhpOpModifyOpenSimModelCreateCustomJoint("Create Custom Joint"),"Modify/Opensim Model", true); //

	m_Logic->Plug(new lhpOpModifyOpenSimModelCreatePinJoint("Create Pin Joint"),"Modify/Opensim Model", true); //
	m_Logic->Plug(new lhpOpModifyOpenSimModelCreateBallJoint("Create Ball Joint"),"Modify/Opensim Model", true); //
	// m_Logic->Plug(new lhpOpModifyOpenSimModelCreateMuscle("Create Muscle"),"Modify/Opensim Model", true);
	m_Logic->Plug(new lhpOpModifyOpenSimModelCreateMuscleMultiPoint("Create Muscle Multi Point"),"Modify/Opensim Model", true); //
	m_Logic->Plug(new lhpOpModifyOpenSimModelCreateMuscleFromMeter("Create Muscle from Action Line"),"Modify/Opensim Model", true); //
	m_Logic->Plug(new lhpOpModifyOpenSimModelCreateMarkerSet("Create Marker Set"),"Modify/Opensim Model", true); //

	#ifdef USE_OPENSIM_API
	// plug the api solvers
	m_Logic->Plug(new lhpOpModifyOpenSimModelRunIKSimulationAPI("Run Inverse Kinematics Simulation (API)"),"Modify/Opensim Model", true); //
	m_Logic->Plug(new lhpOpModifyOpenSimModelRunIDSimulationAPI("Run Inverse Dynamics Simulation (API)"),"Modify/Opensim Model", true); //
	m_Logic->Plug(new lhpOpModifyOpenSimModelRunStaticOptimizationAPI("Run Static Optimization (API)"),"Modify/Opensim Model", true);

	// Mockup operation to create a knee custom joint from API
	// m_Logic->Plug(new lhpOpModifyOpenSimModelCreateCustomJointAPI("Create Knee Custom Joint (Test custom joint build from API)"),"Modify/Opensim Model", true);
	#endif	
	
	#ifndef USE_OPENSIM_API
	// plug the external exe solvers
		m_Logic->Plug(new lhpOpModifyOpenSimModelRunIKSimulation("Run Inverse Kinematics Simulation"),"Modify/Opensim Model", true);
		m_Logic->Plug(new lhpOpModifyOpenSimModelRunIDSimulation("Run Inverse Dynamics Simulation"),"Modify/Opensim Model", true);
	#endif	
	

  //-------------------------------------------------------------

  //------------------------- Views -------------------------
  //View Arbitrary Slice
   mafViewArbitrarySlice *ArbitraryView = new mafViewArbitrarySlice("Arbitrary");
   ArbitraryView->PackageView();
   m_Logic->Plug(ArbitraryView);

   lhpViewSurfacePlot * surfPlotView = new  lhpViewSurfacePlot("Surface ID IK Plot", CAMERA_PERSPECTIVE, false);
   surfPlotView->PlugVisualPipe("mafVMESurface", "lhpPipeSurfacePlot",MUTEX);
   m_Logic->Plug(surfPlotView);

  

  // View Analog graph
  mafViewVTK *vtkgraph = new mafViewVTK("Analog Graph", CAMERA_PERSPECTIVE, false);
  vtkgraph->PlugVisualPipe("medVMEAnalog", "medPipeGraph",MUTEX);
  medViewVTKCompound *graph = new medViewVTKCompound("Analog Graph");
  graph->SetExternalView(vtkgraph);
  graph->PackageView();
  m_Logic->Plug(graph);

  // View DRR
  medViewVTKCompound *vdrr = new medViewVTKCompound("DRR");
  mafViewVTK *drrVTK = new mafViewVTK("DRR");
  drrVTK->PlugVisualPipe("mafVMEVolumeGray","medPipeVolumeDRR",MUTEX);
  drrVTK->PlugVisualPipe("mafVMEVolumeLarge","medPipeVolumeDRR",MUTEX);
  vdrr->SetExternalView(drrVTK);
  vdrr->PackageView();
  m_Logic->Plug(vdrr);

  //View Global Slice
  mafViewGlobalSliceCompound *GlobalSlice = new mafViewGlobalSliceCompound("Global Slice");
  GlobalSlice->PackageView();
  m_Logic->Plug(GlobalSlice);

  //View Isosurface
  medViewVTKCompound *viso = new medViewVTKCompound("Isosurface");
  mafViewVTK *isoVTK = new mafViewVTK("Isosurface");
  isoVTK->PlugVisualPipe("mafVMEVolumeGray", "mafPipeIsosurface",MUTEX);
  isoVTK->PlugVisualPipe("medVMELabeledVolume", "mafPipeIsosurface",MUTEX);
  isoVTK->PlugVisualPipe("mafVMEVolumeLarge","mafPipeIsosurface",MUTEX);
  viso->SetExternalView(isoVTK);
  viso->PackageView();
  m_Logic->Plug(viso);

  //View Orthoslice
  mafViewOrthoSlice *viewOrthoSlice = new mafViewOrthoSlice("OrthoSlice");
  viewOrthoSlice->PackageView();
  m_Logic->Plug(viewOrthoSlice);

  //View RXCT
  mafViewRXCT *vrxctl = new mafViewRXCT("RXCT");
  vrxctl->PackageView();
  m_Logic->Plug(vrxctl);
  
  //View Surface
  medViewVTKCompound *vsurface = new medViewVTKCompound("Surface");
  mafViewVTK *surfaceVTK = new mafViewVTK("Surface");
  surfaceVTK->PlugVisualPipe("mafVMELandmark", "medPipeTrajectories");
  vsurface->SetExternalView(surfaceVTK);
  vsurface->PackageView();
  m_Logic->Plug(vsurface);

  //View Slicer
  medViewSlicer *slicerView = new medViewSlicer("Slicer");
  slicerView->PackageView();
  m_Logic->Plug(slicerView);
  //-------------------------------------------------------------

  wxBitmap splashBitmap;
  splashBitmap.LoadFile("../Splash/NMSBuilderSplash.bmp", wxBITMAP_TYPE_BMP);
  m_Logic->ShowSplashScreen(splashBitmap); 

  // show the application
	m_Logic->ShowSplashScreen(splashBitmap);
  m_Logic->Show();

  m_Logic->Init(0,NULL); // calls FileNew - which create the root

  return TRUE;
}
//--------------------------------------------------------------------------------
int nmsBuilderApp::OnExit()
//--------------------------------------------------------------------------------
{
  cppDEL(m_Logic);

  //this hack is fixing VTK internal memory leak
  vtkTimerLog::CleanupLog();
  return 0;
}

//void nmsBuilderApp::OnIdle( wxIdleEvent &event )
//{ 
//  vtkDataArrayMemMng::GetDataArrayMemMng()->UnlockAllMemory(0);
//  event.Skip();
//}
