/*=========================================================================
  Program:   Multimod Application Framework
  Module:    $RCSfile: iPoseApp.h,v $
  Language:  C++
  Date:      $Date: 2011-03-22 16:00:30 $
  Version:   $Revision: 1.1.2.1 $
  Authors:   Matteo Giacomoni
==========================================================================
  Copyright (c) 2001/2005 
  CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/
#ifndef __iPoseApp_H__
#define __iPoseApp_H__
//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "lhpBuilderLogic.h" 
#include "mafEvent.h"
#include "mafObserver.h"

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class lhpUser;

class iPoseApp : public wxApp
{
public:
  bool OnInit();
  int  OnExit();

protected:
  lhpBuilderLogic *m_Logic;

};
DECLARE_APP(iPoseApp)
//BES: 28.5.2009 - cannot link under VS 2008 because of some missing link libs
#if _MSC_VER >= 1500
#pragma comment( lib, "ofstd.lib" ) 
#pragma comment( lib, "netapi32.lib" ) 
#endif
#endif 
