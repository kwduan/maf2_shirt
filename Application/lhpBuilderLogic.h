/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: lhpBuilderLogic.h,v $
Language:  C++
Date:      $Date: 2010-10-25 12:24:25 $
Version:   $Revision: 1.1.1.1.2.1 $
Authors:   Paolo Quadrani
==========================================================================
Copyright (c) 2002/2004
CINECA - Interuniversity Consortium (www.cineca.it) 
=========================================================================*/
#ifndef __lhpBuilderLogic_H__
#define __lhpBuilderLogic_H__

//----------------------------------------------------------------------------
// includes :
//----------------------------------------------------------------------------
#include "medLogicWithManagers.h"

//----------------------------------------------------------------------------
// forward reference
//----------------------------------------------------------------------------
class lhpGUINetworkConnectionSettings;
class lhpGUIPythonSettings;
class lhpUser;

//----------------------------------------------------------------------------
// lhpBuilderLogic :
//----------------------------------------------------------------------------
/**
*/
class lhpBuilderLogic: public medLogicWithManagers
{
public:
	lhpBuilderLogic();
	virtual     ~lhpBuilderLogic(); 

	virtual void OnEvent(mafEventBase *maf_event);

	/** Validate the current MSF tree. Return the following enums from mafOpValidateTree

	mafOpValidateTree::VALIDATE_ERROR = 0, (also alert the user with an Error msgbox)
	mafOpValidateTree::VALIDATE_SUCCESS,
	mafOpValidateTree::VALIDATE_WARNING, (also alert the user with a Warning msgbox)

	*/
	int ValidateMSFTree();

	/** Configure the application.
	At this point are plugged all the managers, the side-bar docking panel. 
	Are plugged also all the setting to the dialogs interface. */
	virtual void Configure();

	/** Get user credentials */
	void GetCredentials();

	/** Superclass override with subapps specific contextual menu */
	void ViewContextualMenu(bool vme_menu);

protected:
	/** Respond to a VME_ADDED evt. propagate evt. to SideBar,ViewManager,ecc.. */
	virtual void VmeAdded(mafNode *vme);

	lhpGUINetworkConnectionSettings *m_NetworkConnectionSettings;
	lhpGUIPythonSettings *m_PythonSettings;
	lhpUser  *m_User;
};
#endif
