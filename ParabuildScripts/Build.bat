REM -----------------------------------------------------------------------------
REM driver batch file for MAF building
REM -----------------------------------------------------------------------------

IF "%1" == "VAPP_DEBUG" GOTO VAPP_DEBUG
IF "%1" == "VAPP_RELEASE" GOTO VAPP_RELEASE
IF "%1" == "VAPP_DEBUG_2008" GOTO VAPP_DEBUG_2008
IF "%1" == "VAPP_RELEASE_2008" GOTO VAPP_RELEASE_2008
IF "%1" == "VAPP_RELEASE_2010" GOTO VAPP_RELEASE_2010
IF NOT "%1" == "MAF_DEBUG" GOTO UNKNOWN_CONDITION

:VAPP_DEBUG
CALL "%PROGRAMFILES%/Microsoft Visual Studio .NET 2003/Common7/Tools/vsvars32.bat"
devenv ./LHPBuilder_Parabuild/LHP.sln /project ALL_BUILD.vcproj /build debug /out build_log.txt
GOTO END

:VAPP_RELEASE
CALL "%PROGRAMFILES%/Microsoft Visual Studio .NET 2003/Common7/Tools/vsvars32.bat"
devenv ./LHPBuilder_Parabuild/LHP.sln /project ALL_BUILD.vcproj /build release /out build_log.txt
GOTO END

:VAPP_DEBUG_2008
CALL "%PROGRAMFILES%/Microsoft Visual Studio 9.0/Common7/Tools/vsvars32.bat"
devenv ./LHPBuilder_Parabuild/LHP.sln /project ALL_BUILD.vcproj /build debug /out build_log.txt
GOTO END

:VAPP_RELEASE_2008
CALL "%PROGRAMFILES%/Microsoft Visual Studio 9.0/Common7/Tools/vsvars32.bat"
devenv ./LHPBuilder_Parabuild/LHP.sln /project ALL_BUILD.vcproj /build release /out build_log.txt
GOTO END

:VAPP_RELEASE_2010
CALL "%PROGRAMFILES%/Microsoft Visual Studio 10.0/Common7/Tools/vsvars32.bat"
devenv ./LHPBuilder_Parabuild/LHP.sln /project ALL_BUILD.vcxproj /build release /out build_log.txt
GOTO END

:UNKNOWN_CONDITION

:END
