/*=========================================================================
  Program:   Multimod Application Framework
  Module:    $RCSfile: lhpOpExporterAnsysInputFile.cpp,v $
  Language:  C++
  Date:      $Date: 2010-12-03 14:58:16 $
  Version:   $Revision: 1.1.1.1.2.3 $
  Authors:   Stefano Perticoni
==========================================================================
  Copyright (c) 2001/2005 
  CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/

#include "mafDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "lhpBuilderDecl.h"
#include "lhpUtils.h"
#include "lhpOpExporterAnsysInputFile.h"

#include "wx/busyinfo.h"

#include "mafDecl.h"
#include "mafGUI.h"

#include "mafSmartPointer.h"
#include "mafTagItem.h"
#include "mafTagArray.h"
#include "mafVME.h"
#include "mafVMEMesh.h"
#include "mafVMEMeshAnsysTextExporter.h"
#include "mafAbsMatrixPipe.h"

#include "vtkMAFSmartPointer.h"

#include <iostream>
#include <fstream>

//----------------------------------------------------------------------------
mafCxxTypeMacro(lhpOpExporterAnsysInputFile);
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
lhpOpExporterAnsysInputFile::lhpOpExporterAnsysInputFile(const wxString &label) :
mafOp(label)
//----------------------------------------------------------------------------
{
  m_PythonExeFullPath = "python.exe_UNDEFINED";
  m_PythonwExeFullPath = "pythonw.exe_UNDEFINED";

  m_OpType  = OPTYPE_EXPORTER;
  m_Canundo = true;
  m_ImporterType = 0;
  m_ImportedVmeMesh = NULL;
  m_NodesFileName = "";
  m_ElementsFileName = "";
  m_MaterialsFileName = "";
  m_AnsysInputFileName = "";
  m_CacheDir = (lhpUtils::lhpGetApplicationDirectory() + "\\Data\\AnsysWriterCache").c_str();
  m_AnsysOutputFileNameFullPath		= "";
  m_FileDir = (lhpUtils::lhpGetApplicationDirectory() + "/Data/External/").c_str();
  
  // This is for deploy: need to work on PYTHONPATH to solve issues with Python modules execution path...
  m_AnsysPythonExporterFullPathFileName = (lhpUtils::lhpGetApplicationDirectory() + "\\ASCIIParser\\ansysWriter.py").c_str();
  
  m_Pid = -1;
  m_ABSMatrixFlag = 1;
}

enum ANSYS_EXPORTER_ID
{
  ID_ABS_MATRIX_TO_STL = MINID,  
};

//----------------------------------------------------------------------------
lhpOpExporterAnsysInputFile::~lhpOpExporterAnsysInputFile()
//----------------------------------------------------------------------------
{
  mafDEL(m_ImportedVmeMesh);
}
//----------------------------------------------------------------------------
bool lhpOpExporterAnsysInputFile::Accept(mafNode *node)
//----------------------------------------------------------------------------
{
  return (node->IsA("mafVMEMesh"));
}
//----------------------------------------------------------------------------
mafOp* lhpOpExporterAnsysInputFile::Copy()   
//----------------------------------------------------------------------------
{
  lhpOpExporterAnsysInputFile *cp = new lhpOpExporterAnsysInputFile(m_Label);
  return cp;
}
//----------------------------------------------------------------------------
void lhpOpExporterAnsysInputFile::OpRun()   
//----------------------------------------------------------------------------
{
  // get python interpreters
  mafEvent eventGetPythonExe;
  eventGetPythonExe.SetSender(this);
  eventGetPythonExe.SetId(ID_REQUEST_PYTHON_EXE_INTERPRETER);
  mafEventMacro(eventGetPythonExe);

  if(eventGetPythonExe.GetString())
  {
    m_PythonExeFullPath.Erase(0);
    m_PythonExeFullPath = eventGetPythonExe.GetString()->GetCStr();
    m_PythonExeFullPath.Append(" ");
  }

  mafEvent eventGetPythonwExe;
  eventGetPythonwExe.SetSender(this);
  eventGetPythonwExe.SetId(ID_REQUEST_PYTHONW_EXE_INTERPRETER);
  mafEventMacro(eventGetPythonwExe);

  if(eventGetPythonwExe.GetString())
  {
    m_PythonwExeFullPath.Erase(0);
    m_PythonwExeFullPath = eventGetPythonwExe.GetString()->GetCStr();
    m_PythonwExeFullPath.Append(" ");
  }

  CreateGui();
}
//----------------------------------------------------------------------------
int lhpOpExporterAnsysInputFile::Write()
//----------------------------------------------------------------------------
{
  if (!m_TestMode)
  {
    wxBusyInfo wait(_("Writing file: ..."));
  }

  bool cacheDirExist = wxDirExists(m_CacheDir.GetCStr());

  if (cacheDirExist == false)
  {
	  std::ostringstream stringStream;
	  stringStream << "creating cache dir: " << m_CacheDir.GetCStr() << std::endl;
	  mafLogMessage(stringStream.str().c_str());          
	  wxMkdir(m_CacheDir.GetCStr());
  }
  else
  {
	  std::ostringstream stringStream;
	  stringStream << "found cache dir: " << m_CacheDir.GetCStr() << std::endl;
	  mafLogMessage(stringStream.str().c_str());

  }

  // check for cache dir existence
  assert(wxDirExists(m_CacheDir.GetCStr()));

  m_NodesFileName = m_CacheDir + "\\AnsysExporterNodes.lis" ;
  m_ElementsFileName = m_CacheDir + "\\AnsysExporterElements.lis" ;
  m_MaterialsFileName = m_CacheDir + "\\AnsysExporterMaterials.lis" ;

  mafVMEMeshAnsysTextExporter *exporter = new mafVMEMeshAnsysTextExporter;
  mafVMEMesh *input = mafVMEMesh::SafeDownCast(m_Input);
  assert(input);

  // Must update grid in order to load material data efficiently!
  input->GetUnstructuredGridOutput()->Update();
  exporter->SetInput(input->GetUnstructuredGridOutput()->GetUnstructuredGridData());
  exporter->SetOutputNodesFileName(m_NodesFileName.c_str());
  exporter->SetOutputElementsFileName(m_ElementsFileName.c_str());
  exporter->SetOutputMaterialsFileName(m_MaterialsFileName.c_str());

  mafVMEMesh *inMesh = mafVMEMesh::SafeDownCast(m_Input);
  assert(inMesh);

  exporter->SetMatrix(inMesh->GetAbsMatrixPipe()->GetMatrixPointer()->GetVTKMatrix());
  exporter->SetApplyMatrix(m_ABSMatrixFlag);
  int result = exporter->Write();
  delete exporter;
  
  if (result == MAF_OK)
  {
    // continue
  } 
  else
  {
    wxMessageBox("Problems generating intermediate output files! \n\
See the log area for more details. ");
    return MAF_ERROR;
  }

  wxArrayString output;
  wxArrayString errors;
  
  // execute the Python reader
  wxString command2execute;
  
  mafLogMessage( _T("Current working directory is: '%s' "), wxGetCwd().c_str() );

  // read cache files and create ansys file
  command2execute = m_PythonExeFullPath.GetCStr();
  command2execute.Append(" \"\"");
  command2execute.Append(m_AnsysPythonExporterFullPathFileName.c_str());
  command2execute.Append(" \"");
  command2execute.Append(m_NodesFileName.c_str());
  command2execute.Append("\" \"");
  command2execute.Append(m_ElementsFileName.c_str());
  command2execute.Append("\" \"");
  command2execute.Append(m_MaterialsFileName.c_str());
  command2execute.Append("\" \"");
  command2execute.Append(m_AnsysOutputFileNameFullPath.c_str());
  command2execute.Append("\"");
  mafLogMessage( _T("Executing command: '%s'"), command2execute.c_str() );

  // m_Pid = wxExecute(command2execute, output, errors, wxEXEC_NODISABLE);
  m_Pid = wxExecute(command2execute, wxEXEC_SYNC);

  for (int i = 0; i < output.GetCount(); i++)
  {
    mafLogMessage(output[i].c_str());
  }
  
  if ( !command2execute )
    return MAF_ERROR;

  mafLogMessage(_T("Command process '%s' terminated with exit code %d."),
    command2execute.c_str(), m_Pid);

  return MAF_OK;

}
//----------------------------------------------------------------------------
// Operation constants
//----------------------------------------------------------------------------
enum Mesh_Importer_ID
{
  ID_FIRST = MINID,
  ID_Importer_Type,
  ID_NodesFileName,
  ID_ElementsFileName,
  ID_MaterialsFileName,
  ID_OK,
  ID_CANCEL,
};
//----------------------------------------------------------------------------
void lhpOpExporterAnsysInputFile::CreateGui()
//----------------------------------------------------------------------------
{
  mafString wildc = "Stereo Litography (*.stl)|*.stl";

  m_Gui = new mafGUI(this);
  m_Gui->Label("absolute matrix",true);
  m_Gui->Bool(ID_ABS_MATRIX_TO_STL,"apply",&m_ABSMatrixFlag,0);
  m_Gui->OkCancel();  
  m_Gui->Divider();

  ShowGui();

}
//----------------------------------------------------------------------------
void lhpOpExporterAnsysInputFile::OnEvent(mafEventBase *maf_event) 
//----------------------------------------------------------------------------
{
  if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
  {
    switch(e->GetId())
    {
      case wxOK:
      {
        OnOK();
        this->OpStop(OP_RUN_OK);
      }
      break;
      case wxCANCEL:
      {
        this->OpStop(OP_RUN_CANCEL);
      }
      break;
      default:
        mafEventMacro(*e);
      break;
    }	
  }
}
//----------------------------------------------------------------------------
long lhpOpExporterAnsysInputFile::GetPid()   
//----------------------------------------------------------------------------
{
  return m_Pid;
}

void lhpOpExporterAnsysInputFile::OnOK()
{
  mafString wildcard = "inp files (*.inp)|*.inp|All Files (*.*)|*.*";

  m_AnsysOutputFileNameFullPath = "";

  wxString f;
  f = mafGetSaveFile(m_FileDir,wildcard).c_str(); 
  if(!f.IsEmpty())
  {
    m_AnsysOutputFileNameFullPath = f;
    Write();
  }
 }

//----------------------------------------------------------------------------
void lhpOpExporterAnsysInputFile::OpStop(int result)
//----------------------------------------------------------------------------
{
  HideGui();
  mafEventMacro(mafEvent(this,result));        
}