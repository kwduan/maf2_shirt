/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: lhpOpBonematBatch.cpp,v $
Language:  C++
Date:      $Date: 2012-02-24 15:18:49 $
Version:   $Revision: 1.1.2.7 $
Authors:   Stefano Perticoni
==========================================================================
Copyright (c) 2001/2005 
CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/

#include "mafDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "lhpOpBonematBatch.h"
#include "lhpProceduralElements.h"

#include "wx/busyinfo.h"

#include "mafGUI.h"

#include "mafDecl.h"
#include "mafVMERoot.h"
#include "mafVMEMesh.h"
#include "mafString.h"
#include "mafAbsMatrixPipe.h"

#include <fstream>
#include <stdio.h>
#include <iostream>
#include <string>
#include <math.h>
#include <stdlib.h>

#include "vtkMAFSmartPointer.h"
#include "vtkUnstructuredGrid.h"
#include "vtkImageData.h"
#include "vtkDoubleArray.h"
#include "vtkDoubleArray.h"
#include "vtkGenericCell.h"
#include "vtkCellData.h"
#include "vtkPointData.h"
#include "vtkCellArray.h"
#include "vtkIntArray.h"
#include "vtkFloatArray.h"
#include "vtkTransformFilter.h"
#include "vtkTransform.h"
#include "lhpOpBonemat.h"
#include "lhpOpExporterAnsysInputFile.h"
#include "lhpOpImporterAnsysCDBFile.h"
#include "medOpImporterVTK.h"
#include "lhpBuilderDecl.h"

//----------------------------------------------------------------------------
lhpOpBonematBatch::lhpOpBonematBatch(wxString label) :
mafOp(label)
//----------------------------------------------------------------------------
{
  m_OpType  = OPTYPE_OP;
  m_Canundo = false;
  m_InputPreserving = false;
  m_ConfigurationFileName = "";

  m_PythonExeFullPath = "python.exe_UNDEFINED";
  m_PythonwExeFullPath = "pythonw.exe_UNDEFINED";

}
//----------------------------------------------------------------------------
lhpOpBonematBatch::~lhpOpBonematBatch()
//----------------------------------------------------------------------------
{

}
//----------------------------------------------------------------------------
bool lhpOpBonematBatch::Accept(mafNode *node)
//----------------------------------------------------------------------------
{ 
  return true;
}
//----------------------------------------------------------------------------
mafOp* lhpOpBonematBatch::Copy()   
//----------------------------------------------------------------------------
{
  lhpOpBonematBatch *cp = new lhpOpBonematBatch(m_Label);
  return cp;
}
//----------------------------------------------------------------------------
// constants
//----------------------------------------------------------------------------
enum BONEMAT_ID
{
  ID_FIRST = MINID,
  ID_OPEN_CONFIGURATION_FILE,
  ID_EXECUTE,

};

//----------------------------------------------------------------------------
void lhpOpBonematBatch::CreateGui()
//----------------------------------------------------------------------------
{
  m_Gui = new mafGUI(this);
  m_Gui->Label(""); 

  m_Gui->Button(ID_OPEN_CONFIGURATION_FILE, "open batch run conf file");
  m_Gui->Divider(2);

  m_Gui->Button(ID_EXECUTE, "execute");

  m_Gui->Divider(2);

  m_Gui->OkCancel();
  m_Gui->Label("");
  m_Gui->Label("");
}

//----------------------------------------------------------------------------
void lhpOpBonematBatch::OpRun()   
//----------------------------------------------------------------------------
{
  // get python interpreters
  mafEvent eventGetPythonExe;
  eventGetPythonExe.SetSender(this);
  eventGetPythonExe.SetId(ID_REQUEST_PYTHON_EXE_INTERPRETER);
  mafEventMacro(eventGetPythonExe);

  if(eventGetPythonExe.GetString())
  {
    m_PythonExeFullPath.Erase(0);
    m_PythonExeFullPath = eventGetPythonExe.GetString()->GetCStr();
    m_PythonExeFullPath.Append(" ");
  }

  mafEvent eventGetPythonwExe;
  eventGetPythonwExe.SetSender(this);
  eventGetPythonwExe.SetId(ID_REQUEST_PYTHONW_EXE_INTERPRETER);
  mafEventMacro(eventGetPythonwExe);

  if(eventGetPythonwExe.GetString())
  {
    m_PythonwExeFullPath.Erase(0);
    m_PythonwExeFullPath = eventGetPythonwExe.GetString()->GetCStr();
    m_PythonwExeFullPath.Append(" ");
  }

  CreateGui();
  ShowGui();
}

//----------------------------------------------------------------------------
void lhpOpBonematBatch::OnEvent(mafEventBase *maf_event)
//----------------------------------------------------------------------------
{
  if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
  {
    switch(e->GetId())
    {
    case wxOK:
      {
        OpStop(OP_RUN_OK);
      }
      break;
    case wxCANCEL:
      OpStop(OP_RUN_CANCEL);
      break;


    case ID_OPEN_CONFIGURATION_FILE:
      {
        OpenConfigurationFileFromGUI();
      }  
      break;


    case ID_EXECUTE	:
      {
        Execute();

      }
      break;
    default:
      mafEventMacro(*e);
      break;
    }
  }
}
//----------------------------------------------------------------------------
void lhpOpBonematBatch::OpStop(int result)
//----------------------------------------------------------------------------
{
  HideGui();
  mafEventMacro(mafEvent(this,result));        
}

//----------------------------------------------------------------------------
const char* lhpOpBonematBatch::GetConfigurationFileName()
//----------------------------------------------------------------------------
{
  return m_ConfigurationFileName.GetCStr();
}

//----------------------------------------------------------------------------
void lhpOpBonematBatch::SetConfigurationFileName(const char* name)
//----------------------------------------------------------------------------
{
  m_ConfigurationFileName = name;  
}
//----------------------------------------------------------------------------
int lhpOpBonematBatch::OpenConfigurationFileFromGUI()
//----------------------------------------------------------------------------
{
  mafString wildcconf = "txt (*.txt)|*.txt";

  std::string initial = mafGetApplicationDirectory().c_str();

  std::string returnString = mafGetOpenFile("", wildcconf);

  if (returnString == "")
  {
    return MAF_ERROR;
  }

  m_ConfigurationFileName = returnString.c_str();  

  return MAF_OK;
}

int lhpOpBonematBatch::Execute()
{
  int res = MAF_ERROR;

  // DEBUG
  std::ostringstream stringStream;
  stringStream << "Loading configuration file: " << m_ConfigurationFileName << std::endl;
  mafLogMessage(stringStream.str().c_str());

  int result = MAF_ERROR;
  result = LoadBonematBatchConfigurationFile(m_ConfigurationFileName, m_inCDBVector, m_inVTKVector, m_inBonematConfVector, m_outAnsysInp );

  if (result == MAF_ERROR)
  {
	 return result;
  }

  mafLogMessage("done!");
  mafLogMessage("");

  int numBonematRuns = m_inCDBVector.size(); 

  long progress = 0;
  mafEventMacro(mafEvent(this,PROGRESSBAR_SHOW));

  for (int i = 0; i < numBonematRuns; i++) 
  {
    long progress = (int)(i * 100 / numBonematRuns);
    mafEventMacro(mafEvent(this,PROGRESSBAR_SET_VALUE, progress));

    // DEBUG
    std::ostringstream stringStream;
    stringStream << std::endl;
    stringStream << "-----------------" << std::endl;
    stringStream << "Bonemat Run " << i << std::endl;
    stringStream << "-----------------" << std::endl;
    stringStream << "Input CDB: " << m_inCDBVector[i] << std::endl;
    stringStream << "Input VTK: " << m_inVTKVector[i] << std::endl;
    stringStream << "Input Bonemat Configuration File: " << m_inBonematConfVector[i] << std::endl;
	stringStream << "-----------------" << std::endl;
	stringStream << "Running....." << std::endl;
    mafLogMessage(stringStream.str().c_str());  

    this->RunBonemat(m_PythonExeFullPath, m_inCDBVector[i].c_str() , m_inVTKVector[i].c_str() , m_inBonematConfVector[i].c_str() , m_outAnsysInp[i].c_str());
    
    std::ostringstream stringStream2;
	stringStream2 << std::endl;
	stringStream2 << "-----------------" << std::endl;
    stringStream2 << "Finished run: " << i << std::endl;
	assert(wxFileExists(m_outAnsysInp[i].c_str()));
	stringStream2 << "Output Ansys Inp file generated: " << m_outAnsysInp[i] << std::endl;
	stringStream2 << "-----------------" << std::endl;
	stringStream2 << std::endl;
    mafLogMessage(stringStream2.str().c_str());  
  }

  mafEventMacro(mafEvent(this,PROGRESSBAR_HIDE));

  return MAF_OK;
}

int lhpOpBonematBatch::LoadBonematBatchConfigurationFile( const char * configurationFileName, std::vector<std::string> &inCDBVector, std::vector<std::string> &inVTKVector, std::vector<std::string> &inBnematConfVector, std::vector<std::string> &outAnsysInp )
{

  inCDBVector.clear();
  inVTKVector.clear();
  inBnematConfVector.clear();
  outAnsysInp.clear();

  std::ifstream inputFile(configurationFileName, std::ios::in);

  if (inputFile.fail()) {
    std::cerr << "Error opening " << configurationFileName << "\n";
    assert(false);
    return MAF_ERROR;
  }

  mafString LoadConfigurationFileCacheFileName = configurationFileName;
  LoadConfigurationFileCacheFileName.Append(".Load.cache");

  std::ofstream LoadConfigurationFileCache(LoadConfigurationFileCacheFileName.GetCStr(), std::ios::out);

  if (LoadConfigurationFileCache.fail())
  {
    // 		if (GetTestMode() == false)
    // 		{
    // 
    // 			wxMessageBox("Error creating configuration file cache");
    // 		}
    assert(false);
    return MAF_ERROR;
  }


  std::string buf;
  while(getline(inputFile, buf)) 
  {
    // Find marker at start of line:finish
    size_t pos = buf.find('#');
    if(pos  == 0) 
    {
      // skip line    
    }
    else
    { 
      LoadConfigurationFileCache << buf << std::endl;
    }
  }

  LoadConfigurationFileCache.close();
  //<< std::endl;

  std::ifstream inputFileFromCache(LoadConfigurationFileCacheFileName.GetCStr(), std::ios::in);

  if (inputFileFromCache.fail()) {
	std::cerr << "Error opening " << LoadConfigurationFileCacheFileName.GetCStr() << "\n";
    assert(false);
    return MAF_ERROR;
  }

  std::string cache;
  std::vector<std::string> cacheVector;

  while (  inputFileFromCache >> cache) // << std::endl;
  {
    std::cout << cache << std::endl;
    cacheVector.push_back(cache);  
  }


  assert(cacheVector.size() % 4 == 0);

  int numberOfBonematRuns = cacheVector.size() / 4;


  for (int i = 0; i < numberOfBonematRuns; i++) 
  {
    inCDBVector.push_back(cacheVector[4*i]);
    inVTKVector.push_back(cacheVector[4*i+1]);
    inBnematConfVector.push_back(cacheVector[4*i+2]);
    outAnsysInp.push_back(cacheVector[4*i+3]);    
  }  

  return MAF_OK;
}


void lhpOpBonematBatch::RunBonemat( mafString &pythonExeFullPath, wxString inCDBFileName, wxString inVTKVolumeFileName, wxString inputBonematConfigurationFileName, wxString outputAnsysFileName )
{
	//------------------
	// import Ansys CDB as VTK Unstructured grid
	//------------------

	std::ostringstream waitImportAnsysCDBLog;
	waitImportAnsysCDBLog << std::endl;
	waitImportAnsysCDBLog << "//------------------------------------------------------" << std::endl;
	waitImportAnsysCDBLog << "// Importing Ansys CDB " << inCDBFileName.c_str() <<  " as VTK Unstructured Grid .... "  << std::endl;
	waitImportAnsysCDBLog << "//------------------------------------------------------"  << std::endl;
	mafLogMessage(waitImportAnsysCDBLog.str().c_str());


	wxBusyInfo *waitImportAnsysCDB;
	if(!m_TestMode)
	{
		waitImportAnsysCDB = new wxBusyInfo("Please wait importing Ansys CDB Mesh...");
	}

	lhpOpImporterAnsysCDBFile *ansysCDBImporter=new lhpOpImporterAnsysCDBFile();
	ansysCDBImporter->TestModeOn();

	assert(wxFileExists(inCDBFileName) == true); 

	ansysCDBImporter->SetPythonExeFullPath(pythonExeFullPath.GetCStr());
	ansysCDBImporter->SetFileName(inCDBFileName);
	ansysCDBImporter->Import();

	mafVMEMesh *inputVMEMesh=mafVMEMesh::SafeDownCast(ansysCDBImporter->GetOutput());

	assert(inputVMEMesh!=NULL);
	inputVMEMesh->Modified();
	inputVMEMesh->Update();

	vtkUnstructuredGrid *inputVTKUnstructuredGrid=vtkUnstructuredGrid::SafeDownCast(inputVMEMesh->GetOutput()->GetVTKData());

	assert(inputVTKUnstructuredGrid!=NULL);

	inputVTKUnstructuredGrid->Modified();
	inputVTKUnstructuredGrid->Update();

	if(!m_TestMode)
	{
		delete waitImportAnsysCDB;
	}

	//------------------
	// import VTK volume (Rectilinear grid or structured points)
	//------------------

	std::ostringstream waitImportVTKVolumeLog;
	waitImportVTKVolumeLog << std::endl;
	waitImportVTKVolumeLog << "//------------------------------------------------------"  << std::endl;;
	waitImportVTKVolumeLog << "// Importing VTK Volume " << inVTKVolumeFileName.c_str() << " .... " << std::endl;
	waitImportVTKVolumeLog << "//------------------------------------------------------"  << std::endl;;
	mafLogMessage(waitImportVTKVolumeLog.str().c_str());

	wxBusyInfo *waitImportVTKVolume;
	if(!m_TestMode)
	{
		waitImportVTKVolume = new wxBusyInfo("Please wait importing VTK Volume...");
	}

	medOpImporterVTK *importerVTK=new medOpImporterVTK();
	importerVTK->TestModeOn();

	assert(wxFileExists(inVTKVolumeFileName)); 

	importerVTK->SetFileName(inVTKVolumeFileName);
	importerVTK->ImportVTK();
	mafVMEVolumeGray *inputCTVolume=mafVMEVolumeGray::SafeDownCast(importerVTK->GetOutput());

	assert(inputCTVolume!=NULL);
	inputCTVolume->Modified();
	inputCTVolume->Update();

	std::ostringstream importVTKVolumeOkLog;
	importVTKVolumeOkLog << std::endl;;
	importVTKVolumeOkLog << " VTK volume imported correctly " << std::endl;
	importVTKVolumeOkLog << std::endl;;
	mafLogMessage(importVTKVolumeOkLog.str().c_str());


	assert(inputCTVolume);
	assert(inputVMEMesh);

	if(!m_TestMode)
	{
		delete waitImportVTKVolume;
	}

	//------------------
	// run Bonemat
	//------------------


	std::ostringstream waitExecuteBonematLog;
	waitExecuteBonematLog << std::endl;
	waitExecuteBonematLog << "//------------------------------------------------------"  << std::endl;;
	waitExecuteBonematLog << "// Executing Bonemat ... " << std::endl;
	waitExecuteBonematLog << "//------------------------------------------------------"  << std::endl;;
	mafLogMessage(waitExecuteBonematLog.str().c_str());

	wxBusyInfo *waitExecuteBonemat;
	if(!m_TestMode)
	{
		waitExecuteBonemat = new wxBusyInfo("Please wait for Bonemat Execution...");
	}

	// create the bonemat operation
	lhpOpBonemat *opBonemat = new lhpOpBonemat("op bonemat");
	opBonemat->TestModeOn();
	opBonemat->SetInput(inputVMEMesh);
	opBonemat->SetSourceVolume(inputCTVolume);
	opBonemat->LoadConfigurationFile(inputBonematConfigurationFileName); 
	opBonemat->SetFrequencyFileName("frequencyFileToBeDiscarded.txt");

	// execute bonemat
	int result = opBonemat->Execute();

	assert(result == MAF_OK);

	if(!m_TestMode)
	{
		delete waitExecuteBonemat;
	}

	//------------------
	// export the bonemat mesh as ansys inp file
	//------------------

	std::ostringstream waitExportBonematMeshAsAnsysInpLog;
	waitExportBonematMeshAsAnsysInpLog << std::endl;
	waitExportBonematMeshAsAnsysInpLog << "//------------------------------------------------------"  << std::endl;;
	waitExportBonematMeshAsAnsysInpLog << "// Exporting Bonematted Mesh as " << outputAnsysFileName.c_str()  << " Ansys Inp File .... " << std::endl;
	waitExportBonematMeshAsAnsysInpLog << "//------------------------------------------------------"  << std::endl;;
	mafLogMessage(waitExportBonematMeshAsAnsysInpLog.str().c_str());

	wxBusyInfo *waitExportBonematMeshAsAnsysInp;
	if(!m_TestMode)
	{
		waitExportBonematMeshAsAnsysInp = new wxBusyInfo("Please wait for Bonemat Mesh export to Ansys Inp...");
	}

	lhpOpExporterAnsysInputFile *opExportAnsysInp = new lhpOpExporterAnsysInputFile("op export ansys inp");
	waitExportBonematMeshAsAnsysInpLog << std::endl;
	opExportAnsysInp->SetInput(opBonemat->GetOutput());
	opExportAnsysInp->SetOutputFileName(outputAnsysFileName);	
	opExportAnsysInp->SetPythonExeFullPath(pythonExeFullPath);
	opExportAnsysInp->TestModeOn();
	result = opExportAnsysInp->Write();


	assert(result == MAF_OK);

	assert(wxFileExists(outputAnsysFileName));

	// clean up
	mafDEL(importerVTK);
	mafDEL(ansysCDBImporter);
	mafDEL(opBonemat);
	mafDEL(opExportAnsysInp);

	if(!m_TestMode)
	{
		delete waitExportBonematMeshAsAnsysInp;
	}

}
