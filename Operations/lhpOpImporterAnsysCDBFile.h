/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: lhpOpImporterAnsysCDBFile.h,v $
Language:  C++
Date:      $Date: 2010-11-23 16:50:26 $
Version:   $Revision: 1.1.1.1.2.3 $
Authors:   Daniele Giunchi , Stefano Perticoni
==========================================================================
Copyright (c) 2002/2004
CINECA - Interuniversity Consortium (www.cineca.it) 
=========================================================================*/

#ifndef __lhpOpImporterAnsysCDBFile_H__
#define __lhpOpImporterAnsysCDBFile_H__

//----------------------------------------------------------------------------
// Include :
//----------------------------------------------------------------------------
#include "mafOp.h"
#include "lhpOperationsDefines.h"

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class mafVME;
class mafVMEMesh;
class mafEvent;

//----------------------------------------------------------------------------
// lhpOpImporterAnsysCDBFile :
//----------------------------------------------------------------------------
/** 
Importer for Ansys CDB Input files
*/
class LHP_OPERATIONS_EXPORT lhpOpImporterAnsysCDBFile : public mafOp
{
public:

	lhpOpImporterAnsysCDBFile(const wxString &label = "MeshImporter");
	~lhpOpImporterAnsysCDBFile(); 

	mafTypeMacro(lhpOpImporterAnsysCDBFile, mafOp);

	virtual void OnEvent(mafEventBase *maf_event);

	mafOp* Copy();

	/** Return true for the acceptable vme type. */
	bool Accept(mafNode *node);

	/** Set the input Ansys CDB file */
	void SetFileName(const char *ansysInputFileNameFullPath) {m_AnsysInputFileNameFullPath = ansysInputFileNameFullPath;};

	/** Set the python interpreter that will be used to execute embedded python scripts; 
	to be used if the op is instantiated without gui */
	void SetPythonExeFullPath(mafString pythonExeFullPath) {m_PythonExeFullPath = pythonExeFullPath;};
	
	/** Import the mesh*/
	int Import();

	/** Get the python interpreter used to execute embedded python scripts */
	mafString GetPythonExeFullPath() {return m_PythonExeFullPath;};

	/** Get the input Ansys CDB file */
	wxString GetFileName() {return m_AnsysInputFileNameFullPath.c_str();};

	/** Builds operation's interface. */
	void OpRun();

	/** Return the "pid" of the wxExecute() */
	long GetPid();

protected:
	/** Create the dialog interface for the importer. */
	virtual void CreateGui();  

	wxString m_FileDir;
	wxString m_AnsysInputFileNameFullPath;

	wxString m_AnsysPythonImporterFullPathFileName;

	int m_ImporterType;
	mafVMEMesh *m_ImportedVmeMesh;

	mafString m_CacheDir;

	/** Nodes file name*/
	wxString m_NodesFileName;

	/** Elements file name*/
	wxString m_ElementsFileName;

	/** Materials file name*/
	wxString m_MaterialsFileName;

	long m_Pid;

	mafString m_PythonExeFullPath; //>python  executable
	mafString m_PythonwExeFullPath; //>pythonw  executable

};
#endif
