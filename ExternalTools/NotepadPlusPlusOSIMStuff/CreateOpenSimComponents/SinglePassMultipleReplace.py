from __future__ import nested_scopes

import re 
import csv
import os
import sys

#
# You may combine both the dictionnary and search-and-replace
# into a single object using a 'callable' dictionary wrapper
# which can be directly used as a callback object.
# 

# In Python 2.2+ you may extend the 'dictionary' built-in class instead
from UserDict import UserDict 
class Xlator(UserDict):

  """ An all-in-one multiple string substitution class """ 

  def _make_regex(self): 

    """ Build a regular expression object based on the keys of
    the current dictionary """

    return re.compile("(%s)" % "|".join(map(re.escape, self.keys()))) 

  def __call__(self, mo): 
    
    """ This handler will be invoked for each regex match """

    # Count substitutions
    self.count += 1 # Look-up string

    return self[mo.string[mo.start():mo.end()]]

  def xlat(self, text): 

    """ Translate text, returns the modified text. """ 

    # Reset substitution counter
    self.count = 0 

    # Process text
    return self._make_regex().sub(self, text)


if __name__ == "__main__": 

########################################################################
#
#    Configure a file through a dictionary
#
########################################################################
#    
# Usage:
# python  SinglePassMultipleReplace.py 
#        OSIM_AddBlockToModel_CodeToConfigure.cpp.in (input file to configure) 
#        OSIM_AddBlockToModel_Dictionary.txt  (dictionary)
#        OSIM_AddBlockToModel.cpp (output configured file)
#
# Number of changed strings will be returned
    
    inputFileNameToConfigure = "OSIM_AddBlockToModel_CodeToConfigure.cpp.in"
    inputDictionaryFileName = "OSIM_AddBlockToModel_Dictionary.txt"
    outputConfiguredFileName = "OSIM_AddBlockToModel.cpp"
                
    numberOfArguments =  len(sys.argv)
    if (numberOfArguments == 1):# first argument is SinglePassMultipleReplace.py    
        pass
    elif (numberOfArguments == 4):
        inputFileNameToConfigure = sys.argv[1]
        inputDictionaryFileName = sys.argv[2]
        outputConfiguredFileName = sys.argv[3]
        
        print "pythonCode (argv[0]): " + sys.argv[0]
        print "inputFileToConfigure (argv[1]): " + sys.argv[1]
        print "dictionary (argv[2]): " + sys.argv[2]
        print "outputFile (argv[3]): " + sys.argv[3]
  
    ###########################################################
    #       
    # Input 2 : Dictionary to configure input file ("OSIM_AddBlockToModel_Dictionary.txt")
    #
    ###########################################################
  
    assert(os.path.exists(inputDictionaryFileName))
            
    inputDictionary = {}
    for key, val in csv.reader(open(inputDictionaryFileName,"rU")):
        inputDictionary[key] = val
    
    ###########################################################
    #       
    # Input 1 : File .cpp.in da configurare ("OSIM_AddBlockToModel_CodeToConfigure.cpp.in")
    #
    ###########################################################
    
    assert(os.path.exists(inputFileNameToConfigure))
    
    inFile = open(inputFileNameToConfigure,"r")
    text = inFile.read()
    inFile.close()
    
    inputTextToConfigure = text;
    
    xlat = Xlator(inputDictionary)
    configuredText =  xlat.xlat(inputTextToConfigure)

    print "#########################################################"   
    print "            Changed %d thing(s)" % xlat.count 
    print "#########################################################"
    
    ###########################################################
    #       
    # Output : File .cpp configurato da aprire in notepad++
    #
    ###########################################################
    
    outFile = open(outputConfiguredFileName,"w")
    outFile.write(configuredText);
    outFile.close()

