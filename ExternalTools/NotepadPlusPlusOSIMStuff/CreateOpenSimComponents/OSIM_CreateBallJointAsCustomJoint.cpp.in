
//------------------------------------------------------
//
// Create a new ball joint as custom joint between
// parent: [+++ParentSurfaceVMEName+++]
// and
// child: [+++ChildSurfaceVMEName+++]
//
//------------------------------------------------------

		// articular centre position from NMSB
		Vec3 locationInParentBody[+++ParentSurfaceVMEName+++];
		locationInParentBody[+++ParentSurfaceVMEName+++][0] = [+++ArticularCentreInParent_X+++] / 1000;
		locationInParentBody[+++ParentSurfaceVMEName+++][1] = [+++ArticularCentreInParent_Y+++] / 1000;
		locationInParentBody[+++ParentSurfaceVMEName+++][2] = [+++ArticularCentreInParent_Z+++] / 1000;

		// rotation axis orientation from NMSB
		Vec3 orientationInParentBody[+++ParentSurfaceVMEName+++];
		orientationInParentBody[+++ParentSurfaceVMEName+++][0] = [+++RotationAxisEulerAnglesInRadiansInParent_X+++];
		orientationInParentBody[+++ParentSurfaceVMEName+++][1] = [+++RotationAxisEulerAnglesInRadiansInParent_Y+++];
		orientationInParentBody[+++ParentSurfaceVMEName+++][2] = [+++RotationAxisEulerAnglesInRadiansInParent_Z+++];

		// articular centre position from NMSB
		Vec3 locationInChildBody[+++ChildSurfaceVMEName+++];
		locationInChildBody[+++ChildSurfaceVMEName+++][0] = [+++ArticularCentreInChild_X+++] / 1000;
		locationInChildBody[+++ChildSurfaceVMEName+++][1] = [+++ArticularCentreInChild_Y+++] / 1000;
		locationInChildBody[+++ChildSurfaceVMEName+++][2] = [+++ArticularCentreInChild_Z+++] / 1000;

		// rotation axis euler angles from NMSB
		Vec3 orientationInChildBody[+++ChildSurfaceVMEName+++];
		orientationInChildBody[+++ChildSurfaceVMEName+++][0] = [+++RotationAxisEulerAnglesInRadiansInChild_X+++];
		orientationInChildBody[+++ChildSurfaceVMEName+++][1] = [+++RotationAxisEulerAnglesInRadiansInChild_Y+++];
		orientationInChildBody[+++ChildSurfaceVMEName+++][2] = [+++RotationAxisEulerAnglesInRadiansInChild_Z+++];

		///////////////////////////////
		// Define customJoint coordinates and axes for custom joint's spatial transform.
		// A spatial transform describes the spatial (6-dof) permissible motion
		// between two joint frames. A SpatialTransform is composed of 6 transform axes
		// (X,Y,Z body fixed rotations & X, Y, Z translations of the child joint
		// frame in the parent, in this order). An unspecified axis has no coordinates
		// associate and is a constant 0 value.
		SpatialTransform jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++];

		Array<string> xRotation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]("xRot_[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]", 1, 1);
		Array<string> yRotation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]("yRot_[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]", 1, 1);
		Array<string> zRotation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]("zRot_[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]", 1, 1);

		// child body rotation X
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][0].setCoordinateNames(xRotation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][0].setFunction(new LinearFunction());

		// child body rotation Y
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][1].setCoordinateNames(yRotation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][1].setFunction(new LinearFunction());

		// child body rotation Z
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][2].setCoordinateNames(zRotation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][2].setFunction(new LinearFunction());

		// child body translation X
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][3].setFunction(new Constant());

		// child body translation Y
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][4].setFunction(new Constant());

		// child body translation Z
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][5].setFunction(new Constant());

		// create custom customJoint joint
		CustomJoint *customJoint[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++] = new CustomJoint(
		"[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]",
		*[+++ParentSurfaceVMEName+++] ,
		locationInParentBody[+++ParentSurfaceVMEName+++] ,
		orientationInParentBody[+++ParentSurfaceVMEName+++],
		*[+++ChildSurfaceVMEName+++],
		locationInChildBody[+++ChildSurfaceVMEName+++],
		orientationInChildBody[+++ChildSurfaceVMEName+++],
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);

		// Set the angle and position ranges for the customJoint
		double rotationRange[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][2] = {-SimTK::Pi, SimTK::Pi};
		customJoint[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]->getCoordinateSet()[0].setRange(rotationRange[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		customJoint[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]->getCoordinateSet()[1].setRange(rotationRange[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		customJoint[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]->getCoordinateSet()[2].setRange(rotationRange[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);

//-----------------END SNIPPET--------------
