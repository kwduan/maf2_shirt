
		//------------------------------------------
		// Save the model to a file
		//------------------------------------------
		// BEWARE do not change the output model name since
		// "outputModel.osim" is used by the NMSBuilder application
		// in order to find and open the correct output file.
		// If you want to save outputModel to a different file use the 
		// model editor File->SaveAs facility
		osimModel.print("outputModel.osim");
	}
    catch (OpenSim::Exception ex)
    {
        std::cout << ex.getMessage() << std::endl;
        return 1;
    }
    catch (std::exception ex)
    {
        std::cout << ex.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        std::cout << "UNRECOGNIZED EXCEPTION" << std::endl;
        return 1;
    }

    std::cout << "OpenSim example completed successfully.\n";
	return 0;
}
