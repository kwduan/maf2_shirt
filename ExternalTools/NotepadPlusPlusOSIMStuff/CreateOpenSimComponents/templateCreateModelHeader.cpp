#include <OpenSim/OpenSim.h>

using namespace OpenSim;
using namespace SimTK;
using namespace std;

int main()
{
	try 
	{
		// Create an OpenSim model and set its name
		Model osimModel;
		osimModel.setName("outputModel");

		// Get a reference to the model's ground body
		OpenSim::Body& ground = osimModel.getGroundBody();

	