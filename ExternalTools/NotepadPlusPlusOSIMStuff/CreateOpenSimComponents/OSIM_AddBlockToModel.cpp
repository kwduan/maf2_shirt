
//---------------------------------------------------
// R_Thigh_FEMUR OpenSim Block Body featuring the following 
// inertial parameters from NMSBuilder:
//
// - Mass
// - Inertia
// - Centre of Mass
//
// Target model: osimModel
//------------------------------------------------------

// Specify the mass
double blockMassR_Thigh_FEMUR = 0.787632 ; // from NMSB

// Specify the centre of mass
Vec3 blockMassCenterR_Thigh_FEMUR;
blockMassCenterR_Thigh_FEMUR[0] = -103.7; // from NMSB mm to OpenSim meters
blockMassCenterR_Thigh_FEMUR[1] = 0.324983; // from NMSB mm to OpenSim meters
blockMassCenterR_Thigh_FEMUR[2] = 423.511; // from NMSB mm to OpenSim meters
// Specify the inertia
Inertia blockInertiaR_Thigh_FEMUR; // from NMSBuilder to OpenSim measures units
blockInertiaR_Thigh_FEMUR.setInertia( 21013.6 / 1000000.00, 20685.3 / 1000000.00, 981.648 / 1000000.00,
-218.392 / 1000000.00, -1230.46 / 1000000.00, 3171.1 / 1000000.00 ); 
						
// Create a new block body with specified properties (from NMSB)
OpenSim::Body *R_Thigh_FEMUR = new OpenSim::Body("R_Thigh FEMUR", blockMassR_Thigh_FEMUR, blockMassCenterR_Thigh_FEMUR, blockInertiaR_Thigh_FEMUR);

// Add display geometry to the block to visualize in the GUI (from NMSB)
R_Thigh_FEMUR->addDisplayGeometry("R_Thigh FEMUR.vtp");

Vec3 scaleGeometryR_Thigh_FEMUR;
scaleGeometryR_Thigh_FEMUR[0] = 0.001;
scaleGeometryR_Thigh_FEMUR[1] = 0.001;
scaleGeometryR_Thigh_FEMUR[2] = 0.001;

R_Thigh_FEMUR->scale(scaleGeometryR_Thigh_FEMUR);


// Add the block body to the model
osimModel.addBody(R_Thigh_FEMUR);

//-----------------END SNIPPET--------------
