
//------------------------------------------------------
//
// Create a new free joint as custom joint between
// parent: [+++ParentSurfaceVMEName+++]
// and
// child: [+++ChildSurfaceVMEName+++]
//
//------------------------------------------------------

		// articular centre position from NMSB
		Vec3 locationInParentBody[+++ParentSurfaceVMEName+++];
		locationInParentBody[+++ParentSurfaceVMEName+++][0] = 0;
		locationInParentBody[+++ParentSurfaceVMEName+++][1] = 0;
		locationInParentBody[+++ParentSurfaceVMEName+++][2] = 0;

		// rotation axis orientation from NMSB
		Vec3 orientationInParentBody[+++ParentSurfaceVMEName+++];
		orientationInParentBody[+++ParentSurfaceVMEName+++][0] = 0;
		orientationInParentBody[+++ParentSurfaceVMEName+++][1] = 0;
		orientationInParentBody[+++ParentSurfaceVMEName+++][2] = 0;

		// articular centre position from NMSB
		Vec3 locationInChildBody[+++ChildSurfaceVMEName+++];
		locationInChildBody[+++ChildSurfaceVMEName+++][0] = 0;
		locationInChildBody[+++ChildSurfaceVMEName+++][1] = 0;
		locationInChildBody[+++ChildSurfaceVMEName+++][2] = 0;

		// rotation axis euler angles from NMSB
		Vec3 orientationInChildBody[+++ChildSurfaceVMEName+++];
		orientationInChildBody[+++ChildSurfaceVMEName+++][0] = 0;
		orientationInChildBody[+++ChildSurfaceVMEName+++][1] = 0;
		orientationInChildBody[+++ChildSurfaceVMEName+++][2] = 0;

		///////////////////////////////
		// Define customJoint coordinates and axes for custom joint's spatial transform.
		// A spatial transform describes the spatial (6-dof) permissible motion
		// between two joint frames. A SpatialTransform is composed of 6 transform axes
		// (X,Y,Z body fixed rotations & X, Y, Z translations of the child joint
		// frame in the parent, in this order). An unspecified axis has no coordinates
		// associate and is a constant 0 value.
		SpatialTransform jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++];

		Array<string> xRotation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]("xRot_[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]", 1, 1);
		Array<string> yRotation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]("yRot_[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]", 1, 1);
		Array<string> zRotation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]("zRot_[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]", 1, 1);
		Array<string> xTranslation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]("xTr_[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]", 1, 1);
		Array<string> yTranslation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]("yTr_[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]", 1, 1);
		Array<string> zTranslation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]("zTr_[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]", 1, 1);

		// child body rotation X
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][0].setCoordinateNames(xRotation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][0].setFunction(new LinearFunction());

		// child body rotation Y
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][1].setCoordinateNames(yRotation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][1].setFunction(new LinearFunction());

		// child body rotation Z
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][2].setCoordinateNames(zRotation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][2].setFunction(new LinearFunction());

		// child body translation X
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][3].setCoordinateNames(xTranslation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][3].setFunction(new LinearFunction());

		// child body translation Y
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][4].setCoordinateNames(yTranslation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][4].setFunction(new LinearFunction());

		// child body translation Z
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][5].setCoordinateNames(zTranslation[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][5].setFunction(new LinearFunction());

		// create custom customJoint joint
		CustomJoint *customJoint[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++] = new CustomJoint(
		"[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]",
		[+++ParentSurfaceVMEName+++] ,
		locationInParentBody[+++ParentSurfaceVMEName+++] ,
		orientationInParentBody[+++ParentSurfaceVMEName+++],
		*[+++ChildSurfaceVMEName+++],
		locationInChildBody[+++ChildSurfaceVMEName+++],
		orientationInChildBody[+++ChildSurfaceVMEName+++],
		jointTransform[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);

		// Set the angle and position ranges for the customJoint
		double rotationRange[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][2] = {-SimTK::Pi, SimTK::Pi};
		customJoint[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]->getCoordinateSet()[0].setRange(rotationRange[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		customJoint[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]->getCoordinateSet()[1].setRange(rotationRange[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		customJoint[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]->getCoordinateSet()[2].setRange(rotationRange[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);

		double translationRange[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++][2] = {-10, 10};
		customJoint[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]->getCoordinateSet()[3].setRange(translationRange[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		customJoint[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]->getCoordinateSet()[4].setRange(translationRange[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);
		customJoint[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]->getCoordinateSet()[5].setRange(translationRange[+++ChildSurfaceVMEName+++]To[+++ParentSurfaceVMEName+++]);

//-----------------END SNIPPET--------------
