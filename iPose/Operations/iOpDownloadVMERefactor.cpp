/*=========================================================================
Program:   iPose
Module:    $RCSfile: iOpDownloadVMERefactor.cpp,v $
Language:  C++
Date:      $Date: 2011-04-13 10:44:28 $
Version:   $Revision: 1.1.1.1.2.17 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2012
SCS s.r.l. - BioComputing Competence Centre ( www.scsolutions.it - www.b3c.it)
=========================================================================*/

#include "medDefines.h"
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iDecl.h"
#include "iOpDownloadVMERefactor.h"
#include "lhpBuilderDecl.h"
#include "lhpUploadDownloadVMEHelper.h"
#include "lhpUser.h"
#include "mafVMERoot.h"
#include "iAttributeWorkflow.h"
#include <fstream>
#include <wx\dir.h>

mafCxxTypeMacro(iOpDownloadVMERefactor);

iOpDownloadVMERefactor::iOpDownloadVMERefactor(wxString label, int fromSandbox) :
lhpOpDownloadVMERefactor(label,fromSandbox)
{ 

}

iOpDownloadVMERefactor::~iOpDownloadVMERefactor()
{

}

mafOp* iOpDownloadVMERefactor::Copy()
{
  /** return a copy of itself, needs to put dataVectorIterator into the undo stack */
  return new iOpDownloadVMERefactor(m_Label, m_FromSandbox);
}

void iOpDownloadVMERefactor::OpRun()
{

  //   m_ProcessCounter = 0;
  //   m_StopsCounter = 0;

  mafEventMacro(mafEvent(this, IPOSE_SAVE));

  mafEventMacro(mafEvent(this,IPOSE_MSF_NEW));

  mafEvent eGetRoot;
  eGetRoot.SetSender(this);
  eGetRoot.SetId(IPOSE_GET_ROOT);
  mafEventMacro(eGetRoot);

  this->SetInput(eGetRoot.GetVme());

  m_PythonInterpreterFullPath = m_UploadDownloadVMEHelper->GetPythonInterpreterFullPath();
  m_PythonwInterpreterFullPath = m_UploadDownloadVMEHelper->GetPythonwInterpreterFullPath();

  m_User = m_UploadDownloadVMEHelper->GetUser();
  m_ServiceURL = m_UploadDownloadVMEHelper->GetServiceURL(m_FromSandbox);
  if(m_ServiceURL.Equals("1"))
  {
    wxMessageBox("Error in GetServiceURL(). Please check your internet connection and retry operation.", wxMessageBoxCaptionStr, wxSTAY_ON_TOP | wxOK);
    OpStop(OP_RUN_CANCEL);
    return;
  }

  if(m_User->GetProxyFlag())
  {
    m_ProxyURL = m_User->GetProxyHost();
    m_ProxyPort = m_User->GetProxyPort();
    m_UploadDownloadVMEHelper->SaveConnectionConfigurationFile();
  }
  else
  {
    m_UploadDownloadVMEHelper->RemoveConnectionConfigurationFile();
  }

  int result = OP_RUN_CANCEL;
  m_NumOfDir = 0;

  mafString DebugPath = m_VMEUploaderDownloaderDirABSPath;
  DebugPath.Append("\\Debug.py");
  if (wxFileExists(DebugPath.GetCStr()))
  {
    std::ifstream debugFile;
    debugFile.open(DebugPath.GetCStr());
    if (!debugFile) 
    {
      mafLogMessage("Unable to open Debug.py file");
    }

    std::string isDebug;
    debugFile >> isDebug;
    int pos = isDebug.find_last_of('=');
    isDebug = isDebug.substr(pos+1);
    if (!isDebug.compare("1") || !isDebug.compare("True"))
    {
      m_DebugMode = true;
    }
    debugFile.close();
  }


  bool upToDate = false;
  upToDate = m_UploadDownloadVMEHelper->IsClientSoftwareVersionUpToDate();

  if(upToDate)
  {
    mafString directoryDownload = mafGetApplicationDirectory().c_str();
    directoryDownload << "/download";
    wxDir dir(directoryDownload.GetCStr());
    wxString dirName;

    bool start = dir.GetFirst(&dirName);
    m_NumOfDir =  start ? m_NumOfDir + 1 : m_NumOfDir;

    while(start && dir.GetNext(&dirName))
    {
      m_NumOfDir += 1;
    }

    mafString fileName = "";
    fileName = mafGetApplicationDirectory().c_str();
    fileName<<"/download/";
    fileName<<m_NumOfDir;

    while(wxDir::Exists(fileName.GetCStr()))
    {
      m_NumOfDir++;
      fileName = mafGetApplicationDirectory().c_str();
      fileName<<"/download/";
      fileName<<m_NumOfDir;
    }

    if(!wxDir::Exists(fileName.GetCStr()))
    {
      ::wxMkDir(fileName.GetCStr());
    }

    fileName<<"/Download";
    fileName<<m_NumOfDir;
    fileName<<".msf";

    mafEventMacro(mafEvent(this, MENU_FILE_SAVE,&fileName));


    mafEventMacro(mafEvent(this, MENU_FILE_SAVE));

    if (m_TestMode == true)
    {
      result = OP_RUN_OK;
    }
    else if (m_UseMockResourceFile)
    {
      result = OP_RUN_OK;
    }
    else if(this->SelectResourcesToDownloadByGUI() == MAF_OK)
    {
      result = OP_RUN_OK;
      
    }

    OpStop(result);
    return;

  }
  else
  {
    OpStop(result);
    return;
  }

}

void iOpDownloadVMERefactor::OpDo()
{
  Superclass::OpDo();


  mafEvent eGetRoot;
  eGetRoot.SetSender(this);
  eGetRoot.SetId(IPOSE_GET_ROOT);
  mafEventMacro(eGetRoot);

  mafVME *groupAttributes = FindVmeByTag(mafVMERoot::SafeDownCast(eGetRoot.GetVme()),TAG_GROUP_ATTRIBUTES);

  if (groupAttributes)
  {
    iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));
    if (attribute)
    {
      grid_element element;
      element.m_Name = attribute->GetPatientName();
      element.m_Height = attribute->GetPatientHeight();
      element.m_Weight = attribute->GetPatientWeight();
      element.m_Joint = attribute->GetJoint();
      element.m_Side = attribute->GetSide();
      element.m_Diagnosis = attribute->GetDiagnosis();

      attribute->SetCurrentStep(ID_STEP_MEASURE);
      groupAttributes->SetAttribute("Workflow",attribute);

      UpdateCache("",element,true,m_NumOfDir);

      mafEventMacro(mafEvent(this,MENU_FILE_SAVE));
    }
  }
  else
  {
    wxString dirName = mafGetApplicationDirectory().c_str();
    dirName<<"/download/";
    dirName<<m_NumOfDir;

    wxArrayString files;
    wxDir::GetAllFiles(dirName,&files);
    for (int i=0;i<files.size();i++)
    {
      ::wxRemoveFile(files[i]);
    }

    wxRmdir(dirName);

    mafEventMacro(mafEvent(this,IPOSE_MSF_NEW));

    mafEvent eGetRoot;
    eGetRoot.SetSender(this);
    eGetRoot.SetId(IPOSE_GET_ROOT);
    mafEventMacro(eGetRoot);

    this->SetInput(eGetRoot.GetVme());

    wxMessageBox( _("The data that has been downloaded are are missing and won't be imported") , _("Error"), wxOK | wxICON_ERROR  );
  }
}
