/*=========================================================================
Program:   iPose
Module:    $RCSfile: iOpAddProsthesisToDB.h,v $
Language:  C++
Date:      $Date: 2012-03-21 07:27:51 $
Version:   $Revision: 1.1.2.2 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2002/2004
SCS s.r.l. - BioComputing Competence Centre ( www.scsolutions.it - www.b3c.it)
=========================================================================*/

#ifndef __iOpAddProsthesisToDB_H__
#define __iOpAddProsthesisToDB_H__

//----------------------------------------------------------------------------
// Include :
//----------------------------------------------------------------------------
#include "mafOp.h"
#include "iDecl.h"
#include "iOperationsDefines.h"

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class mafString;

//----------------------------------------------------------------------------
// iOpAddProsthesisToDB :
//----------------------------------------------------------------------------
/** The user choose a zip file that contain prosthesis information.
The operation will import the file inside DataBase directory and update 
xml database file. */
class I_OPERATIONS_EXPORT iOpAddProsthesisToDB: public mafOp
{
public:
  /** Constructor. */
  iOpAddProsthesisToDB(wxString label = "Add Prosthesis to DB");
  /** Destructor. */
  ~iOpAddProsthesisToDB();

  /** RTTI macro. */
  mafTypeMacro(iOpAddProsthesisToDB, mafOp);

  /** Return a copy of the operation */
  /*virtual*/ mafOp* Copy();

  /** Return true for the acceptable vme type. */
  /*virtual*/ bool Accept(mafNode *node);

  /** Builds operation's interface. */
  /*virtual*/ void OpRun();

  /** Execute the operation. */
  /*virtual*/ void OpDo();

protected:
  /** This method is called at the end of the operation and result contain the wxOK or wxCANCEL. */
  /*virtual*/ void OpStop(int result);

  /** Open a zip archive and return the path of the msf file found inside the zip file. */
  mafString iOpAddProsthesisToDB::ZIPOpen(mafString file);

  /** Open the xml file containing the DB information. */
  int OpenDBProsthesis();

//   typedef struct DB
//   {
//     mafString m_Manufacture;
//     mafString m_Model;
//     mafString m_FileName;
//     mafString m_ProsthesisType;
//   }DB_PROSTHESIS_INFORMATION;
  std::vector<DB_PROSTHESIS_INFORMATION> m_DBProsthesisInformation;

  /** Add the new prosthesis to the DB. */
  int FillDBProsthesis(std::vector<DB_PROSTHESIS_INFORMATION> prosthesisToAdd);

};
#endif
