/*=========================================================================
  Program:   iPose
  Module:    $RCSfile: iOpROM.cpp,v $
  Language:  C++
  Date:      $Date: 2012-05-02 09:08:44 $
  Version:   $Revision: 1.1.2.9 $
  Authors:   Matteo Giacomoni
==========================================================================
  Copyright (c) 2001/2005 
  SCS s.r.l. - BioComputing Competence Centre ( www.scsolutions.it - www.b3c.it)
=========================================================================*/

#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iOpROM.h"
#include "wx/busyinfo.h"

#include "iDecl.h"
#include "medVisualPipeCollisionDetection.h"
#include "mafGUI.h"
#include "mafVME.h"
#include "mafView.h"
#include "mafSceneGraph.h"
#include "mafSceneNode.h"
#include "mafVMESurface.h"
#include "mafVMERoot.h"
#include "mafNodeIterator.h"
#include "mafTagArray.h"
#include "mafTagItem.h"
#include "mafTransform.h"
#include "mafVMEGroup.h"
#include "mmuDOMTreeErrorReporter.h"
#include "mafXMLElement.h"
#include "mmuXMLDOMElement.h"
#include "mafXMLString.h"
#include "mafGUICheckListBox.h"
#include "mafGUIPicButton.h"
#include "mafInteractorGenericMouse.h"
#include "mafGUIDialog.h"

#include "vtkMAFSmartPointer.h"
#include "vtkPolyData.h"
#include "vtkAppendPolyData.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkTransform.h"
#include "vtkCellData.h"
#include "vtkDoubleArray.h"
#include "vtkArrowSource.h"
#include "vtkMAFExtendedGlyph3D.h"
#include "vtkMEDCollisionDetectionFilter.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include <xercesc/framework/LocalFileInputSource.hpp>
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/sax/ErrorHandler.hpp>
#include "vtkMath.h"
#include "iAttributeROM.h"

//----------------------------------------------------------------------------
mafCxxTypeMacro(iOpROM);
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
iOpROM::iOpROM(wxString label) :
mafOp(label)
//----------------------------------------------------------------------------
{
  m_OpType  = OPTYPE_OP;
  m_Canundo = true;

  m_FemoralProsthesisOldParent = NULL;
  m_OldFemoralProsthisisPosition = NULL;
  m_OldAcetabularProsthisisPosition = NULL;
  m_AcetabularProsthesisGroup = NULL;
  m_FemoralProsthesisGroup = NULL;
  m_AcetabularSurface = NULL;
  m_FemoralSurface = NULL;
  m_Acetabular = NULL;
  m_Femoral = NULL;
  m_View = NULL;
  m_RefSys = NULL;
  m_CellsToExclude = NULL;
  m_MovementsList = NULL;
  m_MomementName = "";
  m_MovementAxisX = 0.0;
  m_MovementAxisY = 0.0;
  m_MovementAxisZ = 1.0;
  m_MovementAngleInf = 0.0;
  m_MovementAngleSup = 0.0;
  m_MovementStartFromPreviousStep = FALSE;
  m_MovementEnableVisualization = FALSE;
  m_MovementAddToDB = FALSE;
  m_Dialog = NULL;

  m_Orientation[0] = m_Orientation[1] = m_Orientation[2] = 0.0;
  
}
//----------------------------------------------------------------------------
iOpROM::~iOpROM( ) 
//----------------------------------------------------------------------------
{
}
//----------------------------------------------------------------------------
bool iOpROM::Accept(mafNode *node)
//----------------------------------------------------------------------------
{
  mafVMERoot *root = mafVMERoot::SafeDownCast(node->GetRoot());

  int numAcetabularSurface = 0;
  int numFemoralSurface = 0;
  int numAcetabularProsthesis = 0;
  int numFemoralProsthesis = 0;
  mafNodeIterator *iter = root->NewIterator();
  for (mafNode *Inode = iter->GetFirstNode(); Inode; Inode = iter->GetNextNode())
  {
    if (Inode->GetTagArray()->IsTagPresent(TAG_PROSTHESIS_TYPE_ACETABULAR))
    {
      m_AcetabularProsthesisGroup = mafVMEGroup::SafeDownCast(Inode);

      numAcetabularProsthesis++;
    }
    else if (Inode->GetTagArray()->IsTagPresent(TAG_PROSTHESIS_TYPE_FEMORAL))
    {
      m_FemoralProsthesisGroup= mafVMEGroup::SafeDownCast(Inode);

      numFemoralProsthesis++;
    }
    if (Inode->GetTagArray()->IsTagPresent(TAG_SURFACE_TYPE_ACETABULAR))
    {
      m_AcetabularSurface = mafVMESurface::SafeDownCast(Inode);

      numAcetabularSurface++;
    }
    else if (Inode->GetTagArray()->IsTagPresent(TAG_SURFACE_TYPE_FEMORAL))
    {
      m_FemoralSurface= mafVMESurface::SafeDownCast(Inode);

      numFemoralSurface++;
    }
  }

  iter->Delete();
  return (numAcetabularProsthesis > 0 && numAcetabularSurface > 0 && numFemoralProsthesis > 0 && numFemoralSurface > 0);
}
//----------------------------------------------------------------------------
mafOp* iOpROM::Copy()   
//----------------------------------------------------------------------------
{
  iOpROM *cp = new iOpROM(m_Label);
  return cp;
}
//----------------------------------------------------------------------------
void iOpROM::OpRun()   
//----------------------------------------------------------------------------
{

  if (CheckInputs() == MAF_ERROR)
  {
    OpStop(OP_RUN_CANCEL);
    return;
  }

  m_RefSys = m_AcetabularProsthesisGroup->GetOutput()->GetAbsMatrix();

  mafEvent eView;
  eView.SetSender(this);
  eView.SetId(ID_SHOW_HIP_ROM_VIEW);
  //Open the rom view
  mafEventMacro(eView);

  m_View = eView.GetView();

//   mafEvent eProsthesisInteraction;
//   eProsthesisInteraction.SetSender(this);
//   eProsthesisInteraction.SetId(PROSTHESIS_INTERACTION_IS_ACTIVE);
//   //ask if prosthesis interaction is active
//   mafEventMacro(eProsthesisInteraction);
// 
//   if (eProsthesisInteraction.GetBool())
//   {
//     mafEvent eSaveProsthesisPose;
//     eSaveProsthesisPose.SetSender(this);
//     eSaveProsthesisPose.SetId(SAVE_PROSTHESIS_POSE);
//     //Save current prosthesis pose
//     mafEventMacro(eSaveProsthesisPose);
//   }

  LoadRomData();

  CreateGui();
  ShowGui();

  GenerateStaticSurface();
  GenerateMobileSurface();

  /*m_View->VmeShow(m_Acetabular,true);*/
  m_View->VmeShow(m_Femoral,true);
  m_View->VmeShow(m_FemoralProsthesisTMP,true);
//   for (int i=0;i<m_FemoralProsthesis->GetNumberOfComponents();i++)
//   {
//   	m_View->VmeShow(m_FemoralProsthesis->GetComponent(i),true);
//   }

  medVisualPipeCollisionDetection *pipe;

  pipe = medVisualPipeCollisionDetection::SafeDownCast(m_View->GetSceneGraph()->Vme2Node(m_Femoral)->m_Pipe);
  pipe->SetScalarNameToExclude("SurfaceOrProsthesis");
  pipe->ShowSurfaceToCollideOn();
  pipe->SetSurfaceToCollide(m_Acetabular);

//   pipe = medVisualPipeCollisionDetection::SafeDownCast(m_View->GetSceneGraph()->Vme2Node(m_Acetabular)->m_Pipe);
//   pipe->ShowSurfaceToCollideOn();
//   pipe->SetSurfaceToCollide(m_Femoral);

  m_View->CameraUpdate();
}
//----------------------------------------------------------------------------
void iOpROM::OpStop(int result)
//----------------------------------------------------------------------------
{
  if (m_View)
  {
  	m_View->VmeShow(m_FemoralProsthesisTMP,false);
  }

  if (m_Acetabular && m_View)
  {
  	m_View->VmeShow(m_Acetabular,false);
  }

  if (m_Femoral && m_View)
  {
    m_View->VmeShow(m_Femoral,false);
  }

  if (m_OldAcetabularProsthisisPosition)
  {
    m_AcetabularProsthesisGroup->SetAbsMatrix(*m_OldAcetabularProsthisisPosition);
    m_AcetabularProsthesisGroup->GetOutput()->Update();
    m_AcetabularProsthesisGroup->Update();
  }

  if (m_OldFemoralProsthisisPosition)
  {
    m_FemoralProsthesisGroup->SetAbsMatrix(*m_OldFemoralProsthisisPosition);
    m_FemoralProsthesisGroup->GetOutput()->Update();
    m_FemoralProsthesisGroup->Update();
  }

#ifndef _DEBUG
  if (m_Acetabular)
  {
    m_Acetabular->ReparentTo(NULL);
  }
  if (m_Femoral)
  {
    m_Femoral->ReparentTo(NULL);
  }
#endif

  if (m_VmeWithRomInformation && result == OP_RUN_OK)
  {
    iAttributeROM *attribute = iAttributeROM::SafeDownCast(m_VmeWithRomInformation->GetAttribute("DataForROM"));
    iAttributeROM *attributeNew = iAttributeROM::New();
    attributeNew->DeepCopy(attribute);
    attributeNew->CleanMovements(m_FemoralProsthesisGroup->GetId());

    for (int i=0;i<m_DBMomementsInformation.size();i++)
    {
      attributeNew->AddMovement(m_FemoralProsthesisGroup->GetId(),m_DBMomementsInformation[i].m_Name.GetCStr(),m_DBMomementsInformation[i].m_AngleInf,m_DBMomementsInformation[i].m_AngleSup,m_DBMomementsInformation[i].m_AngleCollision,m_DBMomementsInformation[i].m_Axis[0],m_DBMomementsInformation[i].m_Axis[1],m_DBMomementsInformation[i].m_Axis[2],m_DBMomementsInformation[i].m_ShowMovement,m_DBMomementsInformation[i].m_StartFromPrevious,m_DBMomementsInformation[i].m_Passed);
    }

    m_VmeWithRomInformation->SetAttribute("DataForROM",attributeNew);

  }
  
  mafEventMacro(mafEvent(this,ID_CLOSE_HIP_ROM_VIEW));
  if (m_Gui)
  {
  	HideGui();
  }
  mafEventMacro(mafEvent(this,result));
}
//----------------------------------------------------------------------------
void iOpROM::CreateGui()
//----------------------------------------------------------------------------
{
  m_Gui = new mafGUI(this);

  m_MovementsList = m_Gui->CheckList(ID_MOVEMENTS_LIST,_("Movement"));

  FillMovementsList();

  wxBoxSizer *m_Sizer =  new wxBoxSizer( wxHORIZONTAL );
  mafGUIPicButton *down = new mafGUIPicButton(m_Gui,"ARROW_DOWN",ID_ARROW_DOWN, this,5);
  mafGUIPicButton *up = new mafGUIPicButton(m_Gui,"ARROW_UP",ID_ARROW_UP,this,5);

  m_Sizer->Add(down,0,wxALIGN_LEFT);
  m_Sizer->Add(up,0,wxALIGN_LEFT);
  m_Gui->Add(m_Sizer);


  m_Gui->Button(ID_ADD_MOVEMENT,_("Add a movement"));
  m_Gui->Button(ID_EDIT_MOVEMENT,_("Edit a movement"));
  m_Gui->Button(ID_RUN,_("Run"));

  m_Gui->Double(ID_ROTATE_X, _("Rotate X"), &m_Orientation[0]);
  m_Gui->TwoButtons(ID_ROTATE_X_DECREASE,ID_ROTATE_X_INCREASE,_("-"),_("+"));
  m_Gui->Double(ID_ROTATE_Y, _("Rotate Y"), &m_Orientation[1]);
  m_Gui->TwoButtons(ID_ROTATE_Y_DECREASE,ID_ROTATE_Y_INCREASE,_("-"),_("+"));
  m_Gui->Double(ID_ROTATE_Z, _("Rotate Z"), &m_Orientation[2]);
  m_Gui->TwoButtons(ID_ROTATE_Z_DECREASE,ID_ROTATE_Z_INCREASE,_("-"),_("+"));
  m_Gui->Divider();
  m_Gui->Button(ID_RESET, _("Reset"), _("Reset"));

  m_Gui->OkCancel();
  m_Gui->FitGui();
  m_Gui->Update();
}
//----------------------------------------------------------------------------
void iOpROM::OnEvent( mafEventBase *maf_event )
//----------------------------------------------------------------------------
{
  if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
  {
    switch(maf_event->GetId())
    {
    case ID_MOVEMENT_AXIS_X:
    case ID_MOVEMENT_AXIS_Y:
    case ID_MOVEMENT_AXIS_Z:
      {
        double vec[3] = {m_MovementAxisX,m_MovementAxisY,m_MovementAxisZ};
        double norm = sqrt(vtkMath::Normalize(vec));
        m_MovementAxisX = vec[0]/norm;
        m_MovementAxisY = vec[1]/norm;
        m_MovementAxisZ = vec[2]/norm;
        m_MovementGui->Update();
      }
      break;
    case ID_EDIT_MOVEMENT:
      {
        OnEditMovement();
      }
      break;
    case ID_RUN:
      {
        OnRunMovements();
      }
      break;
    case ID_ARROW_UP:
      {
        OnArrowUp();
      }
      break;
    case ID_ARROW_DOWN:
      {
        OnArrowDown();
      }
      break;
    case ID_ADD_MOVEMENT:
      {
        OnAddMovement();
      }
      break;
    case ID_ROTATE_X_INCREASE:
      {
        m_Orientation[0] += 1.0;
        UpdateOrientation();
      }
      break;
    case ID_ROTATE_X_DECREASE:
      {
        m_Orientation[0] -= 1.0;
        UpdateOrientation();
      }
      break;
    case ID_ROTATE_Y_INCREASE:
      {
        m_Orientation[1] += 1.0;
        UpdateOrientation();
      }
      break;
    case ID_ROTATE_Y_DECREASE:
      {
        m_Orientation[1] -= 1.0;
        UpdateOrientation();
      }
      break;
    case ID_ROTATE_Z_INCREASE:
      {
        m_Orientation[2] += 1.0;
        UpdateOrientation();
      }
      break;
    case ID_ROTATE_Z_DECREASE:
      {
        m_Orientation[2] -= 1.0;
        UpdateOrientation();
      }
      break;
    case ID_ROTATE_X:
    case ID_ROTATE_Y:
    case ID_ROTATE_Z:
      {
        UpdateOrientation();
      }
      break;
    case wxCANCEL:
      {
        OpStop(OP_RUN_OK);
      }
      break;
    case wxOK:
      {
        OpStop(OP_RUN_OK);
      }
      break;
    default:
      mafEventMacro(*maf_event);
    }
  }
  else
    mafEventMacro(*maf_event);
}
//----------------------------------------------------------------------------
bool iOpROM::AcceptAcetabularProsthesis(mafNode *node)
//----------------------------------------------------------------------------
{
  if (mafVMESurface::SafeDownCast(node))
  {
    mafVME::SafeDownCast(node)->GetOutput()->Update();
  }
  return(node != NULL && mafVME::SafeDownCast(node)->IsA("mafVMESurface") && mafVMESurface::SafeDownCast(node)->GetTagArray()->IsTagPresent(TAG_SURFACE_TYPE_ACETABULAR));
}
//----------------------------------------------------------------------------
bool iOpROM::AcceptFemoralProsthesis(mafNode *node)
//----------------------------------------------------------------------------
{
  if (mafVME::SafeDownCast(node))
  {
    mafVME::SafeDownCast(node)->GetOutput()->Update();
  }
  return(node != NULL && mafVME::SafeDownCast(node)->IsA("mafVMESurface") && mafVMESurface::SafeDownCast(node)->GetTagArray()->IsTagPresent(TAG_PROSTHESIS_TYPE_FEMORAL));
}
//----------------------------------------------------------------------------
bool iOpROM::AcceptSurface( mafNode *node )
//----------------------------------------------------------------------------
{
  return(node != NULL && mafVME::SafeDownCast(node)->IsA("mafVMESurface"));
}
//----------------------------------------------------------------------------
void iOpROM::GenerateStaticSurface()
//----------------------------------------------------------------------------
{
  vtkPolyData *polydataSurface = vtkPolyData::SafeDownCast(m_AcetabularSurface->GetOutput()->GetVTKData());

  vtkMAFSmartPointer<vtkTransform> transformSurface;
  transformSurface->SetMatrix(m_AcetabularSurface->GetOutput()->GetAbsMatrix()->GetVTKMatrix());

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> transformedSurface;
  transformedSurface->SetTransform(transformSurface);
  transformedSurface->SetInput(polydataSurface);
  transformedSurface->Update();

  vtkMAFSmartPointer<vtkAppendPolyData> prosthesis;
  for (int i=0;i<m_AcetabularProsthesisGroup->GetNumberOfChildren();i++)
  {
    vtkPolyData *polydataProsthesis = vtkPolyData::SafeDownCast(mafVMESurface::SafeDownCast(m_AcetabularProsthesisGroup->GetChild(i))->GetOutput()->GetVTKData());

    vtkMAFSmartPointer<vtkTransform> t;
    t->SetMatrix(mafVMESurface::SafeDownCast(m_AcetabularProsthesisGroup->GetChild(i))->GetOutput()->GetAbsMatrix()->GetVTKMatrix());
    vtkMAFSmartPointer<vtkTransformPolyDataFilter> tpd;
    tpd->SetTransform(t);
    tpd->SetInput(polydataProsthesis);

    if (i == 0)
    {
      prosthesis->SetInput(tpd->GetOutput());
    }
    else
    {
      prosthesis->AddInput(tpd->GetOutput());
    }
  }
  prosthesis->Update();

  vtkMAFSmartPointer<vtkUnsignedCharArray> scalarSurface;
  scalarSurface->SetNumberOfTuples(transformedSurface->GetOutput()->GetNumberOfCells());
  for (int i=0;i<transformedSurface->GetOutput()->GetNumberOfCells();i++)
  {
    scalarSurface->SetTuple1(i,FALSE);
  }
  scalarSurface->SetName("SurfaceOrProsthesis");
  vtkMAFSmartPointer<vtkPolyData> poly1;
  poly1->DeepCopy(transformedSurface->GetOutput());
  poly1->GetCellData()->AddArray(scalarSurface);

  vtkMAFSmartPointer<vtkUnsignedCharArray> scalarProsthesis;
  scalarProsthesis->SetNumberOfTuples(prosthesis->GetOutput()->GetNumberOfCells());
  for (int i=0;i<prosthesis->GetOutput()->GetNumberOfCells();i++)
  {
    scalarProsthesis->SetTuple1(i,TRUE);
  }
  scalarProsthesis->SetName("SurfaceOrProsthesis");
  vtkMAFSmartPointer<vtkPolyData> poly2;
  poly2->DeepCopy(prosthesis->GetOutput());
  poly2->GetCellData()->AddArray(scalarProsthesis);

  vtkMAFSmartPointer<vtkAppendPolyData> append;
  append->SetInput(poly1);
  append->AddInput(poly2);
  append->Update();

  m_CellsToExclude = new bool[append->GetOutput()->GetNumberOfCells()];
  vtkUnsignedCharArray *scalarResults = vtkUnsignedCharArray::SafeDownCast(append->GetOutput()->GetCellData()->GetArray("SurfaceOrProsthesis"));
  for (int i=0;i<scalarResults->GetNumberOfTuples();i++)
  {
    m_CellsToExclude[i] = scalarResults->GetTuple1(i) == 2;
  }

  mafNEW(m_Acetabular);
  m_Acetabular->SetName("AcetabularComponent-ROM");
  m_Acetabular->SetData(append->GetOutput(),0.0);
  m_Acetabular->ReparentTo(m_AcetabularProsthesisGroup->GetRoot());
  m_Acetabular->Update();

}
//----------------------------------------------------------------------------
void iOpROM::GenerateMobileSurface()
//----------------------------------------------------------------------------
{

  double acetabularPosition[3],femoralPosition[3];
  mafTransform::GetPosition(*(m_AcetabularProsthesisGroup->GetOutput()->GetAbsMatrix()),acetabularPosition);

  double b[6];
  mafVMESurface::SafeDownCast(m_FemoralProsthesisGroup->GetChild(m_FemoralProsthesisGroup->GetNumberOfChildren()-1))->GetOutput()->GetBounds(b);
  femoralPosition[0] = (b[1] + b[0])/2.0;
  femoralPosition[1] = (b[3] + b[2])/2.0;
  femoralPosition[2] = (b[5] + b[4])/2.0;

  //Compute translation of femoral component to collocate it inside acetabular component
  double translate[3];
  translate[0] = acetabularPosition[0] - femoralPosition[0];
  translate[1] = acetabularPosition[1] - femoralPosition[1];
  translate[2] = acetabularPosition[2] - femoralPosition[2];

  vtkPolyData *polydataSurface = vtkPolyData::SafeDownCast(m_FemoralSurface->GetOutput()->GetVTKData());

  vtkMAFSmartPointer<vtkTransform> transformSurface;
  transformSurface->SetMatrix(m_FemoralSurface->GetOutput()->GetAbsMatrix()->GetVTKMatrix());
  transformSurface->PostMultiply();
  transformSurface->Translate(translate);

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> transformedSurface;
  transformedSurface->SetTransform(transformSurface);
  transformedSurface->SetInput(polydataSurface);
  transformedSurface->Update();

  vtkMAFSmartPointer<vtkAppendPolyData> prosthesis;
  for (int i=0;i<m_FemoralProsthesisGroup->GetNumberOfChildren();i++)
  {
    vtkPolyData *polydataProsthesis = vtkPolyData::SafeDownCast(mafVMESurface::SafeDownCast(m_FemoralProsthesisGroup->GetChild(i))->GetOutput()->GetVTKData());

    vtkMAFSmartPointer<vtkTransform> t;
    t->SetMatrix(mafVMESurface::SafeDownCast(m_FemoralProsthesisGroup->GetChild(i))->GetOutput()->GetAbsMatrix()->GetVTKMatrix());
    t->PostMultiply();
    t->Translate(translate);
    vtkMAFSmartPointer<vtkTransformPolyDataFilter> tpd;
    tpd->SetTransform(t);
    tpd->SetInput(polydataProsthesis);

    if (i == 0)
    {
      prosthesis->SetInput(tpd->GetOutput());
    }
    else
    {
      prosthesis->AddInput(tpd->GetOutput());
    }
  }
  prosthesis->Update();

  vtkMAFSmartPointer<vtkUnsignedCharArray> scalarSurface;
  scalarSurface->SetNumberOfTuples(transformedSurface->GetOutput()->GetNumberOfCells());
  for (int i=0;i<transformedSurface->GetOutput()->GetNumberOfCells();i++)
  {
    scalarSurface->SetTuple1(i,FALSE);
  }
  scalarSurface->SetName("SurfaceOrProsthesis");
  vtkMAFSmartPointer<vtkPolyData> poly1;
  poly1->DeepCopy(transformedSurface->GetOutput());
  poly1->GetCellData()->AddArray(scalarSurface);

  vtkMAFSmartPointer<vtkUnsignedCharArray> scalarProsthesis;
  scalarProsthesis->SetNumberOfTuples(prosthesis->GetOutput()->GetNumberOfCells());
  for (int i=0;i<prosthesis->GetOutput()->GetNumberOfCells();i++)
  {
    scalarProsthesis->SetTuple1(i,TRUE);
  }
  scalarProsthesis->SetName("SurfaceOrProsthesis");
  vtkMAFSmartPointer<vtkPolyData> poly2;
  poly2->DeepCopy(prosthesis->GetOutput());
  poly2->GetCellData()->AddArray(scalarProsthesis);

  vtkMAFSmartPointer<vtkAppendPolyData> append;
  append->SetInput(poly1);
  /*append->AddInput(poly2);*/
  append->Update();

  m_CellsToExclude = new bool[append->GetOutput()->GetNumberOfCells()];
  vtkUnsignedCharArray *scalarResults = vtkUnsignedCharArray::SafeDownCast(append->GetOutput()->GetCellData()->GetArray("SurfaceOrProsthesis"));
  for (int i=0;i<scalarResults->GetNumberOfTuples();i++)
  {
    m_CellsToExclude[i] = scalarResults->GetTuple1(i) == 2;
  }

  mafNEW(m_Femoral);
  m_Femoral->SetName("FemoralComponent-ROM");
  m_Femoral->SetData(append->GetOutput(),0.0);
  m_Femoral->ReparentTo(m_FemoralProsthesisGroup->GetRoot());
  m_Femoral->Update();

  mafNEW(m_FemoralProsthesisTMP);
  /*m_FemoralProsthesisTMP->SetAbsMatrix(*m_FemoralProsthesis->GetOutput()->GetAbsMatrix());*/
  m_FemoralProsthesisTMP->SetName("FemoralProsthesisTMP-ROM");
  m_FemoralProsthesisTMP->SetData(poly2,0.0);
  m_FemoralProsthesisTMP->ReparentTo(m_Femoral);
  m_FemoralProsthesisTMP->Update();
}
//----------------------------------------------------------------------------
void iOpROM::UpdateOrientation()
//----------------------------------------------------------------------------
{
  double pos[3];
  mafTransform::GetPosition(*m_RefSys,pos);
  vtkMAFSmartPointer<vtkTransform> t;
  mafMatrix m;
  t->PostMultiply();
  t->Translate(-pos[0],-pos[1],-pos[2]);
  t->RotateY(m_Orientation[1]);
  t->RotateX(m_Orientation[0]);
  t->RotateZ(m_Orientation[2]);
  t->Translate(pos[0],pos[1],pos[2]);
  t->Update();

  m.SetVTKMatrix(t->GetMatrix());

  m_Femoral->SetAbsMatrix(t->GetMatrix());
  m_Femoral->GetOutput()->GetAbsMatrix();
  m_Femoral->GetOutput()->Update();
  m_Femoral->Update();

  m_View->CameraUpdate();

  m_Gui->Update();
}
//----------------------------------------------------------------------------
int iOpROM::FillMovementsList()
//----------------------------------------------------------------------------
{
  iAttributeROM *attribute = NULL;
  if (m_VmeWithRomInformation)
  {
    attribute = iAttributeROM::SafeDownCast(m_VmeWithRomInformation->GetAttribute("DataForROM"));
  }
  if (m_VmeWithRomInformation != NULL && attribute->GetNumberOfMovementsPerVME(m_FemoralProsthesisGroup->GetId())>0)
  {
    for (int index = 0;index < attribute->GetNumberOfMovementsPerVME(m_FemoralProsthesisGroup->GetId());index++)
    {
      wxString name;
      double angleInf,angleSup,axis[3],angleCollision;
      bool startFromPrevious,showMovement,passed;
      attribute->GetMovement(m_FemoralProsthesisGroup->GetId(),index,name,angleInf,angleSup,angleCollision,axis,showMovement,startFromPrevious,passed);
      DB_MOVEMENTS_INFORMATION info;
      info.m_Name = name.c_str();
      info.m_Axis[0] = axis[0];
      info.m_Axis[1] = axis[1];
      info.m_Axis[2] = axis[2];
      info.m_AngleInf = angleInf;
      info.m_AngleSup = angleSup;
      info.m_AngleCollision = angleCollision;
      info.m_ShowMovement = showMovement;
      info.m_StartFromPrevious = startFromPrevious;
      info.m_Passed = passed;

      m_DBMomementsInformation.push_back(info);
    }
  }
  else//If attribute not exist so load standard DB
  {
    //Open the file xml with manufacture and model information
    try {
      XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Initialize();
    }
    catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
      // Do your failure processing here
      return MAF_ERROR;
    }

    XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser *XMLParser = new XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser;

    XMLParser->setValidationScheme(XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser::Val_Auto);
    XMLParser->setDoNamespaces(false);
    XMLParser->setDoSchema(false);
    XMLParser->setCreateEntityReferenceNodes(false);

    mmuDOMTreeErrorReporter *errReporter = new mmuDOMTreeErrorReporter();
    XMLParser->setErrorHandler(errReporter);

    mafString FileName = mafGetApplicationDirectory().c_str();
    FileName<<PATH_MOVEMENTS_DB_FILE;
    try {
      XMLParser->parse(FileName.GetCStr());
      int errorCount = XMLParser->getErrorCount(); 

      if (errorCount != 0)
      {
        // errors while parsing...
        mafErrorMessage("Errors while parsing XML file");
        return MAF_ERROR;
      }
      else
      {
        // extract the root element and wrap inside a mafXMLElement
        XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument *doc = XMLParser->getDocument();
        XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *root = doc->getDocumentElement();
        assert(root);

        mafString name = mafXMLString(root->getTagName());
        if (name == "DB")
        {
          mafString version = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)root)->getAttribute(mafXMLString("Version")));

          //Check the DB version
          if (version != "1.0")
          {
            return MAF_ERROR;
          }
        }

        //Reset old DB
        m_DBMomementsInformation.clear();

        XERCES_CPP_NAMESPACE_QUALIFIER DOMNodeList *movementsChildren=root->getChildNodes();
        for (unsigned int i = 0; i<movementsChildren->getLength();i++)
        {
          XERCES_CPP_NAMESPACE_QUALIFIER DOMNode *movement_node=movementsChildren->item(i);
          if (movementsChildren->item(i)->getNodeType()==XERCES_CPP_NAMESPACE_QUALIFIER DOMNode::ELEMENT_NODE)
          {
            XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *movement_element = (XERCES_CPP_NAMESPACE_QUALIFIER DOMElement*)movement_node;
            mafString nameElement = ""; 
            nameElement = mafXMLString(movement_element->getTagName());
            if (nameElement == "MOVEMENT")
            {                        
              mafString nameMovement = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)movement_element)->getAttribute(mafXMLString("Name")));
              double axis[3];
              axis[0] = atof(mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)movement_element)->getAttribute(mafXMLString("AxisX"))).GetCStr());
              axis[1] = atof(mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)movement_element)->getAttribute(mafXMLString("AxisY"))).GetCStr());
              axis[2] = atof(mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)movement_element)->getAttribute(mafXMLString("AxisZ"))).GetCStr());
              double angleInf = atof(mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)movement_element)->getAttribute(mafXMLString("AngleInf"))).GetCStr());;
              double angleSup = atof(mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)movement_element)->getAttribute(mafXMLString("AngleSup"))).GetCStr());;

              DB_MOVEMENTS_INFORMATION info;
              info.m_Name = nameMovement;
              info.m_Axis[0] = axis[0];
              info.m_Axis[1] = axis[1];
              info.m_Axis[2] = axis[2];
              info.m_AngleInf = angleInf;
              info.m_AngleSup = angleSup;
              info.m_ShowMovement = false;
              info.m_StartFromPrevious = false;
              info.m_Passed = false;
              info.m_AngleCollision = -99999.0;

              m_DBMomementsInformation.push_back(info);
            }
          }
        }
      }
    }
    catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
      return MAF_ERROR;
    }
    catch (const XERCES_CPP_NAMESPACE_QUALIFIER DOMException& toCatch) {
      return MAF_ERROR;
    }
    catch (...) {
      return MAF_ERROR;
    }

    cppDEL (errReporter);
    delete XMLParser;

    // terminate the XML library
    XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Terminate();

    mafLogMessage(_("DB opened"));
  }

  for (int i=0;i<m_DBMomementsInformation.size();i++)
  {
    m_MovementsId.push_back(i);
    m_MovementsList->AddItem(i,m_DBMomementsInformation[i].m_Name.GetCStr(),true);
  }

  

  return MAF_OK;
  
}
//----------------------------------------------------------------------------
void iOpROM::OnAddMovement()
//----------------------------------------------------------------------------
{
  
  CreateMovemetDialog();

  int result = m_Dialog->ShowModal();

  if (result == wxID_OK)
  {
    DB_MOVEMENTS_INFORMATION movement;
    movement.m_Name = m_MomementName;
    movement.m_AngleInf = m_MovementAngleInf;
    movement.m_AngleSup = m_MovementAngleSup;
    movement.m_Axis[0] = m_MovementAxisX;
    movement.m_Axis[1] = m_MovementAxisY;
    movement.m_Axis[2] = m_MovementAxisZ;

    m_DBMomementsInformation.push_back(movement);

    if (m_MovementAddToDB == TRUE)
    {
      int result = AddMovementToDB();
      if (result == MAF_ERROR)
      {
        mafLogMessage("<<<<<<< ERROR DURING WRITE OF DB!");
        return;
      }
      wxMessageBox("DataBase updated!");
    }

    m_MovementsId.clear();
    m_MovementsList->Clear();
    for (int i=0;i<m_DBMomementsInformation.size();i++)
    {
      m_MovementsId.push_back(i);
      m_MovementsList->AddItem(i,m_DBMomementsInformation[i].m_Name.GetCStr(),true);
    }
  }
  else
  {

  }


  cppDEL(m_Dialog);
}
//----------------------------------------------------------------------------
int iOpROM::AddMovementToDB()
//----------------------------------------------------------------------------
{
  wxBusyInfo wait("Please wait - Updating DB");
  //Open the file xml with manufacture and model information
  try {
    XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Initialize();
  }
  catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
    // Do your failure processing here
    return MAF_ERROR;
  }

  XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument *doc;
  XMLCh tempStr[100];
  XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("LS", tempStr, 99);
  XERCES_CPP_NAMESPACE_QUALIFIER DOMImplementation *impl = XERCES_CPP_NAMESPACE_QUALIFIER DOMImplementationRegistry::getDOMImplementation(tempStr);
  XERCES_CPP_NAMESPACE_QUALIFIER DOMWriter* theSerializer = ((XERCES_CPP_NAMESPACE_QUALIFIER DOMImplementationLS*)impl)->createDOMWriter();
  theSerializer->setNewLine( mafXMLString("\r") );
  doc = impl->createDocument(NULL, mafXMLString("DB"), NULL);

  doc->setEncoding( mafXMLString("UTF-8") );
  doc->setStandalone(true);
  doc->setVersion( mafXMLString("1.0") );

  // extract root element and wrap it with an mafXMLElement object
  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *root = doc->getDocumentElement();
  assert(root);

  // attach version attribute to the root node
  root->setAttribute(mafXMLString("Version"),mafXMLString(MOVEMENTS_DB_VERSION));

  for (int i=0;i<m_DBMomementsInformation.size();i++)
  {
    XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *movementChild=doc->createElement(mafXMLString("MOVEMENT"));
    movementChild->setAttribute(mafXMLString("Name"),mafXMLString(m_DBMomementsInformation[i].m_Name));
    movementChild->setAttribute(mafXMLString("AxisX"),mafXMLString(wxString::Format("%.3f",m_DBMomementsInformation[i].m_Axis[0])));
    movementChild->setAttribute(mafXMLString("AxisY"),mafXMLString(wxString::Format("%.3f",m_DBMomementsInformation[i].m_Axis[1])));
    movementChild->setAttribute(mafXMLString("AxisZ"),mafXMLString(wxString::Format("%.3f",m_DBMomementsInformation[i].m_Axis[2])));
    movementChild->setAttribute(mafXMLString("AngleInf"),mafXMLString(wxString::Format("%.3f",m_DBMomementsInformation[i].m_AngleInf)));
    movementChild->setAttribute(mafXMLString("AngleSup"),mafXMLString(wxString::Format("%.3f",m_DBMomementsInformation[i].m_AngleSup)));

    root->appendChild(movementChild);
  }

  // optionally you can set some features on this serializer
  if (theSerializer->canSetFeature(XERCES_CPP_NAMESPACE_QUALIFIER XMLUni::fgDOMWRTDiscardDefaultContent, true))
    theSerializer->setFeature(XERCES_CPP_NAMESPACE_QUALIFIER XMLUni::fgDOMWRTDiscardDefaultContent, true);

  if (theSerializer->canSetFeature(XERCES_CPP_NAMESPACE_QUALIFIER XMLUni::fgDOMWRTFormatPrettyPrint, true))
    theSerializer->setFeature(XERCES_CPP_NAMESPACE_QUALIFIER XMLUni::fgDOMWRTFormatPrettyPrint, true);

  XERCES_CPP_NAMESPACE_QUALIFIER XMLFormatTarget *XMLTarget;
  mafString fileName = mafGetApplicationDirectory().c_str();
  fileName<<PATH_MOVEMENTS_DB_FILE;
  XMLTarget = new XERCES_CPP_NAMESPACE_QUALIFIER LocalFileFormatTarget(fileName);

  try {
    // do the serialization through DOMWriter::writeNode();
    theSerializer->writeNode(XMLTarget,*doc);
  }
  catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
    char* message = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(toCatch.getMessage());
    XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&message);
    return MAF_ERROR;
  }
  catch (const XERCES_CPP_NAMESPACE_QUALIFIER DOMException& toCatch) {
    char* message = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(toCatch.msg);
    XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&message);
    return MAF_ERROR;
  }
  catch (...) {
    return MAF_ERROR;
  }

  theSerializer->release();
  cppDEL (XMLTarget);
  doc->release();

  XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Terminate();

  mafLogMessage(wxString::Format("New DB has been written %s",fileName.GetCStr()));

  return MAF_OK;
}
//----------------------------------------------------------------------------
void iOpROM::OnArrowDown()
//----------------------------------------------------------------------------
{
  if (m_MovementsList->GetSelection() != -1)
  {
    if (m_MovementsList->GetSelection() != (m_MovementsList->GetNumberOfItems()-1))//Chech if the selected elements is the last
    {
      DB_MOVEMENTS_INFORMATION tmpElement = m_DBMomementsInformation[m_MovementsList->GetSelection()];
      m_DBMomementsInformation[m_MovementsList->GetSelection()] = m_DBMomementsInformation[m_MovementsList->GetSelection() + 1];
      m_DBMomementsInformation[m_MovementsList->GetSelection() + 1] = tmpElement;

      int newSelection = m_MovementsList->GetSelection() + 1;

      m_MovementsId.clear();
      m_MovementsList->Clear();
      for (int i=0;i<m_DBMomementsInformation.size();i++)
      {
        m_MovementsId.push_back(i);
        m_MovementsList->AddItem(i,m_DBMomementsInformation[i].m_Name.GetCStr(),true);
      }

      m_MovementsList->Select(newSelection);
    }
  }
}
//----------------------------------------------------------------------------
void iOpROM::OnArrowUp()
//----------------------------------------------------------------------------
{
  if (m_MovementsList->GetSelection() != -1)
  {
    if (m_MovementsList->GetSelection() != 0)//Chech if the selected elements is the top
    {
      DB_MOVEMENTS_INFORMATION tmpElement = m_DBMomementsInformation[m_MovementsList->GetSelection()];
      m_DBMomementsInformation[m_MovementsList->GetSelection()] = m_DBMomementsInformation[m_MovementsList->GetSelection() - 1];
      m_DBMomementsInformation[m_MovementsList->GetSelection() - 1] = tmpElement;

      int newSelection = m_MovementsList->GetSelection() - 1;

      m_MovementsId.clear();
      m_MovementsList->Clear();
      for (int i=0;i<m_DBMomementsInformation.size();i++)
      {
        m_MovementsId.push_back(i);
        m_MovementsList->AddItem(i,m_DBMomementsInformation[i].m_Name.GetCStr(),true);
      }

      m_MovementsList->Select(newSelection);
    }
  }
}
//----------------------------------------------------------------------------
void iOpROM::OnRunMovements()
//----------------------------------------------------------------------------
{
  int previousItemChecked = -1;

  vtkMAFSmartPointer<vtkMEDCollisionDetectionFilter> collisionFilter;
  collisionFilter->SetInput(0,vtkPolyData::SafeDownCast(m_Acetabular->GetOutput()->GetVTKData()));
  collisionFilter->SetMatrix(0,m_Acetabular->GetOutput()->GetAbsMatrix()->GetVTKMatrix());
  collisionFilter->SetInput(1,vtkPolyData::SafeDownCast(m_Femoral->GetOutput()->GetVTKData()));

  double old_aux_rot[3];
  for (int indexMovement=0;indexMovement<m_MovementsList->GetNumberOfItems();indexMovement++)
  {
    if (m_MovementsList->IsItemChecked(indexMovement))
    {
      bool collision = false;
      bool okStartFromPrevious = false;
      double vectorValues[3];
      vectorValues[0] = m_DBMomementsInformation[indexMovement].m_Axis[0];
      vectorValues[1] = m_DBMomementsInformation[indexMovement].m_Axis[1];
      vectorValues[2] = m_DBMomementsInformation[indexMovement].m_Axis[2];

      double angles[3];
      angles[0] = atan(vectorValues[1] / (vectorValues[2]+0.0001));
      angles[1] = atan(vectorValues[0] / (vectorValues[2]+0.0001));
      angles[2] = 0.0;

      angles[0] *= vtkMath::RadiansToDegrees();
      angles[1] *= vtkMath::RadiansToDegrees();
      angles[2] *= vtkMath::RadiansToDegrees();

      double angleToRotate = m_DBMomementsInformation[indexMovement].m_AngleInf;
      double oldAngleToRotate = 0.0;
      if (m_DBMomementsInformation[indexMovement].m_StartFromPrevious)
      {
        if (previousItemChecked != -1)
        {
          oldAngleToRotate = m_DBMomementsInformation[previousItemChecked].m_AngleSup;
          okStartFromPrevious = true;
          mafTransform::GetPosition(*m_RefSys,old_aux_rot);
        }
//         else
//         {
//           angleToRotate = m_DBMomementsInformation[indexMovement].m_AngleInf;
//         }
      }
//       else
//       {
//         angleToRotate = m_DBMomementsInformation[indexMovement].m_AngleInf;
//       }

      mafTransform::SetOrientation(*m_RefSys,angles);
      
      double aux_pos[3],aux_rot[3];

      bool firstCycle = true;
      do 
      {
        //////////////////////////////////////////////////////////////////////////
        //Generate the matrix for the orientation without translate
        vtkMAFSmartPointer<vtkTransform> t;
        t->PostMultiply();
        double prosthesisPose[3],prosthesisOrientation[3];
        m_Femoral->GetOutput()->GetAbsPose(prosthesisPose,prosthesisOrientation);
        mafTransform::GetOrientation(*m_RefSys,aux_rot);
        mafTransform::GetPosition(*m_RefSys,aux_pos);

        t->Translate(-aux_pos[0],-aux_pos[1],-aux_pos[2]);

        if (okStartFromPrevious)
        {
          t->RotateY(-old_aux_rot[1]);
          t->RotateX(-old_aux_rot[0]);
          t->RotateZ(-old_aux_rot[2]);

          t->RotateY(0);
          t->RotateX(0);
          t->RotateZ(oldAngleToRotate);

          t->RotateY(old_aux_rot[1]);
          t->RotateX(old_aux_rot[0]);
          t->RotateZ(old_aux_rot[2]);
        }

        t->RotateY(-aux_rot[1]);
        t->RotateX(-aux_rot[0]);
        t->RotateZ(-aux_rot[2]);
        t->RotateY(0);
        t->RotateX(0);
        t->RotateZ(angleToRotate);
        t->RotateY(aux_rot[1]);
        t->RotateX(aux_rot[0]);

        t->Translate(aux_pos[0],aux_pos[1],aux_pos[2]);
        t->Update();
        //////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////
        //Compute the distance between the ref sys and the origin of the prosthesis
        double distanceRefSysOrigin[3];
        /*m_Femoral->SetAbsPose(0,0,0,0.0,0.0,0.0);*/
        mafTransform::GetPosition(*m_RefSys,distanceRefSysOrigin);
        //////////////////////////////////////////////////////////////////////////

        //Remove from the prosthesis the orientation
        mafMatrix matrixNoRotation;
        mafTransform::SetPosition(matrixNoRotation,aux_pos[0]-distanceRefSysOrigin[0],aux_pos[1]-distanceRefSysOrigin[1],aux_pos[2]-distanceRefSysOrigin[2]);
        /*m_Femoral->SetAbsPose(aux_pos[0]-distanceRefSysOrigin[0],aux_pos[1]-distanceRefSysOrigin[1],aux_pos[2]-distanceRefSysOrigin[2],0.0,0.0,0.0);*/

        // handle incoming transform events
        vtkMAFSmartPointer<vtkTransform> tr;
        tr->PostMultiply();
        tr->SetMatrix(matrixNoRotation.GetVTKMatrix());
        tr->Concatenate(t->GetMatrix());
        tr->Update();

        mafMatrix absPose;
        absPose.DeepCopy(tr->GetMatrix());
        absPose.SetTimeStamp(0.0);

        // move vme
        m_Femoral->SetAbsMatrix(absPose);
        m_Femoral->GetAbsMatrixPipe()->Update();

//         m_FemoralProsthesisTMP->SetAbsMatrix(absPose);
//         m_FemoralProsthesisTMP->GetAbsMatrixPipe()->Update();

        collisionFilter->SetMatrix(1,absPose.GetVTKMatrix());
        collisionFilter->Update();

        vtkDataArray *array0 = collisionFilter->GetOutput(0)->GetFieldData()->GetArray("ContactCells");
        
        if (array0->GetNumberOfTuples())//exist collision
        {
          collision = true;
          m_DBMomementsInformation[indexMovement].m_Passed = false;
          m_DBMomementsInformation[indexMovement].m_AngleCollision = angleToRotate;
        }

        angleToRotate += 1.0;

        if (m_DBMomementsInformation[indexMovement].m_ShowMovement)
        {
//           m_View->VmeShow(m_Femoral,false);
//           m_View->VmeShow(m_FemoralProsthesisTMP,false);
//           m_View->VmeShow(m_Femoral,true);
//           m_View->VmeShow(m_FemoralProsthesisTMP,true);
// 
//           medVisualPipeCollisionDetection *pipe;
// 
//           pipe = medVisualPipeCollisionDetection::SafeDownCast(m_View->GetSceneGraph()->Vme2Node(m_Femoral)->m_Pipe);
//           pipe->SetScalarNameToExclude("SurfaceOrProsthesis");
//           pipe->ShowSurfaceToCollideOn();
//           pipe->SetSurfaceToCollide(m_Acetabular);

//           mafEvent evReset(this,CAMERA_RESET);
//           m_Femoral->ForwardUpEvent(&evReset);
          mafEvent evUpdate(this,CAMERA_UPDATE);
          m_Femoral->ForwardUpEvent(&evUpdate);
        }
//         else
//         {
//           if (firstCycle)
//           {
//             firstCycle = false;
//             mafEvent evDisable1(this,ID_ROM_DISABLE_PIPE_UPDATE,m_Femoral);
//             mafEventMacro(evDisable1);
//             mafEvent evDisable2(this,ID_ROM_DISABLE_PIPE_UPDATE,m_Acetabular);
//             mafEventMacro(evDisable2);
//           }
//         }

      } while (angleToRotate<=m_DBMomementsInformation[indexMovement].m_AngleSup && !collision);

      m_DBMomementsInformation[indexMovement].m_Passed = !collision;

//       mafEvent evDisable1(this,ID_ROM_ENABLE_PIPE_UPDATE,m_Femoral);
//       mafEventMacro(evDisable1);
//       mafEvent evDisable2(this,ID_ROM_ENABLE_PIPE_UPDATE,m_Acetabular);
//       mafEventMacro(evDisable2);
// 
//       mafEventMacro(mafEvent(this,ID_ROM_VME_ABSMATRIX_UPDATE,m_Femoral));
//       mafEventMacro(mafEvent(this,ID_ROM_VME_ABSMATRIX_UPDATE,m_Acetabular));

//       mafEvent evReset(this,CAMERA_RESET);
//       m_Femoral->ForwardUpEvent(&evReset);
      mafEvent evUpdate(this,CAMERA_UPDATE);
      m_Femoral->ForwardUpEvent(&evUpdate);

#ifdef _DEBUG
      vtkMAFSmartPointer<vtkPolyData> data;
      vtkMAFSmartPointer<vtkPoints> pts;
      pts->InsertNextPoint(aux_pos);
      data->SetPoints(pts);
      data->Update();
      vtkMAFSmartPointer<vtkArrowSource> arrow;
      arrow->SetShaftRadius(0.03);
      arrow->SetTipRadius(0.1);
      arrow->SetTipLength(0.3);
      arrow->Update();
      vtkMAFSmartPointer<vtkMAFExtendedGlyph3D> glyph;
      glyph->SetSource(arrow->GetOutput());
      glyph->SetInput(data);

      vtkMAFSmartPointer<vtkTransform> trRotate;
      trRotate->PostMultiply();
      trRotate->Translate(-aux_pos[0],-aux_pos[1],-aux_pos[2]);
      trRotate->Scale(100,100,100);
      trRotate->RotateY(90.0);
      trRotate->RotateX(0.0);
      trRotate->RotateZ(0.0);
      trRotate->RotateY(angles[1]);
      trRotate->RotateX(angles[0]);
      trRotate->RotateZ(angles[2]);
      trRotate->Translate(aux_pos);


      vtkMAFSmartPointer<vtkTransformPolyDataFilter> tpRotate;
      tpRotate->SetInput(glyph->GetOutput());
      tpRotate->SetTransform(trRotate);
      tpRotate->Update();

      mafVMESurface *vectorSurface;
      mafNEW(vectorSurface);
      vectorSurface->SetName("VECTOR");
      vectorSurface->ReparentTo(m_Input->GetRoot());
      vectorSurface->SetData(tpRotate->GetOutput(),0.0);
      vectorSurface->Update();

      mafEventMacro(mafEvent(this,VME_SHOW,vectorSurface,true));
      mafEventMacro(mafEvent(this,CAMERA_UPDATE));

#endif
      previousItemChecked = indexMovement;
    }
  }
  

  m_MovementsId.clear();
  m_MovementsList->Clear();
  for (int i=0;i<m_DBMomementsInformation.size();i++)
  {
    m_MovementsId.push_back(i);
    wxString name = m_DBMomementsInformation[i].m_Name.GetCStr();
    if (m_DBMomementsInformation[i].m_Passed)
    {
      name.append("_PASSED");
    }
    else
    {
      name.append("_FAILED_");
      name.append(wxString::Format("%.1f",m_DBMomementsInformation[i].m_AngleCollision));
    }
    
    m_MovementsList->AddItem(i,name,true);

  }
  
}
//----------------------------------------------------------------------------
void iOpROM::OnEditMovement()
//----------------------------------------------------------------------------
{
  m_MovementAngleInf = m_DBMomementsInformation[m_MovementsList->GetSelection()].m_AngleInf;
  m_MovementAngleSup = m_DBMomementsInformation[m_MovementsList->GetSelection()].m_AngleSup;
  m_MovementAxisX = m_DBMomementsInformation[m_MovementsList->GetSelection()].m_Axis[0];
  m_MovementAxisY = m_DBMomementsInformation[m_MovementsList->GetSelection()].m_Axis[1];
  m_MovementAxisZ = m_DBMomementsInformation[m_MovementsList->GetSelection()].m_Axis[2];
  m_MomementName = m_DBMomementsInformation[m_MovementsList->GetSelection()].m_Name;
  m_MovementEnableVisualization= m_DBMomementsInformation[m_MovementsList->GetSelection()].m_ShowMovement ? TRUE : FALSE;

  CreateMovemetDialog();

  int result = m_Dialog->ShowModal();

  if (result == wxID_OK)
  {
    DB_MOVEMENTS_INFORMATION movement;
    movement.m_Name = m_MomementName;
    movement.m_AngleInf = m_MovementAngleInf;
    movement.m_AngleSup = m_MovementAngleSup;
    movement.m_Axis[0] = m_MovementAxisX;
    movement.m_Axis[1] = m_MovementAxisY;
    movement.m_Axis[2] = m_MovementAxisZ;
    movement.m_ShowMovement = m_MovementEnableVisualization == TRUE;
    movement.m_StartFromPrevious = m_MovementStartFromPreviousStep == TRUE;

    m_DBMomementsInformation[m_MovementsList->GetSelection()] = movement;

    wxMessageBox("List updated!");

    if (m_MovementAddToDB == TRUE)
    {
      int result = AddMovementToDB();
      if (result == MAF_ERROR)
      {
        mafLogMessage("<<<<<<< ERROR DURING WRITE OF DB!");
        return;
      }
      wxMessageBox("DataBase updated!");
    }

    m_MovementsId.clear();
    m_MovementsList->Clear();
    for (int i=0;i<m_DBMomementsInformation.size();i++)
    {
      m_MovementsId.push_back(i);
      m_MovementsList->AddItem(i,m_DBMomementsInformation[i].m_Name.GetCStr(),true);
    }
  }
  else
  {

  }


  cppDEL(m_Dialog);
}
//----------------------------------------------------------------------------
void iOpROM::CreateMovemetDialog()
//----------------------------------------------------------------------------
{
  m_Dialog = new mafGUIDialog(_("Movement dialog"),mafCLOSEWINDOW | mafOK | mafCLOSE);
  m_Dialog->SetListener(this);

  m_MovementGui = new mafGUI(this);
  m_MovementGui->Label(_("Name"),true);
  m_MovementGui->String(ID_MOVEMENT_NAME,"",&m_MomementName);
  m_MovementGui->Label(_("Axis"),true);
  m_MovementGui->Double(ID_MOVEMENT_AXIS_X,_("x"),&m_MovementAxisX);
  m_MovementGui->Double(ID_MOVEMENT_AXIS_Y,_("y"),&m_MovementAxisY);
  m_MovementGui->Double(ID_MOVEMENT_AXIS_Z,_("z"),&m_MovementAxisZ);
  m_MovementGui->Bool(ID_MOVEMENT_ENABLE_VISUALIZATION,_("Enable vis."),&m_MovementEnableVisualization,1);
  m_MovementGui->Double(ID_MOVEMENT_ANGLE_INF,_("Angle Inf."),&m_MovementAngleInf);
  m_MovementGui->Double(ID_MOVEMENT_ANGLE_SUP,_("Angle Sup."),&m_MovementAngleSup);
  m_MovementGui->Bool(ID_MOVEMENT_START_FROM_PREVIOUS_STEP,_("Start from previous step"),&m_MovementStartFromPreviousStep,1);
  m_MovementGui->Bool(ID_MOVEMENT_ADD_TO_DB,_("Add to DB"),&m_MovementAddToDB,1);
  m_MovementGui->FitGui();
  m_MovementGui->Update();

  m_Dialog->Add(m_MovementGui, 1, wxEXPAND);
}
//----------------------------------------------------------------------------
void iOpROM::PostMultiplyEventMatrix(mafEventBase *maf_event)
//----------------------------------------------------------------------------
{  
  if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
  {
    long arg = e->GetArg();

    // handle incoming transform events
    vtkTransform *tr = vtkTransform::New();
    tr->PostMultiply();
    tr->SetMatrix(m_Femoral->GetOutput()->GetAbsMatrix()->GetVTKMatrix());
    tr->Concatenate(e->GetMatrix()->GetVTKMatrix());
    tr->Update();

    mafMatrix absPose;
    absPose.DeepCopy(tr->GetMatrix());
    absPose.SetTimeStamp(0.0);

    mafLogMessage(wxString::Format("arg is %d",arg));
    if (arg == mafInteractorGenericMouse::MOUSE_MOVE)
    {
      // move vme
      ((mafVME *)m_Femoral)->SetAbsMatrix(absPose);
      ((mafVME *)m_Femoral)->GetAbsMatrixPipe()->Update();

//       mafTransform::GetOrientation(absPose,m_Orientation);
//       mafTransform::GetPosition(absPose,m_Position);

      /*m_GuiPose->Update();*/
      m_Gui->Update();
    } 
    mafEvent ev(this,CAMERA_UPDATE);
    m_Femoral->ForwardUpEvent(&ev);

    // clean up
    tr->Delete();

  }
}
//----------------------------------------------------------------------------
void iOpROM::LoadRomData()
//----------------------------------------------------------------------------
{
  mafVMERoot *root = mafVMERoot::SafeDownCast(m_Input->GetRoot());

  std::vector<mafNode*> vmeWithData;

  mafNodeIterator *iter = root->NewIterator();
  for (mafNode *Inode = iter->GetFirstNode(); Inode; Inode = iter->GetNextNode())
  {
    if (Inode->GetAttribute("DataForROM") != NULL)
    {
      vmeWithData.push_back(Inode);
    }
  }
  iter->Delete();

  m_VmeWithRomInformation = NULL;

  ///more then one rom data
  if (vmeWithData.size() > 1)
  {
    int result = wxMessageBox(_("Would do you like to use last saved prosthesis pose?"),"",wxYES_NO | wxICON_QUESTION);
    if (result == wxYES)
    {
      //Use the last rom data
      mafNode *correctNode = NULL;
      for (int i=0;i<vmeWithData.size();i++)
      {
        int tagValue = atoi(vmeWithData[i]->GetTagArray()->GetTag("PROSTHESIS_POSE_SAVED")->GetValue());

        if (correctNode == NULL)
        {
          correctNode = vmeWithData[i];
        }
        else
        {
          if (tagValue > atoi(correctNode->GetTagArray()->GetTag("PROSTHESIS_POSE_SAVED")->GetValue()))
          {
            correctNode = vmeWithData[i];
          }
        }
      }

      m_VmeWithRomInformation = correctNode;
    }
    else if (result == wxNO)
    {
      //user should select the correct rom data
      mafEvent e;
      mafString title = _("Choose store pose");
      e.SetArg((long)&iOpROM::AcceptRomData);
      e.SetString(&title);
      e.SetId(VME_CHOOSE);
      mafEventMacro(e);
      m_VmeWithRomInformation = e.GetVme();
      if (m_VmeWithRomInformation == NULL)
      {
        wxMessageBox(_("Inputs incorrects"));
        return;
      }
    }
  }
  else if (vmeWithData.size() == 1)
  {
    m_VmeWithRomInformation = vmeWithData[0];
  }
  else if ( vmeWithData.size() == 0)
  {
    //Store old prosthesis position
    m_AcetabularProsthesisGroup->GetOutput()->GetAbsMatrix(*m_OldAcetabularProsthisisPosition);
    m_FemoralProsthesisGroup->GetOutput()->GetAbsMatrix(*m_OldFemoralProsthisisPosition);
    return;
  }

  ApplyPoseToProsthesis();

}
//----------------------------------------------------------------------------
bool iOpROM::AcceptRomData( mafNode *node )
//----------------------------------------------------------------------------
{
  return (node != NULL && node->GetTagArray()->IsTagPresent("PROSTHESIS_POSE_SAVED"));
}
//----------------------------------------------------------------------------
void iOpROM::ApplyPoseToProsthesis()
//----------------------------------------------------------------------------
{

  //Load saved position of prosthesis

  if (m_VmeWithRomInformation == NULL)
  {
    return;
  }

  iAttributeROM *attribute = iAttributeROM::SafeDownCast(m_VmeWithRomInformation->GetAttribute("DataForROM"));

  mafMatrix mAcetabularProsthesis, mFemoralProsthesis;
  double position[3],orientation[3];
  if (attribute->GetPosition(m_AcetabularProsthesisGroup->GetId(),position) == true)
  {
    mafTransform::SetPosition(mAcetabularProsthesis,position);
  }
  if (attribute->GetOrientation(m_AcetabularProsthesisGroup->GetId(),orientation) == true)
  {
    mafTransform::SetOrientation(mAcetabularProsthesis,orientation);
  }

  mafNEW(m_OldAcetabularProsthisisPosition);
  m_OldAcetabularProsthisisPosition->DeepCopy(m_AcetabularProsthesisGroup->GetOutput()->GetAbsMatrix());
  m_AcetabularProsthesisGroup->SetAbsMatrix(mAcetabularProsthesis);
  m_AcetabularProsthesisGroup->GetOutput()->Update();
  m_AcetabularProsthesisGroup->Update();

  if (attribute->GetPosition(m_FemoralProsthesisGroup->GetId(),position) == true)
  {
    mafTransform::SetPosition(mFemoralProsthesis,position);
  }
  if (attribute->GetOrientation(m_FemoralProsthesisGroup->GetId(),orientation) == true)
  {
    mafTransform::SetOrientation(mFemoralProsthesis,orientation);
  }
  
  mafNEW(m_OldFemoralProsthisisPosition);
  m_OldFemoralProsthisisPosition->DeepCopy(m_FemoralProsthesisGroup->GetOutput()->GetAbsMatrix());
  m_FemoralProsthesisGroup->SetAbsMatrix(mFemoralProsthesis);
  m_FemoralProsthesisGroup->GetOutput()->Update();
  m_FemoralProsthesisGroup->Update();
  
}
//----------------------------------------------------------------------------
int iOpROM::CheckInputs()
//----------------------------------------------------------------------------
{
  mafVMERoot *root = mafVMERoot::SafeDownCast(m_Input->GetRoot());

  int numAcetabularSurface = 0;
  int numFemoralSurface = 0;
  int numAcetabularProsthesis = 0;
  int numFemoralProsthesis = 0;
  mafNodeIterator *iter = root->NewIterator();
  for (mafNode *Inode = iter->GetFirstNode(); Inode; Inode = iter->GetNextNode())
  {
    if (Inode->GetTagArray()->IsTagPresent(TAG_PROSTHESIS_TYPE_ACETABULAR))
    {
      m_AcetabularProsthesisGroup = mafVMEGroup::SafeDownCast(Inode);

      numAcetabularProsthesis++;
    }
    else if (Inode->GetTagArray()->IsTagPresent(TAG_PROSTHESIS_TYPE_FEMORAL))
    {
      m_FemoralProsthesisGroup= mafVMEGroup::SafeDownCast(Inode);

      numFemoralProsthesis++;
    }
    if (Inode->GetTagArray()->IsTagPresent(TAG_SURFACE_TYPE_ACETABULAR))
    {
      m_AcetabularSurface = mafVMESurface::SafeDownCast(Inode);

      numAcetabularSurface++;
    }
    else if (Inode->GetTagArray()->IsTagPresent(TAG_SURFACE_TYPE_FEMORAL))
    {
      m_FemoralSurface= mafVMESurface::SafeDownCast(Inode);

      numFemoralSurface++;
    }
  }

  iter->Delete();

  //No acetabular prosthesis or more than 1
  if (numAcetabularProsthesis != 1)
  {
    mafEvent e;
    mafString title = _("Choose Acetabular Prosthesis");
    e.SetArg((long)&iOpROM::AcceptAcetabularProsthesis);
    e.SetString(&title);
    e.SetId(VME_CHOOSE);
    mafEventMacro(e);
    m_AcetabularProsthesisGroup = mafVMEGroup::SafeDownCast(e.GetVme());
    if (m_AcetabularProsthesisGroup == NULL)
    {
      wxMessageBox(_("Inputs incorrects"));
      return MAF_ERROR;
    }
    mafVMEGroup::SafeDownCast(m_AcetabularProsthesisGroup)->GetOutput()->Update();
    mafVMEGroup::SafeDownCast(m_AcetabularProsthesisGroup)->Update();
  }
  //No femoral prosthesis or more than 1
  if (numFemoralProsthesis != 1)
  {
    mafEvent e;
    mafString title = _("Choose Femoral Prosthesis");
    e.SetArg((long)&iOpROM::AcceptFemoralProsthesis);
    e.SetString(&title);
    e.SetId(VME_CHOOSE);
    mafEventMacro(e);
    m_FemoralProsthesisGroup = mafVMEGroup::SafeDownCast(e.GetVme());
    if (m_FemoralProsthesisGroup == NULL)
    {
      wxMessageBox(_("Inputs incorrects"));
      return MAF_ERROR;
    }
    mafVMEGroup::SafeDownCast(m_FemoralProsthesisGroup)->GetOutput()->Update();
    mafVMEGroup::SafeDownCast(m_FemoralProsthesisGroup)->Update();
  }
  //No acetabular surface or more than 1
  if (numAcetabularSurface != 1)
  {
    mafEvent e;
    mafString title = _("Choose Acetabular Surface");
    e.SetArg((long)&iOpROM::AcceptSurface);
    e.SetString(&title);
    e.SetId(VME_CHOOSE);
    mafEventMacro(e);
    m_AcetabularSurface = mafVMESurface::SafeDownCast(e.GetVme());

    if (m_AcetabularSurface == NULL)
    {
      wxMessageBox(_("Inputs incorrects"));
      return MAF_ERROR;
    }
  }
  //No femoral surface or more than 1
  if (numFemoralSurface != 1)
  {
    mafEvent e;
    mafString title = _("Choose Femoral Surface");
    e.SetArg((long)&iOpROM::AcceptSurface);
    e.SetString(&title);
    e.SetId(VME_CHOOSE);
    mafEventMacro(e);

    m_FemoralSurface = mafVMESurface::SafeDownCast(e.GetVme());
    if (m_FemoralSurface == NULL)
    {
      wxMessageBox(_("Inputs incorrects"));
      return MAF_ERROR;
    }
  }

  return MAF_OK;
}
