/*=========================================================================
Program:   iPose
Module:    $RCSfile: iOpDownloadVMERefactor.h,v $
Language:  C++
Date:      $Date: 2011-04-13 10:44:28 $
Version:   $Revision: 1.1.1.1.2.17 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2012
SCS s.r.l. - BioComputing Competence Centre ( www.scsolutions.it - www.b3c.it)
=========================================================================*/

#ifndef __iOpDownloadVMERefactor_H__
#define __iOpDownloadVMERefactor_H__

//----------------------------------------------------------------------------
// Include :
//----------------------------------------------------------------------------
#include "lhpOpDownloadVMERefactor.h"
#include "iOperationsDefines.h"

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// iOpDownloadVMERefactor :
//----------------------------------------------------------------------------
/** Download VME from BiomedTown remote repository

IMPORTANT: use iOpDownloadVMERefactorTest in \Testing\Operations to debug and validate this 
component along with plugged python component tests in VMEUploaderDownloaderRefactor dir

DEBUG/DEVELOPMENT:
to debug this component along with the ThreadedUploaderDownloader server (ie Upload/Download manager)
run ThreadedUploaderDownloader.py in Eclipse in Debug mode on port 50000

REFACTOR THIS: we need to put iOpDownloadVMERefactorTest in automatic regression (Parabuild) 
on both development and production biomedtown as soon as possible. Prerequisites for this:

1: configuration file for webservices
extract all webservices uri/stuff in order to confine
devel/production ws different URIs to a single file. In this way both devel and production 
tests can be run by loading only the different configuration file

2: production server speed enhancement
At the present time this test cannot be run in production since it overloads the server

*/
 class I_OPERATIONS_EXPORT iOpDownloadVMERefactor: public lhpOpDownloadVMERefactor
{
public:

  enum REPOSITORY_VALUES
  {
    FROM_BASKET = 0,
    FROM_SANDBOX = 1,
  };

  iOpDownloadVMERefactor(wxString label = "Download VMEs", int downloadSourceID = FROM_BASKET);

  ~iOpDownloadVMERefactor(); 

  mafTypeMacro(iOpDownloadVMERefactor, lhpOpDownloadVMERefactor);

  /*virtual*/ mafOp* Copy();

  /** Builds operation's interface by calling CreateOpDialog() method. */
  /*virtual*/ void OpRun();

  /** Execute the operation. */
  /*virtual*/ void OpDo();

protected:

  int m_NumOfDir;

};
#endif