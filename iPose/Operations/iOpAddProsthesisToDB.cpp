/*=========================================================================
Program:   iPose
Module:    $RCSfile: iOpAddProsthesisToDB.cpp,v $
Language:  C++
Date:      $Date: 2012-03-21 07:27:51 $
Version:   $Revision: 1.1.2.3 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2001/2005 
SCS s.r.l. - BioComputing Competence Centre ( www.scsolutions.it - www.b3c.it)
=========================================================================*/

#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iOpAddProsthesisToDB.h"
#include <fstream>
#include <wx/busyinfo.h>
#include <wx/zstream.h>
#include <wx/fs_zip.h>

#include "mafString.h"
#include "mafMSFImporter.h"
#include "mafVMEStorage.h"
#include "mafVMERoot.h"
#include "mafVMEGroup.h"
#include "mmuDOMTreeErrorReporter.h"
#include "mafXMLElement.h"
#include "mmuXMLDOMElement.h"
#include "mafXMLString.h"
#include "mafTagArray.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include <xercesc/framework/LocalFileInputSource.hpp>
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/sax/ErrorHandler.hpp>

//----------------------------------------------------------------------------
mafCxxTypeMacro(iOpAddProsthesisToDB);
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
iOpAddProsthesisToDB::iOpAddProsthesisToDB(wxString label) :
mafOp(label)
//----------------------------------------------------------------------------
{
  m_OpType  = OPTYPE_OP;
  m_Canundo = false;
  m_InputPreserving = true;

  wxFileSystem::AddHandler(new wxZipFSHandler);
}
//----------------------------------------------------------------------------
iOpAddProsthesisToDB::~iOpAddProsthesisToDB( ) 
//----------------------------------------------------------------------------
{
}
//----------------------------------------------------------------------------
bool iOpAddProsthesisToDB::Accept(mafNode *node)
//----------------------------------------------------------------------------
{
  return true;
}
//----------------------------------------------------------------------------
mafOp* iOpAddProsthesisToDB::Copy()   
//----------------------------------------------------------------------------
{
  iOpAddProsthesisToDB *cp = new iOpAddProsthesisToDB(m_Label);
  return cp;
}
//----------------------------------------------------------------------------
void iOpAddProsthesisToDB::OpRun()   
//----------------------------------------------------------------------------
{
  mafString wildc = "zip Data (*.zip)|*.zip";
  mafString file;
  mafString fileDirectory = (mafGetApplicationDirectory() + "Config/DataBase/").c_str();
  if (file.IsEmpty())
  {
    file = mafGetOpenFile(fileDirectory, wildc, _("Choose prosthesis file")).c_str();
    if (file.IsEmpty())
    {
      OpStop(OP_RUN_CANCEL);
      return;
    }
  }

  wxString path, name, ext;
  wxSplitPath(file.GetCStr(),&path,&name,&ext);

  //Copy the zip file to the DB directory
  mafString newFilePath = (mafGetApplicationDirectory() + "/Config/DataBase/").c_str();
  newFilePath<<name.c_str();
  newFilePath<<".";
  newFilePath<<ext.c_str();

  ::wxCopyFile(file.GetCStr(), newFilePath.GetCStr());

  //Open the zip file to read the prosthesis information
  mafString msfFile = ZIPOpen(newFilePath);
  if(msfFile=="")
  {
    wxMessageBox("Error Open DB!");
    OpStop(OP_RUN_CANCEL);
    return;
  }

  mafVMEStorage *storage;
  storage = mafVMEStorage::New();
  mafVMERoot *root;
  root = storage->GetRoot();
  root->Initialize();
  root->SetName("RootB");

  mafMSFImporter *importer = new mafMSFImporter; //Import prosthesis MSF
  importer->SetURL(msfFile.GetCStr());
  importer->SetRoot(root);
  importer->Restore();

  mafString newFileName = name.c_str();
  newFileName<<".";
  newFileName<<ext.c_str();

  mafString globalProsthesisType = "Femoral";

  std::vector<DB_PROSTHESIS_INFORMATION> prosthesisToAdd;
  for (int i=0;i<root->GetNumberOfChildren();i++)
  {
	  if (root->GetChild(i) && root->GetChild(i)->GetChild(0))
	  {
      if(root->GetChild(i)->GetChild(0)->GetTagArray()->GetTag("MODEL_COMPONENT_TYPE"))
      {
        globalProsthesisType = root->GetChild(i)->GetChild(0)->GetTagArray()->GetTag("MODEL_COMPONENT_TYPE")->GetValue();
      }

	    for (int j=0;j<root->GetChild(i)->GetChild(0)->GetNumberOfChildren();j++)
      {

	      DB_PROSTHESIS_INFORMATION prosthesis;
		    prosthesis.m_Manufacture = root->GetChild(i)->GetChild(0)->GetName();
		    if (root->GetChild(i)->GetChild(0)->GetChild(j))  
		    {
		      prosthesis.m_Model = root->GetChild(i)->GetChild(0)->GetChild(j)->GetName();
		    }
		    else
		    {
		      cppDEL(importer);
		      mafDEL(storage);
		      OpStop(OP_RUN_CANCEL);
		      return;
		    }

        if(root->GetChild(i)->GetChild(0)->GetChild(j)->GetTagArray()->GetTag("MODEL_COMPONENT_TYPE"))
        {
          prosthesis.m_ProsthesisType = root->GetChild(i)->GetChild(0)->GetChild(j)->GetTagArray()->GetTag("MODEL_COMPONENT_TYPE")->GetValue();
        }
        else
        {
          prosthesis.m_ProsthesisType = globalProsthesisType;
        }
		    prosthesis.m_FileName = newFileName;
		    prosthesisToAdd.push_back(prosthesis);
      }
	  }
	  else
	  {
	    cppDEL(importer);
	    mafDEL(storage);
	    OpStop(OP_RUN_CANCEL);
	    return;
	  }
  }

  if (OpenDBProsthesis() == MAF_ERROR)
  {
    cppDEL(importer);
    mafDEL(storage);
    OpStop(OP_RUN_CANCEL);
    return;
  }

  if (FillDBProsthesis(prosthesisToAdd) == MAF_ERROR)
  {
    cppDEL(importer);
    mafDEL(storage);
    OpStop(OP_RUN_CANCEL);
    return;
  }

  cppDEL(importer);
  mafDEL(storage);

  OpStop(OP_RUN_OK);

}
//-------------------------------------------------------------------------
int iOpAddProsthesisToDB::FillDBProsthesis(std::vector<DB_PROSTHESIS_INFORMATION> prosthesisToAdd)
//-------------------------------------------------------------------------
{
  wxBusyInfo wait("Please wait - Updating DB");
  //Open the file xml with manufacture and model information
  try {
    XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Initialize();
  }
  catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
    // Do your failure processing here
    return MAF_ERROR;
  }

  XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument *doc;
  XMLCh tempStr[100];
  XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("LS", tempStr, 99);
  XERCES_CPP_NAMESPACE_QUALIFIER DOMImplementation *impl = XERCES_CPP_NAMESPACE_QUALIFIER DOMImplementationRegistry::getDOMImplementation(tempStr);
  XERCES_CPP_NAMESPACE_QUALIFIER DOMWriter* theSerializer = ((XERCES_CPP_NAMESPACE_QUALIFIER DOMImplementationLS*)impl)->createDOMWriter();
  theSerializer->setNewLine( mafXMLString("\r") );
  doc = impl->createDocument(NULL, mafXMLString("DB"), NULL);

  doc->setEncoding( mafXMLString("UTF-8") );
  doc->setStandalone(true);
  doc->setVersion( mafXMLString("1.0") );

  // extract root element and wrap it with an mafXMLElement object
  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *root = doc->getDocumentElement();
  assert(root);

  // attach version attribute to the root node
  root->setAttribute(mafXMLString("Version"),mafXMLString(PROSTHESIS_DB_VERSION));

  std::vector<bool> foundManufacture;

  for(int i = 0;i<prosthesisToAdd.size();i++)
  {
    foundManufacture.push_back(false);
  }

  for (int i=0;i<m_DBProsthesisInformation.size();)
  {
    XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *manufactureChild=doc->createElement(mafXMLString("MANUFACTURE"));
    manufactureChild->setAttribute(mafXMLString("Name"),mafXMLString(m_DBProsthesisInformation[i].m_Manufacture));

    do 
    {
      for (int k=0;k<prosthesisToAdd.size();k++)
      {
        if (!foundManufacture[k] && strcmp(m_DBProsthesisInformation[i].m_Manufacture.GetCStr(),prosthesisToAdd[k].m_Manufacture.GetCStr()) == 0)
        {
          mafLogMessage(wxString::Format("Importing MANUFACTURE %s MODEL %s",prosthesisToAdd[k].m_Manufacture.GetCStr(),prosthesisToAdd[k].m_Model.GetCStr()));
          XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *modelChild=doc->createElement(mafXMLString("MODEL"));
          modelChild->setAttribute(mafXMLString("Name"),mafXMLString(prosthesisToAdd[k].m_Model));
          modelChild->setAttribute(mafXMLString("FileName"),mafXMLString(prosthesisToAdd[k].m_FileName));
          modelChild->setAttribute(mafXMLString("Type"),mafXMLString(prosthesisToAdd[k].m_ProsthesisType));
          manufactureChild->appendChild(modelChild);

          foundManufacture[k] = true;
        }
      }


	    XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *modelChild=doc->createElement(mafXMLString("MODEL"));
	    modelChild->setAttribute(mafXMLString("Name"),mafXMLString(m_DBProsthesisInformation[i].m_Model));
	    modelChild->setAttribute(mafXMLString("FileName"),mafXMLString(m_DBProsthesisInformation[i].m_FileName));
      modelChild->setAttribute(mafXMLString("Type"),mafXMLString(m_DBProsthesisInformation[i].m_ProsthesisType));
	    manufactureChild->appendChild(modelChild);
	    i++;


    } while(i<m_DBProsthesisInformation.size() && strcmp(m_DBProsthesisInformation[i-1].m_Manufacture.GetCStr(),m_DBProsthesisInformation[i].m_Manufacture.GetCStr()) == 0);

    root->appendChild(manufactureChild);
  }

  for(int i=0;i<foundManufacture.size();i++)
  {
    if (!foundManufacture[i])
    {
      mafLogMessage(_("WARNING : Manufacture not present in the DB - New manufacture will be added"));
      XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *manufactureChild=doc->createElement(mafXMLString("MANUFACTURE"));
      manufactureChild->setAttribute(mafXMLString("Name"),mafXMLString(prosthesisToAdd[i].m_Manufacture));

      for (int k=0;k<prosthesisToAdd.size();k++)
      {
        if (strcmp(prosthesisToAdd[i].m_Manufacture.GetCStr(),prosthesisToAdd[k].m_Manufacture.GetCStr()) == 0)
        {
          mafLogMessage(wxString::Format("Importing MANUFACTURE %s MODEL %s",prosthesisToAdd[k].m_Manufacture.GetCStr(),prosthesisToAdd[k].m_Model.GetCStr()));
          XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *modelChild=doc->createElement(mafXMLString("MODEL"));
          modelChild->setAttribute(mafXMLString("Name"),mafXMLString(prosthesisToAdd[k].m_Model));
          modelChild->setAttribute(mafXMLString("FileName"),mafXMLString(prosthesisToAdd[k].m_FileName));
          modelChild->setAttribute(mafXMLString("Type"),mafXMLString(prosthesisToAdd[k].m_ProsthesisType));
          manufactureChild->appendChild(modelChild);

          foundManufacture[k] = true;
        }
      }
      root->appendChild(manufactureChild);
    }
  }

  // optionally you can set some features on this serializer
  if (theSerializer->canSetFeature(XERCES_CPP_NAMESPACE_QUALIFIER XMLUni::fgDOMWRTDiscardDefaultContent, true))
    theSerializer->setFeature(XERCES_CPP_NAMESPACE_QUALIFIER XMLUni::fgDOMWRTDiscardDefaultContent, true);

  if (theSerializer->canSetFeature(XERCES_CPP_NAMESPACE_QUALIFIER XMLUni::fgDOMWRTFormatPrettyPrint, true))
    theSerializer->setFeature(XERCES_CPP_NAMESPACE_QUALIFIER XMLUni::fgDOMWRTFormatPrettyPrint, true);

  XERCES_CPP_NAMESPACE_QUALIFIER XMLFormatTarget *XMLTarget;
  mafString fileName = mafGetApplicationDirectory().c_str();
  fileName<<PATH_PROSTHESIS_DB_FILE;
  XMLTarget = new XERCES_CPP_NAMESPACE_QUALIFIER LocalFileFormatTarget(fileName);

  try {
    // do the serialization through DOMWriter::writeNode();
    theSerializer->writeNode(XMLTarget,*doc);
  }
  catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
    char* message = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(toCatch.getMessage());
    XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&message);
    return MAF_ERROR;
  }
  catch (const XERCES_CPP_NAMESPACE_QUALIFIER DOMException& toCatch) {
    char* message = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(toCatch.msg);
    XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&message);
    return MAF_ERROR;
  }
  catch (...) {
    return MAF_ERROR;
  }

  theSerializer->release();
  cppDEL (XMLTarget);
  doc->release();

  XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Terminate();

  mafLogMessage(wxString::Format("New DB has been written %s",fileName.GetCStr()));

  return MAF_OK;
}
//-------------------------------------------------------------------------
int iOpAddProsthesisToDB::OpenDBProsthesis()
//-------------------------------------------------------------------------
{
  wxBusyInfo wait("Please wait - Opening DB");

  //Open the file xml with manufacture and model information
  try {
    XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Initialize();
  }
  catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
    // Do your failure processing here
    return MAF_ERROR;
  }

  XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser *XMLParser = new XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser;

  XMLParser->setValidationScheme(XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser::Val_Auto);
  XMLParser->setDoNamespaces(false);
  XMLParser->setDoSchema(false);
  XMLParser->setCreateEntityReferenceNodes(false);

  mmuDOMTreeErrorReporter *errReporter = new mmuDOMTreeErrorReporter();
  XMLParser->setErrorHandler(errReporter);

  mafString FileName = mafGetApplicationDirectory().c_str();
  FileName<<PATH_PROSTHESIS_DB_FILE;
  try {
    XMLParser->parse(FileName.GetCStr());
    int errorCount = XMLParser->getErrorCount(); 

    if (errorCount != 0)
    {
      // errors while parsing...
      mafErrorMessage("Errors while parsing XML file");
    }
    else
    {
      // extract the root element and wrap inside a mafXMLElement
      XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument *doc = XMLParser->getDocument();
      XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *root = doc->getDocumentElement();
      assert(root);

      mafString name = mafXMLString(root->getTagName());
      if (name == "DB")
      {
        mafString version = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)root)->getAttribute(mafXMLString("Version")));

        //Check the DB version
        if (version != "1.0")
        {
          return MAF_ERROR;
        }
      }

      //Reset old DB
      m_DBProsthesisInformation.clear();

      XERCES_CPP_NAMESPACE_QUALIFIER DOMNodeList *manufacturesChildren=root->getChildNodes();
      for (unsigned int i = 0; i<manufacturesChildren->getLength();i++)
      {
        XERCES_CPP_NAMESPACE_QUALIFIER DOMNode *manufacture_node=manufacturesChildren->item(i);
        if (manufacturesChildren->item(i)->getNodeType()==XERCES_CPP_NAMESPACE_QUALIFIER DOMNode::ELEMENT_NODE)
        {
          XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *manufacture_element = (XERCES_CPP_NAMESPACE_QUALIFIER DOMElement*)manufacture_node;
          mafString nameElement = ""; 
          nameElement = mafXMLString(manufacture_element->getTagName());
          if (nameElement == "MANUFACTURE")
          {                        
            mafString nameManufacture = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)manufacture_element)->getAttribute(mafXMLString("Name")));
            XERCES_CPP_NAMESPACE_QUALIFIER DOMNodeList *modelsChildren=manufacture_element->getChildNodes();
            for (unsigned int j = 0; j<modelsChildren->getLength();j++)
            {
              XERCES_CPP_NAMESPACE_QUALIFIER DOMNode *model_node=modelsChildren->item(j);
              if (modelsChildren->item(j)->getNodeType()==XERCES_CPP_NAMESPACE_QUALIFIER DOMNode::ELEMENT_NODE)
              {
                XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *model_element = (XERCES_CPP_NAMESPACE_QUALIFIER DOMElement*)model_node;

                nameElement = "";
                nameElement = mafXMLString(model_element->getTagName());
                if (nameElement == "MODEL")
                {
                  mafString nameModel = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)model_element)->getAttribute(mafXMLString("Name")));
                  mafString file = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)model_element)->getAttribute(mafXMLString("FileName")));
                  mafString type = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)model_element)->getAttribute(mafXMLString("Type")));

                  DB_PROSTHESIS_INFORMATION info;
                  info.m_Manufacture = nameManufacture;
                  info.m_Model = nameModel;
                  info.m_FileName = file;
                  info.m_ProsthesisType = type;

                  m_DBProsthesisInformation.push_back(info);
                  mafLogMessage(wxString::Format("DB reading :\n\tManufacture %s\n\tModel %s\n\tFile %s\n Type %s\n",nameManufacture.GetCStr(),nameModel.GetCStr(),file.GetCStr(),type.GetCStr()));

                }
              }
            }
          }
        }
      }
    }
  }
  catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
    return MAF_ERROR;
  }
  catch (const XERCES_CPP_NAMESPACE_QUALIFIER DOMException& toCatch) {
    return MAF_ERROR;
  }
  catch (...) {
    return MAF_ERROR;
  }

  cppDEL (errReporter);
  delete XMLParser;

  // terminate the XML library
  XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Terminate();

  mafLogMessage(_("DB opened"));

  return MAF_OK;
}
//----------------------------------------------------------------------------
mafString iOpAddProsthesisToDB::ZIPOpen(mafString file)
//----------------------------------------------------------------------------
{
  wxBusyInfo wait("Please wait - Opening ZIP file");

  mafString ZipFile,tmpDir,MSFFile;
  ZipFile = file;
  mafString zip_cache = wxPathOnly(file.GetCStr());
  if (zip_cache.IsEmpty())
  {
    //zip_cache = ::wxGetCwd();
    zip_cache = wxPathOnly(file.GetCStr());
  }
  //zip_cache = zip_cache + "\\~TmpData";
  if (!wxDirExists(zip_cache.GetCStr()))
    wxMkdir(zip_cache.GetCStr());
  tmpDir = zip_cache;

  wxString path, name, ext, complete_name, zfile, out_file;
  wxSplitPath(ZipFile.GetCStr(),&path,&name,&ext);
  complete_name = name + "." + ext;

  wxFSFile *zfileStream;
  wxZlibInputStream *zip_is;
  wxString pkg = "#zip:";
  wxString header_name = complete_name + pkg;
  int length_header_name = header_name.Length();
  bool enable_mid = false;

  wxFileSystem *zip_fs = new wxFileSystem();
  zip_fs->ChangePathTo(ZipFile.GetCStr());
  // extract file from the zip archive
  zfile = zip_fs->FindFirst(complete_name+pkg+name+"\\*.*");
  if (zfile == "")
  {
    enable_mid = true;
    zfile = zip_fs->FindFirst(complete_name+pkg+"\\*.*");
  }
  if (zfile == "")
  {
    zip_fs->CleanUpHandlers();
    cppDEL(zip_fs);
    //RemoveTempDirectory();
    return "";
  }
  wxSplitPath(zfile,&path,&name,&ext);
  complete_name = name + "." + ext;
  if (enable_mid)
    complete_name = complete_name.Mid(length_header_name);
  zfileStream = zip_fs->OpenFile(zfile);
  if (zfileStream == NULL)
  {
    zip_fs->CleanUpHandlers();
    cppDEL(zip_fs);
    //RemoveTempDirectory();
    return "";
  }
  zip_is = (wxZlibInputStream *)zfileStream->GetStream();
  out_file = tmpDir + "\\" + complete_name;
  char *buf;
  int s_size;
  std::ofstream out_file_stream;

  if(ext == "msf")
  {
    MSFFile = out_file;
    out_file_stream.open(out_file, std::ios_base::out);
  }
  else
  {
    out_file_stream.open(out_file, std::ios_base::binary);
  }
  s_size = zip_is->GetSize();
  buf = new char[s_size];
  zip_is->Read(buf,s_size);
  out_file_stream.write(buf, s_size);
  out_file_stream.close();
  delete[] buf;

  zfileStream->UnRef();
  delete zfileStream;

  while ((zfile = zip_fs->FindNext()) != "")
  {
    zfileStream = zip_fs->OpenFile(zfile);
    if (zfileStream == NULL)
    {
      zip_fs->CleanUpHandlers();
      cppDEL(zip_fs);
      //RemoveTempDirectory();
      return "";
    }
    zip_is = (wxZlibInputStream *)zfileStream->GetStream();
    wxSplitPath(zfile,&path,&name,&ext);
    complete_name = name + "." + ext;
    if (enable_mid)
      complete_name = complete_name.Mid(length_header_name);
    out_file = tmpDir + "\\" + complete_name;
    if(ext == "msf")
    {
      MSFFile = out_file;
      out_file_stream.open(out_file, std::ios_base::out);
    }
    else
      out_file_stream.open(out_file, std::ios_base::binary);
    s_size = zip_is->GetSize();
    buf = new char[s_size];
    zip_is->Read(buf,s_size);
    out_file_stream.write(buf, s_size);
    out_file_stream.close();
    delete[] buf;
    zfileStream->UnRef();
    delete zfileStream;
  }

  zip_fs->ChangePathTo(tmpDir.GetCStr(), TRUE);
  //zip_fs->CleanUpHandlers();
  cppDEL(zip_fs);

  if (MSFFile == "")
  {
    //if (!m_TestMode)
    mafMessage(_("compressed archive is not a valid msf file!"), _("Error"));
    return "";
  }

  /*wxString working_dir;
  working_dir = mafGetApplicationDirectory().c_str();
  wxSetWorkingDirectory(tmpDir.GetCStr());*/

  return MSFFile;
}
//----------------------------------------------------------------------------
void iOpAddProsthesisToDB::OpStop(int result)
//----------------------------------------------------------------------------
{
  mafEventMacro(mafEvent(this,result));
}
//----------------------------------------------------------------------------
void iOpAddProsthesisToDB::OpDo()
//----------------------------------------------------------------------------
{
}
