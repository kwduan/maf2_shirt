/*=========================================================================
  Program:   iPose
  Module:    $RCSfile: iOpROMPreprocessing.cpp,v $
  Language:  C++
  Date:      $Date: 2012-03-29 08:58:52 $
  Version:   $Revision: 1.1.2.4 $
  Authors:   Matteo Giacomoni
==========================================================================
  Copyright (c) 2001/2005 
  SCS s.r.l. - BioComputing Competence Centre ( www.scsolutions.it - www.b3c.it)
=========================================================================*/

#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iOpROMPreprocessing.h"
#include "wx/busyinfo.h"

#include "iDecl.h"
#include "mafSmartPointer.h"
#include "mafNode.h"
#include "mafViewVTK.h"
#include "mafEvent.h"
#include "mafMatrix.h"
#include "mafVME.h"
#include "mmaMaterial.h"
#include "mafVMEGizmo.h"
#include "mafTransform.h"
#include "mafGUI.h"
#include "mafPipeIsosurface.h"
#include "mafSceneGraph.h"
#include "mafSceneNode.h"
#include "mafVMESurface.h"
#include "mafVMERoot.h"
#include "mafNodeIterator.h"
#include "mafTagArray.h"
#include "mafTagItem.h"
#include "mafVMEGizmo.h"
#include "mafGizmoRotate.h"
#include "mafGizmoTranslate.h"
#include "mafInteractorGenericMouse.h"
#include "mafInteractorPicker.h"
#include "mafTransform.h"
#include "mafGUICrossIncremental.h"
#include "mafGUIFloatSlider.h"
#include "mafVMEGroup.h"
// #include "hipVMEProsthesisComponent.h"

#include "vtkMAFSmartPointer.h"
#include "vtkProperty.h"
#include "vtkGlyph3D.h"
#include "vtkArrowSource.h"
#include "vtkPlaneSource.h"
#include "vtkPlane.h"
#include "vtkAppendPolyData.h"
#include "vtkPolyData.h"
#include "vtkPolyDataConnectivityFilter.h"
#include "vtkCleanPolyData.h"
#include "vtkTriangleFilter.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkMAFClipSurfaceBoundingBox.h"
#include "vtkLinearTransform.h"
#include "vtkTransform.h"
#include "vtkOBBTree.h"
#include "vtkPolyDataNormals.h"
#include "vtkPointData.h"
#include "vtkSphereSource.h"
#include "vtkMath.h"
#include "vtkClipPolyData.h"
#include "vtkMAFImplicitPolyData.h"
#include "vtkLinearSubdivisionFilter.h"
#include "vtkDecimatePro.h"
#include "vtkTextureMapToSphere.h"

#include <wx/busyinfo.h>

//----------------------------------------------------------------------------
mafCxxTypeMacro(hipOpROMPreProcessing);
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
hipOpROMPreProcessing::hipOpROMPreProcessing(wxString label) :
mafOp(label)
//----------------------------------------------------------------------------
{
  m_OpType  = OPTYPE_OP;
  m_Canundo = true;

  m_View = NULL;
  m_GizmoImplicitPlane = NULL;
  m_PlaneSource = NULL;
  m_ExtractedIsosurface = NULL;
  m_ClipperPlane = NULL;
  m_Gizmo = NULL;
  m_ContainerGui = NULL;
  m_ClipGui = NULL;
  m_IsoSurfaceGui = NULL;
  m_FemoralSurface = NULL;
  m_AcetabularSurface = NULL;
  m_GizmoRotate = NULL;
  m_Picker = NULL;
  m_Sphere = NULL;
  m_SphereVTK = NULL;
  m_BooleanGui = NULL;
  m_SphereRadius = 0.0;
  m_Decimate = TRUE;
  m_ShowGizmos = TRUE;
  m_FemoralProsthesisGroup = NULL;
  m_AcetabularProsthesisGroup = NULL;
  m_PickingMode = FALSE;
  m_AcetabularSurfaceRemoved = NULL;
  m_AcetabularSurfaceHelperForBoolean = NULL;

  m_PositionClipPlane[0] = m_PositionClipPlane[1] = m_PositionClipPlane[2] = 0.0;
  m_OrientationClipPlane[0] = m_OrientationClipPlane[1] = m_OrientationClipPlane[2] = 0.0;
  m_SphereCenter[0] = m_SphereCenter[1] = m_SphereCenter[2] = 0.0;
  m_StepsCenterTranslation[0] = m_StepsCenterTranslation[1] = m_StepsCenterTranslation[2] = 1.0;
  m_StepRadius = 1.0;
  
}
//----------------------------------------------------------------------------
hipOpROMPreProcessing::~hipOpROMPreProcessing( ) 
//----------------------------------------------------------------------------
{
  vtkDEL(m_ClipperPlane);
  vtkDEL(m_PlaneSource);
  vtkDEL(m_Gizmo);
  mafDEL(m_Picker);
  vtkDEL(m_SphereVTK);

  for (int i=0;i<m_PickedPoints.size();i++)
  {
    delete []m_PickedPoints[i];
    vtkDEL(m_SpheresPicked[i]);
  }
}
//----------------------------------------------------------------------------
bool hipOpROMPreProcessing::Accept(mafNode *node)
//----------------------------------------------------------------------------
{
  int numAcetabularProsthesis = 0;
  int numFemoralProsthesis = 0;
  mafVMERoot *root = mafVMERoot::SafeDownCast(node->GetRoot());
  mafNodeIterator *iter = root->NewIterator();
  for (mafNode *Inode = iter->GetFirstNode(); Inode; Inode = iter->GetNextNode())
  {
    if (Inode->GetTagArray()->IsTagPresent(TAG_PROSTHESIS_TYPE_ACETABULAR))
    {
      m_AcetabularProsthesisGroup = mafVMEGroup::SafeDownCast(Inode);

      numAcetabularProsthesis++;
    }
    else if (Inode->GetTagArray()->IsTagPresent(TAG_PROSTHESIS_TYPE_FEMORAL))
    {
      m_FemoralProsthesisGroup= mafVMEGroup::SafeDownCast(Inode);

      numFemoralProsthesis++;
    }
  }

  iter->Delete();

  return node && node->IsA("mafVMEVolumeGray") && numFemoralProsthesis > 0 && numAcetabularProsthesis > 0;
}
//----------------------------------------------------------------------------
mafOp* hipOpROMPreProcessing::Copy()   
//----------------------------------------------------------------------------
{
  hipOpROMPreProcessing *cp = new hipOpROMPreProcessing(m_Label);
  return cp;
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::OpRun()   
//----------------------------------------------------------------------------
{
  int numAcetabularProsthesis = 0;
  int numFemoralProsthesis = 0;
  mafVMERoot *root = mafVMERoot::SafeDownCast(m_Input->GetRoot());
  mafNodeIterator *iter = root->NewIterator();
  for (mafNode *Inode = iter->GetFirstNode(); Inode; Inode = iter->GetNextNode())
  {
    if (Inode->GetTagArray()->IsTagPresent(TAG_PROSTHESIS_TYPE_ACETABULAR))
    {
      m_AcetabularProsthesisGroup = mafVMEGroup::SafeDownCast(Inode);

      numAcetabularProsthesis++;
    }
    else if (Inode->GetTagArray()->IsTagPresent(TAG_PROSTHESIS_TYPE_FEMORAL))
    {
      m_FemoralProsthesisGroup= mafVMEGroup::SafeDownCast(Inode);

      numFemoralProsthesis++;
    }
  }

  iter->Delete();

  if (m_FemoralProsthesisGroup == NULL || m_AcetabularProsthesisGroup == NULL)
  {
    wxMessageBox(_("Acetabular and/or Femoral prosthesis missing"));
    OpStop(OP_RUN_CANCEL);
    return;
  }

  //more acetabular prosthesis or more than 1
  if (numAcetabularProsthesis > 1)
  {
    mafEvent e;
    mafString title = _("Choose Acetabular Prosthesis");
    e.SetArg((long)&hipOpROMPreProcessing::AcceptAcetabularProsthesis);
    e.SetString(&title);
    e.SetId(VME_CHOOSE);
    mafEventMacro(e);
    m_AcetabularProsthesisGroup = mafVMEGroup::SafeDownCast(e.GetVme());
    if (m_AcetabularProsthesisGroup == NULL)
    {
      wxMessageBox(_("Inputs incorrect"));
      OpStop(OP_RUN_CANCEL);
      return;
    }
    mafVMEGroup::SafeDownCast(m_AcetabularProsthesisGroup)->GetOutput()->Update();
    mafVMEGroup::SafeDownCast(m_AcetabularProsthesisGroup)->Update();
  }
  //more femoral prosthesis or more than 1
  if (numFemoralProsthesis > 1)
  {
    mafEvent e;
    mafString title = _("Choose Femoral Prosthesis");
    e.SetArg((long)&hipOpROMPreProcessing::AcceptFemoralProsthesis);
    e.SetString(&title);
    e.SetId(VME_CHOOSE);
    mafEventMacro(e);
    m_FemoralProsthesisGroup = mafVMEGroup::SafeDownCast(e.GetVme());
    if (m_FemoralProsthesisGroup == NULL)
    {
      wxMessageBox(_("Inputs incorrect"));
      OpStop(OP_RUN_CANCEL);
      return;
    }
    mafVMEGroup::SafeDownCast(m_FemoralProsthesisGroup)->GetOutput()->Update();
    mafVMEGroup::SafeDownCast(m_FemoralProsthesisGroup)->Update();
  }

  mafEvent eView;
  eView.SetSender(this);
  eView.SetId(ID_SHOW_HIP_ROM_VIEW);
  //Open the rom view
  mafEventMacro(eView);

  m_View = mafViewVTK::SafeDownCast(eView.GetView());

  if (m_View == NULL)
  {
    OpStop(OP_RUN_CANCEL);
    return;
  }

  CreateGui();

  m_View->VmeShow(m_Input,true);
  mafPipeIsosurface *pipe;
  pipe = mafPipeIsosurface::SafeDownCast(m_View->GetSceneGraph()->Vme2Node(m_Input)->m_Pipe);
  pipe->GetGui();
  pipe->SetContourValue(250.0);
  m_View->CameraUpdate();

}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::OpStop(int result)
//----------------------------------------------------------------------------
{
  if (m_AcetabularSurface)
  {
  	m_AcetabularSurface->SetBehavior(NULL);
  }

  if (result == OP_RUN_CANCEL)
  {
    if (m_AcetabularSurface)
    {
      m_AcetabularSurface->ReparentTo(NULL);
    }
    if (m_FemoralSurface)
    {
      m_FemoralSurface->ReparentTo(NULL);
    }
  }

  mafDEL(m_FemoralSurface);
  mafDEL(m_AcetabularSurface);

  cppDEL(m_GizmoRotate);
  mafDEL(m_GizmoImplicitPlane);
  if (m_ExtractedIsosurface)
  {
    m_ExtractedIsosurface->ReparentTo(NULL);
  }
  mafDEL(m_ExtractedIsosurface);

  if (m_Sphere)
  {
    m_Sphere->ReparentTo(NULL);
  }
  mafDEL(m_Sphere);

  if (m_AcetabularSurfaceHelperForBoolean)
  {
    m_AcetabularSurfaceHelperForBoolean->ReparentTo(NULL);
  }
  mafDEL(m_AcetabularSurfaceHelperForBoolean);

  if (m_AcetabularSurfaceRemoved)
  {
    m_AcetabularSurfaceRemoved->ReparentTo(NULL);
  }
  mafDEL(m_AcetabularSurfaceRemoved);

  mafEventMacro(mafEvent(this,ID_CLOSE_HIP_ROM_VIEW));
  if (m_Gui)
  {  
    HideGui();
  }
  mafEventMacro(mafEvent(this,result));
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::ShowClipPlane(bool show)
  //----------------------------------------------------------------------------
{
  if(show)
  {
    if(m_ClipperPlane == NULL)
    {
      double b[6];
      ((mafVME *)m_Input)->GetOutput()->GetVMEBounds(b);

      // bounding box dim
      double xdim = b[1] - b[0];
      double ydim = b[3] - b[2];
      double zdim = b[5] - b[4];

      double m_PlaneWidth = 550;
      double m_PlaneHeight = 550;

      // create the gizmo plane on the z = 0 plane
      vtkNEW(m_PlaneSource);
      m_PlaneSource->SetPoint1(m_PlaneWidth/2,-m_PlaneHeight/2, 0);
      m_PlaneSource->SetPoint2(-m_PlaneWidth/2, m_PlaneHeight/2, 0);
      m_PlaneSource->SetOrigin(-m_PlaneWidth/2,-m_PlaneHeight/2, 0);
      m_PlaneSource->Update();

      vtkNEW(m_ClipperPlane);
      m_ClipperPlane->SetOrigin(m_PlaneSource->GetOrigin());
      m_ClipperPlane->SetNormal(m_PlaneSource->GetNormal());

      vtkMAFSmartPointer<vtkArrowSource> arrowShape;
      arrowShape->SetShaftResolution(40);
      arrowShape->SetTipResolution(40);

      vtkMAFSmartPointer<vtkGlyph3D> arrow;
      arrow->SetInput(m_PlaneSource->GetOutput());
      arrow->SetSource(arrowShape->GetOutput());
      arrow->SetVectorModeToUseNormal();

      int m_ClipInside = FALSE;
      int clip_sign = m_ClipInside ? 1 : -1;
      arrow->SetScaleFactor(clip_sign * abs(zdim/10.0));
      arrow->Update();

      vtkNEW(m_Gizmo);
      m_Gizmo->AddInput(m_PlaneSource->GetOutput());
      m_Gizmo->AddInput(arrow->GetOutput());
      m_Gizmo->Update();

      mafNEW(m_GizmoImplicitPlane);
      m_GizmoImplicitPlane->SetData(m_Gizmo->GetOutput());
      m_GizmoImplicitPlane->SetName("implicit plane gizmo");
      m_GizmoImplicitPlane->ReparentTo(mafVME::SafeDownCast(m_Input->GetRoot()));

      // position the plane
      mafSmartPointer<mafTransform> currTr;
      currTr->Translate((b[0] + b[1]) / 2, (b[2] + b[3]) / 2,(b[4] + b[5]) / 2 , POST_MULTIPLY);
      currTr->Update();

      mafMatrix mat;
      mat.DeepCopy(&currTr->GetMatrix());
      mat.SetTimeStamp(((mafVME *)m_Input)->GetTimeStamp());

      m_GizmoImplicitPlane->SetAbsMatrix(mat);

      m_View->VmeShow(m_GizmoImplicitPlane,true);
    }
    mmaMaterial *material = m_GizmoImplicitPlane->GetMaterial();
    material->m_Prop->SetOpacity(0.5);
    material->m_Opacity = material->m_Prop->GetOpacity();

    if(!m_PlaneCreated)
    {
      material->m_Prop->SetColor(0.2,0.2,0.8);
      material->m_Prop->GetDiffuseColor(material->m_Diffuse);
      m_PlaneCreated = true;
    }
  }
  else
  {
    if(m_GizmoImplicitPlane != NULL)
    {
      mmaMaterial *material = m_GizmoImplicitPlane->GetMaterial();
      material->m_Prop->SetOpacity(0);
      material->m_Opacity = material->m_Prop->GetOpacity();
    }
  }

  m_View->CameraUpdate();
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::CreateGui()
//----------------------------------------------------------------------------
{
  m_Gui = new mafGUI(this);
  m_ContainerGui = new mafGUI(this);

  m_IsoSurfaceGui = new mafGUI(this);

  double range[2] = {0, 0};
  mafVME::SafeDownCast(m_Input)->GetOutput()->GetVTKData()->GetScalarRange(range);

  m_ContourValue = 250.0;
  m_ContourSlider = m_IsoSurfaceGui->FloatSlider(ID_CONTOUR_VALUE,_("contour"), &m_ContourValue,range[0],range[1]);
  m_TransparencyValue = 1.0;
  m_TransparencySlider = m_IsoSurfaceGui->FloatSlider(ID_TRANSPARENCY_VALUE,_("transparency"), &m_TransparencyValue,0.0,1.0);

  m_IsoSurfaceGui->Bool(ID_DECIMATE,_("Apply decimate"),&m_Decimate,1);
  m_IsoSurfaceGui->Button(ID_ACCEPT_ISOSURFACE,_("Accept Isosurface"),"",_("Accept the current isosurface"));

  m_ContainerGui->AddGui(m_IsoSurfaceGui);
  m_ContainerGui->FitGui();
  m_ContainerGui->Update();

  m_Gui->AddGui(m_ContainerGui);
  m_Gui->Button(ID_CANCEL,_("Cancel"));
  /*m_Gui->OkCancel();*/

  m_Gui->FitGui();
  m_Gui->Update();

  ShowGui();
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::OnEvent( mafEventBase *maf_event )
//----------------------------------------------------------------------------
{
  if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
  {
    switch(maf_event->GetId())
    {
    case ID_ENABLE_PICKER:
      {
        m_AcetabularSurface->SetBehavior(m_PickingMode == TRUE ? m_Picker : NULL);
//         if (m_PickingMode == TRUE)
//         {
//           m_AcetabularSurface->SetBehavior(m_Picker);
//         }
//         else
//         {
//           m_AcetabularSurface->SetBehavior(NULL);
//         }
      }
      break;
    case ID_CONTOUR_VALUE:
      {
        mafPipeIsosurface *pipe;
        pipe = mafPipeIsosurface::SafeDownCast(m_View->GetSceneGraph()->Vme2Node(m_Input)->m_Pipe);
        if (pipe)
        {
	        pipe->SetContourValue(m_ContourValue);
	        m_View->CameraUpdate();
        }
      }
      break;
    case ID_TRANSPARENCY_VALUE:
      {
        mafPipeIsosurface *pipe;
        pipe = mafPipeIsosurface::SafeDownCast(m_View->GetSceneGraph()->Vme2Node(m_Input)->m_Pipe);
        if (pipe)
        {
          pipe->SetAlphaValue(m_TransparencyValue);
          m_View->CameraUpdate();
        }
      }
      break;
    case ID_TRANSLATE:
      {
        OnTranslateGui();
        Clip();
      }
      break;
    case ID_ACCEPT_BOOLEAN:
      {
        BooleanOperation(true);
		    
        ApplyBoneMaterial(m_AcetabularSurface);
        ApplyBoneMaterial(m_FemoralSurface);

        OpStop(OP_RUN_OK);
      }
      break;
    case ID_SHOW_GIZMOS:
      {
        m_GizmoRotate->Show(m_ShowGizmos == TRUE,m_ShowGizmos == TRUE,false);
        m_View->CameraUpdate();
      }
      break;
    case ID_SPHERE_RADIUS:
    case ID_SPHERE_RADIUS_INC:
    case ID_SPHERE_CENTER_X:
    case ID_SPHERE_CENTER_Y:
    case ID_SPHERE_CENTER_Z:
      {
        if (m_SphereVTK)
        {
	        m_SphereVTK->SetRadius(m_SphereRadius);
	        m_SphereVTK->SetCenter(m_SphereCenter);
	        m_SphereVTK->Update();
	
	        if (m_Sphere == NULL)
	        {
	          mafNEW(m_Sphere);
	          m_Sphere->ReparentTo(m_Input);
	        }
	
	        m_Sphere->SetData(m_SphereVTK->GetOutput(),0.0);
	        m_Sphere->Update();
	
	        m_View->VmeShow(m_Sphere,false);
	        m_View->VmeShow(m_Sphere,true);
	        m_View->CameraUpdate();
	
	        m_BooleanGui->Update();

          BooleanOperation();
        }
      }
      break;
    case VME_PICKED:
      {
        OnVmePicked(e);
      }
      break;
    case ID_ROTATE_X:
    case ID_ROTATE_Y:
      {
        OnRotateGui();
        Clip();
      }
      break;
    case ID_TRANSFORM:
      {    
        // post multiplying matrixes coming from the gizmo to the vme
        // gizmo does not set vme pose  since they cannot scale
        PostMultiplyEventMatrix(maf_event);
        Clip();
      }
      break;
      break;;
    case ID_CLIP:
      {
        Clip();
        InitializeBooleanStep();
      }
      break;
    case ID_ACCEPT_ISOSURFACE:
      {
        AcceptIsosurface();
        InitializeClipStep();        
      }
      break;
    case ID_CANCEL:
      {
        OpStop(OP_RUN_CANCEL);
      }
      break;
    default:
      mafEventMacro(*maf_event);
    }
  }
  else
    mafEventMacro(*maf_event);
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::AcceptIsosurface()
//----------------------------------------------------------------------------
{
  mafPipeIsosurface *pipe;
  pipe = mafPipeIsosurface::SafeDownCast(m_View->GetSceneGraph()->Vme2Node(m_Input)->m_Pipe);
  pipe->SetEnableContourAnalysis(true);

  mafNEW(m_ExtractedIsosurface);
  pipe->ExctractIsosurface(m_ExtractedIsosurface);

  // m_ExtractedIsosurface->ReparentTo(m_Input);

  vtkDataSet *ds = m_ExtractedIsosurface->GetOutput()->GetVTKData();
  if (ds)
  {
    ds->Update();
  }

  vtkMAFSmartPointer<vtkPolyData> poly;
  poly->DeepCopy(vtkPolyData::SafeDownCast(m_ExtractedIsosurface->GetOutput()->GetVTKData()));
  poly->Update();

  vtkMAFSmartPointer<vtkPolyDataConnectivityFilter> connectivityFilter;
  connectivityFilter->SetExtractionModeToLargestRegion();
  connectivityFilter->SetInput(poly);
  connectivityFilter->Update();

  vtkMAFSmartPointer<vtkCleanPolyData> clean;
  clean->SetInput(connectivityFilter->GetOutput());
  clean->ConvertLinesToPointsOff();
  clean->ConvertPolysToLinesOff();
  clean->ConvertStripsToPolysOff();
  clean->Update();

  vtkMAFSmartPointer<vtkTriangleFilter> triangulate;
  triangulate->SetInput(clean->GetOutput());
  triangulate->Update();

  vtkMAFSmartPointer<vtkPolyData> result;

  if (m_Decimate == TRUE)
  {
	  vtkMAFSmartPointer<vtkDecimatePro> decimate;
	  decimate->SetInput(triangulate->GetOutput());
	  decimate->SetTargetReduction((double)1.0 - (50000.0 / poly->GetNumberOfCells()));
	  decimate->Update();

    result->DeepCopy(decimate->GetOutput());
    result->Update();
  }
  else
  {
    result->DeepCopy(triangulate->GetOutput());
    result->Update();
  }

  m_ContainerGui->Remove(m_IsoSurfaceGui);
  m_ContainerGui->FitGui();
  m_ContainerGui->Update();
  m_Gui->FitGui();
  m_Gui->Update();

  m_View->VmeShow(m_Input,false);

  m_ExtractedIsosurface->SetData(result,0.0);
  m_ExtractedIsosurface->Update();

  ApplyBoneMaterial(m_ExtractedIsosurface);

  cppDEL(m_IsoSurfaceGui);

}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::InitializeClipStep()
//----------------------------------------------------------------------------
{
  m_View->PlugVisualPipe("mafVMESurface","mafPipeSurface");

  m_View->VmeShow((mafNode*)m_ExtractedIsosurface,true);

  ShowClipPlane(true);

  if (m_FemoralProsthesisGroup->GetNumberOfChildren() > 2) //has neck component
  {
    mafVMESurface *neck = mafVMESurface::SafeDownCast(m_FemoralProsthesisGroup->GetChild(1));//neck Component
    neck->Update();

    mafMatrix *m = neck->GetOutput()->GetAbsMatrix();
   
    m_GizmoImplicitPlane->SetAbsMatrix(*m);

    mafTransform::GetOrientation(*m, m_OrientationClipPlane);
    mafTransform::GetPosition(*m, m_PositionClipPlane);
  }

  m_GizmoRotate = new mafGizmoRotate(mafVME::SafeDownCast(m_GizmoImplicitPlane), this,false);
  m_GizmoRotate->SetRefSys(m_GizmoImplicitPlane);
  m_GizmoRotate->Show(true,true,false);

  m_GizmoTranslate = new mafGizmoTranslate(mafVME::SafeDownCast(m_GizmoImplicitPlane), this,false);
  m_GizmoTranslate->SetRefSys(m_GizmoImplicitPlane);
  m_GizmoTranslate->Show(false,false,true);

  m_ClipGui = new mafGUI(this);
  m_ClipGui->Label(_("Neck resection"),true);
  m_ClipGui->Divider(2);
  m_ClipGui->Bool(ID_SHOW_GIZMOS,_("Show Gizmos"),&m_ShowGizmos,1);
  m_ClipGui->Label("rotation gizmo abs orientation", true);
  m_ClipGui->Double(ID_ROTATE_X, _("Rotate X"), &m_OrientationClipPlane[0]);
  m_ClipGui->Double(ID_ROTATE_Y, _("Rotate Y"), &m_OrientationClipPlane[1]);
//   m_ClipGui->Divider();
//   m_ClipGui->Double(ID_TRANSLATE, _("Traslate"), &m_PositionClipPlane[2]);
  m_ClipGui->Button(ID_CLIP,_("Resection"),"",_("Clip the current surface using the plane position"));

  m_ContainerGui->AddGui(m_ClipGui);
  m_ContainerGui->FitGui();
  m_ContainerGui->Update();
  m_Gui->FitGui();
  m_Gui->Update();

  /*m_View->CameraReset((mafNode*)m_GizmoRotate);*/

  Clip();
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::Clip()
//----------------------------------------------------------------------------
{

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> transform_plane;
  transform_plane->SetTransform(m_GizmoImplicitPlane->GetAbsMatrixPipe()->GetVTKTransform());
  transform_plane->SetInput(m_PlaneSource->GetOutput());
  transform_plane->Update();

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> transform_data_input;
  transform_data_input->SetTransform(m_ExtractedIsosurface->GetAbsMatrixPipe()->GetVTKTransform());
  transform_data_input->SetInput(vtkPolyData::SafeDownCast(m_ExtractedIsosurface->GetOutput()->GetVTKData()));
  transform_data_input->Update();

  vtkMAFSmartPointer<vtkMAFClipSurfaceBoundingBox> m_ClipperBoundingBox;
  m_ClipperBoundingBox->SetInput(transform_data_input->GetOutput());
  m_ClipperBoundingBox->SetMask(transform_plane->GetOutput());
  m_ClipperBoundingBox->SetClipInside(1);
  m_ClipperBoundingBox->Update();

  vtkMAFSmartPointer<vtkPolyData> femoralPolyData;
  femoralPolyData->DeepCopy(m_ClipperBoundingBox->GetOutput());
  femoralPolyData->Update();

  vtkMAFSmartPointer<vtkPolyDataConnectivityFilter> filter1;
  filter1->SetInput(femoralPolyData);
  filter1->SetExtractionModeToLargestRegion();
  filter1->Update();

  vtkMAFSmartPointer<vtkCleanPolyData> clean1;  
  clean1->SetInput(filter1->GetOutput());
  clean1->ConvertLinesToPointsOff();
  clean1->ConvertPolysToLinesOff();
  clean1->ConvertStripsToPolysOff();
  clean1->Update();

  vtkMAFSmartPointer<vtkTriangleFilter> triangulate1;
  triangulate1->SetInput(clean1->GetOutput());
  triangulate1->Update();

  if (m_FemoralSurface)
  {
    m_View->VmeShow(m_FemoralSurface,false);
    m_FemoralSurface->ReparentTo(NULL);
    mafDEL(m_FemoralSurface);
  }

  mafNEW(m_FemoralSurface);
  m_FemoralSurface->SetData(triangulate1->GetOutput(),0.0);
  m_FemoralSurface->GetMaterial()->m_Diffuse[0] = 0.0;
  m_FemoralSurface->GetMaterial()->m_Diffuse[1] = 1.0;
  m_FemoralSurface->GetMaterial()->m_Diffuse[2] = 0.0;
  m_FemoralSurface->GetMaterial()->UpdateProp();
  m_FemoralSurface->SetName("Femoral");
  m_FemoralSurface->GetTagArray()->SetTag(mafTagItem(TAG_SURFACE_TYPE_FEMORAL,""));
  m_FemoralSurface->ReparentTo(m_Input);
  m_FemoralSurface->Update();

  m_View->VmeShow(m_FemoralSurface,true);

  m_ClipperBoundingBox->SetClipInside(0);
  m_ClipperBoundingBox->Update();

  vtkMAFSmartPointer<vtkPolyData> acetabularPolydata;
  acetabularPolydata->DeepCopy(m_ClipperBoundingBox->GetOutput());
  acetabularPolydata->Update();

  vtkMAFSmartPointer<vtkPolyDataConnectivityFilter> filter2;
  filter2->SetInput(acetabularPolydata);
  filter2->SetExtractionModeToLargestRegion();
  filter2->Update();

  vtkMAFSmartPointer<vtkCleanPolyData> clean2;  
  clean2->SetInput(filter2->GetOutput());
  clean2->ConvertLinesToPointsOff();
  clean2->ConvertPolysToLinesOff();
  clean2->ConvertStripsToPolysOff();
  clean2->Update();

  vtkMAFSmartPointer<vtkTriangleFilter> triangulate2;
  triangulate2->SetInput(clean2->GetOutput());
  triangulate2->Update();

  if (m_AcetabularSurface)
  {
    m_View->VmeShow(m_AcetabularSurface,false);
    m_AcetabularSurface->ReparentTo(NULL);
    mafDEL(m_AcetabularSurface);
  }

  mafNEW(m_AcetabularSurface);
  m_AcetabularSurface->SetData(triangulate2->GetOutput(),0.0);
  m_AcetabularSurface->GetMaterial()->m_Diffuse[0] = 1.0;
  m_AcetabularSurface->GetMaterial()->m_Diffuse[1] = 0.0;
  m_AcetabularSurface->GetMaterial()->m_Diffuse[2] = 0.0;
  m_AcetabularSurface->GetMaterial()->UpdateProp();
  m_AcetabularSurface->SetName("Acetabular");
  m_AcetabularSurface->GetTagArray()->SetTag(mafTagItem(TAG_SURFACE_TYPE_ACETABULAR,""));
  m_AcetabularSurface->ReparentTo(m_Input);
  m_AcetabularSurface->Update();

  m_View->VmeShow(m_AcetabularSurface,true);
  m_View->VmeShow(m_ExtractedIsosurface,false);

//   m_GizmoRotate->Show(false,false,false);
//   cppDEL(m_GizmoRotate);
//   m_GizmoImplicitPlane->ReparentTo(NULL);
//   mafDEL(m_GizmoImplicitPlane);
// 
//   ShowClipPlane(false);

//   m_ContainerGui->Remove(m_ClipGui);
//   m_ContainerGui->FitGui();
//   m_ContainerGui->Update();
//   m_Gui->FitGui();
//   m_Gui->Update();
// 
//   cppDEL(m_ClipGui);
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::PostMultiplyEventMatrix(mafEventBase *maf_event)
//----------------------------------------------------------------------------
{
  if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
  {
    long arg = e->GetArg();

    // handle incoming transform events
    vtkTransform *tr = vtkTransform::New();
    tr->PostMultiply();
    tr->SetMatrix(((mafVME *)m_GizmoImplicitPlane)->GetOutput()->GetAbsMatrix()->GetVTKMatrix());
    tr->Concatenate(e->GetMatrix()->GetVTKMatrix());
    tr->Update();

    mafMatrix absPose;
    absPose.DeepCopy(tr->GetMatrix());
    absPose.SetTimeStamp(0.0);

    if (arg == mafInteractorGenericMouse::MOUSE_MOVE)
    {
      // move vme
      ((mafVME *)m_GizmoImplicitPlane)->SetAbsMatrix(absPose);
      m_GizmoRotate->SetAbsPose(&absPose);
      m_GizmoTranslate->SetAbsPose(&absPose);

      mafTransform::GetOrientation(absPose, m_OrientationClipPlane);
      mafTransform::GetPosition(absPose, m_PositionClipPlane);

      m_ClipGui->Update();
    } 
    /*m_View->CameraUpdate();*/

    // clean up
    tr->Delete();
  }
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::OnRotateGui()
//----------------------------------------------------------------------------
{
  mafMatrix mNew;
  mafTransform::SetOrientation(mNew, m_OrientationClipPlane[0], m_OrientationClipPlane[1], m_OrientationClipPlane[2]);

  mafSmartPointer<mafMatrix> M;
  mafMatrix invOldAbsPose;
  mafSmartPointer<mafMatrix> newAbsPose;

  // incoming matrix is a rotation matrix
  newAbsPose->DeepCopy(m_GizmoRotate->GetAbsPose());
  // copy rotation from incoming matrix
  mafTransform::CopyRotation(mNew, *newAbsPose.GetPointer());

  invOldAbsPose.DeepCopy(m_GizmoRotate->GetAbsPose());
  invOldAbsPose.Invert();

  mafMatrix::Multiply4x4(*newAbsPose.GetPointer(),invOldAbsPose,*M.GetPointer());

  // update gizmo abs pose
  m_GizmoRotate->SetAbsPose(newAbsPose, true);

  mafEvent e2s;
  e2s.SetSender(this);
  e2s.SetMatrix(M);
  e2s.SetId(ID_TRANSFORM);
  e2s.SetArg(mafInteractorGenericMouse::MOUSE_MOVE);

  PostMultiplyEventMatrix(&e2s);
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::InitializeBooleanStep()
//----------------------------------------------------------------------------
{
  m_GizmoRotate->Show(false,false,false);
  m_GizmoTranslate->Show(false,false,false);
  cppDEL(m_GizmoRotate);
  cppDEL(m_GizmoTranslate);
  m_GizmoImplicitPlane->ReparentTo(NULL);
  mafDEL(m_GizmoImplicitPlane);

  ShowClipPlane(false);

  m_ContainerGui->Remove(m_ClipGui);
  m_ContainerGui->FitGui();
  m_ContainerGui->Update();
  m_Gui->FitGui();
  m_Gui->Update();

  cppDEL(m_ClipGui);

  m_BooleanGui = new mafGUI(this);
  m_BooleanGui->Label(_("Acetabular D(igital)-Subtraction"),true);
  m_BooleanGui->Bool(ID_ENABLE_PICKER,_("Enable Picking Mode"),&m_PickingMode,1);
  m_BooleanGui->Label(_("Sphere Center"));
  mafString text[2];
  if (strcmp(m_Input->GetTagArray()->GetTag("VOLUME_SIDE")->GetValue(),"RIGHT") == 0)
  {
    text[0] = "L";
    text[1] = "M";
  }
  else
  {
    text[0] = "M";
    text[1] = "L";
  }
  m_BooleanGui->CrossIncremental(ID_SPHERE_CENTER_X,_("X"),&m_StepsCenterTranslation[0],NULL,&m_SphereCenter[0],mafGUICrossIncremental::ID_LEFT_RIGHT_LAYOUT,"",true,false,-1,NULL,text);
  text[0] = "P";
  text[1] = "A";
  m_BooleanGui->CrossIncremental(ID_SPHERE_CENTER_Y,_("Y"),&m_StepsCenterTranslation[1],NULL,&m_SphereCenter[1],mafGUICrossIncremental::ID_LEFT_RIGHT_LAYOUT,"",true,false,-1,NULL,text);
  text[0] = "I";
  text[1] = "S";
  m_BooleanGui->CrossIncremental(ID_SPHERE_CENTER_Z,_("Z"),&m_StepsCenterTranslation[2],NULL,&m_SphereCenter[2],mafGUICrossIncremental::ID_LEFT_RIGHT_LAYOUT,"",true,false,-1,NULL,text);
  
  m_BooleanGui->Label(_("Sphere Radius"));
  m_BooleanGui->Double(ID_SPHERE_RADIUS,"",&m_SphereRadius,0.0);
  m_BooleanGui->CrossIncremental(ID_SPHERE_RADIUS_INC,"",&m_StepRadius,NULL,&m_SphereRadius,mafGUICrossIncremental::ID_LEFT_RIGHT_LAYOUT);
  m_BooleanGui->Button(ID_ACCEPT_BOOLEAN,_("Accept"));

  m_ContainerGui->AddGui(m_BooleanGui);
  m_ContainerGui->FitGui();
  m_ContainerGui->Update();
  m_Gui->FitGui();
  m_Gui->Update();

  m_View->VmeShow(m_AcetabularSurface,true);
  m_View->VmeShow(m_FemoralSurface,false);
  m_View->CameraReset((mafNode*)m_AcetabularSurface);
  m_View->CameraUpdate();

  mafMatrix *refSys = m_AcetabularProsthesisGroup->GetOutput()->GetAbsMatrix();
  mafTransform::GetPosition(*refSys,m_SphereCenter);

  double b[6];
  mafVMESurface::SafeDownCast(m_AcetabularProsthesisGroup->GetChild(0))->GetOutput()->Update();
  mafVMESurface::SafeDownCast(m_AcetabularProsthesisGroup->GetChild(0))->GetOutput()->GetVTKData()->Update();
  mafVMESurface::SafeDownCast(m_AcetabularProsthesisGroup->GetChild(0))->GetOutput()->GetVTKData()->GetBounds(b);

  m_SphereRadius = (b[1] - b[0]) > (b[3] - b[2]) ? (b[1] - b[0]) : ((b[3] - b[2]) > (b[5] - b[4]) ? (b[3] - b[2]) : (b[5] - b[4]));
  m_SphereRadius /= 2.0;

  if (m_SphereVTK == NULL)
  {
    vtkNEW(m_SphereVTK);
    m_SphereVTK->SetPhiResolution(20);
    m_SphereVTK->SetThetaResolution(20);
  }
  m_SphereVTK->SetCenter(m_SphereCenter);
  m_SphereVTK->SetRadius(m_SphereRadius);
  m_SphereVTK->Update();

  if (m_Sphere == NULL)
  {
    mafNEW(m_Sphere);
    m_Sphere->ReparentTo(m_Input);
    m_AcetabularSurface->GetMaterial()->m_Opacity = 0.45;
    m_AcetabularSurface->GetMaterial()->UpdateProp();
  }
  else
  {
    m_View->VmeShow(m_Sphere,false);
  }

  m_Sphere->SetData(m_SphereVTK->GetOutput(),0.0);
  m_Sphere->Update();
  
  m_View->VmeShow(m_Sphere,true);
  m_View->CameraUpdate();

  m_BooleanGui->Update();  

  //create the picker
  mafNEW(m_Picker);
  m_Picker->SetListener(this);
  if (m_PickingMode == TRUE)
  {
  	m_AcetabularSurface->SetBehavior(m_Picker);
  }

  m_View->VmeShow(m_AcetabularSurface,false);

  mafNEW(m_AcetabularSurfaceHelperForBoolean);
  m_AcetabularSurfaceHelperForBoolean->DeepCopy(m_AcetabularSurface);
  m_AcetabularSurfaceHelperForBoolean->ReparentTo(m_AcetabularSurface->GetParent());
  mafNEW(m_AcetabularSurfaceRemoved);
  m_AcetabularSurfaceRemoved->ReparentTo(m_Input);

  m_AcetabularSurfaceRemoved->GetMaterial()->m_Diffuse[0] = 1.0;
  m_AcetabularSurfaceRemoved->GetMaterial()->m_Diffuse[1] = 0.0;
  m_AcetabularSurfaceRemoved->GetMaterial()->m_Diffuse[2] = 0.0;
  m_AcetabularSurfaceRemoved->GetMaterial()->UpdateProp();

  m_AcetabularSurfaceHelperForBoolean->GetMaterial()->m_Diffuse[0] = 0.0;
  m_AcetabularSurfaceHelperForBoolean->GetMaterial()->m_Diffuse[1] = 1.0;
  m_AcetabularSurfaceHelperForBoolean->GetMaterial()->m_Diffuse[2] = 0.0;
  m_AcetabularSurfaceHelperForBoolean->GetMaterial()->UpdateProp();

  m_AcetabularSurface->GetMaterial()->m_Diffuse[0] = 0.0;
  m_AcetabularSurface->GetMaterial()->m_Diffuse[1] = 1.0;
  m_AcetabularSurface->GetMaterial()->m_Diffuse[2] = 0.0;
  m_AcetabularSurface->GetMaterial()->UpdateProp();

  m_AcetabularSurfaceRemoved->GetMaterial()->m_Diffuse[0] = 1.0;
  m_AcetabularSurfaceRemoved->GetMaterial()->m_Diffuse[1] = 0.0;
  m_AcetabularSurfaceRemoved->GetMaterial()->m_Diffuse[2] = 0.0;
  m_AcetabularSurfaceRemoved->GetMaterial()->UpdateProp();
  
  BooleanOperation();
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::OnVmePicked(mafEvent *e)
//----------------------------------------------------------------------------
{
  vtkPolyData *polydata = vtkPolyData::SafeDownCast(m_AcetabularSurface->GetOutput()->GetVTKData());
  polydata->Update();

  vtkMAFSmartPointer<vtkTransform> t;
  t->SetMatrix(m_AcetabularSurface->GetOutput()->GetAbsMatrix()->GetVTKMatrix());
  t->Update();

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> tr;
  tr->SetTransform(t);
  tr->SetInput(polydata);
  tr->Update();

  double b[6];
  tr->GetOutput()->GetBounds(b);

  double diff = (b[1] - b[0]) > (b[3] - b[2]) ? (b[1] - b[0]) : ((b[3] - b[2]) > (b[5] - b[4]) ? (b[3] - b[2]) : (b[5] - b[4]));

  double *pointPicked = new double[3];
  tr->GetOutput()->GetPoint(e->GetArg(),pointPicked);
  m_PickedPoints.push_back(pointPicked);

  vtkMAFSmartPointer<vtkSphereSource> sphereVTK;
  sphereVTK->SetRadius(diff/150.0);
  sphereVTK->SetCenter(pointPicked);
  mafVMESurface *sphere;
  mafNEW(sphere);
  sphere->SetData(sphereVTK->GetOutput(),0.0);
  sphere->ReparentTo(m_Input);
  sphere->Update();
  m_SpheresPicked.push_back(sphere);
  m_View->VmeShow(sphere,true);

  if (m_PickedPoints.size()>2)
  {
    vtkMAFSmartPointer<vtkPoints> pts;
    for (int i=0;i<m_PickedPoints.size();i++)
    {
      pts->InsertNextPoint(m_PickedPoints[i][0],m_PickedPoints[i][1],m_PickedPoints[i][2]);
    }
    vtkMAFSmartPointer<vtkPolyData> tmp;
    tmp->SetPoints(pts);
    tmp->Update();
    vtkMAFSmartPointer<vtkTextureMapToSphere> interpolate;
    interpolate->SetInput(tmp);
    interpolate->AutomaticSphereGenerationOn();
    interpolate->PreventSeamOff();
    interpolate->Update();
    interpolate->GetCenter(m_SphereCenter);

    //Compute radius medium
    double sum = 0.0;
    for (int i=0;i<m_PickedPoints.size();i++)
    {
      sum += vtkMath::Distance2BetweenPoints(m_PickedPoints[i],m_SphereCenter);
    }
    sum /= m_PickedPoints.size();
    m_SphereRadius = sqrt(sum);

    if (m_SphereVTK == NULL)
    {
      vtkNEW(m_SphereVTK);
      m_SphereVTK->SetPhiResolution(20);
      m_SphereVTK->SetThetaResolution(20);
    }
    m_SphereVTK->SetCenter(m_SphereCenter);
    m_SphereVTK->SetRadius(m_SphereRadius);
    m_SphereVTK->Update();

    if (m_Sphere == NULL)
    {
      mafNEW(m_Sphere);
      m_Sphere->ReparentTo(m_Input);
      m_AcetabularSurface->GetMaterial()->m_Opacity = 0.45;
      m_AcetabularSurface->GetMaterial()->UpdateProp();
    }
    else
    {
      m_View->VmeShow(m_Sphere,false);
    }

    m_Sphere->SetData(m_SphereVTK->GetOutput(),0.0);
    m_Sphere->Update();

    m_View->VmeShow(m_Sphere,true);
    m_View->CameraUpdate();
  }


  /*vtkMAFSmartPointer<vtkPolyDataNormals> normalFilter;
  normalFilter->SetInput(tr->GetOutput());

  normalFilter->ComputePointNormalsOn();
  normalFilter->SplittingOff();
  normalFilter->FlipNormalsOff();
  normalFilter->SetFeatureAngle(30);
  normalFilter->Update();

  double *normal = normalFilter->GetOutput()->GetPointData()->GetNormals()->GetTuple3(e->GetArg());
  int sign[3];
  sign[0]=normal[0]<0? -1 :1;
  sign[1]=normal[1]<0? -1 :1;
  sign[2]=normal[2]<0? -1 :1;

  double point1[3];
  normalFilter->GetOutput()->GetPoint(e->GetArg(),point1);
  //point1=m_Centers->GetPoint(m_CellFilter->GetIdMarkedCell(0));
  double point2[3];
  point2[0] = point1[0] - (normal[0]*diff);
  point2[1] = point1[1] - (normal[1]*diff);
  point2[2] = point1[2] - (normal[2]*diff);

  vtkMAFSmartPointer<vtkOBBTree> OBB;
  OBB->SetDataSet(tr->GetOutput());
  OBB->CacheCellBoundsOn();
  OBB->BuildLocator();

  vtkMAFSmartPointer<vtkPoints> p;
  OBB->IntersectWithLine(point1,point2,p,NULL);

  if (p->GetNumberOfPoints() > 0)
  {
    m_SphereCenter[0] = (point1[0] + p->GetPoint(p->GetNumberOfPoints()-1)[0])/2.0;
    m_SphereCenter[1] = (point1[1] + p->GetPoint(p->GetNumberOfPoints()-1)[1])/2.0;
    m_SphereCenter[2] = (point1[2] + p->GetPoint(p->GetNumberOfPoints()-1)[2])/2.0;
    if (m_SphereVTK == NULL)
    {
      vtkNEW(m_SphereVTK);
      m_SphereVTK->SetPhiResolution(20);
      m_SphereVTK->SetThetaResolution(20);
    }
    else
    {
      m_SphereCenter[0] = (m_SphereVTK->GetCenter()[0] + m_SphereCenter[0])/2.0;
      m_SphereCenter[1] = (m_SphereVTK->GetCenter()[1] + m_SphereCenter[1])/2.0;
      m_SphereCenter[2] = (m_SphereVTK->GetCenter()[2] + m_SphereCenter[2])/2.0;
    }

    if (m_SphereRadius == 0.0)
    {
    	m_SphereRadius = sqrt(vtkMath::Distance2BetweenPoints(point1, m_SphereCenter));
    }
	  
	  m_SphereVTK->SetRadius(m_SphereRadius);
	  m_SphereVTK->SetCenter(m_SphereCenter);
	  m_SphereVTK->Update();

    if (m_Sphere == NULL)
    {
      mafNEW(m_Sphere);
      m_Sphere->ReparentTo(m_Input);
    }
    else
    {
	    m_View->VmeShow(m_Sphere,false);
    }

	  m_Sphere->SetData(m_SphereVTK->GetOutput(),0.0);
	  m_Sphere->Update();
	
	  m_View->VmeShow(m_Sphere,true);
	  m_View->CameraUpdate();

    m_BooleanGui->Update();
  }*/
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::BooleanOperation(bool saveResult /* = false */)
//----------------------------------------------------------------------------
{
  if(m_Sphere && m_AcetabularSurface)
  {
    /*wxBusyInfo wait("Please wait...");*/
    m_View->VmeShow(m_AcetabularSurfaceHelperForBoolean,false);
    m_View->VmeShow(m_AcetabularSurfaceRemoved,false);
    vtkPolyData *dataFirstOperator = vtkPolyData::SafeDownCast(m_AcetabularSurface->GetOutput()->GetVTKData());
    vtkPolyData *dataSecondOperator = vtkPolyData::SafeDownCast(m_Sphere->GetOutput()->GetVTKData());
    if(dataFirstOperator && dataSecondOperator)
    {
      vtkMAFSmartPointer<vtkTransformPolyDataFilter> transformFirstDataInput;
      transformFirstDataInput->SetTransform((vtkAbstractTransform *)((mafVME *)m_AcetabularSurface)->GetAbsMatrixPipe()->GetVTKTransform());
      transformFirstDataInput->SetInput((vtkPolyData *)((mafVME *)m_AcetabularSurface)->GetOutput()->GetVTKData());
      transformFirstDataInput->Update();

      vtkMAFSmartPointer<vtkTransformPolyDataFilter> transformSecondDataInput;
      transformSecondDataInput->SetTransform((vtkAbstractTransform *)((mafVME *)m_Sphere)->GetAbsMatrixPipe()->GetVTKTransform());
      transformSecondDataInput->SetInput((vtkPolyData *)((mafVME *)m_Sphere)->GetOutput()->GetVTKData());
      transformSecondDataInput->Update();

      //Intersection

      // clip input surface by another surface
      // triangulate input for subdivision filter
      vtkMAFSmartPointer<vtkTriangleFilter> triangles;
      triangles->SetInput(transformFirstDataInput->GetOutput());
      triangles->Update();

      // subdivide triangles in sphere 1 to get better clipping
//       vtkMAFSmartPointer<vtkLinearSubdivisionFilter> subdivider;
//       subdivider->SetInput(triangles->GetOutput());
//       subdivider->SetNumberOfSubdivisions(3);   //  use  this  (0-3+)  to  see improvement in clipping
//       subdivider->Update();

      vtkMAFSmartPointer<vtkMAFImplicitPolyData> implicitPolyData;
      implicitPolyData->SetInput(transformSecondDataInput->GetOutput());

      vtkMAFSmartPointer<vtkClipPolyData> clipper;
      clipper->SetInput(triangles->GetOutput());
      clipper->SetClipFunction(implicitPolyData);

      clipper->SetGenerateClipScalars(0); // 0 outputs input data scalars, 1 outputs implicit function values
      clipper->SetInsideOut(0);  // use 0/1 to reverse sense of clipping
      clipper->SetValue(0);               // use this to control clip distance from clipper function to surface
      clipper->Update();

      //End Intersection

      vtkMAFSmartPointer<vtkTransformPolyDataFilter> transformResultDataInput;
      transformResultDataInput->SetTransform((vtkAbstractTransform *)((mafVME *)m_AcetabularSurface)->GetAbsMatrixPipe()->GetVTKTransform()->GetInverse());
      transformResultDataInput->SetInput(clipper->GetOutput());
      transformResultDataInput->Update();

      vtkMAFSmartPointer<vtkPolyDataConnectivityFilter> connectivity;
      connectivity->SetInput(transformResultDataInput->GetOutput());
      connectivity->SetExtractionModeToLargestRegion();
      connectivity->Update();

      vtkMAFSmartPointer<vtkCleanPolyData> clean;
      clean->SetInput(connectivity->GetOutput());
      clean->Update();

      vtkMAFSmartPointer<vtkPolyData> resultPolydata;
      resultPolydata->DeepCopy(clean->GetOutput());

      int result=m_AcetabularSurfaceHelperForBoolean->SetData(resultPolydata,((mafVME*)m_Input)->GetTimeStamp());
      if(result == MAF_ERROR)
      {
        wxMessageBox(_("The result surface hasn't any points"),_("Warning"),wxICON_EXCLAMATION);

        return;
      }

      connectivity->SetExtractionModeToSpecifiedRegions();
      connectivity->Update();

      int region = connectivity->GetNumberOfExtractedRegions();
      bool first = true;
      vtkMAFSmartPointer<vtkAppendPolyData> append;
      for (int i=1;i<region;i++)
      {
        connectivity->InitializeSpecifiedRegionList();
        connectivity->AddSpecifiedRegion(i);
        connectivity->Update();

        vtkMAFSmartPointer<vtkPolyData> resultPolydata2;
        resultPolydata2->DeepCopy(connectivity->GetOutput());
        resultPolydata2->Update();

        vtkMAFSmartPointer<vtkCleanPolyData> clean;
        clean->SetInput(resultPolydata2);
        clean->Update();

        if ( clean->GetOutput()->GetNumberOfPoints() != resultPolydata->GetNumberOfPoints() && clean->GetOutput()->GetNumberOfCells() != resultPolydata->GetNumberOfCells() )
        {
	        if (!first)
	        {
	        	append->AddInput(clean->GetOutput());
	        }
	        else
	        {
            first = false;
	          append->SetInput(clean->GetOutput());
	        }
        }
      }
      append->Update();

      vtkMAFSmartPointer<vtkPolyData> resultPolydataRemoved;
      resultPolydataRemoved->DeepCopy(append->GetOutput());
      result=m_AcetabularSurfaceRemoved->SetData(resultPolydataRemoved,((mafVME*)m_Input)->GetTimeStamp());
      if(result == MAF_ERROR)
      {
        mafLogMessage(_("The result surface hasn't any points"));
      }
      else
      {
        m_View->VmeShow(m_AcetabularSurfaceRemoved,true);
      }

      m_View->VmeShow(m_AcetabularSurfaceHelperForBoolean,true);

      m_View->CameraUpdate();

      if (saveResult)
      {
        m_AcetabularSurface->SetData(resultPolydata,((mafVME*)m_Input)->GetTimeStamp());
        m_AcetabularSurface->Update();
      }

//       m_BooleanGui->Enable(ID_ACCEPT_BOOLEAN,false);
//       m_BooleanGui->Update();
//       
//       m_View->VmeShow(m_AcetabularSurface,true);
//       m_View->VmeShow(m_Sphere,false);
// 
//       for (int i=0;i<m_SpheresPicked.size();i++)
//       {
//         m_View->VmeShow(m_SpheresPicked[i],false);
//       }
// 
//       m_View->CameraUpdate();
// 
//       m_AcetabularSurface->SetBehavior(NULL);     
      
    }
    else
    {
      return;
    }
  }
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::OnTranslateGui()
//----------------------------------------------------------------------------
{
  mafMatrix mNew;
  mafTransform::SetPosition(mNew, m_PositionClipPlane[0], m_PositionClipPlane[1], m_PositionClipPlane[2]);

  mafSmartPointer<mafMatrix> M;
  mafMatrix invOldAbsPose;
  mafSmartPointer<mafMatrix> newAbsPose;

  // incoming matrix is a rotation matrix
  newAbsPose->DeepCopy(m_GizmoTranslate->GetAbsPose());
  // copy rotation from incoming matrix
  mafTransform::CopyTranslation(mNew, *newAbsPose.GetPointer());

  invOldAbsPose.DeepCopy(m_GizmoTranslate->GetAbsPose());
  invOldAbsPose.Invert();

  mafMatrix::Multiply4x4(*newAbsPose.GetPointer(),invOldAbsPose,*M.GetPointer());

  // update gizmo abs pose
  m_GizmoRotate->SetAbsPose(newAbsPose, true);

  mafEvent e2s;
  e2s.SetSender(this);
  e2s.SetMatrix(M);
  e2s.SetId(ID_TRANSFORM);
  e2s.SetArg(mafInteractorGenericMouse::MOUSE_MOVE);

  PostMultiplyEventMatrix(&e2s);
}
//----------------------------------------------------------------------------
bool hipOpROMPreProcessing::AcceptAcetabularProsthesis(mafNode *node)
  //----------------------------------------------------------------------------
{
  if (mafVMESurface::SafeDownCast(node))
  {
    mafVME::SafeDownCast(node)->GetOutput()->Update();
  }
  return(node != NULL && mafVME::SafeDownCast(node)->IsA("mafVMESurface") && mafVMESurface::SafeDownCast(node)->GetTagArray()->IsTagPresent(TAG_PROSTHESIS_TYPE_ACETABULAR));
}
//----------------------------------------------------------------------------
bool hipOpROMPreProcessing::AcceptFemoralProsthesis(mafNode *node)
  //----------------------------------------------------------------------------
{
  if (mafVMESurface::SafeDownCast(node))
  {
    mafVME::SafeDownCast(node)->GetOutput()->Update();
  }
  return(node != NULL && mafVME::SafeDownCast(node)->IsA("mafVMESurface") && mafVMESurface::SafeDownCast(node)->GetTagArray()->IsTagPresent(TAG_PROSTHESIS_TYPE_FEMORAL));
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::OpDo()
//----------------------------------------------------------------------------
{
  if (m_AcetabularSurface)
  {
    m_AcetabularSurface->ReparentTo(m_Input);
    mafEventMacro(mafEvent(this,CAMERA_UPDATE));
  }
  if (m_FemoralSurface)
  {
    m_FemoralSurface->ReparentTo(m_Input);
    mafEventMacro(mafEvent(this,CAMERA_UPDATE));
  }
}
//----------------------------------------------------------------------------
void hipOpROMPreProcessing::OpUndo()
//----------------------------------------------------------------------------
{
  if (m_AcetabularSurface)
  {
    mafEventMacro(mafEvent(this, VME_REMOVE, m_AcetabularSurface));
    mafEventMacro(mafEvent(this,CAMERA_UPDATE));
  }
  if (m_FemoralSurface)
  {
    mafEventMacro(mafEvent(this, VME_REMOVE, m_FemoralSurface));
    mafEventMacro(mafEvent(this,CAMERA_UPDATE));
  }
}

void hipOpROMPreProcessing::ApplyBoneMaterial( mafVMESurface *surface )
{
	surface->GetMaterial()->m_AmbientIntensity = 0.10;
	surface->GetMaterial()->m_Ambient[0] = 0.25;
	surface->GetMaterial()->m_Ambient[1] = 0.0;
	surface->GetMaterial()->m_Ambient[2] = 0.0;
	surface->GetMaterial()->m_DiffuseIntensity = 0.91;
	surface->GetMaterial()->m_Diffuse[0] = 0.95;
	surface->GetMaterial()->m_Diffuse[1] = 0.94;
	surface->GetMaterial()->m_Diffuse[2] = 0.81;
	surface->GetMaterial()->m_SpecularIntensity = 0.06;
	surface->GetMaterial()->m_Specular[0] = 1.0;
	surface->GetMaterial()->m_Specular[1] = 1.0;
	surface->GetMaterial()->m_Specular[2] = 1.0;
	surface->GetMaterial()->m_SpecularPower = 1.0;
	surface->GetMaterial()->m_Opacity = 1.0;
	surface->GetMaterial()->m_Representation = 2;

	surface->GetMaterial()->UpdateProp();
}
