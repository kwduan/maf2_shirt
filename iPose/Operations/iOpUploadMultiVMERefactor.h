/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: iOpUploadMultiVMERefactor.h,v $
Language:  C++
Date:      $Date: 2011-04-28 12:02:31 $
Version:   $Revision: 1.1.1.1.2.12 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2002/2007
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)

MafMedical Library use license agreement

The software named MafMedical Library and any accompanying documentation, 
manuals or data (hereafter collectively "SOFTWARE") is property of the SCS s.r.l.
This is an open-source copyright as follows:
Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, 
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, 
this list of conditions and the following disclaimer in the documentation and/or 
other materials provided with the distribution.
* Modified source versions must be plainly marked as such, and must not be misrepresented 
as being the original software.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 'AS IS' 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR 
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

MafMedical is partially based on OpenMAF.
=========================================================================*/

#ifndef __iOpUploadMultiVMERefactor_H__
#define __iOpUploadMultiVMERefactor_H__

//----------------------------------------------------------------------------
// Include :
//----------------------------------------------------------------------------
#include "lhpOpUploadMultiVMERefactor.h"
#include "iOperationsDefines.h"


//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// lhpOpUploadMultiVMERefactor :
//----------------------------------------------------------------------------
/**Upload multiple VME using lhpOpUploadVMERefactor component

IMPORTANT: use lhpOpUploadMultiVMERefactorTest in \Testing\Operations to debug and validate this 
component along with plugged python components tests in VMEUploaderDownloaderRefactor dir

DEBUG/DEVELOPMENT:
to debug this component along with the ThreadedUploaderDownloader server (ie Upload/Download manager)
run ThreadedUploaderDownloader.py in Eclipse in Debug mode on port 50000

REFACTOR THIS: we need to put lhpOpUploadMultiVMERefactorTest in automatic regression (Parabuild) 
on both development and production biomedtown as soon as possible. Prerequisites for this:

1: configuration file for webservices
extract all webservices uri/stuff in order to confine
devel/production ws different URIs to a single file. In this way both devel and production 
tests can be run by loading only the different configuration file

2: production server speed enhancement
At the present time this test cannot be run in production since it overloads the server

*/
class I_OPERATIONS_EXPORT iOpUploadMultiVMERefactor: public lhpOpUploadMultiVMERefactor
{
public:

	iOpUploadMultiVMERefactor(wxString label = "Upload Multi Vme");
	~iOpUploadMultiVMERefactor(); 

	mafTypeMacro(iOpUploadMultiVMERefactor, lhpOpUploadMultiVMERefactor);

	mafOp* Copy();

  mafString m_RxUri;

protected:

  /** Upload of node and all its children*/
  /*virtual*/ int UploadVMEWithItsChildren(mafNode *vme);

};
#endif