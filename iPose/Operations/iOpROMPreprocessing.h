/*=========================================================================
  Program:   iPose
  Module:    $RCSfile: iOpROMPreprocessing.h,v $
  Language:  C++
  Date:      $Date: 2012-03-29 08:58:52 $
  Version:   $Revision: 1.1.2.3 $
  Authors:   Matteo Giacomoni
==========================================================================
  Copyright (c) 2002/2004
  SCS s.r.l. - BioComputing Competence Centre ( www.scsolutions.it - www.b3c.it)
=========================================================================*/

#ifndef __hipOpROMPreliminaries_H__
#define __hipOpROMPreliminaries_H__

//----------------------------------------------------------------------------
// Include :
//----------------------------------------------------------------------------
#include "iOperationsDefines.h"
#include "mafOp.h"

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class mafView;
class mafVMEGizmo;
class mafVMESurface;
class mafGizmoRotate;
class mafGizmoTranslate;
class mafInteractorPicker;
class mafInteractor;
class mafGUIFloatSlider;
class mafVMEGroup;
class mafViewVTK;
class vtkPlane;
class vtkClipPolyData;
class vtkPlaneSource;
class vtkAppendPolyData;
class vtkSphereSource;

//----------------------------------------------------------------------------
// hipOpROMPreliminaries :
//----------------------------------------------------------------------------
/** */
class I_OPERATIONS_EXPORT hipOpROMPreProcessing: public mafOp
{
public:
  /** constructor */
  hipOpROMPreProcessing(wxString label = "ROM Pre-Processing");
  /** destructor */
  ~hipOpROMPreProcessing();
  /** RTTI macro */
  mafTypeMacro(hipOpROMPreProcessing, mafOp);

  /** clone the object*/
  /*virtual*/ mafOp* Copy();

  /** Return true for the acceptable vme type. */
  /*virtual*/ bool Accept(mafNode *node);

  /** Builds operation's interface. */
  /*virtual*/ void OpRun();

  /** listen to other object events*/
  /*virtual*/ void OnEvent(mafEventBase *maf_event);

  /** show on/off clip plane */
	void ShowClipPlane(bool show);

  /** Execute the operation. */
  /*virtual*/ void OpDo();

  /** Makes the undo for the operation. */
  /*virtual*/ void OpUndo();

protected:
  //----------------------------------------------------------------------------
  // Widgets ID's
  //----------------------------------------------------------------------------
  enum PROSTHESIS_GUI_ID
  {
    ID_ACCEPT_ISOSURFACE = MINID,
    ID_CLIP,
    ID_ROTATE_X,
    ID_ROTATE_Y,
    ID_SPHERE_RADIUS,
    ID_ACCEPT_BOOLEAN,
    ID_DECIMATE,
    ID_SHOW_GIZMOS,
    ID_TRANSLATE,
    ID_SPHERE_CENTER_X,
    ID_SPHERE_CENTER_Y,
    ID_SPHERE_CENTER_Z,
    ID_CONTOUR_VALUE,
    ID_TRANSPARENCY_VALUE,
    ID_SPHERE_RADIUS_INC,
    ID_ENABLE_PICKER,
    ID_CANCEL,
  };

  /** This method is called at the end of the operation and result contain the wxOK or wxCANCEL. */
  /*virtual*/ void OpStop(int result);

  /** Apply bone material to input surface */
  void ApplyBoneMaterial(mafVMESurface *surface);

  /** create gui */
  void CreateGui();

  /** generate isosurface */
  void AcceptIsosurface();

  /** initialize gui and view for clip step */
  void InitializeClipStep();

  /** perform clip step */
  void Clip();

  /** process events ID_TRANSFORM */
  void PostMultiplyEventMatrix(mafEventBase *maf_event);

  /** manage rotations by gui */
  void OnRotateGui();

  /** manage translate by gui */
  void OnTranslateGui();

  /** initialize step to remove acetabular sphere bone */
  void InitializeBooleanStep();

  /** manage the vme picked events */
  void OnVmePicked(mafEvent *e);

  /** perform difference boolean operation */
  void BooleanOperation(bool saveResult = false);

  /** static function to select a surface in the tree */
  static bool AcceptAcetabularProsthesis(mafNode *node);

  /** static function to select a surface in the tree */
  static bool AcceptFemoralProsthesis(mafNode *node);


  mafInteractor *m_OldPicker;
  mafInteractorPicker *m_Picker;
  mafGUI *m_ContainerGui;
  mafGUI *m_IsoSurfaceGui;
  mafGUI *m_ClipGui;
  mafGUI *m_BooleanGui;
  mafViewVTK *m_View;
  mafGizmoTranslate *m_GizmoTranslate;
  mafGizmoRotate *m_GizmoRotate;
  mafVMEGizmo *m_GizmoImplicitPlane;
  mafVMESurface *m_ExtractedIsosurface;
  mafVMESurface *m_AcetabularSurface;
  mafVMESurface *m_AcetabularSurfaceHelperForBoolean;
  mafVMESurface *m_AcetabularSurfaceRemoved;
  mafVMESurface *m_FemoralSurface;
  vtkPlane *m_ClipperPlane; 
  vtkPlaneSource	*m_PlaneSource;
  vtkAppendPolyData	*m_Gizmo;
  bool m_PlaneCreated;
  
  double m_PositionClipPlane[3];
  double m_OrientationClipPlane[3];
  double m_SphereCenter[3];
  double m_SphereRadius;
  double m_StepsCenterTranslation[3];
  double m_ContourValue;
  double m_TransparencyValue;
  double m_StepRadius;

  mafVMESurface *m_Sphere;
  vtkSphereSource *m_SphereVTK;
  int m_Decimate;
  int m_ShowGizmos;
  int m_PickingMode;

  std::vector<double *> m_PickedPoints;
  std::vector<mafVMESurface *> m_SpheresPicked;
  mafVMEGroup *m_AcetabularProsthesisGroup;
  mafVMEGroup *m_FemoralProsthesisGroup;

  mafGUIFloatSlider *m_ContourSlider;
  mafGUIFloatSlider *m_TransparencySlider;

};
#endif
