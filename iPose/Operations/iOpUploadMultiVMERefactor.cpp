/*=========================================================================
Program:   Multimod Application Framework
Module:    $RCSfile: iOpUploadMultiVMERefactor.cpp,v $
Language:  C++
Date:      $Date: 2011-10-26 12:19:17 $
Version:   $Revision: 1.1.1.1.2.42 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2002/2007
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)

MafMedical Library use license agreement

The software named MafMedical Library and any accompanying documentation, 
manuals or data (hereafter collectively "SOFTWARE") is property of the SCS s.r.l.
This is an open-source copyright as follows:
Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, 
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, 
this list of conditions and the following disclaimer in the documentation and/or 
other materials provided with the distribution.
* Modified source versions must be plainly marked as such, and must not be misrepresented 
as being the original software.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 'AS IS' 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR 
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

MafMedical is partially based on OpenMAF.
=========================================================================*/

#include "medDefines.h"
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iDecl.h"
#include "lhpBuilderDecl.h"
#include "lhpUtils.h"
#include "iOpUploadMultiVMERefactor.h"
#include "lhpOpUploadVMERefactor.h"
#include "mafTagArray.h"
#include "mafTagItem.h"

//----------------------------------------------------------------------------
mafCxxTypeMacro(iOpUploadMultiVMERefactor);
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
iOpUploadMultiVMERefactor::iOpUploadMultiVMERefactor(wxString label) :
lhpOpUploadMultiVMERefactor(label)
//----------------------------------------------------------------------------
{
}
//----------------------------------------------------------------------------
iOpUploadMultiVMERefactor::~iOpUploadMultiVMERefactor()
//----------------------------------------------------------------------------
{
}
//----------------------------------------------------------------------------
mafOp* iOpUploadMultiVMERefactor::Copy()
//----------------------------------------------------------------------------
{
	/** return a copy of itself, needs to put it into the undo stack */
	return new iOpUploadMultiVMERefactor(m_Label);
}
//----------------------------------------------------------------------------
int iOpUploadMultiVMERefactor::UploadVMEWithItsChildren( mafNode *vme )
//----------------------------------------------------------------------------
{
  int number = 0;
  mafString rollBackLocalFileName = "msfList";
  mafString xmlDataResourcesRollBackLocalFileName = rollBackLocalFileName;

  while(wxFileExists(\
    m_VMEUploaderDownloaderABSFolder + xmlDataResourcesRollBackLocalFileName.GetCStr() + ".lhp"))
  {
    xmlDataResourcesRollBackLocalFileName = rollBackLocalFileName;
    xmlDataResourcesRollBackLocalFileName << number;
    number++;
  }

  mafString msfFilePath = m_VMEUploaderDownloaderABSFolder + xmlDataResourcesRollBackLocalFileName.GetCStr() + ".lhp";
  FILE *file = fopen(msfFilePath,"w");
  fclose(file);
  xmlDataResourcesRollBackLocalFileName << ".lhp";

  std::vector<const mafNode::mafChildrenVector*> childrenVector;
  childrenVector.clear();
  bool hasBinaryData = false;
  bool alreadyUploaded = false;
  bool emptyNode = false;

  int numVmeChild = 0;
  int i = 0;
  mafNode *childToUpload = NULL;

  const mafNode::mafChildrenVector *childrenAll;
  const mafNode::mafChildrenVector *children;
  mafNode::mafChildrenVector *childrenTmp;

  childrenTmp = new mafNode::mafChildrenVector();

  childrenAll = vme->GetChildren();

  // iterate over children and remove vme not visible to traverse
  // BUG #2066
  for(int j = 0; j < childrenAll->size(); j++)
  {
    mafNode *node = childrenAll->at(j);
    if(node->IsVisible() && node->IsInTree(node))
      childrenTmp->push_back(node);
  }

  children = childrenTmp;

  childrenVector.push_back(children);
  numVmeChild = children->size();
  if (numVmeChild != 0)
  {
    childToUpload = children->at(0);

    std::ostringstream stringStream;
    stringStream << "childToUpload:" << childToUpload->GetName() << std::endl;
    mafLogMessage(stringStream.str().c_str());
  }

  while (numVmeChild != 0)
  {
    i = 0;
    for ( i ; i < children->size() ; i++)
    {
      numVmeChild = children->size();
      alreadyUploaded = false;
      emptyNode = false;

      //Check if VME children has been already uploaded
      for (int c = 0; c < m_EmptyNodeVector.size(); c++)
      {
        if (m_EmptyNodeVector[c]->Equals(children->at(i)) && m_EmptyNodeVector[c]->GetId() == children->at(i)->GetId())
        {
          emptyNode = true;
          break;
        }
      }

      // avoid bad children count bug #2070 fix
      children = children->at(i)->GetChildren();

      childrenTmp = new mafNode::mafChildrenVector();
      for(int j = 0; j < children->size(); j++)
      {
        mafNode *node = children->at(j);
        if(node->IsVisible() && node->IsInTree(node))
          childrenTmp->push_back(node);
      }
      children = childrenTmp;

      if (children->size() == 0 || emptyNode)
      {
        children = childrenVector.back();
        childToUpload = children->at(i);

        for (int c = 0; c < m_AlreadyUploadedVMEVector.size(); c++) 
        {
          if (m_AlreadyUploadedVMEVector[c]->Equals(children->at(i)) && \
            m_AlreadyUploadedVMEVector[c]->GetId() == children->at(i)->GetId())
          {
            alreadyUploaded = true;
            break;
          }
        }

        if (!alreadyUploaded)
        {
          if (childToUpload->GetNumberOfLinks() != 0)
          {
            m_DerivedVMEsIdVector.push_back(childToUpload->GetId());
          }

          if(childToUpload->IsVisible()) // avoid upload gizmos :) bug #2066 fix
          {
            m_OpUploadVME->SetInput(childToUpload);

            if (GetUploadError())
            {
              RemoveAlreadyUploadedXMLResources(m_AlreadyUploadedXMLURIVector);
              return MAF_ERROR;
            }
            // Iterate over children to discard gizmos
            int numchildren = 0;
            const mafNode::mafChildrenVector *cVector = childToUpload->GetChildren();
            for(int j = 0; j < cVector->size(); j++)
            {
              mafNode *node = cVector->at(j);
              if(node->IsVisible() && node->IsInTree(node))
                numchildren++;
            }

            m_OpUploadVME->SetWithChild(numchildren!=0);
            m_OpUploadVME->SetXMLUploadedResourcesRollBackLocalFileName(xmlDataResourcesRollBackLocalFileName);
            m_OpUploadVME->SetIsLast(false);

            if (m_OpUploadVME->Upload()\
              == MAF_ERROR)
            {
              RemoveAlreadyUploadedXMLResources(m_AlreadyUploadedXMLURIVector);
              return MAF_ERROR;
            }

            if (childToUpload->GetTagArray()->IsTagPresent(TAG_RX))
            {
              m_RxUri = m_OpUploadVME->GetRemoteXMLResourceURI();
            }
          }

          m_AlreadyUploadedVMEVector.push_back(childToUpload);
          m_EmptyNodeVector.push_back(childToUpload);
          m_AlreadyUploadedXMLURIVector.push_back(m_OpUploadVME->GetRemoteXMLResourceURI());
          if (SaveChildrenURIFile(childToUpload, m_OpUploadVME->GetRemoteXMLResourceURI()) \
            == MAF_ERROR)
          {

            return MAF_ERROR;
          }
        }
      }
      else
      {
        childrenVector.push_back(children);
        i = -1;
      }
    }
    m_EmptyNodeVector.push_back(childToUpload->GetParent());

    if (childrenVector.size() > 1)
      childrenVector.pop_back();
    else
      break;

    children = childrenVector.back();
    numVmeChild = children->size();

    mafSleep(5000);
  }


  if (GetUploadError())
  {
    RemoveAlreadyUploadedXMLResources(m_AlreadyUploadedXMLURIVector);
    return MAF_ERROR;
  }

  // finally upload VME Root
  m_OpUploadVME->SetInput(vme);
  m_OpUploadVME->SetWithChild(vme->GetNumberOfChildren() != 0);
  m_OpUploadVME->SetXMLUploadedResourcesRollBackLocalFileName(xmlDataResourcesRollBackLocalFileName);
  m_OpUploadVME->SetIsLast(true);

  if (m_OpUploadVME->Upload() == MAF_ERROR)
  {
    RemoveAlreadyUploadedXMLResources(m_AlreadyUploadedXMLURIVector);
    return MAF_ERROR;
  }

  m_AlreadyUploadedVMEVector.push_back(vme);
  m_AlreadyUploadedXMLURIVector.push_back(m_OpUploadVME->GetRemoteXMLResourceURI());

  if (AddLinksURIToDerivedVMEMetadata(vme) == MAF_ERROR)
  {
    RemoveAlreadyUploadedXMLResources(m_AlreadyUploadedXMLURIVector);
    return MAF_ERROR;
  }

  return MAF_OK;

}