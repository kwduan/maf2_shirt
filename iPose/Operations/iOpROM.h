/*=========================================================================
  Program:   HipOp
  Module:    $RCSfile: iOpROM.h,v $
  Language:  C++
  Date:      $Date: 2012-05-02 09:08:44 $
  Version:   $Revision: 1.1.2.5 $
  Authors:   Matteo Giacomoni
==========================================================================
  Copyright (c) 2002/2004
  SCS s.r.l. - BioComputing Competence Centre ( www.scsolutions.it - www.b3c.it)
=========================================================================*/

#ifndef __iOpROM_H__
#define __iOpROM_H__

//----------------------------------------------------------------------------
// Include :
//----------------------------------------------------------------------------
#include "iOperationsDefines.h"
#include "mafOp.h"
#include "iDecl.h"

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class mafVMESurface;
class mafVMEGroup;
class mafView;
class mafGUICheckListBox;
class mafGUIDialog;
class mafMatrix;

//----------------------------------------------------------------------------
// iOpROM :
//----------------------------------------------------------------------------
/** */
class I_OPERATIONS_EXPORT iOpROM: public mafOp
{
public:
  /** constructor */
	iOpROM(wxString label = "ROM");
	/** destructor */
  ~iOpROM();
	/** RTTI macro */
  mafTypeMacro(iOpROM, mafOp);

  /** clone the object*/
  /*virtual*/ mafOp* Copy();

	/** Return true for the acceptable vme type. */
	/*virtual*/ bool Accept(mafNode *node);

  /** Builds operation's interface. */
	/*virtual*/ void OpRun();

  /** listen to other object events*/
  /*virtual*/ void OnEvent(mafEventBase *maf_event);

protected:

  //----------------------------------------------------------------------------
  // Widgets ID's
  //----------------------------------------------------------------------------
  enum PROSTHESIS_GUI_ID
  {
    ID_ROTATE_X = MINID,
    ID_ROTATE_X_DECREASE,
    ID_ROTATE_X_INCREASE,
    ID_ROTATE_Y,
    ID_ROTATE_Y_DECREASE,
    ID_ROTATE_Y_INCREASE,
    ID_ROTATE_Z,
    ID_ROTATE_Z_DECREASE,
    ID_ROTATE_Z_INCREASE,
    ID_RESET,
    ID_MOVEMENTS_LIST,
    ID_ADD_MOVEMENT,
    ID_EDIT_MOVEMENT,
    ID_ARROW_UP,
    ID_ARROW_DOWN,
    ID_RUN,
    ID_MOVEMENT_NAME,
    ID_MOVEMENT_AXIS_X,
    ID_MOVEMENT_AXIS_Y,
    ID_MOVEMENT_AXIS_Z,
    ID_MOVEMENT_ANGLE_INF,
    ID_MOVEMENT_ANGLE_SUP,
    ID_MOVEMENT_START_FROM_PREVIOUS_STEP,
    ID_MOVEMENT_ENABLE_VISUALIZATION,
    ID_MOVEMENT_ADD_TO_DB,
  };

  /** Create dialog to add a movement */
  void OnAddMovement();

  /** This method is called at the end of the operation and result contain the wxOK or wxCANCEL. */
  /*virtual*/ void OpStop(int result);

  /** create gui */
  void CreateGui();

  /** static function to select a group with ROM data */
  static bool AcceptRomData(mafNode *node);

  /** static function to select a surface in the tree */
  static bool AcceptAcetabularProsthesis(mafNode *node);

  /** static function to select a surface in the tree */
  static bool AcceptFemoralProsthesis(mafNode *node);

  /** static function to select a surface in the tree */
  static bool AcceptSurface(mafNode *node);

  /** create a surface that will be static (acetabular) */
  void GenerateStaticSurface();

  /** create a surface that will be rotated (femoral) */
  void GenerateMobileSurface();

  /** rotate mobile surface */
  void UpdateOrientation();

  /** fill the check list of movements */
  int FillMovementsList();

  /** add a movement to the DB */
  int AddMovementToDB();

  /** move a movement down */
  void OnArrowDown();

  /** move a movement up */
  void OnArrowUp();

  /** run selected movements */
  void OnRunMovements();

  /** open dialog to edit movement values */
  void OnEditMovement();

  /** create dialog to add/edit movements */
  void CreateMovemetDialog();

  /** load prosthesis poses and rom information */
  void LoadRomData();

  /** Apply stored positions to prosthesis */
  void ApplyPoseToProsthesis();

  /** Check if inputs are correct */
  int CheckInputs();

  void PostMultiplyEventMatrix(mafEventBase *maf_event);

  std::vector<DB_MOVEMENTS_INFORMATION> m_DBMomementsInformation;

  mafVMEGroup *m_AcetabularProsthesisGroup;
  mafVMEGroup *m_FemoralProsthesisGroup;

  mafNode *m_FemoralProsthesisOldParent;
  mafVMESurface *m_FemoralProsthesisTMP;
  mafVMESurface *m_FemoralSurface;
  mafVMESurface *m_AcetabularSurface;
  mafVMESurface *m_Femoral;;
  mafVMESurface *m_Acetabular;

  mafView *m_View;

  mafMatrix *m_RefSys;
  double m_Orientation[3];

  mafGUICheckListBox *m_MovementsList;
  std::vector<int> m_MovementsId;

  bool *m_CellsToExclude;

  mafNode *m_VmeWithRomInformation;
  mafMatrix *m_OldAcetabularProsthisisPosition;
  mafMatrix *m_OldFemoralProsthisisPosition;

  //Dialog stuff
  mafGUIDialog *m_Dialog;
  mafString m_MomementName;
  double m_MovementAxisX;
  double m_MovementAxisY;
  double m_MovementAxisZ;
  double m_MovementAngleInf;
  double m_MovementAngleSup;
  int m_MovementStartFromPreviousStep;
  int m_MovementEnableVisualization;
  int m_MovementAddToDB;
  mafGUI *m_MovementGui;
};
#endif
