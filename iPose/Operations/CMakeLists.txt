PROJECT(iOperations)

# Set your list of sources here.
SET(PROJECT_SRCS
    #iOpPlanningWorkflow.cpp
    #iOpPlanningWorkflow.h
    iOpAddProsthesisToDB.cpp
    iOpAddProsthesisToDB.h
	#iOpROMPreprocessing.cpp
	#iOpROMPreprocessing.h
	#iOpROM.cpp
	#iOpROM.h
	#iOpPlaneTraining.cpp
	#iOpPlaneTraining.h
	iOpDownloadVMERefactor.cpp
	iOpDownloadVMERefactor.h
  iOpUploadMultiVMERefactor.cpp
	iOpUploadMultiVMERefactor.h
)

IF(LHP_BUILD_DLL)
  SET(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} /FORCE:MULTIPLE")
  SET(BUILD_SHARED_LIBS 1)
  ADD_DEFINITIONS(-DI_OPERATIONS_EXPORTS)
  # Create the library.
  ADD_LIBRARY(${PROJECT_NAME} ${PROJECT_SRCS})

  TARGET_LINK_LIBRARIES(${PROJECT_NAME} mafDLL lhpOperations iVME iCommon)

ELSE(LHP_BUILD_DLL)
  # Create the library.
  ADD_LIBRARY(${PROJECT_NAME} ${PROJECT_SRCS})
  TARGET_LINK_LIBRARIES(${PROJECT_NAME} ${PROJECT_LIBS})
ENDIF (LHP_BUILD_DLL)