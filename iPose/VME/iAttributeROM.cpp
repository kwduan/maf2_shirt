/*=========================================================================
Program:   iPose
Module:    $RCSfile: iAttributeROM.cpp,v $
Language:  C++
Date:      $Date: 2011-06-24 14:00:17 $
Version:   $Revision: 1.1.2.10 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2012
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/

#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iAttributeROM.h"
#include "iDecl.h"
#include "mafStorageElement.h"
#include "mafIndent.h"

using namespace std;

//----------------------------------------------------------------------------
mafCxxTypeMacro(iAttributeROM)
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
iAttributeROM::iAttributeROM()
//----------------------------------------------------------------------------
{  
  m_Name = "DataForROM";
}
//----------------------------------------------------------------------------
iAttributeROM::~iAttributeROM()
//----------------------------------------------------------------------------
{
  std::map<int,double*>::iterator it;

  for (it = m_Positions.begin();it != m_Positions.end();it++)
  {
    delete []it->second;
  }
  for (it = m_Orientations.begin();it != m_Orientations.end();it++)
  {
    delete []it->second;
  }
}
//-------------------------------------------------------------------------
void iAttributeROM::DeepCopy(const mafAttribute *a)
//-------------------------------------------------------------------------
{ 
  if (!a->IsA("hipAttributeROM"))
  {
    return;
  }

  std::map<int,double*>::iterator it;

  for (it = ((iAttributeROM*)a)->m_Positions.begin();it != ((iAttributeROM*)a)->m_Positions.end();it++)
  {
    double *position = new double[3];
    position[0] = it->second[0];
    position[1] = it->second[1];
    position[2] = it->second[2];
    this->m_Positions[it->first] = position;
  }

  for (it = ((iAttributeROM*)a)->m_Orientations.begin();it != ((iAttributeROM*)a)->m_Orientations.end();it++)
  {
    double *orientation = new double[3];
    orientation[0] = it->second[0];
    orientation[1] = it->second[1];
    orientation[2] = it->second[2];
    this->m_Orientations[it->first] = orientation;
  }

  std::map<int,std::vector<DB_MOVEMENTS_INFORMATION>>::iterator itMovements;
  for (itMovements = ((iAttributeROM*)a)->m_Momements.begin();itMovements != ((iAttributeROM*)a)->m_Momements.end();itMovements++)
  {
    std::vector<DB_MOVEMENTS_INFORMATION> movementsPerVme;
    for (int i=0;i<itMovements->second.size();i++)
    {
      DB_MOVEMENTS_INFORMATION movement;
      movement.m_AngleInf = itMovements->second[i].m_AngleInf;
      movement.m_AngleSup = itMovements->second[i].m_AngleSup;
      movement.m_AngleCollision = itMovements->second[i].m_AngleCollision;
      movement.m_Axis[0] = itMovements->second[i].m_Axis[0];
      movement.m_Axis[1] = itMovements->second[i].m_Axis[1];
      movement.m_Axis[2] = itMovements->second[i].m_Axis[2];
      movement.m_Name = itMovements->second[i].m_Name;
      movement.m_ShowMovement = itMovements->second[i].m_ShowMovement;
      movement.m_StartFromPrevious = itMovements->second[i].m_StartFromPrevious;
      movement.m_Passed = itMovements->second[i].m_Passed;

      movementsPerVme.push_back(movement);
    }
  }

  Superclass::DeepCopy(a);
}
//----------------------------------------------------------------------------
bool iAttributeROM::Equals(const mafAttribute *a)
//----------------------------------------------------------------------------
{
  if (Superclass::Equals(a))
  {
    if (m_Positions.size() != ((iAttributeROM*)a)->GetNumberOfPositions())
    {
      return false;
    }

    if (m_Orientations.size() != ((iAttributeROM*)a)->GetNumberOfOrientations())
    {
      return false;
    }

    std::map<int,double*>::iterator it;
    for (it = m_Positions.begin();it != m_Positions.end();it++)
    {
      double position[3];
      if (((iAttributeROM*)a)->GetPosition(it->first,position))
      {
      	if (position[0] != it->second[0] || position[1] != it->second[1] || position[2] != it->second[2])
      	{
          return false;
      	}
      }
      else
      {
        return false;
      }
    }

    for (it = m_Orientations.begin();it != m_Orientations.end();it++)
    {
      double orientation[3];
      if (((iAttributeROM*)a)->GetOrientation(it->first,orientation))
      {
        if (orientation[0] != it->second[0] || orientation[1] != it->second[1] || orientation[2] != it->second[2])
        {
          return false;
        }
      }
      else
      {
        return false;
      }
    }


    return true;
  }
  return false;
}

//-----------------------------------------------------------------------
int iAttributeROM::InternalStore(mafStorageElement *parent)
  //-----------------------------------------------------------------------
{  
  if (Superclass::InternalStore(parent)==MAF_OK)
  {
    parent->StoreInteger("NumOrientations",m_Orientations.size());
    parent->StoreInteger("NumPositions",m_Positions.size());

    std::map<int,double*>::iterator it;
    int index = 0;
    for (index = 0,it = m_Orientations.begin();it != m_Orientations.end();it++,index++)
    {
      mafString nameOrientation = "Orientation_";
      mafString nameOrientationVME = "OrientationVME_";
      nameOrientation << index;
      nameOrientationVME << index;

      parent->StoreInteger(nameOrientationVME,it->first);
      parent->StoreVectorN(nameOrientation,it->second,3);
    }

    for (index = 0,it = m_Positions.begin();it != m_Positions.end();it++,index++)
    {
      mafString namePosition = "Position_";
      mafString namePositionVME = "PositionVME_";
      namePosition << index;
      namePositionVME << index;

      parent->StoreInteger(namePositionVME,it->first);
      parent->StoreVectorN(namePosition,it->second,3);
    }

    parent->StoreInteger("NumMovements",m_Momements.size());

    std::map<int,std::vector<DB_MOVEMENTS_INFORMATION>>::iterator itMovements;
    for (index = 0, itMovements = m_Momements.begin();itMovements != m_Momements.end();itMovements++,index++)
    {
      mafString nameMovementsVME = "MovementsVME_";
      nameMovementsVME << index;
      parent->StoreInteger(nameMovementsVME,itMovements->first);
      mafString nameNumMovementsVME = "NumMovementsVME_";
      nameNumMovementsVME << itMovements->first;
      parent->StoreInteger(nameNumMovementsVME,itMovements->second.size());
      for (int i=0;i<itMovements->second.size();i++)
      {
        mafString preName = "Movement_";
        preName << itMovements->first;
        preName << "_";
        preName << i;
        mafString nameValue;
        nameValue = preName;
        nameValue << "_AngleInf";
        parent->StoreDouble(nameValue,itMovements->second[i].m_AngleInf);
        nameValue = preName;
        nameValue << "_AngleSup";
        parent->StoreDouble(nameValue,itMovements->second[i].m_AngleSup);
        nameValue = preName;
        nameValue << "_AngleOfCollision";
        parent->StoreDouble(nameValue,itMovements->second[i].m_AngleCollision);
        nameValue = preName;
        nameValue << "_Axis";
        parent->StoreVectorN(nameValue,itMovements->second[i].m_Axis,3);
        nameValue = preName;
        nameValue << "_Name";
        parent->StoreText(nameValue,itMovements->second[i].m_Name);
        nameValue = preName;
        nameValue << "_ShowMovement";
        parent->StoreInteger(nameValue,itMovements->second[i].m_ShowMovement ? TRUE : FALSE);
        nameValue = preName;
        nameValue << "_StartFromPrevious";
        parent->StoreInteger(nameValue,itMovements->second[i].m_StartFromPrevious ? TRUE : FALSE);
        nameValue = preName;
        nameValue << "_MovementPassed";
        parent->StoreInteger(nameValue,itMovements->second[i].m_Passed ? TRUE : FALSE);
      }
    }

    return MAF_OK;
  }
  return MAF_ERROR;
}
//----------------------------------------------------------------------------
int iAttributeROM::InternalRestore(mafStorageElement *node)
  //----------------------------------------------------------------------------
{
  if (Superclass::InternalRestore(node) == MAF_OK)
  {
    int numOfPositions = 0;
    node->RestoreInteger("NumPositions",numOfPositions);

    int numOfOrientations = 0;
    node->RestoreInteger("NumOrientations",numOfOrientations);

    for (int index = 0;index<numOfPositions;index++)
    {
      mafString namePosition = "Position_";
      mafString namePositionVME = "PositionVME_";
      namePosition << index;
      namePositionVME << index;

      int id;
      double *pos = new double[3];
      node->RestoreInteger(namePositionVME,id);
      node->RestoreVectorN(namePosition,pos,3);

      m_Positions[id] = pos;
    }

    for (int index = 0;index<numOfOrientations;index++)
    {
      mafString nameOrientation = "Orientation_";
      mafString nameOrientationVME = "OrientationVME_";
      nameOrientation << index;
      nameOrientationVME << index;

      int id;
      double *rot = new double[3];
      node->RestoreInteger(nameOrientationVME,id);
      node->RestoreVectorN(nameOrientation,rot,3);

      m_Orientations[id] = rot;
    }

    int numOfMovements = 0;
    node->RestoreInteger("NumMovements",numOfMovements);

    for (int i=0;i<numOfMovements;i++)
    {
      int id = -1;
      mafString nameMovementsVME = "MovementsVME_";
      nameMovementsVME << i;
      node->RestoreInteger(nameMovementsVME,id);
      mafString nameNumMovementsVME = "NumMovementsVME_";
      nameNumMovementsVME << id;
      int numOfMovementsVME = 0;
      node->RestoreInteger(nameNumMovementsVME,numOfMovementsVME);

      std::vector<DB_MOVEMENTS_INFORMATION> movementsPerVME;
      for (int j=0;j<numOfMovementsVME;j++)
      {
        mafString preName = "Movement_";
        preName << id;
        preName << "_";
        preName << j;
        mafString nameValue;
        nameValue = preName;
        nameValue << "_AngleInf";
        double angleInf;
        node->RestoreDouble(nameValue,angleInf);
        nameValue = preName;
        nameValue << "_AngleSup";
        double angleSup;
        node->RestoreDouble(nameValue,angleSup);
        nameValue = preName;
        nameValue << "_Axis";
        double axis[3];
        node->RestoreVectorN(nameValue,axis,3);
        nameValue = preName;
        nameValue << "_Name";
        mafString name;
        node->RestoreText(nameValue,name);
        nameValue = preName;
        nameValue << "_ShowMovement";
        int showMovement;
        node->RestoreInteger(nameValue,showMovement);
        nameValue = preName;
        nameValue << "_StartFromPrevious";
        int startFromPrevious;
        node->RestoreInteger(nameValue,startFromPrevious);
        nameValue = preName;
        nameValue << "_MovementPassed";
        int movementPassed;
        node->RestoreInteger(nameValue,movementPassed);
        nameValue = preName;
        nameValue << "_AngleOfCollision";
        double angleOfCollision;
        node->RestoreDouble(nameValue,angleOfCollision);

        DB_MOVEMENTS_INFORMATION movement;
        movement.m_AngleInf = angleInf;
        movement.m_AngleSup = angleSup;
        movement.m_AngleCollision = angleOfCollision;
        movement.m_Name = name;
        movement.m_Axis[0] = axis[0];
        movement.m_Axis[1] = axis[1];
        movement.m_Axis[2] = axis[2];
        movement.m_ShowMovement = showMovement == TRUE ? true : false;
        movement.m_StartFromPrevious = startFromPrevious == TRUE ? true : false;
        movement.m_Passed = movementPassed == TRUE ? true : false;

        movementsPerVME.push_back(movement);
      }

      m_Momements[id] = movementsPerVME;
    }

    return MAF_OK;
  }
  return MAF_ERROR;
}
//-----------------------------------------------------------------------
void iAttributeROM::Print(std::ostream& os, const int tabs) const
//-----------------------------------------------------------------------
{
  Superclass::Print(os,tabs);
  mafIndent indent(tabs);
}
//-----------------------------------------------------------------------
void iAttributeROM::AddProsthesisPose( int vmeID,double position[3],double orientation[3] )
//-----------------------------------------------------------------------
{
  double *pos = new double[3];
  pos[0] = position[0];
  pos[1] = position[1];
  pos[2] = position[2];
  m_Positions[vmeID] = pos;

  double *orien = new double[3];
  orien[0] = orientation[0];
  orien[1] = orientation[1];
  orien[2] = orientation[2];
  m_Orientations[vmeID] = orien;
}
//-----------------------------------------------------------------------
int iAttributeROM::GetNumberOfPositions()
//-----------------------------------------------------------------------
{
  return m_Positions.size();
}
//-----------------------------------------------------------------------
int iAttributeROM::GetNumberOfOrientations()
//-----------------------------------------------------------------------
{
  return m_Orientations.size();
}
//-----------------------------------------------------------------------
bool iAttributeROM::GetPosition( int vmeID,double position[3] )
//-----------------------------------------------------------------------
{
  std::map<int,double*>::iterator it = m_Positions.find(vmeID);
  if (it == m_Positions.end())
  {
    position[0] = -1.0;
    position[1] = -1.0;
    position[2] = -1.0;
    return false;
  }

  position[0] = it->second[0];
  position[1] = it->second[1];
  position[2] = it->second[2];

  return true;
}
//-----------------------------------------------------------------------
bool iAttributeROM::GetOrientation( int vmeID,double orientation[3] )
//-----------------------------------------------------------------------
{
  std::map<int,double*>::iterator it = m_Orientations.find(vmeID);
  if (it == m_Orientations.end())
  {
    orientation[0] = -1.0;
    orientation[1] = -1.0;
    orientation[2] = -1.0;
    return false;
  }

  orientation[0] = it->second[0];
  orientation[1] = it->second[1];
  orientation[2] = it->second[2];

  return true;
}
//-----------------------------------------------------------------------
void iAttributeROM::AddMovement(int vmeID, wxString name, double angleInf, double angleSup, double angleCollision, double axisX, double axisY, double axisZ, bool shoMovement, bool startFromPrevious, bool passed)
//-----------------------------------------------------------------------
{
  DB_MOVEMENTS_INFORMATION movement;
  movement.m_AngleInf = angleInf;
  movement.m_AngleSup = angleSup;
  movement.m_Axis[0] = axisX;
  movement.m_Axis[1] = axisY;
  movement.m_Axis[2] = axisZ;
  movement.m_Name = name.c_str();
  movement.m_ShowMovement = shoMovement;
  movement.m_StartFromPrevious = startFromPrevious;
  movement.m_AngleCollision = angleCollision;
  movement.m_Passed = passed;


  std::vector<DB_MOVEMENTS_INFORMATION> movementsPerVME;
  std::map<int,std::vector<DB_MOVEMENTS_INFORMATION>>::iterator it = m_Momements.find(vmeID);
  if (it == m_Momements.end())//First movement per vme
  {
    movementsPerVME.push_back(movement);
  }
  else
  {
    movementsPerVME = m_Momements[vmeID];
    movementsPerVME.push_back(movement);
  }

  m_Momements[vmeID] = movementsPerVME;
}
//-----------------------------------------------------------------------
void iAttributeROM::CleanMovements( int vmeID )
//-----------------------------------------------------------------------
{
  std::map<int,std::vector<DB_MOVEMENTS_INFORMATION>>::iterator it = m_Momements.find(vmeID);
  if (it != m_Momements.end())
  {
    m_Momements.erase(vmeID);
  }
}
//-----------------------------------------------------------------------
int iAttributeROM::GetNumberOfMovementsPerVME( int vmeID )
//-----------------------------------------------------------------------
{
  std::map<int,std::vector<DB_MOVEMENTS_INFORMATION>>::iterator it = m_Momements.find(vmeID);
  if (it != m_Momements.end())
  {
    return m_Momements[vmeID].size();
  }
  return 0;
}
//-----------------------------------------------------------------------
void iAttributeROM::GetMovement(int vmeID,int index,wxString &name, double &angleInf, double &angleSup, double &angleCollision, double axis[3], bool &showMovement, bool &startFromPrevious, bool &passed)
//-----------------------------------------------------------------------
{
  name = m_Momements[vmeID][index].m_Name.GetCStr();
  angleInf = m_Momements[vmeID][index].m_AngleInf;
  angleSup = m_Momements[vmeID][index].m_AngleSup;
  axis[0] = m_Momements[vmeID][index].m_Axis[0];
  axis[1] = m_Momements[vmeID][index].m_Axis[1];
  axis[2] = m_Momements[vmeID][index].m_Axis[2];
  showMovement = m_Momements[vmeID][index].m_ShowMovement;
  startFromPrevious = m_Momements[vmeID][index].m_StartFromPrevious;
  passed = m_Momements[vmeID][index].m_Passed;
  angleCollision =  m_Momements[vmeID][index].m_AngleCollision;
}
