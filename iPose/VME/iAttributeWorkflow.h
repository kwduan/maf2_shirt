/*=========================================================================
Program:   iPose
Module:    $RCSfile: iAttributeWorkflow.h,v $
Language:  C++
Date:      $Date: 2012-03-21 07:29:13 $
Version:   $Revision: 1.1.2.8 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/

#ifndef __iAttributeWorkflow_H__
#define __iAttributeWorkflow_H__

//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "mafAttribute.h"
#include "iVMEDefines.h"
#include <map>
#include <vector>

//----------------------------------------------------------------------------
// forward refs :
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// iAttributeWorkflow:
//----------------------------------------------------------------------------
/** 

Salva lo step in cui mi trovo per evitare di rifare lo studio da capo 

*/  
class I_VME_EXPORT iAttributeWorkflow : public mafAttribute
{
public:
  iAttributeWorkflow();
  virtual ~iAttributeWorkflow();

  mafTypeMacro(iAttributeWorkflow, mafAttribute);

  /** Print a dump of this object */
  /*virtual*/ void Print(std::ostream& os, const int tabs=0) const;

  /** Copy the contents of another Meter attribute into this one. */
  /*virtual*/ void DeepCopy(const mafAttribute *a);

  /** Compare with another Meter attribute. */
  /*virtual*/ bool Equals(const mafAttribute *a);

  /** Return the index of the last step */
  int GetCurrentStep();

  /** Set the index of the last step */
  void SetCurrentStep(int step);

  /** Return the name of the patinet of the study */
  mafString GetPatientName();

  /** Set the name of the patient of the study */
  void SetPatientName(mafString patientName);

  /** Return the side of the study */
  int GetSide();

  /** Set the side of the study */
  void SetSide(int side);

  /** Return the joint of the study */
  int GetJoint();

  /** Set the joint of the study */
  void SetJoint(int joint);

  /** set patient weight */
  void SetPatientWeight(double weight);

  /** return the patient weight */
  double GetPatientWeight(){return m_PatientWeight;};

  /** set patient height */
  void SetPatientHeight(double height);

  /** return the patient height */
  double GetPatientHeight(){return m_PatientHeight;};

  /** set patient sex */
  void SetPatientSex(mafString sex);

  /** return the patient sex */
  mafString GetPatientSex(){return m_PatientSex;};

  /** set patient birthdate */
  void SetPatientBirthdate(mafString birthdate);

  /** return the patient birthdate */
  mafString GetPatientBirthdate(){return m_PatientBirthdate;};

  /** set pca parameters */
  void SetPCA(double pca[35]);

  /** get pca parameters */
  void GetPCA(double pca[35]);

  /** set neck vector */
  void SetNeckVector(double neckVector[3]);

  /** get neck vector */
  void GetNeckVector(double neckVector[3]);

  /** set diagnosis */
  void SetDiagnosis(mafString diagnosis);

  /** get diagnosis */
  mafString GetDiagnosis();

  /** set standard deviation bmi*/
  void SetStdBMI(double stdBmi);

  /** get standard deviation bmi*/
  double GetStdBMI();

  /** set average bmi*/
  void SetAvrBMI(double avrBmi);

  /** get average bmi*/
  double GetAvrBMI();

  /** add a prosthesis used in workflow */
  void AddProsthesis( mafString manufacture,mafString model,mafString type,std::map<mafString,mafString> componentsSizes );

  /** return if a prosthesis is acetabular or femural etc. */
  mafString GetProsthesisType(mafString manufacture,mafString model);

  /** return number of componets for a particular model */
  int GetNumberOfComponents(mafString manufacture,mafString model);

  /** return sizes for every component */
  void GetComponentsSizes(mafString manufacture,mafString model,std::map<mafString,mafString> &componentsSizes);

  /** return i-th manufactore */
  mafString GetManufacture(int index){return m_Prosthesis[index].m_Manufacture;};

  /** return i-th model */
  mafString GetModel(int index){return m_Prosthesis[index].m_Model;};

  /** return the number of prosthesis */
  int GetNumberOfProsthesis(){return m_Prosthesis.size();};

  /** remove all prosthesis */
  void RemoveAllProsthesis(){m_Prosthesis.clear();};
  
  /** set on if the study is already uploaded */
  void StudyIsUploadedOn(){m_StudyUploaded = true;};

  /** set off if the study is already uploaded */
  void StudyIsUploadedOff(){m_StudyUploaded = false;};;

  /** return if the study is already uploaded */
  bool StudyIsUploaded(){return m_StudyUploaded;};

  /** set uri of dataresource of rx image */
  void SetRxUri(mafString rxUri);

  /** get uri of dataresource of rx image */
  mafString GetRxUri();

  /************************************************************************/
  /* MEASURES                                                             */
  /************************************************************************/
  /** set neck length */
  void SetHipNeckLength(double neckLength);

  /** get neck length */
  double GetHipNeckLength();

  /** set CCD */
  void SetHipCCD(double ccd);

  /** get CCD */
  double GetHipCCD();

  /** set Femoral Canal */
  void SetHipFemoralCanal(double femoralCanal);

  /** get Femoral Canal */
  double GetHipFemoralCanal();

  /** set Femoral Head */
  void SetHipFemoralHead(double femoralHead);

  /** get Femoral Head */
  double GetHipFemoralHead();

  /** set anteversion */
  void SetHipAnteversion(double anteversion);

  /** get anteversion */
  double GetHipAnteversion();

  /** set rx calibration */
  void SetCalibration(double calibration);

  /** get rx calibration */
  double GetCalibration();

  /** set training score */
  void SetScore(int score);

  /** get training score */
  int GetScore();

protected:
  /*virtual*/ int InternalStore(mafStorageElement *parent);
  /*virtual*/ int InternalRestore(mafStorageElement *node);

  int m_CurrentStep;
  int m_Side;
  int m_Joint;
  mafString m_PatientName;
  mafString m_Diagnosis;
  mafString m_PatientSex;
  mafString m_PatientBirthdate;
  mafString m_RxUri;

  typedef std::map<mafString,mafString> COMPONENT_SIZE_TYPE;

  typedef struct
  {
    mafString m_Manufacture;
    mafString m_Model;
    mafString m_Type;
    COMPONENT_SIZE_TYPE m_ComponetSize;
  } PROSTHESIS;

  std::vector<PROSTHESIS> m_Prosthesis;
  double m_Pca[35];
  double m_PatientHeight;
  double m_PatientWeight;
  double m_NeckVector[3];
  double m_HipNeckLength;
  double m_HipCCD;
  double m_HipFemoralCanal;
  double m_HipFemoralHead;
  double m_HipAnteversion;
  double m_StdBMI;
  double m_AvrBMI;
  double m_Calibration;
  int m_TrainingScore;
  bool m_StudyUploaded;

};
#endif
