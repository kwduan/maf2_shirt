/*=========================================================================
Program:   iPose
Module:    $RCSfile: iAttributeWorkflow.cpp,v $
Language:  C++
Date:      $Date: 2012-03-21 07:29:13 $
Version:   $Revision: 1.1.2.8 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/

#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iAttributeWorkflow.h"
#include "iDecl.h"
#include "mafStorageElement.h"
#include "mafIndent.h"

//----------------------------------------------------------------------------
mafCxxTypeMacro(iAttributeWorkflow)
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
iAttributeWorkflow::iAttributeWorkflow()
//----------------------------------------------------------------------------
{  
  m_Name = "Workflow";

  m_Side = ID_SIDE_RIGHT;
  m_PatientName = "";
  m_CurrentStep = ID_STEP_USER_CHOICE;
  m_Joint = ID_JOINT_KNEE;
  m_PatientHeight = 0.0;
  m_PatientWeight = 0.0;
  m_Calibration = 0.0;

  for (int i=0;i<35;i++)
  {
    m_Pca[i] = 0.0; 
  }

  for (int i=0;i<3;i++)
  {
    m_NeckVector[i] = 0.0;
  }

  m_HipNeckLength = 0.0;
  m_HipCCD = 0.0;
  m_HipFemoralCanal = 0.0;
  m_HipFemoralHead = 0.0;
  m_HipAnteversion = 0.0;
  m_Diagnosis = "";
  m_StdBMI = 0.0;
  m_AvrBMI = 0.0;
  m_PatientSex = "f";
  m_PatientBirthdate = "01/01/1940";
  m_TrainingScore = 0;
  m_StudyUploaded = false;
  m_RxUri = "";
}
//----------------------------------------------------------------------------
iAttributeWorkflow::~iAttributeWorkflow()
//----------------------------------------------------------------------------
{
}
//-------------------------------------------------------------------------
void iAttributeWorkflow::DeepCopy(const mafAttribute *a)
//-------------------------------------------------------------------------
{ 
  Superclass::DeepCopy(a);

  m_CurrentStep = ((iAttributeWorkflow*)a)->GetCurrentStep();
  m_PatientName = ((iAttributeWorkflow*)a)->GetPatientName();
  m_Side = ((iAttributeWorkflow*)a)->GetSide();
  m_Joint = ((iAttributeWorkflow*)a)->GetJoint();
  m_PatientHeight = ((iAttributeWorkflow*)a)->GetPatientHeight();
  m_PatientWeight = ((iAttributeWorkflow*)a)->GetPatientWeight();
  m_PatientSex = ((iAttributeWorkflow*)a)->GetPatientSex();
  m_PatientBirthdate = ((iAttributeWorkflow*)a)->GetPatientBirthdate();

  ((iAttributeWorkflow*)a)->GetPCA(m_Pca);
  ((iAttributeWorkflow*)a)->GetNeckVector(m_NeckVector);
  
  m_HipNeckLength = ((iAttributeWorkflow*)a)->GetHipNeckLength();
  m_HipCCD = ((iAttributeWorkflow*)a)->GetHipCCD();
  m_HipFemoralCanal = ((iAttributeWorkflow*)a)->GetHipFemoralCanal();
  m_HipFemoralHead = ((iAttributeWorkflow*)a)->GetHipFemoralHead();
  m_HipAnteversion = ((iAttributeWorkflow*)a)->GetHipAnteversion();
  m_Diagnosis = ((iAttributeWorkflow*)a)->GetDiagnosis();
  m_StdBMI = ((iAttributeWorkflow*)a)->GetStdBMI();
  m_AvrBMI = ((iAttributeWorkflow*)a)->GetAvrBMI();
  m_Calibration = ((iAttributeWorkflow*)a)->GetCalibration();
  m_TrainingScore = ((iAttributeWorkflow*)a)->GetScore();

  m_StudyUploaded = ((iAttributeWorkflow*)a)->StudyIsUploaded() == true;

  m_RxUri = ((iAttributeWorkflow*)a)->GetRxUri();


  for (int i=0;i<((iAttributeWorkflow*)a)->GetNumberOfProsthesis();i++)
  {
    PROSTHESIS prosthesis;
    prosthesis.m_Manufacture = ((iAttributeWorkflow*)a)->GetManufacture(i);
    prosthesis.m_Model = ((iAttributeWorkflow*)a)->GetModel(i);
    prosthesis.m_Type = ((iAttributeWorkflow*)a)->GetProsthesisType(prosthesis.m_Manufacture,prosthesis.m_Model);

    std::map<mafString,mafString> tmp;
    ((iAttributeWorkflow*)a)->GetComponentsSizes(prosthesis.m_Manufacture,prosthesis.m_Model,tmp);
    COMPONENT_SIZE_TYPE::iterator iter;
    for (iter = tmp.begin();iter != tmp.end(); iter++)
    {
      prosthesis.m_ComponetSize[iter->first] = iter->second;
    }

    m_Prosthesis.push_back(prosthesis);

  }

}
//----------------------------------------------------------------------------
bool iAttributeWorkflow::Equals(const mafAttribute *a)
//----------------------------------------------------------------------------
{
  if (Superclass::Equals(a))
  {
    if (m_CurrentStep != ((iAttributeWorkflow*)a)->GetCurrentStep())
    {
      return false;
    }

    if (m_PatientName != ((iAttributeWorkflow*)a)->GetPatientName())
    {
      return false;
    }
    
    if (m_Side != ((iAttributeWorkflow*)a)->GetSide())
    {
      return false;
    }

    if (m_Joint != ((iAttributeWorkflow*)a)->GetJoint())
    {
      return false;
    }

    if (m_PatientHeight != ((iAttributeWorkflow*)a)->GetPatientHeight())
    {
      return false;
    }

    if (m_PatientWeight != ((iAttributeWorkflow*)a)->GetPatientWeight())
    {
      return false;
    }

    if (m_PatientSex != ((iAttributeWorkflow*)a)->GetPatientSex())
    {
      return false;
    }

    if (m_PatientBirthdate != ((iAttributeWorkflow*)a)->GetPatientBirthdate())
    {
      return false;
    }

    double pca[35];
    ((iAttributeWorkflow*)a)->GetPCA(pca);
    for (int i=0;i<35;i++)
    {
      if (m_Pca[i] != pca[i])
      {
        return false;
      }
      
    }

    double neckVector[35];
    ((iAttributeWorkflow*)a)->GetNeckVector(neckVector);
    for (int i=0;i<3;i++)
    {
      if (m_NeckVector[i] != neckVector[i])
      {
        return false;
      }

    }

    if ( ((iAttributeWorkflow*)a)->GetHipNeckLength() != m_HipNeckLength)
    {
      return false;
    }

    if ( ((iAttributeWorkflow*)a)->GetHipCCD() != m_HipCCD)
    {
      return false;
    }

    if ( ((iAttributeWorkflow*)a)->GetHipFemoralCanal() != m_HipFemoralCanal)
    {
      return false;
    }

    if ( ((iAttributeWorkflow*)a)->GetHipFemoralHead() != m_HipFemoralHead)
    {
      return false;
    }

    if ( ((iAttributeWorkflow*)a)->GetHipAnteversion() != m_HipAnteversion)
    {
      return false;
    }
    if ( ((iAttributeWorkflow*)a)->GetDiagnosis() != m_Diagnosis)
    {
      return false;
    }
    if ( ((iAttributeWorkflow*)a)->GetStdBMI() != m_StdBMI)
    {
      return false;
    }
    if ( ((iAttributeWorkflow*)a)->GetAvrBMI() != m_AvrBMI)
    {
      return false;
    }
    if ( ((iAttributeWorkflow*)a)->GetCalibration() != m_Calibration)
    {
      return false;
    }
    if ( ((iAttributeWorkflow*)a)->GetScore() != m_TrainingScore)
    {
      return false;
    }
    if ( ((iAttributeWorkflow*)a)->GetRxUri() != m_RxUri)
    {
      return false;
    }

    return true;
  }
  return false;
}
//-----------------------------------------------------------------------
int iAttributeWorkflow::InternalStore(mafStorageElement *parent)
//-----------------------------------------------------------------------
{  
  if (Superclass::InternalStore(parent)==MAF_OK)
  {
    parent->StoreInteger("CurrentStep",m_CurrentStep);
    parent->StoreInteger("Side",m_Side);
    parent->StoreInteger("Joint",m_Joint);
    parent->StoreText("PatientName",m_PatientName.GetCStr());
    parent->StoreDouble("PatientWeight",m_PatientWeight);
    parent->StoreDouble("PatientHeight",m_PatientHeight);
    parent->StoreText("PatientSex",m_PatientSex);
    parent->StoreText("PatientBirthdate",m_PatientBirthdate);
    parent->StoreVectorN("PCA",m_Pca,35);
    parent->StoreVectorN("NeckVector",m_NeckVector,3);
    parent->StoreDouble("HipNeckLength",m_HipNeckLength);
    parent->StoreDouble("HipCCD",m_HipCCD);
    parent->StoreDouble("HipFemoralCanal",m_HipFemoralCanal);
    parent->StoreDouble("HipFemoralHead",m_HipFemoralHead);
    parent->StoreDouble("HipAnteversion",m_HipAnteversion);
    parent->StoreText("Diagnosis",m_Diagnosis);
    parent->StoreDouble("StandardDeviationBMI",m_StdBMI);
    parent->StoreDouble("AverageBMI",m_AvrBMI);
    parent->StoreDouble("RXCalibration",m_Calibration);
    parent->StoreInteger("TrainingScore",m_TrainingScore);
    int value = m_StudyUploaded ? TRUE : FALSE;
    parent->StoreInteger("StudyUploaded",value);
    parent->StoreText("RxUri",m_RxUri);

    parent->StoreInteger("NumberOfProsthesis",m_Prosthesis.size());
    for (int i=0;i<m_Prosthesis.size();i++)
    {

      mafString name = "Prosthesis_";
      name << i;
      name << "_Manufacture";
      parent->StoreText(name,m_Prosthesis[i].m_Manufacture);
      
      name = "Prosthesis_";
      name << i;
      name << "_Model";
      parent->StoreText(name,m_Prosthesis[i].m_Model);

      name = "Prosthesis_";
      name << i;
      name << "_Type";
      parent->StoreText(name,m_Prosthesis[i].m_Type);

      COMPONENT_SIZE_TYPE tmp = m_Prosthesis[i].m_ComponetSize;
      name = "Prosthesis_";
      name << i;
      name << "_NumberOfComponents";
      parent->StoreInteger(name,tmp.size());

      COMPONENT_SIZE_TYPE::iterator iter;
      int j = 0;
      for (iter = tmp.begin();iter != tmp.end(); iter++)
      {
        name = "Prosthesis_";
        name << i;
        name << "_Component_";
        name << j;

        parent->StoreText(name,iter->first);

        name = "Prosthesis_";
        name << i;
        name << "_Component_";
        name << j;
        name << "_Component_Size";

        parent->StoreText(name,iter->second);

        j++;
      }
    }

    return MAF_OK;
  }
  return MAF_ERROR;
}
//----------------------------------------------------------------------------
int iAttributeWorkflow::InternalRestore(mafStorageElement *node)
//----------------------------------------------------------------------------
{
  if (Superclass::InternalRestore(node) == MAF_OK)
  {
    node->RestoreInteger("CurrentStep",m_CurrentStep);
    node->RestoreInteger("Side",m_Side);
    node->RestoreInteger("Joint",m_Joint);
    node->RestoreText("PatientName",m_PatientName);
    node->RestoreDouble("PatientWeight",m_PatientWeight);
    node->RestoreDouble("PatientHeight",m_PatientHeight);
    node->RestoreText("PatientSex",m_PatientSex);
    node->RestoreText("PatientBirthdate",m_PatientBirthdate);
    node->RestoreVectorN("PCA",m_Pca,35);
    node->RestoreVectorN("NeckVector",m_NeckVector,3);
    node->RestoreDouble("HipNeckLength",m_HipNeckLength);
    node->RestoreDouble("HipCCD",m_HipCCD);
    node->RestoreDouble("HipFemoralCanal",m_HipFemoralCanal);
    node->RestoreDouble("HipFemoralHead",m_HipFemoralHead);
    node->RestoreDouble("HipAnteversion",m_HipAnteversion);
    node->RestoreText("Diagnosis",m_Diagnosis);
    node->RestoreDouble("StandardDeviationBMI",m_StdBMI);
    node->RestoreDouble("AverageBMI",m_AvrBMI);
    node->RestoreDouble("RXCalibration",m_Calibration);
    node->RestoreInteger("TrainingScore",m_TrainingScore);
    int value;
    node->RestoreInteger("StudyUploaded",value);
    m_StudyUploaded = value == TRUE;
    node->RestoreText("RxUri",m_RxUri);

    int numberOfProsthesis = 0;
    node->RestoreInteger("NumberOfProsthesis",numberOfProsthesis);
    for (int i=0;i<numberOfProsthesis;i++)
    {
      PROSTHESIS prosthesis;
      mafString tmpString,name;

      name = "Prosthesis_";
      name << i;
      name << "_Manufacture";
      node->RestoreText(name,tmpString);
      prosthesis.m_Manufacture = tmpString;

      name = "Prosthesis_";
      name << i;
      name << "_Model";
      node->RestoreText(name,tmpString);
      prosthesis.m_Model = tmpString;

      name = "Prosthesis_";
      name << i;
      name << "_Type";
      node->RestoreText(name,tmpString);
      prosthesis.m_Type = tmpString;

      int numOfComponents = 0;
      name = "Prosthesis_";
      name << i;
      name << "_NumberOfComponents";
      node->RestoreInteger(name,numOfComponents);

      for (int j=0;j<numOfComponents;j++)
      {
        mafString component,size;

        name = "Prosthesis_";
        name << i;
        name << "_Component_";
        name << j;
        node->RestoreText(name,component);

        name = "Prosthesis_";
        name << i;
        name << "_Component_";
        name << j;
        name << "_Component_Size";

        node->RestoreText(name,size);

        prosthesis.m_ComponetSize[component] = size;
      }

      m_Prosthesis.push_back(prosthesis);
    }

    return MAF_OK;
  }
  return MAF_ERROR;
}
//-----------------------------------------------------------------------
void iAttributeWorkflow::Print(std::ostream& os, const int tabs) const
//-----------------------------------------------------------------------
{
  Superclass::Print(os,tabs);
  mafIndent indent(tabs);

  os << indent << "Patient Name: " << m_PatientName << "\n";

  mafString stepName = ""; 
  switch(m_CurrentStep)
  {
  case ID_STEP_SELECT_CASE_EXISTING:
    stepName = "Selecting Case Exist";
    break;
  case ID_STEP_SHOW_RX:
    stepName = "Show RX";
    break;
  case ID_STEP_3D_RECONSTRUTION_START:
    stepName = "3D Reconstruction";
    break;
  case ID_STEP_PLACING_IMPLANT:
    stepName = "Placing Implant";
    break;
  case ID_STEP_CHOICE_IMPLANT:
    stepName = "ID_STEP_CHOICE_IMPLANT";
  }
  os << indent << "CurrentStep: " << stepName << "\n";

  mafString sideName = "";
  switch(m_Side)
  {
  case ID_SIDE_RIGHT:
    sideName = "Right";
    break;
  case ID_SIDE_LEFT:
    sideName = "Left";
    break;
  }
  os<< indent << "Side: " << sideName << "\n";

  mafString jointName = "";
  switch(m_Joint)
  {
  case ID_JOINT_HIP:
    jointName = "Hip";
    break;
  case ID_JOINT_KNEE:
    jointName = "Knew";
    break;
  case ID_JOINT_SHOULDER:
    jointName = "Shoulder";
    break;
  }
  os<< indent << "Joint: " << jointName << "\n";
  os<< indent << "Patient Weight: " << m_PatientWeight << "\n";
  os<< indent << "Patient Height: " << m_PatientHeight << "\n";
  os<< indent << "Patient Sex: " << m_PatientSex << "\n";
  os<< indent << "Patient Birthdate: " << m_PatientBirthdate << "\n";
  os<< indent << "Diagnosis: " << m_Diagnosis << "\n";

  os<< indent << "Hip Neck Length: " << m_HipNeckLength<< "\n";
  os<< indent << "Hip CCD: " << m_HipCCD<< "\n";
  os<< indent << "Hip Femoral Canal: " << m_HipFemoralCanal<< "\n";
  os<< indent << "Hip Femoral Head: " << m_HipFemoralHead<< "\n";
  os<< indent << "Hip Anteversion: " << m_HipAnteversion<< "\n";
  os<< indent << "Standard deviation BMI: " << m_StdBMI<< "\n";
  os<< indent << "Average BMI: " << m_AvrBMI<< "\n";
  os<< indent << "RX Uri: " << m_RxUri<< "\n";

  for (int i=0;i<m_Prosthesis.size();i++)
  {
    os<< indent << "Prosthesis:\n";
    os<< indent << indent << "Manufacture: " << m_Prosthesis[i].m_Manufacture << "\n";
    os<< indent << indent << "Model: " << m_Prosthesis[i].m_Model << "\n";

    COMPONENT_SIZE_TYPE::iterator iter;
    COMPONENT_SIZE_TYPE tmp = m_Prosthesis[i].m_ComponetSize;
    for (iter = tmp.begin();iter != tmp.end(); iter++)
    {
      os<< indent << indent << "Component: " << iter->first << " " << iter->second << "\n";
    }

  }

  os<< indent << "RX calibration: " << m_Calibration << "\n";
}
//----------------------------------------------------------------------------
int iAttributeWorkflow::GetCurrentStep()
//----------------------------------------------------------------------------
{
  return m_CurrentStep;
}
//----------------------------------------------------------------------------
void iAttributeWorkflow::SetCurrentStep(int step)
//----------------------------------------------------------------------------
{
  m_CurrentStep = step;
}
//----------------------------------------------------------------------------
void iAttributeWorkflow::SetPatientName(mafString patientName)
//----------------------------------------------------------------------------
{
  m_PatientName = patientName;
}
//----------------------------------------------------------------------------
mafString iAttributeWorkflow::GetPatientName()
//----------------------------------------------------------------------------
{
  return m_PatientName;
}
//----------------------------------------------------------------------------
void iAttributeWorkflow::SetSide(int side)
//----------------------------------------------------------------------------
{
  m_Side = side;
}
//----------------------------------------------------------------------------
int iAttributeWorkflow::GetSide()
//----------------------------------------------------------------------------
{
  return m_Side;
}
//----------------------------------------------------------------------------
void iAttributeWorkflow::SetJoint(int joint)
//----------------------------------------------------------------------------
{
  m_Joint = joint;
}
//----------------------------------------------------------------------------
int iAttributeWorkflow::GetJoint()
//----------------------------------------------------------------------------
{
  return m_Joint;
}
//----------------------------------------------------------------------------
void iAttributeWorkflow::SetPatientWeight(double weight)
//----------------------------------------------------------------------------
{
  m_PatientWeight = weight;
}
//----------------------------------------------------------------------------
void iAttributeWorkflow::SetPatientHeight(double height)
//----------------------------------------------------------------------------
{
  m_PatientHeight = height;
}
//----------------------------------------------------------------------------
void iAttributeWorkflow::SetPCA( double pca[35] )
//----------------------------------------------------------------------------
{
  for (int i=0;i<35;i++)
  {
    m_Pca[i] = pca[i];
  }
}
//----------------------------------------------------------------------------
void iAttributeWorkflow::GetPCA( double pca[35] )
//----------------------------------------------------------------------------
{
  for (int i=0;i<35;i++)
  {
    pca[i] = m_Pca[i];
  }
}
//----------------------------------------------------------------------------
void iAttributeWorkflow::GetNeckVector(double neckVector[3])
//----------------------------------------------------------------------------
{
  for (int i=0;i<3;i++)
  {
    neckVector[i] = m_NeckVector[i];
  }
}
//----------------------------------------------------------------------------
void iAttributeWorkflow::SetNeckVector(double neckVector[3])
//----------------------------------------------------------------------------
{
  for (int i=0;i<3;i++)
  {
    m_NeckVector[i] = neckVector[i];
  }
}
//----------------------------------------------------------------------------
void iAttributeWorkflow::SetHipNeckLength( double neckLength )
//----------------------------------------------------------------------------
{
  m_HipNeckLength = neckLength;
}
//----------------------------------------------------------------------------
void iAttributeWorkflow::SetHipCCD( double ccd )
//----------------------------------------------------------------------------
{
  m_HipCCD = ccd;
}
//----------------------------------------------------------------------------
double iAttributeWorkflow::GetHipCCD()
//----------------------------------------------------------------------------
{
  return m_HipCCD;
}

void iAttributeWorkflow::SetHipFemoralCanal( double femoralCanal )
{
  m_HipFemoralCanal = femoralCanal;
}

double iAttributeWorkflow::GetHipFemoralCanal()
{
  return m_HipFemoralCanal;
}

void iAttributeWorkflow::SetHipFemoralHead( double femoralHead )
{
  m_HipFemoralHead = femoralHead;
}

double iAttributeWorkflow::GetHipFemoralHead()
{
  return m_HipFemoralHead;
}

void iAttributeWorkflow::SetHipAnteversion(double anteversion)
{
  m_HipAnteversion = anteversion;
}

double iAttributeWorkflow::GetHipAnteversion()
{
  return m_HipAnteversion;
}

double iAttributeWorkflow::GetHipNeckLength()
{
  return m_HipNeckLength;
}

void iAttributeWorkflow::SetDiagnosis( mafString diagnosis)
{
  m_Diagnosis = diagnosis;
}

mafString iAttributeWorkflow::GetDiagnosis()
{
  return m_Diagnosis;
}

void iAttributeWorkflow::SetStdBMI( double stdBmi )
{
  m_StdBMI = stdBmi;
}

double iAttributeWorkflow::GetStdBMI()
{
  return m_StdBMI;
}

void iAttributeWorkflow::SetAvrBMI( double avrBmi )
{
  m_AvrBMI = avrBmi;
}

double iAttributeWorkflow::GetAvrBMI()
{
  return m_AvrBMI;
}

void iAttributeWorkflow::AddProsthesis( mafString manufacture,mafString model,mafString type,std::map<mafString,mafString> componenteSize )
{
  PROSTHESIS prosthesis;
  prosthesis.m_Manufacture = manufacture;
  prosthesis.m_Model = model;
  prosthesis.m_Type = type;

  std::map<mafString,mafString>::iterator iter;
  for (iter = componenteSize.begin();iter != componenteSize.end(); iter++)
  {
    prosthesis.m_ComponetSize[iter->first] = iter->second;
  }

  m_Prosthesis.push_back(prosthesis);

}

mafString iAttributeWorkflow::GetProsthesisType(mafString manufacture,mafString model)
{
  for(int i=0;i<m_Prosthesis.size();i++)
  {
    if(m_Prosthesis[i].m_Manufacture == manufacture && m_Prosthesis[i].m_Model)
    {
      return m_Prosthesis[i].m_Type;
    }
  }
  return "";
}

int iAttributeWorkflow::GetNumberOfComponents(mafString manufacture,mafString model)
{
  for(int i=0;i<m_Prosthesis.size();i++)
  {
    if(m_Prosthesis[i].m_Manufacture == manufacture && m_Prosthesis[i].m_Model)
    {
      return m_Prosthesis[i].m_ComponetSize.size();
    }
  }
  return 0;
}

void iAttributeWorkflow::GetComponentsSizes(mafString manufacture,mafString model,std::map<mafString,mafString> &componentsSizes)
{

  for(int i=0;i<m_Prosthesis.size();i++)
  {
    if(m_Prosthesis[i].m_Manufacture == manufacture && m_Prosthesis[i].m_Model)
    {
      componentsSizes = m_Prosthesis[i].m_ComponetSize;
      return;
    }
  }

}

void iAttributeWorkflow::SetPatientSex( mafString sex )
{
  m_PatientSex = sex;
}

void iAttributeWorkflow::SetPatientBirthdate( mafString birthdate )
{
  m_PatientBirthdate = birthdate;
}

double iAttributeWorkflow::GetCalibration()
{
  return m_Calibration;
}

void iAttributeWorkflow::SetCalibration( double calibration )
{
  m_Calibration = calibration;
}

void iAttributeWorkflow::SetScore( int score )
{
  m_TrainingScore = score;
}

int iAttributeWorkflow::GetScore()
{
  return m_TrainingScore;
}

void iAttributeWorkflow::SetRxUri( mafString rxUri )
{
  m_RxUri = rxUri;
}

mafString iAttributeWorkflow::GetRxUri()
{
  return m_RxUri;
}
