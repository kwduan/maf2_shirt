/*=========================================================================
Program:   iPose
Module:    $RCSfile: iAttributeROM.h,v $
Language:  C++
Date:      $Date: 2012-04-06 09:50:16 $
Version:   $Revision: 1.1.2.9 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2012
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/

#ifndef __hipAttributeROM_H__
#define __hipAttributeROM_H__

//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "iVMEDefines.h"
#include "iDecl.h"
#include "mafAttribute.h"
#include <map>

//----------------------------------------------------------------------------
// forward refs :
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// hipAttributeROM:
//----------------------------------------------------------------------------
/**  */  
class I_VME_EXPORT iAttributeROM : public mafAttribute
{
public:
  /** constructor. */
  iAttributeROM();
  /** destructor. */
  virtual ~iAttributeROM();

  /** RTTI macro.*/
  mafTypeMacro(iAttributeROM, mafAttribute);

  /** Print a dump of this object */
  /*virtual*/ void Print(std::ostream& os, const int tabs=0) const;

  /** Copy the contents of another Meter attribute into this one. */
  /*virtual*/ void DeepCopy(const mafAttribute *a);

  /** Compare with another Meter attribute. */
  /*virtual*/ bool Equals(const mafAttribute *a);

  /** store position for a prosthesis vme (vme is identify by his ID) */
  void AddProsthesisPose(int vmeID,double position[3],double orientation[3]);

  /** return number of positions */
  int GetNumberOfPositions();

  /** return number of orientations */
  int GetNumberOfOrientations();

  /** return a position for a particular vme id - return true if element is found otherwise return false */
  bool GetPosition(int vmeID,double position[3]);

  /** return a orientation for a particular vme id - return true if element is found otherwise return false */
  bool GetOrientation(int vmeID,double orientation[3]);

  /** add a movement to store */
  void AddMovement(int vmeID, wxString name, double angleInf, double angleSup, double angleCollision, double axisX, double axisY, double axisZ, bool shoMovement, bool startFromPrevious, bool passed);
 
  /** remove all movements for a particular vme femoral prosthesis */
  void CleanMovements(int vmeID);

  /** return number of movements for a particular vme femoral prosthesis  */
  int GetNumberOfMovementsPerVME(int vmeID);

  /** return a single movement */
  void GetMovement(int vmeID,int index,wxString &name, double &angleInf, double &angleSup, double &angleCollision, double axis[3], bool &showMovement, bool &startFromPrevious, bool &passed);

  std::map<int,double*> m_Positions;
  std::map<int,double*> m_Orientations;
  std::map<int,std::vector<DB_MOVEMENTS_INFORMATION>> m_Momements;//<<<a set of movements should be associated to a single vme femoral prosthesis

protected:
  //** Auto storing function */  
  /*virtual*/ int InternalStore(mafStorageElement *parent);

  //** Auto restoring function */  
  /*virtual*/ int InternalRestore(mafStorageElement *node);

};
#endif
