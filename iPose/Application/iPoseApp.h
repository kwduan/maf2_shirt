/*=========================================================================
  Program:   Multimod Application Framework
  Module:    $RCSfile: iPoseApp.h,v $
  Language:  C++
  Date:      $Date: 2011-09-05 14:14:06 $
  Version:   $Revision: 1.1.2.2 $
  Authors:   Matteo Giacomoni
==========================================================================
  Copyright (c) 2001/2005 
  CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/
#ifndef __iPoseApp_H__
#define __iPoseApp_H__
//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class lhpUser;
class iLogic;

class iPoseApp : public wxApp
{
public:
  bool OnInit();
  int  OnExit();

protected:
  iLogic *m_Logic;

};
DECLARE_APP(iPoseApp)
//BES: 28.5.2009 - cannot link under VS 2008 because of some missing link libs
#if _MSC_VER >= 1500
#pragma comment( lib, "ofstd.lib" ) 
#pragma comment( lib, "netapi32.lib" ) 
#endif
#endif 
