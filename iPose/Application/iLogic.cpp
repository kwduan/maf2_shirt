/*=========================================================================
Program:   iPose
Module:    $RCSfile: iLogic.cpp,v $
Language:  C++
Date:      $Date: 2011-09-28 14:03:48 $
Version:   $Revision: 1.1.2.2 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/


#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iLogic.h"
#include "iDecl.h"

#include "mafOpManager.h"
#include "mafOp.h"
#include "mafGUIMDIFrame.h"

//----------------------------------------------------------------------------
iLogic::iLogic():
lhpBuilderLogic()
//----------------------------------------------------------------------------
{
}
//----------------------------------------------------------------------------
iLogic::~iLogic()
//----------------------------------------------------------------------------
{
}
//----------------------------------------------------------------------------
void iLogic::OnEvent(mafEventBase *maf_event)
//----------------------------------------------------------------------------
{
	if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
	{
		switch(e->GetId())
		{
    case IPOSE_SAVE:
      {
        if (m_VMEManager->MSFIsModified())
        {
          OnFileSave();
        }
      }
      break;
    case IPOSE_MSF_NEW:
      {
        if(m_VMEManager)
        {
          m_VMEManager->MSFNew();
        }
	      m_Win->SetTitle(wxString(m_AppTitle.GetCStr()));
      }
      break;
    case IPOSE_GET_ROOT:
      {
        e->SetVme(m_VMEManager->GetRoot());
      }
      break;
    case MENU_FILE_SAVE:
			{
        if (e->GetString() && !(e->GetString()->IsEmpty()))
        /*if( this->m_OpManager->GetRunningOperation() && this->m_OpManager->GetRunningOperation()->IsA("iOpPlanningWorkflow") && this->m_OpManager->GetRunningOperation() == e->GetSender() )*/
        {
          mafString *fileName = e->GetString();
          this->m_VMEManager->SetFileName(*fileName);
        }
        
				OnFileSave(); 

				ValidateMSFTree();
			}
		break;
		default:
			lhpBuilderLogic::OnEvent(maf_event);
			break; 
		} // end switch case
	} // end if SafeDowncast
}
