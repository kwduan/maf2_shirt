/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialogJointLoading.h,v $
Language:  C++
Date:      $Date: 2012-03-16 15:03:26 $
Version:   $Revision: 1.1.2.4 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it
=========================================================================*/

#ifndef __iGuiDialogJointLoading_H__
#define __iGuiDialogJointLoading_H__

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class mafRWI;
class mafDeviceButtonsPadMouse;
class mafVMERoot;
class mafGUIButton;
class vtkActor;
class vtkPolyDataMapper;
class vtkTexture;
class vtkWindowLevelLookupTable;
class vtkPlaneSource;
class vtkTextActor;

//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "mafGUIDialog.h"
#include <map>

//----------------------------------------------------------------------------
// Const:
//----------------------------------------------------------------------------

/** iGuiDialogJointLoading
*/
class iGuiDialogJointLoading : public mafGUIDialog
{
public:

  /** Constructor to execute joint loading external app*/
  iGuiDialogJointLoading (const wxString& title,mafDeviceButtonsPadMouse *mouse,mafVMERoot *input,int jointType,int id,double weight, bool training = false);

  /** Constructor to don't execute joint loading external app*/
  iGuiDialogJointLoading (const wxString& title,mafDeviceButtonsPadMouse *mouse,mafVMERoot *input,double vector[3], bool training = false);

  virtual ~iGuiDialogJointLoading (); 

  /** process events coming from other components */
  /*virtual*/ void OnEvent(mafEventBase *maf_event);

  /** execute the external application to compute primary stability */
  static bool ExecuteExternalApplication(double weight,int patientId,double vectorOut[3],mafVMERoot *root,bool training = false);

protected:

  enum ID_GUIs
  {
    ID_CANCEL = MINID,
    ID_ACCEPT_BUTTON,
    ID_REJECT_BUTTON,
  };

  /** Create the interface */
  void CreateGui();

  /** visualize the first vme in the three with the tag "3D_RECONSTRUCTION" */
  void ShowReconstruction3D();

  /** show vector in the rwi */
  void ShowVector(double vector[3]);

  /** show landmarks in the rwi */
  void ShowLandmarks();

  vtkTextActor *m_ComponentsTextActors;

  mafRWI *m_Rwi3DView;

  mafDeviceButtonsPadMouse *m_Mouse;

  mafGUIButton *m_ButtonAccept;

  mafVMERoot *m_Input;

  bool m_Training;

};
#endif
