/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialogShowRx.cpp,v $
Language:  C++
Date:      $Date: 2012-02-06 10:52:30 $
Version:   $Revision: 1.1.2.30 $
Authors:   Matteo Giacomoni , Stefano Perticoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/


#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iGuiDialogShowRx.h"
#include "iDecl.h"

#include "mafGUIButton.h"
#include "mafGUIValidator.h"
#include "mafRWI.h"
#include "mafDeviceButtonsPadMouse.h"
#include "mafVMEImage.h"
#include "mafEventIO.h"
#include "mafVMERoot.h"
#include "mafTagArray.h"
#include "mafNodeIterator.h"
#include "mafTransform.h"
#include "medInteractorDICOMImporter.h"
#include "vtkLineSource.h"
#include "vtkSphereSource.h"
#include "vtkPlane.h"
#include "vtkCutter.h"
#include "vtkPolyData.h"
#include "vtkTextProperty.h"

#if _MSC_VER >= 1500
#define ssize_t VS2008_ssize_t_HACK 
#endif

#include "dcmtk/config/osconfig.h"    /* make sure OS specific configuration is included first */

#if _MSC_VER >= 1500
#undef VS2008_ssize_t_HACK
#endif

#include "dcmtk/ofstd/ofstream.h"
#include "dcmtk/ofstd/ofstring.h"
#include "dcmtk/dcmdata/dctk.h"
#include "dcmtk/dcmdata/dcdebug.h"
#include "dcmtk/dcmdata/cmdlnarg.h"
#include "dcmtk/ofstd/ofconapp.h"
#include "dcmtk/dcmdata/dcuid.h"       /* for dcmtk version name */
#include "dcmtk/dcmdata/dcistrmz.h"    /* for dcmZlibExpectRFC1950Encoding */
#include "dcmtk/dcmimgle/dcmimage.h."
#include "dcmtk/dcmjpeg/djdecode.h."
#include "dcmtk/dcmdata/dcrledrg.h"

#include "vtkMAFSmartPointer.h"
#include "vtkRenderWindow.h"
#include "vtkImageData.h"
#include "vtkCharArray.h"
#include "vtkPointData.h"
#include "vtkShortArray.h"
#include "vtkUnsignedShortArray.h"
#include "vtkDataSetWriter.h"
#include "vtkTexture.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkWindowLevelLookupTable.h"
#include "vtkPlaneSource.h"
#include "vtkRenderer.h"
#include "vtkCamera.h"
#include "vtkDataSetWriter.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkTransform.h"
#include <wx/tokenzr.h>
#include <wx/busyinfo.h>
#include "vtkPoints.h"
#include "vtkTubeFilter.h"
#include "vtkMath.h"
#include <string>
#include <algorithm>
#include "wx/filename.h"
#include "vcl_cassert.h"
#include "vtkProperty.h"
#include "iGuiDialogListCase.h"
#include "mafVMEStorage.h"
#include "vtkCaptionActor2D.h"
#include <wx/dir.h>


const wxString RX_CALIBRATION_SPACING_FILENAME = "RXImageCalibrationSpacing.txt";
const wxString RX_DICOM_FILENAME = "RXDicomFileName.txt";
const int UNDEFINED_CALIBRATION_SPACING = -9999;

const mafString UNDEFINED_PATIENT_CASE_ID = "";

//----------------------------------------------------------------------------
iGuiDialogShowRx::iGuiDialogShowRx(const wxString& title,mafDeviceButtonsPadMouse *mouse, mafVMERoot *input, int dialogType , mafString rxFileName /* = "" */, mafString patientCaseId /* = "" */, 
  bool training /* = false */) : mafGUIDialog(title,mafCLOSEWINDOW | mafRESIZABLE)
	//----------------------------------------------------------------------------
{
	m_PatientCaseId = patientCaseId;
	m_Spacing[0] = -1;
	m_Spacing[1] = -1;
	m_Spacing[2] = -1;

	m_MeasureValue = 0;
	m_MeasureNameId = 0;
	m_JointNameId = 0;
  m_PatientHeight = 0.0;
  m_PatientWeight = 0.0;

	m_CurrentMeasure = NULL;

	m_CalibrationShape = LINE;
	m_CalibrationEnable = 0;
	m_CalibratorSize = 1;
	m_Dragging = false;
	m_ResizingCircle = false;

	m_P1LinePicked = false;
	m_P1Grabbed = false;
	m_P2LinePicked = false;
	m_P2Grabbed = false;

	m_RXSpacingAfterCalibrationGUIPreview = 0; // undefined
	m_RXImageCalibrationSpacing = UNDEFINED_CALIBRATION_SPACING;
	m_RepereSize = 100;

	m_CalibrationLine = NULL;
	m_CalibrationLineTubeFilter = NULL;
	m_CalibrationLineMapper = NULL;
	m_CalibrationLineActor = NULL;

	m_CalibrationCircleSphereSource = NULL;
	m_CalibrationCircleCuttingPlane = NULL;
	m_CalibrationCircleCutter = NULL;
	m_CalibrationCircleTubeFilter = NULL;
	m_CalibrationCirclePDM = NULL;

	vtkActor* m_Actor;

	m_Rwi3DView = NULL;

	m_Input = input;

	m_Mouse = mouse;
	m_DialogType = dialogType;

  m_Training = training;

	//If filename isn't empty I must add the abs path to the filename
	m_FileRX = rxFileName;

	if (m_DialogType == SHOW_RX_ONLY && !training)
	{
		LoadCalibrationSpacing();
	}
	if (m_DialogType == CALIBRATE_TOOL && m_FileRX == "" && !training)
	{
		LoadDicomFileName();
	}
	else if (m_DialogType == MEASURE_TOOL && m_FileRX == "" && !training)
	{
		LoadDicomFileName();
		LoadCalibrationSpacing();
	}
	

	m_PatientName = "";
	m_SpacingLabel = "";
	m_Side = ID_SIDE_RIGHT;
	m_Joint = ID_JOINT_KNEE;

	m_SliceActor = NULL;
	m_SliceMapper = NULL;
	m_SliceTexture = NULL;
	m_SliceLookupTable = NULL;
	m_SlicePlane = NULL;

	m_OutputImage = NULL;

	int x_pos,y_pos,w,h;
	mafGetFrame()->GetPosition(&x_pos,&y_pos);
	this->GetSize(&w,&h);
	this->SetSize(x_pos+5,y_pos+5,w,h);

	this->SetSize(640, 480);

	CreateGui();

	CreateSlicePipeline();
	CreateCalibrationLinePipeline();
	CreateCalibrationCirclePipeline();

	if (m_FileRX != "")
	{
		ReadDICOM();
	}

	ShowRX();

}
//----------------------------------------------------------------------------
iGuiDialogShowRx::~iGuiDialogShowRx()
	//----------------------------------------------------------------------------
{
	if(m_DialogType && !m_Training)
	{
		FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_RX);
	}

	mafDEL(m_OutputImage);

	if (m_SliceActor)
	{
		m_Rwi3DView->m_RenFront->RemoveActor(m_SliceActor);
	}
	cppDEL(m_Rwi3DView);

	vtkDEL(m_SlicePlane);
	vtkDEL(m_SliceActor);
	vtkDEL(m_SliceMapper);
	vtkDEL(m_SliceTexture);
	vtkDEL(m_SliceLookupTable);

	// calibration
	vtkDEL(m_CalibrationLine);
	vtkDEL(m_CalibrationLineTubeFilter);
	vtkDEL(m_CalibrationLineMapper);
	vtkDEL(m_CalibrationLineActor);

	if(m_PickerInteractor)
	{
		m_Mouse->RemoveObserver(m_PickerInteractor);
	}

}
//----------------------------------------------------------------------------
void iGuiDialogShowRx::OnEvent(mafEventBase *maf_event)
	//----------------------------------------------------------------------------
{
	if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
	{
		switch(e->GetId())
		{

			case ID_ADD_TO_REPORT:
			{

				StoreMeasures();
			}
			break;

		case ID_OPEN_RX_FILE:
			{
				if (m_FileRX != "")
				{
					ReadDICOM();
					ShowRX();
				}
			}
			break;

		case ID_MEASURE_NAME_COMBO:
			{
				// deselect old measure
				m_CurrentMeasure->Select(false);

				m_CurrentMeasure = m_MeasuresVector[m_MeasureNameId];

				// select new measure
				m_CurrentMeasure->Select(true);
				m_Rwi3DView->CameraUpdate();

				m_MeasureValue = m_CurrentMeasure->GetValue();

				
				UpdateMeasureValues();

				m_LateralGui->Update();

			}
			break;

		case ID_CALIBRATE_BUTTON:
			{
				vtkImageData *id = vtkImageData::SafeDownCast(m_OutputImage->GetVTKOutput()->GetVTKData());
				assert(id);

				id->SetSpacing(m_RXSpacingAfterCalibrationGUIPreview, m_RXSpacingAfterCalibrationGUIPreview , 1);
				id->Modified();

				std::ostringstream stringStream;
				stringStream << setprecision(4) << m_RXSpacingAfterCalibrationGUIPreview;

				m_SpacingLabel = stringStream.str().c_str();
				m_SpacingLabel << " , ";
				m_SpacingLabel << stringStream.str().c_str();

				m_LateralGui->Update();

				m_RXImageCalibrationSpacing = m_RXSpacingAfterCalibrationGUIPreview;
			}
			break;

		case ID_REMOVE_CALIBRATOR_SHAPE_BUTTON:
			{
				OnIdRemoveCalibrationShapeButton();
			}
			break;

		case ID_CALIBRATOR_SHAPE:
			{
				OnIdCalibratorShape();
			}
			break;

		case MOUSE_DOWN:
			{
				double dicomSlicePlaneOrigin[3];
				m_SlicePlane->GetOrigin(dicomSlicePlaneOrigin);

				m_Dragging = true;
				double pos[3];
				vtkPoints *p = (vtkPoints *)e->GetVtkObj();
				p->GetPoint(0,pos);				
				mafLogMessage("mouse down");
				pos[1] = dicomSlicePlaneOrigin[1];

				std::ostringstream stringStream;
				stringStream << "Picked Position: " << pos[0] << " " << pos[1] << " " << pos[2] << std::endl;          
				mafLogMessage(stringStream.str().c_str());

				if (m_DialogType == CALIBRATE_TOOL)
				{
					if (m_CalibrationShape == LINE)
						OnMouseDownLine(pos);

					else if (m_CalibrationShape == CIRCLE) // mouse down
						OnMouseDownCircle(pos);
				}
				else if (m_DialogType == MEASURE_TOOL)
				{
					m_CurrentMeasure->Select(true);
					m_CurrentMeasure->OnMouseDown(pos);
					m_MeasureValue = m_CurrentMeasure->GetValue();

					UpdateMeasureValues();

					m_LateralGui->Update();
				}
				break;

			}
		case MOUSE_MOVE:
			{
				double dicomSlicePlaneOrigin[3];
				m_SlicePlane->GetOrigin(dicomSlicePlaneOrigin);

				double currentPosition[3];
				vtkPoints *p = (vtkPoints *)e->GetVtkObj();
				p->GetPoint(0,currentPosition);
				currentPosition[1] = dicomSlicePlaneOrigin[1];

				std::ostringstream stringStream;
				stringStream << "Dragged Position: " << currentPosition[0] << " " << currentPosition[1] << " " << currentPosition[2] << std::endl;          
				mafLogMessage(stringStream.str().c_str());

				if (m_DialogType == CALIBRATE_TOOL)
				{
					if (m_CalibrationShape == LINE)
					{
						OnMouseMoveLine(currentPosition);
					}
					else if (m_CalibrationShape == CIRCLE) // mouse move
					{
						OnMouseMoveCircle(currentPosition);
					}
				}
				else if (m_DialogType == MEASURE_TOOL)
				{
					m_CurrentMeasure->OnMouseMove(currentPosition);
					m_MeasureValue = m_CurrentMeasure->GetValue();

					UpdateMeasureValues();

					m_LateralGui->Update();
				}

			}
			break;

		case MOUSE_UP:
			{
				if (m_DialogType == CALIBRATE_TOOL)
				{
					m_Dragging = false;
					m_P1Grabbed = false;
					m_P2Grabbed = false;
					m_ResizingCircle = true;
					mafLogMessage("mouse up");
				}
				else if (m_DialogType == MEASURE_TOOL)
				{
					m_CurrentMeasure->OnMouseUp();
					m_MeasureValue = m_CurrentMeasure->GetValue();

					UpdateMeasureValues();

					m_LateralGui->Update();
				}
			}
			break; 

		case ID_REPERE_SIZE:
			{
				UpdateSpacingAfterCalibration();
				m_LateralGui->Update();
			}
			break;

		case ID_CALIBRATION_MODE:
			{
				EnableCustomPickerInteractor();
			}
			break;

		case ID_CANCEL:
			{
				//FindAndRemove(m_Input,TAG_RX);
				this->EndModal(wxCANCEL);
			}
			break;
		case ID_ACCEPT_BUTTON:
			//If dialog are in modality open RX file
			if (m_DialogType == ADD_POSSIBILITY_TO_OPEN_RX)
			{
        grid_element element;
        element.m_FileNameRX = m_FileRX;
        element.m_Height = m_PatientHeight;
        element.m_Weight = m_PatientWeight;
        element.m_Side = m_Side;
        element.m_Joint = m_Joint;
        element.m_Diagnosis = m_Diagnosis;
        element.m_Name = m_PatientName,
				UpdateCache(m_FileRX,element);
				StoreDicomFileName();
			}
			else if (m_DialogType == CALIBRATE_TOOL)
			{
				StoreCalibrationSpacing();
			}

			this->EndModal(wxID_RETURN_NEXT);
			break;
		case ID_REJECT_BUTTON:
			FindAndRemove(m_Input,TAG_RX);
			this->EndModal(wxID_RETURN_PREV);
			break;
		default:
			mafEventMacro(*e);
			}
		}
	}

	//----------------------------------------------------------------------------
	void iGuiDialogShowRx::CreateGui()
		//----------------------------------------------------------------------------
	{
		m_Rwi3DView = new mafRWI(this,ONE_LAYER,false);
		m_Rwi3DView->SetListener(this);
		m_Rwi3DView->CameraSet(CAMERA_OS_Y);
		m_Rwi3DView->SetSize(0,0,640,640);
		m_Rwi3DView->m_RenderWindow->SetDesiredUpdateRate(0.0001f);
		m_Rwi3DView->Show(true);
		m_Rwi3DView->m_RwiBase->SetMouse(m_Mouse);

		wxPoint p = wxDefaultPosition;

		m_LateralGui = new mafGUI(this);
		m_LateralGui->Reparent(this);

		if (m_DialogType == ADD_POSSIBILITY_TO_OPEN_RX)
		{
			m_LateralGui->Label("");
			m_LateralGui->Label("Choose input RX");
			m_LateralGui->FileOpen(ID_OPEN_RX_FILE,"",&m_FileRX);
			
		}

		if (m_DialogType == SHOW_RX_ONLY || m_DialogType == ADD_POSSIBILITY_TO_OPEN_RX)
		{
			m_LateralGui->Label("");
			m_LateralGui->String(ID_PATIENT_NAME,_("name"),&m_PatientName);
      m_LateralGui->Double(ID_PATIENT_HEIGHT,_("height (cm)"),&m_PatientHeight,0.0);
      m_LateralGui->Double(ID_PATIENT_WEIGHT,_("weight (kg)"),&m_PatientWeight,0.0);
			m_LateralGui->String(ID_DIAGNOSIS,_("diagnosis"),&m_Diagnosis);
			wxString sideChoices[2] = {_("right"),_("left")};
			m_LateralGui->Combo(ID_SIDE,_("side"),&m_Side,2,sideChoices);
			wxString jointChoices[3] = {_("knee"),_("hip"),_("shoulder")};
			m_LateralGui->Combo(ID_JOINT,_("joint"),&m_Joint,3,jointChoices);
			m_LateralGui->Label("");
			m_LateralGui->Label(_("Spacing: "),&m_SpacingLabel);
		}

		if (m_DialogType == CALIBRATE_TOOL)
		{
			m_LateralGui->Label("");

			wxString shapes[2] = {"Line" , "Circle"};
			m_LateralGui->Label("Pick and drag the Calibration");
			m_LateralGui->Label("Shape to fit the Repere size.");
			m_LateralGui->Label("When the Fit is ok, press the");
			m_LateralGui->Label("Calibrate Button to update ");
			m_LateralGui->Label("the spacing to be used for ");
			m_LateralGui->Label("the next Measurement step");
			m_LateralGui->Label("");
			m_LateralGui->Label("Enter Repere size (mm)");
			m_LateralGui->Integer(ID_REPERE_SIZE, "", &m_RepereSize, MININT, MAXINT, "Size in mm of the reference object to be fitted");
			m_LateralGui->Label("");
			m_LateralGui->Label("Choose the Calibration Shape");
			m_LateralGui->Combo(ID_CALIBRATOR_SHAPE, "", &m_CalibrationShape, 2, shapes , "Choose the calibration shape" );  
			m_LateralGui->Button(ID_REMOVE_CALIBRATOR_SHAPE_BUTTON, "Remove", "" , "Remove the calibration shape");

			m_LateralGui->Label("");

			m_LateralGui->Label("New spacing Preview(mm)");  
			m_LateralGui->Double(ID_NEW_SPACING, "", &m_RXSpacingAfterCalibrationGUIPreview, MINDOUBLE, MAXDOUBLE, -1, "New spacing that will be applied by calibration");
			
			m_LateralGui->Label("");
			m_LateralGui->Label("Perform the Calibration");
			m_LateralGui->Label("and update the ");
			m_LateralGui->Label("Calibration Spacing");
			m_LateralGui->Button(ID_CALIBRATE_BUTTON, "Calibrate", "" , "Perform the calibration");
			m_LateralGui->Label("");
			m_LateralGui->Label("This Spacing will be applied to");
			m_LateralGui->Label("perform Measures in next step");

			m_SpacingLabel = m_Spacing[0];
			m_SpacingLabel << " , ";
			m_SpacingLabel << m_Spacing[2];

			m_LateralGui->Label("Xsp,Zsp: ",&m_SpacingLabel);

			EnableCustomPickerInteractor();

		}

		if (m_DialogType == MEASURE_TOOL)
		{
			m_LateralGui->Label("");
			m_LateralGui->Label("Pick and drag to measure:");
			m_LateralGui->Label("The correct measure tool");
			m_LateralGui->Label("will be created based");	
			m_LateralGui->Label("on Joint and Measure");
			m_LateralGui->Label("");
			m_LateralGui->Label(_("Spacing: "),&m_SpacingLabel);
			m_LateralGui->Label("from calibration step");
			m_LateralGui->Label("will be used");
			m_LateralGui->Label("");

			InitializeMeasures(m_MeasuresDataStructure);

			vector<pair<string , vector<vector<string> > > >::iterator vectorPairIterator;

			for (vectorPairIterator = m_MeasuresDataStructure.begin() ; vectorPairIterator != m_MeasuresDataStructure.end(); ++vectorPairIterator ) 
			{
				wxString joint = (*vectorPairIterator).first.c_str();

				m_JointNames.Add(joint);
			
				vector<vector<string> > grid = (*vectorPairIterator).second;
				vector<vector<string> >::iterator iter;

				vector< vector<string> >::iterator row_it = grid.begin();
				vector< vector<string> >::iterator row_end = grid.end();

				for ( ; row_it != row_end; ++row_it ) {

					m_MeasureJoints.push_back(joint.c_str());
					m_MeasureNames.push_back((*row_it)[0].c_str());
					m_MeasureTypes.push_back((*row_it)[1].c_str());
					m_MeasureValues.push_back((*row_it)[2].c_str());
				
				}

			}

			m_LateralGui->Label("Joint Name");
			m_LateralGui->Combo(ID_JOINT_NAME, "", &m_JointNameId, 1, m_JointNames.GetStringArray() , "Name of the joint" );  
		
			m_LateralGui->Label("");
			m_LateralGui->Label("Measure Name");

			m_LateralGui->Combo(ID_MEASURE_NAME_COMBO, "", &m_MeasureNameId, m_MeasureNames.size(), m_MeasureNames.GetStringArray() , "Name of the measure" );  
			m_LateralGui->Label("");

			m_LateralGui->Label("Measure Value");  
			m_LateralGui->Double(ID_MEASURE_VALUE, "", &m_MeasureValue, MINDOUBLE, MAXDOUBLE, -1, "Value of the current measure");

			m_LateralGui->Label("");
			m_LateralGui->Label("Add performed measures to");
			m_LateralGui->Label("Patient Case Report");
			m_LateralGui->Button(ID_ADD_TO_REPORT , "Add to Report");  

			EnableCustomPickerInteractor();

			CreateMeasures();

			assert(m_MeasuresVector[0]);
			m_CurrentMeasure = m_MeasuresVector[0];
		}

		m_LateralGui->FitGui();
		m_LateralGui->Update();

		wxBoxSizer *topSizer = new wxBoxSizer(wxHORIZONTAL);
		wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);

		mafGUIButton *b_accept = new mafGUIButton(this, ID_ACCEPT_BUTTON,_("accept"), p, wxSize(100,20));
		b_accept->SetValidator(mafGUIValidator(this,ID_ACCEPT_BUTTON,b_accept));
		mafGUIButton *b_cancel = new mafGUIButton(this, ID_CANCEL,_("cancel"), p, wxSize(100,20));
		b_cancel->SetValidator(mafGUIValidator(this,ID_CANCEL,b_cancel));
		mafGUIButton *b_reject = new mafGUIButton(this, ID_REJECT_BUTTON,_("reject"), p, wxSize(100,20));
		b_reject->SetValidator(mafGUIValidator(this,ID_REJECT_BUTTON,b_reject));

		topSizer->Add(m_Rwi3DView->m_RwiBase,0);
		topSizer->Add(m_LateralGui,0);

		buttonSizer->Add(b_reject,0);
		buttonSizer->Add(b_cancel,0);
		buttonSizer->Add(b_accept,0);

		m_GuiSizer->Add(topSizer,0,wxCENTRE,5);
		m_GuiSizer->Add(buttonSizer,0,wxCENTRE,5);
	}
	//----------------------------------------------------------------------------
	void iGuiDialogShowRx::ShowRX()
	//----------------------------------------------------------------------------
	{
		mafVMERoot *root = m_Input;

		mafVMEImage *RX = NULL;
		mafNodeIterator *iter = root->NewIterator();
		for (mafNode *node = iter->GetFirstNode(); node; node = iter->GetNextNode())
		{
			if (node->GetTagArray()->IsTagPresent(TAG_RX))
			{
				RX = mafVMEImage::SafeDownCast(node);
				break;
			}
		}

		iter->Delete();

		if (!RX)
		{
			return;
		}

		vtkImageData *image = vtkImageData::SafeDownCast(RX->GetOutput()->GetVTKData());
		image->Update();

		image->GetSpacing(m_Spacing);

		vtkMAFSmartPointer<vtkImageData> imageCopy;
		imageCopy->DeepCopy(image);

		if (m_DialogType == MEASURE_TOOL || m_DialogType == SHOW_RX_ONLY )
		{
			if (m_RXImageCalibrationSpacing == UNDEFINED_CALIBRATION_SPACING)
			{
				// se lo spacing di calibrazione non e' definito usa il default dal dicom
				m_RXImageCalibrationSpacing = m_Spacing[0];
			}

			imageCopy->SetSpacing(m_RXImageCalibrationSpacing, m_RXImageCalibrationSpacing , 1);		
			imageCopy->Modified();

			m_SpacingLabel = m_RXImageCalibrationSpacing;
			m_SpacingLabel << " , ";
			m_SpacingLabel << m_RXImageCalibrationSpacing;

			mafString storeCalibrationSpacing = m_RXImageCalibrationSpacing;
			RX->GetTagArray()->SetTag("TAG_RX_CALIBRATION_SPACING" , storeCalibrationSpacing.GetCStr());
			m_LateralGui->Update();

		}
		
		double origin[3],bounds[6];
		imageCopy->GetOrigin(origin);
		imageCopy->GetBounds(bounds);
		double diffx,diffy;
		diffx=bounds[1]-bounds[0];
		diffy=bounds[3]-bounds[2];

		m_SliceTexture->InterpolateOn();
		m_SliceTexture->SetQualityTo32Bit();
		m_SliceTexture->RepeatOff();

		m_SlicePlane->SetOrigin(origin);
		m_SlicePlane->SetPoint1(origin[0]+diffx,origin[1],origin[2]);
		m_SlicePlane->SetPoint2(origin[0],origin[1],origin[2]-diffy);

		m_SliceMapper->SetInput(m_SlicePlane->GetOutput());
		m_SliceMapper->ScalarVisibilityOff();

		m_SliceActor->SetMapper(m_SliceMapper);
		m_SliceActor->SetTexture(m_SliceTexture); 
		m_SliceActor->VisibilityOn();

		m_Rwi3DView->m_RenFront->AddActor(m_SliceActor);

		m_SliceTexture->SetInput(imageCopy);

		m_SliceTexture->Modified();

		double range[2];
		image->GetScalarRange(range);
		m_SliceLookupTable->SetTableRange(range);
		m_SliceLookupTable->SetWindow(range[1] - range[0]);
		m_SliceLookupTable->SetLevel((range[1] + range[0]) / 2.0);
		m_SliceLookupTable->Build();

		m_SliceTexture->MapColorScalarsThroughLookupTableOn();
		m_SliceTexture->SetLookupTable((vtkLookupTable *)m_SliceLookupTable);

		m_Rwi3DView->CameraReset();
		m_Rwi3DView->CameraUpdate();

	}
	//----------------------------------------------------------------------------
	void iGuiDialogShowRx::ReadDICOM()
		//----------------------------------------------------------------------------
	{
		wxBusyInfo wait_info(_("Reading file! Please Wait!"));

		DcmFileFormat dicomImg;

		DJDecoderRegistration::registerCodecs(); // register JPEG codecs
		DcmRLEDecoderRegistration ::registerCodecs(OFFalse, OFFalse,OFFalse); // register RLE codecs

		OFCondition status = dicomImg.loadFile(m_FileRX);//load data into offis structure

		DcmDataset *dicomDataset = dicomImg.getDataset();//obtain dataset information from dicom file (loaded into memory)

		// decompress data set if compressed
		OFCondition error = dicomDataset->chooseRepresentation(EXS_LittleEndianExplicit, NULL);

		DJDecoderRegistration::cleanup(); // deregister JPEG codecs
		DcmRLEDecoderRegistration::cleanup();

		if (!error.good())
		{
			wxLogMessage(wxString::Format("Error decoding the image <%s>",m_FileRX));   
			return;
		}

		long int val_long;
		dicomDataset->findAndGetLongInt(DCM_Columns, val_long); 

		// width
		int dcmColumns = val_long;

		dicomDataset->findAndGetLongInt(DCM_Rows, val_long);

		// height
		int dcmRows = val_long;

		double dcmImageOrientationPatient[6] = {0.0,0.0,0.0,0.0,0.0,0.0};
		double dcmImagePositionPatient[3] = {0.0,0.0,0.0};

		dicomDataset->findAndGetFloat64(DCM_ImagePositionPatient,dcmImagePositionPatient[0],0);
		dicomDataset->findAndGetFloat64(DCM_ImagePositionPatient,dcmImagePositionPatient[1],1);
		dicomDataset->findAndGetFloat64(DCM_ImagePositionPatient,dcmImagePositionPatient[2],2);

		dicomDataset->findAndGetFloat64(DCM_ImageOrientationPatient,dcmImageOrientationPatient[0],0);
		dicomDataset->findAndGetFloat64(DCM_ImageOrientationPatient,dcmImageOrientationPatient[1],1);
		dicomDataset->findAndGetFloat64(DCM_ImageOrientationPatient,dcmImageOrientationPatient[2],2);
		dicomDataset->findAndGetFloat64(DCM_ImageOrientationPatient,dcmImageOrientationPatient[3],3);
		dicomDataset->findAndGetFloat64(DCM_ImageOrientationPatient,dcmImageOrientationPatient[4],4);
		dicomDataset->findAndGetFloat64(DCM_ImageOrientationPatient,dcmImageOrientationPatient[5],5);

		double dcmPixelSpacing[3];
		dcmPixelSpacing[2] = 1;
		if(dicomDataset->findAndGetFloat64(DCM_PixelSpacing,dcmPixelSpacing[0],0).bad())
		{
			//Unable to get element: DCM_PixelSpacing[0];
			dcmPixelSpacing[0] = 1.0;// for RGB??
		} 
		if(dicomDataset->findAndGetFloat64(DCM_PixelSpacing,dcmPixelSpacing[1],1).bad())
		{
			//Unable to get element: DCM_PixelSpacing[0];
			dcmPixelSpacing[1] = 1.0;// for RGB??
		} 
		else
		{
			dcmPixelSpacing[2] = dcmPixelSpacing[1];//to visualize it is used plane XZ
		}

		double dcmRescaleSlope;
		if(dicomDataset->findAndGetFloat64(DCM_RescaleSlope,dcmRescaleSlope).bad())
		{
			//Unable to get element: DCM_RescaleSlope[0];
			dcmRescaleSlope = 1;
		} 

		long dcmHighBit; 
		double dcmRescaleIntercept;
		if(dicomDataset->findAndGetLongInt(DCM_HighBit,dcmHighBit).bad())
		{
			//Unable to get element: DCM_RescaleIntercept[0];
			dcmHighBit = 0;
		} 

		if(dicomDataset->findAndGetFloat64(DCM_RescaleIntercept,dcmRescaleIntercept).bad())
		{
			//Unable to get element: DCM_RescaleIntercept[0];
			dcmRescaleIntercept = 0;
		}

		vtkMAFSmartPointer<vtkImageData> dicomSliceVTKImageData;
		dicomSliceVTKImageData->SetDimensions(dcmColumns, dcmRows,1);
		dicomSliceVTKImageData->SetWholeExtent(0,dcmColumns-1,0,dcmRows-1,0,0);
		dicomSliceVTKImageData->SetUpdateExtent(0,dcmColumns-1,0,dcmRows-1,0,0);
		dicomSliceVTKImageData->SetExtent(dicomSliceVTKImageData->GetUpdateExtent());
		dicomSliceVTKImageData->SetNumberOfScalarComponents(1);
		dicomSliceVTKImageData->SetSpacing(dcmPixelSpacing);
		dicomSliceVTKImageData->Update();

		m_SpacingLabel = dcmPixelSpacing[0];
		m_SpacingLabel << " , ";
		m_SpacingLabel << dcmPixelSpacing[2];

		long dcmPixelRepresentation;
		dicomDataset->findAndGetLongInt(DCM_PixelRepresentation,dcmPixelRepresentation);
		dicomDataset->findAndGetLongInt(DCM_BitsAllocated,val_long);

		long dcmLargestImagePixelValue;
		long dcmSmallestImagePixelValue;
		dicomDataset->findAndGetLongInt(DCM_SmallestImagePixelValue, dcmSmallestImagePixelValue);
		dicomDataset->findAndGetLongInt(DCM_LargestImagePixelValue, dcmLargestImagePixelValue);

		if(val_long==16 && dcmPixelRepresentation == 0 )
		{
			if(dcmSmallestImagePixelValue*dcmRescaleSlope+dcmRescaleIntercept >= VTK_UNSIGNED_SHORT_MIN && dcmLargestImagePixelValue*dcmRescaleSlope+dcmRescaleIntercept <= VTK_UNSIGNED_SHORT_MAX)
			{
				dicomSliceVTKImageData->SetScalarType(VTK_UNSIGNED_SHORT);
			}
			else if (dcmSmallestImagePixelValue*dcmRescaleSlope+dcmRescaleIntercept >= VTK_SHORT_MIN && dcmLargestImagePixelValue*dcmRescaleSlope+dcmRescaleIntercept <= VTK_SHORT_MAX)
			{
				dicomSliceVTKImageData->SetScalarType(VTK_SHORT);
			}
			else
			{
				wxLogMessage(wxString::Format("Inconsistent scalar values. Can not import file <%s>",m_FileRX));
				return;
			}
		}
		else if(val_long==16 && dcmPixelRepresentation == 1)
		{
			if (dcmSmallestImagePixelValue*dcmRescaleSlope+dcmRescaleIntercept >= VTK_SHORT_MIN && dcmLargestImagePixelValue*dcmRescaleSlope+dcmRescaleIntercept <= VTK_SHORT_MAX)
			{
				dicomSliceVTKImageData->SetScalarType(VTK_SHORT);
			}
			else
			{
				wxLogMessage(wxString::Format("Inconsistent scalar values. Can not import file <%s>",m_FileRX));
				return;
			}
		}
		else if(val_long==8 && dcmPixelRepresentation == 0)
		{
			dicomSliceVTKImageData->SetScalarType(VTK_UNSIGNED_SHORT);
		}
		else if(val_long==8)
		{
			dicomSliceVTKImageData->SetScalarType(VTK_CHAR);
		}

		dicomSliceVTKImageData->AllocateScalars();
		dicomSliceVTKImageData->GetPointData()->GetScalars()->SetName("Scalars");
		dicomSliceVTKImageData->Update();

		const Uint16 *dicom_buf_short = NULL; 
		const Uint8* dicom_buf_char = NULL; 
		if (val_long==16) 
		{ 
			dicomDataset->findAndGetUint16Array(DCM_PixelData, dicom_buf_short); 
			int counter=0; 
			for(int y=0;y<dcmRows;y++) 
			{ 
				for(int x=0;x<dcmColumns;x++) 
				{ 
					dicomSliceVTKImageData->GetPointData()->GetScalars()->SetTuple1(counter, dicom_buf_short[dcmColumns*y+x]); 
					counter++; 
				} 
			} 
		} 
		else 
		{ 
			dicomDataset->findAndGetUint8Array(DCM_PixelData, dicom_buf_char); 
			int counter=0; 
			for(int y=0;y<dcmRows;y++) 
			{ 
				for(int x=0;x<dcmColumns;x++) 
				{ 
					dicomSliceVTKImageData->GetPointData()->GetScalars()->SetTuple1(counter, dicom_buf_char[dcmColumns*y+x]); 
					counter++; 
				} 
			} 
		} 

		dicomSliceVTKImageData->Update();

		if (dcmRescaleSlope != 1 || dcmRescaleIntercept != 0)
		{
			int scalarType = dicomSliceVTKImageData->GetScalarType();

			if (dicomSliceVTKImageData->GetScalarType() == VTK_UNSIGNED_SHORT)
			{
				vtkUnsignedShortArray *scalars=vtkUnsignedShortArray::SafeDownCast(dicomSliceVTKImageData->GetPointData()->GetScalars());
				for(int indexScalar=0;indexScalar<dicomSliceVTKImageData->GetPointData()->GetScalars()->GetNumberOfTuples();indexScalar++)
				{
					scalars->SetTuple1(indexScalar,scalars->GetTuple1(indexScalar)*dcmRescaleSlope+dcmRescaleIntercept);//modify scalars using slope and intercept
				}
			}
			else if (dicomSliceVTKImageData->GetScalarType() == VTK_SHORT)
			{
				vtkShortArray *scalars=vtkShortArray::SafeDownCast(dicomSliceVTKImageData->GetPointData()->GetScalars());
				for(int indexScalar=0;indexScalar<dicomSliceVTKImageData->GetPointData()->GetScalars()->GetNumberOfTuples();indexScalar++)
				{
					scalars->SetTuple1(indexScalar,scalars->GetTuple1(indexScalar)*dcmRescaleSlope+dcmRescaleIntercept);//modify scalars using slope and intercept
				}
			}
			else if (dicomSliceVTKImageData->GetScalarType() == VTK_CHAR)
			{
				vtkCharArray *scalars=vtkCharArray::SafeDownCast(dicomSliceVTKImageData->GetPointData()->GetScalars());
				for(int indexScalar=0;indexScalar<dicomSliceVTKImageData->GetPointData()->GetScalars()->GetNumberOfTuples();indexScalar++)
				{
					scalars->SetTuple1(indexScalar,scalars->GetTuple1(indexScalar)*dcmRescaleSlope+dcmRescaleIntercept);//modify scalars using slope and intercept
				}
			}

			dicomSliceVTKImageData->Update();
		}


		const char *dcmModality = "?";
		dicomDataset->findAndGetString(DCM_Modality,dcmModality);

		const char *dcmPatientPosition = "?";
		dicomDataset->findAndGetString(DCM_PatientPosition,dcmPatientPosition);

		const char *dcmStudyInstanceUID = "?";
		dicomDataset->findAndGetString(DCM_StudyInstanceUID,dcmStudyInstanceUID);

		const char *dcmSeriesInstanceUID = "?";
		dicomDataset->findAndGetString(DCM_SeriesInstanceUID,dcmSeriesInstanceUID);

		const char *patientName = "?";
		dicomDataset->findAndGetString(DCM_PatientsName,patientName);
		m_PatientName = patientName;

		m_LateralGui->Update();

		if(dicomDataset->findAndGetFloat64(DCM_ImagePositionPatient,dcmImagePositionPatient[2]).bad())
		{
			std::ostringstream stringStream;
			stringStream << "Cannot read dicom tag DCM_ImagePositionPatient. Exiting"<< std::endl;          
			mafLogMessage(stringStream.str().c_str());
			return;
		} 
		else
		{
			dicomDataset->findAndGetFloat64(DCM_ImagePositionPatient,dcmImagePositionPatient[0],0);
			dicomDataset->findAndGetFloat64(DCM_ImagePositionPatient,dcmImagePositionPatient[1],1);
			dicomDataset->findAndGetFloat64(DCM_ImagePositionPatient,dcmImagePositionPatient[2],2);
		}

		dicomSliceVTKImageData->SetOrigin(dcmImagePositionPatient);
		dicomSliceVTKImageData->Update();

    const char *dcmPatientSex = "f";
    dicomDataset->findAndGetString(DCM_PatientsSex,dcmPatientSex);

    m_PatientSex = dcmPatientSex;

    const char *dcmPatientBirthdate = "?";
    dicomDataset->findAndGetString(DCM_PatientsBirthDate,dcmPatientBirthdate);

    m_PatientBirthdate = dcmPatientBirthdate;

		FindAndRemove(m_Input,TAG_RX);

		mafVMERoot *root = m_Input;

		mafNEW(m_OutputImage);
		m_OutputImage->SetName("RX");
		mafSmartPointer<mafTransform> tr;
		// tr->RotateX(90,TRUE);1
		// m_OutputImage->SetAbsMatrix(tr->GetMatrix());
		m_OutputImage->SetData(dicomSliceVTKImageData,0.0);
		m_OutputImage->GetTagArray()->SetTag(TAG_RX,"");
		m_OutputImage->ReparentTo(root);

		m_OutputImage->Update();

	}
	//----------------------------------------------------------------------------
	void iGuiDialogShowRx::CreateSlicePipeline()
		//----------------------------------------------------------------------------
	{
		vtkNEW(m_SliceLookupTable);
		vtkNEW(m_SliceTexture);
		m_SliceTexture->InterpolateOn();
		vtkNEW(m_SlicePlane);
		vtkNEW(m_SliceMapper);
		m_SliceMapper->SetInput(m_SlicePlane->GetOutput());
		vtkNEW(m_SliceActor);
		m_SliceActor->SetMapper(m_SliceMapper);
		m_SliceActor->SetTexture(m_SliceTexture); 
		m_SliceActor->VisibilityOff();

		m_Rwi3DView->m_RenFront->AddActor(m_SliceActor);
		m_Rwi3DView->m_Camera->ParallelProjectionOn();
	}
	//----------------------------------------------------------------------------
	void iGuiDialogShowRx::SetPatientName(mafString name)
		//----------------------------------------------------------------------------
	{
		m_PatientName = name;
		if (m_LateralGui)
		{
			m_LateralGui->Update();
		}
	}
	//----------------------------------------------------------------------------
	void iGuiDialogShowRx::SetJoint( int joint )
		//----------------------------------------------------------------------------
	{
		m_Joint = joint;
		if (m_LateralGui)
		{
			m_LateralGui->Update();
		}
	}
	//----------------------------------------------------------------------------
	void iGuiDialogShowRx::SetSide( int side )
		//----------------------------------------------------------------------------
	{
		m_Side = side;
		if (m_LateralGui)
		{
			m_LateralGui->Update();
		}
	}
	//----------------------------------------------------------------------------
	void iGuiDialogShowRx::SetDiagnosis( mafString diagnosis )
		//----------------------------------------------------------------------------
	{
		m_Diagnosis = diagnosis;
		if (m_LateralGui)
		{
			m_LateralGui->Update();
		}
	}
  //----------------------------------------------------------------------------
	void iGuiDialogShowRx::EnableCustomPickerInteractor()
  //----------------------------------------------------------------------------
	{
		m_CalibrationEnable = 1;
		EnableCalibrationGui(true);

		mafNEW(m_PickerInteractor);
		m_PickerInteractor->SetListener(this);
		m_Mouse->AddObserver(m_PickerInteractor, MCH_INPUT);
	}

	//----------------------------------------------------------------------------
	void iGuiDialogShowRx::CreateCalibrationLinePipeline()
		//----------------------------------------------------------------------------
	{
		vtkNEW(m_CalibrationLine);
		vtkNEW(m_CalibrationLineTubeFilter);
		vtkNEW(m_CalibrationLineMapper);
		m_CalibrationLineTubeFilter->SetInput(m_CalibrationLine->GetOutput());
		m_CalibrationLineTubeFilter->SetRadius(5);
		m_CalibrationLineMapper->SetInput(m_CalibrationLineTubeFilter->GetOutput());
		vtkNEW(m_CalibrationLineActor);
		m_CalibrationLineActor->SetMapper(m_CalibrationLineMapper);
		m_CalibrationLineActor->VisibilityOff();

		m_Rwi3DView->m_AlwaysVisibleRenderer->AddActor(m_CalibrationLineActor);
	}

	//----------------------------------------------------------------------------
	void iGuiDialogShowRx::CreateCalibrationCirclePipeline()
	//----------------------------------------------------------------------------
	{
		vtkNEW(m_CalibrationCircleSphereSource);
		m_CalibrationCircleSphereSource->SetPhiResolution(50);
		vtkNEW(m_CalibrationCircleCuttingPlane);
		vtkNEW(m_CalibrationCircleCutter);
		m_CalibrationCircleCutter->SetInput(m_CalibrationCircleSphereSource->GetOutput());
		m_CalibrationCircleCutter->SetCutFunction(m_CalibrationCircleCuttingPlane);
		vtkNEW(m_CalibrationCircleTubeFilter);
		m_CalibrationCircleTubeFilter->SetInput(m_CalibrationCircleCutter->GetOutput());
		m_CalibrationCircleTubeFilter->SetRadius(5);
		vtkNEW(m_CalibrationCirclePDM);
		m_CalibrationCirclePDM->SetInput(m_CalibrationCircleTubeFilter->GetOutput());
		vtkNEW(m_CalibrationCircleActor);
		m_CalibrationCircleActor->SetMapper(m_CalibrationCirclePDM);
		m_CalibrationCircleActor->VisibilityOff();
		m_Rwi3DView->m_AlwaysVisibleRenderer->AddActor(m_CalibrationCircleActor);
	}

	void iGuiDialogShowRx::EnableCalibrationGui( bool enable )
	{
		m_LateralGui->Enable(ID_CALIBRATOR_SHAPE, enable);
		m_LateralGui->Enable(ID_REMOVE_CALIBRATOR_SHAPE_BUTTON, enable);
		m_LateralGui->Enable(ID_NEW_SPACING, enable);
		m_LateralGui->Enable(ID_REPERE_SIZE, enable);
		m_LateralGui->Enable(ID_CALIBRATE_BUTTON, enable);
	}

	void iGuiDialogShowRx::OnIdRemoveCalibrationShapeButton()
	{
		m_CalibrationCircleActor->VisibilityOff();
		m_CalibrationLineActor->VisibilityOff();
		m_P1LinePicked = false;
		m_P2LinePicked = false;
		m_P1Grabbed = false;
		m_P2Grabbed = false;
		m_CalibrationCircleSphereSource->SetRadius(0);
		m_Rwi3DView->CameraUpdate();
	}

	void iGuiDialogShowRx::OnIdCalibratorShape()
	{
		OnIdRemoveCalibrationShapeButton();
	}

	void iGuiDialogShowRx::UpdateSpacingAfterCalibration()
	{
		m_RXSpacingAfterCalibrationGUIPreview = ((double) m_RepereSize * m_Spacing[0]) / ((double) m_CalibratorSize);
	}

	void iGuiDialogShowRx::OnMouseMoveCircle( double * currentPosition )
	{
		bool originPicked = m_P1LinePicked;

		if (m_Dragging == true)
		{
			if (originPicked)
			{
				double dOrigin_PickedPoint = sqrt(vtkMath::Distance2BetweenPoints(m_CalibrationCircleSphereSource->GetCenter() , currentPosition));

				bool outOfCircle = dOrigin_PickedPoint > m_CalibrationCircleSphereSource->GetRadius();

				if (outOfCircle || m_ResizingCircle) // drag the radius
				{
					std::ostringstream stringStream;
					stringStream << "p1 " << currentPosition[0] << " " << currentPosition[1] << " " << currentPosition[2] << std::endl;          
					mafLogMessage(stringStream.str().c_str());

					mafLogMessage("mouse move with dragging");

					double dicomSlicePlaneNormal[3] = {0,1,0};

					m_CalibrationCircleCuttingPlane->SetNormal(m_SlicePlane->GetNormal());
					m_CalibrationCircleCuttingPlane->SetOrigin(m_SlicePlane->GetOrigin());

					m_CalibrationCircleSphereSource->SetRadius(dOrigin_PickedPoint);

					m_Rwi3DView->CameraUpdate();						
				}
				else // drag the center
				{
					// pick the new origin
					m_P1[0] = currentPosition[0];
					m_P1[1] = currentPosition[1];
					m_P1[2] = currentPosition[2];

					m_CalibrationCircleSphereSource->SetCenter(currentPosition);
					m_Rwi3DView->CameraUpdate();
				}

			}

		}
		else
		{
			mafLogMessage("mouse move without dragging");
		}

		m_CalibratorSize = 2 * m_CalibrationCircleSphereSource->GetRadius();
		UpdateSpacingAfterCalibration();
		this->m_LateralGui->Update();
	}

	void iGuiDialogShowRx::OnMouseMoveLine( double * currentPosition )
	{
		m_CalibrationLineActor->VisibilityOn();

		if (m_Dragging == true) // drag P2
		{
			if (m_P1LinePicked == true && m_P2LinePicked == false) // line does not exists
			{
				std::ostringstream stringStream;
				stringStream << "p1 " << currentPosition[0] << " " << currentPosition[1] << " " << currentPosition[2] << std::endl;          
				mafLogMessage(stringStream.str().c_str());

				mafLogMessage("mouse move with dragging");
				m_P2[0] = currentPosition[0];
				m_P2[1] = currentPosition[1];
				m_P2[2] = currentPosition[2];

				m_CalibrationLine->SetPoint2(m_P2);
				m_CalibrationLineTubeFilter->Update();

				m_Rwi3DView->CameraUpdate();						
				m_P2LinePicked = true;
			}
			else if (m_P1LinePicked == true && m_P2LinePicked == true) // line exists already
			{
				if (m_P1Grabbed)
				{
					m_P1[0] = currentPosition[0];
					m_P1[1] = currentPosition[1];
					m_P1[2] = currentPosition[2];
					m_CalibrationLine->SetPoint1(m_P1);
				}
				else if (m_P2Grabbed)
				{
					m_P2[0] = currentPosition[0];
					m_P2[1] = currentPosition[1];
					m_P2[2] = currentPosition[2];
					m_CalibrationLine->SetPoint2(m_P2);
				}

				m_Rwi3DView->CameraUpdate();						
			}
		}
		else
		{
			mafLogMessage("mouse move without dragging");
		}

		m_CalibratorSize = sqrt(vtkMath::Distance2BetweenPoints(m_P1, m_P2));
		UpdateSpacingAfterCalibration();
		this->m_LateralGui->Update();
	}

	void iGuiDialogShowRx::OnMouseDownLine( double * pos )
	{
		{
			if (m_P1LinePicked == false)
			{
				m_P1[0] = pos[0];
				m_P1[1] = pos[1];
				m_P1[2] = pos[2];
				m_P1LinePicked = true;
				m_P2Grabbed = true;
				m_CalibrationLine->SetPoint1(m_P1);
				m_CalibrationLine->SetPoint2(m_P1);
				m_CalibrationLineActor->VisibilityOn();
				m_Rwi3DView->CameraUpdate();
			}
			else if (m_P1LinePicked == true && m_P2LinePicked == false)
			{
				m_P2[0] = pos[0];
				m_P2[1] = pos[1];
				m_P2[2] = pos[2];
				m_P2LinePicked = true;
				m_P2Grabbed = true;
				m_CalibrationLine->SetPoint2(m_P1);
				m_Rwi3DView->CameraUpdate();
			}
			else if (m_P1LinePicked == true && m_P2LinePicked == true)
			{
				double currentPoint[3] = {pos[0], pos[1], pos[2]};

				double dP1_CP = vtkMath::Distance2BetweenPoints(m_P1 , currentPoint);
				double dP2_CP = vtkMath::Distance2BetweenPoints(m_P2 , currentPoint);

				if (dP1_CP >= dP2_CP)
				{
					m_P1Grabbed = false;
					m_P2Grabbed = true;

					m_P2[0] = pos[0];
					m_P2[1] = pos[1];
					m_P2[2] = pos[2];

					m_CalibrationLine->SetPoint2(m_P2);
					m_Rwi3DView->CameraUpdate();
				}
				else
				{
					m_P1Grabbed = true;
					m_P2Grabbed = false;

					m_P1[0] = pos[0];
					m_P1[1] = pos[1];
					m_P1[2] = pos[2];

					m_CalibrationLine->SetPoint1(m_P1);
					m_Rwi3DView->CameraUpdate();
				}
			}
		}
	}

	void iGuiDialogShowRx::OnMouseDownCircle( double * pos )
	{
		{
			m_CalibrationCircleActor->VisibilityOn();
			if (m_P1LinePicked == false)
			{
				m_P1[0] = pos[0];
				m_P1[1] = pos[1];
				m_P1[2] = pos[2];
				m_P1LinePicked = true;
				m_CalibrationCircleSphereSource->SetCenter(m_P1);
				m_Rwi3DView->CameraUpdate();
				m_Dragging = true;

			}
			else if (m_P1LinePicked == true)
			{
				// circle exists
				double currentPickedPoint[3] = {pos[0], pos[1], pos[2]};

				double dOrigin_PickedPoint = sqrt(vtkMath::Distance2BetweenPoints(m_CalibrationCircleSphereSource->GetCenter() , currentPickedPoint));

				bool outOfCircle = dOrigin_PickedPoint > 0.9 * m_CalibrationCircleSphereSource->GetRadius();

				if (outOfCircle)
				{
					m_CalibrationCircleSphereSource->SetRadius(dOrigin_PickedPoint);
					m_Rwi3DView->CameraUpdate();					
					m_ResizingCircle = true;
				}
				else
				{	
					// change the circle center
					m_P1[0] = pos[0];
					m_P1[1] = pos[1];
					m_P1[2] = pos[2];

					m_CalibrationCircleSphereSource->SetCenter(m_P1);
					m_Rwi3DView->CameraUpdate();
					m_ResizingCircle = false;
				}

				m_Dragging = true;
			}
		}
	}

	void iGuiDialogShowRx::CreateMeasures()
	{
		for (int i = 0; i < m_MeasureNames.size(); i++)
		{
			if (m_MeasureTypes[i] == "POINT_DIST")
			{
				DistanceBetweenPointsMeasure* distanceBetweenPointsMeasure = new DistanceBetweenPointsMeasure();
				distanceBetweenPointsMeasure->SetRWI	(m_Rwi3DView);
				distanceBetweenPointsMeasure->CreatePipe();
				m_MeasuresVector.push_back(distanceBetweenPointsMeasure);
			}
			else if (m_MeasureTypes[i] == "PARALLEL_DIST")
			{
				DistanceBetweenLinesMeasure* distanceBetweenLinesMeasure = new DistanceBetweenLinesMeasure();
				distanceBetweenLinesMeasure->SetRWI(m_Rwi3DView);
				distanceBetweenLinesMeasure->CreatePipe();
				m_MeasuresVector.push_back(distanceBetweenLinesMeasure);
			}
			else if (m_MeasureTypes[i] == "ANGLE")
			{
				AngleMeasure *angleMeasure = new AngleMeasure();
				angleMeasure->SetRWI(m_Rwi3DView);
				angleMeasure->CreatePipe();
				m_MeasuresVector.push_back(angleMeasure);
			}
		}
	}

	void iGuiDialogShowRx::StoreDicomFileName()
	{
		wxString patientDirectory = GetPatientCaseDirectory();
		wxString dicomFileName = patientDirectory;
		dicomFileName += "/";
		dicomFileName += RX_DICOM_FILENAME;

		string dicomFileABSPath = m_FileRX.GetCStr();

		ofstream myfile;
		myfile.open(dicomFileName.c_str());
		myfile << dicomFileABSPath;
		myfile.close();
	}

	void iGuiDialogShowRx::LoadDicomFileName()
	{
		wxString patientDirectory = GetPatientCaseDirectory();
		wxString dicomFileName = patientDirectory;
		dicomFileName += "/";
		dicomFileName += RX_DICOM_FILENAME;

		assert(wxFileExists(dicomFileName));

		string dicomFileABSPath;
		ifstream myfile;
		myfile.open(dicomFileName.c_str());
		getline(myfile , dicomFileABSPath);
		m_FileRX = dicomFileABSPath.c_str();
	}

	void iGuiDialogShowRx::StoreCalibrationSpacing()
	{
		wxString patientDirectory = GetPatientCaseDirectory();
		wxString RXImageCalibrationSpacing = patientDirectory;
		RXImageCalibrationSpacing += "/";
		RXImageCalibrationSpacing += RX_CALIBRATION_SPACING_FILENAME;

		string dicomFileABSPath = m_FileRX.GetCStr();

		if (m_RXImageCalibrationSpacing == UNDEFINED_CALIBRATION_SPACING)
		{
			m_RXImageCalibrationSpacing = m_Spacing[0];
		}

		ofstream myfile;
		myfile.open(RXImageCalibrationSpacing.c_str());
		myfile << m_RXImageCalibrationSpacing;
		myfile.close();
	}

	void iGuiDialogShowRx::LoadCalibrationSpacing()
	{
		wxString patientDirectory = GetPatientCaseDirectory();
		wxString RXImageCalibrationSpacingABSFileName = patientDirectory;
		RXImageCalibrationSpacingABSFileName += "/";
		RXImageCalibrationSpacingABSFileName += RX_CALIBRATION_SPACING_FILENAME;

		if (wxFileExists(RXImageCalibrationSpacingABSFileName) == false)
		{
			std::ostringstream stringStream;
			stringStream << "Calibration Spacing File: " << RXImageCalibrationSpacingABSFileName.c_str() << " not found" << endl;          
			mafLogMessage(stringStream.str().c_str());
			return;
		};

		ifstream myfile;
		myfile.open(RXImageCalibrationSpacingABSFileName.c_str());
		myfile >> m_RXImageCalibrationSpacing;
	}

	void iGuiDialogShowRx::InitializeMeasures( vector<pair<string , vector<vector<string> > > > &measures )
	{
		vector<vector<string> > hipMeasures;
		
		vector<string> hipMeasureNeckLength;
		hipMeasureNeckLength.push_back("neck length (mm)");
		hipMeasureNeckLength.push_back("POINT_DIST");
		hipMeasureNeckLength.push_back("MEASURE_VALUE_UNDEFINED");
		hipMeasures.push_back(hipMeasureNeckLength);

		vector<string> hipMeasureCCD;
		hipMeasureCCD.push_back("ccd (deg)");
		hipMeasureCCD.push_back("ANGLE");
		hipMeasureCCD.push_back("MEASURE_VALUE_UNDEFINED");
		hipMeasures.push_back(hipMeasureCCD);

		vector<string> hipMeasureFemoralCanal;
		hipMeasureFemoralCanal.push_back("femoral canal (mm)");
		hipMeasureFemoralCanal.push_back("PARALLEL_DIST");
		hipMeasureFemoralCanal.push_back("MEASURE_VALUE_UNDEFINED");
		hipMeasures.push_back(hipMeasureFemoralCanal);

		vector<string> hipMeasureFemoralHead;
		hipMeasureFemoralHead.push_back("femoral head (mm)");
		hipMeasureFemoralHead.push_back("POINT_DIST");
		hipMeasureFemoralHead.push_back("MEASURE_VALUE_UNDEFINED");
		hipMeasures.push_back(hipMeasureFemoralHead);

		vector<string> hipMeasureAnteversion;
		hipMeasureAnteversion.push_back("anteversion (deg)");
		hipMeasureAnteversion.push_back("ANGLE");
		hipMeasureAnteversion.push_back("MEASURE_VALUE_UNDEFINED");
		hipMeasures.push_back(hipMeasureAnteversion);

		pair<string, vector<vector<string> > > hipPair;
		hipPair.first = "Hip";
		hipPair.second = hipMeasures;

		measures.push_back(hipPair);
	}

	void iGuiDialogShowRx::StoreMeasures()
	{
		wxBusyInfo busy("Please Wait, adding measures to report");
		wxString tmp = GetPatientCaseDirectory();

		wxString patientDirectory = GetPatientCaseDirectory();
		wxString measuresFileName = patientDirectory;
		measuresFileName += "/";
		measuresFileName += RX_MEASURES_FILENAME;

		ofstream myfile;
		myfile.open(measuresFileName.c_str());

		for (int i=0 ; i< m_MeasureJoints.size(); i ++)
		{
      myfile << m_MeasureJoints[i] << "#" << m_MeasureNames[i] << "#" << m_MeasureTypes[i] << "#" << m_MeasureValues[i] << endl; 
		}

		myfile.close();


		wxMessageBox("Measures stored to Report");

		mafLogMessage("------------------------------------------");
		mafLogMessage("Measures stored to Report");
		mafLogMessage("");
		mafLogMessage(measuresFileName.c_str());
		mafLogMessage("------------------------------------------");

	}

	wxString iGuiDialogShowRx::GetPatientCaseDirectory()
	{
		mafString cacheDirectory;

		if (m_PatientCaseId.Equals(UNDEFINED_PATIENT_CASE_ID) == false )
		{
			// se e' stato passato l'id del PatientCase nel costruttore usa quello ...
			cacheDirectory = mafGetApplicationDirectory().c_str();
      if (m_Training)
      {
        cacheDirectory<<"/download/";
      }
      else
      {
			  cacheDirectory<<"/cache/";
      }
			cacheDirectory<<m_PatientCaseId.GetCStr();
		}
		else
		{
			// ... altrimenti prova a caricare l'id dalla cache
			iGuiDialogListCase* listCases = new iGuiDialogListCase("tmp");

      int lastCaseId;
      if (listCases->GetListCaseSize() > 0)
      {
        lastCaseId = listCases->GetLastCaseId();
      }
      else
      {
        lastCaseId = 0;
      }
			cacheDirectory = mafGetApplicationDirectory().c_str();
      if (m_Training)
      {
        cacheDirectory<<"/download/";
      }
      else
      {
        cacheDirectory<<"/cache/";
      }
			cacheDirectory<<lastCaseId;

			cppDEL(listCases);
		}

		if(!wxDirExists(cacheDirectory.GetCStr()))
		{
			// se la cartella del paziente ancora non esiste creala
			::wxMkDir(cacheDirectory.GetCStr());
		}

		assert(wxDirExists(cacheDirectory.GetCStr()));

		return cacheDirectory;
	}

	void iGuiDialogShowRx::UpdateMeasureValues()
	{
		std::ostringstream stringStream;
		stringStream << m_MeasureValue;          
		string value = stringStream.str().c_str();

		m_MeasureValues[m_MeasureNameId] = value.c_str();
	}

  void iGuiDialogShowRx::SetPatientWeight( double weight )
  {
    m_PatientWeight = weight;
    if (m_LateralGui)
    {
      m_LateralGui->Update();
    }
  }

  void iGuiDialogShowRx::SetPatientHeight(double height)
  {
    m_PatientHeight = height;
    if (m_LateralGui)
    {
      m_LateralGui->Update();
    }
  }

  void iGuiDialogShowRx::SetPatientBirthdate( mafString birthdate )
  {
    m_PatientBirthdate = birthdate;
  }

	//----------------------------------------------------
	//  Measure
	//----------------------------------------------------
	Measure::Measure()
	{
		m_Rwi3DView = NULL;
		m_Value = 0;
		m_Select = false;
	}

	Measure::~Measure()
	{

	}

	void Measure::Store()
	{
		/*
			
			fatti dare gli atributi per il caso corrente
			salvaci la misura
			nome della misura da salvare
			valore della misura da salvare

		*/
	}

	void Measure::middle( double *middlePoint, double *p1 , double *p2 )
	{
		middlePoint[0] = (p1[0] + p2[0]) / 2;
		middlePoint[1] = (p1[1] + p2[1]) / 2;
		middlePoint[2] = (p1[2] + p2[2]) / 2;
	}

	//----------------------------------------------------
	//  DistanceBetweenPointsMeasure
	//----------------------------------------------------
	DistanceBetweenPointsMeasure::DistanceBetweenPointsMeasure()
	{
		m_Line  = NULL;
		m_DBPLineTubeFilter = NULL;
		m_DBPLineMapper = NULL;
		m_DBPLineActor = NULL;

		m_P1Picked = false;
		m_P2Picked = false;

		m_P1Grabbed = false;
		m_P2Grabbed = false;

		m_P1[0] = m_P1[1] = m_P1[2] = 0;
		m_P2[0] = m_P2[1] = m_P2[2] = 0;

		bool m_Dragging = false;
	}

	DistanceBetweenPointsMeasure::~DistanceBetweenPointsMeasure()
	{
		DestroyPipe();
	}

	void DistanceBetweenPointsMeasure::CreatePipe()
	{
		vtkNEW(m_Line);
		vtkNEW(m_DBPLineTubeFilter);
		vtkNEW(m_DBPLineMapper);
		m_DBPLineTubeFilter->SetInput(m_Line->GetOutput());
		m_DBPLineTubeFilter->SetRadius(2);
		m_DBPLineMapper->SetInput(m_DBPLineTubeFilter->GetOutput());
		vtkNEW(m_DBPLineActor);
		m_DBPLineActor->SetMapper(m_DBPLineMapper);
		m_DBPLineActor->VisibilityOff();

		vtkNEW(m_Property);
		m_Property->SetColor(1,0,0);

		m_DBPLineActor->SetProperty(m_Property);
		
		assert(m_Rwi3DView);
		m_Rwi3DView->m_AlwaysVisibleRenderer->AddActor(m_DBPLineActor);

		m_CaptionActor = vtkCaptionActor2D::New();

		m_CaptionActor->SetPosition(0,0);
		m_CaptionActor->ThreeDimensionalLeaderOff();

		m_CaptionActor->SetHeight(0.15);
		m_CaptionActor->SetWidth(0.15);
		m_CaptionActor->BorderOff();

		vtkNEW(m_CaptionActorProperty);
		m_CaptionActorProperty->SetColor(1,0,0);

		m_CaptionActor->SetCaptionTextProperty(m_CaptionActorProperty);
		m_CaptionActor->SetVisibility(true);

		m_Rwi3DView->m_AlwaysVisibleRenderer->AddActor2D(m_CaptionActor);

	}

	void DistanceBetweenPointsMeasure::DestroyPipe()
	{
		vtkDEL(m_Line);
		vtkDEL(m_DBPLineTubeFilter);
		vtkDEL(m_DBPLineMapper);
		vtkDEL(m_DBPLineActor);
		vtkDEL(m_CaptionActor); 		
		vtkDEL(m_CaptionActorProperty);
		vtkDEL(m_Property);
	}

	void DistanceBetweenPointsMeasure::OnMouseDown( double *pickedPos )
	{
		{
			if (m_P1Picked == false)
			{
				m_P1[0] = pickedPos[0];
				m_P1[1] = pickedPos[1];
				m_P1[2] = pickedPos[2];
				m_P1Picked = true;
				m_P2Grabbed = true;
				m_Line->SetPoint1(m_P1);
				m_Line->SetPoint2(m_P1);
				m_DBPLineActor->VisibilityOn();

				// P2 initialization after P1 pick
				m_P2[0] = pickedPos[0];
				m_P2[1] = pickedPos[1];
				m_P2[2] = pickedPos[2];

				m_Rwi3DView->CameraUpdate();
			}
			else if (m_P1Picked == true && m_P2Picked == false)
			{
				m_P2[0] = pickedPos[0];
				m_P2[1] = pickedPos[1];
				m_P2[2] = pickedPos[2];
				m_P2Picked = true;
				m_P2Grabbed = true;

				m_Line->SetPoint2(m_P1);

				m_Rwi3DView->CameraUpdate();

			}
			else if (m_P1Picked == true && m_P2Picked == true)
			{
				double currentPoint[3] = {pickedPos[0], pickedPos[1], pickedPos[2]};

				double dP1_CP = vtkMath::Distance2BetweenPoints(m_P1 , currentPoint);
				double dP2_CP = vtkMath::Distance2BetweenPoints(m_P2 , currentPoint);

				if (dP1_CP >= dP2_CP)
				{
					m_P1Grabbed = false;
					m_P2Grabbed = true;

					// pick P2
					m_P2[0] = pickedPos[0];
					m_P2[1] = pickedPos[1];
					m_P2[2] = pickedPos[2];

					m_Line->SetPoint2(m_P2);
			
				}
				else
				{
					m_P1Grabbed = true;
					m_P2Grabbed = false;

					// pick P1
					m_P1[0] = pickedPos[0];
					m_P1[1] = pickedPos[1];
					m_P1[2] = pickedPos[2];

					m_Line->SetPoint1(m_P1);
					
				}

				m_Value = sqrt(vtkMath::Distance2BetweenPoints(m_P1, m_P2));
				UpdateCaptionActor();
				m_Rwi3DView->CameraUpdate();
			}
		}
		m_Dragging = true;
	}

	void DistanceBetweenPointsMeasure::OnMouseMove( double *pickedPos )
	{
		m_DBPLineActor->VisibilityOn();

		if (m_Dragging == true) // drag P2
		{
			if (m_P1Picked == true && m_P2Picked == false) // line does not exists
			{
				std::ostringstream stringStream;
				stringStream << "p1 " << pickedPos[0] << " " << pickedPos[1] << " " << pickedPos[2] << std::endl;          
				mafLogMessage(stringStream.str().c_str());

				mafLogMessage("mouse move with dragging");
				m_P2[0] = pickedPos[0];
				m_P2[1] = pickedPos[1];
				m_P2[2] = pickedPos[2];

				m_Line->SetPoint2(m_P2);
				m_DBPLineTubeFilter->Update();
				
				m_Rwi3DView->CameraUpdate();						
				m_P2Picked = true;
			}
			else if (m_P1Picked == true && m_P2Picked == true) // line exists already
			{
				if (m_P1Grabbed)
				{
					m_P1[0] = pickedPos[0];
					m_P1[1] = pickedPos[1];
					m_P1[2] = pickedPos[2];
					m_Line->SetPoint1(m_P1);
				}
				else if (m_P2Grabbed)
				{
					m_P2[0] = pickedPos[0];
					m_P2[1] = pickedPos[1];
					m_P2[2] = pickedPos[2];
					m_Line->SetPoint2(m_P2);
				}

				m_Value = sqrt(vtkMath::Distance2BetweenPoints(m_P1, m_P2));

				UpdateCaptionActor();
				m_Rwi3DView->CameraUpdate();						
			}
		}
		else
		{
			mafLogMessage("mouse move without dragging");
		}

	}

	void DistanceBetweenPointsMeasure::OnMouseUp()
	{
		m_Dragging = false;
		mafLogMessage("mouse up");

		m_P1Grabbed = false;
		m_P2Grabbed = false;
	}

	void DistanceBetweenPointsMeasure::Show( bool show )
	{
		throw std::exception("The method or operation is not implemented.");
		m_DBPLineActor->SetVisibility(show);
	}

	void DistanceBetweenPointsMeasure::Store()
	{
		// create the storage msf if not existent
		// store

	}

	void DistanceBetweenPointsMeasure::UpdateCaptionActor()
	{
		double middlePoint[3];
		middle(middlePoint, m_P1 , m_P2);

		m_CaptionActor->SetAttachmentPoint(middlePoint);

		std::ostringstream stringStream;
		stringStream << m_Value;          
		string valueString = stringStream.str().c_str();

		m_CaptionActor->SetCaption(valueString.c_str());
	}

	void DistanceBetweenPointsMeasure::Select(bool select)
	{
		m_Select = select;

		if (m_Select == true)
		{
			m_Property->SetColor(1,1,0);
			m_CaptionActorProperty->SetColor(1,1,0);			
		}
		else if (m_Select == false)
		{
			m_Property->SetColor(1,0,0);
			m_CaptionActorProperty->SetColor(1,0,0);
		}
	}

	//---------------------------------------------
	//  DistanceLinesMeasure
	//---------------------------------------------
	DistanceBetweenLinesMeasure::DistanceBetweenLinesMeasure()
	{
		m_LineOriginal = NULL;
		m_LineOriginalTubeFilter = NULL;
		m_LineOriginalMapper = NULL;
		m_LineOriginalActor = NULL;

		m_LineCopy = NULL;
		m_LineCopyTubeFilter = NULL;
		m_LineCopyMapper = NULL;
		m_LineCopyActor = NULL;

		m_P0LineOriginalPicked = false;
		m_P1LineOriginalPicked = false;
		m_P1LineOriginalGrabbed = false;
		m_PMiddleLineCopyPicked = false;
		m_PMiddleLineCopyGrabbed = false;

		m_LineOriginalGrabbed = false;
		m_LineCopyGrabbed = false;
	}

	DistanceBetweenLinesMeasure::~DistanceBetweenLinesMeasure()
	{
		DestroyPipe();
	}

	void DistanceBetweenLinesMeasure::CreatePipe()
	{
		vtkNEW(m_LineOriginal);
		vtkNEW(m_LineOriginalTubeFilter);
		vtkNEW(m_LineOriginalMapper);

		m_LineOriginalTubeFilter->SetInput(m_LineOriginal->GetOutput());
		m_LineOriginalTubeFilter->SetRadius(2);
		m_LineOriginalMapper->SetInput(m_LineOriginalTubeFilter->GetOutput());
		vtkNEW(m_LineOriginalActor);
		m_LineOriginalActor->SetMapper(m_LineOriginalMapper);
		m_LineOriginalActor->VisibilityOff();

		m_Rwi3DView->m_AlwaysVisibleRenderer->AddActor(m_LineOriginalActor);

		vtkNEW(m_LineCopy);
		vtkNEW(m_LineCopyTubeFilter);
		vtkNEW(m_LineCopyMapper);

		m_LineCopyTubeFilter->SetInput(m_LineCopy->GetOutput());
		m_LineCopyTubeFilter->SetRadius(2);
		m_LineCopyMapper->SetInput(m_LineCopyTubeFilter->GetOutput());
		vtkNEW(m_LineCopyActor);
		m_LineCopyActor->SetMapper(m_LineCopyMapper);
		m_LineCopyActor->VisibilityOff();

		vtkNEW(m_Property);
		m_Property->SetColor(1,0,0);

		m_LineCopyActor->SetProperty(m_Property);
		m_LineOriginalActor->SetProperty(m_Property);

		m_Rwi3DView->m_AlwaysVisibleRenderer->AddActor(m_LineCopyActor);

		m_CaptionActor = vtkCaptionActor2D::New();

		m_CaptionActor->SetPosition(0,0);
		m_CaptionActor->ThreeDimensionalLeaderOff();

		m_CaptionActor->SetHeight(0.15);
		m_CaptionActor->SetWidth(0.15);
		m_CaptionActor->BorderOff();

		vtkNEW(m_CaptionActorProperty);
		m_CaptionActorProperty->SetColor(1,0,0);

		m_CaptionActor->SetCaptionTextProperty(m_CaptionActorProperty);
		m_CaptionActor->SetVisibility(true);

		m_Rwi3DView->m_AlwaysVisibleRenderer->AddActor2D(m_CaptionActor);

	}

	void DistanceBetweenLinesMeasure::UpdateCaptionActor()
	{
		double middlePointOriginal[3];
		middlePointOriginal[0] = (m_P1LineOriginal[0] + m_P0LineOriginal[0]) / 2;
		middlePointOriginal[1] = (m_P1LineOriginal[1] + m_P0LineOriginal[1]) / 2;
		middlePointOriginal[2] = (m_P1LineOriginal[2] + m_P0LineOriginal[2]) / 2;

		double middlePointCopy[3];
		middlePointCopy[0] = (m_P1LineCopy[0] + m_P0LineCopy[0]) / 2;
		middlePointCopy[1] = (m_P1LineCopy[1] + m_P0LineCopy[1]) / 2;
		middlePointCopy[2] = (m_P1LineCopy[2] + m_P0LineCopy[2]) / 2;

		double middlePoint[3];
		middle(middlePoint, middlePointOriginal , middlePointCopy);

		m_CaptionActor->SetAttachmentPoint(middlePoint);

		std::ostringstream stringStream;
		stringStream << m_Value;          
		string valueString = stringStream.str().c_str();

		m_CaptionActor->SetCaption(valueString.c_str());
	}

	void DistanceBetweenLinesMeasure::Select(bool select)
	{
		m_Select = select;

		if (m_Select == true)
		{
			m_Property->SetColor(1,1,0);
			m_CaptionActorProperty->SetColor(1,1,0);			
		}
		else if (m_Select == false)
		{
			m_Property->SetColor(1,0,0);
			m_CaptionActorProperty->SetColor(1,0,0);
		}
	}

	void DistanceBetweenLinesMeasure::OnMouseDown( double *pickedPos )
	{
		{
			if (m_P0LineOriginalPicked == false) 
			{
				m_P0LineOriginal[0] = pickedPos[0];
				m_P0LineOriginal[1] = pickedPos[1];
				m_P0LineOriginal[2] = pickedPos[2];

				m_P0LineOriginalPicked = true;
				m_P1LineOriginalGrabbed = true;

				m_LineOriginal->SetPoint1(m_P0LineOriginal);
				m_LineOriginal->SetPoint2(m_P0LineOriginal);

				m_LineCopy->SetPoint1(m_P0LineOriginal);
				m_LineCopy->SetPoint2(m_P0LineOriginal);

				m_LineOriginalActor->VisibilityOn();
				m_LineCopyActor->VisibilityOn();

			}
			else if (m_P0LineOriginalPicked == true && m_P1LineOriginalPicked == false) // pick the second original line point
			{
				m_P1LineOriginal[0] = pickedPos[0];
				m_P1LineOriginal[1] = pickedPos[1];
				m_P1LineOriginal[2] = pickedPos[2];

				m_P1LineOriginalPicked = true;
				m_P1LineOriginalGrabbed = true; // grabbing P1Angle

				m_LineOriginal->SetPoint2(m_P1LineOriginal); // pick P1

			}
			else if (m_P0LineOriginalPicked == true && m_P1LineOriginalPicked == true && m_PMiddleLineCopyPicked == false) // build the copy line
			{
				// build the copy line and set its middle point
				m_PMiddleLineCopy[0] = pickedPos[0];
				m_PMiddleLineCopy[1] = pickedPos[1];
				m_PMiddleLineCopy[2] = pickedPos[2];

				m_PMiddleLineCopyPicked = true;
				m_PMiddleLineCopyGrabbed = true; // Grabbing middle point
				m_LineCopyGrabbed = true;
				// P1_M
				UpdateLineCopy(m_PMiddleLineCopy);

				m_Value = sqrt(DistanceToLine(m_P1LineCopy, m_P0LineOriginal, m_P1LineOriginal));
				UpdateCaptionActor();

			}
			else if (m_P0LineOriginalPicked == true && m_P1LineOriginalPicked == true && m_PMiddleLineCopyPicked == true)
			{
				// continua e guarda a che distanza sei dalle due linee: a quale sei piu' vicino?
				double currentPoint[3] = {pickedPos[0], pickedPos[1], pickedPos[2]};

				double dLineOriginal_CP = DistanceToLine( currentPoint , m_P0LineOriginal, m_P1LineOriginal);
				double dLineCopy_CP = DistanceToLine( currentPoint , m_P0LineCopy , m_P1LineCopy);

				if (dLineOriginal_CP > dLineCopy_CP) // move line copy
				{
					m_LineOriginalGrabbed = false;
					m_LineCopyGrabbed = true;

					// pick P1Angle
					UpdateLineCopy(currentPoint);
				}
				else // move line original
				{
					m_LineOriginalGrabbed = true;
					m_LineCopyGrabbed = false;

					UpdateLineOriginal(currentPoint);
				}

				m_Value = sqrt(DistanceToLine(m_P1LineCopy, m_P0LineOriginal, m_P1LineOriginal));
				UpdateCaptionActor();

			}

			m_Rwi3DView->CameraUpdate();

		}

		m_Dragging = true;
	}

	void DistanceBetweenLinesMeasure::OnMouseMove( double *pickedPos )
	{
		m_LineOriginalActor->VisibilityOn();

		if (m_Dragging == true) 
		{
			if (m_P0LineOriginalPicked == true && m_P1LineOriginalPicked == false && m_PMiddleLineCopyPicked == false)
			{
				std::ostringstream stringStream;
				stringStream << "dragging P1Angle " << pickedPos[0] << " " << pickedPos[1] << " " << pickedPos[2] << std::endl;          
				mafLogMessage(stringStream.str().c_str());

				mafLogMessage("mouse move with dragging");
				m_P1LineOriginal[0] = pickedPos[0];
				m_P1LineOriginal[1] = pickedPos[1];
				m_P1LineOriginal[2] = pickedPos[2];

				m_LineOriginal->SetPoint2(m_P1LineOriginal);
				m_LineOriginalTubeFilter->Update();

				m_P1LineOriginalPicked = true;

			}
			else if (m_P0LineOriginalPicked == true && m_P1LineOriginalPicked == true && m_PMiddleLineCopyPicked == false) 
			{
				if (m_P0LineOriginalGrabbed)
				{
					m_P0LineOriginal[0] = pickedPos[0];
					m_P0LineOriginal[1] = pickedPos[1];
					m_P0LineOriginal[2] = pickedPos[2];

					m_LineOriginal->SetPoint1(m_P0LineOriginal);
					m_LineCopy->SetPoint1(m_P0LineOriginal);
				}
				else if (m_P1LineOriginalGrabbed)
				{
					m_P1LineOriginal[0] = pickedPos[0];
					m_P1LineOriginal[1] = pickedPos[1];
					m_P1LineOriginal[2] = pickedPos[2];
					m_LineOriginal->SetPoint2(m_P1LineOriginal);
				}
				else if (m_LineCopyGrabbed == true)
				{
					m_PMiddleLineCopy[0] = pickedPos[0];
					m_PMiddleLineCopy[1] = pickedPos[1];
					m_PMiddleLineCopy[2] = pickedPos[2];
					UpdateLineCopy(m_PMiddleLineCopy);
				}

			}
			else if (m_P0LineOriginalPicked == true && m_P1LineOriginalPicked == true && m_PMiddleLineCopyPicked == true)
			{
				if (m_LineOriginalGrabbed == true)
				{
					UpdateLineOriginal(pickedPos);
				}
				else if (m_LineCopyGrabbed == true)
				{
					UpdateLineCopy(pickedPos);
				}

				m_Value = sqrt(DistanceToLine(m_P1LineCopy, m_P0LineOriginal, m_P1LineOriginal));
				UpdateCaptionActor();

			}

			m_Rwi3DView->CameraUpdate();						

		}
		else
		{
			mafLogMessage("mouse move without dragging");
		}

	}

	void DistanceBetweenLinesMeasure::OnMouseUp()
	{
		m_Dragging = false;
		m_P0LineOriginalGrabbed = false;
		m_P1LineOriginalGrabbed = false;
		m_PMiddleLineCopyGrabbed = false;
		mafLogMessage("mouse up");
	}

	void DistanceBetweenLinesMeasure::Show( bool show )
	{
		
	}

	void DistanceBetweenLinesMeasure::Store()
	{
		throw std::exception("The method or operation is not implemented.");
	}


	void DistanceBetweenLinesMeasure::DestroyPipe()
	{
		vtkDEL(m_LineOriginal);
		vtkDEL(m_LineOriginalTubeFilter);
		vtkDEL(m_LineOriginalMapper);
		vtkDEL(m_LineOriginalActor);
		vtkDEL(m_LineCopy);
		vtkDEL(m_LineCopyTubeFilter);
		vtkDEL(m_LineCopyMapper);
		vtkDEL(m_LineCopyActor);

		vtkDEL(m_CaptionActor); 		
		vtkDEL(m_CaptionActorProperty);
		vtkDEL(m_Property);

	}

	void DistanceBetweenLinesMeasure::UpdateLineCopy( double pickedPoint[3] )
	{
		double middlePointVectorPlus[3];
		middlePointVectorPlus[0] = (m_P1LineOriginal[0] - m_P0LineOriginal[0]) / 2;
		middlePointVectorPlus[1] = (m_P1LineOriginal[1] - m_P0LineOriginal[1]) / 2;
		middlePointVectorPlus[2] = (m_P1LineOriginal[2] - m_P0LineOriginal[2]) / 2;

		double middlePointVectorMinus[3] = {-middlePointVectorPlus[0] , -middlePointVectorPlus[1], -middlePointVectorPlus[2]};

		double p0Copy[3];
		double p1Copy[3];
		mafTransform::AddVectors(pickedPoint, middlePointVectorPlus, p0Copy);
		mafTransform::AddVectors(pickedPoint, middlePointVectorMinus, p1Copy);

		m_P0LineCopy[0] = p0Copy[0];
		m_P0LineCopy[1] = p0Copy[1];
		m_P0LineCopy[2] = p0Copy[2];

		m_P1LineCopy[0] = p1Copy[0];
		m_P1LineCopy[1] = p1Copy[1];
		m_P1LineCopy[2] = p1Copy[2];

		m_LineCopy->SetPoint1(m_P0LineCopy);
		m_LineCopy->SetPoint2(m_P1LineCopy);

		m_Rwi3DView->CameraUpdate();
	}

	void DistanceBetweenLinesMeasure::UpdateLineOriginal( double pickedPoint[3] )
	{
		double middlePointVectorPlus[3];
		middlePointVectorPlus[0] = (m_P1LineOriginal[0] - m_P0LineOriginal[0]) / 2;
		middlePointVectorPlus[1] = (m_P1LineOriginal[1] - m_P0LineOriginal[1]) / 2;
		middlePointVectorPlus[2] = (m_P1LineOriginal[2] - m_P0LineOriginal[2]) / 2;

		double middlePointVectorMinus[3] = {-middlePointVectorPlus[0] , -middlePointVectorPlus[1], -middlePointVectorPlus[2]};

		double p0Copy[3];
		double p1Copy[3];
		mafTransform::AddVectors(pickedPoint, middlePointVectorPlus, p0Copy);
		mafTransform::AddVectors(pickedPoint, middlePointVectorMinus, p1Copy);

		m_P0LineOriginal[0] = p0Copy[0];
		m_P0LineOriginal[1] = p0Copy[1];
		m_P0LineOriginal[2] = p0Copy[2];

		m_P1LineOriginal[0] = p1Copy[0];
		m_P1LineOriginal[1] = p1Copy[1];
		m_P1LineOriginal[2] = p1Copy[2];

		m_LineOriginal->SetPoint1(m_P0LineOriginal);
		m_LineOriginal->SetPoint2(m_P1LineOriginal);

		m_Rwi3DView->CameraUpdate();
	}

	double DistanceBetweenLinesMeasure::DistanceToLine (double x[3], double p1[3], double p2[3])
	{
		int i;
		double np1[3], p1p2[3], proj, den;

		for (i=0; i<3; i++)
		{
			np1[i] = x[i] - p1[i];
			p1p2[i] = p1[i] - p2[i];
		}

		if ( (den=vtkMath::Norm(p1p2)) != 0.0 )
		{
			for (i=0; i<3; i++)
			{
				p1p2[i] /= den;
			}
		}
		else
		{
			return vtkMath::Dot(np1,np1);
		}

		proj = vtkMath::Dot(np1,p1p2);

		return (vtkMath::Dot(np1,np1) - proj*proj);
	}
	//---------------------------------------------
	//  AngleMeasure
	//---------------------------------------------

	AngleMeasure::AngleMeasure()
	{

		m_O_P1Angle_Line=NULL;
		m_O_P1Angle_LineTubeFilter=NULL;
		m_O_P1Angle_LineMapper=NULL;
		m_O_P1Angle_LineActor=NULL;

		m_O_P2Angle_Line=NULL;
		m_O_P2Angle_LineTubeFilter=NULL;
		m_O_P2Angle_LineMapper=NULL;
		m_O_P2Angle_LineActor=NULL;

		m_Dragging = false;

		m_OriginPicked = false;
		m_OriginGrabbed = false;
		m_P1AnglePicked = false;
		m_P1AngleGrabbed = false;
		m_P2AnglePicked = false;
		m_P2AngleGrabbed = false;

	}

	AngleMeasure::~AngleMeasure()
	{
		DestroyPipe();
	}

	void AngleMeasure::CreatePipe()
	{
		vtkNEW(m_O_P1Angle_Line);
		vtkNEW(m_O_P1Angle_LineTubeFilter);
		vtkNEW(m_O_P1Angle_LineMapper);

		m_O_P1Angle_LineTubeFilter->SetInput(m_O_P1Angle_Line->GetOutput());
		m_O_P1Angle_LineTubeFilter->SetRadius(2);
		m_O_P1Angle_LineMapper->SetInput(m_O_P1Angle_LineTubeFilter->GetOutput());
		vtkNEW(m_O_P1Angle_LineActor);
		m_O_P1Angle_LineActor->SetMapper(m_O_P1Angle_LineMapper);
		m_O_P1Angle_LineActor->VisibilityOff();

		m_Rwi3DView->m_AlwaysVisibleRenderer->AddActor(m_O_P1Angle_LineActor);

		vtkNEW(m_O_P2Angle_Line);
		vtkNEW(m_O_P2Angle_LineTubeFilter);
		vtkNEW(m_O_P2Angle_LineMapper);

		m_O_P2Angle_LineTubeFilter->SetInput(m_O_P2Angle_Line->GetOutput());
		m_O_P2Angle_LineTubeFilter->SetRadius(2);
		m_O_P2Angle_LineMapper->SetInput(m_O_P2Angle_LineTubeFilter->GetOutput());
		vtkNEW(m_O_P2Angle_LineActor);
		m_O_P2Angle_LineActor->SetMapper(m_O_P2Angle_LineMapper);
		m_O_P2Angle_LineActor->VisibilityOff();

		vtkNEW(m_Property);
		m_Property->SetColor(1,0,0);

		m_O_P1Angle_LineActor->SetProperty(m_Property);
		m_O_P2Angle_LineActor->SetProperty(m_Property);

		m_Rwi3DView->m_AlwaysVisibleRenderer->AddActor(m_O_P2Angle_LineActor);

		m_CaptionActor = vtkCaptionActor2D::New();

		m_CaptionActor->SetPosition(0,0);
		m_CaptionActor->ThreeDimensionalLeaderOff();

		m_CaptionActor->SetHeight(0.15);
		m_CaptionActor->SetWidth(0.15);
		m_CaptionActor->BorderOff();

		vtkNEW(m_CaptionActorProperty);
		m_CaptionActorProperty->SetColor(1,0,0);

		m_CaptionActor->SetCaptionTextProperty(m_CaptionActorProperty);
		m_CaptionActor->SetVisibility(true);

		m_Rwi3DView->m_AlwaysVisibleRenderer->AddActor2D(m_CaptionActor);

	}

	void AngleMeasure::DestroyPipe()
	{
		vtkDEL(m_O_P1Angle_Line);
		vtkDEL(m_O_P1Angle_LineTubeFilter);
		vtkDEL(m_O_P1Angle_LineMapper);
		vtkDEL(m_O_P1Angle_LineActor);

		vtkDEL(m_O_P2Angle_Line);
		vtkDEL(m_O_P2Angle_LineTubeFilter);
		vtkDEL(m_O_P2Angle_LineMapper);
		vtkDEL(m_O_P2Angle_LineActor);

		vtkDEL(m_CaptionActor); 		
		vtkDEL(m_CaptionActorProperty);

		vtkDEL(m_Property);
	}

	void AngleMeasure::OnMouseDown( double *pickedPos )
	{
		{
			if (m_OriginPicked == false) // pick the Origin
			{
				m_Origin[0] = pickedPos[0];
				m_Origin[1] = pickedPos[1];
				m_Origin[2] = pickedPos[2];

				m_OriginPicked = true;
				m_P1AngleGrabbed = true; // grabbing P1Angle

				m_O_P1Angle_Line->SetPoint1(m_Origin);
				m_O_P1Angle_Line->SetPoint2(m_Origin);

				m_O_P2Angle_Line->SetPoint1(m_Origin);
				m_O_P2Angle_Line->SetPoint2(m_Origin);

				m_O_P1Angle_LineActor->VisibilityOn();
				m_O_P2Angle_LineActor->VisibilityOn();

			}
			else if (m_OriginPicked == true && m_P1AnglePicked == false) // pick the P1Angle
			{
				m_P1Angle[0] = pickedPos[0];
				m_P1Angle[1] = pickedPos[1];
				m_P1Angle[2] = pickedPos[2];

				m_P1AnglePicked = true;
				m_P1AngleGrabbed = true; // grabbing P1Angle

				m_O_P1Angle_Line->SetPoint2(m_P1Angle); // pick P1

			}
			else if (m_OriginPicked == true && m_P1AnglePicked == true && m_P2AnglePicked == false) // pick P2Angle
			{
				m_P2Angle[0] = pickedPos[0];
				m_P2Angle[1] = pickedPos[1];
				m_P2Angle[2] = pickedPos[2];

				m_P2AnglePicked = true;
				m_P2AngleGrabbed = true; // Grabbing P2Angle

				m_O_P2Angle_Line->SetPoint2(m_P2Angle);

			}
			else if (m_OriginPicked == true && m_P1AnglePicked == true && m_P2AnglePicked == true) // angle created: pick the closest point
			{
				// continua e guarda a che distanza sei dall'origine
				double currentPoint[3] = {pickedPos[0], pickedPos[1], pickedPos[2]};

				double dOrigin_CP = vtkMath::Distance2BetweenPoints(m_Origin , currentPoint);
				double dP1Angle_CP = vtkMath::Distance2BetweenPoints(m_P1Angle , currentPoint);
				double dP2Angle_CP = vtkMath::Distance2BetweenPoints(m_P2Angle , currentPoint);

				std::vector< double > v;
				v.push_back(dOrigin_CP);
				v.push_back(dP1Angle_CP);
				v.push_back(dP2Angle_CP);

				double minElement = *(min_element( v.begin(), v.end() )); 

				if (minElement == dP1Angle_CP)
				{
					m_OriginGrabbed = false;
					m_P1AngleGrabbed = true;
					m_P2AngleGrabbed = false;

					// pick P1Angle
					m_P1Angle[0] = pickedPos[0];
					m_P1Angle[1] = pickedPos[1];
					m_P1Angle[2] = pickedPos[2];

					m_O_P1Angle_Line->SetPoint2(m_P1Angle);
					m_Rwi3DView->CameraUpdate();
				}
				else if (minElement == dP2Angle_CP)
				{
					m_OriginGrabbed = false;
					m_P1AngleGrabbed = false;
					m_P2AngleGrabbed = true;

					// pick P1Angle
					m_P2Angle[0] = pickedPos[0];
					m_P2Angle[1] = pickedPos[1];
					m_P2Angle[2] = pickedPos[2];

					m_O_P2Angle_Line->SetPoint2(m_P2Angle);
					m_Rwi3DView->CameraUpdate();
				}
				else if (minElement == dOrigin_CP)
				{
					m_OriginGrabbed = true;
					m_P1AngleGrabbed = false;
					m_P2AngleGrabbed = false;

					// pick P1
					m_Origin[0] = pickedPos[0];
					m_Origin[1] = pickedPos[1];
					m_Origin[2] = pickedPos[2];

					m_O_P1Angle_Line->SetPoint1(m_Origin);
					m_O_P2Angle_Line->SetPoint1(m_Origin);

				}

				m_Value =  ComputeAngleMeasure();
				UpdateCaptionActor();		

			}

			
		}

		m_Rwi3DView->CameraUpdate();

		m_Dragging = true;
	}

	void AngleMeasure::OnMouseMove( double *pickedPos )
	{
		m_O_P1Angle_LineActor->VisibilityOn();

		if (m_Dragging == true) // drag P2
		{
			if (m_OriginPicked == true && m_P1AnglePicked == false && m_P2AnglePicked == false) // first angle pick
			{
				m_P1Angle[0] = pickedPos[0];
				m_P1Angle[1] = pickedPos[1];
				m_P1Angle[2] = pickedPos[2];

				m_O_P1Angle_Line->SetPoint2(m_P1Angle);
				m_O_P1Angle_LineTubeFilter->Update();

				m_P1AnglePicked = true;

				m_Rwi3DView->CameraUpdate();						
			}
			else if (m_OriginPicked == true) // angle exists already
			{
				if (m_OriginGrabbed)
				{
					m_Origin[0] = pickedPos[0];
					m_Origin[1] = pickedPos[1];
					m_Origin[2] = pickedPos[2];

					m_O_P1Angle_Line->SetPoint1(m_Origin);
					m_O_P2Angle_Line->SetPoint1(m_Origin);
				}
				else if (m_P1AngleGrabbed)
				{
					m_P1Angle[0] = pickedPos[0];
					m_P1Angle[1] = pickedPos[1];
					m_P1Angle[2] = pickedPos[2];
					m_O_P1Angle_Line->SetPoint2(m_P1Angle);
				}
				else if (m_P2AngleGrabbed)
				{
					m_P2Angle[0] = pickedPos[0];
					m_P2Angle[1] = pickedPos[1];
					m_P2Angle[2] = pickedPos[2];
					m_O_P2Angle_Line->SetPoint2(m_P2Angle);
				}
			}

			if (m_OriginPicked == true && m_P1AnglePicked == true && m_P2AnglePicked == true)
			{
				// compute angle measure
				m_Value =  ComputeAngleMeasure();
				UpdateCaptionActor();		
				m_Rwi3DView->CameraUpdate();						
			}
		}
		else
		{
			mafLogMessage("mouse move without dragging");
		}
	
	}

	void AngleMeasure::OnMouseUp()
	{
		m_Dragging = false;
		m_OriginGrabbed = false;
		m_P1AngleGrabbed = false;
		m_P2AngleGrabbed = false;
	}

	void AngleMeasure::Show( bool show )
	{

	}

	void AngleMeasure::Store()
	{
		throw std::exception("The method or operation is not implemented.");
	}

	void AngleMeasure::UpdateCaptionActor()
	{

		m_CaptionActor->SetAttachmentPoint(m_Origin);

		std::ostringstream stringStream;
		stringStream << m_Value;          
		string valueString = stringStream.str().c_str();

		m_CaptionActor->SetCaption(valueString.c_str());
	}

	double AngleMeasure::ComputeAngleMeasure()
	{
      double angle, v1[3], vn1, v2[3], vn2, s;
      v1[0] = m_P1Angle[0] - m_Origin[0];
      v1[1] = m_P1Angle[1] - m_Origin[1];
      v1[2] = m_P1Angle[2] - m_Origin[2];
      v2[0] = m_P2Angle[0] - m_Origin[0];
      v2[1] = m_P2Angle[1] - m_Origin[1];
      v2[2] = m_P2Angle[2] - m_Origin[2];

      vn1 = vtkMath::Norm(v1);
      vn2 = vtkMath::Norm(v2);
      s = vtkMath::Dot(v1,v2);

      if(vn1 != 0 && vn2 != 0)
        angle = acos(s / (vn1 * vn2));
      else
      {
        angle = 0;
      }

      angle *= vtkMath::RadiansToDegrees();

	  return angle;
    } 

	void AngleMeasure::Select(bool select)
	{
		m_Select = select;

		if (m_Select == true)
		{
			m_Property->SetColor(1,1,0);
			m_CaptionActorProperty->SetColor(1,1,0);			
		}
		else if (m_Select == false)
		{
			m_Property->SetColor(1,0,0);
			m_CaptionActorProperty->SetColor(1,0,0);
		}
	}


