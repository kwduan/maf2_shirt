/*=========================================================================
Program:   iPose
Module:    $RCSfile: iOpPlaneTraining.cpp,v $
Language:  C++
Date:      $Date: 2012-03-21 07:29:03 $
Version:   $Revision: 1.1.2.21 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/

#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iOpPlaneTraining.h"
#include "iDecl.h"
#include "iAttributeWorkflow.h"

#include "mafNode.h"
#include "mafGUIDialog.h"
#include "mafVMERoot.h"
#include "mafXMLString.h"
#include "mmuDOMTreeErrorReporter.h"
#include "mafXMLElement.h"
#include "mmuXMLDOMElement.h"
#include "mafVMEGroup.h"
#include "mafTagArray.h"
#include "mafTagItem.h"

#include "iGuiDialogListCase.h"
#include "iGuiDialogShowRx.h"
#include "iGuiDialog3DReconstrution.h"
#include "iGuiDialogChoiceImplant.h"
#include "iGuiDialogPlacingImplant.h"
#include "iGuiDialogValidationValues.h"

#include "vtkMAFSmartPointer.h"
#include "vtkSphereSource.h"
#include "vtkDataSetWriter.h"
#include "vtkPolyDataWriter.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include <xercesc/framework/LocalFileInputSource.hpp>
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/sax/ErrorHandler.hpp>

#include <wx/file.h>
#include <wx/dir.h>
#include <wx/tokenzr.h>
#include "mafNodeIterator.h"

#include <vector>
#include "mafVMEStorage.h"


#define round(x) (x<0?ceil((x)-0.5):floor((x)+0.5))

//----------------------------------------------------------------------------
mafCxxTypeMacro(iOpPlaneTraining);
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
iOpPlaneTraining::iOpPlaneTraining(wxString label) :
mafOp(label)
	//----------------------------------------------------------------------------
{
	m_OpType  = OPTYPE_OP;
	m_Canundo = true;
	m_InputPreserving = true;

	m_AddCase = false;
	m_FileRX = "";

	m_PatientCaseId = "";

	/*
	vtkMAFSmartPointer<vtkSphereSource> sphere;
	sphere->SetRadius(1.0);
	sphere->Update();

	vtkMAFSmartPointer<vtkPolyDataWriter> w;
	w->SetFileName("SFERA.vtk");
	w->SetInput(sphere->GetOutput());
	w->Write();*/
}
//----------------------------------------------------------------------------
iOpPlaneTraining::~iOpPlaneTraining()
	//----------------------------------------------------------------------------
{
}
//----------------------------------------------------------------------------
mafOp* iOpPlaneTraining::Copy()
	//----------------------------------------------------------------------------
{
	/** return a copy of itself, needs to put it into the undo stack */
	return new iOpPlaneTraining(m_Label);
}
//----------------------------------------------------------------------------
bool iOpPlaneTraining::Accept(mafNode* vme)
	//----------------------------------------------------------------------------
{
	return vme != NULL && mafVMERoot::SafeDownCast(vme);
}
//----------------------------------------------------------------------------
void iOpPlaneTraining::OpRun()
	//----------------------------------------------------------------------------
{

	//Check if DB of prosthesis is empty or not
	if (!CheckIfProsthesisArePresent())
	{
		wxMessageBox(_("There aren't any prosthesis! Please fill DB using the operation : \"Add Prosthesis to DB\""));
		OpStop(OP_RUN_CANCEL);
		return;
	}

	int result = 0;

	m_PreStepStored = -1;

	/*mafVMERoot *root = mafVMERoot::SafeDownCast(m_Input->GetRoot());
	iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(root->GetAttribute("Workflow"));
	if (attribute != NULL)
	{
	m_CurrentStep = attribute->GetCurrentStep();
	}
	else
	{
	m_CurrentStep = ID_STEP_USER_CHOICE;
	}*/

	mafEventMacro(mafEvent(this,IPOSE_MSF_NEW));

	mafEvent eGetRoot;
	eGetRoot.SetSender(this);
	eGetRoot.SetId(IPOSE_GET_ROOT);
	mafEventMacro(eGetRoot);

	this->SetInput(eGetRoot.GetVme());

	m_CurrentStep = ID_STEP_SELECT_CASE_EXISTING;
	while (m_CurrentStep != ID_STEP_LAST && result != wxCANCEL)
	{
		m_PreStepStored = m_CurrentStep;

		if (m_CurrentStep == ID_STEP_SELECT_CASE_EXISTING)
		{
			result = StepSelectCase();

      iAttributeWorkflow *attribute = iAttributeWorkflow::New();
      mafVMEGroup *groupAttributes;
      mafNEW(groupAttributes);
      groupAttributes->GetTagArray()->SetTag(TAG_GROUP_ATTRIBUTES_TRAINING,"");
      groupAttributes->SetName("Attributes Training");
      groupAttributes->ReparentTo(m_Input);
      groupAttributes->Update();
      groupAttributes->SetAttribute("Workflow Training",attribute);      

			continue;
		}

    if (m_CurrentStep == ID_STEP_MEASURE)
    {
      result = StepShowRX(iGuiDialogShowRx::MEASURE_TOOL);
      continue;
    }

    if (m_CurrentStep == ID_STEP_3D_RECONSTRUTION_SHOW)
    {
      result = Step3DReconstrution();
      continue;
    }

		if (m_CurrentStep == ID_STEP_CHOICE_IMPLANT)
		{
			result = StepChoiceImplant();
			continue;
		}
		if (m_CurrentStep == ID_STEP_PLACING_IMPLANT)
		{
			result = StepPlacingImplant();
			continue;
		}
		if (m_CurrentStep == ID_STEP_VALIDATION_VALUES)
		{
			result = StepValidationValues();
			continue;
		}
	}

	if (result == wxCANCEL)
	{
    mafEventMacro(mafEvent(this,IPOSE_MSF_NEW));
    mafEvent eGetRoot;
    eGetRoot.SetSender(this);
    eGetRoot.SetId(IPOSE_GET_ROOT);
    mafEventMacro(eGetRoot);
    this->SetInput(eGetRoot.GetVme());

		OpStop(OP_RUN_CANCEL);
		return;
	}
  else if (m_CurrentStep == ID_STEP_LAST)
  {

    ComputeScore();

    mafString fileName = mafGetApplicationDirectory().c_str();
    fileName<<"/download/";
    fileName<<m_PatientCaseId;
    fileName<<"/Download";
    fileName<<m_PatientCaseId;
    fileName<<".msf";

    if(wxFileExists(fileName.GetCStr()) )
    {
      mafVMEStorage *storage;
      storage = mafVMEStorage::New();
      storage->SetURL(fileName.GetCStr());
      storage->Restore();
      mafVMERoot *root;
      root = storage->GetRoot();
      root->Initialize();

      mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(root,TAG_GROUP_ATTRIBUTES));
      if (groupAttributes != NULL)
      {
        iAttributeWorkflow *attribute = NULL;
        attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));
        attribute->SetScore(m_TrainingScore);
      }

      storage->Store();
      mafDEL(storage);
    }
  }


	OpStop(OP_RUN_OK);
}
//----------------------------------------------------------------------------
void iOpPlaneTraining::OpDo()   
	//----------------------------------------------------------------------------
{
	if (m_Output)
	{
		m_Output->ReparentTo(m_Input);
		mafEventMacro(mafEvent(this,CAMERA_UPDATE));
	}
}
//----------------------------------------------------------------------------
void iOpPlaneTraining::OpStop(int result)   
	//----------------------------------------------------------------------------
{
	mafEventMacro(mafEvent(this,result));
}
//----------------------------------------------------------------------------
void iOpPlaneTraining::OnEvent(mafEventBase *maf_event)
	//----------------------------------------------------------------------------
{
	if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
	{
		switch(e->GetId())
		{	
		case wxOK:
			OpStop(OP_RUN_OK);        
			break;
		case wxCANCEL:
			OpStop(OP_RUN_CANCEL);        
			break;
		case ID_EXISTING_CASE:
			m_AddCase = false;
			m_AddOrUseDialog->EndModal(wxID_OK);
			break;
		case ID_ADD_CASE:
			m_AddCase = true;
			m_AddOrUseDialog->EndModal(wxID_OK);
			break;;
		default:
			mafEventMacro(*e);
			break;
		}
	}
	else
	{
		mafEventMacro(*e);
	}
}
//----------------------------------------------------------------------------
int iOpPlaneTraining::GetUserChoiceAddOrUseCase()
	//----------------------------------------------------------------------------
{
	m_AddOrUseDialog = new mafGUIDialog(_("Prosthesis Planning"),mafCLOSEWINDOW);

	mafGUI *gui = new mafGUI(this);

	gui->Button(ID_ADD_CASE,_("Add Case"));
	gui->Button(ID_EXISTING_CASE,_("Use Existing Case"));

	m_AddOrUseDialog->Add(gui);

	m_AddOrUseDialog->ShowModal();

	cppDEL(m_AddOrUseDialog);

	return MAF_OK;
}
//----------------------------------------------------------------------------
int iOpPlaneTraining::Step3DReconstrution()
//----------------------------------------------------------------------------
{
	iGuiDialog3DReconstrution *dialog3DReconstrution = new iGuiDialog3DReconstrution(_("Prosthesis Planning - 3D Reconstruction"),m_Mouse,mafVMERoot::SafeDownCast(m_Input),m_FileRX,m_CurrentStep,atoi(m_PatientCaseId.GetCStr()),true);

	int x_pos,y_pos,w,h;
	mafGetFrame()->GetPosition(&x_pos,&y_pos);
	dialog3DReconstrution->GetSize(&w,&h);
	dialog3DReconstrution->SetSize(x_pos+5,y_pos+5,w,h);
	dialog3DReconstrution->SetMinSize(wxSize(800,600));
	dialog3DReconstrution->Show(true);
	mafYield();

  if (m_CurrentStep == ID_STEP_3D_RECONSTRUTION_SHOW)
  {
    dialog3DReconstrution->ShowLandmarks();
    dialog3DReconstrution->ShowReconstruction();
    dialog3DReconstrution->EnableShowModalityGui();
  }

	int result = dialog3DReconstrution->ShowModal();

	if (result == wxID_RETURN_NEXT)
	{
		m_CurrentStep++;
	}
	else if (result == wxID_RETURN_PREV)
	{
		m_CurrentStep = ID_STEP_SELECT_CASE_EXISTING;//Should skip step START
	}
	else
	{
		m_CurrentStep = ID_STEP_LAST;
	}

	cppDEL(dialog3DReconstrution);

	return result;
}
//----------------------------------------------------------------------------
int iOpPlaneTraining::StepChoiceImplant()
	//----------------------------------------------------------------------------
{
	iGuiDialogChoiceImplant *dialogChoiceImplant = new iGuiDialogChoiceImplant(_("Prosthesis Planning - Choice Implant"),mafVMERoot::SafeDownCast(m_Input),m_Joint,m_Side,m_NeckVector,true);
	int result = dialogChoiceImplant->ShowModal();

	if (result == wxID_RETURN_NEXT)
	{
		m_CurrentStep++;
	}
	else if (result == wxID_RETURN_PREV)
	{
		m_CurrentStep--;
	}
	else
	{
		m_CurrentStep = ID_STEP_LAST;
	}

	cppDEL(dialogChoiceImplant);

	return result;
}
//----------------------------------------------------------------------------
int iOpPlaneTraining::StepPlacingImplant()
	//----------------------------------------------------------------------------
{
	iGuiDialogPlacingImplant *dialogPlacingImplant = new iGuiDialogPlacingImplant(_("Prosthesis Planning - Placing Implant"),mafVMERoot::SafeDownCast(m_Input),m_Side,true);
  int result = dialogPlacingImplant->ShowModal();

	if (result == wxID_RETURN_NEXT)
	{
		m_CurrentStep++;
	}
	else if (result == wxID_RETURN_PREV)
	{
		m_CurrentStep--;
	}
	else
	{
		m_CurrentStep = ID_STEP_LAST;
	}

	cppDEL(dialogPlacingImplant);

	return result;
}
//----------------------------------------------------------------------------
int iOpPlaneTraining::StepValidationValues()
	//----------------------------------------------------------------------------
{
	iGuiDialogValidationValues *dialogValidationValues = new iGuiDialogValidationValues(_("Prosthesis Planning - Validation Values"),m_Mouse,mafVMERoot::SafeDownCast(m_Input),atoi(m_PatientCaseId),m_Joint,m_PatientWeight,m_PatientHeight,m_PCA,true);

	int x_pos,y_pos,w,h;
	mafGetFrame()->GetPosition(&x_pos,&y_pos);
	dialogValidationValues->GetSize(&w,&h);
	dialogValidationValues->SetSize(x_pos+5,y_pos+5,w,h);
	dialogValidationValues->SetMinSize(wxSize(355,180));
	dialogValidationValues->Show(true);
	mafYield();

	dialogValidationValues->ComputeValidation();
	dialogValidationValues->Show(false);

	int result = dialogValidationValues->ShowModal();

	if (result == wxID_RETURN_NEXT)
	{
		m_CurrentStep = ID_STEP_LAST;
	}
	else if (result == wxID_RETURN_PREV)
	{
		m_CurrentStep--;
  }
  else if (result == wxCANCEL)
  {
    m_CurrentStep = wxCANCEL;
  }

	cppDEL(dialogValidationValues);

	return result;
}
//----------------------------------------------------------------------------
int iOpPlaneTraining::StepSelectCase()
	//----------------------------------------------------------------------------
{
	iGuiDialogListCase *listCasesDialog = new iGuiDialogListCase(_("Prosthesis Planning"),"/download/cacheFile.txt",false,false);
	int result = listCasesDialog->ShowModal();

	//Reset old selection
	m_Joint = -1;
	m_Side = -1;
	m_PatientCaseId = "";
	m_PatientName = "";
	m_FileRX = "";
	m_Diagnosis = "";
  m_PatientWeight = 0.0;
  m_PatientHeight = 0.0;
  for (int i=0;i<35;i++)
  {
    m_PCA[i] = 0.0;
  }
	if (result == wxID_RETURN_NEXT)
	{
		m_PatientCaseId = listCasesDialog->GetCaseId();
		m_FileRX = listCasesDialog->GetFileNameCaseSelected();
		m_Side = listCasesDialog->GetSideCaseSelected();
		m_Joint = listCasesDialog->GetJointCaseSelected();
		m_Diagnosis = listCasesDialog->GetDiagnosisCaseSelected();
		m_PatientName = listCasesDialog->GetPatientNameCaseSelected();
    m_PatientWeight = listCasesDialog->GetWeightCaseSelected();
    m_PatientHeight = listCasesDialog->GetHeightCaseSelected();

		m_CurrentStep = ID_STEP_MEASURE;
	}
	else
	{
		m_CurrentStep = wxCANCEL;
		return wxCANCEL;
	}

	cppDEL(listCasesDialog);

	mafString fileName = mafGetApplicationDirectory().c_str();
	fileName<<"/download/";
	fileName<<m_PatientCaseId;
	fileName<<"/Download";
	fileName<<m_PatientCaseId;
	fileName<<".msf";

	if(wxFileExists(fileName.GetCStr()) )
	{

		mafEventMacro(mafEvent(this,IPOSE_MSF_NEW));
		mafEventMacro(mafEvent(this,MENU_FILE_OPEN,&fileName));

		mafEvent eGetRoot;
		eGetRoot.SetSender(this);
		eGetRoot.SetId(IPOSE_GET_ROOT);
		mafEventMacro(eGetRoot);

		this->SetInput(eGetRoot.GetVme());
    mafVMERoot *root = mafVMERoot::SafeDownCast(m_Input->GetRoot());
    iAttributeWorkflow *attribute = NULL;
    mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(root,TAG_GROUP_ATTRIBUTES));
    attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));
		if (attribute != NULL)
		{
			m_CurrentStep = attribute->GetCurrentStep();
			m_Joint = attribute->GetJoint();
			m_Side = attribute->GetSide();
			m_PatientName = attribute->GetPatientName();
      attribute->GetPCA(m_PCA);
      attribute->GetNeckVector(m_NeckVector);
      m_PatientWeight = attribute->GetPatientWeight();
      m_PatientHeight = attribute->GetPatientHeight();
		}
	}

	return result;
}
//----------------------------------------------------------------------------
int iOpPlaneTraining::StepShowRX( int dialogMode /*= DEFAULT*/ )
//----------------------------------------------------------------------------
{
	iGuiDialogShowRx *dialogShowRx = NULL;
	mafString currentRXABSFileName = "";
	wxString dialogText = "Prosthesis Planning - RX View";

	if (dialogMode == iGuiDialogShowRx::MEASURE_TOOL)
	{
		dialogText = "Prosthesis Planning - Measuring Tool";
	}

	const wxString constCastDialogText = dialogText;

	dialogShowRx = new iGuiDialogShowRx(constCastDialogText , m_Mouse,mafVMERoot::SafeDownCast(m_Input),dialogMode,currentRXABSFileName, 
		m_PatientCaseId,true);

	if (dialogMode == iGuiDialogShowRx::SHOW_RX_ONLY || dialogMode == iGuiDialogShowRx::CALIBRATE_TOOL)
	{
		dialogShowRx->SetPatientName(m_PatientName);
		dialogShowRx->SetSide(m_Side);
		dialogShowRx->SetJoint(m_Joint);
		dialogShowRx->SetDiagnosis(m_Diagnosis);
    dialogShowRx->SetPatientHeight(m_PatientHeight);
    dialogShowRx->SetPatientWeight(m_PatientWeight);
	}

	int result = dialogShowRx->ShowModal();

	if (dialogMode == iGuiDialogShowRx::SHOW_RX_ONLY)
	{
		m_PatientName = dialogShowRx->GetPatientName();
		m_Side = dialogShowRx->GetSide();
		m_Joint = dialogShowRx->GetJoint();
		m_Diagnosis = dialogShowRx->GetDiagnosis();
    m_PatientWeight = dialogShowRx->GetPatientWeight();
    m_PatientHeight = dialogShowRx->GetPatientHeight();

		if (result == wxID_RETURN_NEXT)
		{
			m_CurrentStep++;
		}
		else if (result == wxID_RETURN_PREV)
		{
			m_CurrentStep = ID_STEP_SELECT_CASE_EXISTING;
		}
		else
		{
			m_CurrentStep = wxCANCEL;
		}
	}
	else if (dialogMode == iGuiDialogShowRx::MEASURE_TOOL)
	{
		if (result == wxID_RETURN_PREV)
		{
			m_CurrentStep = ID_STEP_CALIBRATE;
		}
		else if (result == wxID_RETURN_NEXT)
		{
			m_CurrentStep = ID_STEP_3D_RECONSTRUTION_SHOW;
		}
		else
		{
			m_CurrentStep = wxCANCEL;
		}
	}


	cppDEL(dialogShowRx);

	return result;
}
//----------------------------------------------------------------------------
bool iOpPlaneTraining::CheckIfProsthesisArePresent()
//----------------------------------------------------------------------------
{
	bool result = false;

	//Open the file xml with manufacture and model information
	try {
		XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Initialize();
	}
	catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
		// Do your failure processing here
		return MAF_ERROR;
	}

	XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser *XMLParser = new XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser;

	XMLParser->setValidationScheme(XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser::Val_Auto);
	XMLParser->setDoNamespaces(false);
	XMLParser->setDoSchema(false);
	XMLParser->setCreateEntityReferenceNodes(false);

	mmuDOMTreeErrorReporter *errReporter = new mmuDOMTreeErrorReporter();
	XMLParser->setErrorHandler(errReporter);

	mafString FileName = mafGetApplicationDirectory().c_str();
	FileName<<PATH_PROSTHESIS_DB_FILE;

	if(!wxFile::Exists(FileName.GetCStr()))
	{
		mafLogMessage(_("DB file don't exist"));
		return false;
	}

	try {
		XMLParser->parse(FileName.GetCStr());
		int errorCount = XMLParser->getErrorCount(); 

		if (errorCount != 0)
		{
			// errors while parsing...
			mafErrorMessage("Errors while parsing XML file");
		}
		else
		{
			// extract the root element and wrap inside a mafXMLElement
			XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument *doc = XMLParser->getDocument();
			XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *root = doc->getDocumentElement();
			assert(root);

			mafString name = mafXMLString(root->getTagName());
			if (name == "DB")
			{
				mafString version = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)root)->getAttribute(mafXMLString("Version")));

				//Check the DB version
				if (version != PROSTHESIS_DB_VERSION)
				{
					return MAF_ERROR;
				}
			}

			XERCES_CPP_NAMESPACE_QUALIFIER DOMNodeList *manufacturesChildren=root->getChildNodes();

			if (manufacturesChildren->getLength() > 1)
			{
				result = true;
			}
		}

	}
	catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
		return false;
	}
	catch (const XERCES_CPP_NAMESPACE_QUALIFIER DOMException& toCatch) {
		return false;
	}
	catch (...) {
		return false;
	}

	cppDEL (errReporter);
	delete XMLParser;

	// terminate the XML library
	XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Terminate();

	return result;
}
//----------------------------------------------------------------------------
void iOpPlaneTraining::ComputeScore()
//----------------------------------------------------------------------------
{
  int score = 0;
  double scorePose = 0;
  double scoreMeasures = 0;
  double scoreProsthesis = 0;

  mafVMERoot *root = mafVMERoot::SafeDownCast(m_Input);

  mafVMEImage *RX = NULL;
  mafNodeIterator *iter = root->NewIterator();

  mafVMEGroup *groupProsthesis = NULL;
  mafVMEGroup *groupProsthesisTraining = NULL;
  iAttributeWorkflow *attributeWorkflow = NULL;
  iAttributeWorkflow *attributeWorkflowTraining = NULL;

  std::vector<mafVME *> prosthesis;
  std::vector<mafVME *> prosthesisTraining;
  FindVmeByTag(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_GROUP,prosthesis);
  FindVmeByTag(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_GROUP_TRAINING,prosthesisTraining);

  for (mafNode *node = iter->GetFirstNode(); node; node = iter->GetNextNode())
  {

    if (attributeWorkflow == NULL && node->GetAttribute("Workflow"))
    {
      attributeWorkflow = iAttributeWorkflow::SafeDownCast(node->GetAttribute("Workflow"));
    }

    if (attributeWorkflowTraining == NULL && node->GetAttribute("Workflow Training"))
    {
      attributeWorkflowTraining = iAttributeWorkflow::SafeDownCast(node->GetAttribute("Workflow Training"));
    }
  }

  iter->Delete();


  //////////////////////////////////////////////////////////////////////////
  // Scrore for measures
  //////////////////////////////////////////////////////////////////////////
  double anteversion = attributeWorkflow->GetHipAnteversion();
  double ccd = attributeWorkflow->GetHipCCD();
  double femoralCanal = attributeWorkflow->GetHipFemoralCanal();
  double femoralHead = attributeWorkflow->GetHipFemoralHead();
  double neckLength = attributeWorkflow->GetHipNeckLength();
  
  wxString measuresFileName = mafGetApplicationDirectory().c_str();
  measuresFileName << "/download/";
  measuresFileName << m_PatientCaseId;
  measuresFileName << "/";
  measuresFileName += RX_MEASURES_FILENAME;
  std::map<wxString,double> measures;
  int result = ReadMeasures(measuresFileName,measures);

  if (result == MAF_ERROR)
  {
    wxMessageBox(_("Error during reading of measures"));
    return;
  }
  
  double anteversionTraining = measures["anteversion (deg)"];
  double ccdTraining = measures["ccd (deg)"];
  double femoralCanalTraining = measures["femoral canal (mm)"];
  double femoralHeadTraining = measures["femoral head (mm)"];
  double neckTraining = measures["neck length (mm)"];

  double diff = 0;
  diff = abs(anteversion-anteversionTraining);
  scoreMeasures += ComputeScoreForDifference(diff);
  diff = abs(ccd-ccdTraining);
  scoreMeasures += ComputeScoreForDifference(diff);
  diff = abs(femoralCanal-femoralCanalTraining);
  scoreMeasures += ComputeScoreForDifference(diff);
  diff = abs(femoralHead-femoralHeadTraining);
  scoreMeasures += ComputeScoreForDifference(diff);
  diff = abs(neckLength-neckTraining);
  scoreMeasures += ComputeScoreForDifference(diff);

  scoreMeasures = (scoreMeasures/5.0);
  //////////////////////////////////////////////////////////////////////////


  std::map<mafString,int> scoresProsthesisSize;
  std::map<mafString,int> scoresProsthesisTranslate;
  std::map<mafString,int> scoresProsthesisRotate;

  int numOfProsthesisForScore = 0;
  if (prosthesis.size()>0 && prosthesisTraining.size()>0)
  {
    for(int iProsthesis = 0;iProsthesis<prosthesis.size();iProsthesis++)
    {
      wxStringTokenizer tkz(prosthesis[iProsthesis]->GetName(),wxT('-')); // Read t values
      int num_t = tkz.CountTokens();

      if(num_t<2) // name is composed by manufacture and model name
      {
        continue;
      }

      mafString manufacture = tkz.GetNextToken();
      mafString model = tkz.GetNextToken();

      mafString prosthesisType = attributeWorkflow->GetProsthesisType(manufacture,model);

      if (prosthesisType == "")
      {
        continue;
      }

      for(int iProsthesisTraining = 0;iProsthesisTraining<prosthesisTraining.size();iProsthesisTraining++)
      {
        wxStringTokenizer tkz(prosthesisTraining[iProsthesisTraining]->GetName(),wxT('-')); // Read t values
        int num_t = tkz.CountTokens();

        if(num_t<2) // name is composed by manufacture and model name
        {
          continue;
        }

        mafString manufactureTraining = tkz.GetNextToken();
        mafString modelTraining = tkz.GetNextToken();

        mafString prosthesisTypeTraining = attributeWorkflowTraining->GetProsthesisType(manufactureTraining,modelTraining);

        if ( prosthesisTypeTraining == "" )
        {
          continue;
        }

        if (manufactureTraining != manufacture || modelTraining !=  model)
        {
          continue;
        }

        mafString nameComplete = manufacture;
        nameComplete << "-";
        nameComplete << model;
        nameComplete << "-";
        nameComplete << prosthesisType;


        int numComponents = attributeWorkflow->GetNumberOfComponents(manufacture,model);
        int numComponentsTraining = attributeWorkflowTraining->GetNumberOfComponents(manufacture,model);
        if ( prosthesisType == prosthesisTypeTraining && numComponents == numComponentsTraining )
        {
          std::map<mafString,mafString> componentsSizes;
          attributeWorkflow->GetComponentsSizes(manufacture,model,componentsSizes);

          std::map<mafString,mafString> componentsSizesTraining;
          attributeWorkflowTraining->GetComponentsSizes(manufactureTraining,modelTraining,componentsSizesTraining);

          std::map<mafString,mafString>::iterator iter;
          for(iter = componentsSizes.begin();iter != componentsSizes.end();iter++)
          {
            std::map<mafString,mafString>::iterator iterTraining = componentsSizesTraining.find(iter->first);

            if (iterTraining == componentsSizesTraining.end())
            {
              continue;
            }
            int sizeTraining = atoi(iterTraining->second.GetCStr());
            int size = atoi(iter->second.GetCStr());

            scoresProsthesisSize[nameComplete] = ComputeScoreForProsthesisSize(size,sizeTraining);
            /*scoreProsthesis += ComputeScoreForProsthesisSize(size,sizeTraining);*/
          }
        }
        
        double pose[3],rot[3];
        groupProsthesis = mafVMEGroup::SafeDownCast(prosthesis[iProsthesis]);
        groupProsthesis->GetOutput()->GetAbsPose(pose,rot);
        double poseTraining[3],rotTraining[3];
        groupProsthesisTraining = mafVMEGroup::SafeDownCast(prosthesisTraining[iProsthesisTraining]);
        groupProsthesisTraining->GetOutput()->GetAbsPose(poseTraining,rotTraining);

        double poseDiff = sqrt( abs(pose[0] - poseTraining[0])*abs(pose[0] - poseTraining[0]) + abs(pose[1] - poseTraining[1])*abs(pose[1] - poseTraining[1]) + abs(pose[2] - poseTraining[2])*abs(pose[2] - poseTraining[2]) );
        double rotDiff = sqrt( abs(rot[0] - rotTraining[0])*abs(rot[0] - rotTraining[0]) + abs(rot[1] - rotTraining[1])*abs(rot[1] - rotTraining[1]) + abs(rot[2] - rotTraining[2])*abs(rot[2] - rotTraining[2]) );

        double tmp = 0;
        /*tmp += ComputeScoreForDifference(poseDiff);*/
        scoresProsthesisTranslate[nameComplete] = ComputeScoreForDifference(poseDiff);
        scoresProsthesisRotate[nameComplete] = ComputeScoreForDifference(rotDiff);
        /*tmp += ComputeScoreForDifference(rotDiff);*/

        /*scorePose += round(tmp/2.0);*/


        numOfProsthesisForScore++;

      }
    }
  }

  score = VTK_INT_MAX;
  mafString textOutput;
  if (numOfProsthesisForScore != 0)
  {
//     scoreProsthesis = round(scoreProsthesis / numOfProsthesisForScore);
//     scorePose = round(scorePose / numOfProsthesisForScore);
// 
//     score += scorePose;
//     score += scoreProsthesis;
// 
//     score = score / 3.0;

    std::map<mafString,int>::iterator iter;
    for (iter = scoresProsthesisSize.begin();iter!=scoresProsthesisSize.end();iter++)
    {
      textOutput << iter->first;
      textOutput << " SIZE : ";
      textOutput << iter->second;
      textOutput << "\n";

      if (iter->second < score)
      {
        score = iter->second;
      }
    }

    for (iter = scoresProsthesisTranslate.begin();iter!=scoresProsthesisTranslate.end();iter++)
    {
      textOutput << iter->first;
      textOutput << " TRASLATE : ";
      textOutput << iter->second;
      textOutput << "\n";

      if (iter->second < score)
      {
        score = iter->second;
      }
    }

    for (iter = scoresProsthesisRotate.begin();iter!=scoresProsthesisRotate.end();iter++)
    {
      textOutput << iter->first;
      textOutput << " ROTATE : ";
      textOutput << iter->second;
      textOutput << "\n";

      if (iter->second < score)
      {
        score = iter->second;
      }
    }

  }
  else
  {
    mafLogMessage(">>>>>ERROR!! No prosthesis information in the selected case!!");
  }
  
  textOutput << "MEASURES : ";
  textOutput << scoreMeasures;
  textOutput << "\n";

  if (scoreMeasures < score)
  {
    score = scoreMeasures;
  }

  textOutput << "Your score is : ";
  textOutput << score;

  m_TrainingScore = score;
  wxMessageBox(textOutput.GetCStr());


  mafEventMacro(mafEvent(this,IPOSE_MSF_NEW));
  mafEvent eGetRoot;
  eGetRoot.SetSender(this);
  eGetRoot.SetId(IPOSE_GET_ROOT);
  mafEventMacro(eGetRoot);
  this->SetInput(eGetRoot.GetVme());
}

int iOpPlaneTraining::ComputeScoreForDifference(double diff)
{
  if (diff < 1)
  {
    return 5;
  }
  else if (diff >= 1 && diff < 2)
  {
    return 4;
  }
  else if (diff >=2 && diff < 3)
  {
    return 3;
  }
  else if (diff >= 3 && diff < 4)
  {
    return 2;
  }
  else if (diff >= 4 && diff <5)
  {
    return 1;
  }

  return 0;
}

int iOpPlaneTraining::ComputeScoreForProsthesisSize(double size,double sizeTraining)
{
  int diff = abs(size - sizeTraining);

  if (diff <= 1)
  {
    return 5;
  }
  else if (diff <= 2)
  {
    return 3;
  }
  else if (diff <= 3)
  {
    return 1;
  }

  return 0;
}
