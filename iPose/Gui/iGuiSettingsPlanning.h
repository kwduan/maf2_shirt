/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiSettingsPlanning.h,v $
Language:  C++
Date:      $Date: 2012-02-01 09:20:29 $
Version:   $Revision: 1.1.2.2 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2001/2005 
CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/

#ifndef __iGuiSettingsPlanning_H__
#define __iGuiSettingsPlanning_H__

//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "mafGUISettings.h"
#include "iGuiDefines.h"

//----------------------------------------------------------------------------
// forward reference
//----------------------------------------------------------------------------
class mafString;

/**
  Class Name : iGuiSettingsPlanning.
*/
class I_GUI_EXPORT iGuiSettingsPlanning : public mafGUISettings
{
public:
  /** constructor.*/
	iGuiSettingsPlanning(mafObserver *Listener, const mafString &label = _("Prosthesis Planning"));
  /** destructor.*/
	~iGuiSettingsPlanning();

	/** Answer to the messages coming from interface. */
	void OnEvent(mafEventBase *maf_event);

  /** Return the command line  */
  mafString GetCommandLine();

  /** Return the STL filename  */
  mafString GetFileNameSTL();

  /** Return the TXT filename  */
  mafString GetFileNameTXT();

  /** Return the input filename */
  mafString GetInputFileName();

  /** Return the working directory */
  mafString GetWorkingDirectory();

  /** Return the average BMI */
  double GetAverageBmi();

  /** Return the standard deviation BMI */
  double GetStandardDeviationBmi();

protected:
	/** Create the GUI for the setting panel.*/
	void CreateGui();

	/** Initialize the application settings.*/
	void InitializeSettings();

  mafString m_CommandLine;
  mafString m_FileNameSTL;
  mafString m_FileNameTXT;
  mafString m_FileNameInput;
  mafString m_WorkingDirectory;

  mafString m_AbsoulutePathFileNameSTL;
  mafString m_AbsoulutePathFileNameTXT;
  mafString m_AbsoulutePathFileNameInput;

  double m_AverageBmi;
  double m_StandardDeviationBmi;
};
#endif
