/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialogPlacingImplant.h,v $
Language:  C++
Date:      $Date: 2012-03-21 07:26:11 $
Version:   $Revision: 1.1.2.10 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it
=========================================================================*/

#ifndef __iGuiDialogPlacingImplant_H__
#define __iGuiDialogPlacingImplant_H__

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class medDeviceButtonsPadMouseDialog;
class mafVMERoot;
class mafGUI;
class mafGUITransformTextEntries;
class mafVMESurface;
class medGUITransformSliders;
class mafVMEGroup;
class mafViewVTK;
class mafVME;
class mafInteractorPER;
class mafInteractorSER;
class mafDeviceManager;
class mafGUITransformMouse;
class vtkActor;
class vtkPolyDataMapper;
class vtkTexture;
class vtkWindowLevelLookupTable;
class vtkPlaneSource;

//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "iGuiDefines.h"
#include "mafGUIDialog.h"

//----------------------------------------------------------------------------
// Const:
//----------------------------------------------------------------------------

/** iGuiDialogPlacingImplant
*/
class I_GUI_EXPORT iGuiDialogPlacingImplant : public mafGUIDialog
{
public:

    iGuiDialogPlacingImplant (const wxString& title,mafVMERoot *input,int side,bool training = false);
    virtual ~iGuiDialogPlacingImplant (); 

    /** process events coming from other components */
    /*virtual*/ void OnEvent(mafEventBase *maf_event);
protected:

    enum ID_GUIs
    {
        ID_CANCEL = MINID,
        ID_ACCEPT_BUTTON,
        ID_REJECT_BUTTON,
        ID_RENDERING_TYPE,
        ID_ENABLE_LATERAL_VIEW,
        MAX_ID,
    };

    enum RENDERING_TYPEs
    {
      NORMAL_RENDERING = 0,
      FLAT_RENDERING,
    };

    typedef struct 
    {
      int ID_TRANSLATE_X;      
      int ID_TRANSLATE_Y;
      int ID_TRANSLATE_Z;
      int ID_ROTATE_X;
      int ID_ROTATE_Y;
      int ID_ROTATE_Z;
    }ID_TRANSFORM_GUI;

    /** Create the interface */
    void CreateGui();

    /** Visualize the selected implant */
    void LoadImplant();

    /** visualize the first vme in the three with the tag "RX" */
    void ShowRX();

    /** visualize the first vme in the three with the tag "3D_RECONSTRUCTION" */
    void ShowReconstruction3D();

    /** visualize the first vme in the three with the tag "LND_CLOUD" */
    void ShowLandmarks();

    /** create a gui to allow the user to translate and rotate prosthesis */
    mafGUI* CreateTransformGui(mafString name,ID_TRANSFORM_GUI ids,double *posX,double *posY, double *posZ, double *rotX, double *rotY, double *rotZ);

    /** Update camera of two views */
    void CameraUpdate();

    void CreateFrontalView();

    void CreateLateralView();

    void PostMultiplyEventMatrix(mafEventBase *maf_event);

    double m_SliceBounds[6];

    mafViewVTK *m_FrontalView;
    mafViewVTK *m_LateralView;

    medDeviceButtonsPadMouseDialog *m_MouseFrontView;
    medDeviceButtonsPadMouseDialog *m_MouseLateralView;

    mafVMERoot *m_Input;

    mafGUI *m_LateralGui;
    std::vector<mafGUITransformTextEntries *> m_Transforms;
    std::vector<mafVMEGroup *> m_Prosthesis;
    std::vector<mafVMEGroup *> m_RefSys;
    std::vector<mafMatrix *> m_OldMatrix;
    std::vector<mafVME*> m_ProsthesisComponent;
    std::vector<mafGUITransformMouse*> m_ProsthesisTransform;

    vtkPolyDataMapper	*m_SliceMapper;
    vtkTexture *m_SliceTexture;
    vtkActor *m_SliceActor;
    vtkActor *m_ReconstructionFrontViewActor;
    vtkActor *m_ReconstructionLateraltViewActor;
    vtkActor *m_LandmarksActor;
    vtkWindowLevelLookupTable	*m_SliceLookupTable;
    vtkPlaneSource *m_SlicePlane;

    std::vector<ID_TRANSFORM_GUI> m_TransformIds;
    std::vector<double*> m_Positions;
    std::vector<double*> m_Rotations;
    std::vector<double*> m_Steps;

    int m_Side;
    int m_ProsthesisRenderingType;
    int m_EnableLateralView;

    wxBoxSizer *m_TopSizer;

    mafInteractorSER *m_SER;
    mafInteractorPER *m_FrontalViewPER;
    mafInteractorPER *m_LateralViewPER;
    mafDeviceManager *m_DeviceManager;


    bool m_Training;

};
#endif
