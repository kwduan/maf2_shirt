/*=========================================================================
Program:   iPose
Module:    $RCSfile: iOpPlaneTraining.h,v $
Language:  C++
Date:      $Date: 2012-03-21 07:29:03 $
Version:   $Revision: 1.1.2.11 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/

#ifndef __iOpPlaneTraining_H__
#define __iOpPlaneTraining_H__

//----------------------------------------------------------------------------
// Include :
//----------------------------------------------------------------------------
#include "mafOp.h"
#include "iGuiDialogShowRx.h"
#include "iGuiDefines.h"

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class mafGUIDialog;

//----------------------------------------------------------------------------
// iOpPlaneTraining :
//----------------------------------------------------------------------------
/**Convert a Parametric Surface into a Normal Surface*/
class I_GUI_EXPORT iOpPlaneTraining: public mafOp
{
public:

  /** constructor */
  iOpPlaneTraining(wxString label = "Plane Training");
  /** destructor */
  ~iOpPlaneTraining(); 

  /** RTTI macro */
  mafTypeMacro(iOpPlaneTraining, mafOp);

  /** Answer to the messages coming from interface. */
  /*virtual*/ void OnEvent(mafEventBase *maf_event);

  /** Return a copy of the operation.*/
  mafOp* Copy();

  /** Return true for the acceptable vme type. */
  /*virtual*/ bool Accept(mafNode* vme);

  /** Builds operation's interface by calling CreateOpDialog() method. */
  /*virtual*/ void OpRun();

  /** Execute the operation. */
  /*virtual*/ void OpDo();

protected:

  enum GUI_IDs
  {
    ID_ADD_CASE = MINID,
    ID_EXISTING_CASE,
  };

  /** compute score of training using prosthesis position */
  void ComputeScore();

  /** Implements the 3D reconstruction step */
  int Step3DReconstrution();

  /** Implements the choice implant step */
  int StepChoiceImplant();

  /** Implements the placing implant step */
  int StepPlacingImplant();

  /** Implements the validation values step */
  int StepValidationValues();

  /** Implements the select case step */
  int StepSelectCase();

  /** Implements the primary stability */
  int StepPrimaryStability();

  /** Implements the show rx step */
  int StepShowRX(int dialogMode = iGuiDialogShowRx::SHOW_RX_ONLY);

  /** This method is called at the end of the operation and result contain the wxOK or wxCANCEL. */
  /*virtual*/ void OpStop(int result);

  /** Create a dialog where the user choice to add or use an existing case */
  int GetUserChoiceAddOrUseCase();

  /** Check if some prosthesis are present in the DB */
  bool CheckIfProsthesisArePresent();

  /** return a score for a difference of measure or position */
  int ComputeScoreForDifference(double diff);

  int ComputeScoreForProsthesisSize(double size,double sizeTraining);

  bool m_AddCase;//<<<FALSE use an existing case - TRUE add a new case
  mafGUIDialog *m_AddOrUseDialog;
  int m_CurrentStep;
  int m_PreStepStored;
  int m_Side;
  int m_Joint;
  int m_TrainingScore;
  double m_PatientHeight;
  double m_PatientWeight;
  double m_PCA[35];
  double m_NeckVector[3];
  mafString m_PatientCaseId; 
  mafString m_PatientName;
  mafString m_FileRX;
  mafString m_Diagnosis;

};
#endif
