/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialogPrimaryStability.h,v $
Language:  C++
Date:      $Date: 2012-03-21 07:24:19 $
Version:   $Revision: 1.1.2.8 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it
=========================================================================*/

#ifndef __iGuiDialogPrimaryStability_H__
#define __iGuiDialogPrimaryStability_H__

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class mafRWI;
class mafDeviceButtonsPadMouse;
class mafVMERoot;
class mafGUIButton;
class vtkActor;
class vtkPolyDataMapper;
class vtkTexture;
class vtkWindowLevelLookupTable;
class vtkPlaneSource;
class vtkScalarBarActor;
class vtkTextActor;

//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "mafGUIDialog.h"

//----------------------------------------------------------------------------
// Const:
//----------------------------------------------------------------------------

/** iGuiDialogPrimaryStability
*/
class iGuiDialogPrimaryStability : public mafGUIDialog
{
public:
  
  /** Constructor to execute primary stability external app*/
  iGuiDialogPrimaryStability (mafVMERoot *input,const wxString& title,mafDeviceButtonsPadMouse *mouse,double weight,double height,double PCA[35],bool training = false);

  /** Constructor to don't execute primary stability external app*/
  iGuiDialogPrimaryStability (mafVMERoot *input,const wxString& title,mafDeviceButtonsPadMouse *mouse,double weight,double height,double stability[16] , double uncertainty,bool training = false);

  virtual ~iGuiDialogPrimaryStability (); 

  /** process events coming from other components */
  /*virtual*/ void OnEvent(mafEventBase *maf_event);

  /** execute the external application to compute primary stability */
  static bool ExecuteExternalApplication(mafVMERoot *input, double pca[35],double weight,double height,double stabilityValues[16], double &uncertainty,bool training = false);

protected:

  enum ID_GUIs
  {
    ID_CANCEL = MINID,
    ID_ACCEPT_BUTTON,
    ID_REJECT_BUTTON,
  };

  /** Create the interface */
  void CreateGui();

  /** show femural model with values mapped */
  void ShowModel();

  mafRWI *m_Rwi3DView;

  mafDeviceButtonsPadMouse *m_Mouse;

  mafGUIButton *m_ButtonAccept;

  double m_NormalizedBMI;
  double m_Uncertainty;
  double m_StabilityValues[16];
  vtkScalarBarActor *m_ScalarBar;
  vtkTextActor *m_UncertaintyActor;
  vtkTextActor *m_InfoActor;


};
#endif
