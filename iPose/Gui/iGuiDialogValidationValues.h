/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialogValidationValues.h,v $
Language:  C++
Date:      $Date: 2012-03-15 10:49:24 $
Version:   $Revision: 1.1.2.7 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it
=========================================================================*/

#ifndef __iGuiDialogValidationValues_H__
#define __iGuiDialogValidationValues_H__

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class mafGUIButton;
class mafDeviceButtonsPadMouse;
class mafVMERoot;

//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "iGuiDefines.h"
#include "mafGUIDialog.h"

//----------------------------------------------------------------------------
// Const:
//----------------------------------------------------------------------------

/** iGuiDialogValidationValues
*/
class I_GUI_EXPORT iGuiDialogValidationValues : public mafGUIDialog
{
public:

    iGuiDialogValidationValues (const wxString& title,mafDeviceButtonsPadMouse *mouse,mafVMERoot *input,int id,int jointType,double weight,double height,double PCA[35],bool training = false);
    virtual ~iGuiDialogValidationValues (); 

    /** process events coming from other components */
    /*virtual*/ void OnEvent(mafEventBase *maf_event);

    void ComputeValidation();

protected:

    enum ID_GUIs
    {
        ID_CANCEL_BUTTON = MINID,
        ID_ACCEPT_BUTTON,
        ID_REJECT_BUTTON,
        ID_SEND_BUTTON,
        ID_PRIMARY_STABILITY_BUTTON,
        ID_JOINT_LOADING_BUTTON,
        ID_UPLOAD_BUTTON,
    };

    /** Create the interface */
    void CreateGui();

    /** Use client xmlrpc to send data to OpenClinica */
    void SendDataToDB();

    /** upload current study */
    void UploadStudy();

    wxGauge *m_Gauge;

    wxStaticBitmap *m_BmpJoint;
    wxStaticBitmap *m_BmpStability;

    mafGUIButton *m_ButtonSend;
    mafGUIButton *m_ButtonAccept;
    mafGUIButton *m_ButtonCancel;
    mafGUIButton *m_ButtonReject;
    mafGUIButton *m_ButtonPrimaryStability;
    mafGUIButton *m_ButtonJointLoading;
    mafGUIButton *m_ButtonUpload;

    mafDeviceButtonsPadMouse *m_Mouse;
    double m_PatientHeight;
    double m_PatientWeight;
    double m_PCA[35];
    double m_Uncertainty;
    double m_Stability[16];
    double m_Vector[3];
    int m_Joint;
    int m_PatientId;
    mafVMERoot *m_Input;
    bool m_Training;

};
#endif
