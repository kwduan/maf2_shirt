/*=========================================================================
Program:   iPose
Module:    $RCSfile: iOpPlanningWorkflow.cpp,v $
Language:  C++
Date:      $Date: 2012-03-21 07:29:03 $
Version:   $Revision: 1.1.2.21 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/

#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iOpPlanningWorkflow.h"
#include "iDecl.h"
#include "iAttributeWorkflow.h"

#include "mafNode.h"
#include "mafGUIDialog.h"
#include "mafVMERoot.h"
#include "mafXMLString.h"
#include "mmuDOMTreeErrorReporter.h"
#include "mafXMLElement.h"
#include "mmuXMLDOMElement.h"
#include "mafTagArray.h"
#include "mafTagItem.h"

#include "iGuiDialogListCase.h"
#include "iGuiDialogShowRx.h"
#include "iGuiDialog3DReconstrution.h"
#include "iGuiDialogChoiceImplant.h"
#include "iGuiDialogPlacingImplant.h"
#include "iGuiDialogValidationValues.h"

#include "vtkMAFSmartPointer.h"
#include "vtkSphereSource.h"
#include "vtkDataSetWriter.h"
#include "vtkPolyDataWriter.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include <xercesc/framework/LocalFileInputSource.hpp>
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/sax/ErrorHandler.hpp>

#include <wx/file.h>
#include <wx/dir.h>
#include "mafVMEGroup.h"
#include "iGuiSettingsPlanning.h"



//----------------------------------------------------------------------------
mafCxxTypeMacro(iOpPlanningWorkflow);
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
iOpPlanningWorkflow::iOpPlanningWorkflow(wxString label) :
mafOp(label)
	//----------------------------------------------------------------------------
{
	m_OpType  = OPTYPE_OP;
	m_Canundo = true;
	m_InputPreserving = true;

	m_AddCase = false;
	m_FileRX = "";

	m_PatientCaseId = "";

  m_Calibration = 0.0;
}
//----------------------------------------------------------------------------
iOpPlanningWorkflow::~iOpPlanningWorkflow()
	//----------------------------------------------------------------------------
{
}
//----------------------------------------------------------------------------
mafOp* iOpPlanningWorkflow::Copy()
	//----------------------------------------------------------------------------
{
	/** return a copy of itself, needs to put it into the undo stack */
	return new iOpPlanningWorkflow(m_Label);
}
//----------------------------------------------------------------------------
bool iOpPlanningWorkflow::Accept(mafNode* vme)
	//----------------------------------------------------------------------------
{
	return vme != NULL && mafVMERoot::SafeDownCast(vme);
}
//----------------------------------------------------------------------------
void iOpPlanningWorkflow::OpRun()
	//----------------------------------------------------------------------------
{

	//Check if DB of prosthesis is empty or not
	if (!CheckIfProsthesisArePresent())
	{
		wxMessageBox(_("There aren't any prosthesis! Please fill DB using the operation : \"Add Prosthesis to DB\""));
		OpStop(OP_RUN_CANCEL);
		return;
	}

	int result = 0;

	m_PreStepStored = -1;

	/*mafVMERoot *root = mafVMERoot::SafeDownCast(m_Input->GetRoot());
	iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(root->GetAttribute("Workflow"));
	if (attribute != NULL)
	{
	m_CurrentStep = attribute->GetCurrentStep();
	}
	else
	{
	m_CurrentStep = ID_STEP_USER_CHOICE;
	}*/

	mafEventMacro(mafEvent(this,IPOSE_MSF_NEW));

	mafEvent eGetRoot;
	eGetRoot.SetSender(this);
	eGetRoot.SetId(IPOSE_GET_ROOT);
	mafEventMacro(eGetRoot);

	this->SetInput(eGetRoot.GetVme());

	m_CurrentStep = ID_STEP_USER_CHOICE;
	while (m_CurrentStep != ID_STEP_LAST && result != wxCANCEL)
	{
		m_PreStepStored = m_CurrentStep;

		if (m_CurrentStep == ID_STEP_USER_CHOICE)
		{
			result = GetUserChoiceAddOrUseCase();
			if (!m_AddCase)
			{
				m_CurrentStep = ID_STEP_SELECT_CASE_EXISTING;
			}
			else
			{
				m_CurrentStep = ID_STEP_ADD_CASE;
			}
			continue;
		}

		if (m_CurrentStep == ID_STEP_ADD_CASE)
		{
			result = StepShowRX(iGuiDialogShowRx::ADD_POSSIBILITY_TO_OPEN_RX);
			continue;
		}

		if (m_CurrentStep == ID_STEP_CALIBRATE)
		{
			result = StepShowRX(iGuiDialogShowRx::CALIBRATE_TOOL);
			continue;
		}

		if (m_CurrentStep == ID_STEP_MEASURE)
		{
			result = StepShowRX(iGuiDialogShowRx::MEASURE_TOOL);
			continue;
		}

		if (m_CurrentStep == ID_STEP_SELECT_CASE_EXISTING)
		{
			result = StepSelectCase();
			continue;
		}

		if (m_CurrentStep == ID_STEP_SHOW_RX)
		{
			result = StepShowRX(iGuiDialogShowRx::SHOW_RX_ONLY);
      StoreAttributes();
			continue;
		}

		if (m_CurrentStep == ID_STEP_3D_RECONSTRUTION_START)
		{
			result = Step3DReconstrution();
			continue;
		}
    if (m_CurrentStep == ID_STEP_3D_RECONSTRUTION_WAITING)
    {
      result = Step3DReconstrution();
      continue;
    }

    if (m_CurrentStep == ID_STEP_3D_RECONSTRUTION_SHOW)
    {
      result = Step3DReconstrution();
      continue;
    }

		if (m_CurrentStep == ID_STEP_CHOICE_IMPLANT)
		{
			result = StepChoiceImplant();
			continue;
		}
		if (m_CurrentStep == ID_STEP_PLACING_IMPLANT)
		{
			result = StepPlacingImplant();
			continue;
		}
		if (m_CurrentStep == ID_STEP_VALIDATION_VALUES)
		{
			result = StepValidationValues();
			continue;
		}
	}

	if (result == wxCANCEL)
	{
		if (m_PreStepStored>=ID_STEP_SHOW_RX)
		{
			int resultQuestion = wxMessageBox(_("Would you like to save the current state ?"),_("Save"),wxCANCEL|wxOK);
			if (resultQuestion == wxOK)
			{
				StoreAttributes();

				if(m_PreStepStored == ID_STEP_SHOW_RX)
				{
					FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_RX);
				}
				else if(m_PreStepStored == ID_STEP_3D_RECONSTRUTION_START )
				{
					FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_LND_CLOUD);
					FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_3D_RECONSTRUCTION);
				}
				else if(m_PreStepStored == ID_STEP_CHOICE_IMPLANT)
				{
					FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_DB_ACETABULAR);//remove old DB
					FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_DB);//remove old DB

					FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_GROUP);//remove old DB
					FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_GROUP);//remove old DB
				}
				else if(m_PreStepStored == ID_STEP_PLACING_IMPLANT)
				{
					//SETTARE PROTESI CON POSIZIONE DEFAULT
				}

				mafString fileName = mafGetApplicationDirectory().c_str();
				fileName<<"/cache/";
				fileName<<m_PatientCaseId;

				if(!wxDir::Exists(fileName.GetCStr()))
				{
					::wxMkDir(fileName.GetCStr());
				}

				fileName<<"/";
				fileName<<m_PatientCaseId;
				fileName<<".msf";


				mafEventMacro(mafEvent(this,MENU_FILE_SAVE,&fileName));
			}
			else
			{
				//REMOVE ALL
				/*FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_RX);
				FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_LND_CLOUD);
				FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_3D_RECONSTRUCTION);
				FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_DB_ACETABULAR);//remove old DB
				FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_DB);//remove old DB
				FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_GROUP);//remove old DB
				FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_GROUP);//remove old DB*/

				mafEventMacro(mafEvent(this,IPOSE_MSF_NEW));
			}
		}
		else
		{
			mafEventMacro(mafEvent(this,IPOSE_MSF_NEW));
		}

		OpStop(OP_RUN_CANCEL);
		return;
	}
  else
  {
    StoreAttributes();

    if(m_PreStepStored == ID_STEP_SHOW_RX)
    {
      FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_RX);
    }
    else if(m_PreStepStored == ID_STEP_3D_RECONSTRUTION_START )
    {
      FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_LND_CLOUD);
      FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_3D_RECONSTRUCTION);
    }
    else if(m_PreStepStored == ID_STEP_CHOICE_IMPLANT)
    {
      FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_DB_ACETABULAR);//remove old DB
      FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_DB);//remove old DB

      FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_GROUP);//remove old DB
      FindAndRemove(mafVMERoot::SafeDownCast(m_Input),TAG_PROSTHESIS_GROUP);//remove old DB
    }
    else if(m_PreStepStored == ID_STEP_PLACING_IMPLANT)
    {
      //SETTARE PROTESI CON POSIZIONE DEFAULT
    }

    mafString fileName = mafGetApplicationDirectory().c_str();
    fileName<<"/cache/";
    fileName<<m_PatientCaseId;

    if(!wxDir::Exists(fileName.GetCStr()))
    {
      ::wxMkDir(fileName.GetCStr());
    }

    fileName<<"/";
    fileName<<m_PatientCaseId;
    fileName<<".msf";


    mafEventMacro(mafEvent(this,MENU_FILE_SAVE,&fileName));
  }


	OpStop(OP_RUN_OK);
}
//----------------------------------------------------------------------------
void iOpPlanningWorkflow::OpDo()   
	//----------------------------------------------------------------------------
{
	if (m_Output)
	{
		m_Output->ReparentTo(m_Input);
		mafEventMacro(mafEvent(this,CAMERA_UPDATE));
	}
}
//----------------------------------------------------------------------------
void iOpPlanningWorkflow::OpStop(int result)   
	//----------------------------------------------------------------------------
{
	mafEventMacro(mafEvent(this,result));
}
//----------------------------------------------------------------------------
void iOpPlanningWorkflow::OnEvent(mafEventBase *maf_event)
	//----------------------------------------------------------------------------
{
	if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
	{
		switch(e->GetId())
		{	
		case wxOK:
			OpStop(OP_RUN_OK);        
			break;
		case wxCANCEL:
			OpStop(OP_RUN_CANCEL);        
			break;
		case ID_EXISTING_CASE:
			m_AddCase = false;
			m_AddOrUseDialog->EndModal(wxID_OK);
			break;
		case ID_ADD_CASE:
			m_AddCase = true;
			m_AddOrUseDialog->EndModal(wxID_OK);
			break;;
		default:
			mafEventMacro(*e);
			break;
		}
	}
	else
	{
		mafEventMacro(*e);
	}
}
//----------------------------------------------------------------------------
int iOpPlanningWorkflow::GetUserChoiceAddOrUseCase()
	//----------------------------------------------------------------------------
{
	m_AddOrUseDialog = new mafGUIDialog(_("Prosthesis Planning"),mafCLOSEWINDOW);

	mafGUI *gui = new mafGUI(this);

	gui->Button(ID_ADD_CASE,_("Add Case"));
	gui->Button(ID_EXISTING_CASE,_("Use Existing Case"));

	m_AddOrUseDialog->Add(gui);

	m_AddOrUseDialog->ShowModal();

	cppDEL(m_AddOrUseDialog);

	return MAF_OK;
}
//----------------------------------------------------------------------------
int iOpPlanningWorkflow::Step3DReconstrution()
//----------------------------------------------------------------------------
{
	iGuiDialog3DReconstrution *dialog3DReconstrution = new iGuiDialog3DReconstrution(_("Prosthesis Planning - 3D Reconstruction"),m_Mouse,mafVMERoot::SafeDownCast(m_Input),m_FileRX,m_CurrentStep,atoi(m_PatientCaseId.GetCStr()));

	int x_pos,y_pos,w,h;
	mafGetFrame()->GetPosition(&x_pos,&y_pos);
	dialog3DReconstrution->GetSize(&w,&h);
	dialog3DReconstrution->SetSize(x_pos+5,y_pos+5,w,h);
	dialog3DReconstrution->SetMinSize(wxSize(800,600));
	dialog3DReconstrution->Show(true);
	mafYield();

	if (m_CurrentStep == ID_STEP_3D_RECONSTRUTION_START)
	{
		dialog3DReconstrution->ComputeReconstrution();
    m_PreStepStored = ID_STEP_3D_RECONSTRUTION_WAITING;
    m_CurrentStep++;
	}

  if (m_CurrentStep == ID_STEP_3D_RECONSTRUTION_WAITING)
  {
    dialog3DReconstrution->EnableWaitingModalityGui();
  }

  if (m_CurrentStep == ID_STEP_3D_RECONSTRUTION_SHOW)
  {
    dialog3DReconstrution->LoadAndShowLandmarks();
    dialog3DReconstrution->LoadAndShowReconstruction();
    dialog3DReconstrution->EnableShowModalityGui();
    dialog3DReconstrution->GetPCA(m_PCA);
    dialog3DReconstrution->GetNeckVector(m_NeckVector);
  }

	int result = dialog3DReconstrution->ShowModal();

	if (result == wxID_RETURN_NEXT)
	{
    if (m_CurrentStep == ID_STEP_3D_RECONSTRUTION_WAITING)
    {
      m_CurrentStep = ID_STEP_3D_RECONSTRUTION_SHOW;
      dialog3DReconstrution->GetPCA(m_PCA);
      dialog3DReconstrution->GetNeckVector(m_NeckVector);
    }
		m_CurrentStep++;
	}
	else if (result == wxID_RETURN_PREV)
	{
    if (m_CurrentStep == ID_STEP_3D_RECONSTRUTION_SHOW)
    {
      m_CurrentStep--;//Should skip steps START AND WAITING
    }
    m_CurrentStep--;
		m_CurrentStep--;//Should skip step START
	}
	else
	{
		m_CurrentStep = ID_STEP_LAST;
	}

	cppDEL(dialog3DReconstrution);

	return result;
}
//----------------------------------------------------------------------------
int iOpPlanningWorkflow::StepChoiceImplant()
	//----------------------------------------------------------------------------
{
	iGuiDialogChoiceImplant *dialogChoiceImplant = new iGuiDialogChoiceImplant(_("Prosthesis Planning - Choice Implant"),mafVMERoot::SafeDownCast(m_Input),m_Joint,m_Side,m_NeckVector);
	int result = dialogChoiceImplant->ShowModal();

	if (result == wxID_RETURN_NEXT)
	{
		m_CurrentStep++;
	}
	else if (result == wxID_RETURN_PREV)
	{
		m_CurrentStep--;
	}
	else
	{
		m_CurrentStep = ID_STEP_LAST;
	}

	cppDEL(dialogChoiceImplant);

	return result;
}
//----------------------------------------------------------------------------
int iOpPlanningWorkflow::StepPlacingImplant()
	//----------------------------------------------------------------------------
{
	iGuiDialogPlacingImplant *dialogPlacingImplant = new iGuiDialogPlacingImplant(_("Prosthesis Planning - Placing Implant"),mafVMERoot::SafeDownCast(m_Input),m_Side);
  int result = dialogPlacingImplant->ShowModal();

	if (result == wxID_RETURN_NEXT)
	{
		m_CurrentStep++;
	}
	else if (result == wxID_RETURN_PREV)
	{
		m_CurrentStep--;
	}
	else
	{
		m_CurrentStep = ID_STEP_LAST;
	}

	cppDEL(dialogPlacingImplant);

	return result;
}
//----------------------------------------------------------------------------
int iOpPlanningWorkflow::StepValidationValues()
	//----------------------------------------------------------------------------
{
	iGuiDialogValidationValues *dialogValidationValues = new iGuiDialogValidationValues(_("Prosthesis Planning - Validation Values"),m_Mouse,mafVMERoot::SafeDownCast(m_Input),atoi(m_PatientCaseId),m_Joint,m_PatientWeight,m_PatientHeight,m_PCA);

  dialogValidationValues->SetListener(this);

	int x_pos,y_pos,w,h;
	mafGetFrame()->GetPosition(&x_pos,&y_pos);
	dialogValidationValues->GetSize(&w,&h);
	dialogValidationValues->SetSize(x_pos+5,y_pos+5,w,h);
	dialogValidationValues->SetMinSize(wxSize(355,180));
	dialogValidationValues->Show(true);
	mafYield();

	dialogValidationValues->ComputeValidation();
	dialogValidationValues->Show(false);

	int result = dialogValidationValues->ShowModal();

	if (result == wxID_RETURN_NEXT)
	{
		m_CurrentStep++;
	}
	else if (result == wxID_RETURN_PREV)
	{
		m_CurrentStep--;
	}
	else if (result == wxID_RESTART_PLANNING)
	{
		m_CurrentStep = ID_STEP_USER_CHOICE;

		mafString fileName = mafGetApplicationDirectory().c_str();
		fileName<<"/cache/";
		fileName<<m_PatientCaseId;

		if(!wxDir::Exists(fileName.GetCStr()))
		{
			::wxMkDir(fileName.GetCStr());
		}

		fileName<<"/";
		fileName<<m_PatientCaseId;
		fileName<<".msf";


		mafEventMacro(mafEvent(this,MENU_FILE_SAVE,&fileName));
		mafEventMacro(mafEvent(this,IPOSE_MSF_NEW));

	}
	else
	{
		m_CurrentStep = ID_STEP_LAST;
	}

	cppDEL(dialogValidationValues);

	return result;
}
//----------------------------------------------------------------------------
int iOpPlanningWorkflow::StepSelectCase()
	//----------------------------------------------------------------------------
{
	iGuiDialogListCase *listCasesDialog = new iGuiDialogListCase(_("Prosthesis Planning"));
	int result = listCasesDialog->ShowModal();

	//Reset old selection
	m_Joint = -1;
	m_Side = -1;
	m_PatientCaseId = "";
	m_PatientName = "";
	m_FileRX = "";
	m_Diagnosis = "";
  m_PatientWeight = 0.0;
  m_PatientHeight = 0.0;
  for (int i=0;i<35;i++)
  {
    m_PCA[i] = 0.0;
  }
	if (result == wxID_RETURN_NEXT)
	{
		m_PatientCaseId = listCasesDialog->GetCaseId();
		m_FileRX = listCasesDialog->GetFileNameCaseSelected();
		m_Side = listCasesDialog->GetSideCaseSelected();
		m_Joint = listCasesDialog->GetJointCaseSelected();
		m_Diagnosis = listCasesDialog->GetDiagnosisCaseSelected();
		m_PatientName = listCasesDialog->GetPatientNameCaseSelected();
    m_PatientWeight = listCasesDialog->GetWeightCaseSelected();
    m_PatientHeight = listCasesDialog->GetHeightCaseSelected();

		m_CurrentStep = ID_STEP_SHOW_RX;
	}
	else if (result == wxID_RETURN_ADD_CASE)
	{
		m_CurrentStep = ID_STEP_ADD_CASE;
		return result;
	}
	else
	{
		m_CurrentStep = ID_STEP_LAST;
		return result;
	}

	cppDEL(listCasesDialog);

	mafString fileName = mafGetApplicationDirectory().c_str();
	fileName<<"/cache/";
	fileName<<m_PatientCaseId;
	fileName<<"/";
	fileName<<m_PatientCaseId;
	fileName<<".msf";

	if(wxFileExists(fileName.GetCStr()) )
	{

		mafEventMacro(mafEvent(this,IPOSE_MSF_NEW));
		mafEventMacro(mafEvent(this,MENU_FILE_OPEN,&fileName));

		mafEvent eGetRoot;
		eGetRoot.SetSender(this);
		eGetRoot.SetId(IPOSE_GET_ROOT);
		mafEventMacro(eGetRoot);

		this->SetInput(eGetRoot.GetVme());

		mafVMERoot *root = mafVMERoot::SafeDownCast(m_Input->GetRoot());
    iAttributeWorkflow *attribute = NULL;
    mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(root,TAG_GROUP_ATTRIBUTES));
    attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));
		if (attribute != NULL)
		{
			m_CurrentStep = attribute->GetCurrentStep();
			m_Joint = attribute->GetJoint();
			m_Side = attribute->GetSide();
			m_PatientName = attribute->GetPatientName();
      attribute->GetPCA(m_PCA);
      attribute->GetNeckVector(m_NeckVector);
      m_PatientWeight = attribute->GetPatientWeight();
      m_PatientHeight = attribute->GetPatientHeight();
		}
	}

	return result;
}
//----------------------------------------------------------------------------
int iOpPlanningWorkflow::StepShowRX( int dialogMode /*= DEFAULT*/ )
//----------------------------------------------------------------------------
{
	iGuiDialogShowRx *dialogShowRx = NULL;
	mafString currentRXABSFileName = "";
	wxString dialogText = "Prosthesis Planning - RX View";

	// se mostriamo solo la RX
	if (dialogMode == iGuiDialogShowRx::SHOW_RX_ONLY && m_FileRX != "")
	{
		currentRXABSFileName = mafGetApplicationDirectory().c_str();
		currentRXABSFileName<<"/cache/";
    currentRXABSFileName<<m_PatientCaseId;
    currentRXABSFileName<<"/";
		currentRXABSFileName<<m_FileRX;
	}

	if (dialogMode == iGuiDialogShowRx::CALIBRATE_TOOL)
	{
		dialogText = "Prosthesis Planning - Calibration Tool";
	}

	if (dialogMode == iGuiDialogShowRx::MEASURE_TOOL)
	{
		dialogText = "Prosthesis Planning - Measuring Tool";
	}

	const wxString constCastDialogText = dialogText;

	dialogShowRx = new iGuiDialogShowRx(constCastDialogText , m_Mouse,mafVMERoot::SafeDownCast(m_Input),dialogMode,currentRXABSFileName, 
		m_PatientCaseId);

	if (dialogMode == iGuiDialogShowRx::SHOW_RX_ONLY || dialogMode == iGuiDialogShowRx::CALIBRATE_TOOL)
	{
		dialogShowRx->SetPatientName(m_PatientName);
		dialogShowRx->SetSide(m_Side);
		dialogShowRx->SetJoint(m_Joint);
		dialogShowRx->SetDiagnosis(m_Diagnosis);
    dialogShowRx->SetPatientHeight(m_PatientHeight);
    dialogShowRx->SetPatientWeight(m_PatientWeight);
	}

	int result = dialogShowRx->ShowModal();

	if (dialogMode == iGuiDialogShowRx::SHOW_RX_ONLY)
	{
		m_PatientName = dialogShowRx->GetPatientName();
		m_Side = dialogShowRx->GetSide();
		m_Joint = dialogShowRx->GetJoint();
		m_Diagnosis = dialogShowRx->GetDiagnosis();
    m_PatientWeight = dialogShowRx->GetPatientWeight();
    m_PatientHeight = dialogShowRx->GetPatientHeight();

		if (result == wxID_RETURN_NEXT)
		{
			m_CurrentStep++;
		}
		else if (result == wxID_RETURN_PREV)
		{
			m_CurrentStep = ID_STEP_SELECT_CASE_EXISTING;
		}
		else
		{
			m_CurrentStep = ID_STEP_LAST;
		}
	}
	else if (dialogMode == iGuiDialogShowRx::ADD_POSSIBILITY_TO_OPEN_RX)// aggiungi una nuova rx
	{

		if (result == wxID_RETURN_PREV)
		{
			m_CurrentStep = ID_STEP_SELECT_CASE_EXISTING;
		}
		else if (result == wxID_RETURN_NEXT)
		{
			m_CurrentStep = ID_STEP_CALIBRATE;
		}
		else
		{
			m_CurrentStep = ID_STEP_LAST;
		}
	}
	else if (dialogMode == iGuiDialogShowRx::CALIBRATE_TOOL)
	{
    /*m_Calibration = dialogShowRx->GetCalibration();*/

		if (result == wxID_RETURN_PREV)
		{
			m_CurrentStep = ID_STEP_ADD_CASE;
		}
		else if (result == wxID_RETURN_NEXT)
		{
			m_CurrentStep = ID_STEP_MEASURE;
		}
		else
		{
			m_CurrentStep = ID_STEP_LAST;
		}
	}
	else if (dialogMode == iGuiDialogShowRx::MEASURE_TOOL)
	{
		if (result == wxID_RETURN_PREV)
		{
			m_CurrentStep = ID_STEP_CALIBRATE;
		}
		else if (result == wxID_RETURN_NEXT)
		{
			m_CurrentStep = ID_STEP_SELECT_CASE_EXISTING;
		}
		else
		{
			m_CurrentStep = ID_STEP_LAST;
		}
	}


	cppDEL(dialogShowRx);

	return result;
}
//----------------------------------------------------------------------------
bool iOpPlanningWorkflow::CheckIfProsthesisArePresent()
//----------------------------------------------------------------------------
{
	bool result = false;

	//Open the file xml with manufacture and model information
	try {
		XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Initialize();
	}
	catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
		// Do your failure processing here
		return MAF_ERROR;
	}

	XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser *XMLParser = new XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser;

	XMLParser->setValidationScheme(XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser::Val_Auto);
	XMLParser->setDoNamespaces(false);
	XMLParser->setDoSchema(false);
	XMLParser->setCreateEntityReferenceNodes(false);

	mmuDOMTreeErrorReporter *errReporter = new mmuDOMTreeErrorReporter();
	XMLParser->setErrorHandler(errReporter);

	mafString FileName = mafGetApplicationDirectory().c_str();
	FileName<<PATH_PROSTHESIS_DB_FILE;

	if(!wxFile::Exists(FileName.GetCStr()))
	{
		mafLogMessage(_("DB file don't exist"));
		return false;
	}

	try {
		XMLParser->parse(FileName.GetCStr());
		int errorCount = XMLParser->getErrorCount(); 

		if (errorCount != 0)
		{
			// errors while parsing...
			mafErrorMessage("Errors while parsing XML file");
		}
		else
		{
			// extract the root element and wrap inside a mafXMLElement
			XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument *doc = XMLParser->getDocument();
			XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *root = doc->getDocumentElement();
			assert(root);

			mafString name = mafXMLString(root->getTagName());
			if (name == "DB")
			{
				mafString version = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)root)->getAttribute(mafXMLString("Version")));

				//Check the DB version
				if (version != PROSTHESIS_DB_VERSION)
				{
					return MAF_ERROR;
				}
			}

			XERCES_CPP_NAMESPACE_QUALIFIER DOMNodeList *manufacturesChildren=root->getChildNodes();

			if (manufacturesChildren->getLength() > 1)
			{
				result = true;
			}
		}

	}
	catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
		return false;
	}
	catch (const XERCES_CPP_NAMESPACE_QUALIFIER DOMException& toCatch) {
		return false;
	}
	catch (...) {
		return false;
	}

	cppDEL (errReporter);
	delete XMLParser;

	// terminate the XML library
	XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Terminate();

	return result;
}
//----------------------------------------------------------------------------
void iOpPlanningWorkflow::StoreAttributes()
//----------------------------------------------------------------------------
{
  iAttributeWorkflow *attribute = NULL;
  mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(mafVMERoot::SafeDownCast(m_Input),TAG_GROUP_ATTRIBUTES));
  if (groupAttributes == NULL)
  {
    mafNEW(groupAttributes);
    groupAttributes->GetTagArray()->SetTag(TAG_GROUP_ATTRIBUTES,"");
    groupAttributes->SetName("Attributes");
    groupAttributes->ReparentTo(m_Input);
    groupAttributes->Update();

    attribute = iAttributeWorkflow::SafeDownCast(m_Input->GetAttribute("Workflow"));//RetrocompatibilitÓ
  }
  else
  {
    attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));
  }
  
  if (attribute != NULL)
  {
    attribute->SetCurrentStep(m_PreStepStored);
  }
  else
  {
    attribute = iAttributeWorkflow::New();
    attribute->SetCurrentStep(m_PreStepStored);
  }

  /************************************************************************/
  /* Reading measures                                                     */
  /************************************************************************/
  wxString measuresFileName = mafGetApplicationDirectory().c_str();
  measuresFileName<<"/cache/";
  measuresFileName<<m_PatientCaseId;
  measuresFileName<<"/";
  measuresFileName += RX_MEASURES_FILENAME;
  std::map<wxString,double> measures;
  int result = ReadMeasures(measuresFileName,measures);

  if (result == MAF_ERROR)
  {
    mafLogMessage(_("Error during reading of measures"));
  }
  /************************************************************************/
  /* End Reading measures                                                 */
  /************************************************************************/

  attribute->SetHipAnteversion(measures["anteversion (deg)"]);
  attribute->SetHipCCD(measures["ccd (deg)"]);
  attribute->SetHipFemoralHead(measures["femoral head (mm)"]);
  attribute->SetHipFemoralCanal(measures["femoral canal (mm)"]);
  attribute->SetHipNeckLength(measures["neck length (mm)"]);

  mafVME *rx = FindVmeByTag(mafVMERoot::SafeDownCast(m_Input->GetRoot()),TAG_RX);
  mafTagItem *calibrationTagItem = rx->GetTagArray()->GetTag("TAG_RX_CALIBRATION_SPACING");

  if (calibrationTagItem != NULL)
  {
    m_Calibration = calibrationTagItem->GetComponentAsDouble(0);
  }

  attribute->SetCalibration(m_Calibration);

  attribute->SetPatientName(m_PatientName);
  attribute->SetJoint(m_Joint);
  attribute->SetSide(m_Side);
  attribute->SetPatientHeight(m_PatientHeight);
  attribute->SetPatientWeight(m_PatientWeight);
  attribute->SetPCA(m_PCA);
  attribute->SetNeckVector(m_NeckVector);

  iGuiSettingsPlanning *setting = new iGuiSettingsPlanning(NULL);
  double avrBmi = setting->GetAverageBmi();
  double sdBmi = setting->GetStandardDeviationBmi();
  attribute->SetAvrBMI(avrBmi);
  attribute->SetStdBMI(sdBmi);
  cppDEL(setting);


  groupAttributes->SetAttribute("Workflow",attribute);
}
