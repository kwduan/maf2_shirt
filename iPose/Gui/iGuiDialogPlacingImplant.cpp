/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialogPlacingImplant.cpp,v $
Language:  C++
Date:      $Date: 2012-03-22 07:12:24 $
Version:   $Revision: 1.1.2.16 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/


#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iGuiDialogPlacingImplant.h"
#include "iDecl.h"

#include "mafGUIButton.h"
#include "mafGUIValidator.h"
#include "mafRWI.h"
#include "mafDeviceButtonsPadMouse.h"
#include "mafVMERoot.h"
#include "mafVMEImage.h"
#include "mafTagArray.h"
#include "mafNodeIterator.h"
#include "mafVMESurface.h"
#include "mafVMEGroup.h"
#include "mafPipeSurface.h"
#include "mafGUITransformTextEntries.h"
#include "medGUITransformSliders.h"
#include "mafGUICrossIncremental.h"
#include "mafTransform.h"
#include "mmaMaterial.h"
#include "mafVMELandmarkCloud.h"
#include "mafGUITransformMouse.h"
#include "mafPipeSurface.h"
#include "mafSceneGraph.h"
#include "mafSceneNode.h"
#include "mafViewVTK.h"
#include "mafInteractorPER.h"
#include "mafInteractorSER.h"
#include "mafDeviceManager.h"
#include "mafInteractionFactory.h"
#include "medDeviceButtonsPadMouseDialog.h"
#include "mafVMELandmark.h"
#include "mafVMESurface.h"
#include "mafRefSys.h"

#include "vtkMAFSmartPointer.h"
#include "vtkRenderWindow.h"
#include "vtkTexture.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkWindowLevelLookupTable.h"
#include "vtkPlaneSource.h"
#include "vtkRenderer.h"
#include "vtkImageData.h"
#include "vtkPolyData.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkTransform.h"
#include "vtkCamera.h"
#include "vtkProperty.h"
#include "vtkMAFExtendedGlyph3D.h"
#include "vtkSphereSource.h"
#include "vtkProperty.h"

#define NUM_IDS_FOR_GUI_TRANSFORM 6

//----------------------------------------------------------------------------
iGuiDialogPlacingImplant::iGuiDialogPlacingImplant(const wxString& title,mafVMERoot *input,int side,bool training /* = false */)
: mafGUIDialog(title,mafCLOSEWINDOW)
//----------------------------------------------------------------------------
{
  m_SER = NULL;
  m_LateralViewPER = NULL;
  m_FrontalViewPER = NULL;
  m_DeviceManager = NULL;  
  m_MouseFrontView = NULL;
  m_MouseLateralView = NULL;
  m_Input = input;
  m_Side = side;
  m_SliceActor = NULL;
  m_SliceMapper = NULL;
  m_SliceTexture = NULL;
  m_SliceLookupTable = NULL;
  m_ReconstructionFrontViewActor = NULL;
  m_LandmarksActor = NULL;
  m_TopSizer = NULL;
  m_EnableLateralView = FALSE;
  m_ProsthesisRenderingType = FALSE;
  m_FrontalView = NULL;
  m_SlicePlane = NULL;
  m_LateralView = NULL;
  m_Training = training;

  int x_pos,y_pos,w,h;
  mafGetFrame()->GetPosition(&x_pos,&y_pos);
  this->GetSize(&w,&h);
  this->SetSize(x_pos+5,y_pos+5,w,h);

  this->SetSize(640, 480);

  CreateFrontalView();
  CreateLateralView();

  CreateGui();

  ShowRX();
  ShowReconstruction3D();
  ShowLandmarks(); 
  LoadImplant();
}
//----------------------------------------------------------------------------
iGuiDialogPlacingImplant::~iGuiDialogPlacingImplant()
//----------------------------------------------------------------------------
{
  for (int i=0;i<m_ProsthesisComponent.size();i++)
  {
    m_FrontalView->VmeShow(m_ProsthesisComponent[i],false);
    m_LateralView->VmeShow(m_ProsthesisComponent[i],false);
  }

  m_FrontalView->GetFrontRenderer()->RemoveActor(m_LandmarksActor);
  vtkDEL(m_LandmarksActor);
  m_FrontalView->GetFrontRenderer()->RemoveActor(m_ReconstructionFrontViewActor);
  m_LateralView->GetFrontRenderer()->RemoveActor(m_ReconstructionFrontViewActor);
  vtkDEL(m_ReconstructionFrontViewActor);
  vtkDEL(m_SlicePlane);
  m_FrontalView->GetFrontRenderer()->RemoveActor(m_SliceActor);
  m_LateralView->GetFrontRenderer()->RemoveActor(m_SliceActor);
  vtkDEL(m_SliceActor);
  vtkDEL(m_SliceMapper);
  vtkDEL(m_SliceTexture);
  vtkDEL(m_SliceLookupTable);

  for (int i=0;i<m_Transforms.size();i++)
  {
    cppDEL(m_Transforms[i]);
  }

  for (int i=0;i<m_OldMatrix.size();i++)
  {
    mafDEL(m_OldMatrix[i]);
  }

  for (int i=0;i<m_ProsthesisTransform.size();i++)
  {
    cppDEL(m_ProsthesisTransform[i]);
  }

  cppDEL(m_LateralView);
  cppDEL(m_FrontalView);

  mafDEL(m_FrontalViewPER);
  mafDEL(m_LateralViewPER);
  mafDEL(m_DeviceManager);

  for (int i=0;i<m_RefSys.size();i++)
  {
    m_RefSys[i]->ReparentTo(NULL);
    mafDEL(m_RefSys[i]);
  }

}
//----------------------------------------------------------------------------
void iGuiDialogPlacingImplant::OnEvent(mafEventBase *maf_event)
//----------------------------------------------------------------------------
{
  if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
  {
    if (e->GetId()>= MAX_ID )//A transform as been applied
    {
      for(int i=0;i<m_TransformIds.size();++i)
      {
        bool pipeUpdate = false;
        ID_TRANSFORM_GUI ids = m_TransformIds[i];
        if(e->GetId() == ids.ID_TRANSLATE_X || e->GetId() == ids.ID_TRANSLATE_Y || e->GetId() == ids.ID_TRANSLATE_Z)
        {
          pipeUpdate = true;
          double *pos;
          mafMatrix *m = m_Prosthesis[i]->GetOutput()->GetAbsMatrix();

          pos = m_Positions[i];

          mafTransform::SetPosition(*m,pos);

          m_Prosthesis[i]->SetAbsMatrix(*m);
        }
        else if (e->GetId() == ids.ID_ROTATE_X || e->GetId() == ids.ID_ROTATE_Y || e->GetId() == ids.ID_ROTATE_Z)
        {
          pipeUpdate = true;
          double *rot;
          mafMatrix *m = m_Prosthesis[i]->GetOutput()->GetAbsMatrix();

          rot = m_Rotations[i];

          mafTransform::SetOrientation(*m,rot);

          m_Prosthesis[i]->SetAbsMatrix(*m);
        }

        if (pipeUpdate)
        {
	        mafNodeIterator *iterProsthesis = m_Prosthesis[i]->NewIterator();
	        int index = 0;
	        for (mafNode *prosthesisChild = iterProsthesis->GetFirstNode();prosthesisChild;prosthesisChild = iterProsthesis->GetNextNode())
	        {
	          if (prosthesisChild->GetTagArray()->IsTagPresent(TAG_PROSTHESIS_COMPONENT))
	          {
	            mafVMESurface *implantComponet = mafVMESurface::SafeDownCast(prosthesisChild);
	
	            vtkPolyData *implantVTK = vtkPolyData::SafeDownCast(implantComponet->GetOutput()->GetVTKData());
	            implantVTK->Update();
	
	            vtkMAFSmartPointer<vtkTransform> tr;
	            tr->SetMatrix(implantComponet->GetOutput()->GetAbsMatrix()->GetVTKMatrix());
	
	            vtkMAFSmartPointer<vtkTransformPolyDataFilter> tp;
	            tp->SetInput(implantVTK);
	            tp->SetTransform(tr);           
	            tp->Update();
	
	            index++;
	          }
	        }
        }
        
        CameraUpdate();

      }
    }
    switch(e->GetId())
    {
    case ID_TRANSFORM:
      {
        PostMultiplyEventMatrix(maf_event);
      }
      break;
    case ID_ENABLE_LATERAL_VIEW:
      {

        m_TopSizer->Remove(m_FrontalView->m_Rwi->m_RwiBase);
        m_FrontalView->m_Rwi->m_RwiBase->Reparent(NULL);
        m_TopSizer->Remove(m_LateralView->m_Rwi->m_RwiBase);
        m_LateralView->m_Rwi->m_RwiBase->Reparent(NULL);
        m_LateralView->m_Rwi->m_RwiBase->Show(false);
        m_TopSizer->Remove(m_LateralGui);

        m_FrontalView->m_Rwi->m_RwiBase->Reparent(this);
        m_TopSizer->Add(m_FrontalView->m_Rwi->m_RwiBase,0,wxALL,5);
        if (m_EnableLateralView == TRUE)
        {
          m_LateralView->m_Rwi->m_RwiBase->Reparent(this);
          m_LateralView->m_Rwi->m_RwiBase->Show(true);
          m_TopSizer->Add(m_LateralView->m_Rwi->m_RwiBase,0,wxALL,5);

          mafVMESurface *reconstruction = mafVMESurface::SafeDownCast(FindVmeByTag(m_Input,TAG_3D_RECONSTRUCTION));
          reconstruction->Update();
          double b[6];
          reconstruction->GetOutput()->GetBounds(b);
          m_LateralView->CameraReset();
          m_LateralView->CameraUpdate();

          m_FrontalView->CameraUpdate();
        }
        m_TopSizer->Add(m_LateralGui,0);

        if (m_EnableLateralView == TRUE)
        {
          this->SetSize(wxSize(1000,560));
          this->OnSize(wxSizeEvent(wxSize(1000,560)));
        }
        else
        {
          this->SetSize(wxSize(720,560));
          this->OnSize(wxSizeEvent(wxSize(720,560)));
        }

        this->Update();
      }
      break;
    case ID_RENDERING_TYPE:
      {
        for (int i=0;i<m_ProsthesisComponent.size();++i)
        {
          if (m_ProsthesisRenderingType == FLAT_RENDERING)
          {
            mafVMESurface::SafeDownCast(m_ProsthesisComponent[i])->GetMaterial()->m_AmbientIntensity = 1.0;
            mafVMESurface::SafeDownCast(m_ProsthesisComponent[i])->GetMaterial()->UpdateProp();
          }
          else if (m_ProsthesisRenderingType == NORMAL_RENDERING)
          {
            mafVMESurface::SafeDownCast(m_ProsthesisComponent[i])->GetMaterial()->m_AmbientIntensity = 0.0;
            mafVMESurface::SafeDownCast(m_ProsthesisComponent[i])->GetMaterial()->UpdateProp();
          }
        }
        CameraUpdate();
      }
      break;
    case ID_CANCEL:
      {
        //Remove all prosthesis if cancel button is pressed
        /*for (int i=0;i<m_Prosthesis.size();i++)
        {
          m_Prosthesis[i]->ReparentTo(NULL);
        }
        FindAndRemove(m_Input,TAG_RX);
        FindAndRemove(m_Input,TAG_3D_RECONSTRUCTION);
        FindAndRemove(m_Input,TAG_LND_CLOUD);*/

        for(int i=0;i<m_Prosthesis.size();i++)
        {
          mafMatrix identity;
          identity.Identity();
          m_Prosthesis[i]->SetAbsMatrix(*m_OldMatrix[i]);
        }


        this->EndModal(wxCANCEL);
      }
      break;
    case ID_ACCEPT_BUTTON:
        this->EndModal(wxID_RETURN_NEXT);
        break;
    case ID_REJECT_BUTTON:
        this->EndModal(wxID_RETURN_PREV);
        break;
    default:
        mafEventMacro(*e);
    }
  }
}
//----------------------------------------------------------------------------
void iGuiDialogPlacingImplant::CreateGui()
//----------------------------------------------------------------------------
{
  wxPoint p = wxDefaultPosition;

  m_LateralGui = new mafGUI(this);
  m_LateralGui->Reparent(this);
    
  wxString choices[2] = {_("Normal"),_("Flat")};
  m_LateralGui->Combo(ID_RENDERING_TYPE,_("Rendering"),&m_ProsthesisRenderingType,2,choices);
  m_LateralGui->Bool(ID_ENABLE_LATERAL_VIEW,_("Lateral View"),&m_EnableLateralView,1);

  m_TopSizer = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);

  mafGUIButton *b_accept = new mafGUIButton(this, ID_ACCEPT_BUTTON,_("accept"), p, wxSize(100,20));
  b_accept->SetValidator(mafGUIValidator(this,ID_ACCEPT_BUTTON,b_accept));
  mafGUIButton *b_cancel = new mafGUIButton(this, ID_CANCEL,_("cancel"), p, wxSize(100,20));
  b_cancel->SetValidator(mafGUIValidator(this,ID_CANCEL,b_cancel));
  mafGUIButton *b_reject = new mafGUIButton(this, ID_REJECT_BUTTON,_("reject"), p, wxSize(100,20));
  b_reject->SetValidator(mafGUIValidator(this,ID_REJECT_BUTTON,b_reject));

  m_FrontalView->m_Rwi->m_RwiBase->Reparent(this);
  m_TopSizer->Add(m_FrontalView->m_Rwi->m_RwiBase,0,wxALL,5);
  if (m_EnableLateralView == TRUE)
  {
    m_LateralView->m_Rwi->m_RwiBase->Reparent(this);
    m_LateralView->m_Rwi->m_RwiBase->Show(true);
    m_TopSizer->Add(m_LateralView->m_Rwi->m_RwiBase,0,wxALL,5);
  }
  m_TopSizer->Add(m_LateralGui,0);

  buttonSizer->Add(b_reject,0);
  buttonSizer->Add(b_cancel,0);
  buttonSizer->Add(b_accept,0);

  m_GuiSizer->Add(m_TopSizer,0,wxCENTRE,5);
  m_GuiSizer->Add(buttonSizer,0,wxCENTRE,5);

  if (m_EnableLateralView == TRUE)
  {
    this->SetSize(wxSize(1000,560));
    this->OnSize(wxSizeEvent(wxSize(1000,560)));
  }
  else
  {
    this->SetSize(wxSize(720,560));
    this->OnSize(wxSizeEvent(wxSize(720,560)));
  }

  this->Update();
}
//----------------------------------------------------------------------------
void iGuiDialogPlacingImplant::LoadImplant()
//----------------------------------------------------------------------------
{
  //Create the device manager
  mafNEW(m_DeviceManager);
  m_DeviceManager->SetListener(this);
  m_DeviceManager->SetName("DialogDeviceManager");
  m_DeviceManager->Initialize();

  //Create the static event router and connect it  
  mafNEW(m_SER);
  m_SER->SetName("StaticEventRouter");
  m_SER->SetListener(this);

  //Create a Mouse device
  mafPlugDevice<medDeviceButtonsPadMouseDialog>("Mouse");
  m_MouseFrontView = (medDeviceButtonsPadMouseDialog *)m_DeviceManager->AddDevice("medDeviceButtonsPadMouseDialog",false); // add as persistent device
  assert(m_MouseFrontView);
  m_MouseFrontView->SetName("DialogMouseFront");
  m_MouseLateralView = (medDeviceButtonsPadMouseDialog *)m_DeviceManager->AddDevice("medDeviceButtonsPadMouseDialog",false); // add as persistent device
  assert(m_MouseLateralView);
  m_MouseLateralView->SetName("DialogMouseLateral");

  //Define the action for pointing and manipulating
  mafAction *pntActionFront = m_SER->AddAction("pntActionFront",-10);
  mafAction *pntActionLateral = m_SER->AddAction("pntActionLateral",-10);

  //create the positional event router
  mafNEW(m_FrontalViewPER);
  m_FrontalViewPER->SetName("perFrontView");
  m_FrontalViewPER->SetListener(this);
  m_FrontalViewPER->SetRenderer(m_FrontalView->GetFrontRenderer());

  mafNEW(m_LateralViewPER);
  m_LateralViewPER->SetName("perLateralView");
  m_LateralViewPER->SetListener(this);
  m_LateralViewPER->SetRenderer(m_LateralView->GetFrontRenderer());

  m_FrontalView->GetRWI()->SetMouse(m_MouseFrontView);
  m_FrontalView->SetMouse(m_MouseFrontView);

  m_LateralView->GetRWI()->SetMouse(m_MouseLateralView);
  m_LateralView->SetMouse(m_MouseLateralView);

  m_MouseFrontView->SetView(m_FrontalView);
  m_MouseLateralView->SetView(m_LateralView);

  m_SER->GetAction("pntActionFront")->BindInteractor(m_FrontalViewPER);
  m_SER->GetAction("pntActionFront")->BindDevice(m_MouseFrontView);
  m_SER->GetAction("pntActionLateral")->BindInteractor(m_LateralViewPER);
  m_SER->GetAction("pntActionLateral")->BindDevice(m_MouseLateralView);

  mafVMERoot *root = m_Input;

  mafVMEImage *RX = NULL;
  mafNodeIterator *iter = root->NewIterator();
  for (mafNode *node = iter->GetFirstNode(); node; node = iter->GetNextNode())
  {
    m_FrontalView->VmeAdd(node);
    m_LateralView->VmeAdd(node);
    mafString tagProsthesisGroup = TAG_PROSTHESIS_GROUP;

    if (m_Training)
    {
      tagProsthesisGroup = TAG_PROSTHESIS_GROUP_TRAINING;
    }

    if (node->GetTagArray()->IsTagPresent(tagProsthesisGroup))
    {
      m_Prosthesis.push_back(mafVMEGroup::SafeDownCast(node));
      std::vector<vtkPolyDataMapper *> mappersFrontView;
      std::vector<vtkActor *> actorsFrontView;
      std::vector<vtkPolyDataMapper *> mappersLateralView;
      std::vector<vtkActor *> actorsLateralView;
      mafNodeIterator *iterProsthesis = node->NewIterator();
      for (mafNode *prosthesisChild = iterProsthesis->GetFirstNode();prosthesisChild;prosthesisChild = iterProsthesis->GetNextNode())
      {
        m_FrontalView->VmeAdd(prosthesisChild);
        m_LateralView->VmeAdd(prosthesisChild);
        mafString tagName;
        if (!m_Training)
        {
          tagName = TAG_PROSTHESIS_COMPONENT;
        }
        else
        {
          tagName = TAG_PROSTHESIS_COMPONENT_TRAINING;
        }
	      if (prosthesisChild->GetTagArray()->IsTagPresent(tagName))
        {
          
	        mafVMESurface *implantComponet = mafVMESurface::SafeDownCast(prosthesisChild);

          m_LateralView->VmeShow(implantComponet,true);
          m_FrontalView->VmeShow(implantComponet,true);

          m_ProsthesisComponent.push_back(implantComponet);

          mafGUITransformMouse *transform = new mafGUITransformMouse(implantComponet,this);
          transform->GetIsaRotate()->GetRotationConstraint()->GetRefSys()->SetTypeToView();
          transform->GetIsaRotate()->GetRotationConstraint()->SetConstraintModality(mafInteractorConstraint::LOCK, mafInteractorConstraint::LOCK, mafInteractorConstraint::FREE);
          m_ProsthesisTransform.push_back(transform);

          mafVMELandmarkCloud *cloud = mafVMELandmarkCloud::SafeDownCast(FindVmeByTag(m_Input,TAG_LND_CLOUD));
          if (m_RefSys.size() != m_Prosthesis.size())
          {
            cloud->Open();
            mafVMEGroup *refSys;
            mafNEW(refSys);
            refSys->SetName("RefSys");
            refSys->ReparentTo(node);
            double positionLandmark[3];
            double positionGroup[3];
            mafMatrix *matrixLandmark = mafVMELandmark::SafeDownCast(cloud->GetChild(0))->GetOutput()->GetAbsMatrix();
            mafTransform::GetPosition(*matrixLandmark,positionLandmark);
            mafMatrix *matrixGroup = mafVMEGroup::SafeDownCast(node)->GetOutput()->GetAbsMatrix();
            mafTransform::GetPosition(*matrixGroup,positionGroup);

            double pos[3];
            pos[0] = -(positionGroup[0] - positionLandmark[0]);
            pos[1] = -(positionGroup[1] - positionLandmark[1]);
            pos[2] = -(positionGroup[2] - positionLandmark[2]);

            mafMatrix m;
            mafTransform::SetPosition(m,pos);

            refSys->SetMatrix(m);
            m_RefSys.push_back(refSys);
            cloud->Close();
          }

          transform->SetRefSys(m_RefSys[m_RefSys.size()-1]);
          //transform->SetRotationConstraintId(hipGUITransformMouse::NORMAL_VIEW_PLANE);
          /*m_ComponentsTransform.push_back(transform);*/
        }
      }

      ID_TRANSFORM_GUI ids;
      ids.ID_TRANSLATE_X = m_Prosthesis.size()*NUM_IDS_FOR_GUI_TRANSFORM + MAX_ID + 0;
      ids.ID_TRANSLATE_Y = m_Prosthesis.size()*NUM_IDS_FOR_GUI_TRANSFORM + MAX_ID + 1;
      ids.ID_TRANSLATE_Z = m_Prosthesis.size()*NUM_IDS_FOR_GUI_TRANSFORM + MAX_ID + 2;
      ids.ID_ROTATE_X = m_Prosthesis.size()*NUM_IDS_FOR_GUI_TRANSFORM + MAX_ID + 3;
      ids.ID_ROTATE_Y = m_Prosthesis.size()*NUM_IDS_FOR_GUI_TRANSFORM + MAX_ID + 4;
      ids.ID_ROTATE_Z = m_Prosthesis.size()*NUM_IDS_FOR_GUI_TRANSFORM + MAX_ID + 5;
    
      double *pos,*rot;
      pos = new double[3];
      rot = new double[3];
      mafVMEGroup::SafeDownCast(node)->GetOutput()->GetAbsPose(pos,rot);
      m_LateralGui->AddGui(CreateTransformGui(node->GetName(),ids,&pos[0],&pos[1],&pos[2],&rot[0],&rot[1],&rot[2]));
      m_LateralGui->FitGui();

      m_Positions.push_back(pos);
      m_Rotations.push_back(rot);

      m_TransformIds.push_back(ids);

      mafMatrix *m;
      mafNEW(m);
      m->DeepCopy(mafVMEGroup::SafeDownCast(node)->GetOutput()->GetAbsMatrix());
      m_OldMatrix.push_back(m);
      
    }
  }

  iter->Delete();

  m_LateralGui->Update();

  m_FrontalView->CameraReset();
  m_FrontalView->CameraUpdate();

}
//----------------------------------------------------------------------------
void iGuiDialogPlacingImplant::ShowRX()
//----------------------------------------------------------------------------
{
  mafVMERoot *root = m_Input;

  mafVMEImage *RX = NULL;
  RX = mafVMEImage::SafeDownCast(FindVmeByTag(root,TAG_RX));
  if (RX == NULL)
  {
    return;
  }

//   mafNodeIterator *iter = root->NewIterator();
//   for (mafNode *node = iter->GetFirstNode(); node; node = iter->GetNextNode())
//   {
//     if (node->GetTagArray()->IsTagPresent(TAG_RX))
//     {
//       RX = mafVMEImage::SafeDownCast(node);
//       break;
//     }
//   }
// 
//   iter->Delete();
// 
//   if (!RX)
//   {
//     return;
//   }

  vtkImageData *image = vtkImageData::SafeDownCast(RX->GetOutput()->GetVTKData());
  image->Update();

  mafTagItem *calibrationTagItem = RX->GetTagArray()->GetTag("TAG_RX_CALIBRATION_SPACING");

  vtkMAFSmartPointer<vtkImageData> imageCopy;

  if (calibrationTagItem != NULL)
  {
	  double calibrationSpacing = calibrationTagItem->GetComponentAsDouble(0);
	  imageCopy->DeepCopy(image);

	  imageCopy->SetSpacing(calibrationSpacing, calibrationSpacing , 1);		
	  imageCopy->Modified();

	  image = imageCopy;
  }

  double origin[3];
  image->GetOrigin(origin);
  image->GetBounds(m_SliceBounds);
  double diffx,diffy;
  diffx=m_SliceBounds[1]-m_SliceBounds[0];
  diffy=m_SliceBounds[3]-m_SliceBounds[2];

  vtkNEW(m_SliceLookupTable);
  vtkNEW(m_SliceTexture);
  m_SliceTexture->InterpolateOn();
  vtkNEW(m_SlicePlane);
  m_SlicePlane->SetOrigin(origin);
  m_SlicePlane->SetPoint1(origin[0]+diffx,origin[1],origin[2]);
  m_SlicePlane->SetPoint2(origin[0],origin[1],origin[2]-diffy);
  vtkNEW(m_SliceMapper);
  m_SliceMapper->SetInput(m_SlicePlane->GetOutput());
  vtkNEW(m_SliceActor);
  m_SliceActor->SetMapper(m_SliceMapper);
  m_SliceActor->SetTexture(m_SliceTexture); 
  m_SliceActor->VisibilityOn();

  m_FrontalView->GetFrontRenderer()->AddActor(m_SliceActor);

  m_SliceTexture->SetInput(image);
  m_SliceTexture->Modified();

  double range[2];
  image->GetScalarRange(range);
  m_SliceLookupTable->SetTableRange(range);
  m_SliceLookupTable->SetWindow(range[1] - range[0]);
  m_SliceLookupTable->SetLevel((range[1] + range[0]) / 2.0);
  m_SliceLookupTable->Build();

  m_SliceTexture->MapColorScalarsThroughLookupTableOn();
  m_SliceTexture->SetLookupTable((vtkLookupTable *)m_SliceLookupTable);

  m_FrontalView->CameraReset();
  m_FrontalView->CameraUpdate();
}
//----------------------------------------------------------------------------
mafGUI* iGuiDialogPlacingImplant::CreateTransformGui(mafString name,ID_TRANSFORM_GUI ids,double *posX,double *posY, double *posZ, double *rotX, double *rotY, double *rotZ)
//----------------------------------------------------------------------------
{
  mafGUI *gui = new mafGUI(this);
  gui->Reparent(this);

  gui->Label(name,true);

  double *step = new double;
  *step = 1.0;
  gui->Label(_("translate"),true);
  gui->CrossIncremental(ids.ID_TRANSLATE_X,_("X"),step,NULL,posX,mafGUICrossIncremental::ID_LEFT_RIGHT_LAYOUT);
  gui->CrossIncremental(ids.ID_TRANSLATE_Y,_("Y"),step,NULL,posY,mafGUICrossIncremental::ID_LEFT_RIGHT_LAYOUT);
  gui->CrossIncremental(ids.ID_TRANSLATE_Z,_("Z"),step,NULL,posZ,mafGUICrossIncremental::ID_LEFT_RIGHT_LAYOUT);

  gui->Label(_("rotate"),true);
  gui->CrossIncremental(ids.ID_ROTATE_X,_("X"),step,NULL,rotX,mafGUICrossIncremental::ID_LEFT_RIGHT_LAYOUT);
  gui->CrossIncremental(ids.ID_ROTATE_Y,_("Y"),step,NULL,rotY,mafGUICrossIncremental::ID_LEFT_RIGHT_LAYOUT);
  gui->CrossIncremental(ids.ID_ROTATE_Z,_("Z"),step,NULL,rotZ,mafGUICrossIncremental::ID_LEFT_RIGHT_LAYOUT);

  gui->FitGui();
  gui->Update();

  return gui;
}
//----------------------------------------------------------------------------
void iGuiDialogPlacingImplant::ShowReconstruction3D()
//----------------------------------------------------------------------------
{
  mafVMESurface *reconstruction = mafVMESurface::SafeDownCast(FindVmeByTag(m_Input,TAG_3D_RECONSTRUCTION));
  reconstruction->Update();

  vtkPolyData *pd = vtkPolyData::SafeDownCast(reconstruction->GetOutput()->GetVTKData());
  pd->Update();

  vtkMAFSmartPointer<vtkTransform> tr;
  tr->SetMatrix(reconstruction->GetOutput()->GetAbsMatrix()->GetVTKMatrix());

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> tp;
  tp->SetInput(pd);
  tp->SetTransform(tr);           
  tp->Update();

  vtkMAFSmartPointer<vtkPolyDataMapper> mapperFront;
  mapperFront->SetInput(tp->GetOutput());

  vtkMAFSmartPointer<vtkPolyDataMapper> mapperLateral;
  mapperLateral->SetInput(tp->GetOutput());

  reconstruction->GetMaterial()->m_Prop->SetOpacity(0.5);

  vtkNEW(m_ReconstructionFrontViewActor);
  m_ReconstructionFrontViewActor->SetMapper(mapperFront);
  m_ReconstructionFrontViewActor->SetProperty(reconstruction->GetMaterial()->m_Prop);
  m_ReconstructionFrontViewActor->PickableOff();

  vtkNEW(m_ReconstructionLateraltViewActor);
  m_ReconstructionLateraltViewActor->SetMapper(mapperLateral);
  m_ReconstructionLateraltViewActor->SetProperty(reconstruction->GetMaterial()->m_Prop);
  m_ReconstructionLateraltViewActor->PickableOff();

  m_FrontalView->GetFrontRenderer()->AddActor(m_ReconstructionFrontViewActor);
  m_FrontalView->CameraReset();
  m_FrontalView->CameraUpdate();

  m_LateralView->GetFrontRenderer()->AddActor(m_ReconstructionLateraltViewActor);
  m_LateralView->CameraReset();
  m_LateralView->CameraUpdate();
}
//----------------------------------------------------------------------------
void iGuiDialogPlacingImplant::ShowLandmarks()
//----------------------------------------------------------------------------
{
  mafVMELandmarkCloud *cloud = mafVMELandmarkCloud::SafeDownCast(FindVmeByTag(m_Input,TAG_LND_CLOUD));

  vtkPolyData *pd = vtkPolyData::SafeDownCast(cloud->GetOutput()->GetVTKData());
  pd->Update();

  vtkMAFSmartPointer<vtkTransform> tr;
  tr->SetMatrix(cloud->GetOutput()->GetAbsMatrix()->GetVTKMatrix());

  vtkMAFSmartPointer<vtkSphereSource> sphere;
  sphere->SetRadius(10.0);
  sphere->Update();

  vtkMAFSmartPointer<vtkMAFExtendedGlyph3D> glyph;
  glyph->SetInput(pd);
  glyph->SetSource(sphere->GetOutput());
  glyph->Update();

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> tp;
  tp->SetInput(glyph->GetOutput());
  tp->SetTransform(tr);           
  tp->Update();

  vtkMAFSmartPointer<vtkPolyDataMapper> mapper;
  mapper->SetInput(tp->GetOutput());

  vtkNEW(m_LandmarksActor);
  m_LandmarksActor->SetMapper(mapper);

  m_FrontalView->GetFrontRenderer()->AddActor(m_LandmarksActor);
  m_FrontalView->CameraUpdate();
}
//----------------------------------------------------------------------------
void iGuiDialogPlacingImplant::CameraUpdate()
//----------------------------------------------------------------------------
{
  m_LateralView->CameraUpdate();
  m_FrontalView->CameraUpdate();
}
//----------------------------------------------------------------------------
void iGuiDialogPlacingImplant::CreateFrontalView()
//----------------------------------------------------------------------------
{
  //Change default frame to our dialog
  wxWindow* oldFrame = mafGetFrame();
  mafSetFrame(this);

  m_FrontalView = new mafViewVTK("view",CAMERA_OS_Y);
  m_FrontalView->Create();
  m_FrontalView->GetSceneGraph()->VmeSelect(m_Input,true);
  m_FrontalView->VmeAdd(m_Input); //add Root
  m_FrontalView->GetGui();
  m_FrontalView->m_Rwi->SetSize(0,0,500,500);
  m_FrontalView->m_Rwi->Show(true);

  mafSetFrame(oldFrame);
}
//----------------------------------------------------------------------------
void iGuiDialogPlacingImplant::CreateLateralView()
//----------------------------------------------------------------------------
{
  //Change default frame to our dialog
  wxWindow* oldFrame = mafGetFrame();
  mafSetFrame(this);

  if (m_Side == ID_SIDE_RIGHT)
  {
    m_LateralView = new mafViewVTK("view",CAMERA_RX_RIGHT);
  }
  else
  {
    m_LateralView = new mafViewVTK("view",CAMERA_RX_LEFT);
  }

  m_LateralView->Create();
  m_LateralView->GetSceneGraph()->VmeSelect(m_Input,true);
  m_LateralView->VmeAdd(m_Input); //add Root
  m_LateralView->GetGui();
  m_LateralView->m_Rwi->SetSize(0,0,250,500);
  m_LateralView->m_Rwi->Show(false);

  mafSetFrame(oldFrame);
}
//----------------------------------------------------------------------------
void iGuiDialogPlacingImplant::PostMultiplyEventMatrix(mafEventBase *maf_event)
  //----------------------------------------------------------------------------
{  
  mafVMEGroup *prosthesisGroup = NULL;
  if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
  {
    int i;
    for (i=0;i<m_ProsthesisTransform.size();i++)
    {
      if (e->GetSender()==m_ProsthesisTransform[i])
      {
        mafVMEGroup *parent = NULL;
        mafVMESurface *tmp = mafVMESurface::SafeDownCast(m_ProsthesisComponent[i]);
        do 
        {
          parent = mafVMEGroup::SafeDownCast(tmp->GetParent());
          tmp = parent == NULL ? mafVMESurface::SafeDownCast(tmp->GetParent()) : NULL;
        } while (parent == NULL);

        prosthesisGroup = parent;
        break;
      }
      
    }
    if (prosthesisGroup == NULL)
    {
      return;
    }

    long arg = e->GetArg();

    // handle incoming transform events
    vtkTransform *tr = vtkTransform::New();
    tr->PostMultiply();
    tr->SetMatrix(prosthesisGroup->GetOutput()->GetAbsMatrix()->GetVTKMatrix());
    tr->Concatenate(e->GetMatrix()->GetVTKMatrix());
    tr->Update();

    mafMatrix absPose;
    absPose.DeepCopy(tr->GetMatrix());
    absPose.SetTimeStamp(0.0);

    mafLogMessage(wxString::Format("arg is %d",arg));
    if (arg == mafInteractorGenericMouse::MOUSE_MOVE)
    {
      mafVMEImage *RX = NULL;
      mafNodeIterator *iter = m_Input->NewIterator();
      int k=0;
      for (mafNode *node = iter->GetFirstNode(); node; node = iter->GetNextNode())
      {
        mafString tagProsthesisGroup = TAG_PROSTHESIS_GROUP;

        if (m_Training)
        {
          tagProsthesisGroup = TAG_PROSTHESIS_GROUP_TRAINING;
        }
        if (node->GetTagArray()->IsTagPresent(tagProsthesisGroup))
        {
          if (node == prosthesisGroup)
          {
            i = k;
          }
          else
          {
            k++;
          }
          
        }

      }
      iter->Delete();
      mafTransform::GetPosition(absPose,m_Positions[k]);
      mafTransform::GetOrientation(absPose,m_Rotations[k]);

      m_LateralGui->Update();

      // move vme
      prosthesisGroup->SetAbsMatrix(absPose);
      prosthesisGroup->GetAbsMatrixPipe()->Update();

      m_RefSys[i]->GetAbsMatrixPipe()->Update();
    } 
    
    m_FrontalView->CameraUpdate();
    m_LateralView->CameraUpdate();

    // clean up
    tr->Delete();

  }
}