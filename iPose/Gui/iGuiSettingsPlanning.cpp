/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiSettingsPlanning.cpp,v $
Language:  C++
Date:      $Date: 2012-02-01 09:20:29 $
Version:   $Revision: 1.1.2.2 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2001/2005 
CINECA - Interuniversity Consortium (www.cineca.it)
=========================================================================*/


#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iGuiSettingsPlanning.h"
#include "mafGUI.h"

/** GUI IDs*/
enum RECONSTRUCTION_SETTINGS_WIDGET_ID
{
  ID_COMMAND_LINE = MINID,
  ID_STL_FILENAME,
  ID_TXT_FILENAME,
  ID_INPUT_FILENAME,
  ID_WORKING_DIR,
  ID_AVERAGE_BMI,
  ID_SD_BMI,
};

#define MaxNumberChar 50

//----------------------------------------------------------------------------
iGuiSettingsPlanning::iGuiSettingsPlanning(mafObserver *Listener, const mafString &label):
mafGUISettings(Listener, label)
//----------------------------------------------------------------------------
{
  m_Config->SetPath("3D Reconstruction");

  m_AverageBmi = 0.0;
  m_StandardDeviationBmi = 0.0;

  InitializeSettings();
}
//----------------------------------------------------------------------------
iGuiSettingsPlanning::~iGuiSettingsPlanning()
//----------------------------------------------------------------------------
{
}
//----------------------------------------------------------------------------
void iGuiSettingsPlanning::CreateGui()
//----------------------------------------------------------------------------
{
  
  m_Gui = new mafGUI(this);

  m_Gui->Label("");
  m_Gui->Label("3D Reconstruction",true);

  m_Gui->Label(_("Working directory:"),true);
  m_Gui->DirOpen(ID_WORKING_DIR,"",&m_WorkingDirectory);
  m_Gui->Label("");

  m_Gui->Label(_("Command line:"),true);
  m_Gui->String(ID_COMMAND_LINE,"",&m_CommandLine);
  m_Gui->Label("");

  m_Gui->Label(_("Output STL filename:"),true);
  m_Gui->String(ID_STL_FILENAME,"",&m_FileNameSTL);
  m_AbsoulutePathFileNameSTL = m_WorkingDirectory;
  m_AbsoulutePathFileNameSTL << "\\";
  m_AbsoulutePathFileNameSTL << m_FileNameSTL;

  if(m_AbsoulutePathFileNameSTL.Length()>MaxNumberChar)
  {
    wxString appo = m_AbsoulutePathFileNameSTL.GetCStr();
    m_AbsoulutePathFileNameSTL = appo.SubString(0,MaxNumberChar-1);
    m_AbsoulutePathFileNameSTL<<"\n";
    m_AbsoulutePathFileNameSTL<<appo.SubString(MaxNumberChar,appo.Length());
  }
  m_Gui->Label(&m_AbsoulutePathFileNameSTL,false,true);
  m_Gui->Label("");

  m_Gui->Label(_("Output TXT filename:"),true);
  m_Gui->String(ID_TXT_FILENAME,"",&m_FileNameTXT);
  m_AbsoulutePathFileNameTXT = m_WorkingDirectory;
  m_AbsoulutePathFileNameTXT << "\\";
  m_AbsoulutePathFileNameTXT << m_FileNameTXT;

  if(m_AbsoulutePathFileNameTXT.Length()>MaxNumberChar)
  {
    wxString appo = m_AbsoulutePathFileNameTXT.GetCStr();
    m_AbsoulutePathFileNameTXT = appo.SubString(0,MaxNumberChar-1);
    m_AbsoulutePathFileNameTXT<<"\n";
    m_AbsoulutePathFileNameTXT<<appo.SubString(MaxNumberChar,appo.Length());
  }
  m_Gui->Label(&m_AbsoulutePathFileNameTXT,false,true);
  m_Gui->Label("");

  m_Gui->Label(_("Input filename:"),true);
  m_Gui->String(ID_INPUT_FILENAME,"",&m_FileNameInput);
  m_AbsoulutePathFileNameInput = m_WorkingDirectory;
  m_AbsoulutePathFileNameInput << "\\";
  m_AbsoulutePathFileNameInput << m_FileNameInput;

  if(m_AbsoulutePathFileNameInput.Length()>MaxNumberChar)
  {
    wxString appo = m_AbsoulutePathFileNameInput.GetCStr();
    m_AbsoulutePathFileNameInput = appo.SubString(0,MaxNumberChar-1);
    m_AbsoulutePathFileNameInput<<"\n";
    m_AbsoulutePathFileNameInput<<appo.SubString(MaxNumberChar,appo.Length());
  }
  m_Gui->Label(&m_AbsoulutePathFileNameInput,false,true);
  m_Gui->Label("");

  m_Gui->Label(_("Primary Stability"),true);
  m_Gui->Label(_("Average BMI (Kg/cm^2)"));
  m_Gui->Double(ID_AVERAGE_BMI,"",&m_AverageBmi);
  m_Gui->Label("Standard Deviation BMI (Kg/cm^2)");
  m_Gui->Double(ID_SD_BMI,"",&m_StandardDeviationBmi);
  m_Gui->Label("");
}
//----------------------------------------------------------------------------
void iGuiSettingsPlanning::OnEvent(mafEventBase *maf_event)
//----------------------------------------------------------------------------
{
  switch(maf_event->GetId())
  {
  case ID_INPUT_FILENAME:
    {
      m_AbsoulutePathFileNameInput = m_WorkingDirectory;
      m_AbsoulutePathFileNameInput << "\\";
      m_AbsoulutePathFileNameInput << m_FileNameInput;

      if(m_AbsoulutePathFileNameInput.Length()>MaxNumberChar)
      {
        wxString appo = m_AbsoulutePathFileNameInput.GetCStr();
        m_AbsoulutePathFileNameInput = appo.SubString(0,MaxNumberChar-1);
        m_AbsoulutePathFileNameInput<<"\n";
        m_AbsoulutePathFileNameInput<<appo.SubString(MaxNumberChar,appo.Length());
      }

      m_Config->Write("FileNameInput",m_FileNameInput);

      m_Gui->Update();
    }
    break;
  case ID_COMMAND_LINE:
    {
      m_Config->Write("CommandLine",m_CommandLine);
    }
    break;
  case ID_STL_FILENAME:
    {
      m_AbsoulutePathFileNameSTL = m_WorkingDirectory;
      m_AbsoulutePathFileNameSTL << "\\";
      m_AbsoulutePathFileNameSTL << m_FileNameSTL;

      if(m_AbsoulutePathFileNameSTL.Length()>MaxNumberChar)
      {
        wxString appo = m_AbsoulutePathFileNameSTL.GetCStr();
        m_AbsoulutePathFileNameSTL = appo.SubString(0,MaxNumberChar-1);
        m_AbsoulutePathFileNameSTL<<"\n";
        m_AbsoulutePathFileNameSTL<<appo.SubString(MaxNumberChar,appo.Length());
      }

      m_Config->Write("FileNameSTL",m_FileNameSTL);

      m_Gui->Update();
    }
    break;
  case ID_TXT_FILENAME:
    {
      m_AbsoulutePathFileNameTXT = m_WorkingDirectory;
      m_AbsoulutePathFileNameTXT << "\\";
      m_AbsoulutePathFileNameTXT << m_FileNameTXT;

      if(m_AbsoulutePathFileNameTXT.Length()>MaxNumberChar)
      {
        wxString appo = m_AbsoulutePathFileNameTXT.GetCStr();
        m_AbsoulutePathFileNameTXT = appo.SubString(0,MaxNumberChar-1);
        m_AbsoulutePathFileNameTXT<<"\n";
        m_AbsoulutePathFileNameTXT<<appo.SubString(MaxNumberChar,appo.Length());
      }

      m_Config->Write("FileNameTXT",m_FileNameTXT);

      m_Gui->Update();
    }
    break;
  case ID_WORKING_DIR:
    {
      m_Config->Write("WorkingDirectory",m_WorkingDirectory);
      m_AbsoulutePathFileNameSTL = m_WorkingDirectory;
      m_AbsoulutePathFileNameSTL << "\\";
      m_AbsoulutePathFileNameSTL << m_FileNameSTL;

      if(m_AbsoulutePathFileNameSTL.Length()>MaxNumberChar)
      {
        wxString appo = m_AbsoulutePathFileNameSTL.GetCStr();
        m_AbsoulutePathFileNameSTL = appo.SubString(0,MaxNumberChar-1);
        m_AbsoulutePathFileNameSTL<<"\n";
        m_AbsoulutePathFileNameSTL<<appo.SubString(MaxNumberChar,appo.Length());
      }

      m_AbsoulutePathFileNameTXT = m_WorkingDirectory;
      m_AbsoulutePathFileNameTXT << "\\";
      m_AbsoulutePathFileNameTXT << m_FileNameTXT;

      if(m_AbsoulutePathFileNameTXT.Length()>MaxNumberChar)
      {
        wxString appo = m_AbsoulutePathFileNameTXT.GetCStr();
        m_AbsoulutePathFileNameTXT = appo.SubString(0,MaxNumberChar-1);
        m_AbsoulutePathFileNameTXT<<"\n";
        m_AbsoulutePathFileNameTXT<<appo.SubString(MaxNumberChar,appo.Length());
      }

      m_AbsoulutePathFileNameInput = m_WorkingDirectory;
      m_AbsoulutePathFileNameInput << "\\";
      m_AbsoulutePathFileNameInput << m_FileNameInput;

      if(m_AbsoulutePathFileNameInput.Length()>MaxNumberChar)
      {
        wxString appo = m_AbsoulutePathFileNameInput.GetCStr();
        m_AbsoulutePathFileNameInput = appo.SubString(0,MaxNumberChar-1);
        m_AbsoulutePathFileNameInput<<"\n";
        m_AbsoulutePathFileNameInput<<appo.SubString(MaxNumberChar,appo.Length());
      }

      m_Gui->Update();
    }
    break;
  case ID_AVERAGE_BMI:
    {
      m_Config->Write("AverageBmi",m_AverageBmi);
    }
    break;
  case ID_SD_BMI:
    {
      m_Config->Write("StandardDeviationBmi",m_StandardDeviationBmi);
    }
    break;
  default:
    mafEventMacro(*maf_event);
    break; 
  }
  m_Config->Flush();
}
//----------------------------------------------------------------------------
void iGuiSettingsPlanning::InitializeSettings()
//----------------------------------------------------------------------------
{
  wxString string_item;

  if(m_Config->Read("WorkingDirectory", &string_item))
  {
    m_WorkingDirectory=string_item;
  }
  else
  {
    m_Config->Write("WorkingDirectory",m_WorkingDirectory);
  }

  if(m_Config->Read("CommandLine", &string_item))
  {
    m_CommandLine=string_item;
  }
  else
  {
    m_Config->Write("CommandLine",m_CommandLine);
  }

  if(m_Config->Read("FileNameTXT", &string_item))
  {
    m_FileNameTXT=string_item;
  }
  else
  {
    m_Config->Write("FileNameTXT",m_FileNameTXT);
  }

  if(m_Config->Read("FileNameSTL", &string_item))
  {
    m_FileNameSTL=string_item;
  }
  else
  {
    m_Config->Write("FileNameSTL",m_FileNameSTL);
  }

  if(m_Config->Read("FileNameInput", &string_item))
  {
    m_FileNameInput=string_item;
  }
  else
  {
    m_Config->Write("FileNameInput",m_FileNameInput);
  }

  double double_item;
  if(m_Config->Read("AverageBmi", &double_item))
  {
    m_AverageBmi=double_item;
  }
  else
  {
    m_Config->Write("AverageBmi",m_AverageBmi);
  }

  if(m_Config->Read("StandardDeviationBmi", &double_item))
  {
    m_StandardDeviationBmi=double_item;
  }
  else
  {
    m_Config->Write("StandardDeviationBmi",m_StandardDeviationBmi);
  }

  m_Config->Flush();
}
//----------------------------------------------------------------------------
mafString iGuiSettingsPlanning::GetCommandLine()
//----------------------------------------------------------------------------
{
  return m_CommandLine;
}
//----------------------------------------------------------------------------
mafString iGuiSettingsPlanning::GetFileNameSTL()
//----------------------------------------------------------------------------
{
  return m_FileNameSTL;
}
//----------------------------------------------------------------------------
mafString iGuiSettingsPlanning::GetFileNameTXT()
//----------------------------------------------------------------------------
{
  return m_FileNameTXT;
}
//----------------------------------------------------------------------------
mafString iGuiSettingsPlanning::GetInputFileName()
//----------------------------------------------------------------------------
{
  return m_FileNameInput;
}
//----------------------------------------------------------------------------
mafString iGuiSettingsPlanning::GetWorkingDirectory()
//----------------------------------------------------------------------------
{
  return m_WorkingDirectory;
}
//----------------------------------------------------------------------------
double iGuiSettingsPlanning::GetAverageBmi()
//----------------------------------------------------------------------------
{
  return m_AverageBmi;
}
//----------------------------------------------------------------------------
double iGuiSettingsPlanning::GetStandardDeviationBmi()
//----------------------------------------------------------------------------
{
  return m_StandardDeviationBmi;
}
