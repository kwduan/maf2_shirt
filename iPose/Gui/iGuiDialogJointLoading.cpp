/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialogJointLoading.cpp,v $
Language:  C++
Date:      $Date: 2012-03-21 07:25:01 $
Version:   $Revision: 1.1.2.10 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/


#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iGuiDialogJointLoading.h"
#include "iDecl.h"
#include "iGuiSettingsPlanning.h"

#include "mafGUIButton.h"
#include "mafGUIValidator.h"
#include "mafRWI.h"
#include "mafDeviceButtonsPadMouse.h"
#include "mafVMESurface.h"
#include "mmaMaterial.h"
#include "mafVMERoot.h"
#include "mafTransform.h"
#include "mafVMELandmarkCloud.h"

#include "vtkMAFSmartPointer.h"
#include "vtkRenderWindow.h"
#include "vtkCamera.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkProperty.h"
#include "vtkTransform.h"
#include "vtkPolyData.h"
#include "vtkArrowSource.h"
#include "vtkMAFExtendedGlyph3D.h"
#include "vtkPoints.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkDoubleArray.h"
#include "vtkPointData.h"
#include "vtkSphereSource.h"
#include "vtkMath.h"
#include "vtkTextActor.h"
#include "vtkTextProperty.h"

#include <wx/busyinfo.h>
#include <wx/tokenzr.h>
#include "mafVMEGroup.h"
#include "iAttributeWorkflow.h"

//----------------------------------------------------------------------------
iGuiDialogJointLoading::iGuiDialogJointLoading(const wxString& title,mafDeviceButtonsPadMouse *mouse,mafVMERoot *input,int jointType,int id,double weight, bool training /* = false */)
: mafGUIDialog(title,mafCLOSEWINDOW | mafRESIZABLE)
//----------------------------------------------------------------------------
{
  int x_pos,y_pos,w,h;
  mafGetFrame()->GetPosition(&x_pos,&y_pos);
  this->GetSize(&w,&h);
  this->SetSize(x_pos+5,y_pos+5,w,h);

  m_Input = input;
  m_Rwi3DView = NULL;
  m_ComponentsTextActors = NULL;

  m_Training = training;

  m_Mouse = mouse;
  this->SetSize(800, 600);

  CreateGui();

#if _DEBUG
  ShowLandmarks();
#endif
  
  ShowReconstruction3D();
  if (jointType == ID_JOINT_HIP)
  {
    double vector[3];
	  if (ExecuteExternalApplication(weight,id,vector,m_Input,m_Training))
	  {
      ShowVector(vector);
	  }
  }
  
}
//----------------------------------------------------------------------------
iGuiDialogJointLoading::iGuiDialogJointLoading(const wxString& title,mafDeviceButtonsPadMouse *mouse,mafVMERoot *input,double vector[3], bool training /* = false */)
: mafGUIDialog(title,mafCLOSEWINDOW | mafRESIZABLE)
//----------------------------------------------------------------------------
{
  int x_pos,y_pos,w,h;
  mafGetFrame()->GetPosition(&x_pos,&y_pos);
  this->GetSize(&w,&h);
  this->SetSize(x_pos+5,y_pos+5,w,h);

  m_Training = training;

  m_Input = input;
  m_Rwi3DView = NULL;

  m_Mouse = mouse;
  this->SetSize(800, 600);

  CreateGui();

#if _DEBUG
  ShowLandmarks();
#endif

  ShowReconstruction3D();
  ShowVector(vector);
}

//----------------------------------------------------------------------------
iGuiDialogJointLoading::~iGuiDialogJointLoading()
//----------------------------------------------------------------------------
{
  m_Rwi3DView->m_RenFront->RemoveAllProps();
  if (m_ComponentsTextActors)
  {
    m_Rwi3DView->m_RenFront->RemoveActor2D(m_ComponentsTextActors);
    vtkDEL(m_ComponentsTextActors);
  }
  cppDEL(m_Rwi3DView);
}
//----------------------------------------------------------------------------
void iGuiDialogJointLoading::OnEvent(mafEventBase *maf_event)
//----------------------------------------------------------------------------
{
  if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
  {
    switch(e->GetId())
    {
    case ID_CANCEL:
      {
        this->EndModal(wxCANCEL);
      }
      break;
    case ID_ACCEPT_BUTTON:
      this->EndModal(wxID_RETURN_NEXT);
      break;
    case ID_REJECT_BUTTON:
      this->EndModal(wxID_RETURN_PREV);
      break;
    default:
      mafEventMacro(*e);
    }
  }
}
//----------------------------------------------------------------------------
void iGuiDialogJointLoading::CreateGui()
//----------------------------------------------------------------------------
{
  wxPoint p = wxDefaultPosition;

  int x,y,z,vx,vy,vz;
  x=0; y=1; z=0; vx=0; vy=0; vz=-1;

  m_Rwi3DView = new mafRWI(this,TWO_LAYER,false);
  m_Rwi3DView->SetListener(this);//SIL. 16-6-2004: 
  m_Rwi3DView->CameraSet(CAMERA_PERSPECTIVE);

  m_Rwi3DView->SetSize(0,0,640,640);

  m_Rwi3DView->m_RenderWindow->SetDesiredUpdateRate(0.0001f);
  m_Rwi3DView->Show(true);
  m_Rwi3DView->m_RwiBase->SetMouse(m_Mouse);

  wxBoxSizer *buttonSizer2 = new wxBoxSizer(wxHORIZONTAL);

  iGuiSettingsPlanning *setting = new iGuiSettingsPlanning(NULL);
  double avrBmi = setting->GetAverageBmi();
  double sdBmi = setting->GetStandardDeviationBmi();


  wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);

  m_ButtonAccept = new mafGUIButton(this, ID_ACCEPT_BUTTON,_("accept"), p, wxSize(100,20));
  m_ButtonAccept->SetValidator(mafGUIValidator(this,ID_ACCEPT_BUTTON,m_ButtonAccept));
  m_ButtonAccept->Enable(true);

  buttonSizer->Add(m_ButtonAccept,0);

  m_GuiSizer->Add(m_Rwi3DView->m_RwiBase,0,wxCENTRE,5);
	m_GuiSizer->Add(buttonSizer,0,wxCENTRE,5);

  cppDEL(setting);
}
//----------------------------------------------------------------------------
bool iGuiDialogJointLoading::ExecuteExternalApplication(double weight,int patientId,double vectorOut[3],mafVMERoot *root,bool training /* = false */)
//----------------------------------------------------------------------------
{
  mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(root,TAG_GROUP_ATTRIBUTES));
  iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));

  wxString ccd = "";
  wxString neck = "";
  wxString anteversion = "";
  if (attribute == NULL || training)
  {
    wxString measuresFileName = mafGetApplicationDirectory().c_str();
    if (!training)
    {
    	measuresFileName<<"/cache/";
    }
    else
    {
      measuresFileName<<"/download/";
    }
    measuresFileName<<patientId;
    measuresFileName<<"/";
    measuresFileName += RX_MEASURES_FILENAME;

    std::map<wxString,double> measures;
    int result = ReadMeasures(measuresFileName,measures);

    if (result == MAF_ERROR)
    {
      wxMessageBox(_("Error during reading of measures"));
      return false;
    }

    ccd << measures["ccd (deg)"];
    neck << measures["neck length (mm)"];
    anteversion << measures["anteversion (deg)"];
  }
  else
  {
    anteversion << attribute->GetHipAnteversion();
    neck << attribute->GetHipNeckLength();
    ccd << attribute->GetHipCCD();
  }


  wxString workingDir = mafGetApplicationDirectory().c_str();
  workingDir << "/ExternalTool";
  wxString oldDir = wxGetCwd();
  wxSetWorkingDirectory(workingDir);

  //Remove old results
  ::wxRemoveFile("Results.out");
  
  wxArrayString output;
  wxArrayString errors;
  wxString command2execute;
  command2execute = "java -jar HipContactForces.jar  ";
  command2execute += "-ccd ";
  command2execute += ccd;
  command2execute += " -anteversion ";
  command2execute += anteversion;
  command2execute += " -neck ";
  command2execute += neck;
  command2execute += " -weight ";
  command2execute += wxString::Format("%.3f",weight);
  command2execute += " > Results.out";

  mafLogMessage("COMMAND: %s",command2execute.c_str());

  long pid = -1;
  if (!wxShell(command2execute/*, output, errors, wxEXEC_SYNC*/))
  {
    
    mafLogMessage(_("Command Errors"));  

    return false;
  }

  FILE *outputFile = fopen("Results.out","r");
  if (!outputFile)
  {
    mafLogMessage(_("Command Errors"));  

    return false;
  }

  char *line = new char[100];
  strcpy(line,"");

  fgets(line, 100, outputFile);
  fgets(line, 100, outputFile);
  wxString vectorLine = line;
  vectorLine.Replace("\t"," ");
  vectorLine.Replace(" ","#");
  wxStringTokenizer tkz(vectorLine,wxT('#')); // Read t values
  int num_t = tkz.CountTokens();
  vectorOut[0] = atof(tkz.GetNextToken());
  vectorOut[1] = atof(tkz.GetNextToken());
  vectorOut[2] = atof(tkz.GetNextToken());

  delete[]line;
  wxSetWorkingDirectory(oldDir);

  return true;
}
//----------------------------------------------------------------------------
void iGuiDialogJointLoading::ShowReconstruction3D()
//----------------------------------------------------------------------------
{
  mafVMESurface *reconstruction = mafVMESurface::SafeDownCast(FindVmeByTag(m_Input,TAG_3D_RECONSTRUCTION));
  reconstruction->Update();

  vtkPolyData *pd = vtkPolyData::SafeDownCast(reconstruction->GetOutput()->GetVTKData());
  pd->Update();

  vtkMAFSmartPointer<vtkTransform> tr;
  tr->SetMatrix(reconstruction->GetOutput()->GetAbsMatrix()->GetVTKMatrix());

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> tp;
  tp->SetInput(pd);
  tp->SetTransform(tr);           
  tp->Update();

  vtkMAFSmartPointer<vtkPolyDataMapper> mapper;
  mapper->SetInput(tp->GetOutput());

  reconstruction->GetMaterial()->m_Prop->SetOpacity(0.5);

  vtkMAFSmartPointer<vtkActor> actor;
  actor->SetMapper(mapper);
  actor->SetProperty(reconstruction->GetMaterial()->m_Prop);

  m_Rwi3DView->m_RenFront->AddActor(actor);
  m_Rwi3DView->m_RenFront->Render();
  double b[6];
  reconstruction->GetOutput()->GetBounds(b);
  m_Rwi3DView->CameraReset(b);
  m_Rwi3DView->CameraUpdate();
}
//----------------------------------------------------------------------------
void iGuiDialogJointLoading::ShowVector(double vector[3])
//----------------------------------------------------------------------------
{
  mafVMESurface *reconstruction = mafVMESurface::SafeDownCast(FindVmeByTag(m_Input,TAG_3D_RECONSTRUCTION));
  reconstruction->Update();

  vtkPolyData *polyReconstruction = vtkPolyData::SafeDownCast(reconstruction->GetOutput()->GetVTKData());
  polyReconstruction->Update();

  double b[6];
  polyReconstruction->GetBounds(b);

  double p1[3],p2[3];
  p1[0] = b[0];
  p1[1] = b[2];
  p1[2] = b[4];

  p2[0] = b[1];
  p2[1] = b[3];
  p2[2] = b[5];

  double bbDiagonal = sqrt(vtkMath::Distance2BetweenPoints(p1,p2));

//   m_Vector[0] = (m_Vector[0]*100)/bbDiagonal;
//   m_Vector[1] = (m_Vector[1]*100)/bbDiagonal;
//   m_Vector[2] = (m_Vector[2]*100)/bbDiagonal;

//   double origin[3] = {0.0,0.0,0.0};
//   double vectorLenght = sqrt(vtkMath::Distance2BetweenPoints(origin,m_Vector));

  vector[0] = - vector[0];
  double newVector[3];
  vtkMAFSmartPointer<vtkTransform> tmaf2;
  tmaf2->PostMultiply();
  tmaf2->SetMatrix(reconstruction->GetOutput()->GetAbsMatrix()->GetVTKMatrix());
  tmaf2->TransformVector(vector,newVector);

  double angleZ = atan(newVector[1] / (newVector[0]));
  double angleY = atan(newVector[2] / (newVector[0]));
  double angleX = atan(newVector[1] / newVector[2]);
  double position[3];
  mafVMELandmarkCloud *lnd = mafVMELandmarkCloud::SafeDownCast(FindVmeByTag(m_Input,TAG_LND_CLOUD));
  lnd->GetLandmarkPosition(0,position);

  position[1] = b[2];

  vtkMAFSmartPointer<vtkTransform> tr;
  tr->SetMatrix(lnd->GetOutput()->GetAbsMatrix()->GetVTKMatrix());

  vtkMAFSmartPointer<vtkArrowSource> arrow;
  arrow->SetShaftRadius(0.03);
  arrow->SetTipRadius(0.1);
  arrow->SetTipLength(0.3);
  arrow->Update();

  vtkMAFSmartPointer<vtkMAFExtendedGlyph3D> glyph;
  vtkMAFSmartPointer<vtkPolyData> data;
  vtkMAFSmartPointer<vtkPoints> pts;
  pts->InsertNextPoint(position);
  data->SetPoints(pts);
  data->Update();
  vtkMAFSmartPointer<vtkDoubleArray> scalars;
  scalars->SetName("SCALARS");
  scalars->SetNumberOfTuples(1);
  scalars->SetTuple1(0,1.0);
  data->GetPointData()->AddArray(scalars);
  data->GetPointData()->SetActiveScalars("SCALARS");
  data->Update();

  double origin[3] = {0,0,0};
  double scaleFactor = sqrt(vtkMath::Distance2BetweenPoints(origin,vector));

  glyph->SetInput(data);
  glyph->SetSource(arrow->GetOutput());
  glyph->SetScaleFactor(- (bbDiagonal*scaleFactor)/800.0);
  glyph->SetScaleModeToScaleByScalar();
  glyph->ScalingOn();
  glyph->ScalarVisibilityOn();
  glyph->Update();

  double glyphBounds[6];
  glyph->GetOutput()->GetBounds(glyphBounds);

//   vtkMAFSmartPointer<vtkTransform> trTranslateOfVectorLenght;
//   /*trTranslateOfVectorLenght->Translate(-(glyphBounds[1]-glyphBounds[0]),(glyphBounds[3]-glyphBounds[2]),(glyphBounds[5]-glyphBounds[4]));*/
//   /*trTranslateOfVectorLenght->Translate(-(glyphBounds[1]-glyphBounds[0]),-(glyphBounds[5]-glyphBounds[4]),-(glyphBounds[3]-glyphBounds[2]));*/
//   trTranslateOfVectorLenght->Translate(-(glyphBounds[1]-glyphBounds[0]),0,0);
//   vtkMAFSmartPointer<vtkTransformPolyDataFilter> tpTranslateOfVectorLenght;
//   tpTranslateOfVectorLenght->SetInput(glyph->GetOutput());
//   tpTranslateOfVectorLenght->SetTransform(trTranslateOfVectorLenght);
//   tpTranslateOfVectorLenght->Update();

  vtkMAFSmartPointer<vtkTransform> trRotate;
  trRotate->PostMultiply();
  // trRotate->Translate(-(glyphBounds[1]-glyphBounds[0]),0,0);
  trRotate->Translate(-position[0],-position[1],-position[2]);
  trRotate->RotateY(vtkMath::RadiansToDegrees()*angleY);
  trRotate->RotateX(vtkMath::RadiansToDegrees()*angleX);
  trRotate->RotateZ(vtkMath::RadiansToDegrees()*angleZ);
  trRotate->Translate(position);


  vtkMAFSmartPointer<vtkTransformPolyDataFilter> tpRotate;
  tpRotate->SetInput(glyph->GetOutput());
  tpRotate->SetTransform(trRotate);
  tpRotate->Update();

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> tp;
  tp->SetInput(tpRotate->GetOutput());
  tp->SetTransform(tr);
  tp->Update();

  vtkMAFSmartPointer<vtkPolyDataMapper> mapper;
  mapper->SetInput(tp->GetOutput());

  vtkMAFSmartPointer<vtkActor> actor;
  actor->SetMapper(mapper);

  m_Rwi3DView->m_RenFront->AddActor(actor);

//   double b[6];
//   tpTranslateOfVectorLenght->GetOutput()->GetBounds(b);
//   m_Rwi3DView->CameraReset(b);
//   m_Rwi3DView->CameraUpdate();

  wxString workingDir = mafGetApplicationDirectory().c_str();
  workingDir << "/ExternalTool";
  wxString oldDir = wxGetCwd();
  wxSetWorkingDirectory(workingDir);

  FILE *outputFile = fopen("Results.out","r");
  if (!outputFile)
  {
    mafLogMessage(_("Results.out doesn't exist"));  

    return;
  }

  char *line = new char[100];
  strcpy(line,"");

  wxString text = "";
  fgets(line, 100, outputFile);
  text += line;
  text += "\n";
  fgets(line, 100, outputFile);
  wxString vectorLine = line;
  vectorLine.Replace("\t"," ");
  text += vectorLine;
  delete[]line;
  wxSetWorkingDirectory(oldDir);

  vtkNEW(m_ComponentsTextActors);
  m_ComponentsTextActors->SetInput(text.c_str());
  m_ComponentsTextActors->SetPosition(m_Rwi3DView->m_RenFront->GetSize()[0]-150,m_Rwi3DView->m_RenFront->GetSize()[1]-80);
  m_ComponentsTextActors->GetTextProperty()->SetBold(TRUE);
  m_Rwi3DView->m_RenFront->AddActor2D(m_ComponentsTextActors);
}

void iGuiDialogJointLoading::ShowLandmarks()
{
  mafVMELandmarkCloud *lnd = mafVMELandmarkCloud::SafeDownCast(FindVmeByTag(m_Input,TAG_LND_CLOUD));
  lnd->Update();

  vtkPolyData *pd = vtkPolyData::SafeDownCast(lnd->GetOutput()->GetVTKData());
  pd->Update();

  vtkMAFSmartPointer<vtkTransform> tr;
  tr->SetMatrix(lnd->GetOutput()->GetAbsMatrix()->GetVTKMatrix());

  vtkMAFSmartPointer<vtkSphereSource> sphere;
  sphere->SetRadius(10.0);
  sphere->Update();

  vtkMAFSmartPointer<vtkMAFExtendedGlyph3D> glyph;
  glyph->SetInput(pd);
  glyph->SetSource(sphere->GetOutput());
  glyph->Update();

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> tp;
  tp->SetInput(glyph->GetOutput());
  tp->SetTransform(tr);           
  tp->Update();

  vtkMAFSmartPointer<vtkPolyDataMapper> mapper;
  mapper->SetInput(tp->GetOutput());

  lnd->GetMaterial()->m_Prop->SetOpacity(0.5);

  vtkMAFSmartPointer<vtkActor> actor;
  actor->SetMapper(mapper);
  actor->SetProperty(lnd->GetMaterial()->m_Prop);

  m_Rwi3DView->m_RenFront->AddActor(actor);
  m_Rwi3DView->m_RenFront->Render();
  double b[6];
  lnd->GetOutput()->GetBounds(b);
//   m_Rwi3DView->CameraReset(b);
//   m_Rwi3DView->CameraUpdate();
}
