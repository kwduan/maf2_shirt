/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialogValidationValues.cpp,v $
Language:  C++
Date:      $Date: 2012-03-15 10:49:24 $
Version:   $Revision: 1.1.2.9 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/


#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iGuiDialogValidationValues.h"
#include "iDecl.h"
#include "iGuiDialogPrimaryStability.h"
#include "iGuiDialogJointLoading.h"

#include "mafGUIButton.h"
#include "mafGUIValidator.h"
#include "mafRWI.h"
#include "mafDeviceButtonsPadMouse.h"
#include "mafVMERoot.h"
#include "mafVMEGroup.h"
#include "iAttributeWorkflow.h"
#include "iOpUploadMultiVMERefactor.h"

#include "vtkRenderWindow.h"

#include <wx/busyinfo.h>
#include <wx/image.h>
#include "lhpUser.h"
#include "lhpUploadDownloadVMEHelper.h"

//----------------------------------------------------------------------------
iGuiDialogValidationValues::iGuiDialogValidationValues(const wxString& title,mafDeviceButtonsPadMouse *mouse,mafVMERoot *input,int id,int jointType,double weight,double height,double PCA[35],bool training /* = false */)
: mafGUIDialog(title,mafCLOSEWINDOW | mafRESIZABLE)
//----------------------------------------------------------------------------
{
  int x_pos,y_pos,w,h;
  mafGetFrame()->GetPosition(&x_pos,&y_pos);
  this->GetSize(&w,&h);
  this->SetSize(x_pos+5,y_pos+5,w,h);

  m_ButtonSend = NULL;
  m_ButtonUpload = NULL;

  m_PatientId = id;
  m_Input = input;
  m_Joint = jointType;
  m_Mouse = mouse;
  m_PatientHeight = height;
  m_PatientWeight = weight;
  for (int i=0;i<35;i++)
  {
    m_PCA[i] = PCA[i];
  }
  m_Training = training;
  
  CreateGui();
}
//----------------------------------------------------------------------------
iGuiDialogValidationValues::~iGuiDialogValidationValues()
//----------------------------------------------------------------------------
{
}
//----------------------------------------------------------------------------
void iGuiDialogValidationValues::OnEvent(mafEventBase *maf_event)
//----------------------------------------------------------------------------
{

  if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
  {
    switch(e->GetId())
    {
    case ID_UPLOAD_BUTTON:
      {
        UploadStudy();
      }
      break;
    case ID_SEND_BUTTON:
      {
        SendDataToDB();
      }
      break;
    case ID_JOINT_LOADING_BUTTON:
      {
        iGuiDialogJointLoading *dialog = new iGuiDialogJointLoading(_("Joint Loading"),m_Mouse,m_Input,m_Vector,m_Training);
        dialog->ShowModal();
      }
      break;

    case ID_PRIMARY_STABILITY_BUTTON:
      {
        iGuiDialogPrimaryStability *dialog = new iGuiDialogPrimaryStability(m_Input,_("Primary Stability"),m_Mouse,m_PatientWeight,m_PatientHeight,m_Stability,m_Uncertainty,m_Training);
        dialog->ShowModal();
      }
      break;
      case ID_CANCEL_BUTTON:
        {
          this->EndModal(wxCANCEL);
        }
      break;
    case ID_ACCEPT_BUTTON:
      {
        if (!m_Training)
        {
	        wxBusyInfo *wait = new wxBusyInfo(_("Wait: Generating report .... "));
	
	        mafSleep(1000);
	
	        delete wait;
	
	        int result = wxMessageBox(_("would you like start a new planning?"),_(""),wxOK|wxCENTRE|wxCANCEL);
	        if (result == wxOK)
	        {
	          this->EndModal(wxID_RESTART_PLANNING);
	        }
	        else
	        {
	          this->EndModal(wxID_RETURN_NEXT);
	        }
        }
        else
        {
          this->EndModal(wxID_RETURN_NEXT);
        }
            
      }
      break;
    case ID_REJECT_BUTTON:
      {
        mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(mafVMERoot::SafeDownCast(m_Input->GetRoot()),TAG_GROUP_ATTRIBUTES));
        iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));
        attribute->StudyIsUploadedOff();
        groupAttributes->SetAttribute("Workflow",attribute);
        this->EndModal(wxID_RETURN_PREV);
      }
        break;
    default:
        mafEventMacro(*e);
    }
  }
}
//----------------------------------------------------------------------------
void iGuiDialogValidationValues::ComputeValidation()
//----------------------------------------------------------------------------
{
  wxBusyInfo *wait = new wxBusyInfo(_("Wait: ... "));

  //FAKE COMPUTATION!!!!
  for (int i=0;i<100;i++)
  {
    m_Gauge->SetValue(i);
    mafYield();
    ::wxSafeYield(NULL,true); //fix on bug #2082
    mafSleep(10);
  }

  mafYield();

  m_Gauge->SetValue(100);

  m_BmpStability->Show(true);
  m_BmpJoint->Show(true);

  ::wxSafeYield(NULL,true); //fix on bug #2082

  delete wait;

  m_ButtonReject->Enable(true);
  m_ButtonCancel->Enable(true);
  m_ButtonAccept->Enable(true);
  if (m_ButtonUpload)
  {
    m_ButtonUpload->Enable(true);
  }
  if (m_ButtonSend)
  {
    mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(mafVMERoot::SafeDownCast(m_Input->GetRoot()),TAG_GROUP_ATTRIBUTES));
    iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));
    m_ButtonSend->Enable(attribute->StudyIsUploaded());
  }
  m_ButtonPrimaryStability->Enable(true);
  m_ButtonJointLoading->Enable(true);
}
//----------------------------------------------------------------------------
void iGuiDialogValidationValues::CreateGui()
//----------------------------------------------------------------------------
{
    //Change default frame to our dialog
    wxWindow* oldFrame = mafGetFrame();
    mafSetFrame(this);

    wxPoint p = wxDefaultPosition;

    wxBoxSizer *labelSizer3 = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText *labelJoint = new wxStaticText  (this, -1, _("Joint Loading") , wxDefaultPosition, wxSize(200,20), 0 );
    mafString bmpFilename3 = mafGetApplicationDirectory().c_str();
    if (m_Joint == ID_JOINT_HIP && iGuiDialogJointLoading::ExecuteExternalApplication(m_PatientWeight,m_PatientId,m_Vector,m_Input,m_Training))
    {
      bmpFilename3<<"/icons/tick.ico";
    }
    else
    {
      bmpFilename3<<"/icons/error.ico";
    }
    wxIcon image3;
    image3.LoadFile(bmpFilename3.GetCStr(),wxBITMAP_TYPE_ICO);
    m_BmpJoint = new wxStaticBitmap(this,-1,image3);

    wxBoxSizer *labelSizer1 = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText *labelStability = new wxStaticText  (this, -1, _("Primary Stability\n[% Uncertainty]") , wxDefaultPosition, wxSize(200,40), 0 );
    mafString bmpFilename1 = mafGetApplicationDirectory().c_str();
    if (iGuiDialogPrimaryStability::ExecuteExternalApplication(m_Input,m_PCA,m_PatientWeight,m_PatientHeight,m_Stability,m_Uncertainty,m_Training))
    {
      if (m_Uncertainty<=10)
      {
        bmpFilename1<<"/icons/tick.ico";
      }
      else if (m_Uncertainty<=20 && m_Uncertainty>10)
      {
        bmpFilename1<<"/icons/error.ico";
      }
      else if (m_Uncertainty>20)
      {
        bmpFilename1<<"/icons/error.ico";
      }
    }
    else
    {
      bmpFilename1<<"/icons/error.ico";
    }
    
    wxIcon image1;
    image1.LoadFile(bmpFilename1.GetCStr(),wxBITMAP_TYPE_ICO);
    m_BmpStability = new wxStaticBitmap(this,-1,image1);
    labelSizer1->Add(labelStability,0);
    labelSizer1->Add(m_BmpStability,0);

    
    labelSizer3->Add(labelJoint,0);
    labelSizer3->Add(m_BmpJoint,0);

    m_BmpStability->Show(false);
    m_BmpJoint->Show(false);

    wxBoxSizer *gaugeSizer = new wxBoxSizer(wxHORIZONTAL);

    m_Gauge = new wxGauge(this, -1, 100,p,wxSize(350,20),wxGA_SMOOTH);
    m_Gauge->SetForegroundColour( *wxRED );
    m_Gauge->Show(TRUE);

    gaugeSizer->Add(m_Gauge,0);


    wxBoxSizer *buttonSizer2 = new wxBoxSizer(wxHORIZONTAL);
    m_ButtonPrimaryStability = new mafGUIButton(this, ID_PRIMARY_STABILITY_BUTTON,_("primary stability"), p, wxSize(150,20));
    m_ButtonPrimaryStability->SetValidator(mafGUIValidator(this,ID_PRIMARY_STABILITY_BUTTON,m_ButtonPrimaryStability));
    m_ButtonPrimaryStability->Enable(false);

    m_ButtonJointLoading = new mafGUIButton(this, ID_JOINT_LOADING_BUTTON,_("joint loading"), p, wxSize(150,20));
    m_ButtonJointLoading->SetValidator(mafGUIValidator(this,ID_JOINT_LOADING_BUTTON,m_ButtonJointLoading));
    m_ButtonJointLoading->Enable(false);

    buttonSizer2->Add(m_ButtonPrimaryStability,0);
    buttonSizer2->Add(m_ButtonJointLoading,0);

    wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);

    m_ButtonAccept = new mafGUIButton(this, ID_ACCEPT_BUTTON,_("accept"), p, wxSize(100,20));
    m_ButtonAccept->SetValidator(mafGUIValidator(this,ID_ACCEPT_BUTTON,m_ButtonAccept));
    m_ButtonAccept->Enable(false);

    m_ButtonCancel = new mafGUIButton(this, ID_CANCEL_BUTTON,_("cancel"), p, wxSize(100,20));
    m_ButtonCancel->SetValidator(mafGUIValidator(this,ID_CANCEL_BUTTON,m_ButtonCancel));
    m_ButtonCancel->Enable(false);

    m_ButtonReject = new mafGUIButton(this, ID_REJECT_BUTTON,_("reject"), p, wxSize(100,20));
    m_ButtonReject->SetValidator(mafGUIValidator(this,ID_REJECT_BUTTON,m_ButtonReject));
    m_ButtonReject->Enable(false);

    if (!m_Training)
    {
	    m_ButtonSend = new mafGUIButton(this, ID_SEND_BUTTON,_("send to DB"), p, wxSize(100,20));
	    m_ButtonSend->SetValidator(mafGUIValidator(this,ID_SEND_BUTTON,m_ButtonSend));
	    m_ButtonSend->Enable(false);

      m_ButtonUpload = new mafGUIButton(this, ID_UPLOAD_BUTTON,_("upload"), p, wxSize(100,20));
	    m_ButtonUpload->SetValidator(mafGUIValidator(this,ID_UPLOAD_BUTTON,m_ButtonUpload));
	    m_ButtonUpload->Enable(false);
      
    }

    buttonSizer->Add(m_ButtonReject,0);
    buttonSizer->Add(m_ButtonCancel,0);
    if (!m_Training)
    {
      buttonSizer->Add(m_ButtonUpload,0);
      buttonSizer->Add(m_ButtonSend,0);
    }
    buttonSizer->Add(m_ButtonAccept,0);

    m_GuiSizer->Add(labelSizer1,0,wxCENTRE,5);
    wxStaticText* div2 = new wxStaticText(this, -1, "",p, wxSize(100, 5), 0);
    m_GuiSizer->Add(div2,0,wxCENTRE,5);
    m_GuiSizer->Add(labelSizer3,0,wxCENTRE,5);
    wxStaticText* div3 = new wxStaticText(this, -1, "",p, wxSize(100, 5), 0);
    m_GuiSizer->Add(div3,0,wxCENTRE,5);
    m_GuiSizer->Add(gaugeSizer,0,wxCENTRE,5);
    wxStaticText* div4 = new wxStaticText(this, -1, "",p, wxSize(100, 5), 0);
    m_GuiSizer->Add(div4,0,wxCENTRE,5);
    m_GuiSizer->Add(buttonSizer2,0,wxCENTRE,5);
    m_GuiSizer->Add(buttonSizer,0,wxCENTRE,5);

    mafSetFrame(oldFrame);
}

void iGuiDialogValidationValues::SendDataToDB()
{
  wxBusyInfo wait (_("Please Wait"));

  wxString workingDir = mafGetApplicationDirectory().c_str();
  workingDir << "/bin";
  wxString oldDir = wxGetCwd();
  wxSetWorkingDirectory(workingDir);

  mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(mafVMERoot::SafeDownCast(m_Input->GetRoot()),TAG_GROUP_ATTRIBUTES));
  iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));

  std::string filenameTest = mafGetApplicationDirectory().c_str();
  filenameTest.append("/bin/mxl_dict.py");
  ofstream f(filenameTest);
  if(!f) 
  {
    return;
  }

  f << "{\n";
  f << "'date_of_birth' : '1950-1-1',\n";
  f << "'gender' : '";
  f << attribute->GetPatientSex();
  f << "',\n";
  f << "'I_MXLD_WEIGHT' : '";
  f << attribute->GetPatientWeight();
  f << "',\n";
  f << "'I_MXLD_HEIGHT' : '";
  f << attribute->GetPatientHeight();
  f << "',\n";
  f << "'I_MXLD_DIAGNOSIS' : '";
  f << attribute->GetDiagnosis();
  f << "',\n";
  f << "'I_MXLD_JOINT' : '";
  switch(attribute->GetJoint())
  {
  case ID_JOINT_HIP:
    f << "Hip";
    break;
  case ID_JOINT_KNEE:
    f << "Knee";
    break;
  case ID_JOINT_SHOULDER:
    f << "Shoulder";
    break;
  }
  f << "',\n";
  f << "'I_MXLD_LATERALITY' : '";
  switch(attribute->GetSide())
  {
  case ID_SIDE_LEFT:
    f << "Left";
    break;
  case ID_SIDE_RIGHT:
    f << "Right";
    break;
  }
  f << "',\n";
  f << "'I_MXLD_ANTEVERSION' : '";
  f << attribute->GetHipAnteversion();
  f << "',\n";
  f << "'I_MXLD_CCD' : '";
  f << attribute->GetHipCCD();
  f << "',\n";
  f << "'I_MXLD_NECKLENGTH' : '";
  f << attribute->GetHipNeckLength();
  f << "',\n";
  f << "'I_MXLD_FEMORALCANAL' : '";
  f << attribute->GetHipFemoralCanal();
  f << "',\n";
  f << "'I_MXLD_FEMORALHEAD' : '";
  f << attribute->GetHipFemoralHead();
  f << "',\n";

  int numOfProsthesis = attribute->GetNumberOfProsthesis();
  for (int i=0;i<numOfProsthesis;i++)
  {
    mafString model = attribute->GetModel(i);
    mafString manufacture = attribute->GetManufacture(i);

    if (attribute->GetProsthesisType(manufacture,model) == "Acetabular")
    {
      f << "'I_MXLD_P_BRAND' : '";
      f << manufacture;
      f << "',\n";
      f << "'I_MXLD_P_MODEL' : '";
      f << model;
      f << "',\n";
      wxString sizes = "";
      std::map<mafString,mafString> componentsSizes;
      attribute->GetComponentsSizes(manufacture,model,componentsSizes);
      std::map<mafString,mafString>::iterator iter;
      for(iter = componentsSizes.begin();iter != componentsSizes.end();iter++)
      {
        sizes << iter->second.GetCStr();
        sizes << " ";
      }

      f << "'I_MXLD_P_SIZE' : '";
      f << sizes;
      f << "',\n";

      break;
    }
  }

  for (int i=0;i<numOfProsthesis;i++)
  {
    mafString model = attribute->GetModel(i);
    mafString manufacture = attribute->GetManufacture(i);

    if (attribute->GetProsthesisType(manufacture,model) != "Acetabular")
    {
      f << "'I_MXLD_D_BRAND' : '";
      f << manufacture;
      f << "',\n";
      f << "'I_MXLD_D_MODEL' : '";
      f << model;
      f << "',\n";
      wxString sizes = "";
      std::map<mafString,mafString> componentsSizes;
      attribute->GetComponentsSizes(manufacture,model,componentsSizes);
      std::map<mafString,mafString>::iterator iter;
      for(iter = componentsSizes.begin();iter != componentsSizes.end();iter++)
      {
        sizes << iter->second.GetCStr();
        sizes << " ";
      }

      f << "'I_MXLD_D_SIZE' : '";
      f << sizes;
      f << "',\n";

      break;
    }
  }


  wxArrayString output;
  wxArrayString errors;
  wxString command2execute;

  f << "'I_MXLD_STRESS_COEFF' : '";
  for (int i=0;i<16;i++)
  {
    f << m_Stability[i];
    f << " ";
  }
  f << "',\n";
  
  f << "'I_MXLD_STRESS_ERR' : '";
  f << m_Uncertainty;
  f << "',\n";
  
  double pca[35];
  attribute->GetPCA(pca);
  f << "'I_MXLD_SSIM_COFF' : '";
  for (int i=0;i<35;i++)
  {
    f << pca[i];
    f << " ";
  }
  f << "',\n";

  f << "'I_MXLD_LOADING_X' : '";
  f << m_Vector[0];
  f << "',\n";
  f << "'I_MXLD_LOADING_Y' : '";
  f << m_Vector[1];
  f << "',\n";
  f << "'I_MXLD_LOADING_Z' : '";
  f << m_Vector[2];
  f << "',\n";
  f << "'I_MXLD_IMAGING_DATA' : '";
  f << attribute->GetRxUri();
  f << "',\n";
  f << "}";

  f.close();

  lhpUploadDownloadVMEHelper *uploadDownloadVMEHelper = NULL;
  uploadDownloadVMEHelper = new lhpUploadDownloadVMEHelper(this->m_Listener);
  wxString pythonInterpreterFullPath = uploadDownloadVMEHelper->GetPythonInterpreterFullPath();

  command2execute = pythonInterpreterFullPath;
  command2execute.Append("mxl_client.py mxl_dict.py");

  mafLogMessage( _T("Executing command: '%s'"), command2execute.c_str() );
  long pid = -1;
  if (pid = wxExecute(command2execute, wxEXEC_SYNC) != 0)
  {
    wxMessageBox("Error during storing data in OpenClinica!", wxMessageBoxCaptionStr, wxSTAY_ON_TOP | wxOK);
    mafLogMessage(_T("mxl client terminated with exit code %d."),command2execute.c_str(), pid);
  }

  wxSetWorkingDirectory(oldDir);
}

void iGuiDialogValidationValues::UploadStudy()
{
  iOpUploadMultiVMERefactor *op = new iOpUploadMultiVMERefactor("Upload");
  op->SetListener(this);
  op->SetDontStopAfterOpRunOn();
  op->SetInput(m_Input->GetRoot());
  op->OpRun();
  if ( op->GetOpRunResult() == OP_RUN_OK )
  {
    op->OpDo();
  }

  mafString rxUri = op->m_RxUri;

  cppDEL(op);

  mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(mafVMERoot::SafeDownCast(m_Input->GetRoot()),TAG_GROUP_ATTRIBUTES));
  iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));

  if (!rxUri.IsEmpty())
  {
    attribute->StudyIsUploadedOn();
    attribute->SetRxUri(rxUri);
    groupAttributes->SetAttribute("Workflow",attribute);
  }

  if (m_ButtonSend)
  {
    mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(mafVMERoot::SafeDownCast(m_Input->GetRoot()),TAG_GROUP_ATTRIBUTES));
    iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));
    m_ButtonSend->Enable(attribute->StudyIsUploaded());
  }

}