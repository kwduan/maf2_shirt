/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialog3DReconstrution.h,v $
Language:  C++
Date:      $Date: 2012-03-21 07:25:44 $
Version:   $Revision: 1.1.2.6 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it
=========================================================================*/

#ifndef __iGuiDialog3DReconstrution_H__
#define __iGuiDialog3DReconstrution_H__

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class mafRWI;
class mafDeviceButtonsPadMouse;
class mafVMERoot;
class mafGUIButton;
class vtkActor;
class vtkPolyDataMapper;
class vtkTexture;
class vtkWindowLevelLookupTable;
class vtkPlaneSource;

//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "iGuiDefines.h"
#include "mafGUIDialog.h"

//----------------------------------------------------------------------------
// Const:
//----------------------------------------------------------------------------

/** iGuiDialog3DReconstrution
*/
class I_GUI_EXPORT iGuiDialog3DReconstrution : public mafGUIDialog
{
public:

  iGuiDialog3DReconstrution (const wxString& title,mafDeviceButtonsPadMouse *mouse,mafVMERoot *input, mafString rxFileName,int step,int id,bool training = false);
  virtual ~iGuiDialog3DReconstrution (); 

  /** process events coming from other components */
  /*virtual*/ void OnEvent(mafEventBase *maf_event);

  /** invoke the 3D reconstrution algorithm */
	void ComputeReconstrution();

  /** Enable/disable gui elements for show results modality */
  void EnableShowModalityGui();

  /** Enable/disable gui elements for waiting results modality */
  void EnableWaitingModalityGui();

  /** load and show the reconstructed model */
  void LoadAndShowReconstruction();

  /** show the reconstructed model from tree */
  void ShowReconstruction();

  /** load and show the landmarks */
  void LoadAndShowLandmarks();

  /** show landmakrs from tree */
  void ShowLandmarks();

  /** return pca */
  void GetPCA(double pca[35]);

  /** return vector neck */
  void GetNeckVector(double neckVector[3]);

  /** check if 3d reconstruction has finished */
  bool CheckResults(bool showResults = true);

protected:

  enum ID_GUIs
  {
    ID_CANCEL = MINID,
    ID_ACCEPT_BUTTON,
    ID_REJECT_BUTTON,
    ID_CHECK_RESULTS,
  };

  /** Create the interface */
  void CreateGui();

  /** visualize the first vme in the three with the tag "RX" */
  void ShowRX();

  mafVMERoot *m_Input;

  mafRWI *m_Rwi3DView;

  mafDeviceButtonsPadMouse *m_Mouse;

  vtkPolyDataMapper	*m_SliceMapper;
  vtkTexture *m_SliceTexture;
  vtkActor *m_SliceActor;
  vtkActor *m_ReconstructionActor;
  vtkActor *m_LandmarksActor;
  vtkWindowLevelLookupTable	*m_SliceLookupTable;
  vtkPlaneSource *m_SlicePlane;

  mafGUIButton *m_ButtonAccept;
  mafGUIButton *m_ButtonCancel;
  mafGUIButton *m_ButtonReject;
  mafGUIButton *m_ButtonCheckResults;

  mafString m_FileRX;

  double m_SliceBounds[6];
  double m_PCA[35];
  double m_NeckVector[3];

  int m_CurrentStep;
  int m_PatientCaseId;
  mafString m_Status;
  mafString m_Path;
  bool m_Training;
  wxStaticText *m_StatusLabel;

};
#endif
