/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialogListCase.cpp,v $
Language:  C++
Date:      $Date: 2012-02-01 09:22:09 $
Version:   $Revision: 1.1.2.13 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/


#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iGuiDialogListCase.h"
#include "iDecl.h"
#include "iAttributeWorkflow.h"

#include "mafVMEStorage.h"
#include "mafString.h"
#include "mafGUIButton.h"
#include "mafGUIValidator.h"
#include "mafVMERoot.h"
#include "iGuiSettingsPlanning.h"
#include "iGuiDialog3DReconstrution.h"
#include "mafTagArray.h"
#include "mafTagItem.h"

#include <wx/tokenzr.h>
#include <wx/dir.h>
#include <wx/image.h>
#include "mafVMEGroup.h"

//----------------------------------------------------------------------------
iGuiDialogListCase::iGuiDialogListCase(const wxString& title,mafString cacheFilePath /* = "/cache/cacheFile.txt" */,bool enableAddButton /* = true */, bool showProgress /* = true */ )
: mafGUIDialog(title,mafCLOSEWINDOW | mafRESIZABLE)
//----------------------------------------------------------------------------
{

  int x_pos,y_pos,w,h;
  mafGetFrame()->GetPosition(&x_pos,&y_pos);
  this->GetSize(&w,&h);
  this->SetSize(x_pos+5,y_pos+5,w,h);

  fgSizer = NULL;

  m_CacheFilePath = cacheFilePath;
  m_EnableAddButton = enableAddButton;

  m_ShowProgress = showProgress;

	CreateGui();
}
//----------------------------------------------------------------------------
iGuiDialogListCase::~iGuiDialogListCase()
//----------------------------------------------------------------------------
{
}
//----------------------------------------------------------------------------
void iGuiDialogListCase::OnEvent(mafEventBase *maf_event)
//----------------------------------------------------------------------------
{
	if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
	{
		switch(e->GetId())
		{
    case ID_CHECKS:
      {
        m_ButtonAccept->Enable(false);
        for (int i=0;i<m_Elements.size();++i)
        {
          if (m_CheckValues[i] == m_OldCheckValues[i] && m_CheckValues[i] == TRUE)
          {
            m_CheckValues[i] = FALSE;
          }

          m_OldCheckValues[i] = m_CheckValues[i];

          if (m_CheckValues[i] == TRUE)
          {
            m_ButtonAccept->Enable(true);
          }

          m_CheckButtons[i]->SetValue(m_CheckValues[i]);
          m_CheckButtons[i]->Update();
        }
        this->Update();
      }
      break;
		case ID_CANCEL_BUTTON:
			this->EndModal(wxCANCEL);
			break;
		case ID_ACCEPT_BUTTON:
			this->EndModal(wxID_RETURN_NEXT);
			break;
    case ID_ADD_CASE_BUTTON:
      this->EndModal(wxID_RETURN_ADD_CASE);
      break;
		default:
			mafEventMacro(*e);
		}
	}
}
//----------------------------------------------------------------------------
void iGuiDialogListCase::CreateGui()
//----------------------------------------------------------------------------
{
	wxPoint p = wxDefaultPosition;

	// m_Grid = new wxGrid(this,ID_CASE_GRID,wxDefaultPosition,wxSize(500,350));

  LoadCache();

	wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);

	m_ButtonAccept = new mafGUIButton(this, ID_ACCEPT_BUTTON,_("accept"), p, wxSize(100,20));
	m_ButtonAccept->SetValidator(mafGUIValidator(this,ID_ACCEPT_BUTTON,m_ButtonAccept));
  m_ButtonAccept->Enable(false);

	mafGUIButton *b_cancel = new mafGUIButton(this, ID_CANCEL_BUTTON,_("cancel"), p, wxSize(100,20));
	b_cancel->SetValidator(mafGUIValidator(this,ID_CANCEL_BUTTON,b_cancel));
  if (m_EnableAddButton)
  {
	  mafGUIButton *b_add = new mafGUIButton(this, ID_ADD_CASE_BUTTON,_("add"), p, wxSize(100,20));
	  b_add->SetValidator(mafGUIValidator(this,ID_ADD_CASE_BUTTON,b_add));
	  
	  buttonSizer->Add(b_add,0);
  }
  buttonSizer->Add(b_cancel,0);
	buttonSizer->Add(m_ButtonAccept,0);

	m_GuiSizer->Add(fgSizer,0,wxALL,5);	
  m_GuiSizer->Add(new wxStaticText(this,-1,_("")),0,wxCENTRE,5);	
	m_GuiSizer->Add(buttonSizer,0,wxCENTRE,5);
}
//----------------------------------------------------------------------------
void iGuiDialogListCase::LoadCache()
//----------------------------------------------------------------------------
{
  mafString cacheFile = mafGetApplicationDirectory().c_str();
  cacheFile << m_CacheFilePath;

  mafLogMessage(_("Opening cache file : %s"),cacheFile.GetCStr());

  FILE *file = fopen(cacheFile.GetCStr(),"r");

  if (!file)
  {
    mafLogMessage(_("ERROR: cache file doesn't exist"));
    return;
  }

  while (!feof(file))
  {
    char *line = new char[100];
    strcpy(line,"");
    //fscanf(file,"%s",line);

//     if (strcmp(line,"") == 0)
//     {
//       break;
//     }

    fgets(line, 100, file);

    wxString lineWX = line;
    // lineWX.Trim();
    wxStringTokenizer tkz(line,wxT('#')); // Read t values
    int num_t = tkz.CountTokens();

    if (num_t != 8 && num_t != 0)
    {
      mafLogMessage(_("ERROR: wrong file format"));
      return;
    }
    if (num_t == 0)
    {
      //End Line
      break;
    }

    int i = 0;
    grid_element element;
    while (tkz.HasMoreTokens())
    {
      wxString token = tkz.GetNextToken();
      token.Replace("\n", "");

      switch (i)
      {
      case ID:
        element.m_Id = atoi(token.c_str());
        break;
      case NAME:
        element.m_Name = (token.c_str());
        break;
      case DIAGNOSIS:
        element.m_Diagnosis = (token.c_str());
        break;
      case JOINT:
        element.m_Joint = atoi(token.c_str());
        break;
      case SIDE:
        element.m_Side = atoi(token.c_str());
        break;
      case FILE_NAME:
        element.m_FileNameRX = (token.c_str());
        break;
      case WEIGHT:
        element.m_Weight = atof(token.c_str());
        break;
      case HEIGHT:
        element.m_Height = atof(token.c_str());
        break;
      }

      ++i;
    }

    m_Elements.push_back(element);

    delete []line;

  }

  wxPoint dp = wxDefaultPosition;
  wxFont boldFont = wxFont(wxSystemSettings::GetFont(wxSYS_DEFAULT_GUI_FONT));

  if (m_ShowProgress)
  {
  	fgSizer =  new wxFlexGridSizer( m_Elements.size()+1, 9, 1, 1 );
  } 
  else
  {
    fgSizer =  new wxFlexGridSizer( m_Elements.size()+1, 7, 1, 1 );
  }
  wxStaticText *emptyLab = new wxStaticText(this,-1,_(""),dp,wxSize(30,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
  fgSizer->Add(emptyLab);
  wxStaticText *idLab = new wxStaticText(this,-1,_("id"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
  idLab->SetFont(boldFont);
  fgSizer->Add(idLab);
  wxStaticText *nameLab = new wxStaticText(this,-1,_("name"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
  nameLab->SetFont(boldFont);
  fgSizer->Add(nameLab);
  wxStaticText *diagnosisLab = new wxStaticText(this,-1,_("diagnosis"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
  diagnosisLab->SetFont(boldFont);
  fgSizer->Add(diagnosisLab);
  wxStaticText *jointLab = new wxStaticText(this,-1,_("joint"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
  jointLab->SetFont(boldFont);
  fgSizer->Add(jointLab);
  wxStaticText *sideLab = new wxStaticText(this,-1,_("side"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
  sideLab->SetFont(boldFont);
  fgSizer->Add(sideLab);
  if (m_ShowProgress)
  {
	  wxStaticText *reconstructionLab = new wxStaticText(this,-1,_("reconstruction"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
	  reconstructionLab->SetFont(boldFont);
	  fgSizer->Add(reconstructionLab);
	  wxStaticText *implantLab = new wxStaticText(this,-1,_("implant"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
	  implantLab->SetFont(boldFont);
	  fgSizer->Add(implantLab);
	  wxStaticText *planningLab = new wxStaticText(this,-1,_("planning"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
	  planningLab->SetFont(boldFont);
	  fgSizer->Add(planningLab);
  }
  else
  {
    wxStaticText *scoreLab = new wxStaticText(this,-1,_("score"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
    scoreLab->SetFont(boldFont);
    fgSizer->Add(scoreLab);
  }

  wxStaticText **labels = new wxStaticText*[m_Elements.size()*5];
  m_CheckButtons = new wxCheckBox *[m_Elements.size()];
  m_CheckValues = new int[m_Elements.size()];
  m_OldCheckValues = new int[m_Elements.size()];

  mafString okICO = mafGetApplicationDirectory().c_str();
  okICO<<"/icons/tick.ico";
  mafString errorICO = mafGetApplicationDirectory().c_str();
  errorICO<<"/icons/error.ico";
  mafString waitICO = mafGetApplicationDirectory().c_str();
  waitICO<<"/icons/wait.ico";

  for (int i=0;i<m_Elements.size();++i)
  {
    m_CheckValues[i] = FALSE;
    m_OldCheckValues[i] = FALSE;
    m_CheckButtons[i] = new wxCheckBox(this,ID_CHECKS,"",dp,wxSize(30,22),wxALIGN_RIGHT);
    m_CheckButtons[i]->SetValidator( mafGUIValidator(this,ID_CHECKS,m_CheckButtons[i],&m_CheckValues[i]) );
    if (i%2 == 0)
    {
      m_CheckButtons[i]->SetBackgroundColour(wxColour(200,200,200));
    }
    else
    {
      m_CheckButtons[i]->SetBackgroundColour(wxColour(110,110,110));
    }
    fgSizer->Add(m_CheckButtons[i]);
    labels[i] = new wxStaticText(this,-1,wxString::Format("%d",m_Elements[i].m_Id),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
    labels[i+1] = new wxStaticText(this,-1,m_Elements[i].m_Name.GetCStr(),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
    labels[i+2] = new wxStaticText(this,-1,m_Elements[i].m_Diagnosis.GetCStr(),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
    switch(m_Elements[i].m_Joint)
    {
    case ID_JOINT_HIP:
      labels[i+3] = new wxStaticText(this,-1,_("Hip"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
      break;
    case ID_JOINT_KNEE:
      labels[i+3] = new wxStaticText(this,-1,_("Knee"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
      break;
    case ID_JOINT_SHOULDER:
      labels[i+3] = new wxStaticText(this,-1,_("Shoulder"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
      break;
    }

    switch(m_Elements[i].m_Side)
    {
    case ID_SIDE_LEFT:
      labels[i+4] = new wxStaticText(this,-1,_("Left"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
      break;
    case ID_SIDE_RIGHT:
      labels[i+4] = new wxStaticText(this,-1,_("Right"),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
      break;
    }

    for (int k=0;k<5;k++)
    {
      if (i%2 == 0)
      {
        labels[i+k]->SetBackgroundColour(wxColour(200,200,200));
      }
      else
      {
        labels[i+k]->SetBackgroundColour(wxColour(110,110,110));
      }

      fgSizer->Add(labels[i+k]);
    }

    if (!m_ShowProgress)
    {
      mafString fileName = mafGetApplicationDirectory().c_str();
      fileName<<"/download/";
      fileName<<m_Elements[i].m_Id;
      fileName<<"/Download";
      fileName<<m_Elements[i].m_Id;
      fileName<<".msf";
      if(wxFileExists(fileName.GetCStr()) )
      {

        mafVMEStorage *storage;
        storage = mafVMEStorage::New();
        storage->SetURL(fileName.GetCStr());
        storage->Restore();
        mafVMERoot *root;
        root = storage->GetRoot();
        root->Initialize();
        
        mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(root,TAG_GROUP_ATTRIBUTES));
        if (groupAttributes != NULL)
        {
          iAttributeWorkflow *attribute = NULL;
          attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));
          attribute->GetScore();

          wxStaticText *labelScore = new wxStaticText(this,-1,wxString::Format("%d",attribute->GetScore()),dp,wxSize(100,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );

          if (i%2 == 0)
          {
            labelScore->SetBackgroundColour(wxColour(200,200,200));
          }
          else
          {
            labelScore->SetBackgroundColour(wxColour(110,110,110));
          }

          fgSizer->Add(labelScore);
        }

        mafDEL(storage);
      }

      continue;
    }

    wxIcon image1;
    wxIcon image2;
    wxIcon image3;

	  image1.LoadFile(errorICO.GetCStr(),wxBITMAP_TYPE_ICO);
	  image2.LoadFile(errorICO.GetCStr(),wxBITMAP_TYPE_ICO);
	  image3.LoadFile(errorICO.GetCStr(),wxBITMAP_TYPE_ICO);

    mafString fileName = mafGetApplicationDirectory().c_str();
    fileName<<"/cache/";
    fileName<<m_Elements[i].m_Id;
	  fileName<<"/";
	  fileName<<m_Elements[i].m_Id;
	  fileName<<".msf";

    if(wxFileExists(fileName.GetCStr()) )
    {
      
      mafVMEStorage *storage;
      storage = mafVMEStorage::New();
      storage->SetURL(fileName.GetCStr());
      mafVMERoot *root;
      root = storage->GetRoot();
      root->Initialize();

      int res = storage->Restore();

      iAttributeWorkflow *attribute = NULL;
      mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(root,TAG_GROUP_ATTRIBUTES));
      if (groupAttributes == NULL)
      {
        
        mafNEW(groupAttributes);
        groupAttributes->GetTagArray()->SetTag(TAG_GROUP_ATTRIBUTES,"");
        groupAttributes->SetName("Attributes");
        groupAttributes->ReparentTo(root);
        groupAttributes->Update();

        attribute = iAttributeWorkflow::SafeDownCast(root->GetAttribute("Workflow"));//RetrocompatibilitÓ

        groupAttributes->SetAttribute("Workflow",attribute);

        storage->Store();
      }
      else
      {
        attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));
      }

      attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));
      if (attribute != NULL)
      {
        int currentStep = attribute->GetCurrentStep();

        if (currentStep == ID_STEP_3D_RECONSTRUTION_WAITING)
        {
          iGuiDialog3DReconstrution *recontruction = new iGuiDialog3DReconstrution("",NULL,NULL,"",ID_STEP_3D_RECONSTRUTION_WAITING,m_Elements[i].m_Id);
          if (recontruction->CheckResults(false))
          {
            double pca[35];
            recontruction->GetPCA(pca);
            attribute->SetCurrentStep(ID_STEP_3D_RECONSTRUTION_SHOW);
            attribute->SetPCA(pca);
            groupAttributes->SetAttribute("Workflow",attribute);
            storage->Store();

            currentStep = ID_STEP_3D_RECONSTRUTION_SHOW;
          }
          else
          {
            image1.LoadFile(waitICO.GetCStr(),wxBITMAP_TYPE_ICO);
          }
          cppDEL(recontruction);
        }

        if (currentStep > ID_STEP_3D_RECONSTRUTION_WAITING)
        {
          image1.LoadFile(okICO.GetCStr(),wxBITMAP_TYPE_ICO);
        }
        else if(currentStep < ID_STEP_3D_RECONSTRUTION_WAITING)
        {
          image1.LoadFile(errorICO.GetCStr(),wxBITMAP_TYPE_ICO);
        }
        if (currentStep>ID_STEP_CHOICE_IMPLANT)
        {
          image2.LoadFile(okICO.GetCStr(),wxBITMAP_TYPE_ICO);
        }
        else
        {
          image2.LoadFile(errorICO.GetCStr(),wxBITMAP_TYPE_ICO);
        }
        if (currentStep>ID_STEP_PLACING_IMPLANT)
        {
          image3.LoadFile(okICO.GetCStr(),wxBITMAP_TYPE_ICO);
        }
        else
        {
          image3.LoadFile(errorICO.GetCStr(),wxBITMAP_TYPE_ICO);
        }
      }

      mafDEL(storage);
    }
    if (m_ShowProgress)
    {
      wxStaticBitmap *bmp1 = new wxStaticBitmap(this,-1,image1,dp,wxSize(100,22),wxALIGN_CENTER);
      wxStaticBitmap *bmp2 = new wxStaticBitmap(this,-1,image2,dp,wxSize(100,22),wxALIGN_CENTER);
      wxStaticBitmap *bmp3 = new wxStaticBitmap(this,-1,image3,dp,wxSize(100,22),wxALIGN_CENTER);

      if (i%2 == 0)
      {
        bmp1->SetBackgroundColour(wxColour(200,200,200));
        bmp2->SetBackgroundColour(wxColour(200,200,200));
        bmp3->SetBackgroundColour(wxColour(200,200,200));
      }
      else
      {
        bmp1->SetBackgroundColour(wxColour(110,110,110));
        bmp2->SetBackgroundColour(wxColour(110,110,110));
        bmp3->SetBackgroundColour(wxColour(110,110,110));
      }

	    fgSizer->Add(bmp1);
	    fgSizer->Add(bmp2);
	    fgSizer->Add(bmp3);
    }
    
  }

  fclose(file);
}
//----------------------------------------------------------------------------
mafString iGuiDialogListCase::GetFileNameCaseSelected()
//----------------------------------------------------------------------------
{
  int iSelected = GetSelectionIndex();
  if (iSelected != -1)
  {
    return m_Elements[iSelected].m_FileNameRX;
  }
  else
  {
    return "";
  }
}
//----------------------------------------------------------------------------
int iGuiDialogListCase::GetSelectionIndex()
//----------------------------------------------------------------------------
{
  for (int i=0;i<m_Elements.size();i++)
  {
    if (m_CheckValues[i] == TRUE)
    {
      return i;
    }
  }

  return -1;
}
//----------------------------------------------------------------------------
int iGuiDialogListCase::GetSideCaseSelected()
//----------------------------------------------------------------------------
{
  int iSelected = GetSelectionIndex();
  if (iSelected != -1)
  {
    return m_Elements[iSelected].m_Side;
  }
  else
  {
    return -1;
  }
}
//----------------------------------------------------------------------------
mafString iGuiDialogListCase::GetPatientNameCaseSelected()
//----------------------------------------------------------------------------
{
  int iSelected = GetSelectionIndex();
  if (iSelected != -1)
  {
    return m_Elements[iSelected].m_Name;
  }
  else
  {
    return "";
  }
}
//----------------------------------------------------------------------------
int iGuiDialogListCase::GetJointCaseSelected()
//----------------------------------------------------------------------------
{
  int iSelected = GetSelectionIndex();
  if (iSelected != -1)
  {
    return m_Elements[iSelected].m_Joint;
  }
  else
  {
    return -1;
  }
}
//----------------------------------------------------------------------------
mafString iGuiDialogListCase::GetDiagnosisCaseSelected()
//----------------------------------------------------------------------------
{
  int iSelected = GetSelectionIndex();
  if (iSelected != -1)
  {
    return m_Elements[iSelected].m_Diagnosis;
  }
  else
  {
    return "";
  }
}

//----------------------------------------------------------------------------
mafString iGuiDialogListCase::GetCaseId()
//----------------------------------------------------------------------------
{
  int iSelected = GetSelectionIndex();
  if (iSelected != -1)
  {
    return m_Elements[iSelected].m_Id;
  }
  else
  {
    return "";
  }
}

//----------------------------------------------------------------------------
int iGuiDialogListCase::GetLastCaseId()
//----------------------------------------------------------------------------
{
	return m_Elements.back().m_Id;
}
//----------------------------------------------------------------------------
void iGuiDialogListCase::Update3DReconstructionState()
//----------------------------------------------------------------------------
{
  for (int iEl = 0;iEl<m_Elements.size();iEl++)
  {
    int id = m_Elements[iEl].m_Id;

    mafVMEStorage storage;
  }
}
//----------------------------------------------------------------------------
int iGuiDialogListCase::GetListCaseSize()
//----------------------------------------------------------------------------
{
  return m_Elements.size();
}
//----------------------------------------------------------------------------
double iGuiDialogListCase::GetWeightCaseSelected()
//----------------------------------------------------------------------------
{
  int iSelected = GetSelectionIndex();
  if (iSelected != -1)
  {
    return m_Elements[iSelected].m_Weight;
  }
  else
  {
    return -1.0;
  }
}
//----------------------------------------------------------------------------
double iGuiDialogListCase::GetHeightCaseSelected()
//----------------------------------------------------------------------------
{
  int iSelected = GetSelectionIndex();
  if (iSelected != -1)
  {
    return m_Elements[iSelected].m_Height;
  }
  else
  {
    return -1.0;
  }
}