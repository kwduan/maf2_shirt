/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialogChoiceImplant.cpp,v $
Language:  C++
Date:      $Date: 2012-03-22 07:12:03 $
Version:   $Revision: 1.1.2.7 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/


#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iGuiDialogChoiceImplant.h"

#include "mafGUIButton.h"
#include "mafGUIValidator.h"
#include "mafRWI.h"
#include "mafDeviceButtonsPadMouse.h"
#include "mmuDOMTreeErrorReporter.h"
#include "mafXMLElement.h"
#include "mmuXMLDOMElement.h"
#include "mafXMLString.h"
#include "mafVMERoot.h"
#include "mafVMEGroup.h"
#include "mafMSFImporter.h"
#include "mafTagArray.h"
#include "mafStorage.h"
#include "mafVMEStorage.h"
#include "mafNodeIterator.h"
#include "mafCrypt.h"
#include "mafVMESurface.h"
#include "mafMatrix.h"
#include "mafMatrixVector.h"
#include "mafTransform.h"
#include "mafVMELandmarkCloud.h"
#include "mafVMELandmark.h"
#include "mmaMaterial.h"

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>

#include <xercesc/framework/LocalFileInputSource.hpp>
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/sax/ErrorHandler.hpp>

#include "vtkMAFSmartPointer.h"
#include "vtkRenderWindow.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyData.h"
#include "vtkTransform.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkPlaneSource.h"
#include "vtkMath.h"

#include <wx/busyinfo.h>
#include <wx/zipstrm.h>
#include <wx/zstream.h>
#include <wx/ffile.h>
#include <wx/wfstream.h>
#include <wx/fs_zip.h>
#include <wx/dir.h>
#include <wx/filename.h>
#include <wx/fs_mem.h>
#include <wx/image.h>
#include <wx/tokenzr.h>
#include "iAttributeWorkflow.h"

//----------------------------------------------------------------------------
iGuiDialogChoiceImplant::iGuiDialogChoiceImplant(const wxString& title,mafVMERoot *input,int prosthesisType,int side,double neckVector[3], bool training /* = false */)
: mafGUIDialog(title,mafCLOSEWINDOW | mafRESIZABLE)
//----------------------------------------------------------------------------
{

  m_ProsthesisType = prosthesisType;
  m_Side = side;
  m_Input = input;

  m_ModelData = NULL;
  m_AcetabularModelData = NULL;

  m_EnableBuildAcetabularProsthesis = TRUE;
  m_EnableBuildFemoralProsthesis = TRUE;

  m_Training = training;

  for (int i=0;i<3;i++)
  {
    m_NeckVector[i] = neckVector[i];
  }

  wxFileSystem::AddHandler(new wxZipFSHandler);

  int x_pos,y_pos,w,h;
  mafGetFrame()->GetPosition(&x_pos,&y_pos);
  this->GetSize(&w,&h);
  this->SetSize(x_pos+5,y_pos+5,w,h);

  if (!training)
  {
    m_TagProsthesisDbAcetabural = TAG_PROSTHESIS_DB_ACETABULAR;
    m_TagProsthesisDb = TAG_PROSTHESIS_DB;
    m_TagProsthesisGroup = TAG_PROSTHESIS_GROUP;
    m_TagAttributesGroup = TAG_GROUP_ATTRIBUTES;
    m_AttributeName = "Workflow";

  }
  else
  {
    m_TagProsthesisDbAcetabural = TAG_PROSTHESIS_DB_ACETABULAR_TRAINING;
    m_TagProsthesisDb = TAG_PROSTHESIS_DB_TRAINING;
    m_TagProsthesisGroup = TAG_PROSTHESIS_GROUP_TRAINING;
    m_TagAttributesGroup = TAG_GROUP_ATTRIBUTES_TRAINING;
    m_AttributeName = "Workflow Training";
  }

  mafVME *group = FindVmeByTag(m_Input,m_TagAttributesGroup);
  if (group->GetAttribute(m_AttributeName) != NULL)
  {
    iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(group->GetAttribute(m_AttributeName));
    attribute->RemoveAllProsthesis();
  }


  FindAndRemove(m_Input,m_TagProsthesisDbAcetabural);//remove old DB
  FindAndRemove(m_Input,m_TagProsthesisDb);//remove old DB

  FindAndRemove(m_Input,m_TagProsthesisGroup);//remove old DB
  FindAndRemove(m_Input,m_TagProsthesisGroup);//remove old DB

  CreateGui();

  OpenDBProsthesis();

  UpdateManufacturersList();
  UpdateModelsList();

  if (m_ProsthesisType == ID_JOINT_HIP)
  {
    UpdateAcetabularManufacturersList();
    UpdateAcetabularModelsList();
  }
}
//----------------------------------------------------------------------------
iGuiDialogChoiceImplant::~iGuiDialogChoiceImplant()
//----------------------------------------------------------------------------
{
  if (m_ModelData)
  {
    m_ModelData->ReparentTo(NULL);
    // mafDEL(m_ModelData);
  }
  if (m_AcetabularModelData)
  {
    m_AcetabularModelData->ReparentTo(NULL);
    // mafDEL(m_AcetabularModelData);
  }
}
//----------------------------------------------------------------------------
void iGuiDialogChoiceImplant::OnEvent(mafEventBase *maf_event)
//----------------------------------------------------------------------------
{
    if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
    {
        switch(e->GetId())
        {
        case ID_ACETABULAR_ENABLE:
          {
            m_AcetabularManufacturesList->Enable(m_EnableBuildAcetabularProsthesis == TRUE);
            m_AcetabularModelsList->Enable(m_EnableBuildAcetabularProsthesis == TRUE);
            m_AcetabularComponentsList->Enable(m_EnableBuildAcetabularProsthesis == TRUE);
            m_AcetabularSizeList->Enable(m_EnableBuildAcetabularProsthesis == TRUE);

            if (m_EnableBuildAcetabularProsthesis == TRUE)
            {
              bool enable = (m_EnableBuildFemoralProsthesis == FALSE || m_ModelsList->GetSelection() != -1) && m_AcetabularModelsList->GetSelection() != -1;
              m_ButtonAccept->Enable(enable);
            }
            else
            {
              bool enable = m_EnableBuildFemoralProsthesis == TRUE && m_ModelsList->GetSelection() != -1;
              m_ButtonAccept->Enable(enable);
            }
          }
          break;
        case ID_FEMORAL_ENABLE:
          {
            m_ManufacturesList->Enable(m_EnableBuildFemoralProsthesis == TRUE);
            m_ModelsList->Enable(m_EnableBuildFemoralProsthesis == TRUE);
            m_ComponentsList->Enable(m_EnableBuildFemoralProsthesis == TRUE);
            m_SizeList->Enable(m_EnableBuildFemoralProsthesis == TRUE);

            if (m_EnableBuildFemoralProsthesis == TRUE)
            {
              bool enable = (m_EnableBuildAcetabularProsthesis == FALSE || m_AcetabularModelsList->GetSelection() != -1) && m_ModelsList->GetSelection() != -1;
            	m_ButtonAccept->Enable(enable);
            }
            else
            {
              bool enable = m_EnableBuildAcetabularProsthesis == TRUE && m_AcetabularModelsList->GetSelection() != -1;
              m_ButtonAccept->Enable(enable);
            }
          }
          break;
        case ID_SIZE_LIST:
          {
            if (m_SizeList->GetSelection()!=-1)
            {
            	m_ComponetsSizes[m_ComponentsList->GetStringSelection()].m_Selection = m_SizeList->GetSelection();
            }
          }
          break;
        case ID_ACETABULAR_SIZE_LIST:
          {
            if (m_AcetabularSizeList->GetSelection()!=-1)
            {
              m_AcetabularComponetsSizes[m_AcetabularComponentsList->GetStringSelection()].m_Selection = m_AcetabularSizeList->GetSelection();
            }
          }
          break;
        case ID_COMPONENTS_LIST:
          {
            if (m_ComponentsList->GetSelection()!=-1)
            {
            	UpdateSizesList();
            }
          }
          break;
        case ID_ACETABULAR_COMPONENTS_LIST:
          {
            if (m_AcetabularComponentsList->GetSelection()!=-1)
            {
              UpdateAcetabularSizesList();
            }
          }
          break;
        case ID_MODEL_LIST:
          {
            if (m_ModelsList->GetSelection()!=-1)
            {
	            UpdateComponentsList();
	            UpdateSizesList();

              if (m_EnableBuildAcetabularProsthesis == TRUE && m_ProsthesisType == ID_JOINT_HIP)
              {
              	m_ButtonAccept->Enable(m_AcetabularModelsList->GetSelection() != -1);
              }
              else
              {
                m_ButtonAccept->Enable(true);
              }
            }
          }
          break;
        case ID_ACETABULAR_MODEL_LIST:
          {
            if (m_AcetabularModelsList->GetSelection()!=-1)
            {
              UpdateAcetabularComponentsList();
              UpdateAcetabularSizesList();

              if (m_EnableBuildFemoralProsthesis == TRUE && m_ProsthesisType == ID_JOINT_HIP)
              {
                m_ButtonAccept->Enable(m_ModelsList->GetSelection() != -1);
              }
              else
              {
                m_ButtonAccept->Enable(true);
              }
            }
          }
          break;
        case ID_MANUFACTURE_LIST:
          {
            if (m_ManufacturesList->GetSelection()!=-1)
            {
            	UpdateModelsList();
            }
          }
          break;
        case ID_ACETABULAR_MANUFACTURE_LIST:
          {
            if (m_AcetabularManufacturesList->GetSelection()!=-1)
            {
              UpdateAcetabularModelsList();
            }
          }
          break;
        case ID_CANCEL:
          {
            this->EndModal(wxCANCEL);
          }
          break;
        case ID_ACCEPT_BUTTON:
          {
            GenerateProsthesis();
            this->EndModal(wxID_RETURN_NEXT);
          }
          break;
        case ID_REJECT_BUTTON:
            this->EndModal(wxID_RETURN_PREV);
            break;
        default:
            mafEventMacro(*e);
        }
    }
}
//----------------------------------------------------------------------------
void iGuiDialogChoiceImplant::CreateGui()
//----------------------------------------------------------------------------
{
  //Change default frame to our dialog
  wxWindow* oldFrame = mafGetFrame();
  mafSetFrame(this);

  wxPoint p = wxDefaultPosition;

  m_Gui = new mafGUI(this);
  m_Gui->Reparent(this);

  if (m_ProsthesisType == ID_JOINT_HIP)
  {
    m_Gui->Bool(ID_FEMORAL_ENABLE,_("Femoral"),&m_EnableBuildFemoralProsthesis,1);
  }

  m_Gui->Label(_("Manufactures"));
  m_ManufacturesList = m_Gui->ListBox(ID_MANUFACTURE_LIST,"");
  m_Gui->Label(_("Models"));
  m_ModelsList = m_Gui->ListBox(ID_MODEL_LIST,"");
  m_Gui->Label(_("Components"));
  m_ComponentsList = m_Gui->ListBox(ID_COMPONENTS_LIST,"");
  m_Gui->Label(_("Sizes"));
  m_SizeList = m_Gui->ListBox(ID_SIZE_LIST,"");

  wxBoxSizer *topSizer = new wxBoxSizer(wxHORIZONTAL);

  topSizer->Add(m_Gui,0);

  if (m_ProsthesisType == ID_JOINT_HIP)
  {
    m_GuiAcetabular = new mafGUI(this);
    m_GuiAcetabular->Reparent(this);
    m_GuiAcetabular->Bool(ID_ACETABULAR_ENABLE,_("Acetabular"),&m_EnableBuildAcetabularProsthesis,1);

    m_GuiAcetabular->Label(_("Manufactures"));
    m_AcetabularManufacturesList = m_GuiAcetabular->ListBox(ID_ACETABULAR_MANUFACTURE_LIST,"");
    m_GuiAcetabular->Label(_("Models"));
    m_AcetabularModelsList = m_GuiAcetabular->ListBox(ID_ACETABULAR_MODEL_LIST,"");
    m_GuiAcetabular->Label(_("Components"));
    m_AcetabularComponentsList = m_GuiAcetabular->ListBox(ID_ACETABULAR_COMPONENTS_LIST,"");
    m_GuiAcetabular->Label(_("Sizes"));
    m_AcetabularSizeList = m_GuiAcetabular->ListBox(ID_ACETABULAR_SIZE_LIST,"");
    m_GuiAcetabular->FitGui();
    m_GuiAcetabular->Update();

    topSizer->Add(m_GuiAcetabular,0);
  }

  wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);

  m_ButtonAccept = new mafGUIButton(this, ID_ACCEPT_BUTTON,_("accept"), p, wxSize(100,20));
  m_ButtonAccept->SetValidator(mafGUIValidator(this,ID_ACCEPT_BUTTON,m_ButtonAccept));
  m_ButtonAccept->Enable(false);//Accept will be enabled only when the user has choised a prosthesis

  m_ButtonCancel = new mafGUIButton(this, ID_CANCEL,_("cancel"), p, wxSize(100,20));
  m_ButtonCancel->SetValidator(mafGUIValidator(this,ID_CANCEL,m_ButtonCancel));

  m_ButtonReject = new mafGUIButton(this, ID_REJECT_BUTTON,_("reject"), p, wxSize(100,20));
  m_ButtonReject->SetValidator(mafGUIValidator(this,ID_REJECT_BUTTON,m_ButtonReject));


  buttonSizer->Add(m_ButtonReject,0);
  buttonSizer->Add(m_ButtonCancel,0);
  buttonSizer->Add(m_ButtonAccept,0);

  m_Gui->FitGui();
  m_Gui->Update();

  m_GuiSizer->Add(topSizer,0,wxCENTRE,5);
  m_GuiSizer->Add(buttonSizer,0,wxCENTRE,5);

  mafSetFrame(oldFrame);
}
//-------------------------------------------------------------------------
int iGuiDialogChoiceImplant::OpenDBProsthesis()
//-------------------------------------------------------------------------
{
  //Open the file xml with manufacture and model information
  try {
    XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Initialize();
  }
  catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
    // Do your failure processing here
    return MAF_ERROR;
  }

  XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser *XMLParser = new XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser;

  XMLParser->setValidationScheme(XERCES_CPP_NAMESPACE_QUALIFIER XercesDOMParser::Val_Auto);
  XMLParser->setDoNamespaces(false);
  XMLParser->setDoSchema(false);
  XMLParser->setCreateEntityReferenceNodes(false);

  mmuDOMTreeErrorReporter *errReporter = new mmuDOMTreeErrorReporter();
  XMLParser->setErrorHandler(errReporter);

  mafString FileName = mafGetApplicationDirectory().c_str();
  FileName<<PATH_PROSTHESIS_DB_FILE;

  if(!wxFile::Exists(FileName.GetCStr()))
  {
    mafLogMessage(_("DB file don't exist"));
    return MAF_ERROR;
  }

  try {
    XMLParser->parse(FileName.GetCStr());
    int errorCount = XMLParser->getErrorCount(); 

    if (errorCount != 0)
    {
      // errors while parsing...
      mafErrorMessage("Errors while parsing XML file");
    }
    else
    {
      // extract the root element and wrap inside a mafXMLElement
      XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument *doc = XMLParser->getDocument();
      XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *root = doc->getDocumentElement();
      assert(root);

      mafString name = mafXMLString(root->getTagName());
      if (name == "DB")
      {
        mafString version = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)root)->getAttribute(mafXMLString("Version")));

        //Check the DB version
        if (version != PROSTHESIS_DB_VERSION)
        {
          return MAF_ERROR;
        }
      }

      //Reset old DB
      m_DBProsthesisInformation.clear();

      XERCES_CPP_NAMESPACE_QUALIFIER DOMNodeList *manufacturesChildren=root->getChildNodes();
      for (unsigned int i = 0; i<manufacturesChildren->getLength();i++)
      {
        XERCES_CPP_NAMESPACE_QUALIFIER DOMNode *manufacture_node=manufacturesChildren->item(i);
        if (manufacturesChildren->item(i)->getNodeType()==XERCES_CPP_NAMESPACE_QUALIFIER DOMNode::ELEMENT_NODE)
        {
          XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *manufacture_element = (XERCES_CPP_NAMESPACE_QUALIFIER DOMElement*)manufacture_node;
          mafString nameElement = ""; 
          nameElement = mafXMLString(manufacture_element->getTagName());
          if (nameElement == "MANUFACTURE")
          {                        
            mafString nameManufacture = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)manufacture_element)->getAttribute(mafXMLString("Name")));
            XERCES_CPP_NAMESPACE_QUALIFIER DOMNodeList *modelsChildren=manufacture_element->getChildNodes();
            for (unsigned int j = 0; j<modelsChildren->getLength();j++)
            {
              XERCES_CPP_NAMESPACE_QUALIFIER DOMNode *model_node=modelsChildren->item(j);
              if (modelsChildren->item(j)->getNodeType()==XERCES_CPP_NAMESPACE_QUALIFIER DOMNode::ELEMENT_NODE)
              {
                XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *model_element = (XERCES_CPP_NAMESPACE_QUALIFIER DOMElement*)model_node;

                nameElement = "";
                nameElement = mafXMLString(model_element->getTagName());
                if (nameElement == "MODEL")
                {
                  mafString nameModel = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)model_element)->getAttribute(mafXMLString("Name")));
                  mafString file = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)model_element)->getAttribute(mafXMLString("FileName")));            
                  
                  DB_PROSTHESIS_INFORMATION info;
                  info.m_Manufacture = nameManufacture;
                  info.m_Model = nameModel;
                  info.m_FileName = file;

                  

                  if (m_ProsthesisType == ID_JOINT_HIP)
                  {
                    mafString type = mafXMLString(((XERCES_CPP_NAMESPACE_QUALIFIER DOMElement *)model_element)->getAttribute(mafXMLString("Type")));
                    info.m_ProsthesisType = type;
                    if (type == "Femoral" || type == "Resurfacing")
                    {
                      m_DBProsthesisInformation.push_back(info);
                    }
                    else if (type == "Acetabular")
                    {
                      m_AcetabularDBProsthesisInformation.push_back(info);
                    }
                  }
                  else
                  {
                    info.m_ProsthesisType = "";
                    m_DBProsthesisInformation.push_back(info);
                  }
                  mafLogMessage(wxString::Format("DB reading :\n\tManufacture %s\n\tModel %s\n\tFile %s\n",nameManufacture.GetCStr(),nameModel.GetCStr(),file.GetCStr()));

                }
              }
            }
          }
        }
      }
    }
  }
  catch (const XERCES_CPP_NAMESPACE_QUALIFIER XMLException& toCatch) {
    return MAF_ERROR;
  }
  catch (const XERCES_CPP_NAMESPACE_QUALIFIER DOMException& toCatch) {
    return MAF_ERROR;
  }
  catch (...) {
    return MAF_ERROR;
  }

  cppDEL (errReporter);
  delete XMLParser;

  // terminate the XML library
  XERCES_CPP_NAMESPACE_QUALIFIER XMLPlatformUtils::Terminate();

  return MAF_OK;
}
//----------------------------------------------------------------------------
void iGuiDialogChoiceImplant::UpdateManufacturersList()
//----------------------------------------------------------------------------
{
  if (m_ManufacturesList == NULL)
  {
    return;
  }

  m_ManufacturesList->Clear();

  for (int i=0;i<m_DBProsthesisInformation.size();i++)
  {
    mafString newName = m_DBProsthesisInformation[i].m_Manufacture;
    if (m_ManufacturesList->FindString(newName.GetCStr()) == wxNOT_FOUND)
    {
      m_ManufacturesList->Append(newName.GetCStr());
    }
  }

  if (m_Training)
  {
	  std::vector<mafVME *> prosthesisGroup;
	  FindVmeByTag(m_Input,TAG_PROSTHESIS_GROUP,prosthesisGroup);

    if (prosthesisGroup.size() == 0)
    {
      m_EnableBuildFemoralProsthesis = FALSE;
      this->OnEvent(&mafEvent(this,ID_FEMORAL_ENABLE));
      return;
    }

	  for(int i=0;i<prosthesisGroup.size()>0;i++)
	  {
	    mafString name = prosthesisGroup[i]->GetName();
	
	    wxStringTokenizer tkz(name.GetCStr(),wxT('-')); // Read t values
	    int num_t = tkz.CountTokens();
	
	    if (num_t == 2)
	    {
	      wxString manufacture = tkz.GetNextToken();
	      wxString model = tkz.GetNextToken();
	
	      if (m_ManufacturesList->FindString(manufacture) != wxNOT_FOUND)
	      {
	        m_ManufacturesList->SetSelection((m_ManufacturesList->FindString(manufacture)));
	        this->OnEvent(&mafEvent(this,ID_MANUFACTURE_LIST));
	        return;
	      }
	
	    }
	  }

    m_EnableBuildFemoralProsthesis = FALSE;
    m_Gui->Update();
    this->OnEvent(&mafEvent(this,ID_FEMORAL_ENABLE));
    return;
  }

  m_ManufacturesList->SetSelection(0);
  m_ManufacturesList->Update();
}
//----------------------------------------------------------------------------
void iGuiDialogChoiceImplant::UpdateAcetabularSizesList()
//----------------------------------------------------------------------------
{
  if (m_AcetabularSizeList == NULL)
  {
    return;
  }

  m_AcetabularSizeList->Clear();

  mafString component = m_AcetabularComponentsList->GetStringSelection();
  SIZES sizes = m_AcetabularComponetsSizes[component];
  for (int i=0;i<sizes.m_NumSizes;i++)
  {
    m_AcetabularSizeList->Append(sizes.m_Sizes[i].GetCStr());
  }

  m_AcetabularSizeList->SetSelection(m_AcetabularComponetsSizes[component].m_Selection);
  m_AcetabularSizeList->Update();
}
//----------------------------------------------------------------------------
void iGuiDialogChoiceImplant::UpdateSizesList()
//----------------------------------------------------------------------------
{
  wxBusyCursor cursor;
  if (m_SizeList == NULL)
  {
    return;
  }

  m_SizeList->Clear();

  mafString component = m_ComponentsList->GetStringSelection();
  SIZES sizes = m_ComponetsSizes[component];
  for (int i=0;i<sizes.m_NumSizes;i++)
  {
    m_SizeList->Append(sizes.m_Sizes[i].GetCStr());
  }

  m_SizeList->SetSelection(m_ComponetsSizes[component].m_Selection);
  m_SizeList->Update();
}
//----------------------------------------------------------------------------
void iGuiDialogChoiceImplant::UpdateModelsList()
//----------------------------------------------------------------------------
{
  wxBusyCursor cursor;
  if (m_ModelsList == NULL)
  {
    return;
  }

  m_ComponentsList->Clear();
  m_SizeList->Clear();
  m_ModelsList->Clear();

  mafString manufacture = m_ManufacturesList->GetStringSelection();
  for (int i=0;i<m_DBProsthesisInformation.size();i++)
  {    
    mafString nameManufacture = m_DBProsthesisInformation[i].m_Manufacture;
    if ( strcmp(manufacture.GetCStr(),nameManufacture.GetCStr()) == 0)
    {
      mafString newName = m_DBProsthesisInformation[i].m_Model;
      m_ModelsList->Append(newName.GetCStr());
    }
  }

  if (m_Training)
  {
	  std::vector<mafVME *> prosthesisGroup;
	  FindVmeByTag(m_Input,TAG_PROSTHESIS_GROUP,prosthesisGroup);
	  for(int i=0;i<prosthesisGroup.size()>0;i++)
	  {
	    mafString name = prosthesisGroup[i]->GetName();
	
	    wxStringTokenizer tkz(name.GetCStr(),wxT('-')); // Read t values
	    int num_t = tkz.CountTokens();
	
	    if (num_t == 2)
	    {
	      wxString manufacture = tkz.GetNextToken();
	      wxString model = tkz.GetNextToken();
	
	      if (m_ModelsList->FindString(model) != wxNOT_FOUND)
	      {
	        m_ModelsList->SetSelection((m_ModelsList->FindString(model)));
	        this->OnEvent(&mafEvent(this,ID_MODEL_LIST));
	        return;
	      }
	
	    }
	  }
  }

  /* m_ModelsList->SetSelection(0);
  m_ModelsList->Update();*/
}
//----------------------------------------------------------------------------
void iGuiDialogChoiceImplant::UpdateComponentsList()
//----------------------------------------------------------------------------
{
  wxBusyCursor cursor;
  wxBusyInfo wait_info(_("Opening the prosthesis file! Please Wait!"));

  m_ManufacturesList->Enable(false);
  m_ModelsList->Enable(false);
  m_ComponentsList->Enable(false);
  m_SizeList->Enable(false);

  mafString manufacture = m_ManufacturesList->GetStringSelection();
  mafString model = m_ModelsList->GetStringSelection();
  mafString file = (mafGetApplicationDirectory()+"/Config/DataBase/").c_str();

  for (int i=0;i<m_DBProsthesisInformation.size();i++)
  {
    if (m_DBProsthesisInformation[i].m_Manufacture == manufacture &&  m_DBProsthesisInformation[i].m_Model == model)
    {
      file += m_DBProsthesisInformation[i].m_FileName;
      break;
    }
  }

  mafString fileOpened=ZIPOpen(file.GetCStr());
  if(fileOpened=="")
  {
    wxMessageBox("Error Open DB!");
    return;
  }

  mafVMEStorage *storage;
  storage = mafVMEStorage::New();
  mafVMERoot *root;
  root = storage->GetRoot();
  root->Initialize();
  root->SetName("RootB");

  mafMSFImporter *importer = new mafMSFImporter; //Import prosthesis MSF
  importer->SetURL(fileOpened.GetCStr());
  importer->SetRoot(root);
  importer->Restore();

  if(!root->GetFirstChild())
  {
    mafDEL(storage);
    wxMessageBox(_("Error Parsing!"));
    return;
  }

  FindAndRemove(m_Input,m_TagProsthesisDb);//remove ald DB

  mafSmartPointer<mafVMEGroup> group; //Create new DB tree
#ifndef _DEBUG
  group->GetTagArray()->SetTag(mafTagItem("VISIBLE_IN_THE_TREE", 0.0));
#endif // _DEBUG
  group->GetTagArray()->SetTag(m_TagProsthesisDb,"");
  group->SetName("Prosthesis Model");
  group->ReparentTo(m_Input);

  mafNode::CopyTree(root->GetFirstChild(),group);

  m_ModelData=group;

  bool foundTag = false;
  mafNodeIterator *iter = group->NewIterator();
  for (mafNode *Inode = iter->GetFirstNode(); Inode; Inode = iter->GetNextNode())
  {
    if(strcmp(Inode->GetName(),model)==0)
    {
      m_ModelData = mafVMEGroup::SafeDownCast(Inode);
      foundTag = true;
      break;
    }
  }
  iter->Delete();

  if (!foundTag)
  {
    mafDEL(importer);
    mafDEL(storage);

    wxMessageBox(_("Prosthesis model not found!"));
    return;
  }

  mafLogMessage(_("Prosthesis model found"));

  m_ComponentsList->Clear();
  m_ComponetsSizes.clear();

  std::vector<mafMatrix> matrixComponents;
  iter = m_ModelData->NewIterator();
  for (mafNode *Inode = iter->GetFirstNode(); Inode; Inode = iter->GetNextNode())
  {
    if(Inode->GetTagArray()->GetTag("HIPOP_COMPONENT_VISIBILITY"))//Find if the node is a component of the Prosthesis DB
    {
      int numberOfTypes=Inode->GetNumberOfChildren();
      mafString *typeList;
      typeList=new mafString[numberOfTypes];
      for(int i=0;i<numberOfTypes;i++)
        typeList[i] = (Inode->GetChild(i)->GetName());//Create a list of all model for a particular components*/

      // mafSmartPointer<mafVMESurface> component;
      m_ComponentsList->Append(Inode->GetName());
      SIZES sizes;
      sizes.m_Sizes = typeList;
      sizes.m_NumSizes = numberOfTypes;
      sizes.m_Selection = 0;
      m_ComponetsSizes[Inode->GetName()] = sizes;
    }
  }

  iter->Delete();
  mafDEL(importer);
  mafDEL(storage);

  m_ComponentsList->SetSelection(0);
  m_ComponentsList->Update();

  m_ManufacturesList->Enable(true);
  m_ModelsList->Enable(true);
  m_ComponentsList->Enable(true);
  m_SizeList->Enable(true);

}
//----------------------------------------------------------------------------
mafString iGuiDialogChoiceImplant::ZIPOpen(mafString file)
//----------------------------------------------------------------------------
{
	mafString ZipFile,tmpDir,MSFFile;
	ZipFile = file;
	mafString zip_cache = wxPathOnly(file.GetCStr());
	if (zip_cache.IsEmpty())
	{
		//zip_cache = ::wxGetCwd();
    zip_cache = wxPathOnly(file.GetCStr());
	}
	//zip_cache = zip_cache + "\\~TmpData";
	if (!wxDirExists(zip_cache.GetCStr()))
		wxMkdir(zip_cache.GetCStr());
	tmpDir = zip_cache;

	wxString path, name, ext, complete_name, zfile, out_file;
	wxSplitPath(ZipFile.GetCStr(),&path,&name,&ext);
	complete_name = name + "." + ext;

	wxFSFile *zfileStream;
	wxZlibInputStream *zip_is;
	wxString pkg = "#zip:";
	wxString header_name = complete_name + pkg;
	int length_header_name = header_name.Length();
	bool enable_mid = false;

	wxFileSystem *zip_fs = new wxFileSystem();
	zip_fs->ChangePathTo(ZipFile.GetCStr());
	// extract m_File from the zip archive
	zfile = zip_fs->FindFirst(complete_name+pkg+name+"\\*.*");
	if (zfile == "")
	{
		enable_mid = true;
		zfile = zip_fs->FindFirst(complete_name+pkg+"\\*.*");
	}
	if (zfile == "")
	{
		zip_fs->CleanUpHandlers();
		cppDEL(zip_fs);
		//RemoveTempDirectory();
		return "";
	}
	wxSplitPath(zfile,&path,&name,&ext);
	complete_name = name + "." + ext;
	if (enable_mid)
		complete_name = complete_name.Mid(length_header_name);
	zfileStream = zip_fs->OpenFile(zfile);
	if (zfileStream == NULL)
	{
		zip_fs->CleanUpHandlers();
		cppDEL(zip_fs);
		//RemoveTempDirectory();
		return "";
	}
	zip_is = (wxZlibInputStream *)zfileStream->GetStream();
	out_file = tmpDir + "\\" + complete_name;
	char *buf;
	int s_size;
	std::ofstream out_file_stream;

	if(ext == "msf")
	{
		MSFFile = out_file;
		out_file_stream.open(out_file, std::ios_base::out);
	}
	else
	{
		out_file_stream.open(out_file, std::ios_base::binary);
	}
	s_size = zip_is->GetSize();
	buf = new char[s_size];
	zip_is->Read(buf,s_size);
	out_file_stream.write(buf, s_size);
	out_file_stream.close();
	delete[] buf;

	zfileStream->UnRef();
	delete zfileStream;

	while ((zfile = zip_fs->FindNext()) != "")
	{
		zfileStream = zip_fs->OpenFile(zfile);
		if (zfileStream == NULL)
		{
			zip_fs->CleanUpHandlers();
			cppDEL(zip_fs);
			//RemoveTempDirectory();
			return "";
		}
		zip_is = (wxZlibInputStream *)zfileStream->GetStream();
		wxSplitPath(zfile,&path,&name,&ext);
		complete_name = name + "." + ext;
		if (enable_mid)
			complete_name = complete_name.Mid(length_header_name);
		out_file = tmpDir + "\\" + complete_name;
		if(ext == "msf")
		{
			MSFFile = out_file;
			out_file_stream.open(out_file, std::ios_base::out);
		}
		else
			out_file_stream.open(out_file, std::ios_base::binary);
		s_size = zip_is->GetSize();
		buf = new char[s_size];
		zip_is->Read(buf,s_size);
		out_file_stream.write(buf, s_size);
		out_file_stream.close();
		delete[] buf;
		zfileStream->UnRef();
		delete zfileStream;
	}

	zip_fs->ChangePathTo(tmpDir.GetCStr(), TRUE);
	//zip_fs->CleanUpHandlers();
	cppDEL(zip_fs);

	if (MSFFile == "")
	{
		//if (!m_TestMode)
			mafMessage(_("compressed archive is not a valid msf file!"), _("Error"));
		return "";
	}

	/*wxString working_dir;
	working_dir = mafGetApplicationDirectory().c_str();
	wxSetWorkingDirectory(tmpDir.GetCStr());*/

	return MSFFile;
}
//----------------------------------------------------------------------------
void iGuiDialogChoiceImplant::GenerateProsthesis()
//----------------------------------------------------------------------------
{
  mafString manufacture = m_ManufacturesList->GetStringSelection();
  mafString model = m_ModelsList->GetStringSelection();
  mafString file = (mafGetApplicationDirectory()+"/Config/DataBase/").c_str();
  mafString type = "";

  for (int i=0;i<m_DBProsthesisInformation.size();i++)
  {
    if (m_DBProsthesisInformation[i].m_Manufacture == manufacture &&  m_DBProsthesisInformation[i].m_Model == model)
    {
      type = m_DBProsthesisInformation[i].m_ProsthesisType;
      break;
    }
  }

  if (m_EnableBuildFemoralProsthesis == TRUE)
  {
    bool canMirror;
	  mafSmartPointer<mafVMEGroup> prosthesisGroup;
	  mafString prosthesisName = m_ManufacturesList->GetStringSelection();
	  prosthesisName<<"-";
	  prosthesisName<<m_ModelsList->GetStringSelection();
	  prosthesisGroup->SetName(prosthesisName);
	  prosthesisGroup->ReparentTo(m_Input);
	  prosthesisGroup->GetTagArray()->SetTag(m_TagProsthesisGroup,"");
	  std::vector<mafVMESurface*> components;
	  std::vector<mafMatrix*> matrixComponents;

    if (m_ModelData->GetTagArray()->IsTagPresent("MODEL_MIRROR_FLAG"))
    {
      int tagValue = (int)m_ModelData->GetTagArray()->GetTag("MODEL_MIRROR_FLAG")->GetValueAsDouble();
      canMirror = tagValue == TRUE;
    }
    else
    {
      canMirror = false;
    }
	
	  mafNodeIterator *iter = m_ModelData->NewIterator();

    std::map<mafString,mafString> componetSizeSelected;
	  for (mafNode *Inode = iter->GetFirstNode(); Inode; Inode = iter->GetNextNode())
	  {
	    if(Inode->GetTagArray()->GetTag("HIPOP_COMPONENT_VISIBILITY"))//Find if the node is a component of the Prosthesis DB
	    {
	      for(int i=0;i<m_ComponetsSizes[Inode->GetName()].m_NumSizes;i++)
	      {
	        if(!strcmp(Inode->GetChild(i)->GetName(),m_ComponetsSizes[Inode->GetName()].m_Sizes[m_ComponetsSizes[Inode->GetName()].m_Selection]))//If I found the correct size
	        {

            componetSizeSelected[Inode->GetName()] = Inode->GetChild(i)->GetName();

	          mafString dataFile = (mafGetApplicationDirectory()+"/Config/DataBase/").c_str();
	          dataFile<<Inode->GetChild(i)->GetTagArray()->GetTag("HIPOP_DB_DATA_FILE")->GetValue();
	
	          vtkPolyData *polydata = LoadData(dataFile);

            vtkMAFSmartPointer<vtkTransform> t;
            if (type == "Resurfacing")
            {
            	t->RotateX(90.0);
            }
            vtkMAFSmartPointer<vtkTransformPolyDataFilter> tp;
            tp->SetInput(polydata);
            tp->SetTransform(t);
            tp->Update();
	
	          mafSmartPointer<mafVMESurface> component;
            mafString tagName;
            if (!m_Training)
            {
              tagName = TAG_PROSTHESIS_COMPONENT;
            }
            else
            {
              tagName = TAG_PROSTHESIS_COMPONENT_TRAINING;
            }
	          component->GetTagArray()->SetTag(tagName,"");
	          wxString componentName = "component-";
            componentName.append(Inode->GetChild(i)->GetName());
            component->SetName(componentName.c_str());
            component->GetMaterial()->m_Diffuse[0] = 0.0;
            component->GetMaterial()->m_Diffuse[1] = 0.0;
            component->GetMaterial()->m_Diffuse[2] = 0.8;
            component->GetMaterial()->UpdateProp();

            if (canMirror && m_Side == ID_SIDE_RIGHT)//Mirror
            {
              mafLogMessage("Mirror of the component");

	            vtkMAFSmartPointer<vtkTransform> mirrorTransform;
	            mirrorTransform->Scale(1,-1,1);
	            vtkMAFSmartPointer<vtkTransformPolyDataFilter> mirrorTransformFilter;
	            mirrorTransformFilter->SetTransform(mirrorTransform);
	            mirrorTransformFilter->SetInput(tp->GetOutput());
	            mirrorTransformFilter->Update();
	            
              component->SetData(mirrorTransformFilter->GetOutput(),0.0);

	            double xyz[3],rxyz[3];
	            component->GetOutput()->GetPose(xyz,rxyz);
	            xyz[1]=-xyz[1];
	            component->SetPose(xyz,rxyz,0.0);
	            component->Modified();
	            component->Update();
            }
            else
            {
              component->SetData(tp->GetOutput(),0.0);
            }

	          
	
	          polydata->Delete();
	
	          if (components.size()>0)
	          {
	            component->ReparentTo(components[components.size()-1]);
	            component->SetMatrix(*matrixComponents[matrixComponents.size()-1]);
	          }
	          else
	          {
	            component->ReparentTo(prosthesisGroup);
	          }
	
	          if(Inode->GetChild(i)->GetChild(0))
	          {
	            if(mafVMEGroup::SafeDownCast(Inode->GetChild(i)->GetChild(0))->GetMatrixVector()->GetMatrix(0))
	            {
	              matrixComponents.push_back(mafVMEGroup::SafeDownCast(Inode->GetChild(i)->GetChild(0))->GetMatrixVector()->GetMatrix(0));
	            }
	          }
	
	          components.push_back(component);
	
	        }// if(!strcmp(Inode->GetChild(i)->GetName(),m_ComponetsSizes[Inode->GetName()].m_Sizes[m_ComponetsSizes[Inode->GetName()].m_Selection]))//If I found the correct size
	      }// for(int i=0;i<m_ComponetsSizes[Inode->GetName()].m_NumSizes;i++)
	    }// if(Inode->GetTagArray()->GetTag("HIPOP_COMPONENT_VISIBILITY"))//Find if the node is a component of the Prosthesis DB
	  }// for (mafNode *Inode = iter->GetFirstNode(); Inode; Inode = iter->GetNextNode())

    mafVMESurface *reconstruction = mafVMESurface::SafeDownCast(FindVmeByTag(m_Input,TAG_3D_RECONSTRUCTION));
    reconstruction->Update();
    double bounds[6];
    reconstruction->GetOutput()->GetBounds(bounds);

    double center[3] = {(bounds[1]+bounds[0])/2.0,(bounds[3]+bounds[2])/2.0,(bounds[5]+bounds[4])/2.0};

    mafMatrix matrix;
    mafSmartPointer<mafTransform> transform;
    transform->SetMatrix(matrix);

    if (m_Side == ID_SIDE_RIGHT)
    {
      transform->RotateZ(150.0,POST_MULTIPLY);
    }
    
    /*transform->SetPosition(center[0],center[1],center[2]);*/

    prosthesisGroup->SetAbsMatrix(transform->GetMatrix());
    prosthesisGroup->Update();

    AutoplacingProsthesis(type,prosthesisGroup);

    std::vector<mafVME *> groups;
    mafVME *group = NULL;
    FindVmeByTag(m_Input,m_TagAttributesGroup,groups);

    for (int i = 0;i<groups.size();i++)
    {
      iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(groups[i]->GetAttribute(m_AttributeName));
      if (attribute != NULL)
      {
        group = groups[i];
        break;
      }
    }
    iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(group->GetAttribute(m_AttributeName));
    attribute->AddProsthesis(m_ManufacturesList->GetStringSelection(),m_ModelsList->GetStringSelection(),type,componetSizeSelected);
    group->SetAttribute(m_AttributeName,attribute);
  }
  
  if (m_EnableBuildAcetabularProsthesis == TRUE && m_ProsthesisType == ID_JOINT_HIP)
  {
    bool canMirror;

    mafSmartPointer<mafVMEGroup> prosthesisGroup;
    mafString prosthesisName = m_AcetabularManufacturesList->GetStringSelection();
    prosthesisName<<"-";
    prosthesisName<<m_AcetabularModelsList->GetStringSelection();
    prosthesisGroup->SetName(prosthesisName);
    prosthesisGroup->ReparentTo(m_Input);
    prosthesisGroup->GetTagArray()->SetTag(m_TagProsthesisGroup,"");
    std::vector<mafVMESurface*> components;
    std::vector<mafMatrix*> matrixComponents;

    if (m_AcetabularModelData->GetTagArray()->IsTagPresent("MODEL_MIRROR_FLAG"))
    {
      int tagValue = (int)m_AcetabularModelData->GetTagArray()->GetTag("MODEL_MIRROR_FLAG")->GetValueAsDouble();
      canMirror = tagValue == TRUE;
    }
    else
    {
      canMirror = false;
    }

    std::map<mafString,mafString> componetSizeSelected;
    mafNodeIterator *iter = m_AcetabularModelData->NewIterator();
    for (mafNode *Inode = iter->GetFirstNode(); Inode; Inode = iter->GetNextNode())
    {
      if(Inode->GetTagArray()->GetTag("HIPOP_COMPONENT_VISIBILITY"))//Find if the node is a component of the Prosthesis DB
      {
        for(int i=0;i<m_AcetabularComponetsSizes[Inode->GetName()].m_NumSizes;i++)
        {
          if(!strcmp(Inode->GetChild(i)->GetName(),m_AcetabularComponetsSizes[Inode->GetName()].m_Sizes[m_AcetabularComponetsSizes[Inode->GetName()].m_Selection]))//If I found the correct size
          {
            componetSizeSelected[Inode->GetName()] = Inode->GetChild(i)->GetName();

            mafString dataFile = (mafGetApplicationDirectory()+"/Config/DataBase/").c_str();
            dataFile<<Inode->GetChild(i)->GetTagArray()->GetTag("HIPOP_DB_DATA_FILE")->GetValue();

            vtkPolyData *polydata = LoadData(dataFile);

            mafSmartPointer<mafVMESurface> component;
            mafString tagName;
            if (!m_Training)
            {
              tagName = TAG_PROSTHESIS_COMPONENT;
            }
            else
            {
              tagName = TAG_PROSTHESIS_COMPONENT_TRAINING;
            }
            component->GetTagArray()->SetTag(tagName,"");
            wxString componentName = "component-";
            componentName.append(Inode->GetChild(i)->GetName());
            component->SetName(componentName.c_str());
            component->GetMaterial()->m_Diffuse[0] = 0.8;
            component->GetMaterial()->m_Diffuse[1] = 0.0;
            component->GetMaterial()->m_Diffuse[2] = 0.0;
            component->GetMaterial()->UpdateProp();
            
            if (canMirror && m_Side == ID_SIDE_RIGHT)//Mirror
            {
              mafLogMessage("Mirror of the component");

              vtkMAFSmartPointer<vtkTransform> mirrorTransform;
              mirrorTransform->Scale(1,-1,1);
              vtkMAFSmartPointer<vtkTransformPolyDataFilter> mirrorTransformFilter;
              mirrorTransformFilter->SetTransform(mirrorTransform);
              mirrorTransformFilter->SetInput(polydata);
              mirrorTransformFilter->Update();

              component->SetData(mirrorTransformFilter->GetOutput(),0.0);

              double xyz[3],rxyz[3];
              component->GetOutput()->GetPose(xyz,rxyz);
              xyz[1]=-xyz[1];
              component->SetPose(xyz,rxyz,0.0);
              component->Modified();
              component->Update();
            }
            else
            {
              component->SetData(polydata,0.0);
            }

            polydata->Delete();

            if (components.size()>0)
            {
              component->ReparentTo(components[components.size()-1]);
              component->SetMatrix(*matrixComponents[matrixComponents.size()-1]);
            }
            else
            {
              component->ReparentTo(prosthesisGroup);
            }

            if(Inode->GetChild(i)->GetChild(0))
            {
              if(mafVMEGroup::SafeDownCast(Inode->GetChild(i)->GetChild(0))->GetMatrixVector()->GetMatrix(0))
              {
                matrixComponents.push_back(mafVMEGroup::SafeDownCast(Inode->GetChild(i)->GetChild(0))->GetMatrixVector()->GetMatrix(0));
              }
            }

            components.push_back(component);

          }// if(!strcmp(Inode->GetChild(i)->GetName(),m_ComponetsSizes[Inode->GetName()].m_Sizes[m_ComponetsSizes[Inode->GetName()].m_Selection]))//If I found the correct size
        }// for(int i=0;i<m_ComponetsSizes[Inode->GetName()].m_NumSizes;i++)
      }// if(Inode->GetTagArray()->GetTag("HIPOP_COMPONENT_VISIBILITY"))//Find if the node is a component of the Prosthesis DB
    }// for (mafNode *Inode = iter->GetFirstNode(); Inode; Inode = iter->GetNextNode())
    mafVMESurface *reconstruction = mafVMESurface::SafeDownCast(FindVmeByTag(m_Input,TAG_3D_RECONSTRUCTION));
    reconstruction->Update();
    double bounds[6];
    reconstruction->GetOutput()->GetBounds(bounds);

    double center[3] = {(bounds[1]+bounds[0])/2.0,(bounds[3]+bounds[2])/2.0,(bounds[5]+bounds[4])/2.0};

    mafMatrix matrix;
    mafSmartPointer<mafTransform> transform;
    transform->SetMatrix(matrix);

    if (m_Side == ID_SIDE_RIGHT)
    {
      transform->RotateY(45,POST_MULTIPLY);
    }
    else if (m_Side == ID_SIDE_LEFT)
    {
      transform->RotateY(-45,POST_MULTIPLY);
    }

    /*transform->SetPosition(center[0],center[1],center[2]);*/

    prosthesisGroup->SetAbsMatrix(transform->GetMatrix());
    prosthesisGroup->Update();

    AutoplacingProsthesis("Acetabular",prosthesisGroup);

    std::vector<mafVME *> groups;
    mafVME *group = NULL;
    FindVmeByTag(m_Input,m_TagAttributesGroup,groups);

    for (int i = 0;i<groups.size();i++)
    {
      iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(groups[i]->GetAttribute(m_AttributeName));
      if (attribute != NULL)
      {
        group = groups[i];
        break;
      }
    }
    iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(group->GetAttribute(m_AttributeName));
    attribute->AddProsthesis(m_AcetabularManufacturesList->GetStringSelection(),m_AcetabularModelsList->GetStringSelection(),"Acetabular",componetSizeSelected);
    group->SetAttribute(m_AttributeName,attribute);
  }
}
//-------------------------------------------------------------------------
vtkPolyData* iGuiDialogChoiceImplant::LoadData(mafString &fileData)
//-------------------------------------------------------------------------
{
  std::string fileMemory;
  mafDecryptFileInMemory(fileData,fileMemory,"d-FUgg&9ogWB,3?UiPb~'sQY@%AsQ");
  if(fileMemory.empty())
    mafDecryptFileInMemory(fileData,fileMemory,"fattinonfostepervivercomebruti");
  if (fileMemory.empty())
  {
    wxMessageBox(_("Decryption Error!"));
    return NULL;
  }

  vtkMAFSmartPointer<vtkPolyDataReader> reader;
  reader->SetFileName(fileData.GetCStr());
  reader->ReadFromInputStringOn();
  reader->SetInputString(fileMemory.c_str(),fileMemory.size());
  reader->Update();

  vtkPolyData *output;
  vtkNEW(output);
  output->DeepCopy(reader->GetOutput());
  output->Update();

  return output;
}
//-------------------------------------------------------------------------
void iGuiDialogChoiceImplant::UpdateAcetabularManufacturersList()
//-------------------------------------------------------------------------
{
  if (m_AcetabularManufacturesList == NULL)
  {
    return;
  }

  m_AcetabularManufacturesList->Clear();

  for (int i=0;i<m_AcetabularDBProsthesisInformation.size();i++)
  {
    mafString newName = m_AcetabularDBProsthesisInformation[i].m_Manufacture;
    if (m_AcetabularManufacturesList->FindString(newName.GetCStr()) == wxNOT_FOUND)
    {
      m_AcetabularManufacturesList->Append(newName.GetCStr());
    }
  }

  if (m_Training)
  {
	  std::vector<mafVME *> prosthesisGroup;
	  FindVmeByTag(m_Input,TAG_PROSTHESIS_GROUP,prosthesisGroup);

    if (prosthesisGroup.size() == 0)
    {
      m_EnableBuildAcetabularProsthesis = FALSE;
      this->OnEvent(&mafEvent(this,ID_ACETABULAR_ENABLE));
      return;
    }

	  for(int i=0;i<prosthesisGroup.size()>0;i++)
	  {
	    mafString name = prosthesisGroup[i]->GetName();
	
	    wxStringTokenizer tkz(name.GetCStr(),wxT('-')); // Read t values
	    int num_t = tkz.CountTokens();
	
	    if (num_t == 2)
	    {
	      wxString manufacture = tkz.GetNextToken();
	      wxString model = tkz.GetNextToken();
	
	      if (m_AcetabularManufacturesList->FindString(manufacture) != wxNOT_FOUND)
	      {
	        m_AcetabularManufacturesList->SetSelection((m_AcetabularManufacturesList->FindString(manufacture)));
	        this->OnEvent(&mafEvent(this,ID_ACETABULAR_MANUFACTURE_LIST));
	        return;
	      }
	
	    }
	  }

    m_EnableBuildAcetabularProsthesis = FALSE;
    m_GuiAcetabular->Update();
    this->OnEvent(&mafEvent(this,ID_ACETABULAR_ENABLE));
    return;
  }

  if (m_AcetabularManufacturesList->GetCount()>0)
  {
	  m_AcetabularManufacturesList->SetSelection(0);
	  m_AcetabularManufacturesList->Update();
  }
}
//-------------------------------------------------------------------------
void iGuiDialogChoiceImplant::UpdateAcetabularModelsList()
//-------------------------------------------------------------------------
{
  if (m_AcetabularModelsList == NULL)
  {
    return;
  }

  m_AcetabularComponentsList->Clear();
  m_AcetabularSizeList->Clear();
  m_AcetabularModelsList->Clear();

  mafString manufacture = m_AcetabularManufacturesList->GetStringSelection();
  for (int i=0;i<m_AcetabularDBProsthesisInformation.size();i++)
  {    
    mafString nameManufacture = m_AcetabularDBProsthesisInformation[i].m_Manufacture;
    if ( strcmp(manufacture.GetCStr(),nameManufacture.GetCStr()) == 0)
    {
      mafString newName = m_AcetabularDBProsthesisInformation[i].m_Model;
      m_AcetabularModelsList->Append(newName.GetCStr());
    }
  }

  if (m_Training)
  {
	  std::vector<mafVME *> prosthesisGroup;
	  FindVmeByTag(m_Input,TAG_PROSTHESIS_GROUP,prosthesisGroup);

	  for(int i=0;i<prosthesisGroup.size()>0;i++)
	  {
	    mafString name = prosthesisGroup[i]->GetName();
	
	    wxStringTokenizer tkz(name.GetCStr(),wxT('-')); // Read t values
	    int num_t = tkz.CountTokens();
	
	    if (num_t == 2)
	    {
	      wxString manufacture = tkz.GetNextToken();
	      wxString model = tkz.GetNextToken();
	
	      if (m_AcetabularModelsList->FindString(model) != wxNOT_FOUND)
	      {
	        m_AcetabularModelsList->SetSelection((m_AcetabularModelsList->FindString(model)));
	        this->OnEvent(&mafEvent(this,ID_ACETABULAR_MODEL_LIST));
	        return;
	      }
	
	    }
	  }
  }

  /* m_ModelsList->SetSelection(0);
  m_ModelsList->Update();*/
}
//-------------------------------------------------------------------------
void iGuiDialogChoiceImplant::UpdateAcetabularComponentsList()
//-------------------------------------------------------------------------
{
  wxBusyCursor cursor;
  wxBusyInfo wait_info(_("Opening the prosthesis file! Please Wait!"));

  m_AcetabularComponentsList->Enable(false);
  m_AcetabularModelsList->Enable(false);
  m_AcetabularManufacturesList->Enable(false);
  m_AcetabularSizeList->Enable(false);

  mafString manufacture = m_AcetabularManufacturesList->GetStringSelection();
  mafString model = m_AcetabularModelsList->GetStringSelection();
  mafString file = (mafGetApplicationDirectory()+"/Config/DataBase/").c_str();

  for (int i=0;i<m_AcetabularDBProsthesisInformation.size();i++)
  {
    if (m_AcetabularDBProsthesisInformation[i].m_Manufacture == manufacture &&  m_AcetabularDBProsthesisInformation[i].m_Model == model)
    {
      file += m_AcetabularDBProsthesisInformation[i].m_FileName;
      break;
    }
  }

  mafString fileOpened=ZIPOpen(file.GetCStr());
  if(fileOpened=="")
  {
    wxMessageBox("Error Open DB!");
    return;
  }

  mafVMEStorage *storage;
  storage = mafVMEStorage::New();
  mafVMERoot *root;
  root = storage->GetRoot();
  root->Initialize();
  root->SetName("RootB");

  mafMSFImporter *importer = new mafMSFImporter; //Import prosthesis MSF
  importer->SetURL(fileOpened.GetCStr());
  importer->SetRoot(root);
  importer->Restore();

  if(!root->GetFirstChild())
  {
    mafDEL(storage);
    wxMessageBox(_("Error Parsing!"));
    return;
  }

  FindAndRemove(m_Input,m_TagProsthesisDbAcetabural);//remove old DB

  mafSmartPointer<mafVMEGroup> group; //Create new DB tree
#ifndef _DEBUG
  group->GetTagArray()->SetTag(mafTagItem("VISIBLE_IN_THE_TREE", 0.0));
#endif // _DEBUG
  group->GetTagArray()->SetTag(m_TagProsthesisDbAcetabural,"");
  group->SetName("Prosthesis Model");
  group->ReparentTo(m_Input);

  mafNode::CopyTree(root->GetFirstChild(),group);

  m_AcetabularModelData=group;

  bool foundTag = false;
  mafNodeIterator *iter = group->NewIterator();
  for (mafNode *Inode = iter->GetFirstNode(); Inode; Inode = iter->GetNextNode())
  {
    if(strcmp(Inode->GetName(),model)==0)
    {
      m_AcetabularModelData = mafVMEGroup::SafeDownCast(Inode);
      foundTag = true;
      break;
    }
  }
  iter->Delete();

  if (!foundTag)
  {
    mafDEL(importer);
    mafDEL(storage);

    wxMessageBox(_("Prosthesis model not found!"));
    return;
  }

  mafLogMessage(_("Prosthesis model found"));

  m_AcetabularComponentsList->Clear();
  m_AcetabularComponetsSizes.clear();

  std::vector<mafMatrix> matrixComponents;
  iter = m_AcetabularModelData->NewIterator();
  for (mafNode *Inode = iter->GetFirstNode(); Inode; Inode = iter->GetNextNode())
  {
    if(Inode->GetTagArray()->GetTag("HIPOP_COMPONENT_VISIBILITY"))//Find if the node is a component of the Prosthesis DB
    {
      int numberOfTypes=Inode->GetNumberOfChildren();
      mafString *typeList;
      typeList=new mafString[numberOfTypes];
      for(int i=0;i<numberOfTypes;i++)
        typeList[i] = (Inode->GetChild(i)->GetName());//Create a list of all model for a particular components*/

      // mafSmartPointer<mafVMESurface> component;
      m_AcetabularComponentsList->Append(Inode->GetName());
      SIZES sizes;
      sizes.m_Sizes = typeList;
      sizes.m_NumSizes = numberOfTypes;
      sizes.m_Selection = 0;
      m_AcetabularComponetsSizes[Inode->GetName()] = sizes;
    }
  }

  iter->Delete();
  mafDEL(importer);
  mafDEL(storage);

  if (m_AcetabularComponentsList->GetCount()>0)
  {
	  m_AcetabularComponentsList->SetSelection(0);
	  m_AcetabularComponentsList->Update();
  }

  m_AcetabularComponentsList->Enable(true);
  m_AcetabularModelsList->Enable(true);
  m_AcetabularManufacturesList->Enable(true);
  m_AcetabularSizeList->Enable(true);
}
//-------------------------------------------------------------------------
void iGuiDialogChoiceImplant::AutoplacingProsthesis(mafString type,mafVMEGroup *prosthesisGroup)
//-------------------------------------------------------------------------
{

  if (prosthesisGroup == NULL)
  {
    return;
  }
  mafVMESurface *prosthesisComponent0 = NULL;
  mafVMESurface *prosthesisComponent1 = NULL;
  mafVMESurface *prosthesisComponent2 = NULL;

  mafString tagName;
  if (!m_Training)
  {
    tagName = TAG_PROSTHESIS_COMPONENT;
  }
  else
  {
    tagName = TAG_PROSTHESIS_COMPONENT_TRAINING;
  }

  if (type == "Femoral")
  {
    prosthesisComponent0 = mafVMESurface::SafeDownCast(prosthesisGroup->GetChild(0));//stem component is first child
    if (prosthesisComponent0 == NULL || !prosthesisComponent0->GetTagArray()->IsTagPresent(tagName))
    {
      return;
    }

	  prosthesisComponent1 = mafVMESurface::SafeDownCast(prosthesisGroup->GetChild(0)->GetChild(0));//Neck component is second child
    if (prosthesisComponent1 == NULL || !prosthesisComponent1->GetTagArray()->IsTagPresent(tagName))
    {
      return;
    }

	  prosthesisComponent2 = mafVMESurface::SafeDownCast(prosthesisGroup->GetChild(0)->GetChild(0)->GetChild(0));//Head component is third child
    if (prosthesisComponent2 == NULL || !prosthesisComponent2->GetTagArray()->IsTagPresent(tagName))
    {
      return;
    }
  }
  else if (type == "Resurfacing" || type == "Acetabular")
  {
    prosthesisComponent0 = mafVMESurface::SafeDownCast(prosthesisGroup->GetChild(0));//stem component is first child
    if (prosthesisComponent0 == NULL || !prosthesisComponent0->GetTagArray()->IsTagPresent(tagName))
    {
      return;
    }
  }

  mafVMELandmarkCloud *lnd = mafVMELandmarkCloud::SafeDownCast(FindVmeByTag(m_Input,TAG_LND_CLOUD));
  if (lnd == NULL)
  {
    return;
  }

  double position[3];

  lnd->Open();
  mafMatrix *matrixLandmark = mafVMELandmark::SafeDownCast(lnd->GetChild(0))->GetOutput()->GetAbsMatrix();
  mafTransform::GetPosition(*matrixLandmark,position);
  lnd->Close();

  mafTransform transform;
  if (type == "Femoral")
  {
  	transform.SetMatrix(prosthesisComponent2->GetOutput()->GetAbsMatrix()->GetVTKMatrix());
  }
  else if (type == "Resurfacing" || type == "Acetabular")
  {
    transform.SetMatrix(prosthesisComponent0->GetOutput()->GetAbsMatrix()->GetVTKMatrix());
  }
  transform.SetPosition(position);
  double outnormal[3];
  double innormal[3] = {0.0,0.0,1.0};
  vtkMAFSmartPointer<vtkTransform> tNormal;
  tNormal->PostMultiply();
  tNormal->SetMatrix(transform.GetMatrix().GetVTKMatrix());
  tNormal->TransformNormal(innormal,outnormal);

  double newNeckVector[3];
  vtkMAFSmartPointer<vtkTransform> tVector;
  tVector->PostMultiply();
  tVector->SetMatrix(lnd->GetOutput()->GetAbsMatrix()->GetVTKMatrix());
  tVector->TransformNormal(m_NeckVector,newNeckVector);

  double angleZNeck = 0.0;
  double angleYNeck = 0.0;
  double angleXNeck = 0.0;

  if (!(m_NeckVector[0] == 0.0 && m_NeckVector[1] == 0.0 && m_NeckVector[2] == 0.0))
  {
	  angleZNeck = atan(newNeckVector[1] / newNeckVector[0]);
	  angleYNeck = atan(newNeckVector[2] / newNeckVector[0]);
	  angleXNeck = atan(newNeckVector[1] / newNeckVector[2]);
  }

  double angleZ = 0.0;
  double angleY = 0.0;
  double angleX = 0.0;

  double z[3];
  vtkMath::Cross(innormal,outnormal,z);
  if (!(z[0] == 0.0 && z[1] == 0.0 && z[2] == 0.0))
  {
	  angleZ = atan(outnormal[1] / outnormal[0]);
	  angleY = atan(outnormal[2] / outnormal[0]);
	  angleX = atan(outnormal[1] / outnormal[2]);
  }

  double positionHead[3];
  if (type == "Resurfacing" || type == "Acetabular")
  {
    mafTransform::GetPosition(*(prosthesisComponent0->GetOutput()->GetAbsMatrix()),positionHead);
  }
  else if (type == "Femoral")
  {
    mafTransform::GetPosition(*(prosthesisComponent2->GetOutput()->GetAbsMatrix()),positionHead);
  }
  double positionGroup[3];
  mafTransform::GetPosition(*(prosthesisGroup->GetOutput()->GetAbsMatrix()),positionGroup);

  mafMatrix matrix;
  mafSmartPointer<mafTransform> transformProsthesis;
  /*transformProsthesis->SetMatrix(prosthesisComponent0->GetOutput()->GetAbsMatrix()->GetVTKMatrix());*/
  transformProsthesis->SetMatrix(transform.GetMatrix());
  transformProsthesis->SetPosition(position[0],position[1],position[2]);
  transformProsthesis->Translate(-position[0],-position[1],-position[2],POST_MULTIPLY);
  transformProsthesis->RotateY(vtkMath::RadiansToDegrees()*(angleYNeck - angleY),POST_MULTIPLY);
  transformProsthesis->RotateX(vtkMath::RadiansToDegrees()*(angleXNeck - angleX),POST_MULTIPLY);
  transformProsthesis->RotateZ(vtkMath::RadiansToDegrees()*(angleZNeck - angleZ),POST_MULTIPLY);
  transformProsthesis->Translate(position[0],position[1],position[2],POST_MULTIPLY);
  transformProsthesis->Translate(-(positionHead[0]-positionGroup[0]),-(positionHead[1]-positionGroup[1]),-(positionHead[2]-positionGroup[2]),POST_MULTIPLY);

  prosthesisGroup->SetAbsMatrix(transformProsthesis->GetMatrix());
  prosthesisGroup->Update();

}
