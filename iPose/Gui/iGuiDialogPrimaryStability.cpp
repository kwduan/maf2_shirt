/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialogPrimaryStability.cpp,v $
Language:  C++
Date:      $Date: 2012-03-21 07:24:19 $
Version:   $Revision: 1.1.2.10 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/


#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iGuiDialogPrimaryStability.h"
#include "iDecl.h"
#include "iGuiSettingsPlanning.h"

#include "mafGUIButton.h"
#include "mafGUIValidator.h"
#include "mafRWI.h"
#include "mafDeviceButtonsPadMouse.h"
#include "mafVMERoot.h"

#include "vtkMAFSmartPointer.h"
#include "vtkRenderWindow.h"
#include "vtkCamera.h"
#include "vtkPolyDataReader.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkCellData.h"
#include "vtkPolyData.h"
#include "vtkScalarBarActor.h"
#include "vtkTextActor.h"
#include "vtkTextProperty.h"
#include <wx/busyinfo.h>
#include "iAttributeWorkflow.h"
#include "mafVMEGroup.h"

//----------------------------------------------------------------------------
iGuiDialogPrimaryStability::iGuiDialogPrimaryStability(mafVMERoot *input,const wxString& title,mafDeviceButtonsPadMouse *mouse,double weight,double height,double PCA[35],bool training /* = false */)
: mafGUIDialog(title,mafCLOSEWINDOW | mafRESIZABLE)
//----------------------------------------------------------------------------
{
  int x_pos,y_pos,w,h;
  mafGetFrame()->GetPosition(&x_pos,&y_pos);
  this->GetSize(&w,&h);
  this->SetSize(x_pos+5,y_pos+5,w,h);

  m_Rwi3DView = NULL;
  m_ScalarBar = NULL;
  m_UncertaintyActor = NULL;
  m_InfoActor = NULL;

  m_Mouse = mouse;

  m_Uncertainty = 0.0;

  double avrBmi;
  double sdBmi;
  if (training)
  {
    iAttributeWorkflow *attribute = NULL;
    mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(input,TAG_GROUP_ATTRIBUTES_TRAINING));

    attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));

    avrBmi = attribute->GetAvrBMI();
    sdBmi = attribute->GetStdBMI();
  }
  else
  {
    iGuiSettingsPlanning *setting = new iGuiSettingsPlanning(NULL);
    avrBmi = setting->GetAverageBmi();
    sdBmi = setting->GetStandardDeviationBmi();
    cppDEL(setting);
  }

  m_NormalizedBMI = (weight/(height*height)-avrBmi)/sdBmi;

  this->SetSize(640, 480);

  CreateGui();
  if (ExecuteExternalApplication(input,PCA,weight,height,m_StabilityValues,m_Uncertainty,training))
  {
    ShowModel();
  }
  
}
//----------------------------------------------------------------------------
iGuiDialogPrimaryStability::iGuiDialogPrimaryStability(mafVMERoot *input,const wxString& title,mafDeviceButtonsPadMouse *mouse,double weight,double height,double stability[16] , double uncertainty,bool training /* = false */)
: mafGUIDialog(title,mafCLOSEWINDOW | mafRESIZABLE)
//----------------------------------------------------------------------------
{
  int x_pos,y_pos,w,h;
  mafGetFrame()->GetPosition(&x_pos,&y_pos);
  this->GetSize(&w,&h);
  this->SetSize(x_pos+5,y_pos+5,w,h);

  m_Rwi3DView = NULL;
  m_ScalarBar = NULL;
  m_UncertaintyActor = NULL;

  double avrBmi;
  double sdBmi;
  if (training)
  {
    iAttributeWorkflow *attribute = NULL;
    mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(input,TAG_GROUP_ATTRIBUTES_TRAINING));

    attribute = iAttributeWorkflow::SafeDownCast(input->GetAttribute("Workflow"));

    avrBmi = attribute->GetAvrBMI();
    sdBmi = attribute->GetStdBMI();
  }
  else
  {
    iGuiSettingsPlanning *setting = new iGuiSettingsPlanning(NULL);
    avrBmi = setting->GetAverageBmi();
    sdBmi = setting->GetStandardDeviationBmi();
    cppDEL(setting);
  }

  m_NormalizedBMI = (weight/(height*height)-avrBmi)/sdBmi;

  m_Mouse = mouse;

  this->SetSize(640, 480);

  CreateGui();
  m_Uncertainty = uncertainty;
  for (int i=0;i<16;i++)
  {
    m_StabilityValues[i] = stability[i];
  }
  ShowModel();
}
//----------------------------------------------------------------------------
iGuiDialogPrimaryStability::~iGuiDialogPrimaryStability()
//----------------------------------------------------------------------------
{
  m_Rwi3DView->m_RenFront->RemoveAllProps();
  m_Rwi3DView->m_RenFront->RemoveActor2D(m_ScalarBar);
  m_Rwi3DView->m_RenFront->RemoveActor2D(m_UncertaintyActor);
  m_Rwi3DView->m_RenFront->RemoveActor2D(m_InfoActor);
  cppDEL(m_Rwi3DView);
  vtkDEL(m_ScalarBar);
  vtkDEL(m_UncertaintyActor);
  vtkDEL(m_InfoActor);
}
//----------------------------------------------------------------------------
void iGuiDialogPrimaryStability::OnEvent(mafEventBase *maf_event)
//----------------------------------------------------------------------------
{
  if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
  {
    switch(e->GetId())
    {
    case ID_CANCEL:
      {
        this->EndModal(wxCANCEL);
      }
      break;
    case ID_ACCEPT_BUTTON:
      this->EndModal(wxID_RETURN_NEXT);
      break;
    case ID_REJECT_BUTTON:
      this->EndModal(wxID_RETURN_PREV);
      break;
    default:
      mafEventMacro(*e);
    }
  }
}
//----------------------------------------------------------------------------
void iGuiDialogPrimaryStability::CreateGui()
//----------------------------------------------------------------------------
{
  wxPoint p = wxDefaultPosition;

  int x,y,z,vx,vy,vz;
  x=0; y=1; z=0; vx=0; vy=0; vz=-1;

  m_Rwi3DView = new mafRWI(this,TWO_LAYER,false);
  m_Rwi3DView->SetListener(this);//SIL. 16-6-2004: 
  m_Rwi3DView->CameraSet(CAMERA_PERSPECTIVE);

  m_Rwi3DView->SetSize(0,0,640,640);

  m_Rwi3DView->m_RenderWindow->SetDesiredUpdateRate(0.0001f);
  m_Rwi3DView->Show(true);
  m_Rwi3DView->m_RwiBase->SetMouse(m_Mouse);

  wxBoxSizer *buttonSizer2 = new wxBoxSizer(wxHORIZONTAL);

  wxStaticText *bmiLab = new wxStaticText(this,-1,wxString::Format("Normalized BMI %.3f",m_NormalizedBMI),p,wxSize(120,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );

  buttonSizer2->Add(bmiLab,0);

  wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);

  m_ButtonAccept = new mafGUIButton(this, ID_ACCEPT_BUTTON,_("accept"), p, wxSize(100,20));
  m_ButtonAccept->SetValidator(mafGUIValidator(this,ID_ACCEPT_BUTTON,m_ButtonAccept));
  m_ButtonAccept->Enable(true);

  buttonSizer->Add(m_ButtonAccept,0);

  m_GuiSizer->Add(m_Rwi3DView->m_RwiBase,0,wxCENTRE,5);
  m_GuiSizer->Add(buttonSizer2,0,wxCENTRE,5);
	m_GuiSizer->Add(buttonSizer,0,wxCENTRE,5);
}
//----------------------------------------------------------------------------
bool iGuiDialogPrimaryStability::ExecuteExternalApplication(mafVMERoot *input, double pca[35],double weight,double height,double stabilityValues[16], double &uncertainty, bool training )
//----------------------------------------------------------------------------
{
  wxBusyInfo wait (_("Please Wait"));

  wxString workingDir = mafGetApplicationDirectory().c_str();
  workingDir << "/ExternalTool";
  wxString oldDir = wxGetCwd();
  wxSetWorkingDirectory(workingDir);

  wxArrayString output;
  wxArrayString errors;
  wxString command2execute;
  
  command2execute = "MXL_DEMO.exe [";

  double avrBmi;
  double sdBmi;
  if (training)
  {
    iAttributeWorkflow *attribute = NULL;
    mafVMEGroup *groupAttributes = mafVMEGroup::SafeDownCast(FindVmeByTag(input,TAG_GROUP_ATTRIBUTES));

    attribute = iAttributeWorkflow::SafeDownCast(groupAttributes->GetAttribute("Workflow"));

    avrBmi = attribute->GetAvrBMI();
    sdBmi = attribute->GetStdBMI();
  }
  else
  {
    iGuiSettingsPlanning *setting = new iGuiSettingsPlanning(NULL);
    avrBmi = setting->GetAverageBmi();
    sdBmi = setting->GetStandardDeviationBmi();
    cppDEL(setting);
  }

  double BMI = 0.0;
  BMI = (weight/(height*height)-avrBmi)/sdBmi;
  command2execute << BMI;
  command2execute << ",";
  for (int i=0;i<35;i++)
  {
    command2execute << pca[i];
    if (i != 34)
    {
    	command2execute << ",";
    }
    else
    {
      command2execute << "]";
    }
  }

  mafLogMessage("<<<<<COMMAND PRIMARY STABILITY:");
  mafLogMessage("<<<<<%s",command2execute.c_str());

  long pid = -1;
  if (pid = wxExecute(command2execute, output, errors, wxEXEC_SYNC) != 0)
  {
    
    mafLogMessage(_("Command Errors"));  

    return false;
  }

  for (int i=1, j=0;i<output.size();i+=2,j++)//Skip first output
  {
    mafLogMessage(output[i]);

    wxString value = output[i].SubString(output[i].Find(':')+1,output[i].size());

    stabilityValues[j] = atof(value);
  }

  wxString value = output[2].SubString(output[2].Find(':')+1,output[2].size());

  uncertainty = atof(value);//Accuracy is equals for every values

  mafLogMessage("Uncertainty %.3f",uncertainty);

  wxSetWorkingDirectory(oldDir);

  return true;
}
//----------------------------------------------------------------------------
void iGuiDialogPrimaryStability::ShowModel()
//----------------------------------------------------------------------------
{
  wxBusyInfo wait (_("Please Wait"));

  iGuiSettingsPlanning *settings = new iGuiSettingsPlanning(NULL);
  wxString fileTemplate;

  fileTemplate = mafGetApplicationDirectory().c_str();
  fileTemplate<<"/Config/Templates/zoned_femur.vtk";

  vtkMAFSmartPointer<vtkPolyDataReader> reader;
  reader->SetFileName(fileTemplate.c_str());
  reader->Update();

  vtkMAFSmartPointer<vtkPolyData> pd;
  pd->DeepCopy(reader->GetOutput());
  pd->Update();

  vtkIntArray *scalars = vtkIntArray::SafeDownCast(pd->GetCellData()->GetArray("Zones"));

  for (int i=0;i<scalars->GetNumberOfTuples();i++)
  {
    scalars->SetTuple1(i,m_StabilityValues[((int)scalars->GetTuple1(i))-1]);
  }

  double sr[2];
  scalars->GetRange(sr);

  /*pd->GetCellData()->AddArray(scalars);*/
  pd->GetCellData()->SetActiveScalars("Zones");
  pd->Update();

  vtkMAFSmartPointer<vtkPolyDataMapper> mapper;
  mapper->SetInput(pd);
  mapper->ScalarVisibilityOn();
  mapper->SetScalarModeToUseCellData();
  mapper->SetScalarRange(sr);
  mapper->Update();

  vtkMAFSmartPointer<vtkActor> actor;
  actor->SetMapper(mapper);

  m_Rwi3DView->m_RenFront->AddActor(actor);

  vtkNEW(m_ScalarBar);
  m_ScalarBar->SetLookupTable(mapper->GetLookupTable());
  ((vtkActor2D*)m_ScalarBar)->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
  ((vtkActor2D*)m_ScalarBar)->GetPositionCoordinate()->SetValue(0.1,0.1);
  m_ScalarBar->SetOrientationToHorizontal();
  m_ScalarBar->SetWidth(0.8);
  m_ScalarBar->SetHeight(0.17);
  m_ScalarBar->SetTitle("Primary Stability % Strain");
  m_ScalarBar->SetMaximumNumberOfColors(16);
  m_ScalarBar->SetLabelFormat("%-#6.0f");

  m_Rwi3DView->m_RenFront->AddActor2D(m_ScalarBar);

  vtkNEW(m_UncertaintyActor);
  m_UncertaintyActor->SetPosition(m_Rwi3DView->m_RenFront->GetSize()[0]-150,m_Rwi3DView->m_RenFront->GetSize()[0]-20);
  m_UncertaintyActor->SetInput(wxString::Format("Uncertainty %.3f",m_Uncertainty));
  m_UncertaintyActor->GetTextProperty()->SetBold(TRUE);
  m_Rwi3DView->m_RenFront->AddActor2D(m_UncertaintyActor);

  vtkNEW(m_InfoActor);
  m_InfoActor->SetPosition(m_Rwi3DView->m_RenFront->GetSize()[0]-450,m_Rwi3DView->m_RenFront->GetSize()[0]-600);
  m_InfoActor->SetInput(_("[Mapping on intacted femur-SSM]"));
  m_InfoActor->GetTextProperty()->SetFontSize(16);
  m_InfoActor->GetTextProperty()->SetBold(TRUE);
  m_Rwi3DView->m_RenFront->AddActor2D(m_InfoActor);


  

  m_Rwi3DView->CameraReset();
  m_Rwi3DView->CameraUpdate();

  cppDEL(settings);
}