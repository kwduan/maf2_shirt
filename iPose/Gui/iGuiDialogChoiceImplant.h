/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialogChoiceImplant.h,v $
Language:  C++
Date:      $Date: 2012-03-21 07:26:31 $
Version:   $Revision: 1.1.2.5 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it
=========================================================================*/

#ifndef __iGuiDialogChoiceImplant_H__
#define __iGuiDialogChoiceImplant_H__

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class mafVMERoot;
class mafVMEGroup;
class mafString;
class mafGUIButton;
class vtkPolyData;

//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "mafGUIDialog.h"
#include "iGuiDefines.h"
#include "iDecl.h"

//----------------------------------------------------------------------------
// Const:
//----------------------------------------------------------------------------

/** iGuiDialogChoiceImplant
*/
class I_GUI_EXPORT iGuiDialogChoiceImplant : public mafGUIDialog
{
public:

    iGuiDialogChoiceImplant (const wxString& title,mafVMERoot *input,int prosthesisType,int side,double neckVector[3], bool training = false);
    virtual ~iGuiDialogChoiceImplant (); 

    /** process events coming from other components */
    /*virtual*/ void OnEvent(mafEventBase *maf_event);
protected:

    enum ID_GUIs
    {
        ID_CANCEL = MINID,
        ID_ACCEPT_BUTTON,
        ID_REJECT_BUTTON,
        ID_MANUFACTURE_LIST,
        ID_MODEL_LIST,
        ID_COMPONENTS_LIST,
        ID_SIZE_LIST,
        ID_ACETABULAR_MANUFACTURE_LIST,
        ID_ACETABULAR_MODEL_LIST,
        ID_ACETABULAR_COMPONENTS_LIST,
        ID_ACETABULAR_SIZE_LIST,
        ID_FEMORAL_ENABLE,
        ID_ACETABULAR_ENABLE,
    };

    /** Create the interface */
    void CreateGui();
    
    /** open and parse the DB file of prosthesis */
    int OpenDBProsthesis();
   
    /** Fill the list of manufactures */
    void UpdateManufacturersList();
    void UpdateAcetabularManufacturersList();

    /** Fill the list of models */
    void UpdateModelsList();
    void UpdateAcetabularModelsList();

    void UpdateComponentsList();
    void UpdateAcetabularComponentsList();

    mafString ZIPOpen(mafString file);
    
    void UpdateSizesList();
    void UpdateAcetabularSizesList();

    /** generate a surface with the parameters chosen by the user*/
    void GenerateProsthesis();

    /** align prosthesis using neck vector*/
    void AutoplacingProsthesis(mafString type,mafVMEGroup *prosthesisGroup);

    /** read vtkPolyData for a particular vtk file */
    vtkPolyData* LoadData(mafString &fileData);


    wxListBox *m_ManufacturesList;
    wxListBox *m_ModelsList;
    wxListBox *m_ComponentsList;
    wxListBox *m_SizeList;
    wxListBox *m_AcetabularComponentsList;
    wxListBox *m_AcetabularModelsList;
    wxListBox *m_AcetabularManufacturesList;
    wxListBox *m_AcetabularSizeList;
    mafGUI *m_Gui;
    mafGUI *m_GuiAcetabular;

    std::vector<DB_PROSTHESIS_INFORMATION> m_DBProsthesisInformation;
    std::vector<DB_PROSTHESIS_INFORMATION> m_AcetabularDBProsthesisInformation;

    typedef struct  
    {
      mafString *m_Sizes;
      int m_NumSizes;
      int m_Selection;
    } SIZES;
    std::map<mafString,SIZES> m_ComponetsSizes;
    std::map<mafString,SIZES> m_AcetabularComponetsSizes;

    mafVMERoot *m_Input;

    mafVMEGroup *m_ModelData;
    mafVMEGroup *m_AcetabularModelData;

    int m_ProsthesisType;
    int m_EnableBuildFemoralProsthesis;
    int m_EnableBuildAcetabularProsthesis;
    int m_Side;

    mafGUIButton *m_ButtonAccept;
    mafGUIButton *m_ButtonCancel;
    mafGUIButton *m_ButtonReject;

    double m_NeckVector[3];

    bool m_Training;

    mafString m_TagProsthesisDbAcetabural;
    mafString m_TagProsthesisDb;
    mafString m_TagProsthesisGroup;
    mafString m_TagAttributesGroup;
    mafString m_AttributeName;

};
#endif
