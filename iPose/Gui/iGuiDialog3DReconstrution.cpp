/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialog3DReconstrution.cpp,v $
Language:  C++
Date:      $Date: 2012-03-21 07:25:44 $
Version:   $Revision: 1.1.2.22 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it)
=========================================================================*/


#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iGuiDialog3DReconstrution.h"
#include "iDecl.h"
#include "iGuiSettingsPlanning.h"

#include "mafGUIButton.h"
#include "mafGUIValidator.h"
#include "mafRWI.h"
#include "mafDeviceButtonsPadMouse.h"
#include "mafNodeIterator.h"
#include "mafEventIO.h"
#include "mafVMERoot.h"
#include "mafTagArray.h"
#include "mafVMEImage.h"
#include "mafOpImporterSTL.h"
#include "mafVMESurface.h"
#include "mafTransform.h"
#include "mafMatrix.h"
#include "mmaMaterial.h"
#include "mafVMELandmarkCloud.h"

#include "vtkMAFSmartPointer.h"
#include "vtkRenderWindow.h"
#include "vtkTexture.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkWindowLevelLookupTable.h"
#include "vtkPlaneSource.h"
#include "vtkRenderer.h"
#include "vtkImageData.h"
#include "vtkCamera.h"
#include "vtkPolyData.h"
#include "vtkTransform.h"
#include "vtkTransformPolyDataFilter.h"
#include "vtkProperty.h"
#include "vtkSphereSource.h"
#include "vtkMAFExtendedGlyph3D.h"

#include <wx/busyinfo.h>
#include <wx/file.h>
#include <wx/tokenzr.h>

//----------------------------------------------------------------------------
iGuiDialog3DReconstrution::iGuiDialog3DReconstrution(const wxString& title,mafDeviceButtonsPadMouse *mouse,mafVMERoot *input, mafString rxFileName,int step,int id,bool training /* = false */)
: mafGUIDialog(title,mafCLOSEWINDOW | mafRESIZABLE)
//----------------------------------------------------------------------------
{
  int x_pos,y_pos,w,h;
  mafGetFrame()->GetPosition(&x_pos,&y_pos);
  this->GetSize(&w,&h);
  this->SetSize(x_pos+5,y_pos+5,w,h);

  m_Rwi3DView = NULL;
  m_SlicePlane = NULL;
  m_SliceActor = NULL;
  m_SliceTexture = NULL;
  m_SliceMapper = NULL;
  m_SliceLookupTable = NULL;
  m_ReconstructionActor = NULL;
  m_LandmarksActor = NULL;

  m_FileRX = rxFileName;

  m_Mouse = mouse;

  m_Input = input;

  m_CurrentStep = step;
  m_PatientCaseId = id;

  m_Training = training;

  if (m_Training)
  {
    m_Path = "/download/";
  }
  else
  {
    m_Path = "/cache/";
  }

  if (!m_Training)
  {
	  FindAndRemove(m_Input,TAG_3D_RECONSTRUCTION);
	  FindAndRemove(m_Input,TAG_LND_CLOUD);
  }

  this->SetSize(640, 480);

  CreateGui();

  ShowRX();

  //////////////////////////////////////////////////////////////////////////

  for (int i=0;i<35;i++)
  {
    m_PCA[i] = 0.0;
  }
  
  //////////////////////////////////////////////////////////////////////////
}
//----------------------------------------------------------------------------
iGuiDialog3DReconstrution::~iGuiDialog3DReconstrution()
//----------------------------------------------------------------------------
{
  if (m_SliceActor)
  {
    m_Rwi3DView->m_RenBack->RemoveActor(m_SliceActor);
  }

  if (m_ReconstructionActor)
  {
    m_Rwi3DView->m_RenFront->RemoveActor(m_ReconstructionActor);
  }

  if (m_LandmarksActor)
  {
    m_Rwi3DView->m_RenFront->RemoveActor(m_LandmarksActor);
  }

  cppDEL(m_Rwi3DView);

  vtkDEL(m_LandmarksActor);
  vtkDEL(m_ReconstructionActor);
  vtkDEL(m_SlicePlane);
  vtkDEL(m_SliceActor);
  vtkDEL(m_SliceMapper);
  vtkDEL(m_SliceTexture);
  vtkDEL(m_SliceLookupTable);
}
//----------------------------------------------------------------------------
void iGuiDialog3DReconstrution::OnEvent(mafEventBase *maf_event)
//----------------------------------------------------------------------------
{
  if (mafEvent *e = mafEvent::SafeDownCast(maf_event))
  {
    switch(e->GetId())
    {

    case ID_CHECK_RESULTS:
      {
        CheckResults();
      }
      break;
    case ID_CANCEL:
      {
        /*int result = wxMessageBox(_("Would you like to save the current state ?"),_("Save"),wxCANCEL|wxOK);
        if (result == wxCANCEL)
        {
      	  FindAndRemove(m_Input,TAG_LND_CLOUD);
      	  FindAndRemove(m_Input,TAG_3D_RECONSTRUCTION);
      	  FindAndRemove(m_Input,TAG_RX);
        }
        else
        {
          mafVMERoot *root = mafVMERoot::SafeDownCast(m_Input->GetRoot());
          iAttributeWorkflow *attribute = iAttributeWorkflow::SafeDownCast(root->GetAttribute("Workflow"));
          if (attribute != NULL)
          {
            attribute->SetCurrentStep(4);
          }
          else
          {
            attribute = iAttributeWorkflow::New();
            attribute->SetCurrentStep(4);
          }

          root->SetAttribute("Workflow",attribute);
        }*/
        this->EndModal(wxCANCEL);
      }

      break;
    case ID_ACCEPT_BUTTON:
      this->EndModal(wxID_RETURN_NEXT);
      break;
    case ID_REJECT_BUTTON:
      FindAndRemove(m_Input,TAG_LND_CLOUD);
      FindAndRemove(m_Input,TAG_3D_RECONSTRUCTION);
      this->EndModal(wxID_RETURN_PREV);
      break;
    default:
      mafEventMacro(*e);
    }
  }
}
//----------------------------------------------------------------------------
void iGuiDialog3DReconstrution::CreateGui()
//----------------------------------------------------------------------------
{
  wxPoint p = wxDefaultPosition;

  int x,y,z,vx,vy,vz;
  x=0; y=1; z=0; vx=0; vy=0; vz=-1;

  m_Rwi3DView = new mafRWI(this,TWO_LAYER,false);
  m_Rwi3DView->SetListener(this);//SIL. 16-6-2004: 
  m_Rwi3DView->CameraSet(CAMERA_OS_Y);

  m_Rwi3DView->SetSize(0,0,640,640);

  m_Rwi3DView->m_RenderWindow->SetDesiredUpdateRate(0.0001f);
  m_Rwi3DView->Show(true);
  m_Rwi3DView->m_RwiBase->SetMouse(m_Mouse);

  m_Status = "";
  if (m_CurrentStep == ID_STEP_3D_RECONSTRUTION_WAITING || m_CurrentStep == ID_STEP_3D_RECONSTRUTION_START )
  {
    m_Status = _("3D Reconstruction in progress");
  }
  else if (m_CurrentStep == ID_STEP_3D_RECONSTRUTION_SHOW)
  {
    m_Status = _("3D Reconstruction done");
  }

  m_StatusLabel = new wxStaticText(this,-1,(&m_Status)->GetCStr(),p,wxSize(300,22),wxALIGN_CENTER |wxST_NO_AUTORESIZE );
  /*m_StatusLabel->SetValidator( mafGUIValidator(this,-1,m_StatusLabel,&m_Status) );*/

  wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);

  m_ButtonAccept = new mafGUIButton(this, ID_ACCEPT_BUTTON,_("accept"), p, wxSize(100,20));
  m_ButtonAccept->SetValidator(mafGUIValidator(this,ID_ACCEPT_BUTTON,m_ButtonAccept));
  m_ButtonAccept->Enable(false);

  m_ButtonCancel = new mafGUIButton(this, ID_CANCEL,_("cancel"), p, wxSize(100,20));
  m_ButtonCancel->SetValidator(mafGUIValidator(this,ID_CANCEL,m_ButtonCancel));
  m_ButtonCancel->Enable(false);

  m_ButtonCheckResults = new mafGUIButton(this, ID_CHECK_RESULTS,_("check results"), p, wxSize(100,20));
  m_ButtonCheckResults->SetValidator(mafGUIValidator(this,ID_CHECK_RESULTS,m_ButtonCheckResults));
  m_ButtonCheckResults->Enable(true);

  m_ButtonReject = new mafGUIButton(this, ID_REJECT_BUTTON,_("reject"), p, wxSize(100,20));
  m_ButtonReject->SetValidator(mafGUIValidator(this,ID_REJECT_BUTTON,m_ButtonReject));
  m_ButtonReject->Enable(false);


  buttonSizer->Add(m_ButtonReject,0);
  buttonSizer->Add(m_ButtonCancel,0);
  buttonSizer->Add(m_ButtonCheckResults,0);
  buttonSizer->Add(m_ButtonAccept,0);

  m_GuiSizer->Add(m_Rwi3DView->m_RwiBase,0,wxCENTRE,5);
  m_GuiSizer->Add(m_StatusLabel,0,wxCENTRE,5);
	m_GuiSizer->Add(buttonSizer,0,wxCENTRE,5);
}
//----------------------------------------------------------------------------
void iGuiDialog3DReconstrution::ComputeReconstrution()
//----------------------------------------------------------------------------
{
  wxBusyInfo *wait = new wxBusyInfo(_("Wait: 3D Reconstrution"));

  iGuiSettingsPlanning *settings = new iGuiSettingsPlanning(NULL);
  mafString commandLine = settings->GetCommandLine();
  mafString fileSTL = settings->GetFileNameSTL();
  mafString fileTXT = settings->GetFileNameTXT();
  mafString fileInput = settings->GetInputFileName();
  mafString workingDir = settings->GetWorkingDirectory();

  mafString cachedRXFileName = mafGetApplicationDirectory().c_str();
  cachedRXFileName<<m_Path;
  cachedRXFileName<<m_PatientCaseId;
  cachedRXFileName<<"/";
  cachedRXFileName<<m_FileRX;

  mafString toolDir =mafGetApplicationDirectory().c_str();
  toolDir << "\\ExternalTools\\Amira";
  wxString old_dir = wxGetCwd();
  wxSetWorkingDirectory(workingDir.GetCStr());

  if (commandLine == "")
  {
    wxMessageBox(_("Error: command line is empty! Set it in settings menu!"));

    ::wxSafeYield(NULL,true); //fix on bug #2082

    delete wait;

    m_ButtonReject->Enable(true);
    m_ButtonCancel->Enable(true);
    m_ButtonAccept->Enable(false);

    wxSetWorkingDirectory(old_dir);

    cppDEL(settings);

    return;
  }

  fileSTL = mafGetApplicationDirectory().c_str();
  fileSTL<<m_Path;
  fileSTL<<m_PatientCaseId;
  fileSTL<<"/";
  fileSTL<<settings->GetFileNameSTL();

  fileTXT = mafGetApplicationDirectory().c_str();
  fileTXT<<m_Path;
  fileTXT<<m_PatientCaseId;
  fileTXT<<"/";
  fileTXT<<settings->GetFileNameTXT();

  
  if (!::wxRemoveFile(fileSTL.GetCStr()))
  {
  	mafLogMessage("Impossible to remove %s",fileSTL.GetCStr());
  }
  if (!::wxRemoveFile(fileTXT.GetCStr()))
  {
    mafLogMessage("Impossible to remove %s",fileTXT.GetCStr());
  }
  /*::wxRemoveFile(fileInput.GetCStr());*/

  ::wxCopyFile(cachedRXFileName.GetCStr(),fileInput.GetCStr());

  mafLogMessage("Executing Command Line : %s",commandLine.GetCStr());

  long pid = wxExecute(commandLine.GetCStr(),wxEXEC_ASYNC);

//   if (pid != 0)
//   {
//     wxMessageBox(_("Error during execution of external tool!"));
//     return;
//   }

//   int i=0;
// 
//   int timeOut = 0;
// 	//FAKE COMPUTATION!!!!
//   while((!wxFile::Exists(fileSTL) || !wxFile::Exists(fileTXT))&&(timeOut<10000))
// 	{
// 		m_Gauge->SetValue(i);
//     mafYield();
//     ::wxSafeYield(NULL,true); //fix on bug #2082
//     
//     mafSleep(500);
// 
//     if (i==100)
//     {
//     	i=0;
//     }
//     else
//     {
//       i+=10;
//     }
// 
//     timeOut++;
// 	}
// 
//   if (timeOut>100)
//   {
//     wxMessageBox("Output files not found!");
//     mafYield();
// 
//     m_Gauge->SetValue(100);
//     ::wxSafeYield(NULL,true); //fix on bug #2082
// 
//     delete wait;
// 
//     m_ButtonReject->Enable(true);
//     m_ButtonCancel->Enable(true);
//     m_ButtonAccept->Enable(false);
// 
//     wxSetWorkingDirectory(old_dir);
// 
//     cppDEL(settings);
//     return;
//   }
// 
//   ShowReconstrution();
//   ShowLandmarks();
// 
//   mafYield();

  ::wxSafeYield(NULL,true); //fix on bug #2082

  delete wait;

  m_ButtonReject->Enable(true);
  m_ButtonCancel->Enable(true);
  /*m_ButtonAccept->Enable(true);*/

  wxSetWorkingDirectory(old_dir);

  cppDEL(settings);
}
//----------------------------------------------------------------------------
void iGuiDialog3DReconstrution::ShowRX()
//----------------------------------------------------------------------------
{

  mafVMERoot *root = m_Input;

  mafVMEImage *RX = NULL;
  mafNodeIterator *iter = root->NewIterator();
  for (mafNode *node = iter->GetFirstNode(); node; node = iter->GetNextNode())
  {
    if (node->GetTagArray()->IsTagPresent(TAG_RX))
    {
      RX = mafVMEImage::SafeDownCast(node);
      break;
    }
  }

  iter->Delete();

  if (!RX)
  {
    return;
  }

  vtkImageData *image = vtkImageData::SafeDownCast(RX->GetOutput()->GetVTKData());
  image->Update();

  mafTagItem *calibrationTagItem = RX->GetTagArray()->GetTag("TAG_RX_CALIBRATION_SPACING");

  vtkMAFSmartPointer<vtkImageData> imageCopy;

  if (calibrationTagItem != NULL)
  {
	  double calibrationSpacing = calibrationTagItem->GetComponentAsDouble(0);
	  imageCopy->DeepCopy(image);

	  imageCopy->SetSpacing(calibrationSpacing, calibrationSpacing , 1);		
	  imageCopy->Modified();

	  image = imageCopy;
  }

  double origin[3];
  image->GetOrigin(origin);
  image->GetBounds(m_SliceBounds);
  double diffx,diffy;
  diffx=m_SliceBounds[1]-m_SliceBounds[0];
  diffy=m_SliceBounds[3]-m_SliceBounds[2];

  vtkNEW(m_SliceLookupTable);
  vtkNEW(m_SliceTexture);
  m_SliceTexture->InterpolateOn();
  m_SliceTexture->SetQualityTo32Bit();
  m_SliceTexture->RepeatOff();
  vtkNEW(m_SlicePlane);
  m_SlicePlane->SetOrigin(origin);
  m_SlicePlane->SetPoint1(origin[0]+diffx,origin[1],origin[2]);
  m_SlicePlane->SetPoint2(origin[0],origin[1],origin[2]-diffy);
  vtkNEW(m_SliceMapper);
  m_SliceMapper->SetInput(m_SlicePlane->GetOutput());
  m_SliceMapper->ScalarVisibilityOff();
  vtkNEW(m_SliceActor);
  m_SliceActor->SetMapper(m_SliceMapper);
  m_SliceActor->SetTexture(m_SliceTexture); 
  m_SliceActor->VisibilityOn();

  m_Rwi3DView->m_RenBack->AddActor(m_SliceActor);

  m_SliceTexture->SetInput(image);
  m_SliceTexture->Modified();

  double range[2];
  image->GetScalarRange(range);
  m_SliceLookupTable->SetTableRange(range);
  m_SliceLookupTable->SetWindow(range[1] - range[0]);
  m_SliceLookupTable->SetLevel((range[1] + range[0]) / 2.0);
  m_SliceLookupTable->Build();

  m_SliceTexture->MapColorScalarsThroughLookupTableOn();
  m_SliceTexture->SetLookupTable((vtkLookupTable *)m_SliceLookupTable);

  m_Rwi3DView->CameraReset();
  m_Rwi3DView->CameraUpdate();

}

//----------------------------------------------------------------------------
void iGuiDialog3DReconstrution::LoadAndShowReconstruction()
//----------------------------------------------------------------------------
{

  iGuiSettingsPlanning *settings = new iGuiSettingsPlanning(NULL);
  mafString fileSTL = "";

  fileSTL = mafGetApplicationDirectory().c_str();
  fileSTL<<m_Path;
  fileSTL<<m_PatientCaseId;
  fileSTL<<"/";
  fileSTL<<settings->GetFileNameSTL();

  if (!::wxFileExists(fileSTL.GetCStr()))
  {
    mafLogMessage("File %s not exist!",fileSTL.GetCStr());
    return;
  }

  mafOpImporterSTL *importer = new mafOpImporterSTL();
  importer->SetFileName(fileSTL.GetCStr());
  importer->SetInput(m_Input);
  importer->ImportSTL();
  importer->OpDo();

  std::vector<mafVMESurface*> surfaces;
  importer->GetImportedSTL(surfaces);

  mafVMESurface *reconstruction = surfaces[0];
  /*reconstruction->GetVTKOutput*/
  if (reconstruction == NULL)
  {
    return;
  }
  
  reconstruction->GetTagArray()->SetTag(TAG_3D_RECONSTRUCTION,"");
  reconstruction->Update();

  vtkPolyData *pd = vtkPolyData::SafeDownCast(reconstruction->GetOutput()->GetVTKData());
  pd->Update();

  double center[3];
  double bounds[6];
  pd->GetCenter(center);
  pd->GetBounds(bounds);

  double origin[3],point1[3],point2[3];
  m_SlicePlane->GetOrigin(origin);
  m_SlicePlane->GetPoint1(point1);
  m_SlicePlane->GetPoint2(point2);

//   m_SlicePlane->SetOrigin(origin[0],bounds[3],origin[2]);
//   m_SlicePlane->SetPoint1(point1[0],bounds[3],point1[2]);
//   m_SlicePlane->SetPoint2(point2[0],bounds[3],point2[2]);
//   m_SlicePlane->Update();

  double diffx,diffy;
  diffx=m_SliceBounds[1]-m_SliceBounds[0];
  diffy=m_SliceBounds[3]-m_SliceBounds[2];

  mafMatrix matrix;
  mafSmartPointer<mafTransform> transform;
  transform->SetMatrix(matrix);
  transform->Translate(-m_SliceBounds[0],-m_SliceBounds[2],-m_SliceBounds[4],POST_MULTIPLY);
  transform->RotateX(-90,POST_MULTIPLY);
  transform->Translate(m_SliceBounds[0],m_SliceBounds[2],m_SliceBounds[4],POST_MULTIPLY);
  // transform->Translate(diffx/2,0.0,-diffy/2,POST_MULTIPLY);

  reconstruction->SetAbsMatrix(transform->GetMatrix());
  reconstruction->Update();

  vtkMAFSmartPointer<vtkTransform> tr;
  tr->SetMatrix(reconstruction->GetOutput()->GetAbsMatrix()->GetVTKMatrix());

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> tp;
  tp->SetInput(pd);
  tp->SetTransform(tr);           
  tp->Update();
  
  vtkMAFSmartPointer<vtkPolyDataMapper> mapper;
  mapper->SetInput(tp->GetOutput());

  reconstruction->GetMaterial()->m_Prop->SetOpacity(0.5);

  vtkNEW(m_ReconstructionActor);
  m_ReconstructionActor->SetMapper(mapper);
  m_ReconstructionActor->SetProperty(reconstruction->GetMaterial()->m_Prop);

  m_Rwi3DView->m_RenFront->AddActor(m_ReconstructionActor);
  m_Rwi3DView->m_RenFront->Render();
  m_Rwi3DView->CameraReset();
  m_Rwi3DView->CameraUpdate();

  cppDEL(importer);
  cppDEL(settings);
}
//----------------------------------------------------------------------------
void iGuiDialog3DReconstrution::ShowLandmarks()
  //----------------------------------------------------------------------------
{
  mafVMELandmarkCloud *cloud = mafVMELandmarkCloud::SafeDownCast(FindVmeByTag(m_Input,TAG_LND_CLOUD));

  if (cloud == NULL)
  {
    return;
  }

  vtkPolyData *pd = vtkPolyData::SafeDownCast(cloud->GetOutput()->GetVTKData());
  pd->Update();

  vtkMAFSmartPointer<vtkTransform> tr;
  tr->SetMatrix(cloud->GetOutput()->GetAbsMatrix()->GetVTKMatrix());

  vtkMAFSmartPointer<vtkSphereSource> sphere;
  sphere->SetRadius(10.0);
  sphere->Update();

  vtkMAFSmartPointer<vtkMAFExtendedGlyph3D> glyph;
  glyph->SetInput(pd);
  glyph->SetSource(sphere->GetOutput());
  glyph->Update();

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> tp;
  tp->SetInput(glyph->GetOutput());
  tp->SetTransform(tr);           
  tp->Update();

  vtkMAFSmartPointer<vtkPolyDataMapper> mapper;
  mapper->SetInput(tp->GetOutput());

  vtkNEW(m_LandmarksActor);
  m_LandmarksActor->SetMapper(mapper);

  m_Rwi3DView->m_RenFront->AddActor(m_LandmarksActor);
  m_Rwi3DView->m_RenFront->Render();
  m_Rwi3DView->CameraReset();
  m_Rwi3DView->CameraUpdate();
}
//----------------------------------------------------------------------------
void iGuiDialog3DReconstrution::LoadAndShowLandmarks()
//----------------------------------------------------------------------------
{
  iGuiSettingsPlanning *settings = new iGuiSettingsPlanning(NULL);
  mafString fileTXT = "";

  fileTXT = mafGetApplicationDirectory().c_str();
  fileTXT<<m_Path;
  fileTXT<<m_PatientCaseId;
  fileTXT<<"/";
  fileTXT<<settings->GetFileNameTXT();

  FILE *file = fopen(fileTXT.GetCStr(),"r");

  mafSmartPointer<mafVMELandmarkCloud> cloud;


  int index = 0;
  int landmarkIndex = 0;
  bool readPCA = false;
  bool readHeadRadius = false;
  while (!feof(file))
  {
    char *line = new char[1000];
    strcpy(line,"");

    fgets(line, 1000, file);

    if (line[0] == '#')
    {
      continue;
    }
    if (index == 0)// Read # Femoral Head Centre
    {
      wxStringTokenizer tkz(line,wxT(' '));
      int num_t = tkz.CountTokens();

      if (num_t != 3 && num_t != 0)
      {
        mafLogMessage(_("ERROR: wrong file format"));
        return;
      }
      if (num_t == 0)
      {
        //End Line
        break;
      }

      double pt[3];
      int i=0;
      while (tkz.HasMoreTokens())
      {
        wxString token = tkz.GetNextToken();
        token.Replace("\n", "");

        pt[i] = atof(token.c_str());
        i++;
      }

      mafString landmarkName = "Lnd_";
      landmarkName << landmarkIndex;
      cloud->AppendLandmark(pt[0],pt[1],pt[2],landmarkName);

      index++;
    }
    else if (index == 1)//Read # Femoral Head Radius
    {
      index++;
      continue;
    }
    else if (index == 2)//Read # Femoral Neck Axis
    {
      wxStringTokenizer tkz(line,wxT(' '));
      int num_t = tkz.CountTokens();

      if (num_t != 3 && num_t != 0)
      {
        mafLogMessage(_("ERROR: wrong file format"));
        return;
      }
      if (num_t == 0)
      {
        //End Line
        break;
      }

      int i=0;
      while (tkz.HasMoreTokens())
      {
        wxString token = tkz.GetNextToken();
        token.Replace("\n", "");

        m_NeckVector[i] = atof(token.c_str());
        i++;
      }
      index++;
      continue;
    }
    else if (index == 3)//Read # Posterior-Anterior Vector
    {
      index++;
      continue;
    }
    else if (index == 4)//Read # Shaft Axis Vector
    {
      index++;
      continue;
    }
    else if (index == 5)//Read # Shape Coefficients
    {
      wxStringTokenizer tkz(line,wxT(' '));
      int num_t = tkz.CountTokens();

      if (num_t < 35 && num_t != 0)//PCA can be more than 35
      {
        mafLogMessage(_("ERROR: wrong file format"));
        return;
      }
      if (num_t == 0)
      {
        //End Line
        break;
      }

      int i=0;
      while (tkz.HasMoreTokens() && i<35)
      {
        wxString token = tkz.GetNextToken();
        token.Replace("\n", "");

        m_PCA[i] = atof(token.c_str());
        i++;
      }

      index++;
      continue;
    }
    else
    {
      index++;
    }

    

  }

  double diffx,diffy;
  diffx=m_SliceBounds[1]-m_SliceBounds[0];
  diffy=m_SliceBounds[3]-m_SliceBounds[2];

  mafMatrix matrix;
  mafSmartPointer<mafTransform> transform;
  transform->SetMatrix(matrix);
  transform->Translate(-m_SliceBounds[0],-m_SliceBounds[2],-m_SliceBounds[4],POST_MULTIPLY);
  transform->RotateX(-90,POST_MULTIPLY);
  transform->Translate(m_SliceBounds[0],m_SliceBounds[2],m_SliceBounds[4],POST_MULTIPLY);
  // transform->Translate(diffx/2,0.0,-diffy/2,POST_MULTIPLY);

  cloud->GetTagArray()->SetTag(TAG_LND_CLOUD,"");
  cloud->SetAbsMatrix(transform->GetMatrix());
  cloud->SetName("Lnd");
  cloud->ReparentTo(m_Input);
  cloud->Update();

  vtkPolyData *pd = vtkPolyData::SafeDownCast(cloud->GetOutput()->GetVTKData());
  pd->Update();

  vtkMAFSmartPointer<vtkTransform> tr;
  tr->SetMatrix(cloud->GetOutput()->GetAbsMatrix()->GetVTKMatrix());

  vtkMAFSmartPointer<vtkSphereSource> sphere;
  sphere->SetRadius(10.0);
  sphere->Update();

  vtkMAFSmartPointer<vtkMAFExtendedGlyph3D> glyph;
  glyph->SetInput(pd);
  glyph->SetSource(sphere->GetOutput());
  glyph->Update();

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> tp;
  tp->SetInput(glyph->GetOutput());
  tp->SetTransform(tr);           
  tp->Update();

  vtkMAFSmartPointer<vtkPolyDataMapper> mapper;
  mapper->SetInput(tp->GetOutput());

  vtkNEW(m_LandmarksActor);
  m_LandmarksActor->SetMapper(mapper);

  m_Rwi3DView->m_RenFront->AddActor(m_LandmarksActor);
  m_Rwi3DView->m_RenFront->Render();
  m_Rwi3DView->CameraReset();
  m_Rwi3DView->CameraUpdate();

  cppDEL(settings);
  fclose(file);
}
//----------------------------------------------------------------------------
bool iGuiDialog3DReconstrution::CheckResults(bool showResults /* = true */)
//----------------------------------------------------------------------------
{
  iGuiSettingsPlanning *settings = new iGuiSettingsPlanning(NULL);
  mafString fileSTL = settings->GetFileNameSTL();
  mafString fileTXT = settings->GetFileNameTXT();

  mafString resultFilenameSTL = mafGetApplicationDirectory().c_str();
  resultFilenameSTL<<m_Path;
  resultFilenameSTL<<m_PatientCaseId;
  resultFilenameSTL<<"/";
  resultFilenameSTL<<fileSTL.GetCStr();

  mafString resultFilenameTXT = mafGetApplicationDirectory().c_str();
  resultFilenameTXT<<m_Path;
  resultFilenameTXT<<m_PatientCaseId;
  resultFilenameTXT<<"/";
  resultFilenameTXT<<fileTXT.GetCStr();

  if (::wxFileExists(resultFilenameSTL.GetCStr()) && ::wxFileExists(resultFilenameTXT.GetCStr()) && showResults)
  {
    if (m_Training == false)
    {
	    LoadAndShowReconstruction();
	    LoadAndShowLandmarks();
    }
    else
    {
      ShowReconstruction();
      ShowLandmarks();
    }

    m_ButtonAccept->Enable(true);
    m_ButtonCheckResults->Enable(false);

    m_StatusLabel->SetLabel(_("3D Reconstruction done"));
  }
  if (!::wxFileExists(resultFilenameSTL.GetCStr()))
  {
    mafLogMessage("Result file %s not exist!",resultFilenameSTL.GetCStr());
    return false;
  }
  if (!::wxFileExists(resultFilenameTXT.GetCStr()))
  {
    mafLogMessage("Result file %s not exist!",resultFilenameTXT.GetCStr());
    return false;
  }

  return true;

}
//----------------------------------------------------------------------------
void iGuiDialog3DReconstrution::EnableShowModalityGui()
//----------------------------------------------------------------------------
{
  if (m_ButtonAccept)
  {
    m_ButtonAccept->Enable(true);
  }
  if (m_ButtonCheckResults)
  {
    m_ButtonCheckResults->Enable(false);
  }
  if (m_ButtonCancel)
  {
    m_ButtonCancel->Enable(true);
  }
  if (m_ButtonReject)
  {
    m_ButtonReject->Enable(true);
  }
}
//----------------------------------------------------------------------------
void iGuiDialog3DReconstrution::EnableWaitingModalityGui()
//----------------------------------------------------------------------------
{
  if (m_ButtonAccept)
  {
    m_ButtonAccept->Enable(false);
  }
  if (m_ButtonCheckResults)
  {
    m_ButtonCheckResults->Enable(true);
  }
  if (m_ButtonCancel)
  {
    m_ButtonCancel->Enable(true);
  }
  if (m_ButtonReject)
  {
    m_ButtonReject->Enable(true);
  }
}
//----------------------------------------------------------------------------
void iGuiDialog3DReconstrution::GetPCA( double pca[35] )
//----------------------------------------------------------------------------
{
  for (int i=0;i<35;i++)
  {
    pca[i] = m_PCA[i];
  }
}
//----------------------------------------------------------------------------
void iGuiDialog3DReconstrution::GetNeckVector(double neckVector[3])
//----------------------------------------------------------------------------
{
  for (int i=0;i<3;i++)
  {
    neckVector[i] = m_NeckVector[i];
  }
}
//----------------------------------------------------------------------------
void iGuiDialog3DReconstrution::ShowReconstruction()
//----------------------------------------------------------------------------
{
  mafVMESurface *reconstruction = mafVMESurface::SafeDownCast(FindVmeByTag(m_Input,TAG_3D_RECONSTRUCTION));

  if (reconstruction == NULL)
  {
    return;
  }

  reconstruction->Update();

  vtkPolyData *pd = vtkPolyData::SafeDownCast(reconstruction->GetOutput()->GetVTKData());
  pd->Update();

  vtkMAFSmartPointer<vtkTransform> tr;
  tr->SetMatrix(reconstruction->GetOutput()->GetAbsMatrix()->GetVTKMatrix());

  vtkMAFSmartPointer<vtkTransformPolyDataFilter> tp;
  tp->SetInput(pd);
  tp->SetTransform(tr);           
  tp->Update();

  vtkMAFSmartPointer<vtkPolyDataMapper> mapper;
  mapper->SetInput(tp->GetOutput());

  reconstruction->GetMaterial()->m_Prop->SetOpacity(0.5);

  vtkNEW(m_ReconstructionActor);
  m_ReconstructionActor->SetMapper(mapper);
  m_ReconstructionActor->SetProperty(reconstruction->GetMaterial()->m_Prop);

  m_Rwi3DView->m_RenFront->AddActor(m_ReconstructionActor);
  m_Rwi3DView->m_RenFront->Render();
  m_Rwi3DView->CameraReset();
  m_Rwi3DView->CameraUpdate();
}