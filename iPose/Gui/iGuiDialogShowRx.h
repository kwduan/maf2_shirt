/*=========================================================================
Program:   iPose
Module:    $RCSfile: iGuiDialogShowRx.h,v $
Language:  C++
Date:      $Date: 2012-01-27 15:46:22 $
Version:   $Revision: 1.1.2.21 $
Authors:   Matteo Giacomoni , Stefano Perticoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it
=========================================================================*/

#ifndef __iGuiDialogShowRx_H__
#define __iGuiDialogShowRx_H__

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class mafRWI;
class mafDeviceButtonsPadMouse;
class mafVMEImage;
class mafVMERoot;
class vtkActor;
class vtkPolyDataMapper;
class vtkTexture;
class vtkWindowLevelLookupTable;
class vtkPlaneSource;
class medInteractorDICOMImporter;
class vtkLineSource;
class vtkMapper;
class vtkTubeFilter;
class vtkSphereSource;
class vtkCutter;
class vtkPlane;

using namespace std;

//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "mafGUIDialog.h"
#include "iGuiDefines.h"

//----------------------------------------------------------------------------
// Include Measure 
//----------------------------------------------------------------------------
#include "vtkRenderer.h"

class Measure;
class DistanceBetweenPointsMeasure;
class DistanceBetweenLinesMeasure;
class AngleMeasure;
class vtkCaptionActor2D;
class vtkTextMapper;
class vtkTextProperty;

//----------------------------------------------------------------------------
// Const:
//----------------------------------------------------------------------------


/** iGuiDialogShowRx

This class shows a dialog featuring an RX: it is used to create different wizard steps based on
the dialogType enum passed into the constructor:

SHOW_RX_ONLY: Only show the RX loaded from rxFileName
ADD_POSSIBILITY_TO_OPEN_RX: Add the possibility to open a dicom file representing an RX image
CALIBRATE_TOOL: Create a tool to calibrate the RX by pick and drag through a circle or a line
MEASURE_TOOL: Create a tool to put measures on the RX (angle, distance between lines, distance between points)

REFACTOR TODO:
1) All measure classes (Measure, AngleMeasure , DistanceBetweenLines, DistanceBetweenPoints) should be extracted to single files and copyied to mafMedical
2) Calibrators code should became a CalibratorCircle and CalibratorLine and inherit from Measure, then this two classes should be moved to mafMedical 
*/
class I_GUI_EXPORT iGuiDialogShowRx : public mafGUIDialog
{
public:

	/** Different dialog types that can be created */
	enum RX_DIALOG_TYPE {SHOW_RX_ONLY = 0, ADD_POSSIBILITY_TO_OPEN_RX , CALIBRATE_TOOL , MEASURE_TOOL};

	iGuiDialogShowRx (const wxString& title,mafDeviceButtonsPadMouse *mouse, mafVMERoot *input, int dialogType , mafString rxFileName = "", mafString patientCaseId = "", bool training = false);
	virtual ~iGuiDialogShowRx (); 
	
	/** process events coming from other components */
	/*virtual*/ void OnEvent(mafEventBase *maf_event);

	/** return the side chosen */
	int GetSide(){return m_Side;};

	/** set the joint */
	void SetSide(int side);

	/** return the joint chosen */
	int GetJoint(){return m_Joint;};

	/** set the joint */
	void SetJoint(int joint);

	/** return the patient name */
	mafString GetPatientName(){return m_PatientName;};

	/** set the patient name */
	void SetPatientName(mafString name);

	/** return the diagnosis */
	mafString GetDiagnosis(){return m_Diagnosis;};

	/** set the diagnosis */
	void SetDiagnosis(mafString diagnosis);

  /** set patient weight */
  void SetPatientWeight(double weight);

  /** return the patient weight */
  double GetPatientWeight(){return m_PatientWeight;};

  /** set patient height */
  void SetPatientHeight(double height);

  /** return the patient height */
  double GetPatientHeight(){return m_PatientHeight;};

  /** set patient sex */
  void SetPatientSex(mafString sex);

  /** return the patient sex */
  mafString GetPatientSex(){return m_PatientSex;};

  /** set patient birthadate */
  void SetPatientBirthdate(mafString birthdate);

  /** return the patient birthadate */
  mafString GetPatientBirthdate(){return m_PatientBirthdate;};

  /** retunr rx calibration */
  double GetCalibration(){return m_RXImageCalibrationSpacing;};

protected:

	/** Create the measuring tools from measure types IDs */
	void CreateMeasures();

	/** Update measures in the m_MeasureValues data structure while performing current measure */
	void UpdateMeasureValues();

	/** Draw the Circle calibrating shape on mouse events*/
	void OnMouseDownCircle( double * pos );
	void OnMouseMoveCircle( double * currentPosition );

	/** Draw the Line calibrating shape on mouse events*/
	void OnMouseDownLine( double * pos );	
	void OnMouseMoveLine( double * currentPosition );
	
	/** Update new spacing ivar used for calibration */	
	void UpdateSpacingAfterCalibration();

	enum ID_GUIs
	{
		ID_CANCEL = MINID,
		ID_ACCEPT_BUTTON,
		ID_REJECT_BUTTON,
		ID_PATIENT_NAME,
    ID_PATIENT_HEIGHT,
    ID_PATIENT_WEIGHT,
		ID_SIDE,
		ID_JOINT,
		ID_OPEN_RX_FILE,
		ID_DIAGNOSIS,
		ID_CALIBRATION_MODE,
		ID_CALIBRATOR_SHAPE,
		ID_REMOVE_CALIBRATOR_SHAPE_BUTTON,
		ID_NEW_SPACING,
		ID_REPERE_SIZE,
		ID_CALIBRATE_BUTTON,
		ID_MEASURE_MODE,
		ID_MEASURE_NAME_COMBO,
		ID_JOINT_NAME,
		ID_MEASURE_VALUE,
		ID_ADD_TO_REPORT,
	};

	/** Create the interface */
	void CreateGui();

	enum MEASURES_COLUMNS {MEASURE_NAME , MEASURE_TYPE};

	/** Build measures to be created: add new measures here */
	void InitializeMeasures( vector<pair<string , vector<vector<string> > > > &m_Measures ) ;

	/** Enable the calibration GUI */
	void EnableCalibrationGui(bool enable);

	/** Read file RX */
	void ReadDICOM();

	/** Show the RX */
	void ShowRX();

	/** Create the RX slice pipeline */
	void CreateSlicePipeline();

	/** On activate/Deactivate calibration */
	void EnableCustomPickerInteractor();
	
	/** On choose the calibration shape */
	void OnIdCalibratorShape();
	
	/** On remove the actual calibration shape*/
	void OnIdRemoveCalibrationShapeButton();
	
	/** Create pipe for calibration line*/
	void CreateCalibrationLinePipeline();
	
	/** Create pipe for calibration circle*/
	void CreateCalibrationCirclePipeline();
	
private:

	/** Return the directory used to store patient informations */
	wxString GetPatientCaseDirectory();

	/** Store the measures in patient directory */
	void StoreMeasures();

	/** Store Loaded RX dicom file name for other steps */
	void StoreDicomFileName();

	/** Load RX dicom file name for other steps */
	void LoadDicomFileName();

	/** Store spacing value from calibration for other steps */
	void StoreCalibrationSpacing();

	/** Load Calibration spacing for other steps */
	void LoadCalibrationSpacing();

	/** Measures to be created */	
	vector<pair<string , vector<vector<string> > > > m_MeasuresDataStructure;

	/** Store the measures for the measuring tool */
	vector<Measure *> m_MeasuresVector;

	/** Measure value of the selected measure */
	double m_MeasureValue;

	/** Populate Combo with joint names */
	wxArrayString m_JointNames;

	wxArrayString m_MeasureJoints;
	wxArrayString m_MeasureNames;
	wxArrayString m_MeasureTypes;
	wxArrayString m_MeasureValues;

	/** Current measure handler */
	Measure *m_CurrentMeasure;

	mafRWI *m_Rwi3DView;

	mafDeviceButtonsPadMouse *m_Mouse;

	/** Register the dialog type */
	int m_DialogType;

	mafGUI *m_LateralGui;

	mafVMERoot *m_Input;

	mafString m_SpacingLabel;
	mafString m_Diagnosis;
	mafString m_FileRX;
	mafString m_PatientName;
  mafString m_PatientCaseId;
  mafString m_PatientSex;
  mafString m_PatientBirthdate;
  double m_PatientHeight;
  double m_PatientWeight;
	int m_Side;
	int m_Joint;

	mafVMEImage *m_OutputImage;

	vtkPolyDataMapper	*m_SliceMapper;
	vtkTexture *m_SliceTexture;
	vtkActor *m_SliceActor;
	vtkWindowLevelLookupTable	*m_SliceLookupTable;
	vtkPlaneSource *m_SlicePlane;

	medInteractorDICOMImporter *m_PickerInteractor;

	enum CALIBRATION_SHAPE {LINE = 0, CIRCLE = 1};

	int m_CalibrationShape;
	int m_CalibrationEnable;
	double m_RXSpacingAfterCalibrationGUIPreview;
	double m_RXImageCalibrationSpacing;
	double m_Spacing[3];
	int m_RepereSize;
	int m_CalibratorSize;

	enum DISTANCE_TYPE {POINT_DIST = 0 , PARALLEL_DIST , ANGLE  };

	int m_JointNameId;
	int m_MeasureNameId;
	bool m_P1LinePicked;
	bool m_P1Grabbed;
	bool m_P2LinePicked;
	bool m_P2Grabbed;

	double m_P1[3];
	double m_P2[3];

	bool m_Dragging;
	bool m_ResizingCircle;
  bool m_Training;

	vtkLineSource* m_CalibrationLine;
	vtkTubeFilter* m_CalibrationLineTubeFilter;
	vtkPolyDataMapper* m_CalibrationLineMapper;
	vtkActor* m_CalibrationLineActor;

	vtkSphereSource* m_CalibrationCircleSphereSource;
	vtkPlane *m_CalibrationCircleCuttingPlane;
	vtkCutter *m_CalibrationCircleCutter;
	vtkTubeFilter* m_CalibrationCircleTubeFilter;
	vtkPolyDataMapper* m_CalibrationCirclePDM;
	vtkActor* m_CalibrationCircleActor;

};

/**

Class representing a measure to be plugged in a 2D maf dialog.
Subclasses are implemented for Angle , Points and distance between lines measures.

*/
class Measure
{
public:

	Measure();
	~Measure();
		
	/** Show the measure tool */
	virtual void Show(bool show) {};

	/** Get the measure value */
	virtual double GetValue() {return m_Value;};

	/** Select the measure */
	virtual void Select(bool select) {};

	/** Set the measure line thickness */
	virtual void SetSize(double size) {m_Size = size;};

	/** Set the measure name to store */
	void SetName(wxString name) {m_Name = name;};

    /** Store the measure */ 
	virtual void Store();

	/** Set the maf render window for the measure tool */
	void SetRWI(mafRWI *rwi) {m_Rwi3DView = rwi;};

	/** Create vtk pipeline */
	virtual void CreatePipe() {};

	/** Destroy vtk pipeline */
	virtual void DestroyPipe() {};

	/** Mouse handlers */
	virtual void OnMouseDown(double *pickedPos) {};
	virtual void OnMouseMove(double *pickedPos) {};
	virtual void OnMouseUp() {};

protected:
	
	double m_Value;
	wxString m_Name;
	mafRWI *m_Rwi3DView;
	double m_Size;

	vtkCaptionActor2D *m_CaptionActor;
	vtkTextMapper *m_TextMapper;
	vtkTextProperty *m_CaptionActorProperty;

	void middle(double *middlePoint, double *p1 ,  double *p2);

	bool m_Select;

private:


};

//----------------------------------------------------------------------------
class AngleMeasure : public Measure
//----------------------------------------------------------------------------
{

public:

	AngleMeasure();
	~AngleMeasure();

	virtual void CreatePipe();
	virtual void DestroyPipe();
	virtual void OnMouseDown( double *pickedPos );
	virtual void OnMouseMove( double *pickedPos );
	virtual void OnMouseUp();
	virtual void Show( bool show );
	virtual void Store();
	virtual void Select(bool select); 

	double ComputeAngleMeasure();
	
protected:

	void UpdateCaptionActor();

private:

	vtkLineSource* m_O_P1Angle_Line;
	vtkTubeFilter* m_O_P1Angle_LineTubeFilter;
	vtkPolyDataMapper* m_O_P1Angle_LineMapper;
	vtkActor* m_O_P1Angle_LineActor;

	vtkLineSource* m_O_P2Angle_Line;
	vtkTubeFilter* m_O_P2Angle_LineTubeFilter;
	vtkPolyDataMapper* m_O_P2Angle_LineMapper;
	vtkActor* m_O_P2Angle_LineActor;
	vtkProperty* m_Property;

	bool m_Dragging;

	bool m_OriginPicked;
	bool m_OriginGrabbed;
	bool m_P1AnglePicked;
	bool m_P1AngleGrabbed;
	bool m_P2AnglePicked;
	bool m_P2AngleGrabbed;

	double m_Origin[3];
	double m_P1Angle[3];
	double m_P2Angle[3];


};

//----------------------------------------------------------------------------
class DistanceBetweenLinesMeasure : public Measure
//----------------------------------------------------------------------------
{
public:

	DistanceBetweenLinesMeasure();
	~DistanceBetweenLinesMeasure();

	virtual void DestroyPipe();

	virtual void CreatePipe();

	virtual void OnMouseDown( double *pickedPos );
	virtual void OnMouseMove( double *pickedPos );
	virtual void OnMouseUp();

	virtual void Show( bool show );
	virtual void Store();

	void Select(bool select);

protected:

	vtkLineSource* m_LineOriginal;
	vtkTubeFilter* m_LineOriginalTubeFilter;
	vtkPolyDataMapper* m_LineOriginalMapper;
	vtkActor* m_LineOriginalActor;

	vtkLineSource* m_LineCopy;
	vtkTubeFilter* m_LineCopyTubeFilter;
	vtkPolyDataMapper* m_LineCopyMapper;
	vtkActor* m_LineCopyActor;

	vtkProperty* m_Property;

private:

	bool m_P0LineOriginalPicked;
	bool m_P1LineOriginalPicked;
	bool m_P1LineOriginalGrabbed;
	bool m_PMiddleLineCopyPicked;
	bool m_PMiddleLineCopyGrabbed;

	double m_P0LineOriginal[3];
	double m_P1LineOriginal[3];

	bool m_LineOriginalGrabbed;
	bool m_LineCopyGrabbed;

	double m_P0LineCopy[3];
	double m_P1LineCopy[3];

	double m_PMiddleLineCopy[3];

	bool m_Dragging;
	bool m_P0LineOriginalGrabbed;

	void UpdateLineCopy( double pickedPoint[3] );
	void UpdateLineOriginal( double pickedPoint[3] );

	static double DistanceToLine (double x[3], double p1[3], double p2[3]);
	
	void UpdateCaptionActor();
};

//----------------------------------------------------------------------------
class DistanceBetweenPointsMeasure : public Measure
//----------------------------------------------------------------------------
{
public:

	DistanceBetweenPointsMeasure();
	~DistanceBetweenPointsMeasure();

	void DestroyPipe();
	void CreatePipe();

	virtual void OnMouseDown( double *pickedPos );	

	void UpdateCaptionActor();
	virtual void OnMouseMove( double *pickedPos );
	virtual void OnMouseUp();
	virtual void Show( bool show );
	virtual void Store();
	void Select(bool select);

protected:

private:

	vtkLineSource* m_Line;
	vtkTubeFilter* m_DBPLineTubeFilter;
	vtkPolyDataMapper* m_DBPLineMapper;
	vtkActor *m_DBPLineActor;
	vtkProperty* m_Property;

	bool m_P1Picked;
	bool m_P2Picked;

	bool m_P1Grabbed;
	bool m_P2Grabbed;

	double m_P1[3];
	double m_P2[3];

	bool m_Dragging;

};

#endif
