/*=========================================================================
Program:   iPose
Module:    $RCSfile: iDecl.h,v $
Language:  C++
Date:      $Date: 2012-03-21 07:23:35 $
Version:   $Revision: 1.1.2.14 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it
=========================================================================*/
#ifndef __iDecl_H__
#define __iDecl_H__

//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#include "iCommonDefines.h"
#include "mafString.h"
#include "lhpBuilderDecl.h"
#include <map>

//----------------------------------------------------------------------------
// forward references :
//----------------------------------------------------------------------------
class mafVMERoot;
class mafVME;
class mafString;

enum IPOSE_EVENTS
{
  IPOSE_GET_ROOT = LHP_EVT_USER_START,
  IPOSE_MSF_NEW, // crea un nuovo msf e evita di richiedere il salvataggio
  ID_SHOW_HIP_ROM_VIEW,
  ID_CLOSE_HIP_ROM_VIEW,
  IPOSE_SAVE,
};


enum IPOSE_PLANNING_WORKFLOW_IDs
{
	wxID_RETURN_NEXT = wxID_HIGHEST,
  wxID_RETURN_PREV, // agiungi calibrazione e misure
  wxID_RETURN_ADD_CASE,
  wxID_RESTART_PLANNING,
};

enum ELEMENT_STRUCT
{
  ID = 0,
  NAME,
  HEIGHT,
  WEIGHT,
  DIAGNOSIS,
  JOINT,
  SIDE,
  FILE_NAME,

};

struct grid_element
{
  mafString m_Name;
  mafString m_Diagnosis;
  mafString m_FileNameRX;
  int m_Joint;
  int m_Side;
  int m_Id;
  double m_Weight;
  double m_Height;
};

enum ID_SIDES
{
  ID_SIDE_RIGHT = 0,
  ID_SIDE_LEFT,
};

enum ID_JOINTS
{
  ID_JOINT_KNEE = 0,
  ID_JOINT_HIP,
  ID_JOINT_SHOULDER,
};

enum STEP_IDs
{
	ID_STEP_USER_CHOICE = 0,
	ID_STEP_SELECT_CASE_EXISTING, 
	ID_STEP_ADD_CASE,  // add a new case by loading an RX image (1/3 Prosthesis Planning -> add case)
	ID_STEP_CALIBRATE, // calibrate an RX image (2/3 Prosthesis Planning -> add case)
	ID_STEP_MEASURE, // Create measures on RX Image (3/3 Prosthesis Planning -> add case)
	ID_STEP_SHOW_RX,
	ID_STEP_3D_RECONSTRUTION_START,
  ID_STEP_3D_RECONSTRUTION_WAITING,
  ID_STEP_3D_RECONSTRUTION_SHOW,
	ID_STEP_CHOICE_IMPLANT,
	ID_STEP_PLACING_IMPLANT,
	ID_STEP_VALIDATION_VALUES,
	ID_STEP_LAST,
};

typedef struct DB
{
  mafString m_Manufacture;
  mafString m_Model;
  mafString m_FileName;
  mafString m_ProsthesisType;
}DB_PROSTHESIS_INFORMATION;

typedef struct DB_Movemets
{
  mafString m_Name;
  double m_Axis[3];
  double m_AngleInf;
  double m_AngleSup;
  double m_AngleCollision;
  bool m_StartFromPrevious;
  bool m_ShowMovement;
  bool m_Passed;
}DB_MOVEMENTS_INFORMATION;

#define RX_MEASURES_FILENAME "RXMeasures.txt";
#define PATH_PROSTHESIS_DB_FILE "/Config/DataBase/ProsthesisDB.xml"
#define PATH_MOVEMENTS_DB_FILE "/Config/DataBase/Movements.xml"
#define PROSTHESIS_DB_VERSION "1.0"
#define MOVEMENTS_DB_VERSION "1.0"

//TAGS to identifier VMEs in tree
#define TAG_3D_RECONSTRUCTION "3D_RECONSTRUCTION"
#define TAG_RX "RX"
#define TAG_RX_CALIBRATION_SPACING "RX_CALIBRATION_SPACING"
#define TAG_PROSTHESIS_TYPE_FEMORAL "PROSTHESIS_TYPE_FEMORAL"
#define TAG_SURFACE_TYPE_FEMORAL "SURFACE_TYPE_FEMORAL"
#define TAG_PROSTHESIS_TYPE_ACETABULAR "PROSTHESIS_TYPE_ACETABULAR"
#define TAG_SURFACE_TYPE_ACETABULAR "SURFACE_TYPE_ACETABULAR"
#define TAG_PROSTHESIS_TYPE_SHOULDER "PROSTHESIS_TYPE_SHOULDER"
#define TAG_PROSTHESIS_TYPE_KNEE "PROSTHESIS_TYPE_KNEE"
#define TAG_PROSTHESIS_TYPE_RESURFACING "PROSTHESIS_TYPE_RESURFACING"
#define TAG_PROSTHESIS_DB_ACETABULAR "PROSTHESIS_DB_ACETABULAR"
#define TAG_PROSTHESIS_DB "PROSTHESIS_DB"
#define TAG_PROSTHESIS_GROUP "PROSTHESIS_GROUP"
#define TAG_PROSTHESIS_COMPONENT "PROSTHESIS_COMPONENT"
#define TAG_LND_CLOUD "LND_CLOUD"
#define TAG_GROUP_ATTRIBUTES "GROUP_ATTRIBUTES"

/************************************************************************/
/* TAG TRAINING                                                         */
/************************************************************************/
#define TAG_PROSTHESIS_DB_ACETABULAR_TRAINING "PROSTHESIS_DB_ACETABULAR_TRAINING"
#define TAG_PROSTHESIS_DB_TRAINING "PROSTHESIS_DB_TRAINING"
#define TAG_PROSTHESIS_GROUP_TRAINING "PROSTHESIS_GROUP_TRAINING"
#define TAG_PROSTHESIS_COMPONENT_TRAINING "PROSTHESIS_COMPONENT_TRAINING"
#define TAG_GROUP_ATTRIBUTES_TRAINING "GROUP_ATTRIBUTES_TRAINING"


/** find and remove in the tree a vme with the tag and remove the vme */
I_COMMON_EXPORT void FindAndRemove(mafVMERoot *root, mafString tag);

/** find a vme by a tag */
I_COMMON_EXPORT mafVME* FindVmeByTag(mafVMERoot *root,mafString tag);

/** find a vmes by a tag */
I_COMMON_EXPORT void FindVmeByTag(mafVMERoot *root,mafString tag,std::vector<mafVME*> &vmes);

/** read measures stored inside the report */  
I_COMMON_EXPORT int ReadMeasures(wxString fileName, std::map<wxString,double> &measures);

/** Update the cache file with the new case */
I_COMMON_EXPORT void UpdateCache(mafString rxFile,grid_element elementToAdd,bool downloadData = false,int id = -1);

#endif