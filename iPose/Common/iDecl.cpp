/*=========================================================================
Program:   iPose
Module:    $RCSfile: iDecl.cpp,v $
Language:  C++
Date:      $Date: 2011-08-04 07:00:06 $
Version:   $Revision: 1.1.2.1 $
Authors:   Matteo Giacomoni
==========================================================================
Copyright (c) 2011
SCS s.r.l. - BioComputing Competence Centre (www.scsolutions.it - www.b3c.it
=========================================================================*/

#include "medDefines.h" 
//----------------------------------------------------------------------------
// NOTE: Every CPP file in the MAF must include "mafDefines.h" as first.
// This force to include Window,wxWidgets and VTK exactly in this order.
// Failing in doing this will result in a run-time error saying:
// "Failure#0: The value of ESP was not properly saved across a function call"
//----------------------------------------------------------------------------

#include "iDecl.h"
#include "mafVMERoot.h"
#include "mafString.h"
#include "mafNodeIterator.h"
#include "mafTagArray.h"
#include "mafVME.h"

#include <wx/tokenzr.h>
#include <wx\dir.h>

//----------------------------------------------------------------------------
void FindAndRemove(mafVMERoot *root, mafString tag)
//----------------------------------------------------------------------------
{
  mafVME *vme = NULL;
  mafNodeIterator *iter = root->NewIterator();
  for (mafNode *node = iter->GetFirstNode(); node; node = iter->GetNextNode())
  {
    if (node->GetTagArray()->IsTagPresent(tag))
    {
      vme = mafVME::SafeDownCast(node);
      break;
    }
  }

  iter->Delete();

  if (vme)
  {
    vme->ReparentTo(NULL);
  }
}
//----------------------------------------------------------------------------
mafVME* FindVmeByTag( mafVMERoot *root,mafString tag )
//----------------------------------------------------------------------------
{

  mafVME *vme = NULL;
  mafNodeIterator *iter = root->NewIterator();
  for (mafNode *node = iter->GetFirstNode(); node; node = iter->GetNextNode())
  {
    if (node->GetTagArray()->IsTagPresent(tag))
    {
      vme = mafVME::SafeDownCast(node);
      break;
    }
  }

  iter->Delete();

  return vme;
}
//----------------------------------------------------------------------------
I_COMMON_EXPORT void FindVmeByTag( mafVMERoot *root,mafString tag,std::vector<mafVME*> &vmes )
//----------------------------------------------------------------------------
{
  mafVME *vme = NULL;
  mafNodeIterator *iter = root->NewIterator();
  for (mafNode *node = iter->GetFirstNode(); node; node = iter->GetNextNode())
  {
    if (node->GetTagArray()->IsTagPresent(tag))
    {
      vme = mafVME::SafeDownCast(node);
      vmes.push_back(vme);
    }
  }

  iter->Delete();
}

//----------------------------------------------------------------------------
int ReadMeasures(wxString fileName, std::map<wxString,double> &measures)
//----------------------------------------------------------------------------
{

  if (!::wxFileExists(fileName))
  {
    return MAF_ERROR;
  }

  FILE *file = fopen(fileName.c_str(),"r");

  while (!feof(file))
  {
    char *line = new char[100];
    strcpy(line,"");

    fgets(line, 100, file);

    wxStringTokenizer tkz(line,wxT('#')); // Read t values
    int num_t = tkz.CountTokens();

    if (num_t != 4 && num_t != 0)
    {
      mafLogMessage(_("ERROR: wrong file format"));
      return MAF_ERROR;
    }
    if (num_t == 0)
    {
      //End Line
      break;
    }
    wxString tokenJointType = tkz.GetNextToken();
    tokenJointType.Replace("\n", "");

    if (tokenJointType != "Hip")
    {
      continue;
    }

    wxString tokenName = tkz.GetNextToken();
    tokenName.Replace("\n", "");

    wxString tokenMeasureType = tkz.GetNextToken();
    tokenMeasureType.Replace("\n","");

    wxString tokenMeasure = tkz.GetNextToken();
    tokenMeasure.Replace("\n","");

    measures[tokenName] = atof(tokenMeasure);

    delete []line;
  }

  std::map<wxString, double>::iterator it;
  it = measures.find("neck length (mm)");
  if(it == measures.end())
  {
    return MAF_ERROR;
  }

  it = measures.find("ccd (deg)");
  if(it == measures.end())
  {
    return MAF_ERROR;
  }

  it = measures.find("anteversion (deg)");
  if(it == measures.end())
  {
    return MAF_ERROR;
  }


  fclose(file);

  return MAF_OK;
}
//----------------------------------------------------------------------------
void UpdateCache(mafString rxFile,grid_element elementToAdd,bool downloadData,int id)
  //----------------------------------------------------------------------------
{
  wxString path, name, ext;
  if (downloadData == false)
  {
  	wxSplitPath(rxFile.GetCStr(),&path,&name,&ext);
  }

  mafString cacheFile = mafGetApplicationDirectory().c_str();
  if (downloadData == false)
  {
  	cacheFile << "/cache/cacheFile.txt";
  }
  else
  {
    cacheFile << "/download/cacheFile.txt";
  }

  mafLogMessage(_("Opening cache file : %s"),cacheFile.GetCStr());

  //Read cache file
  FILE *file = fopen(cacheFile.GetCStr(),"r");

  if (!file)
  {
    mafLogMessage(_("ERROR: cache file doesn't exist"));
    return;
  }

  std::vector<grid_element> elements;

  while (!feof(file))
  {
    char *line = new char[100];
    strcpy(line,"");
    fgets(line, 100, file);

    wxStringTokenizer tkz(line,wxT('#'),wxTOKEN_RET_EMPTY_ALL); // Read t values
    int num_t = tkz.CountTokens();

    if (num_t != 8 && num_t != 0)// == 1 -> empty line
    {
      mafLogMessage(_("ERROR: wrong file format"));
      return;
    }
    if (num_t == 0)
    {
      //End Line
      break;
    }

    int i = 0;
    grid_element element;
    while (tkz.HasMoreTokens())
    {
      wxString token = tkz.GetNextToken();
      token.Replace("\n", "");

      switch (i)
      {
      case ID:
        element.m_Id = atoi(token.c_str());
        break;
      case NAME:
        element.m_Name = (token.c_str());
        break;
      case DIAGNOSIS:
        element.m_Diagnosis = (token.c_str());
        break;
      case JOINT:
        element.m_Joint = atoi(token.c_str());
        break;
      case SIDE:
        element.m_Side = atoi(token.c_str());
        break;
      case FILE_NAME:
        element.m_FileNameRX = (token.c_str());
        break;
      case WEIGHT:
        element.m_Weight = atof(token.c_str());
        break;
      case HEIGHT:
        element.m_Height = atof(token.c_str());
        break;
      }

      ++i;
    }

    elements.push_back(element);

    delete []line;

  }

  int maxID = 0;
  if (!downloadData)
  {
	  //Find max ID
	  maxID = -1;
	  for (int i=0;i<elements.size();++i)
	  {
	    if (elements[i].m_Id > maxID)
	    {
	      maxID = elements[i].m_Id;
	    }
	  }
	
	  ++maxID;
	
	  mafString cachedRXFileName = mafGetApplicationDirectory().c_str();
	  cachedRXFileName << "/cache/";
	  cachedRXFileName << maxID;
	
	  if (wxDir::Exists(cachedRXFileName.GetCStr()))
	  {
	    ::wxRmdir(cachedRXFileName.GetCStr());
	  }
	
	  ::wxMkdir(cachedRXFileName.GetCStr());
	
	  cachedRXFileName << "/";
	  cachedRXFileName << name.c_str();
	  cachedRXFileName << ".";
	  cachedRXFileName << ext.c_str();
	
	  ::wxCopyFile(rxFile.GetCStr(),cachedRXFileName.GetCStr());
  }
  else
  {
    maxID = id;
  }


  grid_element element;
  element.m_Name = elementToAdd.m_Name;
  element.m_Id = maxID;
  element.m_Diagnosis = elementToAdd.m_Diagnosis;
  element.m_Joint = elementToAdd.m_Joint;
  element.m_Side = elementToAdd.m_Side;
  element.m_Height = elementToAdd.m_Height;
  element.m_Weight = elementToAdd.m_Weight;

  element.m_FileNameRX = name.c_str();
  element.m_FileNameRX<<".";
  element.m_FileNameRX<<ext.c_str();

  elements.push_back(element);


  fclose(file);

  //Write cache file with the new value
  file = fopen(cacheFile.GetCStr(),"w");

  for (int i=0;i<elements.size();++i)
  {
    fprintf(file,"%d#%s#%.2f#%.2f#%s#%d#%d#%s\n",elements[i].m_Id,elements[i].m_Name.GetCStr(),elements[i].m_Height,elements[i].m_Weight,elements[i].m_Diagnosis.GetCStr(),elements[i].m_Joint,elements[i].m_Side,elements[i].m_FileNameRX.GetCStr());
  }

  fclose(file);
}