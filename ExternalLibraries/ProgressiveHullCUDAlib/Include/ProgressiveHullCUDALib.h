/*========================================================================= 
Program: ProgressiveHullCUDALib
Module: $RCSfile: ProgressiveHullCUDALib.h,v $ 
Language: C++ 
Date: $Date: 2011-09-07 05:44:14 $ 
Version: $Revision: 1.1.2.1 $ 
Authors: Tomas Janak
Notes:
========================================================================== 
Copyright (c) 2010-2011 University of West Bohemia (www.zcu.cz)
See the COPYINGS file for license details 
=========================================================================
*/

#ifndef __CUDAProgressiveHullLib_h
#define __CUDAProgressiveHullLib_h

//----------------------------------------------------------------------------
// Include:
//----------------------------------------------------------------------------
#ifdef PROGRESSIVEHULLCUDA_EXPORTS
#define VTKCUDADECLSPEC	__declspec(dllexport)
#else
#define VTKCUDADECLSPEC	__declspec(dllimport)

#if defined(_MSC_VER) && _MSC_VER == 1600 && defined(_DEBUG)
#pragma comment(lib, "ProgressiveHullCUDALibd")
#else
#pragma comment(lib, "ProgressiveHullCUDALib")
#endif
#endif

#include <list>

// Class to represent a triangle.
class VTKCUDADECLSPEC Triangle
{
	int vertIndices[3]; // indices of vertices that forms the triangle, counter-clockwise ordering
	int neighbours[3]; // indices of neighboring triangles, counter-clockwise starting with the first edge (vert[0]-vert[1])

public:

	// Creates a new triangle from three given vertices, which indices are in the "indices" array.
	// PARAM:
	// indices - indices of the vertices that form the triangle
	Triangle(int *indices);

	// Creates an empty triangle object - no vertices specified.
	Triangle();

	// Sets the indices of the vertices of the given triangle.
	// PARAM:
	// indices - indices of the vertices that form the triangle
	void SetVertices(int *indices);
	// Get the indices of this triangle.
	// RETURN:
	// Array of indices of vertices that form this triangle (=> its length is always 3).
	int *GetVertices();

	// Sets the indices of the neighbouring triangles.
	// Neighbours must be sorted counter-clockwise, starting with the first edge (vert[0]-vert[1]).
	//  -1 if there is no triangle.
	// Length must be (at least) 3.
	// PARAM:
	// neighbours - indices of the neighbouring triangles
	void SetNeighbours(int *neighbours);

	// Get the indices of the neighbouring triangles.
	// Neighbours are sorted counter-clockwise, starting with the first edge (vert[0]-vert[1]).
	//  -1 if there is no triangle.
	// RETURN:
	// Array of indices of neighbouring triangles (=> its length is always 3).
	int *GetNeighbours();
};

// Class to represent a 3D vertex
class VTKCUDADECLSPEC Vertex
{
	int *edges; // indices of edges containing this vertex
	int numberEdges; // the number of edges that contain this vertex (i.e. number of values in "edges")
	int edgesSize; // the size of edges (allows to tell whether the "edges" need to be resized or not)
	float coo[3]; // x,y,z coordinates of the vertex

public:
	// creates an empty Vertex object
	Vertex();

	// Sets the coordinates of this vertex.
	// PARAM:
	// x, y, z - coordinates of the vertex
	void SetCoordinates(float x, float y, float z);

	// Returns an array containing the coordinates of this vertex.
	// RETURN:
	// An array with three numbers - x coordinate at index 0, y at 1 and z at 2.
	float *GetCoordinates();

	// Returns the number of edges attached to this vertex.
	// RETURN:
	// One integer - the number of edges that this vertex forms.
	int GetNumberOfEdge();

	// Returns the indices of edges attached to this vertex.
	// RETURN:
	// An array of indices to a global edge array. These edges are formed by this vertex.
	// The length of this array can be obtained by calling GetNumberOfEdges on this vertex.
	int *GetEdges();

	// Add an edge (its index) to this vertex.
	// PARAM:
	// edgeID - the index of the edge to be added
	void AddEdge(int edgeID);

	// Removes an edge with the given ID from the list of edges of this vertex.
	// PARAM:
	// edgeID - the index (in the global array of edges) of the edge to be removed.
	void RemoveEdge(int edgeID);
};

// Class to represent an edge in a triangular manifold mesh.
class VTKCUDADECLSPEC Edge
{
	int verts[2];
	int triangles[2];

public:

	// Creates an empty edge object.
	Edge();

	// Sets the vertices that create this edge.
	// PARAM:
	// vertexIdA, vertexIdB - the global indices of the vertices forming this edge.
	// No special order required.
	void SetVertices(int vertexIdA, int vertexIdB);

	// Set the triangles that share this edge.
	// PARAM:
	// triangleIdA, triangleIdB - the global indices of the triangles that share this edge.
	// No special order required.
	void SetTriangles(int triangleIdA, int triangleIdB);

	// Get the vertices that form this edge.
	// RETURN:
	// An array containg global indices of the vertices that form this edge (=> length 2)
	int *GetVertices();

	// Get the triangles that share this edge.
	// RETURN:
	// An array containg global indices of the triangles that share this edge
	// (=> length 2, only manifold meshes are supported).
	int *GetTriangles();
};



class VTKCUDADECLSPEC ProgressiveHullCUDA
{
public:
	class VTKCUDADECLSPEC BuildResult
	{
	public:
		int errorCode;							//0, if no error
		const char* errorMsg;				//NULL, if no error, otherwise a pointer to string to be printed, NOT TO BE RELEASED

		float* newVertCoordinates;	//an array of vertices, must be RELEASED 
		int* newTriangles;					//an array describing the topology of mesh
		int* newVertEdgeIndices;		//edges 

		int newNumberOfEdges;				//number of edges in the final mesh
	public:
		BuildResult();
		~BuildResult();
	};
public:
	// Checks if the machine contatins a cuda-capable graphics card
	// RETURN:
	// true if a cuda 2.0 capable device is present
	static bool IsCudaCapable();

	// Creates a progressive hull of a given mesh (prepares the data then calls
	// a CUDA kernell which actually creates the hull).
	// PARAM:
	// verts - arrays containing all the vertices, edges and triangles of the mesh to be decimated
	// numberOfVertices, numberOfEdges, numberOfTriangles - Amount of primitives the mesh is made of (i.e. lenght of the arrays described above)
	//targetPoints - number of points to be left in the mesh
	//meshRoughness - tolerance of edge decimation, from -1 to 1, 1 means all decimations are tolerated (even bad ones),
	//-1 means only really flat decimation are allowed. Optimal value is around 0.9. Greater value means mode edges will be decimated overall
	//maxValence - control the quality of mesh, an edge cannot be collapsed, if it would introduce a vertex with valence > maxValence
	//RETURNS: instance of BuildResult - must be deleted by the caller
	static BuildResult* BuildHull(Vertex *verts, int numberOfVertices, 
												Edge *edges, int numberOfEdges, 
												Triangle *tris, int numberOfTriangles,
												int targetPoints, double meshRoughness = 0.9,
												int maxValence = 15);	
};
#endif

