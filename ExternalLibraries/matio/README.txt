MATIO release libraries with HDF5 support built with Visual Studio 2010 from
https://github.com/sperticoni/matio-openmeeg

##################################################  
# Howto build MATIO with HDF5 library integration
##################################################  
Library should be built with CMake >=2.8.6 with default options from https://github.com/sperticoni/matio-openmeeg.git
run:

-> git clone -v --recurse-submodules --progress --branch master 
           "https://github.com/sperticoni/matio-openmeeg.git" "C:/YourTargetDir"

the --recurse-submodules command is necessary since some dependencies (like HDF5 library)are integrated as git submodules.
Then, in CMake 2.8.6, 

-> Use the same source and binary dir to build MATIO
-> Use the visual studio 2010 generator

##################################################  
# Howto build nmsBuilder with MATIO library integration
##################################################  

From nmsBuilder CMake 

-> set USE_MATIO_API to ON
-> set MATIO_HEADERS_DIR to YourBuildPath\matio-openmeeg\src\
-> set MATIO_LIB to YourBuildPath\matio-openmeeg\Debug\matio.lib

Depending on your system it may be necessary to copy 
c:\matio-openmeeg\contrib\msinttypes-r26\ files

in c:\matio-openmeeg\src\

when building nmsBuilder with MATIO support

