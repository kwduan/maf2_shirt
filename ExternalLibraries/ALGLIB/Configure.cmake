#
# Program:   MULTIMOD APPLICATION FRAMEWORK (MAF)
# Module:    $RCSfile: Configure.cmake,v $
# Language:  CMake 1.2
# Date:      $Date: 2009-04-21 12:31:39 $
# Version:   $Revision: 1.2.2.1 $
#
# Description:
# Project file for configuring the ALGLIB library as an external project.
# Return variables:
# ALGLIB_CONFIGURED  true if library configured correctly

INCLUDE (${MFL_SOURCE_PATH}/modules/PackagesMacro.cmake)
INCLUDE (${MFL_SOURCE_PATH}/modules/PatchMacro.cmake)

# this is to build ALGLIB inside the MAF tree
IF (EXISTS "${ALGLIB_SOURCE_DIR}/Sources")   
  SET (ALGLIB_SOURCE_PATH "${ALGLIB_SOURCE_DIR}/Sources")
ENDIF (EXISTS "${ALGLIB_SOURCE_DIR}/Sources")

#look if there is a package for ALGLIB
IF (EXISTS "${ALGLIB_SOURCE_DIR}/Packages")   
  MESSAGE(STATUS "ALGLIB: Found ALGLIB packages")
  # here we should unpack into "${ALGLIB_BINARY_DIR}/Sources"
  SET (ALGLIB_PACKAGE_PATH "${ALGLIB_SOURCE_DIR}/Packages")
  SET (ALGLIB_PATCH_PATH "${ALGLIB_SOURCE_DIR}/Patches")
  ##The macro must run only one time
  SET (ALGLIB_SOURCE_PATH "${ALGLIB_BINARY_DIR}/Sources")
  IF (NOT EXISTS "${ALGLIB_SOURCE_PATH}")
	  FIND_AND_UNPACK_PACKAGE (alglib-3.8.0.cpp ${ALGLIB_PACKAGE_PATH} "${ALGLIB_BINARY_DIR}/Sources" ${ALGLIB_SOURCE_PATH})
	  FIND_AND_UNPACK_PACKAGE (extras ${ALGLIB_PACKAGE_PATH} "${ALGLIB_BINARY_DIR}/Sources" ${ALGLIB_SOURCE_PATH})
	  FIND_AND_APPLAY_PATCHES (cpp ${ALGLIB_PATCH_PATH} "${ALGLIB_SOURCE_PATH}")
  ENDIF (NOT EXISTS "${ALGLIB_SOURCE_PATH}")
ENDIF (EXISTS "${ALGLIB_SOURCE_DIR}/Packages")

IF (ALGLIB_SOURCE_PATH)
  MESSAGE(STATUS "ALGLIB: Configuring ALGLIB")

  # default setting for building the ALGLIB library  
  SET (ALGLIB_BINARY_PATH "${ALGLIB_BINARY_DIR}/Build" )
  OPTION (ALGLIB_FORCE_CONFIGURE "Wether to force configuration of ALGLIB" FALSE)
  # check if already configured
  IF (ALGLIB_FORCE_CONFIGURE OR NOT EXISTS ${ALGLIB_BINARY_DIR}/build.cmake)
    # Configuring the ALGLIB library    
    FILE(MAKE_DIRECTORY "${ALGLIB_BINARY_PATH}")
    
    # Run configuration of ALGLIB library
    MESSAGE(STATUS "ALGLIB: Configuring external ALGLIB project")
    EXEC_PROGRAM(${CMAKE_COMMAND} "${ALGLIB_BINARY_PATH}" ARGS "${ALGLIB_SOURCE_PATH}" -G"${CMAKE_GENERATOR}" -DLIBRARY_OUTPUT_PATH:PATH="${LIBRARY_OUTPUT_PATH}" -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
    -DEXECUTABLE_OUTPUT_PATH:PATH="${EXECUTABLE_OUTPUT_PATH}" -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
     OUTPUT_VARIABLE CMAKE_OUTPUT RETURN_VALUE CMAKE_RETURN)
    
    # write configure log file
    FILE(WRITE ${ALGLIB_BINARY_DIR}/CMake-Log.txt "CMake configure output ${CMAKE_OUTPUT}" )
  
    IF (CMAKE_RETURN)
      # in case of error prompt an error dialog
      MESSAGE("CMake configure returned value = " ${CMAKE_RETURN})
      MESSAGE(FATAL_ERROR "${SUBPROJECT_NAME} configure error, cmake output = " ${CMAKE_OUTPUT})     
    ELSE (CMAKE_RETURN)
      # load ALGLIB build command from cmake's cache
      LOAD_CACHE(${ALGLIB_BINARY_PATH} READ_WITH_PREFIX ALGLIB_ MAKECOMMAND)
      LOAD_CACHE(${ALGLIB_BINARY_PATH} READ_WITH_PREFIX ALGLIB_ BUILD_SHARED)
      
      #Split space separated arguments into a semi-colon separated list. Necessary for correct command line generation
      SEPARATE_ARGUMENTS(ALGLIB_MAKECOMMAND)
      IF("${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}" GREATER 2.0)
      	STRING (REPLACE "\\" "\\\\"  ALGLIB_MAKECOMMAND "${ALGLIB_MAKECOMMAND}")
      ENDIF("${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}" GREATER 2.0)
      MESSAGE(STATUS "Creating file ${ALGLIB_BINARY_DIR}/build.cmake")
    	CONFIGURE_FILE("${ALGLIB_SOURCE_DIR}/build.cmake.in" "${ALGLIB_BINARY_DIR}/build.cmake" ESCAPE_QUOTES @ONLY IMMEDIATE)
    	
    ENDIF (CMAKE_RETURN)
  ENDIF (ALGLIB_FORCE_CONFIGURE OR NOT EXISTS ${ALGLIB_BINARY_DIR}/build.cmake)
  
  IF (EXISTS ${ALGLIB_BINARY_DIR}/build.cmake)
    # custom command to build the ALGLIB library
    IF (ALGLIB_BUILD_SHARED)
    	MESSAGE(STATUS "Adding custom command for ALGLIB SHARED library: ${LIBRARY_OUTPUT_PATH}/${CMAKE_CFG_INTDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}ALGLIB${CMAKE_SHARED_LIBRARY_SUFFIX}")
      ADD_CUSTOM_COMMAND(OUTPUT "${LIBRARY_OUTPUT_PATH}/${CMAKE_CFG_INTDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}ALGLIB${CMAKE_SHARED_LIBRARY_SUFFIX}"
                     COMMAND ${CMAKE_COMMAND}
                     ARGS 
                     -DMY_BUILD_PATH:STRING=${CMAKE_CFG_INTDIR} 
                     -P ${ALGLIB_BINARY_DIR}/build.cmake 
                     MAIN_DEPENDENCY ${ALGLIB_BINARY_DIR}/build.cmake
                     COMMENT "Custom command used to build ALGLIB library")
      ADD_CUSTOM_TARGET(BUILD_ALGLIB_LIBRARY DEPENDS "${LIBRARY_OUTPUT_PATH}/${CMAKE_CFG_INTDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}ALGLIB${CMAKE_SHARED_LIBRARY_SUFFIX}" )
    ELSE (ALGLIB_BUILD_SHARED)
      MESSAGE(STATUS "Adding custom command for ALGLIB STATIC library: ${LIBRARY_OUTPUT_PATH}/${CMAKE_CFG_INTDIR}/${CMAKE_STATIC_LIBRARY_PREFIX}ALGLIB${CMAKE_STATIC_LIBRARY_SUFFIX}")
      ADD_CUSTOM_COMMAND(OUTPUT "${LIBRARY_OUTPUT_PATH}/${CMAKE_CFG_INTDIR}/${CMAKE_STATIC_LIBRARY_PREFIX}ALGLIB${CMAKE_STATIC_LIBRARY_SUFFIX}"
                     COMMAND ${CMAKE_COMMAND}
                     ARGS 
                     -DMY_BUILD_PATH:STRING=${CMAKE_CFG_INTDIR} 
                     -P ${ALGLIB_BINARY_DIR}/build.cmake 
                     MAIN_DEPENDENCY ${ALGLIB_BINARY_DIR}/build.cmake
                     COMMENT "Custom command used to build ALGLIB library")
      ADD_CUSTOM_TARGET(BUILD_ALGLIB_LIBRARY ALL DEPENDS "${LIBRARY_OUTPUT_PATH}/${CMAKE_CFG_INTDIR}/${CMAKE_STATIC_LIBRARY_PREFIX}ALGLIB${CMAKE_STATIC_LIBRARY_SUFFIX}" )
    ENDIF (ALGLIB_BUILD_SHARED)
    
    SET (ALGLIB_CONFIGURED 1)
    
    # Add ALGLIB library to LHPExtLibs_TARGETS and its Build Project to LHPExtLibs_BUILD_TARGETS
		# These variables are then used in LHP projects to define dependencies and target libraries
    SET (LHPExtLibs_TARGETS ${LHPExtLibs_TARGETS} ALGLIB)      
    SET (LHPExtLibs_BUILD_TARGETS ${LHPExtLibs_BUILD_TARGETS} BUILD_ALGLIB_LIBRARY)      
    
  ELSE (EXISTS ${ALGLIB_BINARY_DIR}/build.cmake)
      SET (ALGLIB_CONFIGURED 0)
      SET (CONFIGURE_ERROR 1)
  ENDIF (EXISTS ${ALGLIB_BINARY_DIR}/build.cmake)
ENDIF (ALGLIB_SOURCE_PATH)

MARK_AS_ADVANCED (
  ALGLIB_BUILD_FILE
  ALGLIB_FORCE_CONFIGURE
)

# Write the list of variables and their values that are defined by CMake when running this CMakeList file
IF (DEBUG_MESSAGES)
  #FILE(WRITE ${ALGLIB_BINARY_DIR}/AllVariables.txt "")
  GET_CMAKE_PROPERTY(VARS VARIABLES)
  FOREACH(var ${VARS})
  	FILE(APPEND ${ALGLIB_BINARY_DIR}/AllVariables.txt 
  							"${var} \"${${var}}\"\n")
  ENDFOREACH(var ${VARS})
ENDIF (DEBUG_MESSAGES)
