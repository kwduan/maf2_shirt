// GPU_Solver.h

#ifndef _GPU_SOLVER_H_
#define _GPU_SOLVER_H_


#ifndef GPU_SOLVER_EXPORTS
	#define GPU_SOLVER_API __declspec(dllimport)

	typedef unsigned int cublasStatus;	//is defined in cublas.h
	#if defined(_MSC_VER) && _MSC_VER == 1600 && defined(_DEBUG)
		#pragma comment(lib, "GPU_Solverd.lib")
	#else
		#pragma comment(lib, "GPU_Solver.lib")
	#endif
#else
	#define GPU_SOLVER_API __declspec(dllexport)

	// includes, system
	#include "cublas.h"
	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include <math.h>
	#include <time.h>
#endif


typedef float gpu_float ;
typedef double gpu_double;


class GPU_SOLVER_API GPU_Solver {
	public:

	GPU_Solver();
	~GPU_Solver();

	gpu_float* allocateMem(int length);
	gpu_double* allocateMemD(int length);
	
	void registerGLBufferD(int buffer);
	gpu_double* mapGLBufferD(int buffer);
	void unmapGLBufferD(int buffer);

	void	freeMem(gpu_float* data);
	void	freeMemD(gpu_double* data);

	bool	transferMatrixData_hd(float* from, gpu_float* to, int width, int height); // from host to GPU
	bool	transferMatrixDataD_hd(double* from, gpu_double* to, int width, int height); // from host to GPU

	bool	transferMatrixData_hd(float* from, gpu_float* to, int size); // from host to GPU

	bool	transferMatrixData_dh(gpu_float* from, float* to, int width, int height); // from GPU to host
	bool	transferMatrixDataD_dh(gpu_double* from, double* to, int width, int height); // from GPU to host

	bool	transferMatrixData_dh(gpu_float* from, float* to, int size); // from GPU to host

	bool transferVectorData_hd(float* from, gpu_float* to, int length); // from host to GPU
	bool transferVectorDataD_hd(double* from, gpu_double* to, int length);
	
	bool transferVectorData_dh(gpu_float* from, float* to, int length);// from GPU to host
	bool transferVectorDataD_dh(gpu_double* from, double* to, int length);
		


	// perform GPU matrix x matrix multiplication "C= A.B" matrix C has width as matrix B and height as A  A= wA x hA; B= wB x hB; C= wB x hA
	void	matrixMul(gpu_float* A, int width_A, int height_A, gpu_float* B, int width_B, int height_B, gpu_float* C); 
	void	matrixMulD(gpu_double* A, int width_A, int height_A, char op_A, gpu_double* B, int width_B, int height_B, char op_B, gpu_double* C);
	void	matrixKoefMulD(gpu_double* A, int width_A, int height_A, char op_A, double alpha, gpu_double* B, int width_B, int height_B, char op_B, gpu_double* C); 

	// matrix transpose
	void	matrixTransposeD(gpu_double* inmatrix, gpu_double* outmatrix, int inheight, int inwidth);

	// sqare matrix multiplication
	void	matrixMul(gpu_float* A, gpu_float* B,  gpu_float* C, int edgelength); 

	// perform GPU matrix x vector multiplication "b= A.x"  vector c is one column vector 
	void	matrixVectorMul(gpu_float* A, int width_A,int height_A, gpu_float* x, gpu_float* b); 
	void	matrixVectorMul(gpu_float* A, int edgelength, gpu_float* x, gpu_float* b); 

	// perform GPU multiplycation of matrix A by scalar alpha 
	void    scalarMatrixMul(float alpha, gpu_float* A, int width_A,int height_A);
	void    scalarMatrixMul(float alpha, gpu_float* A, int edgelength);

	// compute residuum of function "r= b - Ax"  on GPU
	void	residuumCompute(gpu_float* r, gpu_float* b, gpu_float* A, int width_A,int height_A, gpu_float* x);
	void	residuumCompute(gpu_float* r, gpu_float* b, gpu_float* A, int edgelength, gpu_float* x);
	

	// vector addition with koeficient "x= x + alpha*v"
	void vectorKoefAdd(gpu_float* x, float alpha, gpu_float* v, int length );
	void vectorKoefAddD(gpu_double* x, double alpha, gpu_double* v, int length );

	// vector addition  r=r+b i.e. r+=b
	void	vectorAdd(gpu_float* x, gpu_float* v, int length);
	void	vectorAddD(gpu_double* x, gpu_double* v, int length);

	// vector addition  x= v+w
	void	vectorAdd(gpu_float* x, gpu_float* v, gpu_float* w, int length);

		// vector subtraction  r=r-b i.e. r-=b
	void	vectorSub(gpu_float* x, gpu_float* v, int length);
	void	vectorSubD(gpu_double* x, gpu_double* v, int length);

	// vector subtraction  x= v-w
	void	vectorSub(gpu_float* x, gpu_float* v, gpu_float* w, int length);

	double vectorAbsMax(gpu_double* v, int length);

	//  scalar *[vector]
	void	scalarVectorMul(float alpha, gpu_float* x, int length);
	void	scalarVectorMulD(double alpha, gpu_double* x, int length);

	float	dotProduct(gpu_float* v, gpu_float* w, int length);
	double	dotProductD(gpu_double* v, gpu_double* w, int length);


	float solveLinearSystem_CG(float* matrix, float* rightside, float* solution,int width, int height, int imax, float epsilon);
	float solveLinearSystem_SD(float* matrix, float* rightside, float* solution,int width, int height, int imax, float epsilon);

	float solveNonLinearSystem_CG(void (*computeGradient)(gpu_float* , gpu_float* ,int ) ,gpu_float* solution,int width, int imax, float epsilon);

	bool initCuda(); // call cublas init
		
private:	
	cublasStatus status;
};

extern "C" GPU_SOLVER_API float dotProduct(float* a, float* b, int length);
extern "C" GPU_SOLVER_API float* evaluate(float* matrix, float* vector, int width, int height);
extern "C" GPU_SOLVER_API float* subVectors(float* x, float* y, int length);
extern "C" GPU_SOLVER_API float* addVectors(float* x, float* y, int length);
extern "C" GPU_SOLVER_API float* mulVector(float* x, float multiplicant, int length);
extern "C" GPU_SOLVER_API float slow_solveLinearSystem_CG(float* matrix, float* rightside, float* solution,int width, int height, int imax, float epsilon);
extern "C" GPU_SOLVER_API float slow_solveLinearSystem_SD(float* matrix, float* rightside, float* solution,int width, int height, int imax, float epsilon);
extern "C" GPU_SOLVER_API float slow_solveNonLinearSystem_CG(void (*computeGradient)(float* , float* ,int )  ,float* solution,int width, int imax, float epsilon);
extern "C" GPU_SOLVER_API unsigned int timer;
extern "C" GPU_SOLVER_API void startTimer();
extern "C" GPU_SOLVER_API float stopTimer();

#endif
